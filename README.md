Nationale Nederlanden shop platform (Legal Fit Docs)
=======

[![StyleCI](https://styleci.io/repos/50011761/shield?branch=develop&style=flat)](https://styleci.io/repos/50011761)

## Projectdocumentatie
De projectdocumentatie wordt in de wiki bijgehouden. 

### Development
Aan de slag met development voor Legal Fit Docs? Check de wiki voor richtlijnen en de stappen die je moet doorlopen om een werkende ontwikkelomgeving te krijgen!
