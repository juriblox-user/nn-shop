<?php declare(strict_types=1);

namespace NnShop\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220721061804 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE templates_related (link_id INT AUTO_INCREMENT NOT NULL, related_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.template_id)\', template_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.template_id)\', INDEX IDX_2F8CD0294162C001 (related_id), INDEX IDX_2F8CD0295DA0FB8 (template_id), INDEX ix_primary (template_id, related_id), PRIMARY KEY(link_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE templates_related ADD CONSTRAINT FK_2F8CD0294162C001 FOREIGN KEY (related_id) REFERENCES templates_template (template_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE templates_related ADD CONSTRAINT FK_2F8CD0295DA0FB8 FOREIGN KEY (template_id) REFERENCES templates_template (template_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE orders_order ADD timestamp_referral_mail_sent DATETIME DEFAULT NULL COMMENT \'(DC2Type:core.time.datetime)\', ADD timestamp_upsell_mail_sent DATETIME DEFAULT NULL COMMENT \'(DC2Type:core.time.datetime)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE templates_related');
        $this->addSql('ALTER TABLE orders_order DROP timestamp_referral_mail_sent, DROP timestamp_upsell_mail_sent');
    }
}
