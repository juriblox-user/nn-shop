<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170509114510 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_order ADD timestamp_abandoned DATETIME DEFAULT NULL, ADD timestamp_updated DATETIME NOT NULL, DROP timestamp_interrupted');
        $this->addSql('CREATE INDEX ix_updated ON orders_order (timestamp_updated, order_status)');

        /*
         * Meest recente wijziging van iedere bestelling opzoeken
         */
        $builder = $this->connection->createQueryBuilder()
            ->select('order_id')
            ->from('orders_order', 'o');

        foreach ($builder->execute()->fetchAll() as $row) {
            $builder = $this->connection->createQueryBuilder()
                ->select('change_timestamp')
                ->from('orders_order_status', 's')

                ->where(
                    $builder->expr()->eq('order_id', ':id')
                )
                ->setParameter('id', $row['order_id'])

                ->orderBy('s.change_timestamp', 'DESC');

            $result = $builder->execute()->fetch();

            $this->addSql('UPDATE orders_order SET timestamp_updated = ? WHERE order_id = ?', [
                $result['change_timestamp'],
                $row['order_id'],
            ]);
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX ix_updated ON orders_order');
        $this->addSql('ALTER TABLE orders_order ADD timestamp_interrupted DATETIME DEFAULT NULL, DROP timestamp_abandoned, DROP timestamp_updated');
    }
}
