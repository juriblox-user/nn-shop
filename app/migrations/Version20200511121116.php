<?php

declare(strict_types=1);

namespace NnShop\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200511121116 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_order DROP registered_requested, DROP registered_address, DROP registered_sent');
        $this->addSql('ALTER TABLE templates_template DROP price_registered_email');
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_order ADD registered_requested TINYINT(1) NOT NULL, ADD registered_address VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:core.web.email_address)\', ADD registered_sent DATETIME DEFAULT NULL COMMENT \'(DC2Type:core.time.datetime)\'');
        $this->addSql('ALTER TABLE templates_template ADD price_registered_email INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\'');
    }
}
