<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190401093616 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_customer ADD coc_number VARCHAR(100) DEFAULT NULL, CHANGE customer_id customer_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.customer_id)\', CHANGE profile_id profile_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:profiles.profile_id)\', CHANGE locale_id locale_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:locales.locale_id)\', CHANGE customer_email customer_email VARCHAR(255) NOT NULL COMMENT \'(DC2Type:core.web.email_address)\', CHANGE password_hash password_hash VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:core.security.hashed_password)\', CHANGE password_salt password_salt VARCHAR(6) DEFAULT NULL COMMENT \'(DC2Type:core.security.short_token)\', CHANGE remote_reference remote_reference VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:juriblox.customer_reference)\', CHANGE customer_token customer_token VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:core.security.sensitive_value)\', CHANGE customer_worth customer_worth INT NOT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE timestamp_created timestamp_created DATETIME NOT NULL COMMENT \'(DC2Type:core.time.datetime)\', CHANGE customer_type customer_type VARCHAR(255) NOT NULL COMMENT \'(DC2Type:orders.customer_type)\', CHANGE email_change email_change VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:core.web.email_address)\', CHANGE customer_status customer_status VARCHAR(255) NOT NULL COMMENT \'(DC2Type:orders.customer_status)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_customer DROP coc_number, CHANGE customer_id customer_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.customer_id)\', CHANGE locale_id locale_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:locales.locale_id)\', CHANGE profile_id profile_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:profiles.profile_id)\', CHANGE customer_email customer_email VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:core.web.email_address)\', CHANGE email_change email_change VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:core.web.email_address)\', CHANGE password_hash password_hash VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:core.security.hashed_password)\', CHANGE password_salt password_salt VARCHAR(6) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:core.security.short_token)\', CHANGE remote_reference remote_reference VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:juriblox.customer_reference)\', CHANGE customer_status customer_status VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.customer_status)\', CHANGE timestamp_created timestamp_created DATETIME NOT NULL, CHANGE customer_token customer_token VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:core.security.sensitive_value)\', CHANGE customer_type customer_type VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.customer_type)\', CHANGE customer_worth customer_worth INT NOT NULL COMMENT \'(DC2Type:core.financial.money)\'');
    }
}
