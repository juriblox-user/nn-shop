<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171019105417 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE pages_page (id INT AUTO_INCREMENT NOT NULL, page_title VARCHAR(255) NOT NULL, page_content LONGTEXT NOT NULL, page_slug VARCHAR(255) NOT NULL, page_created DATETIME NOT NULL, page_updated DATETIME NOT NULL, UNIQUE INDEX UNIQ_5FC0FCA01F5987B8 (page_slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pages_page_translations (translation_id INT AUTO_INCREMENT NOT NULL, object_id INT DEFAULT NULL, translation_locale VARCHAR(8) NOT NULL, translation_field VARCHAR(32) NOT NULL, translation_content LONGTEXT DEFAULT NULL, INDEX IDX_27C984EC232D562B (object_id), UNIQUE INDEX lookup_unique_idx (translation_locale, object_id, translation_field), PRIMARY KEY(translation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pages_page_translations ADD CONSTRAINT FK_27C984EC232D562B FOREIGN KEY (object_id) REFERENCES pages_page (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pages_page_translations DROP FOREIGN KEY FK_27C984EC232D562B');
        $this->addSql('DROP TABLE pages_page');
        $this->addSql('DROP TABLE pages_page_translations');
    }
}
