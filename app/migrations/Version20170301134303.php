<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170301134303 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('SET foreign_key_checks = 0');

        $this->addSql('ALTER TABLE orders_advice CHANGE customer_id customer_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.customer_id)\'');
        $this->addSql('ALTER TABLE orders_session_browser CHANGE session_id session_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.session_id)\'');
        $this->addSql('ALTER TABLE orders_invoice CHANGE customer_id customer_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.customer_id)\'');
        $this->addSql('ALTER TABLE orders_invoice_item CHANGE invoice_id invoice_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.invoice_id)\'');
        $this->addSql('ALTER TABLE orders_order_status CHANGE order_id order_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.order_id)\'');
        $this->addSql('ALTER TABLE orders_payment CHANGE invoice_id invoice_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.invoice_id)\'');
        $this->addSql('ALTER TABLE orders_subscription CHANGE order_id order_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.order_id)\'');
        $this->addSql('ALTER TABLE templates_answer CHANGE question_id question_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.question_id)\', CHANGE document_id document_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.document_id)\'');
        $this->addSql('ALTER TABLE templates_category CHANGE shop_id shop_id CHAR(36) NOT NULL COMMENT \'(DC2Type:shops.shop_id)\'');
        $this->addSql('ALTER TABLE templates_company_activity CHANGE type_id type_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.company_type_id)\'');
        $this->addSql('ALTER TABLE templates_document CHANGE order_id order_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.order_id)\'');
        $this->addSql('ALTER TABLE templates_document_request CHANGE document_id document_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.document_id)\'');
        $this->addSql('ALTER TABLE templates_partner CHANGE remote_id remote_id SMALLINT NOT NULL');
        $this->addSql('ALTER TABLE templates_question CHANGE step_id step_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.question_step_id)\'');
        $this->addSql('ALTER TABLE templates_question_condition CHANGE question_id question_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.question_id)\'');
        $this->addSql('ALTER TABLE templates_question_option CHANGE question_id question_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.question_id)\'');
        $this->addSql('ALTER TABLE templates_question_step CHANGE template_id template_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.template_id)\'');
        $this->addSql('ALTER TABLE templates_template CHANGE partner_id partner_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.partner_id)\'');

        $this->addSql('SET foreign_key_checks = 1');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('SET foreign_key_checks = 0');

        $this->addSql('ALTER TABLE orders_advice CHANGE customer_id customer_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.customer_id)\'');
        $this->addSql('ALTER TABLE orders_invoice CHANGE customer_id customer_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.customer_id)\'');
        $this->addSql('ALTER TABLE orders_invoice_item CHANGE invoice_id invoice_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.invoice_id)\'');
        $this->addSql('ALTER TABLE orders_order_status CHANGE order_id order_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.order_id)\'');
        $this->addSql('ALTER TABLE orders_payment CHANGE invoice_id invoice_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.invoice_id)\'');
        $this->addSql('ALTER TABLE orders_session_browser CHANGE session_id session_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.session_id)\'');
        $this->addSql('ALTER TABLE orders_subscription CHANGE order_id order_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.order_id)\'');
        $this->addSql('ALTER TABLE templates_answer CHANGE document_id document_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.document_id)\', CHANGE question_id question_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.question_id)\'');
        $this->addSql('ALTER TABLE templates_category CHANGE shop_id shop_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:shops.shop_id)\'');
        $this->addSql('ALTER TABLE templates_company_activity CHANGE type_id type_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.company_type_id)\'');
        $this->addSql('ALTER TABLE templates_document CHANGE order_id order_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.order_id)\'');
        $this->addSql('ALTER TABLE templates_document_request CHANGE document_id document_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.document_id)\'');
        $this->addSql('ALTER TABLE templates_partner CHANGE remote_id remote_id SMALLINT NOT NULL');
        $this->addSql('ALTER TABLE templates_question CHANGE step_id step_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.question_step_id)\'');
        $this->addSql('ALTER TABLE templates_question_condition CHANGE question_id question_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.question_id)\'');
        $this->addSql('ALTER TABLE templates_question_option CHANGE question_id question_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.question_id)\'');
        $this->addSql('ALTER TABLE templates_question_step CHANGE template_id template_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.template_id)\'');
        $this->addSql('ALTER TABLE templates_template CHANGE partner_id partner_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.partner_id)\'');

        $this->addSql('SET foreign_key_checks = 1');
    }
}
