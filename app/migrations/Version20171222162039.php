<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171222162039 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("INSERT INTO translation_profile(profile_id, profile_created, profile_updated, profile_slug, profile_url, profile_country, profile_timezone, profile_currency, main_locale_id) SELECT UUID(), NOW(), NOW(), 'nl', 'http://juridox.nl', 'NL', 'Europe/Amsterdam', 'EUR', locale_id FROM translation_locale LIMIT 1");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("DELETE FROM translation_profile WHERE profile_slug='nl'");
    }
}
