<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180806150724 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_invoice_discount ADD discount_percentage SMALLINT DEFAULT NULL, ADD discount_amount INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', ADD discount_type VARCHAR(20) NOT NULL COMMENT \'(DC2Type:orders.discount_type)\';');
        $this->addSql('
            UPDATE orders_invoice_discount oid
              JOIN orders_discount od ON oid.discount_id = od.discount_id
            SET oid.discount_amount = od.discount_amount,
            oid.discount_percentage = od.discount_percentage,
            oid.discount_type = od.discount_type;
        ');
    }

    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_invoice_discount DROP discount_percentage, DROP discount_amount, DROP discount_type');
    }
}
