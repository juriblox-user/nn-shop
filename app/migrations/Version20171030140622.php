<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171030140622 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE templates_template DROP FOREIGN KEY FK_2592CF0BCCFA12B8');
        $this->addSql('DROP INDEX IDX_2592CF0BCCFA12B8 ON templates_template');
        $this->addSql('ALTER TABLE templates_template DROP profile_id, CHANGE template_id template_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.template_id)\', CHANGE partner_id partner_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.partner_id)\', CHANGE template_key template_key VARCHAR(6) NOT NULL COMMENT \'(DC2Type:templates.template_key)\', CHANGE price_check price_check INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE price_monthly price_monthly INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE price_unit price_unit INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE remote_id remote_id INT NOT NULL COMMENT \'(DC2Type:juriblox.template_id)\', CHANGE synced_timestamp synced_timestamp DATETIME DEFAULT NULL, CHANGE template_version template_version INT NOT NULL COMMENT \'(DC2Type:templates.template_version)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE templates_template ADD profile_id INT DEFAULT NULL, CHANGE template_id template_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.template_id)\', CHANGE partner_id partner_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.partner_id)\', CHANGE template_key template_key VARCHAR(6) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.template_key)\', CHANGE price_check price_check INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE price_monthly price_monthly INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE price_unit price_unit INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE remote_id remote_id INT NOT NULL COMMENT \'(DC2Type:juriblox.template_id)\', CHANGE synced_timestamp synced_timestamp DATETIME DEFAULT NULL, CHANGE template_version template_version INT NOT NULL COMMENT \'(DC2Type:templates.template_version)\'');
        $this->addSql('ALTER TABLE templates_template ADD CONSTRAINT FK_2592CF0BCCFA12B8 FOREIGN KEY (profile_id) REFERENCES translation_profile (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_2592CF0BCCFA12B8 ON templates_template (profile_id)');
    }
}
