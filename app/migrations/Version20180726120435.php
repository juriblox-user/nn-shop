<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180726120435 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX ix_remote ON templates_question');
        $this->addSql('CREATE INDEX ix_remote ON templates_question (remote_id, step_id)');
        $this->addSql('DROP INDEX ix_lookup ON templates_question_option');
        $this->addSql('DROP INDEX ix_remote ON templates_question_option');
        $this->addSql('CREATE INDEX ix_lookup ON templates_question_option (lookup_id, question_id)');
        $this->addSql('CREATE INDEX ix_remote ON templates_question_option (remote_id, question_id)');
        $this->addSql('DROP INDEX UNIQ_DA263A9D2A3E9C94 ON templates_question_step');
        $this->addSql('CREATE INDEX ix_remote ON templates_question_step (remote_id, template_id)');
        $this->addSql('DROP INDEX UNIQ_2592CF0B2A3E9C94 ON templates_template');
        $this->addSql('DROP INDEX ix_remote ON templates_template');
        $this->addSql('CREATE INDEX ix_remote ON templates_template (remote_id, partner_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX ix_remote ON templates_question');
        $this->addSql('CREATE INDEX ix_remote ON templates_question (remote_id, timestamp_deleted)');
        $this->addSql('DROP INDEX ix_lookup ON templates_question_option');
        $this->addSql('DROP INDEX ix_remote ON templates_question_option');
        $this->addSql('CREATE INDEX ix_lookup ON templates_question_option (lookup_id, timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_remote ON templates_question_option (remote_id, timestamp_deleted)');
        $this->addSql('DROP INDEX ix_remote ON templates_question_step');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DA263A9D2A3E9C94 ON templates_question_step (remote_id)');
        $this->addSql('DROP INDEX ix_remote ON templates_template');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2592CF0B2A3E9C94 ON templates_template (remote_id)');
        $this->addSql('CREATE INDEX ix_remote ON templates_template (remote_id, timestamp_deleted)');
    }
}
