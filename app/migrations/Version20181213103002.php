<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181213103002 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_customer ADD profile_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:profiles.profile_id)\'');
        $this->addSql('ALTER TABLE orders_customer ADD CONSTRAINT FK_461F53CFCCFA12B8 FOREIGN KEY (profile_id) REFERENCES translation_profile (profile_id)');
        $this->addSql('CREATE INDEX IDX_461F53CFCCFA12B8 ON orders_customer (profile_id)');
        $this->addSql('ALTER TABLE orders_customer RENAME INDEX fk_461f53cfe559dfd1 TO IDX_461F53CFE559DFD1');

        $query = $this->connection->prepare('SELECT profile_id FROM translation_profile WHERE profile_slug = \'nl-nl\' LIMIT 1');
        $query->execute();

        $row = $query->fetch();

//        $this->addSql('UPDATE orders_customer SET profile_id = :profile_id', [
//            'profile_id' => $row['profile_id'],
//        ]);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
