<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171103080923 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE blog_category_translations');
        $this->addSql('ALTER TABLE blog_category ADD locale_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE blog_category ADD CONSTRAINT FK_72113DE6E559DFD1 FOREIGN KEY (locale_id) REFERENCES translation_locale (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_72113DE6E559DFD1 ON blog_category (locale_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE blog_category_translations (translation_id INT AUTO_INCREMENT NOT NULL, object_id INT DEFAULT NULL, translation_locale VARCHAR(8) NOT NULL COLLATE utf8mb4_unicode_ci, translation_field VARCHAR(32) NOT NULL COLLATE utf8mb4_unicode_ci, translation_content LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, UNIQUE INDEX lookup_unique_idx (translation_locale, object_id, translation_field), INDEX IDX_85D2E1FE232D562B (object_id), PRIMARY KEY(translation_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE blog_category_translations ADD CONSTRAINT FK_85D2E1FE232D562B FOREIGN KEY (object_id) REFERENCES blog_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blog_article_templates CHANGE template_id template_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.template_id)\'');
        $this->addSql('ALTER TABLE blog_category DROP FOREIGN KEY FK_72113DE6E559DFD1');
        $this->addSql('DROP INDEX IDX_72113DE6E559DFD1 ON blog_category');
        $this->addSql('ALTER TABLE blog_category DROP locale_id');
    }
}
