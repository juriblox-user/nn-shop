<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170530135023 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_order ADD referrer_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:shops.referrer_id)\'');
        $this->addSql('ALTER TABLE orders_order ADD CONSTRAINT FK_B4833C39798C22DB FOREIGN KEY (referrer_id) REFERENCES shops_referrer (referrer_id)');
        $this->addSql('CREATE INDEX IDX_B4833C39798C22DB ON orders_order (referrer_id)');

        $this->addSql('ALTER TABLE orders_session ADD referrer_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:shops.referrer_id)\'');
        $this->addSql('ALTER TABLE orders_session ADD CONSTRAINT FK_2131A7AF798C22DB FOREIGN KEY (referrer_id) REFERENCES shops_referrer (referrer_id)');
        $this->addSql('CREATE INDEX IDX_2131A7AF798C22DB ON orders_session (referrer_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_order DROP FOREIGN KEY FK_B4833C39798C22DB');
        $this->addSql('DROP INDEX IDX_B4833C39798C22DB ON orders_order');
        $this->addSql('ALTER TABLE orders_order DROP referrer_id, CHANGE order_id order_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.order_id)\'');

        $this->addSql('ALTER TABLE orders_session DROP FOREIGN KEY FK_2131A7AF798C22DB');
        $this->addSql('DROP INDEX IDX_2131A7AF798C22DB ON orders_session');
        $this->addSql('ALTER TABLE orders_session DROP referrer_id, CHANGE session_id session_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.session_id)\'');
    }
}
