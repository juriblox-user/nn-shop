<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171012093035 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_order ADD shop_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:shops.shop_id)\'');
        $this->addSql('ALTER TABLE orders_order ADD CONSTRAINT FK_B4833C394D16C4DD FOREIGN KEY (shop_id) REFERENCES shops_shop (shop_id)');
        $this->addSql('CREATE INDEX IDX_B4833C394D16C4DD ON orders_order (shop_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_order DROP FOREIGN KEY FK_B4833C394D16C4DD');
        $this->addSql('DROP INDEX IDX_B4833C394D16C4DD ON orders_order');
        $this->addSql('ALTER TABLE orders_order DROP shop_id, CHANGE order_id order_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.order_id)\'');
    }
}
