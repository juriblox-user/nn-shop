<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Fix database schema after update symfony and symfony-core
 */
class Version20190103162838 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE ext_translations');
        $this->addSql('RENAME TABLE system_task TO system_job');

        $this->addSql('
            ALTER TABLE system_job 
              CHANGE task_id job_id CHAR(36) NOT NULL COMMENT \'(DC2Type:system.job_id)\', 
              CHANGE task_title job_title VARCHAR(100) NOT NULL, 
              CHANGE task_worker job_worker VARCHAR(100) NOT NULL, 
              CHANGE exception_timestamp exception_timestamp DATETIME DEFAULT NULL COMMENT \'(DC2Type:core.time.datetime)\', 
              CHANGE lock_expiration lock_expiration DATETIME DEFAULT NULL COMMENT \'(DC2Type:core.time.datetime)\', 
              CHANGE run_timestamp run_timestamp DATETIME DEFAULT NULL COMMENT \'(DC2Type:core.time.datetime)\', 
              CHANGE scheduled_timestamp scheduled_timestamp DATETIME NOT NULL COMMENT \'(DC2Type:core.time.datetime)\', 
              CHANGE task_schedule job_schedule VARCHAR(50) NOT NULL,
              ADD job_timezone VARCHAR(50) NOT NULL COMMENT \'(DC2Type:core.time.timezone)\' AFTER job_worker
        ');

        $this->addSql('UPDATE system_job SET job_timezone = \'Europe/Amsterdam\'');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

//        $this->addSql('CREATE TABLE ext_translations (translation_id INT AUTO_INCREMENT NOT NULL, translation_locale VARCHAR(8) NOT NULL COLLATE utf8mb4_unicode_ci, object_class VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, translation_field VARCHAR(32) NOT NULL COLLATE utf8mb4_unicode_ci, foreign_key VARCHAR(64) NOT NULL COLLATE utf8mb4_unicode_ci, translation_content LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(translation_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('RENAME TABLE system_job TO system_task');

        $this->addSql('
            ALTER TABLE system_task 
              CHANGE job_id task_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:system.task_id)\', 
              CHANGE job_title task_title VARCHAR(100) NOT NULL COLLATE utf8mb4_unicode_ci, 
              CHANGE job_worker task_worker VARCHAR(100) NOT NULL COLLATE utf8mb4_unicode_ci, 
              DROP job_timezone, 
              CHANGE exception_timestamp exception_timestamp DATETIME DEFAULT NULL, 
              CHANGE lock_expiration lock_expiration DATETIME DEFAULT NULL, 
              CHANGE run_timestamp run_timestamp DATETIME DEFAULT NULL, 
              CHANGE scheduled_timestamp scheduled_timestamp DATETIME NOT NULL, 
              CHANGE job_schedule task_schedule VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci
        ');

        $this->addSql('DROP TABLE products_product');
    }
}
