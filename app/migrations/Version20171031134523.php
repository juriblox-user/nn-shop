<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171031134523 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE shops_referrer ADD profile_id INT DEFAULT NULL, CHANGE referrer_id referrer_id CHAR(36) NOT NULL COMMENT \'(DC2Type:shops.referrer_id)\', CHANGE discount_id discount_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:orders.discount_id)\'');
        $this->addSql('ALTER TABLE shops_referrer ADD CONSTRAINT FK_8F724DD2CCFA12B8 FOREIGN KEY (profile_id) REFERENCES translation_profile (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_8F724DD2CCFA12B8 ON shops_referrer (profile_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE shops_referrer DROP FOREIGN KEY FK_8F724DD2CCFA12B8');
        $this->addSql('DROP INDEX IDX_8F724DD2CCFA12B8 ON shops_referrer');
        $this->addSql('ALTER TABLE shops_referrer DROP profile_id, CHANGE referrer_id referrer_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:shops.referrer_id)\', CHANGE discount_id discount_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.discount_id)\'');
        $this->addSql('ALTER TABLE shops_referrer_template CHANGE referrer_id referrer_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:shops.referrer_id)\', CHANGE template_id template_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.template_id)\'');
    }
}
