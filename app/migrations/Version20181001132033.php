<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181001132033 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_invoice_item ADD item_type VARCHAR(20) NOT NULL DEFAULT \'document\' COMMENT \'(DC2Type:orders.order_item_type)\'');
        $this->addSql('ALTER TABLE orders_invoice_item CHANGE item_type item_type VARCHAR(20) NOT NULL COMMENT \'(DC2Type:orders.order_item_type)\'');
    }

    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_invoice_item DROP item_type, CHANGE item_id item_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.invoice_item_id)\', CHANGE invoice_id invoice_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.invoice_id)\', CHANGE order_id order_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.order_id)\', CHANGE item_price item_price INT NOT NULL COMMENT \'(DC2Type:core.financial.money)\'');
    }
}
