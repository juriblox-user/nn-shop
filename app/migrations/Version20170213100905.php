<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170213100905 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE orders_customer SET address_country_code = \'NL\'');
        $this->addSql('ALTER TABLE orders_customer ADD customer_type VARCHAR(255) NOT NULL COMMENT \'(DC2Type:orders.customer_type)\', CHANGE password_salt password_salt VARCHAR(6) DEFAULT NULL COMMENT \'(DC2Type:core.security.short_token)\', CHANGE address_country_code address_country_code VARCHAR(2) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_customer DROP customer_type, CHANGE password_salt password_salt VARCHAR(6) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:core.security.short_token)\', CHANGE address_country_code address_country_code VARCHAR(3) DEFAULT NULL');
        $this->addSql('UPDATE orders_customer SET address_country_code = \'NLD\'');
    }
}
