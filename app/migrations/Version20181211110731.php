<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181211110731 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_customer ADD locale_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:locales.locale_id)\'');
        $this->addSql('ALTER TABLE orders_customer ADD CONSTRAINT FK_461F53CFE559DFD1 FOREIGN KEY (locale_id) REFERENCES translation_locale (locale_id)');

        $query = $this->connection->prepare('SELECT locale_id FROM translation_locale WHERE locale_slug = \'nl\' LIMIT 1');
        $query->execute();

        $row = $query->fetch();

        $this->addSql('UPDATE orders_customer SET locale_id = :locale_id', [
            'locale_id' => $row['locale_id'],
        ]);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_customer DROP FOREIGN KEY FK_461F53CFE559DFD1');
        $this->addSql('ALTER TABLE orders_customer DROP locale_id');
    }
}
