<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171222163231 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('UPDATE pages_page SET profile_id=(SELECT profile_id FROM translation_profile LIMIT 1)');
        $this->addSql('UPDATE templates_partner SET profile_id=(SELECT profile_id FROM translation_profile LIMIT 1)');
        $this->addSql('UPDATE shops_shop SET profile_id=(SELECT profile_id FROM translation_profile LIMIT 1)');
        $this->addSql('UPDATE templates_company_type SET profile_id=(SELECT profile_id FROM translation_profile LIMIT 1)');
        $this->addSql('UPDATE blog_category SET profile_id=(SELECT profile_id FROM translation_profile LIMIT 1)');
        $this->addSql('UPDATE shops_referrer SET profile_id=(SELECT profile_id FROM translation_profile LIMIT 1)');
        $this->addSql('UPDATE orders_invoice SET profile_id=(SELECT profile_id FROM translation_profile LIMIT 1)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('UPDATE pages_page SET profile_id=NULL');
        $this->addSql('UPDATE templates_partner SET profile_id=NULL');
        $this->addSql('UPDATE shops_shop SET profile_id=NULL');
        $this->addSql('UPDATE templates_company_type SET profile_id=NULL');
        $this->addSql('UPDATE blog_category SET profile_id=NULL');
        $this->addSql('UPDATE shops_referrer SET profile_id=NULL');
        $this->addSql('UPDATE orders_invoice SET profile_id=NULL');
    }
}
