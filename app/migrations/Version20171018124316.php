<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171018124316 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE blog_category_translations (translation_id INT AUTO_INCREMENT NOT NULL, object_id INT DEFAULT NULL, translation_locale VARCHAR(8) NOT NULL, translation_field VARCHAR(32) NOT NULL, translation_content LONGTEXT DEFAULT NULL, INDEX IDX_85D2E1FE232D562B (object_id), UNIQUE INDEX lookup_unique_idx (translation_locale, object_id, translation_field), PRIMARY KEY(translation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ext_translations (translation_id INT AUTO_INCREMENT NOT NULL, translation_locale VARCHAR(8) NOT NULL, object_class VARCHAR(255) NOT NULL, translation_field VARCHAR(32) NOT NULL, foreign_key VARCHAR(64) NOT NULL, translation_content LONGTEXT DEFAULT NULL, PRIMARY KEY(translation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE blog_category_translations ADD CONSTRAINT FK_85D2E1FE232D562B FOREIGN KEY (object_id) REFERENCES blog_category (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE blog_category_translations');
        $this->addSql('DROP TABLE ext_translations');
    }
}
