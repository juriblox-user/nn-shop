<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170228100341 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX ix_worth ON orders_customer');
        $this->addSql('DROP INDEX ix_deleted ON orders_customer');
        $this->addSql('ALTER TABLE orders_customer CHANGE deleted_timestamp timestamp_deleted DATETIME DEFAULT NULL');
        $this->addSql('CREATE INDEX ix_worth ON orders_customer (customer_worth, timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_deleted ON orders_customer (timestamp_deleted)');
        $this->addSql('DROP INDEX ix_deleted ON orders_discount');
        $this->addSql('ALTER TABLE orders_discount CHANGE deleted_timestamp timestamp_deleted DATETIME DEFAULT NULL');
        $this->addSql('CREATE INDEX ix_deleted ON orders_discount (timestamp_deleted)');
        $this->addSql('DROP INDEX ix_customer ON orders_subscription');
        $this->addSql('DROP INDEX ix_expiration ON orders_subscription');
        $this->addSql('DROP INDEX ix_order ON orders_subscription');
        $this->addSql('DROP INDEX ix_template ON orders_subscription');
        $this->addSql('DROP INDEX ix_deleted ON orders_subscription');
        $this->addSql('ALTER TABLE orders_subscription CHANGE deleted_timestamp timestamp_deleted DATETIME DEFAULT NULL');
        $this->addSql('CREATE INDEX ix_customer ON orders_subscription (customer_id, timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_expiration ON orders_subscription (timestamp_expires, timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_order ON orders_subscription (order_id, timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_template ON orders_subscription (template_id, timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_deleted ON orders_subscription (timestamp_deleted, timestamp_created)');
        $this->addSql('DROP INDEX ix_deleted ON shops_shop');
        $this->addSql('ALTER TABLE shops_shop CHANGE deleted_timestamp timestamp_deleted DATETIME DEFAULT NULL');
        $this->addSql('CREATE INDEX ix_deleted ON shops_shop (timestamp_deleted)');
        $this->addSql('DROP INDEX ix_deleted ON templates_answer');
        $this->addSql('DROP INDEX ix_document ON templates_answer');
        $this->addSql('DROP INDEX ix_question ON templates_answer');
        $this->addSql('DROP INDEX ix_answer_to ON templates_answer');
        $this->addSql('ALTER TABLE templates_answer CHANGE deleted_timestamp timestamp_deleted DATETIME DEFAULT NULL');
        $this->addSql('CREATE INDEX ix_deleted ON templates_answer (timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_document ON templates_answer (document_id, timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_question ON templates_answer (question_id, timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_answer_to ON templates_answer (document_id, question_id, timestamp_deleted)');
        $this->addSql('DROP INDEX ix_status ON templates_document');
        $this->addSql('DROP INDEX ix_template ON templates_document');
        $this->addSql('DROP INDEX ix_remote ON templates_document');
        $this->addSql('ALTER TABLE templates_document CHANGE deleted_timestamp timestamp_deleted DATETIME DEFAULT NULL');
        $this->addSql('CREATE INDEX ix_status ON templates_document (document_status, timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_template ON templates_document (template_id, timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_remote ON templates_document (remote_id, timestamp_deleted)');
        $this->addSql('DROP INDEX ix_deleted ON templates_partner');
        $this->addSql('DROP INDEX ix_sync ON templates_partner');
        $this->addSql('ALTER TABLE templates_partner CHANGE deleted_timestamp timestamp_deleted DATETIME DEFAULT NULL');
        $this->addSql('CREATE INDEX ix_deleted ON templates_partner (timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_sync ON templates_partner (sync_enabled, synced_timestamp, timestamp_deleted)');
        $this->addSql('DROP INDEX ix_remote ON templates_question');
        $this->addSql('DROP INDEX ix_step ON templates_question');
        $this->addSql('ALTER TABLE templates_question CHANGE deleted_timestamp timestamp_deleted DATETIME DEFAULT NULL');
        $this->addSql('CREATE INDEX ix_remote ON templates_question (remote_id, timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_step ON templates_question (step_id, timestamp_deleted)');
        $this->addSql('DROP INDEX ix_remote ON templates_question_step');
        $this->addSql('DROP INDEX ix_template ON templates_question_step');
        $this->addSql('ALTER TABLE templates_question_step CHANGE deleted_timestamp timestamp_deleted DATETIME DEFAULT NULL');
        $this->addSql('CREATE INDEX ix_remote ON templates_question_step (remote_id, timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_template ON templates_question_step (template_id, timestamp_deleted)');
        $this->addSql('DROP INDEX ix_deleted ON templates_template');
        $this->addSql('DROP INDEX ix_partner ON templates_template');
        $this->addSql('DROP INDEX ix_published ON templates_template');
        $this->addSql('DROP INDEX ix_remote ON templates_template');
        $this->addSql('DROP INDEX ix_spotlight ON templates_template');
        $this->addSql('ALTER TABLE templates_template CHANGE deleted_timestamp timestamp_deleted DATETIME DEFAULT NULL');
        $this->addSql('CREATE INDEX ix_deleted ON templates_template (timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_partner ON templates_template (partner_id, timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_published ON templates_template (template_corrupted, template_published, timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_remote ON templates_template (remote_id, timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_spotlight ON templates_template (template_spotlight, timestamp_deleted)');
        $this->addSql('DROP INDEX ix_deleted ON templates_update');
        $this->addSql('DROP INDEX ix_template ON templates_update');
        $this->addSql('ALTER TABLE templates_update CHANGE deleted_timestamp timestamp_deleted DATETIME DEFAULT NULL');
        $this->addSql('CREATE INDEX ix_deleted ON templates_update (timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_template ON templates_update (template_id, timestamp_deleted)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
