<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170119103814 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_order DROP FOREIGN KEY FK_B4833C399395C3F3');
        $this->addSql('DROP INDEX ix_customer ON orders_order');
        $this->addSql('DROP INDEX ix_status ON orders_order');
        $this->addSql('CREATE INDEX IDX_B4833C399395C3F3 ON orders_order (customer_id)');
        $this->addSql('CREATE INDEX ix_customer ON orders_order (customer_id, timestamp_created)');
        $this->addSql('CREATE INDEX ix_status ON orders_order (order_status, timestamp_created)');
        $this->addSql('ALTER TABLE orders_order ADD CONSTRAINT FK_B4833C399395C3F3 FOREIGN KEY (customer_id) REFERENCES orders_customer (customer_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_order DROP FOREIGN KEY FK_B4833C399395C3F3');
        $this->addSql('DROP INDEX IDX_B4833C399395C3F3 ON orders_order');
        $this->addSql('DROP INDEX ix_customer ON orders_order');
        $this->addSql('DROP INDEX ix_status ON orders_order');
        $this->addSql('CREATE INDEX ix_customer ON orders_order (customer_id)');
        $this->addSql('CREATE INDEX ix_status ON orders_order (order_status)');
        $this->addSql('ALTER TABLE orders_order ADD CONSTRAINT FK_B4833C399395C3F3 FOREIGN KEY (customer_id) REFERENCES orders_customer (customer_id)');
    }
}
