<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181213100943 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_customer CHANGE customer_firstname customer_firstname VARCHAR(100) DEFAULT NULL, CHANGE customer_lastname customer_lastname VARCHAR(100) DEFAULT NULL');
        $this->addSql('UPDATE orders_customer SET email_verified = 1');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_customer CHANGE customer_firstname customer_firstname VARCHAR(100) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE customer_lastname customer_lastname VARCHAR(100) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
