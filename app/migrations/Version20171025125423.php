<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171025125423 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pages_page ADD profile_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE pages_page ADD CONSTRAINT FK_5FC0FCA0CCFA12B8 FOREIGN KEY (profile_id) REFERENCES translation_profile (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_5FC0FCA0CCFA12B8 ON pages_page (profile_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pages_page DROP FOREIGN KEY FK_5FC0FCA0CCFA12B8');
        $this->addSql('DROP INDEX IDX_5FC0FCA0CCFA12B8 ON pages_page');
        $this->addSql('ALTER TABLE pages_page DROP profile_id');
    }
}
