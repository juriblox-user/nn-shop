<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170202115510 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_customer ADD customer_worth INT NOT NULL COMMENT \'(DC2Type:core.financial.money)\'');
        $this->addSql('CREATE INDEX ix_worth ON orders_customer (customer_worth, deleted_timestamp)');
        $this->addSql('ALTER TABLE orders_discount CHANGE discount_amount discount_amount INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\'');
        $this->addSql('ALTER TABLE orders_invoice CHANGE invoice_subtotal invoice_subtotal INT NOT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE invoice_total invoice_total INT NOT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE vat_amount vat_amount INT NOT NULL COMMENT \'(DC2Type:core.financial.money)\'');
        $this->addSql('ALTER TABLE orders_invoice_item CHANGE item_price item_price INT NOT NULL COMMENT \'(DC2Type:core.financial.money)\'');
        $this->addSql('ALTER TABLE orders_payment CHANGE payment_amount payment_amount INT NOT NULL COMMENT \'(DC2Type:core.financial.money)\'');
        $this->addSql('ALTER TABLE templates_template CHANGE price_check price_check INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE price_monthly price_monthly INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE price_unit price_unit INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX ix_worth ON orders_customer');
        $this->addSql('ALTER TABLE orders_customer DROP customer_worth');
        $this->addSql('ALTER TABLE orders_discount CHANGE discount_amount discount_amount VARCHAR(10) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:core.financial.money)\'');
        $this->addSql('ALTER TABLE orders_invoice CHANGE invoice_subtotal invoice_subtotal VARCHAR(10) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:core.financial.money)\', CHANGE invoice_total invoice_total VARCHAR(10) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:core.financial.money)\', CHANGE vat_amount vat_amount VARCHAR(10) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:core.financial.money)\'');
        $this->addSql('ALTER TABLE orders_invoice_item CHANGE item_price item_price VARCHAR(10) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:core.financial.money)\'');
        $this->addSql('ALTER TABLE orders_payment CHANGE payment_amount payment_amount VARCHAR(10) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:core.financial.money)\'');
        $this->addSql('ALTER TABLE templates_template CHANGE price_check price_check VARCHAR(10) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:core.financial.money)\', CHANGE price_monthly price_monthly VARCHAR(10) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:core.financial.money)\', CHANGE price_unit price_unit VARCHAR(10) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:core.financial.money)\'');
    }
}
