<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171025133532 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE templates_company_type ADD profile_id INT DEFAULT NULL, CHANGE type_id type_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.company_type_id)\'');
        $this->addSql('ALTER TABLE templates_company_type ADD CONSTRAINT FK_6932B7BCCCFA12B8 FOREIGN KEY (profile_id) REFERENCES translation_profile (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_6932B7BCCCFA12B8 ON templates_company_type (profile_id)');
        $this->addSql('ALTER TABLE templates_partner CHANGE partner_id partner_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.partner_id)\', CHANGE remote_id remote_id SMALLINT NOT NULL, CHANGE synced_timestamp synced_timestamp DATETIME DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE templates_partner CHANGE partner_id partner_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.partner_id)\', CHANGE remote_id remote_id SMALLINT NOT NULL, CHANGE synced_timestamp synced_timestamp DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE templates_partner_translations CHANGE object_id object_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.partner_id)\'');
    }
}
