<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170310081303 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_discount CHANGE discount_type discount_type VARCHAR(20) NOT NULL COMMENT \'(DC2Type:orders.discount_type)\'');

        $this->addSql('UPDATE orders_discount SET discount_type = \'fixed\' WHERE discount_type = 1');
        $this->addSql('UPDATE orders_discount SET discount_type = \'percentage\' WHERE discount_type = 2');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE orders_discount SET discount_type = 1 WHERE discount_type = \'fixed\'');
        $this->addSql('UPDATE orders_discount SET discount_type = 2 WHERE discount_type = \'percentage\'');

        $this->addSql('ALTER TABLE orders_discount CHANGE discount_type discount_type SMALLINT NOT NULL');
    }
}
