<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170206125220 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM orders_session_browser');
        $this->addSql('DELETE FROM orders_session');

        $this->addSql('ALTER TABLE orders_session_browser ADD session_token VARCHAR(40) NOT NULL COMMENT \'(DC2Type:orders.browser_session_token)\'');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6CBD243B844A19ED ON orders_session_browser (session_token)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_6CBD243B844A19ED ON orders_session_browser');
        $this->addSql('ALTER TABLE orders_session_browser DROP session_token');
    }
}
