<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171024132829 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE translation_profile ADD main_locale_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE translation_profile ADD CONSTRAINT FK_205B53FDC5EF7873 FOREIGN KEY (main_locale_id) REFERENCES translation_locale (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_205B53FDC5EF7873 ON translation_profile (main_locale_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE translation_profile DROP FOREIGN KEY FK_205B53FDC5EF7873');
        $this->addSql('DROP INDEX IDX_205B53FDC5EF7873 ON translation_profile');
        $this->addSql('ALTER TABLE translation_profile DROP main_locale_id');
    }
}
