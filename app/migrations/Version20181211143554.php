<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181211143554 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_order DROP FOREIGN KEY FK_B4833C39613FECDF');
        $this->addSql('ALTER TABLE orders_session_browser DROP FOREIGN KEY FK_6CBD243B613FECDF');
        $this->addSql('DROP TABLE orders_session');
        $this->addSql('DROP TABLE orders_session_browser');
        $this->addSql('DROP INDEX ix_session ON orders_order');
        $this->addSql('ALTER TABLE orders_order DROP session_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
