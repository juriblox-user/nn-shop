<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170308084152 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE templates_question CHANGE step_id step_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:templates.question_step_id)\'');
        $this->addSql('ALTER TABLE orders_order RENAME INDEX idx_b4833c3973b21e9c TO ix_step');
        $this->addSql('DROP INDEX ix_remote ON templates_question_step');
        $this->addSql('DROP INDEX ix_template ON templates_question_step');
        $this->addSql('ALTER TABLE templates_question_step DROP timestamp_deleted');
        $this->addSql('CREATE INDEX ix_template ON templates_question_step (template_id, step_position)');
        $this->addSql('ALTER TABLE templates_template CHANGE template_id template_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.template_id)\', CHANGE partner_id partner_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.partner_id)\', CHANGE template_key template_key VARCHAR(6) NOT NULL COMMENT \'(DC2Type:templates.template_key)\', CHANGE price_check price_check INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE price_monthly price_monthly INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE price_unit price_unit INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE remote_id remote_id INT NOT NULL COMMENT \'(DC2Type:juriblox.template_id)\', CHANGE synced_timestamp synced_timestamp DATETIME DEFAULT NULL, CHANGE template_version template_version INT NOT NULL COMMENT \'(DC2Type:templates.template_version)\'');
        $this->addSql('ALTER TABLE templates_update CHANGE update_id update_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.update_id)\', CHANGE template_id template_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.template_id)\', CHANGE timestamp_published timestamp_published DATETIME DEFAULT NULL, CHANGE timestamp_received timestamp_received DATETIME NOT NULL, CHANGE update_version update_version INT NOT NULL COMMENT \'(DC2Type:templates.template_version)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE templates_question CHANGE step_id step_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.question_step_id)\'');
        $this->addSql('DROP INDEX ix_template ON templates_question_step');
        $this->addSql('ALTER TABLE templates_question_step ADD timestamp_deleted DATETIME DEFAULT NULL');
        $this->addSql('CREATE INDEX ix_remote ON templates_question_step (remote_id, timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_template ON templates_question_step (template_id, timestamp_deleted)');
    }
}
