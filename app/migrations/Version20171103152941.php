<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171103152941 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_invoice ADD profile_id INT DEFAULT NULL, CHANGE invoice_id invoice_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.invoice_id)\', CHANGE customer_id customer_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.customer_id)\', CHANGE invoice_status invoice_status VARCHAR(255) NOT NULL COMMENT \'(DC2Type:orders.invoice_status)\', CHANGE invoice_subtotal invoice_subtotal INT NOT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE timestamp_credited timestamp_credited DATETIME DEFAULT NULL, CHANGE timestamp_due timestamp_due DATETIME NOT NULL, CHANGE timestamp_issued timestamp_issued DATETIME NOT NULL, CHANGE timestamp_paid timestamp_paid DATETIME DEFAULT NULL, CHANGE timestamp_revoked timestamp_revoked DATETIME DEFAULT NULL, CHANGE invoice_total invoice_total INT NOT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE vat_amount vat_amount INT NOT NULL COMMENT \'(DC2Type:core.financial.money)\'');
        $this->addSql('ALTER TABLE orders_invoice ADD CONSTRAINT FK_6110653FCCFA12B8 FOREIGN KEY (profile_id) REFERENCES translation_profile (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_6110653FCCFA12B8 ON orders_invoice (profile_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_invoice DROP FOREIGN KEY FK_6110653FCCFA12B8');
        $this->addSql('DROP INDEX IDX_6110653FCCFA12B8 ON orders_invoice');
        $this->addSql('ALTER TABLE orders_invoice DROP profile_id, CHANGE invoice_id invoice_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.invoice_id)\', CHANGE customer_id customer_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.customer_id)\', CHANGE invoice_status invoice_status VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.invoice_status)\', CHANGE invoice_subtotal invoice_subtotal INT NOT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE timestamp_credited timestamp_credited DATETIME DEFAULT NULL, CHANGE timestamp_due timestamp_due DATETIME NOT NULL, CHANGE timestamp_issued timestamp_issued DATETIME NOT NULL, CHANGE timestamp_paid timestamp_paid DATETIME DEFAULT NULL, CHANGE timestamp_revoked timestamp_revoked DATETIME DEFAULT NULL, CHANGE invoice_total invoice_total INT NOT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE vat_amount vat_amount INT NOT NULL COMMENT \'(DC2Type:core.financial.money)\'');
    }
}
