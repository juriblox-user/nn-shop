<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170309132429 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE orders_order_discount (order_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.order_id)\', discount_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.discount_id)\', INDEX IDX_A77560D98D9F6D38 (order_id), INDEX IDX_A77560D94C7C611F (discount_id), PRIMARY KEY(order_id, discount_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE orders_order_discount ADD CONSTRAINT FK_A77560D98D9F6D38 FOREIGN KEY (order_id) REFERENCES orders_order (order_id)');
        $this->addSql('ALTER TABLE orders_order_discount ADD CONSTRAINT FK_A77560D94C7C611F FOREIGN KEY (discount_id) REFERENCES orders_discount (discount_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE orders_order_discount');
    }
}
