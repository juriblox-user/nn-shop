<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170111132345 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE system_task (task_id CHAR(36) NOT NULL COMMENT \'(DC2Type:system.task_id)\', exception_message VARCHAR(255) DEFAULT NULL, exception_timestamp DATETIME DEFAULT NULL, exception_trace LONGTEXT DEFAULT NULL, lock_expiration DATETIME DEFAULT NULL, run_requested TINYINT(1) NOT NULL, run_successful TINYINT(1) DEFAULT NULL, run_timestamp DATETIME DEFAULT NULL, task_schedule VARCHAR(50) NOT NULL, scheduled_timestamp DATETIME NOT NULL, task_title VARCHAR(100) NOT NULL, task_worker VARCHAR(100) NOT NULL, UNIQUE INDEX uq_worker (task_worker), PRIMARY KEY(task_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders_advice (advice_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.advice_id)\', customer_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:orders.customer_id)\', INDEX IDX_C512375C9395C3F3 (customer_id), PRIMARY KEY(advice_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders_advice_template (link_id INT AUTO_INCREMENT NOT NULL, advice_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.advice_id)\', template_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.template_id)\', INDEX IDX_13AAB00512998205 (advice_id), INDEX IDX_13AAB0055DA0FB8 (template_id), PRIMARY KEY(link_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders_session_browser (browser_id VARCHAR(128) NOT NULL COMMENT \'(DC2Type:orders.browser_session_id)\', session_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:orders.session_id)\', timestamp_created DATETIME NOT NULL, timestamp_seen DATETIME NOT NULL, INDEX ix_session (session_id), PRIMARY KEY(browser_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders_customer (customer_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.customer_id)\', company_name VARCHAR(100) DEFAULT NULL, email_current VARCHAR(255) NOT NULL COMMENT \'(DC2Type:core.web.email_address)\', email_updated VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:core.web.email_address)\', email_updated_token VARCHAR(6) DEFAULT NULL COMMENT \'(DC2Type:core.security.short_token)\', customer_firstname VARCHAR(100) NOT NULL, customer_lastname VARCHAR(100) NOT NULL, password_hash VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:core.security.hashed_password)\', password_salt VARCHAR(6) NOT NULL COMMENT \'(DC2Type:core.security.short_token)\', remote_reference VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:juriblox.customer_reference)\', customer_secret VARCHAR(32) NOT NULL COMMENT \'(DC2Type:core.security.long_token)\', deleted_timestamp DATETIME DEFAULT NULL, address_city VARCHAR(100) DEFAULT NULL, address_street VARCHAR(100) DEFAULT NULL, address_zipcode VARCHAR(10) DEFAULT NULL, address_country_code VARCHAR(3) DEFAULT NULL, address_number_addition VARCHAR(10) DEFAULT NULL, address_number_value VARCHAR(10) DEFAULT NULL, UNIQUE INDEX UNIQ_461F53CF61335CC4 (email_current), UNIQUE INDEX UNIQ_461F53CFD11AD6D7 (remote_reference), INDEX ix_deleted (deleted_timestamp), PRIMARY KEY(customer_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders_discount (discount_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.discount_id)\', discount_amount VARCHAR(10) DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', discount_code VARCHAR(20) NOT NULL, discount_percentage SMALLINT DEFAULT NULL, discount_title VARCHAR(255) DEFAULT NULL, discount_type SMALLINT NOT NULL, deleted_timestamp DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_26C669C8E9973522 (discount_code), INDEX ix_deleted (deleted_timestamp), PRIMARY KEY(discount_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders_invoice (invoice_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.invoice_id)\', customer_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:orders.customer_id)\', invoice_status VARCHAR(255) NOT NULL COMMENT \'(DC2Type:orders.invoice_status)\', invoice_subtotal VARCHAR(10) NOT NULL COMMENT \'(DC2Type:core.financial.money)\', timestamp_credited DATETIME DEFAULT NULL, timestamp_due DATETIME NOT NULL, timestamp_issued DATETIME NOT NULL, timestamp_paid DATETIME DEFAULT NULL, timestamp_revoked DATETIME DEFAULT NULL, invoice_total VARCHAR(10) NOT NULL COMMENT \'(DC2Type:core.financial.money)\', vat_amount VARCHAR(10) NOT NULL COMMENT \'(DC2Type:core.financial.money)\', vat_percentage SMALLINT NOT NULL, code_number INT NOT NULL, code_year SMALLINT NOT NULL, INDEX IDX_6110653F9395C3F3 (customer_id), INDEX ix_code_year (code_year), INDEX ix_code_number (code_number), UNIQUE INDEX uq_code (code_year, code_number), PRIMARY KEY(invoice_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders_invoice_discount (link_id INT AUTO_INCREMENT NOT NULL, discount_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.discount_id)\', invoice_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.invoice_id)\', discount_timestamp DATETIME NOT NULL, INDEX IDX_2D510834C7C611F (discount_id), INDEX IDX_2D510832989F1FD (invoice_id), PRIMARY KEY(link_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders_invoice_item (item_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.invoice_item_id)\', invoice_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:orders.invoice_id)\', order_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:orders.order_id)\', item_position INT NOT NULL, item_price VARCHAR(10) NOT NULL COMMENT \'(DC2Type:core.financial.money)\', item_quantity INT NOT NULL, item_title VARCHAR(255) NOT NULL, INDEX IDX_D7784F52989F1FD (invoice_id), INDEX IDX_D7784F58D9F6D38 (order_id), PRIMARY KEY(item_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders_order (order_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.order_id)\', customer_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:orders.customer_id)\', subscription_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:orders.subscription_id)\', previous_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:orders.order_id)\', session_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:orders.session_id)\', step_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:templates.question_step_id)\', order_check TINYINT(1) NOT NULL, order_custom TINYINT(1) NOT NULL, order_locked TINYINT(1) NOT NULL, order_number INT NOT NULL COMMENT \'(DC2Type:orders.order_number)\', payment_method VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:orders.payment_method)\', order_status VARCHAR(20) NOT NULL COMMENT \'(DC2Type:orders.order_status)\', timestamp_completed DATETIME DEFAULT NULL, timestamp_created DATETIME NOT NULL, timestamp_delivered DATETIME DEFAULT NULL, timestamp_interrupted DATETIME DEFAULT NULL, timestamp_refreshed DATETIME DEFAULT NULL, order_type VARCHAR(20) DEFAULT NULL COMMENT \'(DC2Type:orders.order_type)\', INDEX IDX_B4833C399A1887DC (subscription_id), UNIQUE INDEX UNIQ_B4833C392DE62210 (previous_id), INDEX IDX_B4833C3973B21E9C (step_id), INDEX ix_type (order_type, order_status), INDEX ix_customer (customer_id), INDEX ix_session (session_id), INDEX ix_status (order_status), UNIQUE INDEX uq_number (order_number), PRIMARY KEY(order_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders_order_status (change_id INT AUTO_INCREMENT NOT NULL, order_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:orders.order_id)\', change_status VARCHAR(20) NOT NULL COMMENT \'(DC2Type:orders.order_status)\', change_timestamp DATETIME NOT NULL, INDEX IDX_A826B6478D9F6D38 (order_id), PRIMARY KEY(change_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders_payment (payment_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.payment_id)\', invoice_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:orders.invoice_id)\', refund_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:orders.refund_id)\', payment_amount VARCHAR(10) NOT NULL COMMENT \'(DC2Type:core.financial.money)\', payment_method VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:orders.payment_method)\', payment_status VARCHAR(255) NOT NULL COMMENT \'(DC2Type:orders.payment_status)\', timestamp_cancelled DATETIME DEFAULT NULL, timestamp_expired DATETIME DEFAULT NULL, timestamp_received DATETIME DEFAULT NULL, timestamp_requested DATETIME NOT NULL, transaction_id VARCHAR(255) NOT NULL COMMENT \'(DC2Type:mollie.transaction_id)\', INDEX IDX_9C5DF6762989F1FD (invoice_id), UNIQUE INDEX UNIQ_9C5DF676189801D5 (refund_id), INDEX ix_invoice (invoice_id, payment_status), INDEX ix_status (payment_status), PRIMARY KEY(payment_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders_refund (refund_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.refund_id)\', processed_timestamp DATETIME DEFAULT NULL, refund_reason VARCHAR(255) DEFAULT NULL, requested_timestamp DATETIME NOT NULL, PRIMARY KEY(refund_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders_session (session_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.session_id)\', session_data LONGTEXT DEFAULT NULL, session_lifetime INT NOT NULL, timestamp_created DATETIME NOT NULL, timestamp_expiration DATETIME NOT NULL, timestamp_logout DATETIME NOT NULL, INDEX ix_expiration (timestamp_expiration), PRIMARY KEY(session_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders_subscription (subscription_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.subscription_id)\', customer_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.customer_id)\', order_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:orders.order_id)\', template_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.template_id)\', timestamp_cancelled DATETIME DEFAULT NULL, deleted_timestamp DATETIME DEFAULT NULL, INDEX IDX_B36FA75D9395C3F3 (customer_id), UNIQUE INDEX UNIQ_B36FA75D8D9F6D38 (order_id), INDEX IDX_B36FA75D5DA0FB8 (template_id), INDEX ix_customer (customer_id, deleted_timestamp), INDEX ix_deleted (deleted_timestamp), PRIMARY KEY(subscription_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shops_shop (shop_id CHAR(36) NOT NULL COMMENT \'(DC2Type:shops.shop_id)\', shop_hostname VARCHAR(100) NOT NULL, shop_title VARCHAR(100) NOT NULL, deleted_timestamp DATETIME DEFAULT NULL, INDEX ix_deleted (deleted_timestamp), PRIMARY KEY(shop_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shops_testimonial (testimonial_id CHAR(36) NOT NULL COMMENT \'(DC2Type:shops.testimonial_id)\', testimonial_company VARCHAR(255) NOT NULL, testimonial_name VARCHAR(255) NOT NULL, testimonial_quote LONGTEXT NOT NULL, PRIMARY KEY(testimonial_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE templates_answer (answer_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.answer_id)\', document_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:templates.document_id)\', question_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:templates.question_id)\', answer_value LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:object)\', deleted_timestamp DATETIME DEFAULT NULL, INDEX IDX_DE8FA186C33F7837 (document_id), INDEX IDX_DE8FA1861E27F6BF (question_id), INDEX ix_deleted (deleted_timestamp), INDEX ix_document (document_id, deleted_timestamp), INDEX ix_question (question_id, deleted_timestamp), INDEX ix_answer_to (document_id, question_id, deleted_timestamp), PRIMARY KEY(answer_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE templates_answer_option (answer_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.answer_id)\', option_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.question_option_id)\', INDEX IDX_6F0E1359AA334807 (answer_id), INDEX IDX_6F0E1359A7C41D6F (option_id), PRIMARY KEY(answer_id, option_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE templates_category (category_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.category_id)\', shop_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:shops.shop_id)\', category_slug VARCHAR(100) NOT NULL, category_title VARCHAR(100) NOT NULL, INDEX IDX_B4BEC9494D16C4DD (shop_id), UNIQUE INDEX uq_slug (shop_id, category_slug), PRIMARY KEY(category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE templates_category_template (link_id INT AUTO_INCREMENT NOT NULL, category_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.category_id)\', template_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.template_id)\', category_primary TINYINT(1) NOT NULL, INDEX IDX_5F69D63D12469DE2 (category_id), INDEX IDX_5F69D63D5DA0FB8 (template_id), INDEX ix_primary (template_id, category_primary), PRIMARY KEY(link_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE templates_company_activity (activity_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.company_activity_id)\', type_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:templates.company_type_id)\', activity_slug VARCHAR(100) NOT NULL, activity_title VARCHAR(100) NOT NULL, UNIQUE INDEX UNIQ_ADA3627B9B5940F0 (activity_slug), INDEX IDX_ADA3627BC54C8C93 (type_id), PRIMARY KEY(activity_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE templates_company_activity_template (link_id INT AUTO_INCREMENT NOT NULL, activity_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.company_activity_id)\', template_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.template_id)\', INDEX IDX_8A596BEE81C06096 (activity_id), INDEX IDX_8A596BEE5DA0FB8 (template_id), PRIMARY KEY(link_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE templates_company_type (type_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.company_type_id)\', type_slug VARCHAR(100) NOT NULL, type_title VARCHAR(100) NOT NULL, UNIQUE INDEX UNIQ_6932B7BC4F925E6A (type_slug), PRIMARY KEY(type_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE templates_document (document_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.document_id)\', order_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:orders.order_id)\', template_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.template_id)\', remote_id INT DEFAULT NULL COMMENT \'(DC2Type:juriblox.document_id)\', document_status VARCHAR(255) NOT NULL COMMENT \'(DC2Type:templates.document_status)\', timestamp_cancelled DATETIME DEFAULT NULL, timestamp_error DATETIME DEFAULT NULL, timestamp_generated DATETIME DEFAULT NULL, timestamp_requested DATETIME DEFAULT NULL, deleted_timestamp DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_6A9B5AFE2A3E9C94 (remote_id), UNIQUE INDEX UNIQ_6A9B5AFE8D9F6D38 (order_id), INDEX IDX_6A9B5AFE5DA0FB8 (template_id), INDEX ix_template (template_id, deleted_timestamp), INDEX ix_remote (remote_id, deleted_timestamp), PRIMARY KEY(document_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE templates_document_request (request_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.document_request_id)\', document_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:templates.document_id)\', remote_id INT DEFAULT NULL COMMENT \'(DC2Type:juriblox.document_request_id)\', timestamp_completed DATETIME DEFAULT NULL, timestamp_failed DATETIME DEFAULT NULL, timestamp_requested DATETIME NOT NULL, UNIQUE INDEX UNIQ_86B8570F2A3E9C94 (remote_id), INDEX ix_document (document_id), PRIMARY KEY(request_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE templates_partner (partner_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.partner_id)\', partner_excerpt LONGTEXT DEFAULT NULL, partner_profile LONGTEXT DEFAULT NULL, remote_id SMALLINT NOT NULL, partner_slug VARCHAR(100) NOT NULL, sync_client_id VARCHAR(255) DEFAULT NULL, sync_client_key VARCHAR(255) DEFAULT NULL, sync_enabled TINYINT(1) NOT NULL, synced_timestamp DATETIME DEFAULT NULL, partner_title VARCHAR(100) NOT NULL, deleted_timestamp DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_7EF09EAF2A3E9C94 (remote_id), UNIQUE INDEX UNIQ_7EF09EAFC8A9A87A (partner_slug), INDEX ix_deleted (deleted_timestamp), INDEX ix_sync (sync_enabled, synced_timestamp, deleted_timestamp), PRIMARY KEY(partner_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE templates_question (question_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.question_id)\', step_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:templates.question_step_id)\', question_help LONGTEXT DEFAULT NULL, question_position INT NOT NULL, remote_id INT NOT NULL COMMENT \'(DC2Type:juriblox.question_id)\', question_required TINYINT(1) NOT NULL, question_title VARCHAR(200) NOT NULL, question_type VARCHAR(255) NOT NULL COMMENT \'(DC2Type:templates.question_type)\', deleted_timestamp DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_40599C62A3E9C94 (remote_id), INDEX IDX_40599C673B21E9C (step_id), INDEX ix_remote (remote_id, deleted_timestamp), INDEX ix_step (step_id, deleted_timestamp), PRIMARY KEY(question_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE templates_question_condition (condition_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.question_condition_id)\', option_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:templates.question_option_id)\', parent_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:templates.question_id)\', question_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:templates.question_id)\', INDEX IDX_5F7289AAA7C41D6F (option_id), INDEX IDX_5F7289AA727ACA70 (parent_id), INDEX ix_question (question_id), PRIMARY KEY(condition_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE templates_question_option (option_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.question_option_id)\', question_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:templates.question_id)\', option_position INT NOT NULL, remote_id INT NOT NULL COMMENT \'(DC2Type:juriblox.question_answer_id)\', option_title VARCHAR(255) DEFAULT NULL, option_value LONGTEXT NOT NULL COMMENT \'(DC2Type:object)\', UNIQUE INDEX UNIQ_7193122E2A3E9C94 (remote_id), INDEX ix_question (question_id), INDEX ix_remote (remote_id), PRIMARY KEY(option_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE templates_question_step (step_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.question_step_id)\', template_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:templates.template_id)\', step_description LONGTEXT DEFAULT NULL, step_position INT NOT NULL, remote_id INT NOT NULL COMMENT \'(DC2Type:juriblox.step_id)\', step_title VARCHAR(255) NOT NULL, deleted_timestamp DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_DA263A9D2A3E9C94 (remote_id), INDEX IDX_DA263A9D5DA0FB8 (template_id), INDEX ix_remote (remote_id, deleted_timestamp), INDEX ix_template (template_id, deleted_timestamp), PRIMARY KEY(step_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE templates_template (template_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.template_id)\', partner_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:templates.partner_id)\', template_corrupted TINYINT(1) NOT NULL, template_description LONGTEXT NOT NULL, template_key VARCHAR(6) NOT NULL COMMENT \'(DC2Type:templates.template_key)\', order_count INT NOT NULL, order_seconds INT NOT NULL, price_check VARCHAR(10) DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', price_monthly VARCHAR(10) DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', price_unit VARCHAR(10) DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', template_published TINYINT(1) NOT NULL, remote_id INT NOT NULL COMMENT \'(DC2Type:juriblox.template_id)\', remote_title VARCHAR(100) NOT NULL, template_slug VARCHAR(100) NOT NULL, synced_timestamp DATETIME DEFAULT NULL, template_title VARCHAR(100) NOT NULL, template_version INT NOT NULL COMMENT \'(DC2Type:templates.template_version)\', deleted_timestamp DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_2592CF0B2A3E9C94 (remote_id), UNIQUE INDEX UNIQ_2592CF0B4D53B10F (template_slug), INDEX IDX_2592CF0B9393F8FE (partner_id), INDEX ix_deleted (deleted_timestamp), INDEX ix_partner (partner_id, deleted_timestamp), INDEX ix_published (template_corrupted, template_published, deleted_timestamp), INDEX ix_remote (remote_id, deleted_timestamp), UNIQUE INDEX uq_key (template_key), PRIMARY KEY(template_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE templates_update (update_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.update_id)\', template_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.template_id)\', timestamp_published DATETIME DEFAULT NULL, timestamp_received DATETIME NOT NULL, update_version INT NOT NULL COMMENT \'(DC2Type:templates.template_version)\', deleted_timestamp DATETIME DEFAULT NULL, INDEX IDX_9C77DEDB5DA0FB8 (template_id), INDEX ix_deleted (deleted_timestamp), INDEX ix_template (template_id, deleted_timestamp), PRIMARY KEY(update_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE orders_advice ADD CONSTRAINT FK_C512375C9395C3F3 FOREIGN KEY (customer_id) REFERENCES orders_customer (customer_id)');
        $this->addSql('ALTER TABLE orders_advice_template ADD CONSTRAINT FK_13AAB00512998205 FOREIGN KEY (advice_id) REFERENCES orders_advice (advice_id)');
        $this->addSql('ALTER TABLE orders_advice_template ADD CONSTRAINT FK_13AAB0055DA0FB8 FOREIGN KEY (template_id) REFERENCES templates_template (template_id)');
        $this->addSql('ALTER TABLE orders_session_browser ADD CONSTRAINT FK_6CBD243B613FECDF FOREIGN KEY (session_id) REFERENCES orders_session (session_id)');
        $this->addSql('ALTER TABLE orders_invoice ADD CONSTRAINT FK_6110653F9395C3F3 FOREIGN KEY (customer_id) REFERENCES orders_customer (customer_id)');
        $this->addSql('ALTER TABLE orders_invoice_discount ADD CONSTRAINT FK_2D510834C7C611F FOREIGN KEY (discount_id) REFERENCES orders_discount (discount_id)');
        $this->addSql('ALTER TABLE orders_invoice_discount ADD CONSTRAINT FK_2D510832989F1FD FOREIGN KEY (invoice_id) REFERENCES orders_invoice (invoice_id)');
        $this->addSql('ALTER TABLE orders_invoice_item ADD CONSTRAINT FK_D7784F52989F1FD FOREIGN KEY (invoice_id) REFERENCES orders_invoice (invoice_id)');
        $this->addSql('ALTER TABLE orders_invoice_item ADD CONSTRAINT FK_D7784F58D9F6D38 FOREIGN KEY (order_id) REFERENCES orders_order (order_id)');
        $this->addSql('ALTER TABLE orders_order ADD CONSTRAINT FK_B4833C399395C3F3 FOREIGN KEY (customer_id) REFERENCES orders_customer (customer_id)');
        $this->addSql('ALTER TABLE orders_order ADD CONSTRAINT FK_B4833C399A1887DC FOREIGN KEY (subscription_id) REFERENCES orders_subscription (subscription_id)');
        $this->addSql('ALTER TABLE orders_order ADD CONSTRAINT FK_B4833C392DE62210 FOREIGN KEY (previous_id) REFERENCES orders_order (order_id)');
        $this->addSql('ALTER TABLE orders_order ADD CONSTRAINT FK_B4833C39613FECDF FOREIGN KEY (session_id) REFERENCES orders_session (session_id)');
        $this->addSql('ALTER TABLE orders_order ADD CONSTRAINT FK_B4833C3973B21E9C FOREIGN KEY (step_id) REFERENCES templates_question_step (step_id)');
        $this->addSql('ALTER TABLE orders_order_status ADD CONSTRAINT FK_A826B6478D9F6D38 FOREIGN KEY (order_id) REFERENCES orders_order (order_id)');
        $this->addSql('ALTER TABLE orders_payment ADD CONSTRAINT FK_9C5DF6762989F1FD FOREIGN KEY (invoice_id) REFERENCES orders_invoice (invoice_id)');
        $this->addSql('ALTER TABLE orders_payment ADD CONSTRAINT FK_9C5DF676189801D5 FOREIGN KEY (refund_id) REFERENCES orders_refund (refund_id)');
        $this->addSql('ALTER TABLE orders_subscription ADD CONSTRAINT FK_B36FA75D9395C3F3 FOREIGN KEY (customer_id) REFERENCES orders_customer (customer_id)');
        $this->addSql('ALTER TABLE orders_subscription ADD CONSTRAINT FK_B36FA75D8D9F6D38 FOREIGN KEY (order_id) REFERENCES orders_order (order_id)');
        $this->addSql('ALTER TABLE orders_subscription ADD CONSTRAINT FK_B36FA75D5DA0FB8 FOREIGN KEY (template_id) REFERENCES templates_template (template_id)');
        $this->addSql('ALTER TABLE templates_answer ADD CONSTRAINT FK_DE8FA186C33F7837 FOREIGN KEY (document_id) REFERENCES templates_document (document_id)');
        $this->addSql('ALTER TABLE templates_answer ADD CONSTRAINT FK_DE8FA1861E27F6BF FOREIGN KEY (question_id) REFERENCES templates_question (question_id)');
        $this->addSql('ALTER TABLE templates_answer_option ADD CONSTRAINT FK_6F0E1359AA334807 FOREIGN KEY (answer_id) REFERENCES templates_answer (answer_id)');
        $this->addSql('ALTER TABLE templates_answer_option ADD CONSTRAINT FK_6F0E1359A7C41D6F FOREIGN KEY (option_id) REFERENCES templates_question_option (option_id)');
        $this->addSql('ALTER TABLE templates_category ADD CONSTRAINT FK_B4BEC9494D16C4DD FOREIGN KEY (shop_id) REFERENCES shops_shop (shop_id)');
        $this->addSql('ALTER TABLE templates_category_template ADD CONSTRAINT FK_5F69D63D12469DE2 FOREIGN KEY (category_id) REFERENCES templates_category (category_id)');
        $this->addSql('ALTER TABLE templates_category_template ADD CONSTRAINT FK_5F69D63D5DA0FB8 FOREIGN KEY (template_id) REFERENCES templates_template (template_id)');
        $this->addSql('ALTER TABLE templates_company_activity ADD CONSTRAINT FK_ADA3627BC54C8C93 FOREIGN KEY (type_id) REFERENCES templates_company_type (type_id)');
        $this->addSql('ALTER TABLE templates_company_activity_template ADD CONSTRAINT FK_8A596BEE81C06096 FOREIGN KEY (activity_id) REFERENCES templates_company_activity (activity_id)');
        $this->addSql('ALTER TABLE templates_company_activity_template ADD CONSTRAINT FK_8A596BEE5DA0FB8 FOREIGN KEY (template_id) REFERENCES templates_template (template_id)');
        $this->addSql('ALTER TABLE templates_document ADD CONSTRAINT FK_6A9B5AFE8D9F6D38 FOREIGN KEY (order_id) REFERENCES orders_order (order_id)');
        $this->addSql('ALTER TABLE templates_document ADD CONSTRAINT FK_6A9B5AFE5DA0FB8 FOREIGN KEY (template_id) REFERENCES templates_template (template_id)');
        $this->addSql('ALTER TABLE templates_document_request ADD CONSTRAINT FK_86B8570FC33F7837 FOREIGN KEY (document_id) REFERENCES templates_document (document_id)');
        $this->addSql('ALTER TABLE templates_question ADD CONSTRAINT FK_40599C673B21E9C FOREIGN KEY (step_id) REFERENCES templates_question_step (step_id)');
        $this->addSql('ALTER TABLE templates_question_condition ADD CONSTRAINT FK_5F7289AAA7C41D6F FOREIGN KEY (option_id) REFERENCES templates_question_option (option_id)');
        $this->addSql('ALTER TABLE templates_question_condition ADD CONSTRAINT FK_5F7289AA727ACA70 FOREIGN KEY (parent_id) REFERENCES templates_question (question_id)');
        $this->addSql('ALTER TABLE templates_question_condition ADD CONSTRAINT FK_5F7289AA1E27F6BF FOREIGN KEY (question_id) REFERENCES templates_question (question_id)');
        $this->addSql('ALTER TABLE templates_question_option ADD CONSTRAINT FK_7193122E1E27F6BF FOREIGN KEY (question_id) REFERENCES templates_question (question_id)');
        $this->addSql('ALTER TABLE templates_question_step ADD CONSTRAINT FK_DA263A9D5DA0FB8 FOREIGN KEY (template_id) REFERENCES templates_template (template_id)');
        $this->addSql('ALTER TABLE templates_template ADD CONSTRAINT FK_2592CF0B9393F8FE FOREIGN KEY (partner_id) REFERENCES templates_partner (partner_id)');
        $this->addSql('ALTER TABLE templates_update ADD CONSTRAINT FK_9C77DEDB5DA0FB8 FOREIGN KEY (template_id) REFERENCES templates_template (template_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_advice_template DROP FOREIGN KEY FK_13AAB00512998205');
        $this->addSql('ALTER TABLE orders_advice DROP FOREIGN KEY FK_C512375C9395C3F3');
        $this->addSql('ALTER TABLE orders_invoice DROP FOREIGN KEY FK_6110653F9395C3F3');
        $this->addSql('ALTER TABLE orders_order DROP FOREIGN KEY FK_B4833C399395C3F3');
        $this->addSql('ALTER TABLE orders_subscription DROP FOREIGN KEY FK_B36FA75D9395C3F3');
        $this->addSql('ALTER TABLE orders_invoice_discount DROP FOREIGN KEY FK_2D510834C7C611F');
        $this->addSql('ALTER TABLE orders_invoice_discount DROP FOREIGN KEY FK_2D510832989F1FD');
        $this->addSql('ALTER TABLE orders_invoice_item DROP FOREIGN KEY FK_D7784F52989F1FD');
        $this->addSql('ALTER TABLE orders_payment DROP FOREIGN KEY FK_9C5DF6762989F1FD');
        $this->addSql('ALTER TABLE orders_invoice_item DROP FOREIGN KEY FK_D7784F58D9F6D38');
        $this->addSql('ALTER TABLE orders_order DROP FOREIGN KEY FK_B4833C392DE62210');
        $this->addSql('ALTER TABLE orders_order_status DROP FOREIGN KEY FK_A826B6478D9F6D38');
        $this->addSql('ALTER TABLE orders_subscription DROP FOREIGN KEY FK_B36FA75D8D9F6D38');
        $this->addSql('ALTER TABLE templates_document DROP FOREIGN KEY FK_6A9B5AFE8D9F6D38');
        $this->addSql('ALTER TABLE orders_payment DROP FOREIGN KEY FK_9C5DF676189801D5');
        $this->addSql('ALTER TABLE orders_session_browser DROP FOREIGN KEY FK_6CBD243B613FECDF');
        $this->addSql('ALTER TABLE orders_order DROP FOREIGN KEY FK_B4833C39613FECDF');
        $this->addSql('ALTER TABLE orders_order DROP FOREIGN KEY FK_B4833C399A1887DC');
        $this->addSql('ALTER TABLE templates_category DROP FOREIGN KEY FK_B4BEC9494D16C4DD');
        $this->addSql('ALTER TABLE templates_answer_option DROP FOREIGN KEY FK_6F0E1359AA334807');
        $this->addSql('ALTER TABLE templates_category_template DROP FOREIGN KEY FK_5F69D63D12469DE2');
        $this->addSql('ALTER TABLE templates_company_activity_template DROP FOREIGN KEY FK_8A596BEE81C06096');
        $this->addSql('ALTER TABLE templates_company_activity DROP FOREIGN KEY FK_ADA3627BC54C8C93');
        $this->addSql('ALTER TABLE templates_answer DROP FOREIGN KEY FK_DE8FA186C33F7837');
        $this->addSql('ALTER TABLE templates_document_request DROP FOREIGN KEY FK_86B8570FC33F7837');
        $this->addSql('ALTER TABLE templates_template DROP FOREIGN KEY FK_2592CF0B9393F8FE');
        $this->addSql('ALTER TABLE templates_answer DROP FOREIGN KEY FK_DE8FA1861E27F6BF');
        $this->addSql('ALTER TABLE templates_question_condition DROP FOREIGN KEY FK_5F7289AA727ACA70');
        $this->addSql('ALTER TABLE templates_question_condition DROP FOREIGN KEY FK_5F7289AA1E27F6BF');
        $this->addSql('ALTER TABLE templates_question_option DROP FOREIGN KEY FK_7193122E1E27F6BF');
        $this->addSql('ALTER TABLE templates_answer_option DROP FOREIGN KEY FK_6F0E1359A7C41D6F');
        $this->addSql('ALTER TABLE templates_question_condition DROP FOREIGN KEY FK_5F7289AAA7C41D6F');
        $this->addSql('ALTER TABLE orders_order DROP FOREIGN KEY FK_B4833C3973B21E9C');
        $this->addSql('ALTER TABLE templates_question DROP FOREIGN KEY FK_40599C673B21E9C');
        $this->addSql('ALTER TABLE orders_advice_template DROP FOREIGN KEY FK_13AAB0055DA0FB8');
        $this->addSql('ALTER TABLE orders_subscription DROP FOREIGN KEY FK_B36FA75D5DA0FB8');
        $this->addSql('ALTER TABLE templates_category_template DROP FOREIGN KEY FK_5F69D63D5DA0FB8');
        $this->addSql('ALTER TABLE templates_company_activity_template DROP FOREIGN KEY FK_8A596BEE5DA0FB8');
        $this->addSql('ALTER TABLE templates_document DROP FOREIGN KEY FK_6A9B5AFE5DA0FB8');
        $this->addSql('ALTER TABLE templates_question_step DROP FOREIGN KEY FK_DA263A9D5DA0FB8');
        $this->addSql('ALTER TABLE templates_update DROP FOREIGN KEY FK_9C77DEDB5DA0FB8');
        $this->addSql('DROP TABLE system_task');
        $this->addSql('DROP TABLE orders_advice');
        $this->addSql('DROP TABLE orders_advice_template');
        $this->addSql('DROP TABLE orders_session_browser');
        $this->addSql('DROP TABLE orders_customer');
        $this->addSql('DROP TABLE orders_discount');
        $this->addSql('DROP TABLE orders_invoice');
        $this->addSql('DROP TABLE orders_invoice_discount');
        $this->addSql('DROP TABLE orders_invoice_item');
        $this->addSql('DROP TABLE orders_order');
        $this->addSql('DROP TABLE orders_order_status');
        $this->addSql('DROP TABLE orders_payment');
        $this->addSql('DROP TABLE orders_refund');
        $this->addSql('DROP TABLE orders_session');
        $this->addSql('DROP TABLE orders_subscription');
        $this->addSql('DROP TABLE shops_shop');
        $this->addSql('DROP TABLE shops_testimonial');
        $this->addSql('DROP TABLE templates_answer');
        $this->addSql('DROP TABLE templates_answer_option');
        $this->addSql('DROP TABLE templates_category');
        $this->addSql('DROP TABLE templates_category_template');
        $this->addSql('DROP TABLE templates_company_activity');
        $this->addSql('DROP TABLE templates_company_activity_template');
        $this->addSql('DROP TABLE templates_company_type');
        $this->addSql('DROP TABLE templates_document');
        $this->addSql('DROP TABLE templates_document_request');
        $this->addSql('DROP TABLE templates_partner');
        $this->addSql('DROP TABLE templates_question');
        $this->addSql('DROP TABLE templates_question_condition');
        $this->addSql('DROP TABLE templates_question_option');
        $this->addSql('DROP TABLE templates_question_step');
        $this->addSql('DROP TABLE templates_template');
        $this->addSql('DROP TABLE templates_update');
    }
}
