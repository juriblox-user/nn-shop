<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171018122014 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE templates_template ADD template_excerpt LONGTEXT DEFAULT NULL, CHANGE template_id template_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.template_id)\', CHANGE partner_id partner_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.partner_id)\', CHANGE template_key template_key VARCHAR(6) NOT NULL COMMENT \'(DC2Type:templates.template_key)\', CHANGE price_check price_check INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE price_monthly price_monthly INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE price_unit price_unit INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE remote_id remote_id INT NOT NULL COMMENT \'(DC2Type:juriblox.template_id)\', CHANGE synced_timestamp synced_timestamp DATETIME DEFAULT NULL, CHANGE template_version template_version INT NOT NULL COMMENT \'(DC2Type:templates.template_version)\', CHANGE template_hidden template_hidden TINYINT(1) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE templates_template DROP template_excerpt, CHANGE template_id template_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.template_id)\', CHANGE partner_id partner_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.partner_id)\', CHANGE template_key template_key VARCHAR(6) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.template_key)\', CHANGE price_check price_check INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE price_monthly price_monthly INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE price_unit price_unit INT DEFAULT NULL COMMENT \'(DC2Type:core.financial.money)\', CHANGE remote_id remote_id INT NOT NULL COMMENT \'(DC2Type:juriblox.template_id)\', CHANGE synced_timestamp synced_timestamp DATETIME DEFAULT NULL, CHANGE template_version template_version INT NOT NULL COMMENT \'(DC2Type:templates.template_version)\', CHANGE template_hidden template_hidden TINYINT(1) DEFAULT NULL');
    }
}
