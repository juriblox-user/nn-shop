<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171101094115 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE shops_landing_page_translations (translation_id INT AUTO_INCREMENT NOT NULL, object_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:shops.landing_page_id)\', translation_locale VARCHAR(8) NOT NULL, translation_field VARCHAR(32) NOT NULL, translation_content LONGTEXT DEFAULT NULL, INDEX IDX_9DF44ABE232D562B (object_id), UNIQUE INDEX lookup_unique_idx (translation_locale, object_id, translation_field), PRIMARY KEY(translation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE shops_landing_page_translations ADD CONSTRAINT FK_9DF44ABE232D562B FOREIGN KEY (object_id) REFERENCES shops_landing_page (page_id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE shops_landing_page_translations');
    }
}
