<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170202144718 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX uq_number ON orders_order');
        $this->addSql('ALTER TABLE orders_order CHANGE order_number order_code INT NOT NULL COMMENT \'(DC2Type:orders.order_code)\'');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B4833C393AE40A8F ON orders_order (order_code)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_B4833C393AE40A8F ON orders_order');
        $this->addSql('ALTER TABLE orders_order CHANGE order_code order_number INT NOT NULL COMMENT \'(DC2Type:orders.order_number)\'');
        $this->addSql('CREATE UNIQUE INDEX uq_number ON orders_order (order_number)');
    }
}
