<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170817090426 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE blog_document (id INT AUTO_INCREMENT NOT NULL, article_id INT DEFAULT NULL, document_file VARCHAR(255) NOT NULL, document_slug VARCHAR(255) NOT NULL, document_created DATETIME NOT NULL, document_updated DATETIME NOT NULL, UNIQUE INDEX UNIQ_AC34AE513F2917F1 (document_slug), INDEX IDX_AC34AE517294869C (article_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE blog_author (id INT AUTO_INCREMENT NOT NULL, author_name VARCHAR(255) NOT NULL, author_job VARCHAR(255) NOT NULL, author_image VARCHAR(255) NOT NULL, author_slug VARCHAR(255) NOT NULL, author_created DATETIME NOT NULL, author_updated DATETIME NOT NULL, UNIQUE INDEX UNIQ_28E07D097C7213BD (author_slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE blog_article (id INT AUTO_INCREMENT NOT NULL, author_id INT DEFAULT NULL, article_title VARCHAR(255) NOT NULL, article_content LONGTEXT NOT NULL, article_active TINYINT(1) NOT NULL, article_image VARCHAR(255) NOT NULL, article_slug VARCHAR(255) NOT NULL, article_created DATETIME NOT NULL, article_updated DATETIME NOT NULL, UNIQUE INDEX UNIQ_EECCB3E528DF1C63 (article_slug), INDEX IDX_EECCB3E5F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE blog_document ADD CONSTRAINT FK_AC34AE517294869C FOREIGN KEY (article_id) REFERENCES blog_article (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE blog_article ADD CONSTRAINT FK_EECCB3E5F675F31B FOREIGN KEY (author_id) REFERENCES blog_author (id) ON DELETE SET NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE blog_article DROP FOREIGN KEY FK_EECCB3E5F675F31B');
        $this->addSql('ALTER TABLE blog_document DROP FOREIGN KEY FK_AC34AE517294869C');
        $this->addSql('DROP TABLE blog_document');
        $this->addSql('DROP TABLE blog_author');
        $this->addSql('DROP TABLE blog_article');
    }
}
