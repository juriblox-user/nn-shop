<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170202142137 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX ix_deleted ON orders_subscription');
        $this->addSql('ALTER TABLE orders_subscription ADD subscription_code INT NOT NULL COMMENT \'(DC2Type:orders.subscription_code)\', ADD timestamp_created DATETIME NOT NULL, ADD timestamp_expires DATETIME NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B36FA75D40292086 ON orders_subscription (subscription_code)');
        $this->addSql('CREATE INDEX ix_expiration ON orders_subscription (timestamp_expires, deleted_timestamp)');
        $this->addSql('CREATE INDEX ix_deleted ON orders_subscription (deleted_timestamp, timestamp_created)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX ix_expiration ON orders_subscription');
        $this->addSql('DROP INDEX ix_deleted ON orders_subscription');
        $this->addSql('DROP INDEX UNIQ_B36FA75D40292086 ON orders_subscription');
        $this->addSql('ALTER TABLE orders_subscription DROP subscription_code, DROP timestamp_created, DROP timestamp_expires');
        $this->addSql('CREATE INDEX ix_deleted ON orders_subscription (deleted_timestamp)');
    }
}
