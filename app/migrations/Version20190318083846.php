<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190318083846 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE blog_article_templates DROP FOREIGN KEY FK_FC0069CC7294869C');
        $this->addSql('ALTER TABLE blog_article_translations DROP FOREIGN KEY FK_AEB2FC15232D562B');
        $this->addSql('ALTER TABLE blog_document DROP FOREIGN KEY FK_AC34AE517294869C');
        $this->addSql('ALTER TABLE blog_article DROP FOREIGN KEY FK_EECCB3E5F675F31B');
        $this->addSql('ALTER TABLE blog_article DROP FOREIGN KEY FK_EECCB3E512469DE2');
        $this->addSql('DROP TABLE blog_article');
        $this->addSql('DROP TABLE blog_article_templates');
        $this->addSql('DROP TABLE blog_article_translations');
        $this->addSql('DROP TABLE blog_author');
        $this->addSql('DROP TABLE blog_category');
        $this->addSql('DROP TABLE blog_document');
        $this->addSql('ALTER TABLE orders_customer DROP intercom_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }
}
