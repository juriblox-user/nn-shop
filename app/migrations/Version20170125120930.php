<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170125120930 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE INDEX ix_order ON orders_subscription (order_id, deleted_timestamp)');
        $this->addSql('CREATE INDEX ix_template ON orders_subscription (template_id, deleted_timestamp)');
        $this->addSql('ALTER TABLE orders_subscription_link ADD document_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.document_id)\', CHANGE link_id link_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.subscription_link_id)\', CHANGE subscription_id subscription_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.subscription_id)\', CHANGE link_format link_format VARCHAR(32) NOT NULL COMMENT \'(DC2Type:orders.subscription_link_format)\', CHANGE link_token link_token VARCHAR(40) NOT NULL COMMENT \'(DC2Type:orders.subscription_link_token)\'');
        $this->addSql('ALTER TABLE orders_subscription_link ADD CONSTRAINT FK_F7D5B24AC33F7837 FOREIGN KEY (document_id) REFERENCES templates_document (document_id)');
        $this->addSql('CREATE INDEX IDX_F7D5B24AC33F7837 ON orders_subscription_link (document_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX ix_order ON orders_subscription');
        $this->addSql('DROP INDEX ix_template ON orders_subscription');
        $this->addSql('ALTER TABLE orders_subscription_link DROP FOREIGN KEY FK_F7D5B24AC33F7837');
        $this->addSql('DROP INDEX IDX_F7D5B24AC33F7837 ON orders_subscription_link');
        $this->addSql('ALTER TABLE orders_subscription_link DROP document_id');
    }
}
