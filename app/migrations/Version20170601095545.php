<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170601095545 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE shops_landing_page (page_id CHAR(36) NOT NULL COMMENT \'(DC2Type:shops.landing_page_id)\', discount_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:orders.discount_id)\', page_logo VARCHAR(40) DEFAULT NULL, page_slug VARCHAR(100) NOT NULL, page_text LONGTEXT DEFAULT NULL, page_title VARCHAR(100) NOT NULL, UNIQUE INDEX UNIQ_2471D4CA1F5987B8 (page_slug), INDEX IDX_2471D4CA4C7C611F (discount_id), PRIMARY KEY(page_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shops_landing_page_template (link_id INT AUTO_INCREMENT NOT NULL, landing_page_id CHAR(36) NOT NULL COMMENT \'(DC2Type:shops.landing_page_id)\', template_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.template_id)\', INDEX IDX_C03C376CDF122DC5 (landing_page_id), INDEX IDX_C03C376C5DA0FB8 (template_id), PRIMARY KEY(link_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE shops_landing_page ADD CONSTRAINT FK_2471D4CA4C7C611F FOREIGN KEY (discount_id) REFERENCES orders_discount (discount_id)');
        $this->addSql('ALTER TABLE shops_landing_page_template ADD CONSTRAINT FK_C03C376CDF122DC5 FOREIGN KEY (landing_page_id) REFERENCES shops_landing_page (page_id)');
        $this->addSql('ALTER TABLE shops_landing_page_template ADD CONSTRAINT FK_C03C376C5DA0FB8 FOREIGN KEY (template_id) REFERENCES templates_template (template_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE shops_landing_page_template DROP FOREIGN KEY FK_C03C376CDF122DC5');
        $this->addSql('DROP TABLE shops_landing_page');
        $this->addSql('DROP TABLE shops_landing_page_template');
    }
}
