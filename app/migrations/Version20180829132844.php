<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180829132844 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE translation_profile ADD allowed_payment_methods LONGTEXT NOT NULL COMMENT \'(DC2Type:json_array)\'');
        $this->addSql('UPDATE translation_profile SET allowed_payment_methods = "[\"banktransfer\",\"ideal\",\"creditcard\",\"paypal\",\"bancontact\",\"sofort\",\"eps\",\"giropay\",\"kbc\",\"belfius\",\"inghomepay\",\"bitcoin\"]"');
    }

    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE translation_profile DROP allowed_payment_methods;');
    }
}
