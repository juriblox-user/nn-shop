<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180315153927 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql('DROP TABLE ext_translations');
        $this->addSql('ALTER TABLE pages_page_translations DROP FOREIGN KEY FK_27C984EC232D562B');
        $this->addSql('ALTER TABLE pages_page MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE pages_page DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE pages_page ADD page_id CHAR(36) NOT NULL COMMENT \'(DC2Type:pages.page_id)\', DROP id, CHANGE profile_id profile_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:profiles.profile_id)\'');
        $this->addSql('ALTER TABLE pages_page ADD PRIMARY KEY (page_id)');
        $this->addSql('ALTER TABLE pages_page_translations CHANGE object_id object_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:pages.page_id)\'');
        $this->addSql('ALTER TABLE pages_page_translations ADD CONSTRAINT FK_27C984EC232D562B FOREIGN KEY (object_id) REFERENCES pages_page (page_id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pages_page DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE pages_page ADD id INT AUTO_INCREMENT NOT NULL, DROP page_id, CHANGE profile_id profile_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:profiles.profile_id)\'');
        $this->addSql('ALTER TABLE pages_page ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE pages_page_translations DROP FOREIGN KEY FK_27C984EC232D562B');
        $this->addSql('ALTER TABLE pages_page_translations CHANGE object_id object_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE pages_page_translations ADD CONSTRAINT FK_27C984EC232D562B FOREIGN KEY (object_id) REFERENCES pages_page (id) ON DELETE CASCADE');
    }
}
