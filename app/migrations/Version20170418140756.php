<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170418140756 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE shops_referrer (referrer_id CHAR(36) NOT NULL COMMENT \'(DC2Type:shops.referrer_id)\', discount_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:orders.discount_id)\', referrer_kickback TINYINT(1) NOT NULL, referrer_landing TINYINT(1) NOT NULL, referrer_logo VARCHAR(40) DEFAULT NULL, referrer_slug VARCHAR(100) NOT NULL, referrer_text LONGTEXT DEFAULT NULL, referrer_title VARCHAR(100) NOT NULL, timestamp_deleted DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8F724DD2465CA948 (referrer_slug), INDEX IDX_8F724DD24C7C611F (discount_id), INDEX ix_deleted (timestamp_deleted), PRIMARY KEY(referrer_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shops_referrer_template (link_id INT AUTO_INCREMENT NOT NULL, referrer_id CHAR(36) NOT NULL COMMENT \'(DC2Type:shops.referrer_id)\', template_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.template_id)\', INDEX IDX_A5D8836F798C22DB (referrer_id), INDEX IDX_A5D8836F5DA0FB8 (template_id), PRIMARY KEY(link_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE shops_referrer ADD CONSTRAINT FK_8F724DD24C7C611F FOREIGN KEY (discount_id) REFERENCES orders_discount (discount_id)');
        $this->addSql('ALTER TABLE shops_referrer_template ADD CONSTRAINT FK_A5D8836F798C22DB FOREIGN KEY (referrer_id) REFERENCES shops_referrer (referrer_id)');
        $this->addSql('ALTER TABLE shops_referrer_template ADD CONSTRAINT FK_A5D8836F5DA0FB8 FOREIGN KEY (template_id) REFERENCES templates_template (template_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE shops_referrer_template DROP FOREIGN KEY FK_A5D8836F798C22DB');
        $this->addSql('DROP TABLE shops_referrer');
        $this->addSql('DROP TABLE shops_referrer_template');
    }
}
