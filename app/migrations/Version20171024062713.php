<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171024062713 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE translation_profile_locale (profile_id INT NOT NULL, locale_id INT NOT NULL, INDEX IDX_D8F151BFCCFA12B8 (profile_id), INDEX IDX_D8F151BFE559DFD1 (locale_id), PRIMARY KEY(profile_id, locale_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE translation_locale (id INT AUTO_INCREMENT NOT NULL, locale_locale VARCHAR(255) NOT NULL, locale_slug VARCHAR(255) NOT NULL, locale_created DATETIME NOT NULL, locale_updated DATETIME NOT NULL, UNIQUE INDEX UNIQ_9CEA4A397FBF53B6 (locale_slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE translation_profile_locale ADD CONSTRAINT FK_D8F151BFCCFA12B8 FOREIGN KEY (profile_id) REFERENCES translation_profile (id)');
        $this->addSql('ALTER TABLE translation_profile_locale ADD CONSTRAINT FK_D8F151BFE559DFD1 FOREIGN KEY (locale_id) REFERENCES translation_locale (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE translation_profile_locale DROP FOREIGN KEY FK_D8F151BFE559DFD1');
        $this->addSql('DROP TABLE translation_profile_locale');
        $this->addSql('DROP TABLE translation_locale');
    }
}
