<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180724075140 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE translation_profile ADD company_name VARCHAR(255) NOT NULL, ADD address_line1 VARCHAR(255) NOT NULL, ADD address_line2 VARCHAR(255) DEFAULT NULL, ADD profile_city VARCHAR(255) NOT NULL, ADD profile_province VARCHAR(255) DEFAULT NULL, ADD profile_zipcode VARCHAR(255) NOT NULL, ADD vat_label VARCHAR(255) NOT NULL, ADD vat_number VARCHAR(255) NOT NULL, ADD coc_label VARCHAR(255) NOT NULL, ADD coc_number VARCHAR(255) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE translation_profile DROP company_name, DROP address_line1, DROP address_line2, DROP profile_city, DROP profile_province, DROP profile_zipcode, DROP vat_label, DROP vat_number, DROP coc_label, DROP coc_number');
    }
}
