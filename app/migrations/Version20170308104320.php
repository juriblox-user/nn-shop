<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170308104320 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE orders_order SET step_id = NULL');
        $this->addSql('DELETE FROM templates_answer_option');
        $this->addSql('DELETE FROM templates_answer');
        $this->addSql('DELETE FROM templates_question_condition');
        $this->addSql('DELETE FROM templates_question_option');
        $this->addSql('DELETE FROM templates_question');
        $this->addSql('DELETE FROM templates_question_step');

        $this->addSql('DROP INDEX UNIQ_40599C62A3E9C94 ON templates_question');
        $this->addSql('ALTER TABLE templates_question ADD lookup_id INT NOT NULL COMMENT \'(DC2Type:juriblox.question_id)\'');
        $this->addSql('CREATE INDEX ix_lookup ON templates_question (lookup_id, timestamp_deleted)');
        $this->addSql('DROP INDEX ix_remote ON templates_question_option');

        $this->addSql('ALTER TABLE templates_question_option DROP FOREIGN KEY FK_7193122E1E27F6BF');
        $this->addSql('DROP INDEX ix_question ON templates_question_option');
        $this->addSql('ALTER TABLE templates_question_option ADD lookup_id INT NOT NULL COMMENT \'(DC2Type:juriblox.question_option_id)\'');
        $this->addSql('ALTER TABLE templates_question_option ADD CONSTRAINT FK_7193122E1E27F6BF FOREIGN KEY (question_id) REFERENCES templates_question (question_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7193122E8955C49D ON templates_question_option (lookup_id)');
        $this->addSql('CREATE INDEX IDX_7193122E1E27F6BF ON templates_question_option (question_id)');
        $this->addSql('CREATE INDEX ix_question ON templates_question_option (question_id, option_position)');

        $this->addSql('SET foreign_key_checks = 1');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('SET foreign_key_checks = 0');

        $this->addSql('DROP INDEX ix_lookup ON templates_question');
        $this->addSql('ALTER TABLE templates_question DROP lookup_id');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_40599C62A3E9C94 ON templates_question (remote_id)');

        $this->addSql('ALTER TABLE templates_question_option DROP FOREIGN KEY FK_7193122E1E27F6BF');
        $this->addSql('DROP INDEX UNIQ_7193122E8955C49D ON templates_question_option');
        $this->addSql('DROP INDEX IDX_7193122E1E27F6BF ON templates_question_option');
        $this->addSql('DROP INDEX ix_question ON templates_question_option');
        $this->addSql('ALTER TABLE templates_question_option DROP lookup_id');
        $this->addSql('ALTER TABLE templates_question_option ADD CONSTRAINT FK_7193122E1E27F6BF FOREIGN KEY (question_id) REFERENCES templates_question (question_id)');
        $this->addSql('CREATE INDEX ix_remote ON templates_question_option (remote_id)');
        $this->addSql('CREATE INDEX ix_question ON templates_question_option (question_id)');

        $this->addSql('SET foreign_key_checks = 1');
    }
}
