<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170125100422 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE orders_subscription_link (link_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.subscription_link_id)\', subscription_id CHAR(36) NOT NULL COMMENT \'(DC2Type:orders.subscription_id)\', link_format VARCHAR(32) NOT NULL COMMENT \'(DC2Type:orders.subscription_link_format)\', link_token VARCHAR(40) NOT NULL COMMENT \'(DC2Type:orders.subscription_link_token)\', UNIQUE INDEX UNIQ_F7D5B24AF08CFDE5 (link_token), INDEX IDX_F7D5B24A9A1887DC (subscription_id), PRIMARY KEY(link_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE orders_subscription_link ADD CONSTRAINT FK_F7D5B24A9A1887DC FOREIGN KEY (subscription_id) REFERENCES orders_subscription (subscription_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE orders_subscription_link');
    }
}
