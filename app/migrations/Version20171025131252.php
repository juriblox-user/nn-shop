<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171025131252 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE templates_partner ADD profile_id INT DEFAULT NULL, CHANGE partner_id partner_id CHAR(36) NOT NULL COMMENT \'(DC2Type:templates.partner_id)\', CHANGE remote_id remote_id SMALLINT NOT NULL, CHANGE synced_timestamp synced_timestamp DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE templates_partner ADD CONSTRAINT FK_7EF09EAFCCFA12B8 FOREIGN KEY (profile_id) REFERENCES translation_profile (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_7EF09EAFCCFA12B8 ON templates_partner (profile_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE templates_partner DROP FOREIGN KEY FK_7EF09EAFCCFA12B8');
        $this->addSql('DROP INDEX IDX_7EF09EAFCCFA12B8 ON templates_partner');
        $this->addSql('ALTER TABLE templates_partner DROP profile_id, CHANGE partner_id partner_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:templates.partner_id)\', CHANGE remote_id remote_id SMALLINT NOT NULL, CHANGE synced_timestamp synced_timestamp DATETIME DEFAULT NULL');
    }
}
