<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171031140117 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE shops_landing_page ADD profile_id INT DEFAULT NULL, CHANGE page_id page_id CHAR(36) NOT NULL COMMENT \'(DC2Type:shops.landing_page_id)\', CHANGE discount_id discount_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:orders.discount_id)\'');
        $this->addSql('ALTER TABLE shops_landing_page ADD CONSTRAINT FK_2471D4CACCFA12B8 FOREIGN KEY (profile_id) REFERENCES translation_profile (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_2471D4CACCFA12B8 ON shops_landing_page (profile_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE shops_landing_page DROP FOREIGN KEY FK_2471D4CACCFA12B8');
        $this->addSql('DROP INDEX IDX_2471D4CACCFA12B8 ON shops_landing_page');
        $this->addSql('ALTER TABLE shops_landing_page DROP profile_id, CHANGE page_id page_id CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:shops.landing_page_id)\', CHANGE discount_id discount_id CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:orders.discount_id)\'');
    }
}
