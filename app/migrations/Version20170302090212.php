<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170302090212 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_payment ADD timestamp_canceled DATETIME DEFAULT NULL, DROP timestamp_cancelled, CHANGE timestamp_expired timestamp_expired DATETIME DEFAULT NULL, CHANGE timestamp_received timestamp_received DATETIME DEFAULT NULL, CHANGE timestamp_requested timestamp_requested DATETIME NOT NULL');
        $this->addSql('ALTER TABLE orders_subscription ADD timestamp_canceled DATETIME DEFAULT NULL, DROP timestamp_cancelled, CHANGE timestamp_created timestamp_created DATETIME NOT NULL, CHANGE timestamp_expires timestamp_expires DATETIME NOT NULL');
        $this->addSql('ALTER TABLE templates_document ADD timestamp_canceled DATETIME DEFAULT NULL, DROP timestamp_cancelled, CHANGE timestamp_error timestamp_error DATETIME DEFAULT NULL, CHANGE timestamp_generated timestamp_generated DATETIME DEFAULT NULL, CHANGE timestamp_requested timestamp_requested DATETIME DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_payment ADD timestamp_cancelled DATETIME DEFAULT NULL, DROP timestamp_canceled, CHANGE timestamp_expired timestamp_expired DATETIME DEFAULT NULL, CHANGE timestamp_received timestamp_received DATETIME DEFAULT NULL, CHANGE timestamp_requested timestamp_requested DATETIME NOT NULL');
        $this->addSql('ALTER TABLE orders_subscription ADD timestamp_cancelled DATETIME DEFAULT NULL, DROP timestamp_canceled, CHANGE timestamp_created timestamp_created DATETIME NOT NULL, CHANGE timestamp_expires timestamp_expires DATETIME NOT NULL');
        $this->addSql('ALTER TABLE templates_document ADD timestamp_cancelled DATETIME DEFAULT NULL, DROP timestamp_canceled, CHANGE timestamp_error timestamp_error DATETIME DEFAULT NULL, CHANGE timestamp_generated timestamp_generated DATETIME DEFAULT NULL, CHANGE timestamp_requested timestamp_requested DATETIME DEFAULT NULL');
    }
}
