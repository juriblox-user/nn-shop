<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use NnShop\Domain\Orders\Enumeration\CustomerStatus;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181211091119 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_customer CHANGE email_current customer_email VARCHAR(255) NOT NULL COMMENT \'(DC2Type:core.web.email_address)\', ADD email_change VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:core.web.email_address)\', ADD email_verified TINYINT(1) NOT NULL, ADD customer_status VARCHAR(255) NOT NULL COMMENT \'(DC2Type:orders.customer_status)\', CHANGE customer_secret customer_token VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:core.security.sensitive_value)\', DROP email_updated, DROP email_updated_token');
        $this->addSql('CREATE INDEX ix_email ON orders_customer (customer_email, timestamp_deleted)');

        $this->addSql('UPDATE orders_customer SET customer_status = :status', [
            'status' => CustomerStatus::DRAFT,
        ]);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
