<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171025105551 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE templates_company_type_translations (translation_id INT AUTO_INCREMENT NOT NULL, object_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:templates.company_type_id)\', translation_locale VARCHAR(8) NOT NULL, translation_field VARCHAR(32) NOT NULL, translation_content LONGTEXT DEFAULT NULL, INDEX IDX_FCC7A4E1232D562B (object_id), UNIQUE INDEX lookup_unique_idx (translation_locale, object_id, translation_field), PRIMARY KEY(translation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE templates_company_type_translations ADD CONSTRAINT FK_FCC7A4E1232D562B FOREIGN KEY (object_id) REFERENCES templates_company_type (type_id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE templates_company_type_translations');
    }
}
