<?php

namespace NnShop\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180307101016 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_7193122E8955C49D ON templates_question_option');
        $this->addSql('DROP INDEX UNIQ_7193122E2A3E9C94 ON templates_question_option');

        $this->addSql('CREATE INDEX ix_lookup ON templates_question_option (lookup_id, timestamp_deleted)');
        $this->addSql('CREATE INDEX ix_remote ON templates_question_option (remote_id, timestamp_deleted)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX ix_lookup ON templates_question_option');
        $this->addSql('DROP INDEX ix_remote ON templates_question_option');

        $this->addSql('CREATE UNIQUE INDEX UNIQ_7193122E8955C49D ON templates_question_option (lookup_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7193122E2A3E9C94 ON templates_question_option (remote_id)');
    }
}
