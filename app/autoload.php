<?php

use Composer\Autoload\ClassLoader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Dotenv\Dotenv;

error_reporting(error_reporting() & ~E_USER_DEPRECATED);

/**
 * @var ClassLoader
 */
$loader = require __DIR__ . '/../vendor/autoload.php';

// Environment laden
$env = new Dotenv(__DIR__ . '/..');
if (file_exists(__DIR__ . '/../.env')) {
    $env->overload();
}

$env->required([
    'APP_DEBUG',
    'APP_EXPOSE',
    'APP_ENVIRONMENT',
    'APP_SECRET',

    'HOST_BASE',
    'HOST_SCHEME',

    'ASSETS_BASE_URL',

    'DATABASE_HOST',
    'DATABASE_PORT',
    'DATABASE_NAME',
    'DATABASE_USER',
    'DATABASE_PASSWORD',

    'MAILER_TRANSPORT',
    'MAILER_HOST',
    'MAILER_USER',
    'MAILER_PASSWORD',

    'RABBITMQ_HOST',
    'RABBITMQ_PORT',
    'RABBITMQ_VHOST',
    'RABBITMQ_USER',
    'RABBITMQ_PASSWORD',

    'JURIBLOX_API_URL',

    'WKHTMLTOPDF_BINARY',
]);

// Build info laden
$build = new Dotenv(__DIR__ . '/..', '.build');
if (file_exists(__DIR__ . '/../.build')) {
    $build->overload();

    $build->required([
        'BUILD_HASH',
        'BUILD_HASH_SHORT',
        'BUILD_TAG',
        'BUILD_BRANCH',
        'BUILD_VERSION',
        'BUILD_MACHINE',
        'BUILD_TIMESTAMP',
    ]);
}

AnnotationRegistry::registerLoader([$loader, 'loadClass']);

return $loader;
