<?php

use Core\Application\AbstractKernel;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Security\Core\User\UserCheckerInterface;

class AppKernel extends AbstractKernel
{
    /**
     * {@inheritdoc}
     */
    public function getApplicationName(): string
    {
        return 'nn-shop';
    }

    /**
     * {@inheritdoc}
     */
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),

            /* 1: JMS serializer - order is important! */
            new JMS\SerializerBundle\JMSSerializerBundle(),

            /* 2: middag/symfony-core - order is important! */
            new Core\Application\CoreBundle\CoreBundle(),
            new Core\Application\RabbitMQBundle\RabbitMQBundle(),

            /* 3: SimpleBus - order is important! */
            new SimpleBus\AsynchronousBundle\SimpleBusAsynchronousBundle(),
            new SimpleBus\SymfonyBridge\SimpleBusCommandBusBundle(),
            new SimpleBus\SymfonyBridge\SimpleBusEventBusBundle(),
            new SimpleBus\JMSSerializerBundleBridge\SimpleBusJMSSerializerBundleBridgeBundle(),
            new SimpleBus\RabbitMQBundleBridge\SimpleBusRabbitMQBundleBridgeBundle(),
            new SimpleBus\SymfonyBridge\DoctrineOrmBridgeBundle(),

            /* 4: other bundles */
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new Knp\Bundle\MarkdownBundle\KnpMarkdownBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Knp\Bundle\GaufretteBundle\KnpGaufretteBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Knp\Bundle\SnappyBundle\KnpSnappyBundle(),
            new DavidBadura\FakerBundle\DavidBaduraFakerBundle(),
            new Eko\FeedBundle\EkoFeedBundle(),
            new OldSound\RabbitMqBundle\OldSoundRabbitMqBundle(),

            new JMS\I18nRoutingBundle\JMSI18nRoutingBundle(),
            new JMS\TranslationBundle\JMSTranslationBundle(),

            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),

            new NnShop\Application\AppBundle\AppBundle(),
        ];

        if ($this->isEnvironment([self::ENVIRONMENT_DEVELOPMENT, self::ENVIRONMENT_STAGING, self::ENVIRONMENT_TEST])) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    /**
     * {@inheritdoc}
     */
    protected function build(ContainerBuilder $container)
    {
        $container
            ->registerForAutoconfiguration(UserCheckerInterface::class)
            ->addTag('user_checker');
    }
}
