require('bridge')(__filename, module);

const hljs = require('highlight.js');

module.exports.index = function () {
    $(document).ready(function() {
        $('pre code').each(function(i, block) {
            hljs.highlightBlock(block);
        });
    });
};