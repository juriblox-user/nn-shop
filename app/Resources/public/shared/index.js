require('expose-loader?Application.Bridge!bridge');

// Modules
require('./vendor/flexibility-2.0.1/lib');

// Client-side JS
require('./modules/global.js');

// SCSS
require('./sass/main.scss');

// Afbeeldingen
require('../static/images/payment-ideal.png');
require('../static/images/payment-paypal.png');
require('../static/images/payment-bitcoin.png');

require('../static/images/phone@2x.png');
require('../static/images/phone-en@2x.png');
require('../static/images/express_phone@2x.png');
require('../static/images/privacy_logo@2x.png');
require('../static/images/document.png');
