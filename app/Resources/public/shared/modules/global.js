const $ = require('jquery');
const sprintf = require('sprintf-js').sprintf;

const Cookies = require('js-cookie');

// Vendor javascript als packages
require('jquery-inview');
require('jquery.scrollto');

require('bootstrap-sass/assets/javascripts/bootstrap/dropdown');
require('bootstrap-sass/assets/javascripts/bootstrap/tooltip');

// Niet-NPM vendor Javascript
require('../vendor/flowtype-1.1.0/flowtype');
require('../vendor/headroom-0.7.0/dist/headroom.js');
require('../vendor/headroom-0.7.0/dist/jQuery.headroom.js');
require('../vendor/timers/jquery.timers');

const Slideout = require('slideout');

$().ready(function () {
    const $header = $('header');

    const searchBars = document.querySelectorAll(".document-search");
    let searchRequest = null;

    Array.from(searchBars).forEach(searchBar => {
        searchBar.addEventListener("keyup", () => {
            let minLength = 3;
            let value = searchBar.value;
            let path = searchBar.getAttribute('data-path');
            const results = $('.search-results')

            if (value.length >= minLength) {
                searchRequest = $.ajax({
                    type: "POST",
                    url: path,
                    data: {
                        'q': value
                    },
                    dataType: "text",
                    success: function (result) {
                        let parsed = JSON.parse(result);

                        const results = document.createElement("div");
                        results.className = "search-results";

                        function appendResults() {
                            if (searchBar.parentElement.getElementsByClassName("search-results").length === 0){
                                searchBar.parentElement.append(results);
                            }
                        }

                        if (parsed.length >= 1){
                           appendResults();

                            parsed.forEach((item) => {
                                const link = document.createElement("a");
                                link.href = item.url;
                                link.className = "search-results__link"
                                link.innerText = item.title;
                                results.appendChild(link);
                            })
                        } else {
                           appendResults();
                           results.append('Er konden geen documenten worden gevonden.')
                        }
                    },
                    error: function () {
                        results.html('');
                        results.hide();
                    }
                })
            } else {
                const results = document.querySelectorAll(".search-results");
                Array.from(results).forEach(results => {
                    results.parentNode.removeChild(results)
                })
            }
        });

        searchBar.addEventListener("blur", () => {
            const results = document.querySelectorAll(".search-results");
                window.setTimeout(() => {
                    Array.from(results).forEach(results => {
                        results.parentNode.removeChild(results)
                    })
                }, 1000)
        })
    });

    /*
     * Header
     */
    $header.headroom({
        'offset': 103 /* = $header-normal-height */,
        'tolerance': 0,

        'classes': {
            'initial': 'header',
            'pinned': 'header-pinned',
            'unpinned': 'header-unpinned',

            'top': 'header-top',
            'notTop': 'header-not-top'
        }
    });

    $('a.dropdown-toggle', $header).dropdown();

    /*
     * Shop dropdown
     */
    var $shopDropdown = $('div.shop-dropdown', $header);

    $('a[data-toggle="shop"]', $header).click(function () {
        var $this = $(this);

        var _hideDropdown = function () {
            $header.removeClass('header-shops');
            $this.attr('aria-expanded', 'false');

            $('html').off('click');
            $header.off('click');

            $shopDropdown.off('inview');
        };

        if ($this.attr('aria-expanded') == 'true') {
            _hideDropdown();
        }
        else {
            $.scrollTo(0, 500);

            $header.addClass('header-shops');
            $this.attr('aria-expanded', 'true');

            $('html').on('click', function () {
                _hideDropdown();
            });

            $header.on('click', function (event) {
                event.stopPropagation();
            });

            $shopDropdown.on('inview', function (event, visible) {
                if (!visible) {
                    $header.hide();
                    $header.oneTime(100, function () {
                        $header.show();
                    });

                    _hideDropdown();
                }
            });
        }

        return false;
    });

    var selectShop = function ($link) {
        $('div.shops a', $shopDropdown).removeClass('active');
        $link.addClass('active');

        $('div.categories div[data-shop]', $shopDropdown).hide();
        $('div.templates div[data-category]', $shopDropdown).hide();
        $(sprintf('div.categories div[data-shop="%s"]', $link.data('shop'))).show();
    };

    /**
     *
     */
    const selectCategory = ($link) => {
        $('div.categories a', $shopDropdown).removeClass('active')
        $link.addClass('active');

        $('div.templates div[data-category]', $shopDropdown).hide();
        $(sprintf('div.templates div[data-category="%s"]', $link.data('category'))).show();
    };

    // Shop openen bij klikken op shop
    $('div.shops a[data-shop]', $shopDropdown).click(function () {
        selectShop($(this));
        return false;
    });

    // Category openen bij klikken
    $('div.categories a[data-category]', $shopDropdown).click(function () {
        selectCategory($(this));
        return false;
    });

    var $defaultShop = $('div.shops a.active:first');
    if ($defaultShop.length > 0) {
        selectShop($defaultShop);
    }
    else {
        selectShop($('div.shops a:first'));
    }

    /*
     * Tooltips
     */
    $('[data-toggle="tooltip"]').tooltip();

    /*
     * Hamburger menu
     */
    const slideout = new Slideout({
        'panel': document.getElementById('panel'),
        'menu': document.getElementById('menu'),
        'side' : 'right',
        'padding': 256,
        'tolerance': 70,

        'easing': 'ease-in-out'
    });

    $('a[data-toggle="slidebar"]', $header).click(function () {
        slideout.toggle();

        return false;
    });

    /*
     * Scrollen naar de volgende section
     */
    $('a[data-scroll-to]').click(function () {
        $.scrollTo($(sprintf('[data-scroll-id="%s"]', $(this).data('scroll-to'))), 500);

        return false;
    });

    /*
     * Flowtype
     */
    $('[data-flow-min][data-flow-max][data-flow-ratio]').each(function () {
        $(this).flowtype({
            minFont: $(this).data('flow-min'),
            maxFont: $(this).data('flow-max'),
            fontRatio: $(this).data('flow-ratio'),
        });
    });

    /*
     * Cookie banner
     */
    if ($('header div.banner div[data-content]:visible').length === 0) {
        const $cookie_banner = $('header div.banner div[data-content="cookies"]');

        if ($cookie_banner.length > 0) {
            if (undefined === Cookies.getJSON('cookie-banner')) {
                $header.addClass('with-banner');
                $cookie_banner.show();

                Cookies.set('cookie-banner', false, {
                    expires: 3650
                });
            }

            $('a[data-role="hide-banner"]', $cookie_banner).click(function () {
                $header.removeClass('with-banner');
                $('header div.banner div[data-content="cookies"]').hide();

                return false;
            });
        }
    }

    /*
    * Header
    */
    const $proxy = $header.clone()
        .addClass('header--proxy')
        .attr('aria-hidden', 'true');

    $proxy.prependTo($header.parent());
});
