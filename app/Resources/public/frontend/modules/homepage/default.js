const bridge = require('bridge')(__filename, module);

const $ = require('jquery');

require('../../../shared/vendor/owl-carousel-2.0.0-beta2.4/owl.carousel.js');

module.exports = function () {

    let interval;

    const timer = function () {
        interval = setInterval(function () {
            let currentSlide = $('.slide.active');
            let nextSlide = currentSlide.next()

            let currentControl = $('.control.active');
            let nextControl = currentControl.next()

            let currentIcon = $('.mobile-slider-icons div.active')
            let nextIcon = currentIcon.next()

            currentSlide.removeClass('active');
            nextSlide.addClass('active');

            currentControl.removeClass('active');
            currentIcon.removeClass('active');
            nextControl.addClass('active');
            nextIcon.addClass('active');

            if(nextSlide.length === 0) {
                $('.slide').first().addClass('active')
            }

            if(nextControl.length === 0) {
                $('.control').first().addClass('active')
            }
        }, 10000)
    }

    timer();

    $('.slider-controls .control').click(function () {
        clearInterval(interval);
        timer();
        let index = $(this).index() + 1

        let currentSlide = $('.slide.active');
        let clickedSlide = $('.slide:nth-child(' + index + ')')

        $('.slider-controls .control').removeClass('active');
        $(this).addClass('active');

        currentSlide.removeClass('active');
        clickedSlide.addClass('active');
    })

    $('.mobile-slider-wrapper .right').click(function () {
        clearInterval(interval);
        timer();
        let current = $('.mobile-slider-icons div.active')
        let next = current.next()
        current.removeClass('active');
        next.addClass('active');

        let currentSlide = $('.slide.active');
        let nextSlide = currentSlide.next()
        currentSlide.removeClass('active');
        nextSlide.addClass('active');

        if(nextSlide.length === 0){
            $('.slide').first().addClass('active')
        }

        if( next.length === 0){
            $('.mobile-slider-icons div').first().addClass('active')
        }
    })

    $('.mobile-slider-wrapper .left').click(function () {
        clearInterval(interval);
        timer();
        let current = $('.mobile-slider-icons div.active')
        let previous = current.prev()
        current.removeClass('active');
        previous.addClass('active');

        let currentSlide = $('.slide.active');
        let previousSlide = currentSlide.prev()
        currentSlide.removeClass('active');
        previousSlide.addClass('active');

        if(previousSlide.length === 0){
            $('.slide').last().addClass('active')
        }

        if( previous.length === 0){
            $('.mobile-slider-icons div').last().addClass('active')
        }
    })

    /*
     * Carrousel voor de referrers
     */
    $('div.referrers div.row.owl-carousel').each(function () {
        const $this = $(this);

        if ($('div.referrer', $this).length <= 1) {
            return;
        }

        $this.owlCarousel({
            loop: true,

            nav: false,
            dots: true,

            autoplay: true,
            autoplaySpeed: 2000,

            responsive: {
                0: {
                    items: 1
                },

                768: {
                    items: 2
                },

                992: {
                    items: Math.min(3, $('div.referrer', $this).length),
                    dots: false
                }
            }
        });
    });

    /*
     * Carrousel voor de templates
     */
    $('div.templates div.row.owl-carousel').each(function () {
        var $this = $(this);

        if ($('div.template', $this).length <= 1) {
            return;
        }

        $this.owlCarousel({
            loop: false,

            nav: false,
            dots: true,

            responsive: {
                0: {
                    items: 1
                },

                768: {
                    items: 2
                },

                992: {
                    items: 3
                },

                1200: {
                    items: Math.min(4, $('div.template', $this).length),
                    dots: false
                }
            }
        });
    });

    /*
     * Carrousel voor de testimonials
     */
    $('div.references div.row.owl-carousel').each(function () {
        var $this = $(this);

        if ($('div.reference', $this).length <= 1) {
            console.error('Only one reference -> hiding carousel');

            return;
        }

        $this.owlCarousel({
            loop: true,

            nav: false,
            dots: true,

            autoplay: true,
            autoplaySpeed: 2000,

            responsive: {
                0: {
                    items: 1
                },

                768: {
                    items: 2
                },

                992: {
                    items: Math.min(3, $('div.reference', $this).length),
                    dots: false
                }
            }
        });
    });
};
