const bridge = require('bridge')(__filename, module);
const interval = require('interval');

const Clipboard = require('clipboard');

module.exports.index = function () {
    new Clipboard('input[data-role="download-url"]');

    var status = bridge.getVariable('status');
    if (_.indexOf(['pending_payment', 'request', 'pending', 'requeue', 'deliver'], status) > -1) {
        interval.every(function () {
            document.location = bridge.getVariable('redirectUrl');
        }, 10000);
    }
};