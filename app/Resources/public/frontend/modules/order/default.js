const bridge = require('bridge')(__filename, module);
const sprintf = require('sprintf-js').sprintf;

const $ = require('jquery');
const _ = require('lodash');

const Cleave = require('cleave.js');

const vex = require('vex-js/src/vex.js');
const markdown = require('markdown').markdown;

require('bootstrap-datepicker');
require('bootstrap-datepicker/js/locales/bootstrap-datepicker.nl.js');

require('select2');
require('select2/dist/js/i18n/nl');

// Initialisatie van Vex
vex.registerPlugin(require('vex-dialog/src/vex.dialog.js'));

function Logger(enabled) {
    /**
     * @type {bool}
     * @private
     */
    var _enabled = enabled;

    /**
     * @type {number}
     * @private
     */
    var _indentation = 0;

    /**
     * @param message
     */
    this.debug = function (message) {
        if (!_enabled) {
            return;
        }

        if (console.debug === undefined) {
            console.log(this.getSpaces(_indentation) + message);
        } else {
            console.debug(this.getSpaces(_indentation) + message);
        }
    };

    /**
     *
     */
    this.indent = function () {
        ++_indentation;
    };

    /**
     * @param message
     */
    this.info = function (message) {
        if (!_enabled) {
            return;
        }

        console.info(this.getSpaces(_indentation) + message);
    };

    /**
     * @param message
     */
    this.log = function (message) {
        if (!_enabled) {
            return;
        }

        console.log(this.getSpaces(_indentation) + message);
    };

    /**
     *
     */
    this.outdent = function () {
        --_indentation;
    };

    /**
     * @param message
     */
    this.warn = function (message) {
        if (!_enabled) {
            return;
        }

        console.warn(this.getSpaces(_indentation) + message);
    };

    /**
     * Voorloper-spaties maken (' '.repeat geeft foutmeldingen in IE11)
     *
     * @param _indentation
     * @returns {string}
     */
    this.getSpaces = function (_indentation) {
        let output = '';
        for (let i = 0; i < _indentation; i++) {
            output += ' ';
        }

        return output;
    };
}

// Logger aanmaken
const logger = new Logger(DEVELOPMENT);

/**
 * @param id
 * @param type
 *
 * @constructor
 */
function Question(id, type) {
    /**
     * @type {string}
     */
    var _id = id;

    /**
     * @type {Condition[]}
     * @private
     */
    var _conditions = [];

    /**
     * @type {string|undefined}
     * @private
     */
    var _title = undefined;

    /**
     * @type {string}
     */
    var _type = type;

    /**
     * @param condition {Condition}
     */
    this.addCondition = function (condition) {
        _conditions.push(condition);
    };

    /**
     * @returns {Condition[]}
     */
    this.getConditions = function () {
        return _conditions;
    };

    /**
     * @returns {string}
     */
    this.getId = function () {
        return _id;
    };

    /**
     * @returns {string|undefined}
     */
    this.getTitle = function () {
        return _title;
    };

    /**
     * @returns {string}
     */
    this.getType = function () {
        return _type;
    };

    /**
     * @param title
     */
    this.setTitle = function (title) {
        _title = title;
    };
}

/**
 * @param type
 * @param reference
 *
 * @constructor
 */
function Condition(type, reference) {
    /**
     * @type {string}
     * @private
     */
    var _type = type;

    /**
     * @type {string}
     * @private
     */
    var _reference = reference;

    /**
     * @type {boolean|undefined}
     * @private
     */
    var _default = undefined;

    /**
     * @type {string|undefined}
     * @private
     */
    var _question = undefined;

    /**
     * @returns {boolean}
     */
    this.getDefault = function () {
        return _default;
    };

    /**
     * @returns {string}
     */
    this.getQuestion = function () {
        return _type === 'question' ? _reference : _question;
    };

    /**
     * @returns {string}
     */
    this.getReference = function () {
        return _reference;
    };

    /**
     * @returns {string}
     */
    this.getType = function () {
        return _type;
    };

    /**
     * @param referenceDefault
     */
    this.setDefault = function (referenceDefault) {
        if (referenceDefault === 'true') {
            referenceDefault = true;
        } else if (referenceDefault === 'false') {
            referenceDefault = false;
        } else if (typeof referenceDefault !== 'boolean') {
            throw 'The default should be either a boolean, "true" or "false"';
        }

        _default = referenceDefault;
    };

    /**
     * @param questionId
     */
    this.setQuestion = function (questionId) {
        if ('question' === _type) {
            throw 'You cannot explicitly set a question for a "question" Condition type';
        }

        _question = questionId;
    };
}

/**
 * @constructor
 */
function QuestionResolver() {
    /**
     * Cache manager
     *
     * @constructor
     */
    function Cache() {
        var _groupElements = [];
        var _optionElements = [];
        var _questionElements = [];

        /**
         *
         */
        this.clear = function() {
            _optionElements = [];
            _questionElements = [];
        };

        /**
         * @param questionId
         * @returns {*}
         */
        this.getGroupElement = function(questionId) {
            var $element = _groupElements[questionId];

            if (undefined === $element) {
                $element = this.writeGroupElement(questionId, $('div.form-group:has([data-id="' + questionId + '"])'));
            }

            return $element;
        };

        /**
         * @param optionId
         * @returns {*}
         */
        this.getOptionElement = function(optionId) {
            var $element = _optionElements[optionId];

            if (undefined === $element) {
                $element = this.writeOptionElement(optionId, $('[value="' + optionId + '"]'));
            }

            return $element;
        };

        /**
         * @param questionId
         * @returns {*}
         */
        this.getQuestionElement = function(questionId) {
            var $element = _questionElements[questionId];

            if (undefined === $element) {
                $element = this.writeQuestionElement(questionId, $('[data-id="' + questionId + '"][name]'));
            }

            return $element;
        };

        /**
         * @param questionId
         * @param $element
         */
        this.writeGroupElement = function(questionId, $element) {
            _groupElements[questionId] = $element;

            return $element;
        };

        /**
         * @param optionId
         * @param $element
         */
        this.writeOptionElement = function(optionId, $element) {
            _optionElements[optionId] = $element;

            return $element;
        };

        /**
         * @param questionId
         * @param $element
         */
        this.writeQuestionElement = function(questionId, $element) {
            _questionElements[questionId] = $element;

            return $element;
        };
    }

    /**
     * @type {Cache}
     * @private
     */
    var _cache = new Cache();

    /**
     * @type {Array}
     * @private
     */
    var _dependencies = [];

    /**
     * @type {Array}
     * @private
     */
    var _questions = [];

    /**
     * @type {QuestionResolver}
     * @private
     */
    const _this = this;

    /**
     * @param parentId
     * @param childId
     */
    this.addDependency = function (parentId, childId) {
        var dependency = _.find(_dependencies, {
            id: parentId
        });

        if (undefined === dependency) {
            dependency = {
                id: parentId,
                children: []
            };
        }

        if (_.indexOf(dependency.children, childId) > -1) {
            return;
        }

        dependency.children.push(childId);

        _.remove(_dependencies, function (test) {
            return test.id === parentId
        });

        _dependencies.push(dependency);
    };

    /**
     * @param question {Question}
     */
    this.calculate = function(question) {
        logger.log(sprintf('Calculating "%s" (%s)...', question.getTitle(), question.getType()));
        logger.indent();

        var result = question.getConditions().length === 0;

        if (question.getConditions().length === 0) {
            logger.debug('No conditions, results in true');
        }

        _.forEach(question.getConditions(), function (condition) {
            var parent = undefined;
            var conditionResult = undefined;

            /*
             * Conditie is gekoppeld aan een bepaalde vraag
             */
            if ('question' === condition.getType()) {

                var $input = _cache.getQuestionElement(condition.getReference());
                if ($input.length === 0) {
                    logger.warn('Question "' + condition.getReference() + '" could not be found, using default: ' + condition.getDefault());

                    conditionResult = condition.getDefault();
                }
                else {
                    parent = _this.get(condition.getQuestion());

                    if ('statement' === parent.getType()) {
                        conditionResult = $input.is(':checked');

                        logger.debug(sprintf('Statement "%s" = %s', parent.getTitle(), conditionResult));
                    }
                    else if ('' !== $input.val()) {
                        conditionResult = true;

                        logger.debug(sprintf('Condition question "%s" = %s', parent.getTitle(), conditionResult));
                    }
                }

            }

            /*
             * Conditie is gekoppeld aan een optie
             */
            else if ('option' === condition.getType()) {

                var $option = _cache.getOptionElement(condition.getReference());
                if ($option.length === 0) {
                    logger.warn('Option [' + condition.getReference() + '] could not be found, using default: ' + condition.getDefault());

                    conditionResult = condition.getDefault();
                }
                else {
                    conditionResult = $option.is(':checked');

                    logger.debug(sprintf('Condition option "%s" = %s', $(sprintf('label:has(input[value="%s"])', condition.getReference())).text().trim(), conditionResult));
                }
            }

            // Iets onbekends
            else {
                throw 'Unknown condition type "' + condition.getType() + '" while evaluating question [' + question.getId() + ']';
            }

            logger.indent();

            var lowerResult = true;
            if (conditionResult) {
                var lowerQuestion = _this.get(condition.getQuestion());

                if (undefined !== lowerQuestion) {
                    lowerResult = _this.calculate(lowerQuestion);
                }
            }

            logger.outdent();

            result = result || (lowerResult && conditionResult);
        });

        logger.outdent();
        logger.debug(' -> Result: ' + result);

        return result;
    };

    /**
     * @param question {Question}
     */
    this.evaluate = function (question) {
        _.forEach(_this.getDependants(question.getId()), function (child) {
            _this.evaluate(_this.get(child));
        });

        _this.toggle(question);
    };

    /**
     * @param questionId
     * @returns {Question|undefined}
     */
    this.get = function (questionId) {
        var match = _.find(_questions, {
            id: questionId
        });

        if (undefined === match) {
            return undefined;
        }

        return match.object;
    };

    /**
     * @param questionId
     * @returns {Array}
     */
    this.getDependants = function(questionId) {
        var match = _.find(_dependencies, {
            id: questionId
        });

        if (undefined === match) {
            return [];
        }

        return match.children;
    };

    /**
     * @returns {Question[]}
     */
    this.getAll = function () {
        var questions = [];
        _.forEach(_questions, function (question) {
            questions.push(question.object);
        });

        return questions;
    };

    /**
     * @param questionId     string
     * @param questionType   string
     * @param conditionsData string
     *
     * @returns {Question}
     */
    this.parse = function (questionId, questionType, conditionsData) {
        var existingQuestion = _this.get(questionId);
        var question = undefined === existingQuestion ? new Question(questionId, questionType) : existingQuestion;

        if (DEVELOPMENT) {
            var $label = $(sprintf('div.form-group:has([data-id="%s"]) > label', questionId));

            question.setTitle($label.text().trim());
        }

        if ('' !== conditionsData) {
            _.forEach(conditionsData.split(','), function (data) {
                var parts = data.split(':');

                var condition = new Condition(parts[0], parts[1]);
                condition.setDefault(parts[2]);

                if ('option' === parts[0]) {
                    condition.setQuestion(parts[3]);
                }

                // Conditie toevoegen
                question.addCondition(condition);

                // Afhankelijkheid toevoegen
                _this.addDependency(condition.getQuestion(), question.getId());
            });
        }

        _.remove(_questions, function (test) {
            return test.id === question.getId();
        });

        _questions.push({
            id: question.getId(),
            object: question
        });

        return question;
    };

    /**
     * @param question {Question}
     */
    this.toggle = function (question) {
        var visible = _this.calculate(question);

        var $group = _cache.getGroupElement(question.getId());
        const $question = _cache.getQuestionElement(question.getId());

        if (visible) {
            $group.show();
            $question.trigger('show');
        } else {
            $group.hide();
            $question.trigger('hide');
        }

        logger.debug(sprintf(' -> Resulting visibility is %s', visible));
    };
}

/*
 * Stap 1: vragenlijst
 */
var orderQuestionsStep = function () {
    var resolver = new QuestionResolver();

    const unloadConfirmation = function(){
        return 'Weet u zeker dat u deze pagina wilt verlaten?';
    };

    $(window).on('beforeunload', unloadConfirmation);

    $('form').submit(function () {
        $(window).off('beforeunload', unloadConfirmation);
    });

    /*
     * Help-icoontjes
     */
    $('[data-id][data-help!=""]').each(function () {
        var $question = $(this);

        var $label = $('> label', $question.parents('div.form-group'));

        var $help = $('<a href="#" class="help"><i class="flaticon solid question-2"></i></a>')
            .appendTo($label);

        $help.click(function () {
            vex.dialog.alert({
                className: 'vex-theme-default question-help',
                unsafeMessage: '<span class="title">' + _.escape($label.text()) + '</span><div class="vex-message-body">' + markdown.toHTML(_.replace($question.data('help'), new RegExp('\\\\n', 'g'), "\n")) + '</div>',
            });

            return false;
        });
    });

    /*
     * Input formatting
     */
    $('input[data-type="coc"]').each(function () {
        new Cleave($(this), {
            numeral: true,
            delimiter: '',
            numeralDecimalMark: ''
        });
    });

    $('input[data-type="date"]').each(function() {
        var $date = $(this).datepicker({
            format: 'dd-mm-yyyy',
            language: 'nl'
        });

        $date.on('changeDate', function () {
            $date.datepicker('hide');
        });
    });

    $('input[data-type="price"]').each(function() {
        new Cleave($(this), {
            numeral: true,
            delimiter: '.',
            numeralDecimalMark: ','
        });
    });

    /*
     * Conditionele vragen
     */
    const $questions = $('[data-id][data-conditions]');

    logger.log(sprintf('There are %d questions on this page', $questions.length));

    $questions.each(function () {
        var question = resolver.parse($(this).data('id'), $(this).data('type'), $(this).data('conditions'));

        _.forEach(question.getConditions(), function (condition) {

            // Conditie is gekoppeld aan een bepaalde vraag
            if ('question' === condition.getType()) {
                $('[data-id="' + condition.getReference() + '"][name]').bind('change keyup', function () {
                    resolver.evaluate(question);
                });
            }

            // Conditie is gekoppeld aan een optie
            else if ('option' === condition.getType()) {
                var $option = $('input[value="' + condition.getReference() + '"]');

                $('input[name="' + $option.attr('name') + '"]').change('change', function () {
                    resolver.evaluate(question);
                });
            }

            // Iets onbekends
            else {
                throw 'Unknown question type "' + condition.getType() + '" while doing initial evaluation for question [' + question.getId() + ']';
            }
        });

        resolver.evaluate(question);
    });
};

/**
 * Stap 2: aanmelden
 */
var orderAccountStep = function () {
    /*
     * if NL, BTW op 0 en veld verwijderen
     */
    $('select#order_account_customerAddress_country').change(function () {
        if ($('select#order_account_customerAddress_country option:selected').val() === 'NL') {
            $('#shift_vat_alert').hide();
        } else {
            $('#shift_vat_alert').show()
        }
    });

    $('select#order_account_customerAddress_country').change();
};

module.exports.index = function () {
    logger.log('Current status: ' + bridge.getVariable('status'));

    /*
     * Radio buttons
     */
    $('div.choices input[type="radio"]').change(function () {
        if ($(this).is(':checked')) {
            $('div.choices div.block').removeClass('selected');
            $(this).parents('div.block').addClass('selected');
        }
    }).change();

    $('div.choices div[role="radio"]').click(function () {
        $('div.choices input[type="radio"]').attr('checked', false);

        $('input[type="radio"]', $(this))
            .attr('checked', true)
            .change();

        return false;
    });

    /*
     * Beperkingen bij abonnementsvormen
     */
    $('div.choices input[type="radio"][name="order_options[type]"]').change(function () {
        var $checkbox = $('div.options input[name="order_options[custom]"]');
        var $block = $checkbox.parents('div.block');

        if ($(this).val() == 'subscription' && $(this).is(':checked')) {
            $checkbox.data('checked', $checkbox.is(':checked'))
                .data('restore', true);

            $checkbox.attr('disabled', true)
                .removeAttr('checked')
                .change();

            $block.addClass('disabled');
        } else if ($checkbox.data('restore')) {
            $block.removeClass('disabled');

            $checkbox.removeAttr('disabled')
                .attr('checked', $checkbox.data('checked'))
                .change();

            $checkbox.data('restore', false);
        }
    }).change();

    /*
     * Checkboxes
     */
    $('div.options input[type="checkbox"]').change(function () {
        var $block = $(this).parents('div.block');

        if ($(this).is(':checked')) {
            $block.addClass('selected');
        } else {
            $block.removeClass('selected');
        }
    }).change();

    $('div.options div[role="checkbox"] a').click(function (e) {
        e.stopPropagation();
    });

    $('div.options div[role="checkbox"]').click(function () {
        var $checkbox = $('input[type="checkbox"]', $(this));

        if ($checkbox.is(':disabled')) {
            return false;
        }

        if ($checkbox.is(':checked')) {
            $checkbox.removeAttr('checked');
        } else {
            $checkbox.attr('checked', true);
        }

        $checkbox.change();

        return false;
    });

    /*
     * JS specifiek voor de huidige stap
     */
    if ('step_questions' === bridge.getVariable('status')) {
        orderQuestionsStep();
    } else if ('step_account' === bridge.getVariable('status')) {
        orderAccountStep();
    }

    /*
     * JS voor KvK-koppeling
     */
    var $kvkSearch = $('select#order_account_customerCompanyName');

    $kvkSearch.select2({
        ajax: {
            url: "/kvk-controle",
            dataType: "json",
            delay: 250,
            data: function (params) {
                return {
                    q: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: data.items,
                };
            },
            cache: true,
        },
        tags: true,
        selectOnBlur: true,
        placeholder: 'Zoek uw bedrijfsnaam',
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 1,
        templateResult: formatResult,
        templateSelection: formatResultSelection
    });

    if ($kvkSearch.data('default')) {
        $kvkSearch.append(
            new Option($kvkSearch.data('default'), $kvkSearch.data('default'), false, false)
        );
    }

    function formatResult (result) {
        if (result.loading) {
            return "Bezig met laden...";
        }

        if (result.text && result.kvk) {
            return result.text + ' (' + result.kvk + ')';
        } else {
            return result.text + ' (mijn bedrijf wordt niet gevonden, handmatige invoer)';
        }
    }

    function formatResultSelection (result) {
        return result.text;
    }

    $kvkSearch.on('select2:select', function (e) {
        $.ajax('/kvk-detail', {
            data: {
                id: e.params.data.kvk,
                name: e.params.data.text,
            },
            success: function(data) {
                $('select#order_account_customerCompanyName').val(data.trade_name_full);
                $('input#order_account_customerCoc').val(data.dossier_number);
                $('input#order_account_customerAddressStreet').val(data.correspondence_street);
                $('input#order_account_customerAddressNumber').val(data.correspondence_house_number);
                $('input#order_account_customerAddressAddition').val(data.correspondence_house_number_addition && data.correspondence_house_number_addition[0]);
                $('input#order_account_customerAddressZipcode').val(data.establishment_postcode);
                $('input#order_account_customerAddressCity').val(data.correspondence_city);
            },
        });
    });
};
