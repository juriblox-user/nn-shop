const bridge = require('bridge')(__filename, module);

const $ = require('jquery');

module.exports.index = function () {
    /*
     * Checkboxes
     */
    $('div.options input[type="checkbox"]').change(function () {
        var $block = $(this).parents('div.option');

        if ($(this).is(':checked')) {
            $block.addClass('selected');
        } else {
            $block.removeClass('selected');
        }
    }).change();

    $('div.options div[role="checkbox"]').click(function () {
        var $checkbox = $('input[type="checkbox"]', $(this));

        if ($checkbox.is(':disabled')) {
            return false;
        }

        if ($checkbox.is(':checked')) {
            $checkbox.prop('checked', false);
        } else {
            $checkbox.prop('checked', true);
        }

        $checkbox.change();

        return false;
    });
};