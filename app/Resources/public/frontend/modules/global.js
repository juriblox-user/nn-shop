const $ = require('jquery');
const Cookies = require('js-cookie');

import ColoredText from '../lib/colored-text';

$().ready(function () {
    /*
     * Banner
     */
    if ($('header div.banner div[data-content]:visible').length == 0) {
        const $banner = $('header div.banner div[data-content="juriblox"]');

        if ($banner.length > 0) {
            let banner = Cookies.getJSON('juriblox-banner');
            if (undefined === banner) {
                banner = {
                    hide: false,
                    counter: 1
                };
            }

            if (banner.counter <= 5 && !banner.hide) {
                $('header').addClass('with-banner');
                $banner.show();

                banner.counter++;

                Cookies.set('juriblox-banner', banner, {
                    expires: 30
                });
            }

            $('a[data-role="hide-banner"]', $banner).click(function () {
                $('header').removeClass('with-banner');

                banner.hide = true;

                Cookies.set('banner', banner, {
                    expires: 365
                });

                return false;
            });
        }
    }

    /*
     * Gradients in tekst
     */
    $('[data-component="ColoredText"]').each(function () {
        new ColoredText($(this)[0]);
    });
});
