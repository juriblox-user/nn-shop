const bridge = require('bridge')(__filename, module);
const interval = require('interval');

const Clipboard = require('clipboard');

module.exports.index = function () {
    const coupon = new Clipboard('div.coupon');

    $('a.menu-button').on('click', function () {
        $('nav div.links').toggleClass('hide-mobile');
        return false;
    });

    coupon.on('success', function () {
        const $coupon = $('div.coupon');

        $coupon.addClass('copied');
        $('span.title', $coupon).text('✓ Gekopieerd naar klembord');

        setTimeout(function () {
            $coupon.removeClass('copied');
        }, 2000);
    });
};