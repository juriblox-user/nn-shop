const bridge = require('bridge')(__filename, module);
const interval = require('interval');

module.exports.index = function () {
    interval.every(function () {
        document.location = document.location + '?payment=' + bridge.getVariable('paymentId');
    }, 5000);
};