require('expose-loader?Application.Bridge!bridge');

// Modules
require('./modules/homepage/default.js');
require('./modules/landingpages/page.js');
require('./modules/order/default.js');
require('./modules/order/status.js');
require('./modules/payment/default.js');
require('./modules/referrers/details.js');
require('./modules/wizard/activities.js');

// Client-side JS
require('./modules/global.js');

// Afbeeldingen
require('../static/images/references/bite.jpg');
require('../static/images/references/frametales.jpg');
require('../static/images/references/genkgo.jpg');
require('../static/images/references/greenflash.jpg');
require('../static/images/references/indigowebstudio.jpg');
