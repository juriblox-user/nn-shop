require('expose-loader?Application.Bridge!bridge');
require('expose-loader?window.Tether!tether');

// Theme JS
require('./vendor/theme/js/bootstrap/alert.js');
require('./vendor/theme/js/bootstrap/button.js');
require('./vendor/theme/js/bootstrap/carousel.js');
require('./vendor/theme/js/bootstrap/collapse.js');
require('./vendor/theme/js/bootstrap/dropdown.js');
require('./vendor/theme/js/bootstrap/modal.js');
require('./vendor/theme/js/bootstrap/tooltip.js');
require('./vendor/theme/js/bootstrap/popover.js');
require('./vendor/theme/js/bootstrap/scrollspy.js');
require('./vendor/theme/js/bootstrap/tab.js');

require('./vendor/theme/js/custom/affix.js');
require('./vendor/theme/js/custom/datepicker.js');

// Modules
require('./modules/global.js');
require('./modules/financial/discounts.js');
require('./modules/financial/invoices.js');
require('./modules/settings/partners.js');
require('./modules/settings/referrers.js');
require('./modules/settings/shops.js');
require('./modules/templates/edit.js');
require('./modules/templates/publish.js');
require('./modules/pages/page');
require('./modules/customers/default');
require('./modules/orders/default');

// SCSS
require('./sass/main.scss');
