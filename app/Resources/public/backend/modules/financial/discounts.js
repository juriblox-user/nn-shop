const bridge = require('bridge')(__filename, module);

const $ = require('jquery');

/**
 * Inputs voor korting tonen/verbergen
 *
 * @private
 */
const _toggleDiscountInputs = function () {
    $('input[name="type"]').change(function () {
        if ('fixed' === $('input[name="type"]:checked').val()) {
            $('div.form-group:has(input#amount)').show();
            $('div.form-group:has(input#percentage)').hide();
        } else {
            $('div.form-group:has(input#amount)').hide();
            $('div.form-group:has(input#percentage)').show();
        }
    }).change();
};

/**
 * Delete popup handler.
 * @private
 */
const _popup = function () {
    $('#confirm-delete').on('show.bs.modal', function(e) {

        $(this).find('.btn-ok').on("click", function() {
            $.redirect($(e.relatedTarget).data('href'));
        });

        $(this).find('.modal-header').html($(e.relatedTarget).data('document'));
    });
};

/**
 * Template handler.
 * @private
 */
const _toggleTemplates = function () {
    let handleChangeEvent = function (target) {
        let $templates = $('.form-templates');
        if ($(target).is(':checked')) {
            $templates.show();
            return;
        }

        $templates.hide();
    };

    let hasTemplateSelector = '.form-has-templates input';

    $(hasTemplateSelector).on('change', function (e) {
        handleChangeEvent(e.target);
    });

    handleChangeEvent(hasTemplateSelector);
};

/**
 * Template handler.
 * @private
 */
const _toggleEmails = function () {
    let handleChangeEvent = function (target) {
        let $templates = $('.form-emails');
        if ($(target).is(':checked')) {
            $templates.show();
            return;
        }

        $templates.hide();
    };

    let hasTemplateSelector = '.form-has-emails input';

    $(hasTemplateSelector).on('change', function (e) {
        handleChangeEvent(e.target);
    });

    handleChangeEvent(hasTemplateSelector);
};

module.exports.create = function () {
    _toggleDiscountInputs();
    _toggleTemplates();
    _toggleEmails();
};

module.exports.edit = function () {
    _toggleDiscountInputs();
    _popup();
    _toggleTemplates();
    _toggleEmails();
};