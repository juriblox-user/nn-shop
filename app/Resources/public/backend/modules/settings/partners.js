const bridge = require('bridge')(__filename, module);

const $ = require('jquery');

/**
 * Inputs met client ID en client key tonen/verbergen
 *
 * @private
 */
var _toggleSyncInputs = function () {
    $('input#sync_enabled').change(function () {
        var $groups = $('div.form-group:has(input#sync_client_id), div.form-group:has(input#sync_client_key)');

        if ($(this).is(':checked')) {
            $groups.show();
        } else {
            $groups.hide();
        }
    }).change();
};

module.exports.create = function () {
    _toggleSyncInputs();
};

module.exports.edit = function () {
    _toggleSyncInputs();
};