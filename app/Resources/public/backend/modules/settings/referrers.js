const bridge = require('bridge')(__filename, module);

const $ = require('jquery');

/**
 * Inputs voor landingspagina's tonen/verbergen
 *
 * @private
 */
const _toggleLandingInputs = function () {
    $('input#landing').change(function () {
        const $groups = $('div[data-linked-input="landing"]');

        if ($(this).is(':checked')) {
            $groups.show();
        } else {
            $groups.hide();
        }
    }).change();
};

module.exports.create = function () {
    _toggleLandingInputs();
};

module.exports.edit = function () {
    _toggleLandingInputs();
};