const bridge = require('bridge')(__filename, module);

const $ = require('jquery');

const Editor = require('simplemde');

/**
 * @private
 */
var _updateHostname = function () {
    $('input#hostname').keyup(function () {
        $('var[data-id="hostname"]').text($(this).val() ? $(this).val() : 'domein.nl');
    }).keyup();
};

module.exports.create = function () {
    _updateHostname();
};

module.exports.edit = function () {
    _updateHostname();

    $('input#hostname').keyup(function () {
        if ($(this).val() != bridge.getVariable('hostname')) {
            $('div[data-role="dns-results"]').hide();
        } else {
            $('div[data-role="dns-results"]').show();
        }
    });

    $('.editable').each(function () {
        new Editor({ element: this });
    })

};