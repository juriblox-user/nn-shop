const $ = require('jquery');

$('table[data-role="link-categories"]').each(function () {
    var $table = $(this);

    var updateInputs = function ($trigger) {
        var $selected = $('input[type="checkbox"]', $trigger.parents('tr'));
        var $primary = $('input[type="radio"]', $trigger.parents('tr'));

        if ($selected.is(':checked')) {
            $primary.attr('disabled', false);
        } else {
            $primary.attr('disabled', true);

            if ($primary.is(':checked')) {
                $primary.attr('checked', false);
            }
        }
    };

    $('input', $table).change(function () {
        updateInputs($(this));
    });

    $('tr td input', $table).each(function () {
        updateInputs($(this));
    });
});