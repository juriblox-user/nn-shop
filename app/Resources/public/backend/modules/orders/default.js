const bridge = require('bridge')(__filename, module);

const $ = require('jquery');
require('bootstrap-datepicker');
require('bootstrap-datepicker/js/locales/bootstrap-datepicker.nl.js');

var _search = function () {
    var searchBlock = $('.search-block');
    searchBlock.hide();

    if ($('.clicked').length) {
        searchBlock.show();
    }

    $('.show-search-button').on('click', function(e) {
        if (searchBlock.attr('style') === 'display: none;') {
            searchBlock.show();
        } else {
            searchBlock.hide();
        }
    });

    // Reset button handler
    $('.reset-button').on('click', function(){
        $('.search-form').find('input').val('');
        $('.submit-search-button').click();
    })
};

module.exports.index = function() {
    _search();
};