const bridge = require('bridge')(__filename, module);

const $ = require('jquery');

module.exports.index = function () {
    $('input#subscription').change(function () {
        var $group = $('div.input-group:has(input#monthly_price)');

        if ($(this).is(':checked')) {
            $group.show();
        } else {
            $group.hide();
        }
    }).change();
};