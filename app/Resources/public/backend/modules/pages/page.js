const bridge = require('bridge')(__filename, module);

const $ = require('jquery');

const Editor = require('simplemde');

const Redirect = require('jquery.redirect');

/**
 * Enable markdown for content
 *
 * @private
 */
var _editor = function () {
    $('.editable').each(function () {
        var pageEditor = new Editor({ element: this });
    })
};

var _popup = function () {
    $('#confirm-delete').on('show.bs.modal', function(e) {

        $(this).find('.btn-ok').on("click", function() {
            $.redirect($(e.relatedTarget).data('href'));
        });

        $(this).find('.modal-header').html($(e.relatedTarget).data('article'));
    });
}

module.exports.create = function () {
    _editor();
};

module.exports.edit = function () {
    _editor();
};

module.exports.index = function() {
    _popup();
}