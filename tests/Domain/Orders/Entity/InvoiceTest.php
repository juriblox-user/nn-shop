<?php

namespace Tests\Core\Infrastructure\CoreBundle\Controller;

use Carbon\CarbonInterval;
use Core\Domain\Value\Web\EmailAddress;
use Money\Currency;
use Money\Money;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Entity\Discount;
use NnShop\Domain\Orders\Entity\Invoice;
use NnShop\Domain\Orders\Entity\InvoiceItem;
use NnShop\Domain\Orders\Enumeration\CustomerType;
use NnShop\Domain\Orders\Enumeration\DiscountType;
use NnShop\Domain\Orders\Enumeration\OrderItemType;
use NnShop\Domain\Orders\Value\CustomerId;
use NnShop\Domain\Orders\Value\DiscountId;
use NnShop\Domain\Orders\Value\InvoiceCode;
use NnShop\Domain\Orders\Value\InvoiceId;
use NnShop\Domain\Orders\Value\InvoiceItemId;
use NnShop\Domain\Translation\Entity\Locale;
use NnShop\Domain\Translation\Entity\Profile;
use NnShop\Domain\Translation\Value\LocaleId;
use NnShop\Domain\Translation\Value\ProfileId;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class InvoiceTest extends KernelTestCase
{
    private function makeInvoiceEntity()
    {
        $locale = Locale::create(LocaleId::generate());

        $profile = Profile::create(ProfileId::generate(), $locale);

        $customer = Customer::create(
            CustomerId::generate(),
            CustomerType::fromName(CustomerType::BUSINESS),
            EmailAddress::from('test@test.nl'),
            $profile,
            $locale
        );

        return Invoice::issue(
            InvoiceId::generate(),
            InvoiceCode::generate(date('Y'), 1),
            $customer,
            CarbonInterval::days(14),
            $profile
        );
    }

    private function makeDiscountEntity(int $percentage = 25)
    {
        $discount = Discount::create(DiscountId::generate(), 'CODE', DiscountType::fromName(DiscountType::PERCENTAGE));
        $discount->setPercentage($percentage);

        return $discount;
    }

    /** @test */
    public function it_can_create_a_credit_invoice()
    {
        $invoice = $this->makeInvoiceEntity();

        $item = InvoiceItem::create(InvoiceItemId::generate(), $invoice, 2, new Money(100, new Currency('EUR')));
        $item->setTitle("Dummy Item");
        $item->setType(OrderItemType::fromName(OrderItemType::DOCUMENT));

        $invoice->recalculate();

        $this->assertEquals(200, $invoice->getItemsTotal()->getAmount());
        $this->assertEquals(242, $invoice->getTotal()->getAmount());

        $creditInvoice = $invoice->credit(InvoiceId::generate(), InvoiceCode::generate(date('Y'), 2));

        $this->assertEquals(-200, $creditInvoice->getItemsTotal()->getAmount());
        $this->assertEquals(-242, $creditInvoice->getTotal()->getAmount());
    }

    /** @test */
    public function it_can_create_a_credit_invoice_with_both_free_and_paid_items()
    {
        $invoice = $this->makeInvoiceEntity();

        $item = InvoiceItem::create(InvoiceItemId::generate(), $invoice, 1, new Money(0, new Currency('EUR')));
        $item->setTitle("Free Item");
        $item->setType(OrderItemType::fromName(OrderItemType::DOCUMENT));

        $item = InvoiceItem::create(InvoiceItemId::generate(), $invoice, 1, new Money(100, new Currency('EUR')));
        $item->setTitle("Paid Item");
        $item->setType(OrderItemType::fromName(OrderItemType::DOCUMENT));

        $invoice->recalculate();

        $this->assertEquals(100, $invoice->getItemsTotal()->getAmount());
        $this->assertEquals(121, $invoice->getTotal()->getAmount());

        $creditInvoice = $invoice->credit(InvoiceId::generate(), InvoiceCode::generate(date('Y'), 2));

        $this->assertEquals(-100, $creditInvoice->getItemsTotal()->getAmount());
        $this->assertEquals(-121, $creditInvoice->getTotal()->getAmount());
    }

    /** @test */
    public function it_can_create_a_credit_invoice_of_a_discounted_invoice()
    {
        $invoice  = $this->makeInvoiceEntity();
        $discount = $this->makeDiscountEntity();

        $item = InvoiceItem::create(InvoiceItemId::generate(), $invoice, 4, new Money(100, new Currency('EUR')));
        $item->setTitle("Paid Item");
        $item->setType(OrderItemType::fromName(OrderItemType::DOCUMENT));

        $invoice->applyDiscount($discount);

        $this->assertEquals(400, $invoice->getItemsTotal()->getAmount());
        $this->assertEquals(363, $invoice->getTotal()->getAmount());

        $creditInvoice = $invoice->credit(InvoiceId::generate(), InvoiceCode::generate(date('Y'), 2));

        $this->assertEquals(-400, $creditInvoice->getItemsTotal()->getAmount());
        $this->assertEquals(-363, $creditInvoice->getTotal()->getAmount());
    }

    /** @test */
    public function it_can_create_a_credit_invoice_of_an_invoice_with_additional_services()
    {
        $invoice = $this->makeInvoiceEntity();

        $item = InvoiceItem::create(InvoiceItemId::generate(), $invoice, 1, new Money(400, new Currency('EUR')));
        $item->setTitle("Document Item");
        $item->setType(OrderItemType::fromName(OrderItemType::DOCUMENT));

        $item = InvoiceItem::create(InvoiceItemId::generate(), $invoice, 1, new Money(200, new Currency('EUR')));
        $item->setTitle("Additional Service Item");
        $item->setType(OrderItemType::fromName(OrderItemType::ADDITIONAL_SERVICE));

        $invoice->recalculate();

        $this->assertEquals(400, $invoice->getItemsTotal()->getAmount());
        $this->assertEquals(726, $invoice->getTotal()->getAmount());    // 600 * 1.21

        $creditInvoice = $invoice->credit(InvoiceId::generate(), InvoiceCode::generate(date('Y'), 2));

        $this->assertEquals(-400, $creditInvoice->getItemsTotal()->getAmount());
        $this->assertEquals(-726, $creditInvoice->getTotal()->getAmount());
    }
}
