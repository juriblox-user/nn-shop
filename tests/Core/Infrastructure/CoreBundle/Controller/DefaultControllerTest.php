<?php

namespace Tests\Core\Infrastructure\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 */
class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        //$this->assertContains('Hello World', $client->getResponse()->getContent());
    }
}
