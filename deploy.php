<?php

namespace Deployer;

use Symfony\Component\Console\Input\InputOption;

require 'recipe/common.php';

// --force option toevoegen
option('force', null, InputOption::VALUE_NONE, 'Force this deploy (ignores exceptions related to the current environment)');

// SSH config
set('ssh_type', 'native');
set('ssh_multiplexing', true);

// Maximaal 3 releases behouden
set('keep_releases', 3);

// Staging
host('web01.juridox.nl/staging')
    ->stage('staging')

    ->user('webdev')
    ->forwardAgent()

    ->set('branch', 'develop')
    ->set('deploy_path', '/data/www/sites/docs.legalfit.eu/staging');

// Productieomgeving
host('web01.juridox.nl/production')
    ->stage('production')

    ->user('webdev')
    ->forwardAgent()

    ->set('branch', 'master')
    ->set('deploy_path', '/data/www/sites/docs.legalfit.eu/production');

set('repository', 'git@github.com:juriblox/nn-shop.git');

set('cache_path', '{{deploy_path}}/cache');
set('current_path', '{{deploy_path}}/current');
set('shared_path', '{{deploy_path}}/shared');

set('shared_dirs', [
    'var/documents',
    'var/invoices',
    'var/logs',
    'var/samples',
    'web/var/images',
    'web/var/blog',
    'web/var/pages',
    'web/var/previews'
]);

set('shared_files', [
    '.access.yml',
    '.env',
    'app/config/parameters.yml',
]);

set('workers', [
    'nane-shop-2020-{{stage}}-commands',
    'nane-shop-2020-{{stage}}-events',
    'nane-shop-2020-{{stage}}-tasks',
]);

set('writable_shared_dirs', [
    'var/documents',
    'var/invoices',
    'var/logs',
    'var/samples',
    'web/var/images',
    'web/var/blog',
    'web/var/pages',
    'web/var/previews',
]);

set('writable_owned_dirs', [
    'var/cache',
]);

/*
 * Bugsnag informeren over de deploy.
 */
task('app:notify-bugsnag', function () {
    run('php {{release_path}}/bin/console bugsnag:notify-deploy {{repository}} --no-debug --no-interaction');
})->desc('Notify Bugsnag about the new release');

/*
 * Take the application down for maintenance.
 */
task('app:maintenance:down', function () {
    try {
        run('if [ -f {{current_path}}/bin/console ]; then php {{current_path}}/bin/console app:maintenance:down --no-interaction; fi');
    } catch (\RuntimeException $exception) {
        if (!input()->getOption('force')) {
            throw $exception;
        }

        writeln('    <error>IGNORING</error> ' . str_replace("\n", ' ', $exception->getMessage()));
    }
})->desc('Take the application down for maintenance');

/*
 * Bring application back up after maintenance.
 */
task('app:maintenance:up', function () {
    run('php {{release_path}}/bin/console app:maintenance:up --no-interaction');
})->desc('Bring application back up after maintenance');

/*
 * Setup.
 */
task('app:setup', function () {
    run('php {{release_path}}/bin/console app:setup:generate-secret --no-interaction');
})->desc('Setup the application');

/*
 * Build assets.
 */
task('assets:build', function () {
    cd('{{release_path}}');
    run('npm run build');
})->desc('Build process');

/*
 * Build info genereren.
 */
task('assets:build-info', function () {
    cd('{{release_path}}');

    run('npm run build-info:write');
})->desc('Generate .build file with build info');

/*
 * Overbodige bestanden naar assets build opruimen.
 */
task('assets:cleanup', function () {
    cd('{{release_path}}');
    run('rm -rf node_modules');
})->desc('Build process');

/*
 * Assets-omgeving voorbereiden
 */
task('assets:prepare', function () {
    if (version_compare(run('npm -v'), '2.15.0') < 0) {
        throw new \RuntimeException('At least npm 3.5.0 is required for building the assets');
    }

    cd('{{release_path}}');
    run('yarn install');
})->desc('Prepare assets build environment');

/*
 * Database migratie.
 */
task('database:migrate', function () {
    run('php {{release_path}}/bin/console doctrine:migrations:migrate --no-debug --no-interaction');
})->desc('Migrate database');

/*
 * Cache legen en opwarmen.
 */
task('deploy:cache', function () {
    run('php {{release_path}}/bin/console cache:clear --no-warmup --no-debug --no-interaction');
    run('php {{release_path}}/bin/console cache:warmup --no-debug --no-interaction');
})->desc('Clear and then warm up the cache');

/*
 * Overbodige entrypoints verwijderen.
 */
task('deploy:cleanup:entrypoints', function () {
    if ('staging' !== input()->getArgument('stage')) {
        run('rm -f {{release_path}}/web/config.php');
    }
})->setPrivate();

after('deploy:update_code', 'deploy:cleanup:entrypoints');

/*
 *
 */
task('deploy:shared:prepare', function () {
    $templates = [
        '.env.{{stage}}' => '.env',
    ];

    foreach ($templates as $source => $file) {
        $file = '{{deploy_path}}/shared/' . $file;
        $source = '{{release_path}}/' . str_replace('{{stage}}', input()->getArgument('stage'), $source);

        run(sprintf('if [ ! -d %1$s ]; then mkdir -p %1$s; fi', dirname($file)));
        run(sprintf('if [ ! -f %1$s ]; then cp %2$s %1$s; fi', $file, $source));
    }

    foreach (get('shared_dirs') as $dir) {
        run(sprintf('if [ ! -d {{deploy_path}}/shared/%1$s ]; then mkdir -p {{deploy_path}}/shared/%1$s; fi', $dir));
        run(sprintf('if [ -d {{release_path}}/%1$s ]; then cp -R -T {{release_path}}/%1$s {{deploy_path}}/shared/%1$s && rm -rf {{release_path}}/%1$s; fi', $dir));
    }
})->desc('Prepare shared files (ensure their existance)');

/*
 *
 */
task('deploy:writable:owned', function () {
    foreach (get('writable_owned_dirs') as $dir) {
        $dir = '{{release_path}}/' . rtrim($dir, '/');

        run(sprintf('sudo chown -R webdev:www-data %s', trim($dir, '/')));

        run(sprintf('setfacl -R -m u:www-data:rwX -m u:webdev:rwX %s', trim($dir, '/')));
        run(sprintf('setfacl -dR -m u:www-data:rwX -m u:webdev:rwX %s', trim($dir, '/')));
    }
})->desc('Make shared directories writable');

/*
 *
 */
task('deploy:writable:shared', function () {
    foreach (get('writable_shared_dirs') as $dir) {
        $dir = '{{shared_path}}/' . rtrim($dir, '/');

        run(sprintf('sudo chown -R webdev:www-data %s', trim($dir, '/')));

        run(sprintf('setfacl -R -m u:www-data:rwX -m u:webdev:rwX %s', trim($dir, '/')));
        run(sprintf('setfacl -dR -m u:www-data:rwX -m u:webdev:rwX %s', trim($dir, '/')));
    }
})->desc('Make shared directories writable');

/*
 *
 */
task('workers:start', function () {
    foreach (get('workers') as $process) {
        $process = str_replace('{{stage}}', input()->getArgument('stage'), $process);

        writeln('    - starting <comment>' . $process . '</comment>...');
        run('supervisorctl start ' . $process);
    }
});

/*
 *
 */
task('workers:stop', function () {
    foreach (get('workers') as $process) {
        $process = str_replace('{{stage}}', input()->getArgument('stage'), $process);

        writeln('    - stopping <comment>' . $process . '</comment>...');
        run('supervisorctl stop ' . $process);
    }
});

/*
 * Main task.
 */
task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared:prepare',
    'deploy:shared',
    'deploy:writable:owned',
    'deploy:writable:shared',
    'deploy:vendors',

    'assets:prepare',
    'assets:build-info',

    'assets:build',
    'assets:cleanup',

    'workers:stop',
    'app:maintenance:down',

    'app:setup',
    'database:migrate',

    'deploy:cache',
    'deploy:symlink',

    'app:notify-bugsnag',
    'app:maintenance:up',
    'workers:start',

    'cleanup',
])->desc('Deploy application');
