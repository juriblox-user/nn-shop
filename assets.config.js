const environments = require('gulp-environments');

module.exports = {
    copy: [
        { from: './app/Resources/public/static/favicons', to: 'static/favicons' }
    ],

    entrypoints: {
        backend: './app/Resources/public/backend',
        frontend: './app/Resources/public/frontend',
        hosted: './app/Resources/public/hosted',
        shared: './app/Resources/public/shared',

        vendor: [
            'bridge',
            'jquery',
            'lodash'
        ]
    },

    keep: {
        images: './app/Resources/public/static'
    },

    manifest: {
        root: 'app/Resources/public'
    },

    output: {
        root: 'app/Resources',

        buildUrl: '$ASSETS_BASE_URL',
        serverUrl: '$ASSETS_BASE_URL'
    }
};

if (environments.production()) {
    process.noDeprecation = true;
}
