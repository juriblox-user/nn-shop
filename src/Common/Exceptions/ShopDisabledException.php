<?php

namespace NnShop\Common\Exceptions;

class ShopDisabledException extends ShopContextException
{
}
