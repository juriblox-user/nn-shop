<?php

namespace NnShop\Common\Exceptions\Security;

use Core\Common\Exception\SecurityException;

class MagicUrlSecurityException extends SecurityException
{
}
