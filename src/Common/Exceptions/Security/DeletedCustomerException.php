<?php

namespace NnShop\Common\Exceptions\Security;

use Symfony\Component\Security\Core\Exception\AccountStatusException;

class DeletedCustomerException extends AccountStatusException
{
    /**
     * {@inheritdoc}
     */
    public function getMessageKey()
    {
        return 'deleted_customer';
    }
}
