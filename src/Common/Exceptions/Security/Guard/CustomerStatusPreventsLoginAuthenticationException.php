<?php

namespace NnShop\Common\Exceptions\Security\Guard;

use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;

class CustomerStatusPreventsLoginAuthenticationException extends CustomUserMessageAuthenticationException
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct('U kunt zich op dit moment nog niet aanmelden. Controleer of u eerder een e-mail met instructies van ons heeft ontvangen, of neem contact met ons op voor assistentie.');
    }
}
