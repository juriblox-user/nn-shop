<?php

namespace NnShop\Common\Exceptions\Security\Guard;

use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;

class CustomerHasPendingInvitationAuthenticationException extends CustomUserMessageAuthenticationException
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct('U kunt nog niet worden aangemeld omdat u uw accountregistratie nog niet heeft afgerond. Volg de stappen in de welkom e-mail die u heeft ontvangen om verder te gaan.');
    }
}
