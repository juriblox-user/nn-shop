<?php

namespace NnShop\Common\Exceptions\Security\Guard;

use NnShop\Common\Exceptions\Security\Reason\Reason;
use NnShop\Common\Exceptions\Security\Reason\ReasonAwareExceptionInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;

class InvalidPasswordAuthenticationException extends CustomUserMessageAuthenticationException implements ReasonAwareExceptionInterface
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct('Het wachtwoord dat u heeft opgegeven is onjuist. Probeer nogmaals, alstublieft.');
    }

    /**
     * {@inheritdoc}
     */
    public function getReason(): Reason
    {
        return new Reason(Reason::SESSION_INVALID_PASSWORD);
    }
}
