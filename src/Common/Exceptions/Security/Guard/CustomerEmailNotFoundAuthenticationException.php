<?php

namespace NnShop\Common\Exceptions\Security\Guard;

use NnShop\Common\Exceptions\Security\Reason\Reason;
use NnShop\Common\Exceptions\Security\Reason\ReasonAwareExceptionInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;

class CustomerEmailNotFoundAuthenticationException extends CustomUserMessageAuthenticationException implements ReasonAwareExceptionInterface
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct('Er kon helaas geen account met dit e-mailadres worden gevonden.');
    }

    /**
     * {@inheritdoc}
     */
    public function getReason(): Reason
    {
        return new Reason(Reason::SESSION_EMAIL_NOT_FOUND);
    }
}
