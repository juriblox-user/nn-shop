<?php

namespace NnShop\Common\Exceptions\Security;

use Symfony\Component\Security\Core\Exception\AuthenticationException;

class ExpiredMagicLinkException extends AuthenticationException
{
    /**
     * {@inheritdoc}
     */
    public function getMessageKey()
    {
        return 'expired_magic_link';
    }
}
