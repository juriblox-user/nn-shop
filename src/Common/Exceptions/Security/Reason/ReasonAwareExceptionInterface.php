<?php

namespace NnShop\Common\Exceptions\Security\Reason;

interface ReasonAwareExceptionInterface
{
    public function getReason(): Reason;
}
