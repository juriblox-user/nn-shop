<?php

namespace NnShop\Common\Exceptions\Security\Reason;

use Core\Domain\Enumeration\AbstractEnumeration;

class Reason extends AbstractEnumeration
{
    const SESSION_EMAIL_NOT_FOUND = 'SESSION_EMAIL_NOT_FOUND';
    const SESSION_INVALID_PASSWORD = 'SESSION_INVALID_PASSWORD';
}
