<?php

namespace NnShop\Common\Exceptions;

use Core\Common\Exception\DataIntegrityException;

class CorruptRemoteDataException extends DataIntegrityException
{
}
