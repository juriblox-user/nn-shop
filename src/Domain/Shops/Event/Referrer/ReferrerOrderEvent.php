<?php

namespace NnShop\Domain\Shops\Event\Referrer;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Orders\Value\OrderId;
use NnShop\Domain\Shops\Value\ReferrerId;

class ReferrerOrderEvent
{
    /**
     * @Serializer\Type("NnShop\Domain\Orders\Value\OrderId")
     *
     * @var OrderId
     */
    private $orderId;

    /**
     * @Serializer\Type("NnShop\Domain\Shops\Value\ReferrerId")
     *
     * @var ReferrerId
     */
    private $referrerId;

    /**
     * Constructor.
     *
     * @param ReferrerId $referrerId
     * @param OrderId    $orderId
     */
    private function __construct(ReferrerId $referrerId, OrderId $orderId)
    {
        $this->referrerId = $referrerId;
        $this->orderId = $orderId;
    }

    /**
     * @param ReferrerId $referrerId
     * @param OrderId    $orderId
     *
     * @return static
     */
    public static function create(ReferrerId $referrerId, OrderId $orderId)
    {
        return new static($referrerId, $orderId);
    }

    /**
     * @return OrderId
     */
    public function getOrderId(): OrderId
    {
        return $this->orderId;
    }

    /**
     * @return ReferrerId
     */
    public function getReferrerId(): ReferrerId
    {
        return $this->referrerId;
    }
}
