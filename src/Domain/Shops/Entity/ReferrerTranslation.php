<?php

namespace NnShop\Domain\Shops\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;

/**
 * @ORM\Entity(repositoryClass="Gedmo\Translatable\Entity\Repository\TranslationRepository")
 * @ORM\Table(name="shops_referrer_translations",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_idx", columns={
 *         "translation_locale", "object_id", "translation_field"
 *     })}
 * )
 */
class ReferrerTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="Referrer", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="referrer_id", onDelete="CASCADE")
     */
    protected $object;

    /**
     * PageTranslation constructor.
     *
     * @param string $locale
     * @param string $field
     * @param string $value
     */
    public function __construct(string $locale, string $field, string $value)
    {
        $this->setLocale($locale);
        $this->setField($field);
        $this->setContent($content);
    }
}
