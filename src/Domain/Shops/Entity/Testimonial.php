<?php

namespace NnShop\Domain\Shops\Entity;

use Doctrine\ORM\Mapping as ORM;
use NnShop\Domain\Shops\Value\TestimonialId;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Shops\Repository\TestimonialDoctrineRepository")
 */
class Testimonial
{
    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $company;

    /**
     * @ORM\Id
     * @ORM\Column(type="shops.testimonial_id")
     *
     * @var TestimonialId
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     *
     * @var string
     */
    private $quote;

    /**
     * Testimonial constructor.
     *
     * @param TestimonialId $id
     */
    private function __construct(TestimonialId $id)
    {
        $this->id = $id;
    }

    /**
     * @param TestimonialId $id
     * @param string        $name
     * @param string        $company
     * @param string        $quote
     *
     * @return Testimonial
     */
    public static function create(TestimonialId $id, string $name, string $company, string $quote): self
    {
        $testimonial = new self($id);
        $testimonial->setName($name);
        $testimonial->setCompany($company);
        $testimonial->setQuote($quote);

        return $testimonial;
    }

    /**
     * @return string
     */
    public function getCompany(): string
    {
        return $this->company;
    }

    /**
     * @return TestimonialId
     */
    public function getId(): TestimonialId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getQuote(): string
    {
        return $this->quote;
    }

    /**
     * @param string $company
     */
    public function setCompany(string $company)
    {
        $this->company = $company;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param string $quote
     */
    public function setQuote(string $quote)
    {
        $this->quote = $quote;
    }
}
