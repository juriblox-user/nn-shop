<?php

namespace NnShop\Domain\Shops\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;

/**
 * @ORM\Entity(repositoryClass="Gedmo\Translatable\Entity\Repository\TranslationRepository")
 * @ORM\Table(name="shops_shop_translations",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_idx", columns={
 *         "translation_locale", "object_id", "translation_field"
 *     })}
 * )
 */
class ShopTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="shop_id", onDelete="CASCADE")
     */
    protected $object;

    /**
     * ShopTranslation constructor.
     *
     * @param string $locale
     * @param string $field
     * @param string $value
     */
    public function __construct(string $locale, string $field, string $value)
    {
        $this->setLocale($locale);
        $this->setField($field);
        $this->setContent($value);
    }
}
