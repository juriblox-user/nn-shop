<?php

namespace NnShop\Domain\Shops\Entity;

use Core\Annotation\Doctrine as Core;
use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\Domain\Traits\SoftDeleteableEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Comparable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use NnShop\Domain\Orders\Entity\Discount;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Shops\Value\ReferrerId;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Translation\Entity\Profile;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Shops\Repository\ReferrerDoctrineRepository")
 * @Core\QueryBuilder(class="NnShop\Infrastructure\Shops\QueryBuilder\ReferrerDoctrineQueryBuilder")
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(name="ix_deleted", columns={"timestamp_deleted"}),
 *     @ORM\Index(name="ix_homepage", columns={"referrer_homepage", "timestamp_deleted"})
 * })
 *
 * @Gedmo\SoftDeleteable(fieldName="timestampDeleted")
 * @Gedmo\TranslationEntity(class="NnShop\Domain\Shops\Entity\ReferrerTranslation")
 */
class Referrer implements Comparable
{
    use SoftDeleteableEntityTrait;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Orders\Entity\Discount", inversedBy="referrers")
     * @ORM\JoinColumn(referencedColumnName="discount_id", nullable=true)
     *
     * @var Discount
     */
    private $discount;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $homepage;

    /**
     * @ORM\Id
     * @ORM\Column(type="shops.referrer_id")
     *
     * @var ReferrerId
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $kickback;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $landing;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     *
     * @var string
     */
    private $logo;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\Order", mappedBy="referrer", fetch="EXTRA_LAZY")
     *
     * @var Collection|Order[]
     */
    private $orders;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     * @Gedmo\Slug(fields={"title"})
     *
     * @var string
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Shops\Entity\ReferrerTemplate", mappedBy="referrer", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @var Collection|ReferrerTemplate[]
     */
    private $templateLinks;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Translatable
     *
     * @var string
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=100)
     * @Gedmo\Translatable
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Gedmo\Translatable
     *
     * @var string
     */
    private $website;

    /**
     * @var Profile
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Translation\Entity\Profile", inversedBy="referrers")
     * @ORM\JoinColumn(name="profile_id", referencedColumnName="profile_id", onDelete="SET NULL")
     */
    private $profile;

    /**
     * @ORM\OneToMany(
     *     targetEntity="ReferrerTranslation",
     *     mappedBy="object",
     *     cascade={"persist", "remove"}
     * )
     */
    private $translations;

    /**
     * Referrer constructor.
     *
     * @param ReferrerId $id
     * @param Profile    $profile
     */
    private function __construct(ReferrerId $id, Profile $profile)
    {
        $this->id = $id;
        $this->profile = $profile;

        $this->homepage = false;
        $this->kickback = false;
        $this->landing = false;

        $this->profile->addReferrer($this);

        $this->orders = new ArrayCollection();
        $this->templateLinks = new ArrayCollection();
        $this->translations = new ArrayCollection();
    }

    /**
     * @param ReferrerId $id
     * @param string     $title
     * @param Profile    $profile
     *
     * @return Referrer
     */
    public static function create(ReferrerId $id, string $title, Profile $profile): self
    {
        $referrer = new self($id, $profile);
        $referrer->title = $title;

        return $referrer;
    }

    /**
     * @param Order $order
     */
    public function addOrder(Order $order)
    {
        if ($this->orders->contains($order)) {
            throw new DuplicateEntityException();
        }

        $this->orders->add($order);
    }

    /**
     * @param Template $template
     *
     * @return ReferrerTemplate
     */
    public function addTemplate(Template $template): ReferrerTemplate
    {
        return ReferrerTemplate::create($this, $template);
    }

    /**
     * @param ReferrerTemplate $link
     */
    public function addTemplateLink(ReferrerTemplate $link)
    {
        if ($this->hasTemplate($link->getTemplate())) {
            return;
        }

        $this->templateLinks->add($link);
    }

    /**
     * {@inheritdoc}
     *
     * @param $other Referrer
     */
    public function compareTo($other)
    {
        return $this->getId() == $other->getId();
    }

    public function detach()
    {
        // TODO
    }

    /**
     * @return Discount
     */
    public function getDiscount(): Discount
    {
        return $this->discount;
    }

    /**
     * @return ReferrerId
     */
    public function getId(): ReferrerId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return Collection|Template[]
     */
    public function getTemplates(): Collection
    {
        $templates = new ArrayCollection();
        foreach ($this->templateLinks as $link) {
            $templates->add($link->getTemplate());
        }

        return $templates;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @return bool
     */
    public function hasDiscount(): bool
    {
        return null !== $this->discount;
    }

    /**
     * @return bool
     */
    public function hasHomepage(): bool
    {
        return $this->homepage;
    }

    /**
     * @return bool
     */
    public function hasKickback(): bool
    {
        return $this->kickback;
    }

    /**
     * @return bool
     */
    public function hasLanding(): bool
    {
        return $this->landing;
    }

    /**
     * @param Template $template
     *
     * @return bool
     */
    public function hasTemplate(Template $template): bool
    {
        return null !== $this->findTemplateLink($template);
    }

    /**
     * @param Discount $discount
     *
     * @throws \DomainException
     */
    public function linkDiscount(Discount $discount)
    {
        if (null !== $this->discount) {
            throw new \DomainException('This referrer has already been linked to a discount');
        }

        $this->discount = $discount;
        $this->discount->addReferrer($this);
    }

    /**
     * @param Order $order
     */
    public function removeOrder(Order $order)
    {
        if (!$this->orders->contains($order)) {
            return;
        }

        $this->orders->removeElement($order);
    }

    /**
     * @param Template $template
     */
    public function removeTemplate(Template $template)
    {
        $link = $this->findTemplateLink($template);
        if (null === $link) {
            return;
        }

        $this->removeTemplateLink($link);
    }

    /**
     * @param ReferrerTemplate $link
     */
    public function removeTemplateLink(ReferrerTemplate $link)
    {
        if (!$this->templateLinks->contains($link)) {
            return;
        }

        $link->detach();

        $this->templateLinks->removeElement($link);
    }

    /**
     * @param bool $homepage
     */
    public function setHomepage(bool $homepage)
    {
        $this->homepage = $homepage;
    }

    /**
     * @param bool $kickback
     */
    public function setKickback(bool $kickback)
    {
        $this->kickback = $kickback;
    }

    /**
     * @param bool $landing
     */
    public function setLanding(bool $landing)
    {
        $this->landing = $landing;
    }

    /**
     * @param string $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo ?: null;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text ?: null;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * @param string $website
     */
    public function setWebsite($website)
    {
        $this->website = $website ?: null;
    }

    /**
     * Referrer loskoppelen van een korting.
     */
    public function unlinkDiscount()
    {
        if (null === $this->discount) {
            return;
        }

        $reference = $this->discount;

        $this->discount = null;
        $reference->removeReferrer($this);
    }

    /**
     * @return Profile
     */
    public function getProfile(): Profile
    {
        return $this->profile;
    }

    /**
     * @return PersistentCollection
     */
    public function getTranslations(): PersistentCollection
    {
        return $this->translations;
    }

    /**
     * @param ReferrerTranslation $t
     */
    public function addTranslation(ReferrerTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    /**
     * @param Template $template
     *
     * @return ReferrerTemplate|null
     */
    private function findTemplateLink(Template $template)
    {
        $links = $this->templateLinks->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('template', $template)
            )
        );

        return $links->isEmpty() ? null : $links->first();
    }
}
