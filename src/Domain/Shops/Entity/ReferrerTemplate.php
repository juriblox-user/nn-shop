<?php

namespace NnShop\Domain\Shops\Entity;

use Doctrine\ORM\Mapping as ORM;
use NnShop\Domain\Templates\Entity\Template;

/**
 * @ORM\Entity
 */
class ReferrerTemplate
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="link_id")
     * @ORM\GeneratedValue
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Shops\Entity\Referrer", inversedBy="templateLinks")
     * @ORM\JoinColumn(referencedColumnName="referrer_id", nullable=false)
     *
     * @var Referrer
     */
    private $referrer;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Template", inversedBy="referrerLinks")
     * @ORM\JoinColumn(referencedColumnName="template_id", nullable=false)
     *
     * @var Template
     */
    private $template;

    /**
     * @param Referrer $referrer
     * @param Template $template
     */
    private function __construct(Referrer $referrer, Template $template)
    {
        $this->referrer = $referrer;
        $this->template = $template;

        $this->referrer->addTemplateLink($this);
        $this->template->addReferrerLink($this);
    }

    /**
     * @param Referrer $referrer
     * @param Template $template
     *
     * @return ReferrerTemplate
     */
    public static function create(Referrer $referrer, Template $template): self
    {
        return new self($referrer, $template);
    }

    /**
     * Detach the link from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->referrer) {
            $reference = $this->referrer;

            $this->referrer = null;
            $reference->removeTemplateLink($this);
        }

        if (null !== $this->template) {
            $reference = $this->template;

            $this->template = null;
            $reference->removeReferrerLink($this);
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Referrer
     */
    public function getReferrer(): Referrer
    {
        return $this->referrer;
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->template;
    }
}
