<?php

namespace NnShop\Domain\Shops\Entity;

use Core\Annotation\Doctrine as Core;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Comparable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use NnShop\Domain\Orders\Entity\Discount;
use NnShop\Domain\Shops\Value\LandingPageId;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Translation\Entity\Profile;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Shops\Repository\LandingPageDoctrineRepository")
 * @Core\QueryBuilder(class="NnShop\Infrastructure\Shops\QueryBuilder\LandingPageDoctrineQueryBuilder")
 * @Gedmo\TranslationEntity(class="NnShop\Domain\Shops\Entity\LandingPageTranslation")
 */
class LandingPage implements Comparable
{
    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Orders\Entity\Discount", inversedBy="landingPages")
     * @ORM\JoinColumn(referencedColumnName="discount_id", nullable=true)
     *
     * @var Discount
     */
    private $discount;

    /**
     * @ORM\Id
     * @ORM\Column(type="shops.landing_page_id", name="page_id")
     *
     * @var LandingPageId
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     * @Gedmo\Slug(fields={"title"})
     *
     * @var string
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Shops\Entity\LandingPageTemplate", mappedBy="landingPage", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @var Collection|ReferrerTemplate[]
     */
    private $templateLinks;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Translatable
     *
     * @var string
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=100)
     * @Gedmo\Translatable
     *
     * @var string
     */
    private $title;

    /**
     * @var Profile
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Translation\Entity\Profile", inversedBy="landingPages")
     * @ORM\JoinColumn(name="profile_id", referencedColumnName="profile_id", onDelete="SET NULL")
     */
    private $profile;

    /**
     * @ORM\OneToMany(
     *     targetEntity="LandingPageTranslation",
     *     mappedBy="object",
     *     cascade={"persist", "remove"}
     * )
     */
    private $translations;

    /**
     * LandingPage constructor.
     *
     * @param LandingPageId $id
     * @param Profile       $profile
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    private function __construct(LandingPageId $id, Profile $profile)
    {
        $this->id = $id;
        $this->profile = $profile;

        $this->profile->addLandingPage($this);

        $this->translations = new ArrayCollection();
        $this->templateLinks = new ArrayCollection();
    }

    /**
     * @param LandingPageId $id
     * @param string        $title
     * @param Profile       $profile
     *
     * @return LandingPage
     */
    public static function create(LandingPageId $id, string $title, Profile $profile): self
    {
        $referrer = new self($id, $profile);
        $referrer->title = $title;

        return $referrer;
    }

    /**
     * @param Template $template
     *
     * @return LandingPageTemplate
     */
    public function addTemplate(Template $template): LandingPageTemplate
    {
        return LandingPageTemplate::create($this, $template);
    }

    /**
     * @param LandingPageTemplate $link
     */
    public function addTemplateLink(LandingPageTemplate $link)
    {
        if ($this->hasTemplate($link->getTemplate())) {
            return;
        }

        $this->templateLinks->add($link);
    }

    /**
     * {@inheritdoc}
     *
     * @param $other Referrer
     */
    public function compareTo($other)
    {
        return $this->getId() == $other->getId();
    }

    public function detach()
    {
        $this->unlinkDiscount();
    }

    /**
     * @return Discount
     */
    public function getDiscount(): Discount
    {
        return $this->discount;
    }

    /**
     * @return LandingPageId
     */
    public function getId(): LandingPageId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return Collection|Template[]
     */
    public function getTemplates(): Collection
    {
        $templates = new ArrayCollection();
        foreach ($this->templateLinks as $link) {
            $templates->add($link->getTemplate());
        }

        return $templates;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function hasDiscount(): bool
    {
        return null !== $this->discount;
    }

    /**
     * @param Template $template
     *
     * @return bool
     */
    public function hasTemplate(Template $template): bool
    {
        return null !== $this->findTemplateLink($template);
    }

    /**
     * @param Discount $discount
     *
     * @throws \DomainException
     */
    public function linkDiscount(Discount $discount)
    {
        if (null !== $this->discount) {
            throw new \DomainException('This landing page has already been linked to a discount');
        }

        $this->discount = $discount;
        $this->discount->addLandingPage($this);
    }

    /**
     * @param Template $template
     */
    public function removeTemplate(Template $template)
    {
        $link = $this->findTemplateLink($template);
        if (null === $link) {
            return;
        }

        $this->removeTemplateLink($link);
    }

    /**
     * @param LandingPageTemplate $link
     */
    public function removeTemplateLink(LandingPageTemplate $link)
    {
        if (!$this->templateLinks->contains($link)) {
            return;
        }

        $link->detach();

        $this->templateLinks->removeElement($link);
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text ?: null;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * Referrer loskoppelen van een korting.
     */
    public function unlinkDiscount()
    {
        if (null === $this->discount) {
            return;
        }

        $reference = $this->discount;

        $this->discount = null;
        $reference->removeLandingPage($this);
    }

    /**
     * @return Profile
     */
    public function getProfile(): Profile
    {
        return $this->profile;
    }

    /**
     * @return PersistentCollection
     */
    public function getTranslations(): PersistentCollection
    {
        return $this->translations;
    }

    /**
     * @param LandingPageTranslation $t
     */
    public function addTranslation(LandingPageTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    /**
     * @param Template $template
     *
     * @return LandingPageTemplate|null
     */
    private function findTemplateLink(Template $template)
    {
        $links = $this->templateLinks->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('template', $template)
            )
        );

        return $links->isEmpty() ? null : $links->first();
    }
}
