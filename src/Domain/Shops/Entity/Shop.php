<?php

namespace NnShop\Domain\Shops\Entity;

use Core\Annotation\Doctrine as Core;
use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\Domain\Traits\SoftDeleteableEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Shops\Value\ShopId;
use NnShop\Domain\Templates\Entity\Category;
use NnShop\Domain\Templates\Entity\Category as TemplateCategory;
use NnShop\Domain\Translation\Entity\Profile;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Shops\Repository\ShopDoctrineRepository")
 * @Core\QueryBuilder(class="NnShop\Infrastructure\Shops\QueryBuilder\ShopDoctrineQueryBuilder")
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(name="ix_enabled", columns={"shop_enabled", "timestamp_deleted"}),
 *     @ORM\Index(name="ix_deleted", columns={"timestamp_deleted"})
 * })
 *
 * @Gedmo\SoftDeleteable(fieldName="timestampDeleted")
 * @Gedmo\TranslationEntity(class="ShopTranslation")
 */
class Shop
{
    use SoftDeleteableEntityTrait;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\Category", mappedBy="shop", cascade={"persist"}, fetch="EXTRA_LAZY")
     *
     * @var Collection|Category[]
     */
    private $categories;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $enabled;

    /**
     * @ORM\Column(type="string", length=100)
     *
     * @var string
     */
    private $hostname;

    /**
     * @ORM\Id
     * @ORM\Column(type="shops.shop_id")
     *
     * @var ShopId
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Gedmo\Translatable
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Gedmo\Translatable
     *
     * @var string
     */
    private $metaDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Translatable
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\Order", mappedBy="shop", fetch="EXTRA_LAZY")
     *
     * @var Collection|Order[]
     */
    private $orders;

    /**
     * @var Profile
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Translation\Entity\Profile", inversedBy="shops")
     * @ORM\JoinColumn(name="profile_id", referencedColumnName="profile_id", onDelete="SET NULL")
     */
    private $profile;

    /**
     * @ORM\OneToMany(
     *     targetEntity="ShopTranslation",
     *     mappedBy="object",
     *     cascade={"persist", "remove"}
     * )
     */
    private $translations;

    /**
     * Shop constructor.
     *
     * @param ShopId  $id
     * @param Profile $profile
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    private function __construct(ShopId $id, Profile $profile)
    {
        $this->id = $id;
        $this->profile = $profile;

        $this->enabled = true;

        $this->profile->addShop($this);

        $this->categories = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }

    /**
     * @param ShopId  $id
     * @param string  $title
     * @param string  $hostname
     * @param Profile $profile
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     *
     * @return Shop
     */
    public static function create(ShopId $id, string $title, string $hostname, Profile $profile): self
    {
        $shop = new self($id, $profile);
        $shop->setTitle($title);
        $shop->setHostname($hostname);

        return $shop;
    }

    /**
     * @param Order $order
     */
    public function addOrder(Order $order)
    {
        if ($this->orders->contains($order)) {
            return;
        }

        $this->orders->add($order);
    }

    /**
     * @param Order $order
     */
    public function removeOrder(Order $order)
    {
        if (!$this->orders->contains($order)) {
            return;
        }

        $this->orders->removeElement($order);
    }

    /**
     * @param Category $category
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    public function addCategory(Category $category)
    {
        if ($this->hasCategory($category)) {
            throw new DuplicateEntityException();
        }

        $this->categories->add($category);
    }

    /**
     * Shop uitschakelen.
     */
    public function disable()
    {
        $this->enabled = false;
    }

    /**
     * Shop inschakelen.
     */
    public function enable()
    {
        $this->enabled = true;
    }

    /**
     * @return Collection|TemplateCategory[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    /**
     * @return string
     */
    public function getHostname(): string
    {
        return $this->hostname;
    }

    /**
     * @return ShopId
     */
    public function getId(): ShopId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param Category $category
     *
     * @return bool
     */
    public function hasCategory(Category $category): bool
    {
        return $this->categories->contains($category);
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param TemplateCategory $category
     */
    public function removeCategory(Category $category)
    {
        if (!$this->hasCategory($category)) {
            return;
        }

        $this->categories->remove($category);
    }

    /**
     * @param string $hostname
     */
    public function setHostname(string $hostname)
    {
        $this->hostname = $hostname;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param string|null $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription ?: null;
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription($description)
    {
        $this->description = $description ?: null;
    }

    /**
     * @return Profile
     */
    public function getProfile(): Profile
    {
        return $this->profile;
    }

    /**
     * @return PersistentCollection
     */
    public function getTranslations(): PersistentCollection
    {
        return $this->translations;
    }

    /**
     * @param ShopTranslation $t
     */
    public function addTranslation(ShopTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }
}
