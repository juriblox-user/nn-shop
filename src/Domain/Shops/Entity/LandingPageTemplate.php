<?php

namespace NnShop\Domain\Shops\Entity;

use Doctrine\ORM\Mapping as ORM;
use NnShop\Domain\Templates\Entity\Template;

/**
 * @ORM\Entity
 */
class LandingPageTemplate
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="link_id")
     * @ORM\GeneratedValue
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Shops\Entity\LandingPage", inversedBy="templateLinks")
     * @ORM\JoinColumn(referencedColumnName="page_id", nullable=false)
     *
     * @var LandingPage
     */
    private $landingPage;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Template", inversedBy="landingPageLinks")
     * @ORM\JoinColumn(referencedColumnName="template_id", nullable=false)
     *
     * @var Template
     */
    private $template;

    /**
     * Constructor.
     *
     * @param LandingPage $landingPage
     * @param Template    $template
     */
    private function __construct(LandingPage $landingPage, Template $template)
    {
        $this->landingPage = $landingPage;
        $this->template = $template;

        $this->landingPage->addTemplateLink($this);
        $this->template->addLandingPageLink($this);
    }

    /**
     * @param LandingPage $landingPage
     * @param Template    $template
     *
     * @return LandingPageTemplate
     */
    public static function create(LandingPage $landingPage, Template $template): self
    {
        return new self($landingPage, $template);
    }

    /**
     * Detach the link from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->landingPage) {
            $reference = $this->landingPage;

            $this->landingPage = null;
            $reference->removeTemplateLink($this);
        }

        if (null !== $this->template) {
            $reference = $this->template;

            $this->template = null;
            $reference->removeLandingPageLink($this);
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return LandingPage
     */
    public function getLandingPage(): LandingPage
    {
        return $this->landingPage;
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->template;
    }
}
