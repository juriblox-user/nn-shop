<?php

namespace NnShop\Domain\Shops\Command\LandingPage;

use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Domain\Orders\Value\DiscountId;
use NnShop\Domain\Shops\Value\LandingPageId;
use NnShop\Domain\Templates\Value\TemplateId;
use NnShop\Domain\Translation\Value\ProfileId;

class CreateLandingPageCommand
{
    /**
     * @var DiscountId
     */
    private $discountId;

    /**
     * @var LandingPageId
     */
    private $id;

    /**
     * @var ArrayCollection|TemplateId[]
     */
    private $templates;

    /**
     * @var null|string
     */
    private $text;

    /**
     * @var null|string
     */
    private $title;

    /**
     * @var ProfileId
     */
    private $profileId;

    /**
     * @param LandingPageId $id
     * @param ProfileId     $profileId
     */
    private function __construct(LandingPageId $id, ProfileId $profileId)
    {
        $this->id = $id;
        $this->profileId = $profileId;

        $this->templates = new ArrayCollection();
    }

    /**
     * @param LandingPageId $id
     * @param ProfileId     $profileId
     *
     * @return CreateLandingPageCommand
     */
    public static function prepare(LandingPageId $id, ProfileId $profileId): self
    {
        return new self($id, $profileId);
    }

    /**
     * @param TemplateId $templateId
     */
    public function addTemplate(TemplateId $templateId)
    {
        $this->templates->add($templateId);
    }

    /**
     * @return DiscountId
     */
    public function getDiscountId(): DiscountId
    {
        return $this->discountId;
    }

    /**
     * @return LandingPageId
     */
    public function getId(): LandingPageId
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection|TemplateId[]
     */
    public function getTemplates(): ArrayCollection
    {
        return $this->templates;
    }

    /**
     * @return null|string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return null|string
     */
    public function getTitle()
    {
        return $this->title;
    }

    public function hasDiscount(): bool
    {
        return null !== $this->discountId;
    }

    /**
     * @param DiscountId $discountId
     */
    public function linkDiscount(DiscountId $discountId)
    {
        $this->discountId = $discountId;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text ?: null;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * @return ProfileId
     */
    public function getProfileId(): ProfileId
    {
        return $this->profileId;
    }
}
