<?php

namespace NnShop\Domain\Shops\Command\LandingPage;

use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Domain\Orders\Value\DiscountId;
use NnShop\Domain\Shops\Entity\LandingPage;
use NnShop\Domain\Shops\Value\LandingPageId;
use NnShop\Domain\Templates\Value\TemplateId;

class UpdateLandingPageCommand
{
    /**
     * @var DiscountId
     */
    private $discountId;

    /**
     * @var LandingPageId
     */
    private $id;

    /**
     * @var ArrayCollection|TemplateId[]
     */
    private $templates;

    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $title;

    /**
     * @param LandingPageId $id
     */
    private function __construct(LandingPageId $id)
    {
        $this->id = $id;
        $this->templates = new ArrayCollection();

        $this->deleteLogo = false;
    }

    /**
     * @param LandingPage $landingPage
     *
     * @return UpdateLandingPageCommand
     */
    public static function prepare(LandingPage $landingPage): self
    {
        $command = new self($landingPage->getId());
        $command->title = $landingPage->getTitle();
        $command->text = $landingPage->getText();

        $command->discountId = $landingPage->hasDiscount() ? $landingPage->getDiscount() : null;

        return $command;
    }

    /**
     * @param TemplateId $templateId
     */
    public function addTemplate(TemplateId $templateId)
    {
        $this->templates->add($templateId);
    }

    /**
     * @return DiscountId
     */
    public function getDiscountId(): DiscountId
    {
        return $this->discountId;
    }

    /**
     * @return LandingPageId
     */
    public function getId(): LandingPageId
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection|TemplateId[]
     */
    public function getTemplates(): ArrayCollection
    {
        return $this->templates;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function hasDiscount(): bool
    {
        return null !== $this->discountId;
    }

    /**
     * @param DiscountId $discountId
     */
    public function linkDiscount(DiscountId $discountId)
    {
        $this->discountId = $discountId;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text ?: null;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    public function unlinkDiscount()
    {
        $this->discountId = null;
    }
}
