<?php

namespace NnShop\Domain\Shops\Command\LandingPage;

use NnShop\Domain\Shops\Value\LandingPageId;

class DeleteLandingPageCommand
{
    /**
     * @var LandingPageId
     */
    private $id;

    /**
     * @param LandingPageId $id
     */
    private function __construct(LandingPageId $id)
    {
        $this->id = $id;
    }

    /**
     * @param LandingPageId $id
     *
     * @return DeleteLandingPageCommand
     */
    public static function create(LandingPageId $id): self
    {
        return new self($id);
    }

    /**
     * @return LandingPageId
     */
    public function getId(): LandingPageId
    {
        return $this->id;
    }
}
