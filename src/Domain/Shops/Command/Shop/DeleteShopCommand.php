<?php

namespace NnShop\Domain\Shops\Command\Shop;

use Core\Domain\Value\AbstractUuidValue;
use NnShop\Domain\Shops\Value\ShopId;

class DeleteShopCommand
{
    /**
     * @var ShopId
     */
    private $id;

    /**
     * CreateShopCommand constructor.
     *
     * @param ShopId|AbstractUuidValue $id
     */
    private function __construct(ShopId $id)
    {
        $this->id = $id;
    }

    /**
     * @param ShopId $id
     *
     * @return DeleteShopCommand
     */
    public static function create(ShopId $id): self
    {
        return new self($id);
    }

    /**
     * @return ShopId
     */
    public function getId(): ShopId
    {
        return $this->id;
    }
}
