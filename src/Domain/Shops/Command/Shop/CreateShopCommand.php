<?php

namespace NnShop\Domain\Shops\Command\Shop;

use NnShop\Domain\Shops\Value\ShopId;
use NnShop\Domain\Translation\Value\ProfileId;

class CreateShopCommand
{
    /**
     * @var string
     */
    private $hostname;

    /**
     * @var ShopId
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $metaDescription;

    /**
     * @var string
     */
    private $description;

    /**
     * @var ProfileId
     */
    private $profileId;

    /**
     * CreateShopCommand constructor.
     *
     * @param ShopId    $id
     * @param ProfileId $profileId
     */
    private function __construct(ShopId $id, ProfileId $profileId)
    {
        $this->id = $id;
        $this->profileId = $profileId;
    }

    /**
     * @return string
     */
    public function getHostname(): string
    {
        return $this->hostname;
    }

    /**
     * @return ShopId
     */
    public function getId(): ShopId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param ShopId    $id
     * @param ProfileId $profileId
     *
     * @return CreateShopCommand
     */
    public static function prepare(ShopId $id, ProfileId $profileId): self
    {
        return new self($id, $profileId);
    }

    /**
     * @param string $hostname
     */
    public function setHostname(string $hostname)
    {
        $hostname = mb_strtolower($hostname);
        if (0 == preg_match('/^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)*[a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?$/', $hostname)) {
            throw new \InvalidArgumentException('The provided hostname does not look like a valid domain name');
        }

        $this->hostname = $hostname;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription ?: null;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description ?: null;
    }

    /**
     * @return ProfileId
     */
    public function getProfileId(): ProfileId
    {
        return $this->profileId;
    }
}
