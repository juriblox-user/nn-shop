<?php

namespace NnShop\Domain\Shops\Command\Shop;

use Core\Domain\Value\AbstractUuidValue;
use NnShop\Domain\Shops\Value\ShopId;

class UpdateShopCommand
{
    /**
     * @var bool
     */
    private $enabled;

    /**
     * @var string
     */
    private $hostname;

    /**
     * @var ShopId
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $metaDescription;

    /**
     * @var string
     */
    private $description;

    /**
     * CreateShopCommand constructor.
     *
     * @param ShopId|AbstractUuidValue $id
     */
    private function __construct(ShopId $id)
    {
        $this->id = $id;
    }

    /**
     * @param ShopId $id
     *
     * @return UpdateShopCommand
     */
    public static function prepare(ShopId $id): self
    {
        return new self($id);
    }

    /**
     * @return string
     */
    public function getHostname(): string
    {
        return $this->hostname;
    }

    /**
     * @return ShopId
     */
    public function getId(): ShopId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @param string $hostname
     */
    public function setHostname(string $hostname)
    {
        $hostname = mb_strtolower($hostname);
        if (0 == preg_match('/^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)*[a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?$/', $hostname)) {
            throw new \InvalidArgumentException('The provided hostname does not look like a valid domain name');
        }

        $this->hostname = $hostname;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription ?: null;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description ?: null;
    }
}
