<?php

namespace NnShop\Domain\Shops\Command\Referrer;

use NnShop\Domain\Shops\Value\ReferrerId;

class DeleteReferrerCommand
{
    /**
     * @var ReferrerId
     */
    private $id;

    /**
     * @param ReferrerId $id
     */
    private function __construct(ReferrerId $id)
    {
        $this->id = $id;
    }

    /**
     * @param ReferrerId $id
     *
     * @return DeleteReferrerCommand
     */
    public static function create(ReferrerId $id): self
    {
        return new self($id);
    }

    /**
     * @return ReferrerId
     */
    public function getId(): ReferrerId
    {
        return $this->id;
    }
}
