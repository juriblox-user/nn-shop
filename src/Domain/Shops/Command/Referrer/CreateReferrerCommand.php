<?php

namespace NnShop\Domain\Shops\Command\Referrer;

use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Domain\Orders\Value\DiscountId;
use NnShop\Domain\Shops\Value\ReferrerId;
use NnShop\Domain\Templates\Value\TemplateId;
use NnShop\Domain\Translation\Value\ProfileId;
use Symfony\Component\HttpFoundation\File\File;

class CreateReferrerCommand
{
    /**
     * @var DiscountId
     */
    private $discountId;

    /**
     * @var bool
     */
    private $homepage;

    /**
     * @var ReferrerId
     */
    private $id;

    /**
     * @var bool
     */
    private $kickback;

    /**
     * @var bool
     */
    private $landing;

    /**
     * @var File
     */
    private $logo;

    /**
     * @var ArrayCollection|TemplateId[]
     */
    private $templates;

    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $website;

    /**
     * @var ProfileId
     */
    private $profileId;

    /**
     * @param ReferrerId $id
     * @param ProfileId  $profileId
     */
    private function __construct(ReferrerId $id, ProfileId $profileId)
    {
        $this->id = $id;
        $this->profileId = $profileId;

        $this->homepage = false;
        $this->kickback = false;
        $this->landing = false;

        $this->templates = new ArrayCollection();
    }

    /**
     * @param ReferrerId $id
     * @param ProfileId  $profileId
     *
     * @return CreateReferrerCommand
     */
    public static function prepare(ReferrerId $id, ProfileId $profileId): self
    {
        return new self($id, $profileId);
    }

    /**
     * @param TemplateId $templateId
     */
    public function addTemplate(TemplateId $templateId)
    {
        $this->templates->add($templateId);
    }

    /**
     * @return DiscountId
     */
    public function getDiscountId(): DiscountId
    {
        return $this->discountId;
    }

    /**
     * @return ReferrerId
     */
    public function getId(): ReferrerId
    {
        return $this->id;
    }

    /**
     * @return File
     */
    public function getLogo(): File
    {
        return $this->logo;
    }

    /**
     * @return ArrayCollection|TemplateId[]
     */
    public function getTemplates(): ArrayCollection
    {
        return $this->templates;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    public function hasDiscount(): bool
    {
        return null !== $this->discountId;
    }

    /**
     * @return bool
     */
    public function hasHomepage(): bool
    {
        return $this->homepage;
    }

    /**
     * @return bool
     */
    public function hasKickback(): bool
    {
        return $this->kickback;
    }

    /**
     * @return bool
     */
    public function hasLanding(): bool
    {
        return $this->landing;
    }

    /**
     * @return bool
     */
    public function hasLogo(): bool
    {
        return null !== $this->logo;
    }

    /**
     * @param DiscountId $discountId
     */
    public function linkDiscount(DiscountId $discountId)
    {
        $this->discountId = $discountId;
    }

    /**
     * @param bool $homepage
     */
    public function setHomepage(bool $homepage)
    {
        $this->homepage = $homepage;
    }

    /**
     * @param bool $kickback
     */
    public function setKickback(bool $kickback)
    {
        $this->kickback = $kickback;
    }

    /**
     * @param bool $landing
     */
    public function setLanding(bool $landing)
    {
        $this->landing = $landing;
    }

    /**
     * @param File $logo
     */
    public function setLogo(File $logo)
    {
        $this->logo = $logo;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text ?: null;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * @param string $website
     */
    public function setWebsite($website)
    {
        $this->website = $website ?: null;
    }

    /**
     * @return ProfileId
     */
    public function getProfileId(): ProfileId
    {
        return $this->profileId;
    }
}
