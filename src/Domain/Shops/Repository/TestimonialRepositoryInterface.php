<?php

namespace NnShop\Domain\Shops\Repository;

use Core\Domain\RepositoryInterface;
use NnShop\Domain\Shops\Entity\Testimonial;
use NnShop\Domain\Shops\Value\TestimonialId;

interface TestimonialRepositoryInterface extends RepositoryInterface
{
    /**
     * @param TestimonialId $id
     *
     * @return Testimonial|null
     */
    public function findOneById(TestimonialId $id);

    /**
     * @param TestimonialId $id
     *
     * @return Testimonial
     */
    public function getById(TestimonialId $id): Testimonial;

    /**
     * @param TestimonialId $id
     *
     * @return Testimonial
     */
    public function getReference(TestimonialId $id): Testimonial;
}
