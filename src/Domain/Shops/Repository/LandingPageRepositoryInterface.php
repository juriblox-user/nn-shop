<?php

namespace NnShop\Domain\Shops\Repository;

use Core\Domain\RepositoryInterface;
use NnShop\Domain\Shops\Entity\LandingPage;
use NnShop\Domain\Shops\Entity\Referrer;
use NnShop\Domain\Shops\Value\LandingPageId;

interface LandingPageRepositoryInterface extends RepositoryInterface
{
    /**
     * @return Referrer[]
     */
    public function findAll(): array;

    /**
     * @param LandingPageId $id
     *
     * @return self|null
     */
    public function findOneById(LandingPageId $id);

    /**
     * @param string $slug
     *
     * @return LandingPage|null
     */
    public function findOneBySlug(string $slug);

    /**
     * @param LandingPageId $id
     *
     * @return LandingPage
     */
    public function getById(LandingPageId $id): LandingPage;

    /**
     * @param LandingPageId $id
     *
     * @return LandingPage
     */
    public function getReference(LandingPageId $id): LandingPage;
}
