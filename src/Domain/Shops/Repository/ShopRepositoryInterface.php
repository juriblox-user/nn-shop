<?php

namespace NnShop\Domain\Shops\Repository;

use Core\Common\Exception\Domain\EntityNotFoundException;
use Core\Domain\RepositoryInterface;
use NnShop\Domain\Shops\Entity\Shop;
use NnShop\Domain\Shops\Value\ShopId;
use NnShop\Domain\Translation\Entity\Profile;

interface ShopRepositoryInterface extends RepositoryInterface
{
    /**
     * @return array|Shop[]
     */
    public function findAll();

    /**
     * @return Shop[]
     */
    public function findAllVisible(Profile $profile);

    /**
     * @param $hostname
     *
     * @return Shop
     */
    public function findOneByHostname($hostname);

    /**
     * @param ShopId $id
     *
     * @return Shop
     */
    public function findOneById(ShopId $id);

    /**
     * @param $hostname
     *
     * @throws EntityNotFoundException
     *
     * @return Shop
     */
    public function getByHostname($hostname): Shop;

    /**
     * @param ShopId $id
     *
     * @throws EntityNotFoundException
     *
     * @return Shop
     */
    public function getById(ShopId $id): Shop;

    /**
     * @param ShopId $id
     *
     * @return Shop
     */
    public function getReference(ShopId $id): Shop;

    /**
     * @param string $hostname
     *
     * @return bool
     */
    public function hasWithHostname($hostname): bool;

    /**
     * @return array
     *
     * Get entities for backend nav menu
     */
    public function getMenu(): array;

    /**
     * @param Profile $profile
     *
     * @return array
     */
    public function getMenuByProfile(Profile $profile): array;

    /**
     * @param string $host
     *
     * @return Shop|null
     */
    public function findByHost(string $host);
}
