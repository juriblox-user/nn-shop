<?php

namespace NnShop\Domain\Shops\Repository;

use Core\Domain\RepositoryInterface;
use NnShop\Domain\Shops\Entity\Referrer;
use NnShop\Domain\Shops\Value\ReferrerId;
use NnShop\Domain\Translation\Entity\Profile;

interface ReferrerRepositoryInterface extends RepositoryInterface
{
    /**
     * @return Referrer[]
     */
    public function findAll();

    /**
     * @param ReferrerId $id
     *
     * @return self|null
     */
    public function findOneById(ReferrerId $id);

    /**
     * @param string $slug
     *
     * @return Referrer|null
     */
    public function findOneBySlug(string $slug);

    /**
     * @param int     $limit
     * @param Profile $profile
     *
     * @return array|Referrer[]
     */
    public function findShuffledForHomepage(int $limit, Profile $profile): array;

    /**
     * @param ReferrerId $id
     *
     * @return Referrer
     */
    public function getById(ReferrerId $id): Referrer;

    /**
     * @param string $slug
     *
     * @return Referrer
     */
    public function getBySlug($slug): Referrer;

    /**
     * @param ReferrerId $id
     *
     * @return Referrer
     */
    public function getReference(ReferrerId $id): Referrer;
}
