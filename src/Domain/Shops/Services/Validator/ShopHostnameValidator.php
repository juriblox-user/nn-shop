<?php

namespace NnShop\Domain\Shops\Services\Validator;

use NnShop\Domain\Shops\Entity\Shop;
use NnShop\Domain\Shops\Repository\ShopRepositoryInterface;

class ShopHostnameValidator
{
    /**
     * @var ShopRepositoryInterface
     */
    private $repository;

    /**
     * ShopHostnameValidator constructor.
     *
     * @param ShopRepositoryInterface $repository
     */
    public function __construct(ShopRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string    $hostname
     * @param Shop|null $entity
     *
     * @return bool
     */
    public function isUnique(string $hostname, Shop $entity = null): bool
    {
        $existing = $this->repository->findOneByHostname($hostname);
        if (null !== $existing) {
            if (null !== $entity && $entity->getId() == $existing->getId()) {
                return true;
            }

            return false;
        }

        return true;
    }
}
