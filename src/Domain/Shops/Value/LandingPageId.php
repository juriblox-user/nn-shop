<?php

namespace NnShop\Domain\Shops\Value;

use Core\Domain\Value\AbstractUuidValue;

class LandingPageId extends AbstractUuidValue
{
}
