<?php

namespace NnShop\Domain\Shops\Value;

use Core\Domain\Value\AbstractUuidValue;

class ShopId extends AbstractUuidValue
{
}
