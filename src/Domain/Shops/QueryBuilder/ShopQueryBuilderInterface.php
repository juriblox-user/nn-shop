<?php

namespace NnShop\Domain\Shops\QueryBuilder;

use Core\Doctrine\ORM\QueryBuilderInterface;
use NnShop\Domain\Translation\Value\ProfileId;

interface ShopQueryBuilderInterface extends QueryBuilderInterface
{
    /**
     * @param ProfileId $profileId
     *
     * @return ShopQueryBuilderInterface
     */
    public function filterProfile(ProfileId $profileId): self;
}
