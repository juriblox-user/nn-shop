<?php

namespace NnShop\Domain\Shops\QueryBuilder;

use Core\Doctrine\ORM\QueryBuilderInterface;
use NnShop\Domain\Translation\Value\ProfileId;

interface LandingPageQueryBuilderInterface extends QueryBuilderInterface
{
    /**
     * @param ProfileId $profileId
     *
     * @return LandingPageQueryBuilderInterface
     */
    public function filterProfile(ProfileId $profileId): self;
}
