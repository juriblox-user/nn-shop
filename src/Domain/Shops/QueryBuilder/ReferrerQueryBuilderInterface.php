<?php

namespace NnShop\Domain\Shops\QueryBuilder;

use Core\Doctrine\ORM\QueryBuilderInterface;
use NnShop\Domain\Translation\Value\ProfileId;

interface ReferrerQueryBuilderInterface extends QueryBuilderInterface
{
    /**
     * @param ProfileId $profileId
     *
     * @return ReferrerQueryBuilderInterface
     */
    public function filterProfile(ProfileId $profileId): self;
}
