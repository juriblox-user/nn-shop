<?php

namespace NnShop\Domain\Pages\QueryBuilder;

use Core\Doctrine\ORM\QueryBuilderInterface;
use NnShop\Domain\Translation\Value\ProfileId;

/**
 * Interface PageQueryBuilderInterface.
 */
interface PageQueryBuilderInterface extends QueryBuilderInterface
{
    /**
     * @param ProfileId $profileId
     *
     * @return PageQueryBuilderInterface
     */
    public function filterProfile(ProfileId $profileId): self;
}
