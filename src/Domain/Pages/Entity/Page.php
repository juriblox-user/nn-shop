<?php

namespace NnShop\Domain\Pages\Entity;

use Core\Annotation\Doctrine as Core;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use NnShop\Domain\Pages\Value\PageId;
use NnShop\Domain\Translation\Entity\Profile;

/**
 * Class Page.
 *
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Pages\Repository\PageDoctrineRepository")
 * @Core\QueryBuilder(class="NnShop\Infrastructure\Pages\QueryBuilder\PageDoctrineQueryBuilder")
 * @Gedmo\TranslationEntity(class="NnShop\Domain\Pages\Entity\PageTranslation")
 */
class Page
{
    /**
     * @ORM\Id
     * @ORM\Column(type="pages.page_id")
     *
     * @var PageId
     */
    private $id;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $code;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string|null
     */
    private $lead;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string
     */
    private $content;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string|null
     */
    private $footer;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     * @Gedmo\Translatable
     */
    private $pdf;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Gedmo\Translatable
     */
    private $pdfName;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Gedmo\Translatable
     */
    private $pdfDirectory;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     * @Gedmo\Translatable
     */
    private $lastUpdated;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=255, unique=true)
     */
    private $slug;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @ORM\OneToMany(
     *     targetEntity="PageTranslation",
     *     mappedBy="object",
     *     cascade={"persist", "remove"}
     * )
     */
    private $translations;

    /**
     * @var Profile
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Translation\Entity\Profile", inversedBy="pages")
     * @ORM\JoinColumn(name="profile_id", referencedColumnName="profile_id", onDelete="SET NULL")
     */
    private $profile;

    /**
     * Page constructor.
     *
     * @param PageId    $id
     * @param \DateTime $lastUpdated
     * @param Profile   $profile
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    private function __construct(PageId $id, \DateTime $lastUpdated, Profile $profile)
    {
        $this->id = $id;
        $this->lastUpdated = $lastUpdated;
        $this->profile = $profile;

        $this->profile->addPage($this);

        $this->translations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }

    /**
     * @param PageId  $id
     * @param Profile $profile
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     *
     * @return Page
     */
    public static function create(PageId $id, Profile $profile): self
    {
        return new self($id, new \DateTime(), $profile);
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @return PageId
     */
    public function getId(): PageId
    {
        return $this->id;
    }

    /**
     * @return PersistentCollection
     */
    public function getTranslations(): PersistentCollection
    {
        return $this->translations;
    }

    /**
     * @param PageTranslation $t
     */
    public function addTranslation(PageTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    /**
     * @return string|null
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Page
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     *
     * @return Page
     */
    public function setContent(string $content = null): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return Page
     */
    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * @param string|null $lead
     *
     * @return Page
     */
    public function setLead(string $lead = null): self
    {
        $this->lead = $lead;

        return $this;
    }

    /**
     * @param string|null $footer
     *
     * @return Page
     */
    public function setFooter(string $footer = null): self
    {
        $this->footer = $footer;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getFooter()
    {
        return $this->footer;
    }

    /**
     * @return string|null
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * @param $pdf
     *
     * @return Page
     */
    public function setPdf($pdf): self
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * @param \DateTime $lastUpdated
     *
     * @return Page
     */
    public function setLastUpdated(\DateTime $lastUpdated): self
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdated(): \DateTime
    {
        return $this->lastUpdated;
    }

    /**
     * @return string|null
     */
    public function getPdfDirectory()
    {
        return $this->pdfDirectory;
    }

    /**
     * @param string|null $directory
     *
     * @return Page
     */
    public function setPdfDirectory(string $directory = null): self
    {
        $this->pdfDirectory = $directory;

        return $this;
    }

    /**
     * @param string|null $name
     *
     * @return Page
     */
    public function setPdfName(string $name = null): self
    {
        $this->pdfName = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPdfName()
    {
        return $this->pdfName;
    }

    /**
     * @return Profile
     */
    public function getProfile(): Profile
    {
        return $this->profile;
    }
}
