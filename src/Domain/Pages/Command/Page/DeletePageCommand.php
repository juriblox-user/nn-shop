<?php

namespace NnShop\Domain\Pages\Command\Page;

use NnShop\Domain\Pages\Value\PageId;

/**
 * Class DeletePageCommand.
 */
class DeletePageCommand
{
    /**
     * @var PageId
     */
    private $id;

    /**
     * DeletePageCommand constructor.
     *
     * @param PageId $pageId
     */
    private function __construct(PageId $pageId)
    {
        $this->id = $pageId;
    }

    /**
     * @param PageId $pageId
     *
     * @return DeletePageCommand
     */
    public static function create(PageId $pageId): self
    {
        return new static($pageId);
    }

    /**
     * @return PageId
     */
    public function getId(): PageId
    {
        return $this->id;
    }
}
