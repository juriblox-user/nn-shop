<?php

namespace NnShop\Domain\Pages\Command\Page;

use NnShop\Domain\Pages\Value\PageId;
use NnShop\Domain\Translation\Value\ProfileId;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class CreatePageCommand.
 */
class CreatePageCommand
{
    /**
     * @var PageId
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string|null
     */
    private $content;

    /**
     * @var string|null
     */
    private $lead;

    /**
     * @var string|null
     */
    private $footer;

    /**
     * @var \DateTime
     */
    private $lastUpdated;

    /**
     * @var File
     */
    private $pdf;

    /**
     * @var ProfileId
     */
    private $profileId;

    /**
     * @var string|null
     */
    private $pdfDirectory;

    /**
     * @var string|null
     */
    private $pdfName;

    /**
     * CreatePageCommand constructor.
     *
     * @param PageId    $id
     * @param ProfileId $profileId
     */
    private function __construct(PageId $id, ProfileId $profileId)
    {
        $this->id = $id;
        $this->profileId = $profileId;
    }

    /**
     * @param PageId    $pageId
     * @param ProfileId $profileId
     *
     * @return CreatePageCommand
     */
    public static function create(PageId $pageId, ProfileId $profileId): self
    {
        return new static($pageId, $profileId);
    }

    /**
     * @return PageId
     */
    public function getId(): PageId
    {
        return $this->id;
    }

    /**
     * @param string $code
     *
     * @return CreatePageCommand
     */
    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $title
     *
     * @return CreatePageCommand
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string|null $content
     *
     * @return CreatePageCommand
     */
    public function setContent(string $content = null): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string|null $lead
     *
     * @return CreatePageCommand
     */
    public function setLead(string $lead = null): self
    {
        $this->lead = $lead;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * @param string|null $footer
     *
     * @return CreatePageCommand
     */
    public function setFooter(string $footer = null): self
    {
        $this->footer = $footer;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getFooter()
    {
        return $this->footer;
    }

    /**
     * @param \DateTime $lastUpdated
     *
     * @return CreatePageCommand
     */
    public function setLastUpdated(\DateTime $lastUpdated): self
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdated(): \DateTime
    {
        return $this->lastUpdated;
    }

    /**
     * @return File|null
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * @param File $pdf
     *
     * @return CreatePageCommand
     */
    public function setPdf(File $pdf): self
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasPdf(): bool
    {
        return null !== $this->pdf;
    }

    /**
     * @return ProfileId
     */
    public function getProfileId(): ProfileId
    {
        return $this->profileId;
    }

    /**
     * @return string|null
     */
    public function getPdfDirectory()
    {
        return $this->pdfDirectory;
    }

    /**
     * @param string|null $directory
     *
     * @return CreatePageCommand
     */
    public function setPdfDirectory(string $directory = null): self
    {
        $this->pdfDirectory = $directory;

        return $this;
    }

    /**
     * @param string|null $name
     *
     * @return CreatePageCommand
     */
    public function setPdfName(string $name = null): self
    {
        $this->pdfName = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPdfName()
    {
        return $this->pdfName;
    }
}
