<?php

namespace NnShop\Domain\Pages\Command\Page;

use NnShop\Domain\Pages\Value\PageId;
use NnShop\Domain\Translation\Value\ProfileId;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class UpdatePageCommand.
 */
class UpdatePageCommand
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string|null
     */
    private $content;

    /**
     * @var string|null
     */
    private $lead;

    /**
     * @var string|null
     */
    private $footer;

    /**
     * @var \DateTime
     */
    private $lastUpdated;

    /**
     * @var bool
     */
    private $deletePdf = false;

    /**
     * @var File
     */
    private $pdf;

    /**
     * @var ProfileId
     */
    private $profileId;

    /**
     * @var PageId
     */
    private $id;

    /**
     * UpdatePageCommand constructor.
     *
     * @param PageId $id
     */
    private function __construct(PageId $id)
    {
        $this->id = $id;
    }

    /**
     * @param PageId $pageId
     *
     * @return UpdatePageCommand
     */
    public static function create(PageId $pageId): self
    {
        return new static($pageId);
    }

    /**
     * @return PageId
     */
    public function getId(): PageId
    {
        return $this->id;
    }

    /**
     * @param string $code
     *
     * @return self
     */
    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string|null $content
     *
     * @return self
     */
    public function setContent(string $content = null): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string|null $lead
     *
     * @return self
     */
    public function setLead(string $lead = null): self
    {
        $this->lead = $lead;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * @param string|null $footer
     *
     * @return UpdatePageCommand
     */
    public function setFooter(string $footer = null): self
    {
        $this->footer = $footer;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getFooter()
    {
        return $this->footer;
    }

    /**
     * @param \DateTime $lastUpdated
     *
     * @return self
     */
    public function setLastUpdated(\DateTime $lastUpdated): self
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdated(): \DateTime
    {
        return $this->lastUpdated;
    }

    /**
     * @param bool $deletePdf
     *
     * @return self
     */
    public function setDeletePdf(bool $deletePdf): self
    {
        $this->deletePdf = $deletePdf;

        return $this;
    }

    /**
     * @return bool
     */
    public function deletePdf(): bool
    {
        return $this->deletePdf;
    }

    /**
     * @return File|null
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * @param File $pdf
     *
     * @return self
     */
    public function setPdf(File $pdf): self
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasPdf(): bool
    {
        return null !== $this->pdf;
    }

    /**
     * @return ProfileId
     */
    public function getProfileId(): ProfileId
    {
        return $this->profileId;
    }
}
