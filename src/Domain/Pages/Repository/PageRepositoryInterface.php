<?php

namespace NnShop\Domain\Pages\Repository;

use Core\Domain\RepositoryInterface;
use NnShop\Domain\Pages\Entity\Page;
use NnShop\Domain\Pages\Value\PageId;

/**
 * Interface PageRepositoryInterface.
 */
interface PageRepositoryInterface extends RepositoryInterface
{
    /**
     * @param PageId $pageId
     *
     * @return Page
     */
    public function getById(PageId $pageId): Page;
}
