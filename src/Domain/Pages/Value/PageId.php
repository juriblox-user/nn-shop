<?php

namespace NnShop\Domain\Pages\Value;

use Core\Domain\Value\AbstractUuidValue;

/**
 * Class PageId.
 */
class PageId extends AbstractUuidValue
{
}
