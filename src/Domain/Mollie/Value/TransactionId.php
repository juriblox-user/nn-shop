<?php

namespace NnShop\Domain\Mollie\Value;

use Core\Domain\Value\AbstractStringValue;

class TransactionId extends AbstractStringValue
{
    /**
     * Controleren of het ID eruit ziet als een geldig Mollie transactie ID.
     *
     * @param $input
     *
     * @return bool
     */
    public static function valid($input): bool
    {
        return preg_match('/^tr_[a-zA-Z0-9]+$/', $input) > 0;
    }

    /**
     * {@inheritdoc}
     */
    protected function setValue(string $value)
    {
        if (!self::valid($value)) {
            throw new \InvalidArgumentException('This does not look like a valid (Mollie) TransactionId');
        }

        parent::setValue($value);
    }
}
