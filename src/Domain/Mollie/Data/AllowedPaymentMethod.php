<?php

namespace NnShop\Domain\Mollie\Data;

use NnShop\Domain\Orders\Enumeration\PaymentMethod;
use Mollie\Api\Resources\Method as MolliePaymentMethod;

/**
 * Class AllowedPaymentMethod.
 */
class AllowedPaymentMethod
{
    /** @var string */
    private $id;

    /** @var MolliePaymentMethod */
    private $method;

    /** @var PaymentMethod */
    private $paymentMethod;

    /** @var bool */
    private $externallyDisabled;

    /** @var bool */
    private $enabled;

    /**
     * AllowedPaymentMethodObject constructor.
     *
     * @param string                   $id
     * @param bool                     $externallyDisabled
     * @param bool                     $enabled
     * @param MolliePaymentMethod|null $method
     * @param PaymentMethod|null       $paymentMethod
     */
    public function __construct(string $id, bool $externallyDisabled, bool $enabled, MolliePaymentMethod $method = null, PaymentMethod $paymentMethod = null)
    {
        $this->id = $id;
        $this->externallyDisabled = $externallyDisabled;
        $this->enabled = $enabled;
        $this->method = $method;
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return MolliePaymentMethod
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return bool
     */
    public function isExternallyDisabled(): bool
    {
        return $this->externallyDisabled;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @return PaymentMethod
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }
}
