<?php

namespace NnShop\Domain\Mollie\Event\Transaction;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Mollie\Value\TransactionId;

class ReceivedTransactionNotificationEvent
{
    /**
     * @Serializer\Type("NnShop\Domain\Mollie\Value\TransactionId")
     *
     * @var TransactionId
     */
    private $transactionId;

    /**
     * ReceivedPaymentNotification constructor.
     *
     * @param TransactionId $transactionId
     */
    public function __construct(TransactionId $transactionId)
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @return TransactionId
     */
    public function getTransactionId(): TransactionId
    {
        return $this->transactionId;
    }
}
