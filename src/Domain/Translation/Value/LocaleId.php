<?php

namespace NnShop\Domain\Translation\Value;

use Core\Domain\Value\AbstractUuidValue;

/**
 * Class LocaleId.
 */
class LocaleId extends AbstractUuidValue
{
}
