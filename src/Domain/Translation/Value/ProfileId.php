<?php

namespace NnShop\Domain\Translation\Value;

use Core\Domain\Value\AbstractUuidValue;

/**
 * Class ProfileId.
 */
class ProfileId extends AbstractUuidValue
{
}
