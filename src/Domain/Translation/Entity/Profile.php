<?php

namespace NnShop\Domain\Translation\Entity;

use Core\Annotation\Doctrine as Core;
use Core\Common\Exception\Domain\DuplicateEntityException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;

use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Entity\Invoice;
use NnShop\Domain\Pages\Entity\Page;
use NnShop\Domain\Shops\Entity\LandingPage;
use NnShop\Domain\Shops\Entity\Referrer;
use NnShop\Domain\Shops\Entity\Shop;
use NnShop\Domain\Templates\Entity\CompanyType;
use NnShop\Domain\Templates\Entity\Partner;
use NnShop\Domain\Translation\Value\ProfileId;
use Stringy\Stringy;

/**
 * Class Profile.
 *
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Translation\Repository\ProfileDoctrineRepository")
 * @Core\QueryBuilder(class="NnShop\Infrastructure\Translation\QueryBuilder\ProfileDoctrineQueryBuilder")
 */
class Profile
{
    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $addressLine1;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string
     */
    private $addressLine2;

    /**
     * @ORM\Column(type="json_array")
     *
     * @var ArrayCollection
     */
    private $allowedPaymentMethods;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $city;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $cocLabel;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $cocNumber;

    /**
     * @ORM\OneToMany(targetEntity="NnShop\Domain\Templates\Entity\CompanyType", mappedBy="profile")
     */
    private $companyTypes;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $country;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $currency;

    /**
     * @ORM\OneToMany(targetEntity="NnShop\Domain\Orders\Entity\Customer", mappedBy="profile")
     *
     * @var Collection|Selectable|Customer[]
     */
    private $customers;

    /**
     * @ORM\Column(type="string", name="profile_email")
     *
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $entityCountry;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $entityName;

    /**
     * @ORM\Id
     * @ORM\Column(type="profiles.profile_id")
     *
     * @var ProfileId
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="NnShop\Domain\Orders\Entity\Invoice", mappedBy="profile")
     */
    private $invoices;

    /**
     * @ORM\OneToMany(targetEntity="NnShop\Domain\Shops\Entity\LandingPage", mappedBy="profile")
     */
    private $landingPages;

    /**
     * @var \Doctrine\Common\Collections\Collection|Locale[]
     *
     * @ORM\ManyToMany(targetEntity="Locale", inversedBy="profiles")
     * @ORM\JoinTable(
     *     name="translation_profile_locale",
     *     joinColumns={
     *         @ORM\JoinColumn(name="profile_id", referencedColumnName="profile_id")
     *     },
     *     inverseJoinColumns={
     *         @ORM\JoinColumn(name="locale_id", referencedColumnName="locale_id")
     *     }
     * )
     */
    private $locales;

    /**
     * @var Locale
     * @ORM\ManyToOne(targetEntity="Locale", inversedBy="mainProfiles")
     * @ORM\JoinColumn(name="main_locale_id", referencedColumnName="locale_id", onDelete="SET NULL")
     */
    private $mainLocale;

    /**
     * @ORM\Column(type="string", name="profile_mollie")
     *
     * @var string
     */
    private $mollieKey;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Pages\Entity\Page", mappedBy="profile")
     */
    private $pages;

    /**
     * @ORM\OneToMany(targetEntity="NnShop\Domain\Templates\Entity\Partner", mappedBy="countryProfile")
     *
     * @var PersistentCollection
     */
    private $partners;

    /**
     * @ORM\Column(type="string", name="profile_phone")
     *
     * @var string
     */
    private $phone;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string
     */
    private $province;

    /**
     * @ORM\OneToMany(targetEntity="NnShop\Domain\Shops\Entity\Referrer", mappedBy="profile")
     */
    private $referrers;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Shops\Entity\Shop", mappedBy="profile")
     */
    private $shops;

    /**
     * @ORM\Column(length=255, unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $timezone;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $url;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $vatLabel;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $vatNumber;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $zipcode;

    /**
     * Profile constructor.
     *
     * @param ProfileId $id
     * @param Locale    $locale
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    public function __construct(ProfileId $id, Locale $locale)
    {
        $this->id = $id;

        $this->customers = new ArrayCollection();
        $this->locales = new ArrayCollection();
        $this->pages = new ArrayCollection();
        $this->partners = new ArrayCollection();
        $this->companyTypes = new ArrayCollection();
        $this->referrers = new ArrayCollection();
        $this->landingPages = new ArrayCollection();
        $this->invoices = new ArrayCollection();
        $this->shops = new ArrayCollection();
        $this->allowedPaymentMethods = [];

        $this->mainLocale = $locale;
        $this->mainLocale->addMainProfile($this);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->url;
    }

    /**
     * @param ProfileId $id
     * @param Locale    $locale
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     *
     * @return Profile
     */
    public static function create(ProfileId $id, Locale $locale): self
    {
        return new self($id, $locale);
    }

    /**
     * @param CompanyType $companyType
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    public function addCompanyType(CompanyType $companyType)
    {
        if ($this->hasCompanyType($companyType)) {
            throw new DuplicateEntityException(sprintf('Duplicate CompanyType %s', $companyType->getId()));
        }

        $this->companyTypes->add($companyType);
    }

    /**
     * @param Customer $customer
     *
     * @internal
     */
    public function addCustomer(Customer $customer)
    {
        $this->customers->add($customer);
    }

    /**
     * @param Invoice $invoice
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    public function addInvoice(Invoice $invoice)
    {
        if ($this->hasInvoice($invoice)) {
            throw new DuplicateEntityException(sprintf('Duplicate Invoice %s', $invoice->getId()));
        }

        $this->invoices->add($invoice);
    }

    /**
     * @param LandingPage $landingPage
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    public function addLandingPage(LandingPage $landingPage)
    {
        if ($this->hasLandingPage($landingPage)) {
            throw new DuplicateEntityException(sprintf('Duplicate LandingPage %s', $landingPage->getId()));
        }

        $this->landingPages->add($landingPage);
    }

    /**
     * @param Locale $locale
     */
    public function addLocale(Locale $locale)
    {
        $locale->addProfile($this);
        $this->locales[] = $locale;
    }

    /**
     * @param Page $page
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    public function addPage(Page $page)
    {
        if ($this->hasPage($page)) {
            throw new DuplicateEntityException(sprintf('Duplicate Page %s', $page->getId()));
        }

        $this->pages->add($page);
    }

    /**
     * @param Partner $partner
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    public function addPartner(Partner $partner)
    {
        if ($this->hasPartner($partner)) {
            throw new DuplicateEntityException(sprintf('Duplicate Partner %s', $partner->getId()));
        }

        $this->partners->add($partner);
    }

    /**
     * @param Referrer $referrer
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    public function addReferrer(Referrer $referrer)
    {
        if ($this->hasReferrer($referrer)) {
            throw new DuplicateEntityException(sprintf('Duplicate Referrer %s', $referrer->getId()));
        }

        $this->referrers->add($referrer);
    }

    /**
     * @param Shop $shop
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    public function addShop(Shop $shop)
    {
        if ($this->hasShop($shop)) {
            throw new DuplicateEntityException(sprintf('Duplicate Shop %s', $shop->getId()));
        }

        $this->shops->add($shop);
    }

    /**
     * @return string
     */
    public function getAddressLine1()
    {
        return $this->addressLine1;
    }

    /**
     * @return string
     */
    public function getAddressLine2()
    {
        return $this->addressLine2;
    }

    /**
     * @return array
     */
    public function getAllowedPaymentMethods(): array
    {
        return $this->allowedPaymentMethods;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getCocLabel()
    {
        return $this->cocLabel;
    }

    /**
     * @return string
     */
    public function getCocNumber()
    {
        return $this->cocNumber;
    }

    /**
     * @return string|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @return string|null
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getEntityCountry()
    {
        return $this->entityCountry;
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return $this->entityName;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return parse_url($this->url, PHP_URL_HOST);
    }

    /**
     * @return ProfileId
     */
    public function getId(): ProfileId
    {
        return $this->id;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|Locale[]
     */
    public function getLocales()
    {
        return $this->locales;
    }

    /**
     * @return Locale
     */
    public function getMainLocale(): Locale
    {
        return $this->mainLocale;
    }

    /**
     * @return string
     */
    public function getMollieKey()
    {
        return $this->mollieKey;
    }

    /**
     * @return PersistentCollection
     */
    public function getPartners(): PersistentCollection
    {
        return $this->partners;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * @return string
     */
    public function getScheme(): string
    {
        return parse_url($this->url, PHP_URL_SCHEME);
    }

    /**
     * @return Collection
     */
    public function getShops(): Collection
    {
        return $this->shops;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string|null
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getVatLabel()
    {
        return $this->vatLabel;
    }

    /**
     * @return string
     */
    public function getVatNumber()
    {
        return $this->vatNumber;
    }

    /**
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param CompanyType $companyType
     *
     * @return bool
     */
    public function hasCompanyType(CompanyType $companyType): bool
    {
        return $this->companyTypes->contains($companyType);
    }

    /**
     * @param Invoice $invoice
     *
     * @return bool
     */
    public function hasInvoice(Invoice $invoice): bool
    {
        return $this->invoices->contains($invoice);
    }

    /**
     * @param LandingPage $landingPage
     *
     * @return bool
     */
    public function hasLandingPage(LandingPage $landingPage): bool
    {
        return $this->landingPages->contains($landingPage);
    }

    /**
     * @param Page $page
     *
     * @return bool
     */
    public function hasPage(Page $page): bool
    {
        return $this->pages->contains($page);
    }

    /**
     * @param Partner $partner
     *
     * @return bool
     */
    public function hasPartner(Partner $partner): bool
    {
        return $this->partners->contains($partner);
    }

    /**
     * @param Referrer $referrer
     *
     * @return bool
     */
    public function hasReferrer(Referrer $referrer): bool
    {
        return $this->referrers->contains($referrer);
    }

    /**
     * @param Shop $shop
     *
     * @return bool
     */
    public function hasShop(Shop $shop): bool
    {
        return $this->shops->contains($shop);
    }

    /**
     * @param Customer $customer
     *
     * @internal
     */
    public function removeCustomer(Customer $customer)
    {
        $this->customers->remove($customer);
    }

    /**
     * @param string $addressLine1
     */
    public function setAddressLine1(string $addressLine1)
    {
        $this->addressLine1 = $addressLine1;
    }

    /**
     * @param string $addressLine2|null
     */
    public function setAddressLine2($addressLine2)
    {
        $this->addressLine2 = $addressLine2;
    }

    /**
     * @param array $allowedPaymentMethods
     *
     * @return Profile
     */
    public function setAllowedPaymentMethods(array $allowedPaymentMethods)
    {
        $this->allowedPaymentMethods = $allowedPaymentMethods;

        return $this;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city)
    {
        $this->city = $city;
    }

    /**
     * @param string $cocLabel
     */
    public function setCocLabel(string $cocLabel)
    {
        $this->cocLabel = $cocLabel;
    }

    /**
     * @param string $cocNumber
     */
    public function setCocNumber(string $cocNumber)
    {
        $this->cocNumber = $cocNumber;
    }

    /**
     * @param string $country
     *
     * @return Profile
     */
    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @param string $currency
     *
     * @return Profile
     */
    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param string $entityCountry
     */
    public function setEntityCountry(string $entityCountry)
    {
        $this->entityCountry = $entityCountry;
    }

    /**
     * @param string $entityName
     */
    public function setEntityName(string $entityName)
    {
        $this->entityName = $entityName;
    }

    /**
     * @param Locale|null $locale
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     *
     * @return Profile
     */
    public function setMainLocale(Locale $locale = null): self
    {
        $this->mainLocale = $locale;

        return $this;
    }

    /**
     * @param string $mollieKey
     */
    public function setMollieKey($mollieKey)
    {
        $apiKeyString = Stringy::create($mollieKey);
        if (!$apiKeyString->startsWith('test_') && !$apiKeyString->startsWith('live_')) {
            throw new \InvalidArgumentException(sprintf('The Mollie API key "%s" is invalid', $mollieKey));
        }

        $this->mollieKey = $mollieKey ?: null;
    }

    /**
     * @param string $phone
     *
     * @return self
     */
    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @param string $province|null
     */
    public function setProvince($province)
    {
        $this->province = $province;
    }

    /**
     * @param string $slug
     *
     * @return Profile
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @param string $timezone
     *
     * @return Profile
     */
    public function setTimezone(string $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * @param string $url
     *
     * @return Profile
     */
    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @param string $vatLabel
     */
    public function setVatLabel(string $vatLabel)
    {
        $this->vatLabel = $vatLabel;
    }

    /**
     * @param string $vatNumber
     */
    public function setVatNumber(string $vatNumber)
    {
        $this->vatNumber = $vatNumber;
    }

    /**
     * @param string $zipcode
     */
    public function setZipcode(string $zipcode)
    {
        $this->zipcode = $zipcode;
    }
}
