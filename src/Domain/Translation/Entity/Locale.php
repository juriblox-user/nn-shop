<?php

namespace NnShop\Domain\Translation\Entity;

use Core\Annotation\Doctrine as Core;
use Core\Common\Exception\Domain\DuplicateEntityException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Translation\Value\LocaleId;

/**
 * Class Locale.
 *
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Translation\Repository\LocaleDoctrineRepository")
 * @Core\QueryBuilder(class="NnShop\Infrastructure\Translation\QueryBuilder\LocaleDoctrineQueryBuilder")
 */
class Locale
{
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\OneToMany(targetEntity="NnShop\Domain\Orders\Entity\Customer", mappedBy="locale")
     *
     * @var Collection|Selectable|Customer[]
     */
    private $customers;

    /**
     * @ORM\Id
     * @ORM\Column(type="locales.locale_id")
     *
     * @var LocaleId
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $locale;

    /**
     * @ORM\OneToMany(targetEntity="Profile", mappedBy="mainLocale")
     */
    private $mainProfiles;

    /**
     * @var \Doctrine\Common\Collections\Collection|Profile[]
     * @ORM\ManyToMany(targetEntity="Profile", mappedBy="locales")
     */
    private $profiles;

    /**
     * @Gedmo\Slug(fields={"locale"})
     * @ORM\Column(length=255, unique=true)
     */
    private $slug;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * Locale constructor.
     *
     * @param LocaleId $id
     */
    private function __construct(LocaleId $id)
    {
        $this->id = $id;

        $this->customers = new ArrayCollection();
        $this->profiles = new ArrayCollection();
        $this->mainProfiles = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->locale;
    }

    /**
     * @param LocaleId $id
     *
     * @return Locale
     */
    public static function create(LocaleId $id): self
    {
        return new self($id);
    }

    /**
     * @param Customer $customer
     *
     * @internal
     */
    public function addCustomer(Customer $customer)
    {
        $this->customers->add($customer);
    }

    /**
     * @param Profile $profile
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    public function addMainProfile(Profile $profile)
    {
        if ($this->hasMainProfile($profile)) {
            throw new DuplicateEntityException(sprintf('Duplicate Profile %s', $profile->getId()));
        }

        $this->mainProfiles->add($profile);
    }

    /**
     * @param Profile $profile
     *
     * @return Locale
     */
    public function addProfile(Profile $profile): self
    {
        $this->profiles[] = $profile;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @return LocaleId
     */
    public function getId(): LocaleId
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|Locale[]
     */
    public function getProfiles()
    {
        return $this->profiles;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @param Profile $profile
     *
     * @return bool
     */
    public function hasMainProfile(Profile $profile): bool
    {
        return $this->mainProfiles->contains($profile);
    }

    /**
     * @param Customer $customer
     *
     * @internal
     */
    public function removeCustomer(Customer $customer)
    {
        $this->customers->remove($customer);
    }

    /**
     * @param string $locale
     *
     * @return Locale
     */
    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }
}
