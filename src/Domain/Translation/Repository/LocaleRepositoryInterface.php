<?php

namespace NnShop\Domain\Translation\Repository;

use Core\Domain\RepositoryInterface;
use NnShop\Domain\Translation\Entity\Locale;
use NnShop\Domain\Translation\Value\LocaleId;

/**
 * Interface ProfileRepositoryInterface.
 */
interface LocaleRepositoryInterface extends RepositoryInterface
{
    /**
     * @param LocaleId $id
     *
     * @return Locale
     */
    public function getById(LocaleId $id): Locale;

    /**
     * @param LocaleId $id
     *
     * @return Locale
     */
    public function getReference(LocaleId $id): Locale;
}
