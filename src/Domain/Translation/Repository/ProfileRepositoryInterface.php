<?php

namespace NnShop\Domain\Translation\Repository;

use Core\Domain\RepositoryInterface;
use NnShop\Domain\Translation\Entity\Profile;
use NnShop\Domain\Translation\Value\ProfileId;

/**
 * Interface ProfileRepositoryInterface.
 */
interface ProfileRepositoryInterface extends RepositoryInterface
{
    /**
     * @param string $host
     *
     * @return Profile|null
     */
    public function findByHost(string $host);

    /**
     * @param string $slug
     *
     * @return Profile|null
     */
    public function findOneBySlug(string $slug);

    /**
     * @param ProfileId $id
     *
     * @return Profile
     */
    public function getById(ProfileId $id): Profile;

    /**
     * @return Profile|null
     */
    public function getFirst();

    /**
     * @return array
     *
     * Return entities for backend menus for linked entities
     */
    public function getMenu(): array;

    /**
     * @param ProfileId $id
     *
     * @return Profile
     */
    public function getReference(ProfileId $id): Profile;
}
