<?php

namespace NnShop\Domain\Translation\QueryBuilder;

use Core\Doctrine\ORM\QueryBuilderInterface;

interface LocaleQueryBuilderInterface extends QueryBuilderInterface
{
}
