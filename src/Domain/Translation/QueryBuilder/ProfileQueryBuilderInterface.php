<?php

namespace NnShop\Domain\Translation\QueryBuilder;

use Core\Doctrine\ORM\QueryBuilderInterface;

interface ProfileQueryBuilderInterface extends QueryBuilderInterface
{
}
