<?php

namespace NnShop\Domain\Translation\Command\AllowedPaymentMethod;

use NnShop\Domain\Mollie\Data\AllowedPaymentMethod;
use NnShop\Domain\Translation\Value\ProfileId;

/**
 * Class UpdateAllowedPaymentMethodCommand.
 */
class UpdateAllowedPaymentMethodCommand
{
    /**
     * @var ProfileId
     */
    private $profileId;

    /**
     * @var string
     */
    private $method;

    /**
     * @var bool
     */
    private $enabled;

    /**
     * @var bool
     */
    private $externallyDisabled;

    /**
     * @var AllowedPaymentMethod
     */
    private $methodObject;

    /**
     * @return ProfileId
     */
    public function getProfileId(): ProfileId
    {
        return $this->profileId;
    }

    /**
     * @param ProfileId $profileId
     *
     * @return self
     */
    public function setProfileId(ProfileId $profileId): self
    {
        $this->profileId = $profileId;

        return $this;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     *
     * @return self
     */
    public function setMethod(string $method): self
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     *
     * @return self
     */
    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return bool
     */
    public function isExternallyDisabled(): bool
    {
        return $this->externallyDisabled;
    }

    /**
     * @param bool $externallyDisabled
     *
     * @return self
     */
    public function setExternallyDisabled(bool $externallyDisabled): self
    {
        $this->externallyDisabled = $externallyDisabled;

        return $this;
    }

    /**
     * @return AllowedPaymentMethod
     */
    public function getMethodObject(): AllowedPaymentMethod
    {
        return $this->methodObject;
    }

    /**
     * @param AllowedPaymentMethod $methodObject
     *
     * @return self
     */
    public function setMethodObject(AllowedPaymentMethod $methodObject): self
    {
        $this->methodObject = $methodObject;

        return $this;
    }
}
