<?php

namespace NnShop\Domain\Translation\Command\Locale;

use NnShop\Domain\Translation\Value\LocaleId;

/**
 * Class CreateLocaleCommand.
 */
class CreateLocaleCommand
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string
     */
    private $locale;

    /**
     * @var array
     */
    private $profiles = [];

    /**
     * CreateLocaleCommand constructor.
     *
     * @param LocaleId $id
     */
    private function __construct(LocaleId $id)
    {
        $this->id = $id;
    }

    /**
     * @param string $locale
     */
    public function setLocale(string $locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @param array $profiles
     */
    public function setProfiles(array $profiles)
    {
        $this->profiles = $profiles;
    }

    /**
     * @return array
     */
    public function getProfiles(): array
    {
        return $this->profiles;
    }

    /**
     * @param LocaleId $id
     *
     * @return CreateLocaleCommand
     */
    public static function create(LocaleId $id): self
    {
        return new static($id);
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }
}
