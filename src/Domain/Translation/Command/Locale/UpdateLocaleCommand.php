<?php

namespace NnShop\Domain\Translation\Command\Locale;

use NnShop\Domain\Translation\Value\LocaleId;

/**
 * Class UpdateLocaleCommand.
 */
class UpdateLocaleCommand
{
    /**
     * @var LocaleId
     */
    private $id;

    /**
     * @var string
     */
    private $locale;

    /**
     * UpdateLocaleCommand constructor.
     *
     * @param LocaleId $id
     */
    private function __construct(LocaleId $id)
    {
        $this->id = $id;
    }

    /**
     * @param string $locale
     */
    public function setLocale(string $locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @param LocaleId $id
     *
     * @return UpdateLocaleCommand
     */
    public static function update(LocaleId $id): self
    {
        return new static($id);
    }

    /**
     * @return LocaleId
     */
    public function getId(): LocaleId
    {
        return $this->id;
    }
}
