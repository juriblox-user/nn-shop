<?php

namespace NnShop\Domain\Translation\Command\Profile;

use NnShop\Domain\Translation\Entity\Locale;
use NnShop\Domain\Translation\Value\ProfileId;

/**
 * Class UpdateProfileCommand.
 */
class UpdateProfileCommand
{
    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var array
     */
    private $locales = [];

    /**
     * @var Locale|null
     */
    private $mainLocale;

    /**
     * @var string
     */
    private $url;

    /**
     * @var ProfileId
     */
    private $id;

    /**
     * @var string
     */
    private $mollieKey;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $entityName;

    /**
     * @var string
     */
    private $addressLine1;

    /**
     * @var string
     */
    private $addressLine2;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $province;

    /**
     * @var string
     */
    private $zipcode;

    /**
     * @var string
     */
    private $entityCountry;

    /**
     * @var string
     */
    private $vatLabel;

    /**
     * @var string
     */
    private $vatNumber;

    /**
     * @var string
     */
    private $cocLabel;

    /**
     * @var string
     */
    private $cocNumber;

    /**
     * UpdateProfileCommand constructor.
     *
     * @param ProfileId $id
     */
    private function __construct(ProfileId $id)
    {
        $this->id = $id;
    }

    /**
     * @param ProfileId $id
     *
     * @return UpdateProfileCommand
     */
    public static function create(ProfileId $id): self
    {
        return new static($id);
    }

    /**
     * @return ProfileId
     */
    public function getId(): ProfileId
    {
        return $this->id;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $timezone
     */
    public function setTimezone(string $timezone)
    {
        $this->timezone = $timezone;
    }

    /**
     * @return string
     */
    public function getTimezone(): string
    {
        return $this->timezone;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param array $locales
     */
    public function setLocales(array $locales)
    {
        $this->locales = $locales;
    }

    /**
     * @return array
     */
    public function getLocales(): array
    {
        return $this->locales;
    }

    /**
     * @param Locale $locale
     */
    public function setMainLocale(Locale $locale)
    {
        $this->mainLocale = $locale;
    }

    /**
     * @return Locale|null
     */
    public function getMainLocale()
    {
        return $this->mainLocale;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getMollieKey(): string
    {
        return $this->mollieKey;
    }

    /**
     * @param string $mollieKey
     */
    public function setMollieKey(string $mollieKey)
    {
        $this->mollieKey = $mollieKey;
    }

    /**
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string|null
     */
    public function getEntityName()
    {
        return $this->entityName;
    }

    /**
     * @param string $entityName
     */
    public function setEntityName(string $entityName)
    {
        $this->entityName = $entityName;
    }

    /**
     * @return string|null
     */
    public function getAddressLine1()
    {
        return $this->addressLine1;
    }

    /**
     * @param string $addressLine1
     */
    public function setAddressLine1(string $addressLine1)
    {
        $this->addressLine1 = $addressLine1;
    }

    /**
     * @return string|null
     */
    public function getAddressLine2()
    {
        return $this->addressLine2;
    }

    /**
     * @param string $addressLine2|null
     */
    public function setAddressLine2($addressLine2)
    {
        $this->addressLine2 = $addressLine2;
    }

    /**
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city)
    {
        $this->city = $city;
    }

    /**
     * @return string|null
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * @param string $province|null
     */
    public function setProvince($province)
    {
        $this->province = $province;
    }

    /**
     * @return string|null
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param string $zipcode
     */
    public function setZipcode(string $zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return string|null
     */
    public function getEntityCountry()
    {
        return $this->entityCountry;
    }

    /**
     * @param string $entityCountry
     */
    public function setEntityCountry(string $entityCountry)
    {
        $this->entityCountry = $entityCountry;
    }

    /**
     * @return string|null
     */
    public function getVatLabel()
    {
        return $this->vatLabel;
    }

    /**
     * @param string $vatLabel
     */
    public function setVatLabel(string $vatLabel)
    {
        $this->vatLabel = $vatLabel;
    }

    /**
     * @return string|null
     */
    public function getVatNumber()
    {
        return $this->vatNumber;
    }

    /**
     * @param string $vatNumber
     */
    public function setVatNumber(string $vatNumber)
    {
        $this->vatNumber = $vatNumber;
    }

    /**
     * @return string|null
     */
    public function getCocLabel()
    {
        return $this->cocLabel;
    }

    /**
     * @param string $cocLabel
     */
    public function setCocLabel(string $cocLabel)
    {
        $this->cocLabel = $cocLabel;
    }

    /**
     * @return string|null
     */
    public function getCocNumber()
    {
        return $this->cocNumber;
    }

    /**
     * @param string $cocNumber
     */
    public function setCocNumber(string $cocNumber)
    {
        $this->cocNumber = $cocNumber;
    }
}
