<?php

namespace NnShop\Domain\System\Repository;

use Core\Common\Exception\Domain\EntityNotFoundException;
use Core\Domain\RepositoryInterface;
use NnShop\Domain\System\Entity\Counter;
use NnShop\Domain\System\Enumeration\CounterName;
use NnShop\Domain\System\Value\CounterId;

interface CounterRepositoryInterface extends RepositoryInterface
{
    /**
     * @param CounterId $id
     *
     * @return Counter|null
     */
    public function findOneById(CounterId $id);

    /**
     * @param CounterName $name
     *
     * @return Counter|null
     */
    public function findOneByName(CounterName $name);

    /**
     * @param CounterId $id
     *
     * @throws EntityNotFoundException
     *
     * @return Counter
     */
    public function getById(CounterId $id): Counter;

    /**
     * @param CounterName $name
     *
     * @throws EntityNotFoundException
     *
     * @return Counter
     */
    public function getByName(CounterName $name): Counter;

    /**
     * @param CounterId $id
     *
     * @return Counter
     */
    public function getReference(CounterId $id): Counter;
}
