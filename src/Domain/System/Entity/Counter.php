<?php

namespace NnShop\Domain\System\Entity;

use Doctrine\ORM\Mapping as ORM;
use NnShop\Domain\System\Enumeration\CounterName;
use NnShop\Domain\System\Value\CounterId;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\System\Repository\CounterDoctrineRepository")
 */
class Counter
{
    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $amount;

    /**
     * @ORM\Id
     * @ORM\Column(type="system.counter_id")
     *
     * @var CounterId
     */
    private $id;

    /**
     * @ORM\Column(type="system.counter_name", unique=true)
     *
     * @var CounterName
     */
    private $name;

    /**
     * Counter constructor.
     *
     * @param CounterId   $id
     * @param CounterName $name
     */
    public function __construct(CounterId $id, CounterName $name)
    {
        $this->id = $id;
        $this->name = $name;

        $this->amount = 0;
    }

    /**
     * @param CounterId   $id
     * @param CounterName $name
     *
     * @return Counter
     */
    public static function create(CounterId $id, CounterName $name): self
    {
        return new self($id, $name);
    }

    /**
     * Teller verlagen.
     */
    public function decrease()
    {
        --$this->amount;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return CounterId
     */
    public function getId(): CounterId
    {
        return $this->id;
    }

    /**
     * @return CounterName
     */
    public function getName(): CounterName
    {
        return $this->name;
    }

    /**
     * Teller ophogen.
     */
    public function increase()
    {
        ++$this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount(int $amount)
    {
        $this->amount = $amount;
    }
}
