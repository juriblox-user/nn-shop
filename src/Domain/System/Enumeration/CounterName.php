<?php

namespace NnShop\Domain\System\Enumeration;

use Core\Domain\Enumeration\AbstractEnumeration;

class CounterName extends AbstractEnumeration
{
    /**
     * Totaal aantal gegenereerde documenten.
     */
    const GENERATED_DOCUMENTS = 'documents.generated';

    /**
     * Totaal aantal bestellingen.
     */
    const TOTAL_ORDERS = 'orders.total';

    /**
     * Totaal aantal templates.
     */
    const TOTAL_TEMPLATES = 'templates.total';
}
