<?php

namespace NnShop\Domain\System\Command\Counter;

use NnShop\Domain\System\Enumeration\CounterName;
use NnShop\Domain\System\Value\CounterId;

class CreateCounterCommand
{
    /**
     * @var CounterId
     */
    private $id;

    /**
     * @var CounterName
     */
    private $name;

    /**
     * CreateCounterCommand constructor.
     *
     * @param CounterId   $id
     * @param CounterName $name
     */
    private function __construct(CounterId $id, CounterName $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @param CounterId   $id
     * @param CounterName $name
     *
     * @return CreateCounterCommand
     */
    public static function create(CounterId $id, CounterName $name): self
    {
        return new self($id, $name);
    }

    /**
     * @return CounterId
     */
    public function getId(): CounterId
    {
        return $this->id;
    }

    /**
     * @return CounterName
     */
    public function getName(): CounterName
    {
        return $this->name;
    }
}
