<?php

namespace NnShop\Domain\System\Command\Counter;

use NnShop\Domain\System\Enumeration\CounterName;
use NnShop\Domain\System\Value\CounterId;

class DecreaseCounterCommand
{
    /**
     * @var CounterId
     */
    private $id;

    /**
     * @var CounterName
     */
    private $name;

    /**
     * DecreaseCounterCommand constructor.
     */
    private function __construct()
    {
    }

    /**
     * @param CounterId $id
     *
     * @return DecreaseCounterCommand
     */
    public static function fromId(CounterId $id): self
    {
        $command = new self();
        $command->id = $id;

        return $command;
    }

    /**
     * @param CounterName $name
     *
     * @return DecreaseCounterCommand
     */
    public static function fromName(CounterName $name): self
    {
        $command = new self();
        $command->name = $name;

        return $command;
    }

    /**
     * @return CounterId
     */
    public function getId(): CounterId
    {
        return $this->id;
    }

    /**
     * @return CounterName
     */
    public function getName(): CounterName
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return null !== $this->id;
    }

    /**
     * @return bool
     */
    public function hasName(): bool
    {
        return null !== $this->name;
    }
}
