<?php

namespace NnShop\Domain\Templates\Services\Validator;

use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use NnShop\Domain\Templates\Value\TemplateKey;

class TemplateKeyValidator
{
    /**
     * @var TemplateRepositoryInterface
     */
    private $repository;

    /**
     * TemplateKeyValidator constructor.
     *
     * @param TemplateRepositoryInterface $repository
     */
    public function __construct(TemplateRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param TemplateKey $key
     *
     * @return bool
     */
    public function isUnique(TemplateKey $key): bool
    {
        return !$this->repository->hasWithKey($key);
    }
}
