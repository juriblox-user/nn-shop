<?php

namespace NnShop\Domain\Templates\Services\Generator;

use Core\Common\Generator\Security\TokenGenerator;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use NnShop\Domain\Templates\Services\Validator\TemplateKeyValidator;
use NnShop\Domain\Templates\Value\TemplateKey;
use NnShop\Infrastructure\Templates\TemplateKeyType;

class TemplateKeyGenerator
{
    /**
     * @var TemplateRepositoryInterface
     */
    private $repository;

    /**
     * @var TemplateKeyValidator
     */
    private $validator;

    /**
     * TemplateKeyGenerator constructor.
     *
     * @param TemplateRepositoryInterface $repository
     * @param TemplateKeyValidator        $validator
     */
    public function __construct(TemplateRepositoryInterface $repository, TemplateKeyValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * @return TemplateKey
     */
    public function generate(): TemplateKey
    {
        do {
            $key = TemplateKey::from(TokenGenerator::generate(TemplateKeyType::LENGTH));
        } while (!$this->validator->isUnique($key));

        return $key;
    }
}
