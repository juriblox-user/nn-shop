<?php

namespace NnShop\Domain\Templates\Event;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Templates\Value\TemplateId;

class TemplateWasSynchronized
{
    /**
     * @Serializer\Type("NnShop\Domain\Templates\Value\TemplateId")
     *
     * @var TemplateId
     */
    private $id;

    /**
     * TemplateWasSynchronized constructor.
     *
     * @param TemplateId $id
     */
    public function __construct(TemplateId $id)
    {
        $this->id = $id;
    }

    /**
     * @return TemplateId
     */
    public function getId(): TemplateId
    {
        return $this->id;
    }
}
