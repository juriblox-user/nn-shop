<?php

namespace NnShop\Domain\Templates\Event;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Templates\Value\PartnerId;

class PartnerWasCreated
{
    /**
     * @Serializer\Type("NnShop\Domain\Templates\Value\PartnerId")
     *
     * @var PartnerId
     */
    private $id;

    /**
     * PartnerWasCreated constructor.
     *
     * @param PartnerId $id
     */
    public function __construct(PartnerId $id)
    {
        $this->id = $id;
    }

    /**
     * @return PartnerId
     */
    public function getId(): PartnerId
    {
        return $this->id;
    }
}
