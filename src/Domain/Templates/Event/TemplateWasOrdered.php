<?php

namespace NnShop\Domain\Templates\Event;

use NnShop\Domain\Templates\Value\DocumentId;
use NnShop\Domain\Templates\Value\TemplateId;

class TemplateWasOrdered
{
    /**
     * @var DocumentId
     */
    private $documentId;

    /**
     * @var TemplateId
     */
    private $templateId;

    /**
     * TemplateWasOrdered constructor.
     *
     * @param TemplateId $templateId
     * @param DocumentId $documentId
     */
    public function __construct(TemplateId $templateId, DocumentId $documentId)
    {
        $this->templateId = $templateId;
        $this->documentId = $documentId;
    }

    /**
     * @return DocumentId
     */
    public function getDocumentId(): DocumentId
    {
        return $this->documentId;
    }

    /**
     * @return TemplateId
     */
    public function getTemplateId(): TemplateId
    {
        return $this->templateId;
    }
}
