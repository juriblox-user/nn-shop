<?php

namespace NnShop\Domain\Templates\Event\Template;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Templates\Value\TemplateId;

class TemplateCreatedEvent
{
    /**
     * @Serializer\Type("NnShop\Domain\Templates\Value\TemplateId")
     *
     * @var TemplateId
     */
    private $id;

    /**
     * TemplateCreatedEvent constructor.
     *
     * @param TemplateId $id
     */
    private function __construct(TemplateId $id)
    {
        $this->id = $id;
    }

    /**
     * @param TemplateId $id
     *
     * @return TemplateCreatedEvent
     */
    public static function create(TemplateId $id): self
    {
        return new self($id);
    }

    /**
     * @return TemplateId
     */
    public function getId(): TemplateId
    {
        return $this->id;
    }
}
