<?php

namespace NnShop\Domain\Templates\Event;

use NnShop\Domain\Templates\Value\TemplateId;

class TemplateWasDeleted
{
    /**
     * @var TemplateId
     */
    private $id;

    /**
     * TemplateWasDeleted constructor.
     *
     * @param TemplateId $id
     */
    public function __construct(TemplateId $id)
    {
        $this->id = $id;
    }

    /**
     * @return TemplateId
     */
    public function getId(): TemplateId
    {
        return $this->id;
    }
}
