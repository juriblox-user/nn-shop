<?php

namespace NnShop\Domain\Templates\Event;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Templates\Value\DocumentId;

class DocumentGenerationSucceededEvent
{
    /**
     * @Serializer\Type("NnShop\Domain\Templates\Value\DocumentId")
     *
     * @var DocumentId
     */
    private $id;

    /**
     * DocumentGenerationSucceeded constructor.
     *
     * @param DocumentId $id
     */
    private function __construct(DocumentId $id)
    {
        $this->id = $id;
    }

    /**
     * @param DocumentId $id
     *
     * @return DocumentGenerationSucceededEvent
     */
    public static function create(DocumentId $id): self
    {
        return new self($id);
    }

    /**
     * @return DocumentId
     */
    public function getId(): DocumentId
    {
        return $this->id;
    }
}
