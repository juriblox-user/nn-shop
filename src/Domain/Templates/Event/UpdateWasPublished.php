<?php

namespace NnShop\Domain\Templates\Event;

use NnShop\Domain\Templates\Value\UpdateId;

class UpdateWasPublished
{
    /**
     * @var UpdateId
     */
    private $id;

    /**
     * UpdateWasPublished constructor.
     *
     * @param UpdateId $id
     */
    public function __construct(UpdateId $id)
    {
        $this->id = $id;
    }

    /**
     * @return UpdateId
     */
    public function getId(): UpdateId
    {
        return $this->id;
    }
}
