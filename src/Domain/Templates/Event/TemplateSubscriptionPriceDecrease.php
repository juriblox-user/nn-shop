<?php

namespace NnShop\Domain\Templates\Event;

use NnShop\Domain\Templates\Value\TemplateId;
use Money\Money;

class TemplateSubscriptionPriceDecrease
{
    /**
     * @var TemplateId
     */
    private $id;

    /**
     * @var Money|null
     */
    private $oldPrice;

    /**
     * TemplateSubscriptionPriceDecrease constructor.
     *
     * @param TemplateId $id
     * @param Money|null $oldPrice
     */
    public function __construct(TemplateId $id, $oldPrice)
    {
        $this->id = $id;
        $this->oldPrice = $oldPrice;
    }
}
