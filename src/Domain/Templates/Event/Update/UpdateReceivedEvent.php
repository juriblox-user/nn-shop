<?php

namespace NnShop\Domain\Templates\Event\Update;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Templates\Value\TemplateId;
use NnShop\Domain\Templates\Value\TemplateVersion;
use NnShop\Domain\Templates\Value\UpdateId;

class UpdateReceivedEvent
{
    /**
     * @Serializer\Type("NnShop\Domain\Templates\Value\TemplateId")
     *
     * @var TemplateId
     */
    private $templateId;

    /**
     * @Serializer\Type("NnShop\Domain\Templates\Value\UpdateId")
     *
     * @var UpdateId
     */
    private $updateId;

    /**
     * @Serializer\Type("NnShop\Domain\Templates\Value\TemplateVersion")
     *
     * @var TemplateVersion
     */
    private $version;

    /**
     * @param UpdateId        $updateId
     * @param TemplateId      $templateId
     * @param TemplateVersion $version
     */
    private function __construct(UpdateId $updateId, TemplateId $templateId, TemplateVersion $version)
    {
        $this->updateId = $updateId;
        $this->templateId = $templateId;

        $this->version = $version;
    }

    /**
     * @param UpdateId        $updateId
     * @param TemplateId      $templateId
     * @param TemplateVersion $version
     *
     * @return UpdateReceivedEvent
     */
    public static function create(UpdateId $updateId, TemplateId $templateId, TemplateVersion $version): self
    {
        return new self($updateId, $templateId, $version);
    }

    /**
     * @return TemplateId
     */
    public function getTemplateId(): TemplateId
    {
        return $this->templateId;
    }

    /**
     * @return UpdateId
     */
    public function getUpdateId(): UpdateId
    {
        return $this->updateId;
    }

    /**
     * @return TemplateVersion
     */
    public function getVersion(): TemplateVersion
    {
        return $this->version;
    }
}
