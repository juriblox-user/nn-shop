<?php

namespace NnShop\Domain\Templates\Event\Document;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Templates\Value\DocumentId;

class DocumentDownloadedEvent
{
    /**
     * @Serializer\Type("NnShop\Domain\Templates\Value\DocumentId")
     *
     * @var DocumentId
     */
    private $id;

    /**
     * DocumentDownloadedEvent constructor.
     *
     * @param DocumentId $id
     */
    private function __construct(DocumentId $id)
    {
        $this->id = $id;
    }

    /**
     * @param DocumentId $id
     *
     * @return DocumentDownloadedEvent
     */
    public static function create(DocumentId $id): self
    {
        return new self($id);
    }

    /**
     * @return DocumentId
     */
    public function getId(): DocumentId
    {
        return $this->id;
    }
}
