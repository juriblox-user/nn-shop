<?php

namespace NnShop\Domain\Templates\Transformer;

use NnShop\Domain\Templates\Enumeration\QuestionType;

interface QuestionTypeTransformerInterface
{
    /**
     * Convert a remote value to a QuestionType instance.
     *
     * @param $value
     *
     * @return QuestionType
     */
    public static function fromRemote($value): QuestionType;

    /**
     * Convert a QuestionType instance to a remote value.
     *
     * @param QuestionType $type
     *
     * @return mixed
     */
    public static function toRemote(QuestionType $type);
}
