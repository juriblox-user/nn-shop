<?php

namespace NnShop\Domain\Templates\Enumeration;

use Core\Domain\Enumeration\AbstractEnumeration;

class DocumentStatus extends AbstractEnumeration
{
    /**
     * Documentgeneratie is geannuleerd.
     */
    const CANCELED = 'canceled';

    /**
     * Document wordt voorbereid.
     */
    const DRAFT = 'draft';

    /**
     * Documentgeneratie is mislukt.
     */
    const ERROR = 'error';

    /**
     * Document is gegenereerd.
     */
    const GENERATED = 'generated';

    /**
     * Document is aangevraagd.
     */
    const PENDING = 'pending';

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->in([self::CANCELED, self::GENERATED]);
    }

    /**
     * @return bool
     */
    public function isError(): bool
    {
        return $this->is(self::ERROR);
    }

    /**
     * @return bool
     */
    public function isPending(): bool
    {
        return $this->is(self::PENDING);
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->is(self::GENERATED);
    }
}
