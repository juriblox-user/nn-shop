<?php

namespace NnShop\Domain\Templates\Enumeration;

use Core\Domain\Enumeration\AbstractEnumeration;

class QuestionType extends AbstractEnumeration
{
    /**
     * Boolean.
     */
    const TYPE_BOOLEAN = 'boolean';

    /**
     * Radio button.
     */
    const TYPE_CHOICE = 'choice';

    /**
     * Chamber of Commerce.
     */
    const TYPE_COC = 'coc';

    /**
     * Date.
     */
    const TYPE_DATE = 'date';

    /**
     * Dropdown.
     */
    const TYPE_DROPDOWN = 'dropdown';

    /**
     * Infobox.
     */
    const TYPE_INFOBOX = 'infobox';

    /**
     * Long text input.
     */
    const TYPE_MULTILINE_TEXT = 'multiline-text';

    /**
     * Checkbox.
     */
    const TYPE_MULTIPLE_BOOLEANS = 'multiple-booleans';

    /**
     * Numeric.
     */
    const TYPE_NUMERIC = 'numeric';

    /**
     * Price.
     */
    const TYPE_PRICE = 'price';

    /**
     * Short text input.
     */
    const TYPE_SINGLE_LINE_TEXT = 'single-line-text';

    /**
     * Statement ("Ja ik wil"-achtige vraag).
     */
    const TYPE_STATEMENT = 'statement';

    /**
     * Toegestane conversies.
     *
     * @var array
     */
    private static $conversions = [
        self::TYPE_BOOLEAN => [self::TYPE_CHOICE, self::TYPE_DROPDOWN, self::TYPE_MULTIPLE_BOOLEANS],
        self::TYPE_CHOICE => [self::TYPE_BOOLEAN, self::TYPE_DROPDOWN, self::TYPE_MULTIPLE_BOOLEANS],
        self::TYPE_COC => [self::TYPE_SINGLE_LINE_TEXT],
        self::TYPE_DATE => [],
        self::TYPE_DROPDOWN => [self::TYPE_BOOLEAN, self::TYPE_CHOICE, self::TYPE_MULTIPLE_BOOLEANS],
        self::TYPE_MULTILINE_TEXT => [],
        self::TYPE_MULTIPLE_BOOLEANS => [self::TYPE_BOOLEAN, self::TYPE_CHOICE, self::TYPE_DROPDOWN],
        self::TYPE_NUMERIC => [self::TYPE_COC, self::TYPE_PRICE, self::TYPE_SINGLE_LINE_TEXT],
        self::TYPE_PRICE => [self::TYPE_SINGLE_LINE_TEXT],
        self::TYPE_SINGLE_LINE_TEXT => [self::TYPE_COC, self::TYPE_DATE, self::TYPE_DROPDOWN, self::TYPE_NUMERIC, self::TYPE_PRICE, self::TYPE_MULTIPLE_BOOLEANS, self::TYPE_MULTILINE_TEXT],
        self::TYPE_STATEMENT => [],
    ];

    /**
     * Vraagtype biedt de mogelijkheid om meerdere opties te selecteren.
     *
     * @return bool
     */
    public function hasMultipleOptions(): bool
    {
        return $this->in([self::TYPE_MULTIPLE_BOOLEANS]);
    }

    /**
     * @param QuestionType $destinationType
     *
     * @return bool
     */
    public function isAllowedImplicitConversion(self $destinationType): bool
    {
        if (!isset(self::$conversions[$this->getValue()])) {
            throw new \LogicException(sprintf('QuestionType "%s" is not available in the conversions table.', $this->getName()));
        }

        $allowed = in_array($destinationType, self::$conversions[$this->getValue()]);
        if ($allowed && $this->usesOptions() !== $destinationType->usesOptions()) {
            throw new \LogicException(sprintf('The QuestionType::usesOptions() result for %s and %s is not the same. This is weird, because this should be caught by the conversions table.', $this->getName(), $destinationType->getName()));
        }

        return $allowed;
    }

    /**
     * @return array|QuestionType[]
     */
    public function getAllowedConversions()
    {
        if (!isset(self::$conversions[$this->getValue()])) {
            throw new \LogicException(sprintf('QuestionType "%s" is not available in the conversions table.', $this->getName()));
        }

        $types = [];
        foreach (self::$conversions[$this->getValue()] as $value) {
            $types[] = self::from($value);
        }

        return $types;
    }

    /**
     * Vraagtype maakt gebruikt van QuestionOptions.
     *
     * @return bool
     */
    public function usesOptions(): bool
    {
        return $this->in([self::TYPE_BOOLEAN, self::TYPE_CHOICE, self::TYPE_DROPDOWN, self::TYPE_MULTIPLE_BOOLEANS]);
    }
}
