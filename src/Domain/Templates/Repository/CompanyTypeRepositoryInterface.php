<?php

namespace NnShop\Domain\Templates\Repository;

use Core\Domain\RepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;
use NnShop\Domain\Templates\Entity\CompanyType;
use NnShop\Domain\Templates\Value\CompanyTypeId;
use NnShop\Domain\Translation\Entity\Profile;

interface CompanyTypeRepositoryInterface extends RepositoryInterface
{
    /**
     * @param Profile $profile
     *
     * @return array|CompanyType[]
     */
    public function findAllOrderedByTitle(Profile $profile): array;

    /**
     * @param CompanyTypeId $id
     *
     * @return CompanyType|null
     */
    public function findOneById(CompanyTypeId $id);

    /**
     * @param $slug
     *
     * @return CompanyType|null
     */
    public function findOneBySlug(string $slug);

    /**
     * @param CompanyTypeId $id
     *
     * @throws EntityNotFoundException
     *
     * @return CompanyType
     */
    public function getById(CompanyTypeId $id): CompanyType;

    /**
     * @param $slug
     *
     * @throws EntityNotFoundException
     *
     * @return CompanyType
     */
    public function getBySlug(string $slug): CompanyType;

    /**
     * @param CompanyTypeId $id
     *
     * @return CompanyType
     */
    public function getReference(CompanyTypeId $id): CompanyType;

    /**
     * @param $slug
     *
     * @return bool
     */
    public function hasWithSlug(string $slug): bool;

    /**
     * @param Profile $profile
     *
     * @return array
     */
    public function getMenuByProfile(Profile $profile): array;
}
