<?php

namespace NnShop\Domain\Templates\Repository;

use Core\Domain\RepositoryInterface;
use JuriBlox\Sdk\Domain\Documents\Values\QuestionnaireStepId as RemoteId;
use NnShop\Domain\Templates\Entity\Document;
use NnShop\Domain\Templates\Entity\QuestionStep;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Templates\Value\QuestionStepId;

interface QuestionStepRepositoryInterface extends RepositoryInterface
{
    /**
     * @param QuestionStepId $id
     *
     * @return QuestionStep|null
     */
    public function findOneById(QuestionStepId $id);

    /**
     * @param RemoteId      $id
     * @param Template|null $template
     *
     * @return QuestionStep|null
     */
    public function findOneByRemoteId(RemoteId $id, Template $template = null);

    /**
     * @param QuestionStepId $id
     *
     * @return QuestionStep
     */
    public function getById(QuestionStepId $id): QuestionStep;

    /**
     * @param Document $document
     *
     * @return array|QuestionStep[]
     */
    public function getGraph(Document $document): array;

    /**
     * @param QuestionStepId $id
     *
     * @return QuestionStep
     */
    public function getReference(QuestionStepId $id): QuestionStep;
}
