<?php

namespace NnShop\Domain\Templates\Repository;

use Core\Domain\RepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;
use NnShop\Domain\Templates\Entity\Update;
use NnShop\Domain\Templates\Value\UpdateId;

interface UpdateRepositoryInterface extends RepositoryInterface
{
    /**
     * @param UpdateId $id
     *
     * @return Update|null
     */
    public function findOneById(UpdateId $id);

    /**
     * @param UpdateId $id
     *
     * @throws EntityNotFoundException
     *
     * @return Update
     */
    public function getById(UpdateId $id): Update;

    /**
     * @param UpdateId $id
     *
     * @return Update
     */
    public function getReference(UpdateId $id): Update;
}
