<?php

namespace NnShop\Domain\Templates\Repository;

use Core\Domain\RepositoryInterface;
use NnShop\Domain\Templates\Entity\Category;
use NnShop\Domain\Templates\Entity\CategoryTemplate;
use NnShop\Domain\Templates\Entity\Template;

interface CategoryTemplateRepositoryInterface extends RepositoryInterface
{
    /**
     * @param Template $template
     *
     * @return array|CategoryTemplate[]
     */
    public function findByTemplate(Template $template): array;

    /**
     * @param Category $category
     * @param Template $template
     *
     * @return CategoryTemplate
     */
    public function getByCategoryAndTemplate(Category $category, Template $template): CategoryTemplate;
}
