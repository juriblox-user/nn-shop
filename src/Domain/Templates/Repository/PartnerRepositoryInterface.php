<?php

namespace NnShop\Domain\Templates\Repository;

use Core\Domain\RepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;
use JuriBlox\Sdk\Domain\Offices\Values\OfficeId as RemoteId;
use NnShop\Domain\Templates\Entity\Partner;
use NnShop\Domain\Templates\Value\PartnerId;
use NnShop\Domain\Translation\Entity\Profile;

interface PartnerRepositoryInterface extends RepositoryInterface
{
    /**
     * @param string $slug
     *
     * @return mixed
     */
    public function findAllExceptSlug(string $slug): array;

    /**
     * @return array|Partner[]
     */
    public function findBySyncEnabled(): array;

    /**
     * @param PartnerId $id
     *
     * @return Partner|null
     */
    public function findOneById(PartnerId $id);

    /**
     * @param RemoteId $id
     *
     * @return Partner|null
     */
    public function findOneByRemoteId(RemoteId $id);

    /**
     * @param $slug
     *
     * @return Partner|null
     */
    public function findOneBySlug($slug);

    /**
     * @param PartnerId $id
     *
     * @throws EntityNotFoundException
     *
     * @return Partner
     */
    public function getById(PartnerId $id): Partner;

    /**
     * @param RemoteId $id
     *
     * @return Partner|null
     */
    public function getByRemoteId(RemoteId $id);

    /**
     * @param $slug
     *
     * @throws EntityNotFoundException
     *
     * @return Partner
     */
    public function getBySlug($slug): Partner;

    /**
     * @param PartnerId $id
     *
     * @return Partner
     */
    public function getReference(PartnerId $id): Partner;

    /**
     * @param $slug
     *
     * @return bool
     */
    public function hasWithSlug($slug): bool;

    /**
     * @param Profile $profile
     *
     * @return array
     */
    public function getMenuByProfile(Profile $profile): array;

    /**
     * @param int    $remoteId
     * @param string $partnerId
     *
     * @return mixed
     */
    public function remoteIdIsDuplicate(int $remoteId, string $partnerId): bool;

    /**
     * @param Profile $profile
     *
     * @return array
     */
    public function getByProfile(Profile $profile): array;
}
