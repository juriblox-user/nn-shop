<?php

namespace NnShop\Domain\Templates\Repository;

use Core\Domain\RepositoryInterface;
use Doctrine\ORM\QueryBuilder;
use JuriBlox\Sdk\Domain\Documents\Values\TemplateId as RemoteId;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Shops\Entity\LandingPage;
use NnShop\Domain\Shops\Entity\Referrer;
use NnShop\Domain\Templates\Entity\Partner;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Templates\Value\PartnerId;
use NnShop\Domain\Templates\Value\TemplateId;
use NnShop\Domain\Templates\Value\TemplateKey;
use NnShop\Domain\Translation\Entity\Profile;

interface TemplateRepositoryInterface extends RepositoryInterface
{
    /**
     * @return array|Template[]
     */
    public function findAll();

    /**
     * @param PartnerId $partnerId
     *
     * @return array|Template[]
     */
    public function findByPartner(PartnerId $partnerId): array;

    /**
     * @param string $query
     * @return array|Template[]
     */
    public function findPublishedByString(string $query): array;

    /**
     * @param Template $template
     *
     * @return array
     */
    public function findCategories(Template $template): array;

    /**
     * @param TemplateId $id
     *
     * @return Template|null
     */
    public function findOneById(TemplateId $id);

    /**
     * @param TemplateKey $key
     *
     * @return Template|null
     */
    public function findOneByKey(TemplateKey $key);

    /**
     * @param RemoteId     $id
     * @param Partner|null $partner
     *
     * @return Template|null
     */
    public function findOneByRemoteId(RemoteId $id, Partner $partner = null);

    /**
     * @param string $categorySlug
     * @param string $templateSlug
     *
     * @return Template|null
     */
    public function findOneBySlugs(string $categorySlug, string $templateSlug);

    /**
     * @param Template $template
     * @param int      $limit
     *
     * @return array|Order[]
     */
    public function findRecentOrders(Template $template, int $limit): array;

    /**
     * @param int|null $limit
     *
     * @return array|Template[]
     */
    public function findShuffledSpotlight(int $limit = null): array;

    /**
     * @param Profile $profile
     *
     * @return array|Template[]
     */
    public function findVisible(Profile $profile);

    /**
     * @param $activityIds
     *
     * @return array
     */
    public function findVisibleByActivityIds($activityIds): array;

    /**
     * @param LandingPage $landingPage
     *
     * @return Template[]
     */
    public function findVisibleByLandingPage(LandingPage $landingPage): array;

    /**
     * @param Referrer $referrer
     *
     * @return Template[]
     */
    public function findVisibleByReferrer(Referrer $referrer): array;

    /**
     * @param Profile $profile
     *
     * @return array|Template[]
     */
    public function findVisibleWithCategoriesAndShops(Profile $profile);

    /**
     * @param TemplateId $id
     *
     * @return Template
     */
    public function getById(TemplateId $id): Template;

    /**
     * @param TemplateKey $key
     *
     * @return Template
     */
    public function getByKey(TemplateKey $key): Template;

    /**
     * @param $categorySlug
     * @param $templateSlug
     *
     * @return Template
     */
    public function getByLegacySlugs($categorySlug, $templateSlug): Template;

    /**
     * @param RemoteId     $id
     * @param Partner|null $partner
     *
     * @return Template|null
     */
    public function getByRemoteId(RemoteId $id, Partner $partner = null): Template;

    /**
     * @param string $slug
     *
     * @return Template
     */
    public function getBySlug(string $slug): Template;

    /**
     * @param string $categorySlug
     * @param string $templateSlug
     *
     * @return Template
     */
    public function getBySlugs(string $categorySlug, string $templateSlug): Template;

    /**
     * @param TemplateId $id
     *
     * @return Template
     */
    public function getReference(TemplateId $id): Template;

    /**
     * @param string $slug
     *
     * @return Template
     */
    public function getVisibleBySlug(string $slug): Template;

    /**
     * @param TemplateKey $key
     *
     * @return bool
     */
    public function hasWithKey(TemplateKey $key): bool;

    /**
     * @param Profile $profile
     *
     * @return QueryBuilder
     */
    public function getArticleFormTemplates(Profile $profile): QueryBuilder;
}
