<?php

namespace NnShop\Domain\Templates\Repository;

use Core\Domain\RepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;
use NnShop\Domain\Templates\Entity\CompanyActivity;
use NnShop\Domain\Templates\Value\CompanyActivityId;

interface CompanyActivityRepositoryInterface extends RepositoryInterface
{
    /**
     * @param CompanyActivityId $id
     *
     * @return CompanyActivity|null
     */
    public function findOneById(CompanyActivityId $id);

    /**
     * @param $slug
     *
     * @return CompanyActivity|null
     */
    public function findOneBySlug($slug);

    /**
     * @param CompanyActivityId $id
     *
     * @throws EntityNotFoundException
     *
     * @return CompanyActivity
     */
    public function getById(CompanyActivityId $id): CompanyActivity;

    /**
     * @param $slug
     *
     * @throws EntityNotFoundException
     *
     * @return CompanyActivity
     */
    public function getBySlug($slug): CompanyActivity;

    /**
     * @param CompanyActivityId $id
     *
     * @return CompanyActivity
     */
    public function getReference(CompanyActivityId $id): CompanyActivity;

    /**
     * @param $slug
     *
     * @return bool
     */
    public function hasWithSlug($slug): bool;
}
