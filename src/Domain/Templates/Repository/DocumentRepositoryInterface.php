<?php

namespace NnShop\Domain\Templates\Repository;

use Core\Domain\RepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;
use JuriBlox\Sdk\Domain\Documents\Values\DocumentId as RemoteId;
use NnShop\Domain\Templates\Entity\Document;
use NnShop\Domain\Templates\Value\DocumentId;

interface DocumentRepositoryInterface extends RepositoryInterface
{
    /**
     * @param DocumentId $id
     *
     * @return Document|null
     */
    public function findOneById(DocumentId $id);

    /**
     * @param RemoteId $id
     *
     * @return Document|null
     */
    public function findOneByRemoteId(RemoteId $id);

    /**
     * @return array|Document[]
     */
    public function findPending();

    /**
     * @param DocumentId $id
     *
     * @throws EntityNotFoundException
     *
     * @return Document
     */
    public function getById(DocumentId $id): Document;

    /**
     * @param RemoteId $id
     *
     * @return Document
     */
    public function getByRemoteId(RemoteId $id): Document;

    /**
     * @param DocumentId $id
     *
     * @return Document
     */
    public function getReference(DocumentId $id): Document;
}
