<?php

namespace NnShop\Domain\Templates\Repository;

use Core\Domain\RepositoryInterface;
use NnShop\Domain\Templates\Entity\QuestionCondition;
use NnShop\Domain\Templates\Value\QuestionConditionId;

interface QuestionConditionRepositoryInterface extends RepositoryInterface
{
    /**
     * @param QuestionConditionId $id
     *
     * @return QuestionCondition|null
     */
    public function findOneById(QuestionConditionId $id);

    /**
     * @param QuestionConditionId $id
     *
     * @return QuestionCondition
     */
    public function getById(QuestionConditionId $id): QuestionCondition;

    /**
     * @param QuestionConditionId $id
     *
     * @return QuestionCondition
     */
    public function getReference(QuestionConditionId $id): QuestionCondition;
}
