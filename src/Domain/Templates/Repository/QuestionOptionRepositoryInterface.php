<?php

namespace NnShop\Domain\Templates\Repository;

use Core\Domain\RepositoryInterface;
use JuriBlox\Sdk\Domain\Documents\Values\QuestionOptionId as RemoteId;
use NnShop\Domain\Templates\Entity\Question;
use NnShop\Domain\Templates\Entity\QuestionOption;
use NnShop\Domain\Templates\Value\QuestionOptionId;

interface QuestionOptionRepositoryInterface extends RepositoryInterface
{
    /**
     * @param QuestionOptionId $id
     *
     * @return QuestionOption|null
     */
    public function findOneById(QuestionOptionId $id);

    /**
     * @param RemoteId      $id
     * @param Question|null $question
     *
     * @return QuestionOption|null
     */
    public function findOneByLookupId(RemoteId $id, Question $question = null);

    /**
     * @param QuestionOptionId $id
     *
     * @return QuestionOption
     */
    public function getById(QuestionOptionId $id): QuestionOption;

    /**
     * @param RemoteId $lookupId
     *
     * @return QuestionOption
     */
    public function getByLookupId(RemoteId $lookupId): QuestionOption;

    /**
     * @param QuestionOptionId $id
     *
     * @return QuestionOption
     */
    public function getReference(QuestionOptionId $id): QuestionOption;
}
