<?php

namespace NnShop\Domain\Templates\Repository;

use Core\Domain\RepositoryInterface;
use JuriBlox\Sdk\Domain\Documents\Values\DocumentRequestId as RemoteId;
use NnShop\Domain\Templates\Entity\DocumentRequest;
use NnShop\Domain\Templates\Value\DocumentRequestId;

interface DocumentRequestRepositoryInterface extends RepositoryInterface
{
    /**
     * @param DocumentRequestId $id
     *
     * @return DocumentRequest|null
     */
    public function findOneById(DocumentRequestId $id);

    /**
     * @param RemoteId $id
     *
     * @return DocumentRequest|null
     */
    public function findOneByRemoteId(RemoteId $id);

    /**
     * @param DocumentRequestId $id
     *
     * @return DocumentRequest
     */
    public function getById(DocumentRequestId $id): DocumentRequest;

    /**
     * @param RemoteId $id
     *
     * @return DocumentRequest
     */
    public function getByRemoteId(RemoteId $id): DocumentRequest;

    /**
     * @param DocumentRequestId $id
     *
     * @return DocumentRequest
     */
    public function getReference(DocumentRequestId $id): DocumentRequest;
}
