<?php

namespace NnShop\Domain\Templates\Repository;

use Core\Domain\RepositoryInterface;
use JuriBlox\Sdk\Domain\Documents\Values\QuestionId as RemoteId;
use NnShop\Domain\Templates\Entity\Question;
use NnShop\Domain\Templates\Entity\QuestionStep;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Templates\Value\QuestionId;

interface QuestionRepositoryInterface extends RepositoryInterface
{
    /**
     * @param Template $template
     *
     * @return array|Question[]
     */
    public function findByTemplate(Template $template): array;

    /**
     * @param QuestionId $id
     *
     * @return Question|null
     */
    public function findOneById(QuestionId $id);

    /**
     * @param RemoteId          $id
     * @param QuestionStep|null $step
     *
     * @return Question|null
     */
    public function findOneByLookupId(RemoteId $id, QuestionStep $step = null);

    /**
     * @param QuestionId $id
     *
     * @return Question
     */
    public function getById(QuestionId $id): Question;

    /**
     * @param RemoteId          $id
     * @param QuestionStep|null $step
     *
     * @return Question
     */
    public function getByLookupId(RemoteId $id, QuestionStep $step = null): Question;

    /**
     * @param QuestionId $id
     *
     * @return Question
     */
    public function getReference(QuestionId $id): Question;
}
