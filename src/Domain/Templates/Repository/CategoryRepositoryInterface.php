<?php

namespace NnShop\Domain\Templates\Repository;

use Core\Domain\RepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;
use NnShop\Domain\Templates\Entity\Category;
use NnShop\Domain\Templates\Value\CategoryId;
use NnShop\Domain\Translation\Entity\Profile;

interface CategoryRepositoryInterface extends RepositoryInterface
{
    /**
     * @return array|Category[]
     */
    public function findAllWithShop(): array;

    /**
     * @param CategoryId $id
     *
     * @return Category|null
     */
    public function findOneById(CategoryId $id);

    /**
     * @param $slug
     *
     * @return Category|null
     */
    public function findOneBySlug($slug);

    /**
     * @param CategoryId $id
     *
     * @throws EntityNotFoundException
     *
     * @return Category
     */
    public function getById(CategoryId $id): Category;

    /**
     * @param $slug
     *
     * @throws EntityNotFoundException
     *
     * @return Category
     */
    public function getBySlug($slug): Category;

    /**
     * @param CategoryId $id
     *
     * @return Category
     */
    public function getReference(CategoryId $id): Category;

    /**
     * @param $slug
     *
     * @return bool
     */
    public function hasWithSlug($slug): bool;

    /**
     * @param Profile $profile
     *
     * @return array
     */
    public function findAllWithShopAndProfile(Profile $profile): array;
}
