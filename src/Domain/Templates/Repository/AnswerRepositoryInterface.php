<?php

namespace NnShop\Domain\Templates\Repository;

use Core\Domain\RepositoryInterface;
use NnShop\Domain\Templates\Entity\Answer;
use NnShop\Domain\Templates\Entity\Document;
use NnShop\Domain\Templates\Entity\Question;
use NnShop\Domain\Templates\Value\AnswerId;

interface AnswerRepositoryInterface extends RepositoryInterface
{
    /**
     * @param Question $question
     *
     * @return array|Answer[]
     */
    public function findByQuestion(Question $question): array;

    /**
     * @param Document $document
     * @param Question $question
     *
     * @return Answer|null
     */
    public function findOneByDocumentAndQuestion(Document $document, Question $question);

    /**
     * @param AnswerId $id
     *
     * @return Answer|null
     */
    public function findOneById(AnswerId $id);

    /**
     * @param AnswerId $id
     *
     * @return Answer
     */
    public function getById(AnswerId $id): Answer;

    /**
     * @param AnswerId $id
     *
     * @return Answer
     */
    public function getReference(AnswerId $id): Answer;
}
