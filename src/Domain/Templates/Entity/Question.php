<?php

namespace NnShop\Domain\Templates\Entity;

use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\Common\Exception\Domain\EntityNotFoundException;
use Core\Common\Validation\Assertion;
use Core\Domain\Traits\SoftDeleteableEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Comparable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JuriBlox\Sdk\Domain\Documents\Values\QuestionId as RemoteId;
use NnShop\Domain\Templates\Enumeration\QuestionType;
use NnShop\Domain\Templates\Value\QuestionId;
use NnShop\Domain\Templates\Value\QuestionOptionId;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Templates\Repository\QuestionDoctrineRepository")
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(name="ix_lookup", columns={"lookup_id", "timestamp_deleted"}),
 *     @ORM\Index(name="ix_remote", columns={"remote_id", "step_id"}),
 *     @ORM\Index(name="ix_step", columns={"step_id", "timestamp_deleted"})
 * })
 *
 * @Gedmo\SoftDeleteable(fieldName="timestampDeleted")
 */
class Question implements Comparable
{
    use SoftDeleteableEntityTrait;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\Answer", mappedBy="question", cascade={"persist"}, fetch="EXTRA_LAZY")
     *
     * @var Collection|Answer[]
     */
    private $answers;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\QuestionCondition", mappedBy="question", cascade={"persist"})
     *
     * @var Collection|QuestionCondition[]
     */
    private $conditions;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $corrupted;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\QuestionCondition", mappedBy="parent", cascade={"persist"})
     *
     * @var Collection|QuestionCondition[]
     */
    private $dependants;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string
     */
    private $help;

    /**
     * @ORM\Id
     * @ORM\Column(type="templates.question_id")
     *
     * @var QuestionId
     */
    private $id;

    /**
     * @ORM\Column(type="juriblox.question_id")
     *
     * @var RemoteId
     */
    private $lookupId;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\QuestionOption", mappedBy="question", cascade={"persist"})
     *
     * @var Collection|QuestionOption[]
     */
    private $options;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $position;

    /**
     * @ORM\Column(type="juriblox.question_id")
     *
     * @var RemoteId
     */
    private $remoteId;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $required;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\QuestionStep", inversedBy="questions")
     * @ORM\JoinColumn(referencedColumnName="step_id", nullable=true)
     *
     * @var QuestionStep
     */
    private $step;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="templates.question_type")
     *
     * @var QuestionType
     */
    private $type;

    /**
     * Question constructor.
     *
     * @param QuestionId   $id
     * @param QuestionStep $step
     * @param QuestionType $type
     * @param string       $title
     * @param RemoteId     $lookupId
     * @param RemoteId     $remoteId
     */
    private function __construct(QuestionId $id, QuestionStep $step, QuestionType $type, string $title, RemoteId $lookupId, RemoteId $remoteId)
    {
        $this->id = $id;

        $this->lookupId = $lookupId;
        $this->remoteId = $remoteId;

        $this->step = $step;
        $this->step->addQuestion($this);

        $this->type = $type;
        $this->title = $title;

        $this->corrupted = false;
        $this->required = false;

        $this->answers = new ArrayCollection();
        $this->options = new ArrayCollection();

        $this->conditions = new ArrayCollection();
        $this->dependants = new ArrayCollection();

        $this->position = -1;
    }

    /**
     * @param QuestionId   $id
     * @param QuestionStep $step
     * @param QuestionType $type
     * @param string       $title
     * @param RemoteId     $remoteId
     * @param RemoteId     $lookupId
     *
     * @return Question
     */
    public static function create(QuestionId $id, QuestionStep $step, QuestionType $type, string $title, RemoteId $lookupId, RemoteId $remoteId): self
    {
        return new self($id, $step, $type, $title, $lookupId, $remoteId);
    }

    /**
     * @param Answer $answer
     */
    public function addAnswer(Answer $answer)
    {
        if ($this->hasAnswer($answer)) {
            throw new DuplicateEntityException();
        }

        $this->answers->add($answer);
    }

    /**
     * @param QuestionCondition $condition
     */
    public function addCondition(QuestionCondition $condition)
    {
        if ($this->hasCondition($condition)) {
            throw new DuplicateEntityException();
        }

        $this->conditions->add($condition);
    }

    /**
     * @param QuestionCondition $condition
     */
    public function addDependant(QuestionCondition $condition)
    {
        if ($this->hasDependant($condition)) {
            throw new DuplicateEntityException();
        }

        $this->dependants->add($condition);
    }

    /**
     * @param QuestionOption $option
     */
    public function addOption(QuestionOption $option)
    {
        if ($this->hasOption($option)) {
            throw new DuplicateEntityException();
        }

        $this->options->add($option);
    }

    /**
     * {@inheritdoc}
     */
    public function compareTo($other)
    {
        Assertion::isInstanceOf($other, self::class);

        return $other->getId() == $this->id ? 0 : -1;
    }

    /**
     * @param QuestionType $destinationType
     */
    public function convert(QuestionType $destinationType)
    {
        if (!$this->type->isAllowedImplicitConversion($destinationType)) {
            throw new \LogicException(sprintf('It is not possible to implicitly convert a %s to a %s Question', $this->type->getName(), $destinationType->getName()));
        }

        $this->type = $destinationType;
    }

    /**
     * Detach the question from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->step) {
            $this->step->removeQuestion($this);
            $this->step = null;
        }
    }

    /**
     * @return Collection|Answer[]
     */
    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    /**
     * @return Collection|QuestionCondition[]
     */
    public function getConditions(): Collection
    {
        return $this->conditions;
    }

    /**
     * @return Collection
     */
    public function getDefaultVisibleOptions(): Collection
    {
        return $this->options->matching((
            Criteria::create()->where(
                Criteria::expr()->andX(
                    Criteria::expr()->eq('default', true),
                    Criteria::expr()->isNull('timestampDeleted')
                )
            )
        ));
    }

    /**
     * @return string
     */
    public function getHelp()
    {
        return $this->help;
    }

    /**
     * @return QuestionId
     */
    public function getId(): QuestionId
    {
        return $this->id;
    }

    /**
     * @return RemoteId
     */
    public function getLookupId(): RemoteId
    {
        return $this->lookupId;
    }

    /**
     * @param QuestionOptionId $optionId
     *
     * @return QuestionOption
     */
    public function getOption(QuestionOptionId $optionId): QuestionOption
    {
        $matches = $this->options->matching(
            Criteria::create()->where(
                 Criteria::expr()->eq('id', $optionId)
            )
        );

        if ($matches->isEmpty()) {
            throw EntityNotFoundException::fromClassWithCriteria(QuestionOption::class, [
                'id' => $optionId,
            ]);
        }

        return $matches->first();
    }

    /**
     * @return Collection|QuestionOption[]
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @return RemoteId
     */
    public function getRemoteId(): RemoteId
    {
        return $this->remoteId;
    }

    /**
     * @return QuestionStep|null
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return QuestionType
     */
    public function getType(): QuestionType
    {
        return $this->type;
    }

    /**
     * @return Collection|QuestionOption[]
     */
    public function getVisibleOptions(): Collection
    {
        return $this->options->matching((
            Criteria::create()->where(
                Criteria::expr()->isNull('timestampDeleted')
            )
        ));
    }

    /**
     * @param Answer $answer
     *
     * @return bool
     */
    public function hasAnswer(Answer $answer): bool
    {
        return $this->answers->contains($answer);
    }

    /**
     * @param QuestionCondition $condition
     *
     * @return bool
     */
    public function hasCondition(QuestionCondition $condition): bool
    {
        return $this->conditions->contains($condition);
    }

    /**
     * @param QuestionCondition $condition
     *
     * @return bool
     */
    public function hasDependant(QuestionCondition $condition): bool
    {
        return $this->dependants->contains($condition);
    }

    /**
     * @param QuestionOption $option
     *
     * @return bool
     */
    public function hasOption(QuestionOption $option): bool
    {
        return $this->options->contains($option);
    }

    /**
     * @return bool
     */
    public function isCorrupted(): bool
    {
        return $this->corrupted;
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return $this->required;
    }

    /**
     * @param QuestionStep $step
     */
    public function move(QuestionStep $step)
    {
        $this->unlinkStep();

        $this->step = $step;
        $this->step->addQuestion($this);
    }

    /**
     * @param Answer $answer
     */
    public function removeAnswer(Answer $answer)
    {
        if (!$this->hasAnswer($answer)) {
            return;
        }

        $this->answers->removeElement($answer);
    }

    /**
     * @param QuestionCondition $condition
     */
    public function removeCondition(QuestionCondition $condition)
    {
        if (!$this->hasCondition($condition)) {
            return;
        }

        $this->conditions->removeElement($condition);
    }

    /**
     * @param QuestionCondition $condition
     */
    public function removeDependant(QuestionCondition $condition)
    {
        if (!$this->hasDependant($condition)) {
            return;
        }

        $this->dependants->removeElement($condition);
    }

    /**
     * @param QuestionOption $option
     */
    public function removeOption(QuestionOption $option)
    {
        if (!$this->hasOption($option)) {
            return;
        }

        $this->options->removeElement($option);
    }

    /**
     * @param bool $corrupted
     */
    public function setCorrupted(bool $corrupted)
    {
        $this->corrupted = $corrupted;
    }

    /**
     * @param string $help
     */
    public function setHelp($help)
    {
        $this->help = $help ?: null;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @param RemoteId $remoteId
     */
    public function setRemoteId(RemoteId $remoteId)
    {
        $this->remoteId = $remoteId;
    }

    /**
     * @param bool $required
     */
    public function setRequired(bool $required)
    {
        $this->required = $required;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * @param QuestionType $type
     */
    public function setType(QuestionType $type)
    {
        $this->type = $type;
    }

    /**
     * Vraag loskoppelen van een stap.
     *
     * @internal
     */
    public function unlinkStep()
    {
        if (null !== $this->step) {
            $reference = $this->step;

            $this->step = null;
            $reference->removeQuestion($this);
        }
    }
}
