<?php

namespace NnShop\Domain\Templates\Entity;

use Doctrine\ORM\Mapping as ORM;
use NnShop\Domain\Templates\Value\QuestionConditionId;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Templates\Repository\QuestionConditionDoctrineRepository")
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(name="ix_question", columns={"question_id"})
 * })
 */
class QuestionCondition
{
    /**
     * @ORM\Id
     * @ORM\Column(type="templates.question_condition_id")
     *
     * @var QuestionConditionId
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\QuestionOption", inversedBy="conditions")
     * @ORM\JoinColumn(referencedColumnName="option_id")
     *
     * @var QuestionOption
     */
    private $option;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Question", inversedBy="dependants")
     * @ORM\JoinColumn(referencedColumnName="question_id")
     *
     * @var Question
     */
    private $parent;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Question", inversedBy="conditions")
     * @ORM\JoinColumn(referencedColumnName="question_id", nullable=false)
     *
     * @var Question
     */
    private $question;

    /**
     * QuestionCondition constructor.
     *
     * @param QuestionConditionId $id
     * @param Question            $question
     */
    private function __construct(QuestionConditionId $id, Question $question)
    {
        $this->id = $id;

        $this->question = $question;
        $this->question->addCondition($this);
    }

    /**
     * @param QuestionConditionId $id
     * @param Question            $question The question we are defining this condition for
     * @param QuestionOption      $option   The option that's required for this question to appear
     *
     * @return QuestionCondition
     */
    public static function createWithOption(QuestionConditionId $id, Question $question, QuestionOption $option): self
    {
        $condition = new self($id, $question);
        $condition->linkOption($option);

        return $condition;
    }

    /**
     * @param QuestionConditionId $id
     * @param Question            $question The question we are defining this condition for
     * @param Question            $parent   The parent question to link this condition to
     *
     * @return QuestionCondition
     */
    public static function createWithParent(QuestionConditionId $id, Question $question, Question $parent): self
    {
        $condition = new self($id, $question);
        $condition->linkParent($parent);

        return $condition;
    }

    /**
     * Detach the condition from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->option) {
            $this->option->removeCondition($this);
            $this->option = null;
        }

        if (null !== $this->parent) {
            $this->parent->removeDependant($this);
            $this->parent = null;
        }

        if (null !== $this->question) {
            $this->question->removeCondition($this);
            $this->question = null;
        }
    }

    /**
     * @return QuestionConditionId
     */
    public function getId(): QuestionConditionId
    {
        return $this->id;
    }

    /**
     * @return QuestionOption|null
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * @return Question|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return Question
     */
    public function getQuestion(): Question
    {
        return $this->question;
    }

    /**
     * @param QuestionOption $option
     */
    public function linkOption(QuestionOption $option)
    {
        if (null !== $this->parent) {
            throw new \DomainException('Cannot link this condition to a QuestionOption because it has been linked to a parent Question');
        }

        $this->option = $option;
        $this->option->addCondition($this);
    }

    /**
     * @param Question $parent
     */
    public function linkParent(Question $parent)
    {
        if (null !== $this->option) {
            throw new \DomainException('Cannot link this condition to a parent Question because it has been linked to a QuestionOption');
        }

        $this->parent = $parent;
        $this->parent->addDependant($this);
    }
}
