<?php

namespace NnShop\Domain\Templates\Entity;

use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\Common\Exception\Domain\EntityNotFoundException;
use Core\Common\Validation\Assertion;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Comparable;
use Doctrine\ORM\Mapping as ORM;
use JuriBlox\Sdk\Domain\Documents\Values\QuestionnaireStepId as RemoteId;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Templates\Value\QuestionId;
use NnShop\Domain\Templates\Value\QuestionStepId;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Templates\Repository\QuestionStepDoctrineRepository")
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(name="ix_remote", columns={"remote_id", "template_id"}),
 *     @ORM\Index(name="ix_template", columns={"template_id", "step_position"})
 * })
 */
class QuestionStep implements Comparable
{
    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\Id
     * @ORM\Column(type="templates.question_step_id")
     *
     * @var QuestionStepId
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\Order", mappedBy="step", fetch="EXTRA_LAZY")
     *
     * @var Collection|Order[]
     */
    private $orders;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $position;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\Question", mappedBy="step", cascade={"persist", "remove"}, orphanRemoval=false)
     * @ORM\OrderBy({"position": "ASC"})
     *
     * @var Collection|Question[]
     */
    private $questions;

    /**
     * @ORM\Column(type="juriblox.step_id")
     *
     * @var RemoteId
     */
    private $remoteId;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Template", inversedBy="questionSteps")
     * @ORM\JoinColumn(referencedColumnName="template_id", nullable=false)
     *
     * @var Template
     */
    private $template;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $title;

    /**
     * QuestionStep constructor.
     *
     * @param QuestionStepId $id
     * @param Template       $template
     * @param RemoteId       $remoteId
     */
    private function __construct(QuestionStepId $id, Template $template, RemoteId $remoteId)
    {
        $this->id = $id;
        $this->remoteId = $remoteId;

        $this->template = $template;
        $this->template->addQuestionStep($this);

        $this->orders = new ArrayCollection();
        $this->questions = new ArrayCollection();

        $this->position = -1;
    }

    /**
     * @param QuestionStepId $id
     * @param Template       $template
     * @param string         $title
     * @param RemoteId       $remoteId
     *
     * @return QuestionStep
     */
    public static function create(QuestionStepId $id, Template $template, string $title, RemoteId $remoteId): self
    {
        $step = new self($id, $template, $remoteId);
        $step->setTitle($title);

        return $step;
    }

    /**
     * @param Order $order
     */
    public function addOrder(Order $order)
    {
        if ($this->hasOrder($order)) {
            throw new DuplicateEntityException();
        }

        $this->orders->add($order);
    }

    /**
     * @param Question $question
     */
    public function addQuestion(Question $question)
    {
        if ($this->hasQuestion($question)) {
            throw new DuplicateEntityException();
        }

        $this->questions->add($question);
    }

    /**
     * {@inheritdoc}
     */
    public function compareTo($other)
    {
        Assertion::isInstanceOf($other, self::class);

        return $other->getId() == $this->id ? 0 : -1;
    }

    /**
     * Entiteit loskoppelen.
     *
     * @internal
     */
    public function detach()
    {
        if (null !== $this->template) {
            $reference = $this->template;

            $this->template = null;
            $reference->removeQuestionStep($this);
        }

        // Vragen loskoppelen van deze entity
        foreach ($this->questions as $question) {
            $question->unlinkStep();
        }

        $this->position = -1;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return QuestionStepId
     */
    public function getId(): QuestionStepId
    {
        return $this->id;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param QuestionId $questionId
     *
     * @return Question
     */
    public function getQuestion(QuestionId $questionId): Question
    {
        $questions = $this->questions->filter(function (Question $question) use ($questionId) {
            return $question->getId()->getString() == $questionId->getString();
        });

        if ($questions->isEmpty()) {
            throw EntityNotFoundException::fromClassWithCriteria(Question::class, [
                'id' => $questionId,
            ]);
        }

        return $questions->first();
    }

    /**
     * @return Collection|Question[]
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    /**
     * @return RemoteId
     */
    public function getRemoteId(): RemoteId
    {
        return $this->remoteId;
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->template;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return Collection|Question[]
     */
    public function getVisibleQuestions(): Collection
    {
        return $this->questions->matching(
            Criteria::create()->where(
                Criteria::expr()->isNull('timestampDeleted')
            )
        );
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    public function hasOrder(Order $order): bool
    {
        return $this->orders->contains($order);
    }

    /**
     * @param Question $question
     *
     * @return bool
     */
    public function hasQuestion(Question $question): bool
    {
        return $this->questions->contains($question);
    }

    /**
     * @param QuestionId $questionId
     *
     * @return bool
     */
    public function hasQuestionId(QuestionId $questionId): bool
    {
        $questions = $this->questions->filter(function (Question $question) use ($questionId) {
            return $question->getId()->getString() == $questionId->getString();
        });

        return !$questions->isEmpty();
    }

    /**
     * @param Order $order
     */
    public function removeOrder(Order $order)
    {
        if (!$this->hasOrder($order)) {
            return;
        }

        $this->orders->removeElement($order);
    }

    /**
     * @param Question $question
     */
    public function removeQuestion(Question $question)
    {
        if (!$this->hasQuestion($question)) {
            return;
        }

        $this->questions->removeElement($question);
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description ?: null;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position)
    {
        $this->position = $position;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }
}
