<?php

namespace NnShop\Domain\Templates\Entity;

use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\Domain\Traits\SoftDeleteableEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JuriBlox\Sdk\Domain\Documents\Values\QuestionOptionId as RemoteId;
use NnShop\Domain\Templates\Value\QuestionOptionId;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Templates\Repository\QuestionOptionDoctrineRepository")
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(name="ix_question", columns={"question_id", "option_position"}),
 *     @ORM\Index(name="ix_lookup", columns={"lookup_id", "question_id"}),
 *     @ORM\Index(name="ix_remote", columns={"remote_id", "question_id"})
 * })
 *
 * @Gedmo\SoftDeleteable(fieldName="timestampDeleted")
 */
class QuestionOption
{
    use SoftDeleteableEntityTrait;

    /**
     * @ORM\ManyToMany(targetEntity="\NnShop\Domain\Templates\Entity\Answer", mappedBy="options", cascade={"persist", "remove"}, orphanRemoval=true, fetch="EXTRA_LAZY")
     *
     * @var Collection|Answer[]
     */
    private $answers;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\QuestionCondition", mappedBy="option", cascade={"persist", "remove"}, orphanRemoval=true, fetch="EXTRA_LAZY")
     *
     * @var Collection|QuestionCondition[]
     */
    private $conditions;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $default;

    /**
     * @ORM\Id
     * @ORM\Column(type="templates.question_option_id")
     *
     * @var QuestionOptionId
     */
    private $id;

    /**
     * @ORM\Column(type="juriblox.question_option_id")
     *
     * @var RemoteId
     */
    private $lookupId;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Question", inversedBy="options")
     * @ORM\JoinColumn(referencedColumnName="question_id", nullable=false)
     *
     * @var Question
     */
    private $question;

    /**
     * @ORM\Column(type="juriblox.question_option_id")
     *
     * @var RemoteId
     */
    private $remoteId;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="object")
     *
     * @var mixed
     */
    private $value;

    /**
     * QuestionOption constructor.
     *
     * @param QuestionOptionId $id
     * @param Question         $question
     * @param RemoteId         $lookupId
     * @param RemoteId         $remoteId
     */
    private function __construct(QuestionOptionId $id, Question $question, RemoteId $lookupId, RemoteId $remoteId)
    {
        $this->id = $id;

        $this->lookupId = $lookupId;
        $this->remoteId = $remoteId;

        $this->question = $question;
        $this->question->addOption($this);

        $this->answers = new ArrayCollection();
        $this->conditions = new ArrayCollection();

        $this->default = false;

        $this->position = -1;
    }

    /**
     * @param QuestionOptionId $id
     * @param Question         $question
     * @param mixed            $value
     * @param RemoteId         $lookupId
     * @param RemoteId         $remoteId
     *
     * @return QuestionOption
     */
    public static function create(QuestionOptionId $id, Question $question, $value, RemoteId $lookupId, RemoteId $remoteId): self
    {
        $option = new self($id, $question, $lookupId, $remoteId);
        $option->setValue($value);

        return $option;
    }

    /**
     * @param Answer $answer
     */
    public function addAnswer(Answer $answer)
    {
        if ($this->hasAnswer($answer)) {
            throw new DuplicateEntityException();
        }

        $this->answers->add($answer);
    }

    /**
     * @param QuestionCondition $condition
     */
    public function addCondition(QuestionCondition $condition)
    {
        if ($this->hasCondition($condition)) {
            throw new DuplicateEntityException();
        }

        $this->conditions->add($condition);
    }

    /**
     * Detach the option from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->question) {
            $this->question->removeOption($this);
            $this->question = null;
        }
    }

    /**
     * @return Collection|QuestionCondition[]
     */
    public function getConditions(): Collection
    {
        return $this->conditions;
    }

    /**
     * @return QuestionOptionId
     */
    public function getId(): QuestionOptionId
    {
        return $this->id;
    }

    /**
     * @return RemoteId
     */
    public function getLookupId(): RemoteId
    {
        return $this->lookupId;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @return Question
     */
    public function getQuestion(): Question
    {
        return $this->question;
    }

    /**
     * @return RemoteId
     */
    public function getRemoteId(): RemoteId
    {
        return $this->remoteId;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param Answer $answer
     *
     * @return bool
     */
    public function hasAnswer(Answer $answer): bool
    {
        return $this->answers->contains($answer);
    }

    /**
     * @param QuestionCondition $condition
     *
     * @return bool
     */
    public function hasCondition(QuestionCondition $condition): bool
    {
        return $this->conditions->contains($condition);
    }

    /**
     * @return bool
     */
    public function isDefault(): bool
    {
        return $this->default;
    }

    /**
     * @param Question $question
     */
    public function move(Question $question)
    {
        if (null !== $this->question && $question->getId()->equals($this->question->getId())) {
            return;
        }

        if (null !== $this->question) {
            $reference = $this->question;

            $this->question = null;
            $reference->removeOption($this);
        }

        $this->question = $question;
        $this->question->addOption($this);
    }

    /**
     * @param Answer $answer
     */
    public function removeAnswer(Answer $answer)
    {
        if (!$this->hasAnswer($answer)) {
            return;
        }

        $this->answers->removeElement($answer);
    }

    /**
     * @param QuestionCondition $condition
     */
    public function removeCondition(QuestionCondition $condition)
    {
        if (!$this->hasCondition($condition)) {
            return;
        }

        $this->conditions->removeElement($condition);
    }

    /**
     * @param bool $default
     */
    public function setDefault(bool $default)
    {
        $this->default = $default;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position)
    {
        $this->position = $position;
    }

    /**
     * @param RemoteId $remoteId
     */
    public function setRemoteId(RemoteId $remoteId)
    {
        $this->remoteId = $remoteId;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}
