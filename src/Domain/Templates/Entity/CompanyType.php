<?php

namespace NnShop\Domain\Templates\Entity;

use Core\Annotation\Doctrine as Core;
use Core\Common\Exception\Domain\DuplicateEntityException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use NnShop\Domain\Templates\Value\CompanyTypeId;
use NnShop\Domain\Translation\Entity\Profile;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Templates\Repository\CompanyTypeDoctrineRepository")
 * @Core\QueryBuilder(class="NnShop\Infrastructure\Templates\QueryBuilder\CompanyTypeDoctrineQueryBuilder")
 * @Gedmo\TranslationEntity(class="CompanyTypeTranslation")
 */
class CompanyType
{
    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\CompanyActivity", mappedBy="type", cascade={"persist", "remove"}, orphanRemoval=true, fetch="LAZY")
     * @ORM\OrderBy({"position": "ASC", "title": "ASC"})
     *
     * @var Collection|CompanyActivity[]
     */
    private $activities;

    /**
     * @ORM\Column(type="text")
     * @Gedmo\Translatable
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\Id
     * @ORM\Column(type="templates.company_type_id")
     *
     * @var CompanyTypeId
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     * @Gedmo\Slug(fields={"title"})
     * @Gedmo\Translatable
     *
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=100)
     * @Gedmo\Translatable
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\OneToMany(
     *     targetEntity="CompanyTypeTranslation",
     *     mappedBy="object",
     *     cascade={"persist", "remove"}
     * )
     */
    private $translations;

    /**
     * @var Profile
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Translation\Entity\Profile", inversedBy="companyTypes")
     * @ORM\JoinColumn(name="profile_id", referencedColumnName="profile_id", onDelete="SET NULL")
     */
    private $profile;

    /**
     * CompanyType constructor.
     *
     * @param CompanyTypeId $id
     * @param Profile       $profile
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    private function __construct(CompanyTypeId $id, Profile $profile)
    {
        $this->id = $id;
        $this->profile = $profile;

        $this->profile->addCompanyType($this);

        $this->activities = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }

    /**
     * @param CompanyActivity $activity
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    public function addActivity(CompanyActivity $activity)
    {
        if ($this->hasActivity($activity)) {
            throw new DuplicateEntityException();
        }

        $this->activities->add($activity);
    }

    /**
     * @param CompanyTypeId $id
     * @param string        $title
     * @param Profile       $profile
     *
     * @return CompanyType
     */
    public static function create(CompanyTypeId $id, string $title, Profile $profile): self
    {
        $type = new self($id, $profile);
        $type->setTitle($title);

        return $type;
    }

    /**
     * @return Collection|CompanyActivity[]
     */
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return CompanyTypeId
     */
    public function getId(): CompanyTypeId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param CompanyActivity $activity
     *
     * @return bool
     */
    public function hasActivity(CompanyActivity $activity): bool
    {
        return $this->activities->contains($activity);
    }

    /**
     * @param CompanyActivity $activity
     */
    public function removeActivity(CompanyActivity $activity)
    {
        if (!$this->hasActivity($activity)) {
            return;
        }

        $this->activities->removeElement($activity);
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description ?: null;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return PersistentCollection
     */
    public function getTranslations(): PersistentCollection
    {
        return $this->translations;
    }

    /**
     * @param CompanyTypeTranslation $t
     */
    public function addTranslation(CompanyTypeTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    /**
     * @return Profile
     */
    public function getProfile(): Profile
    {
        return $this->profile;
    }
}
