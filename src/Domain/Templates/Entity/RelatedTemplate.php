<?php

namespace NnShop\Domain\Templates\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="templates_related", indexes={
 *     @ORM\Index(name="ix_primary", columns={"template_id", "related_id"})
 * })
 */
class RelatedTemplate
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="link_id")
     * @ORM\GeneratedValue
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Template", inversedBy="relatedToTemplateLinks")
     * @ORM\JoinColumn(referencedColumnName="template_id", nullable=false, onDelete="CASCADE")
     *
     * @var Template
     */
    private $related;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Template", inversedBy="relatedTemplateLinks")
     * @ORM\JoinColumn(referencedColumnName="template_id", nullable=false, onDelete="CASCADE")
     *
     * @var Template
     */
    private $template;

    private function __construct(Template $template, Template $related)
    {
        $this->template = $template;
        $this->template->addRelatedTemplateLink($this);

        $this->related = $related;
        $this->related->addRelatedToTemplateLink($this);
    }

    public static function create(Template $template, Template $related)
    {
        return new self($template, $related);
    }

    /**
     * Detach the link from its associated entities.
     */
    public function detach(): void
    {
        if (null !== $this->template) {
            $this->template->removeRelatedTemplateLink($this);
            $this->template = null;
        }

        if (null !== $this->template) {
            $this->template->removeRelatedToTemplateLink($this);
            $this->template = null;
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getRelated(): Template
    {
        return $this->related;
    }

    public function getTemplate(): Template
    {
        return $this->template;
    }
}
