<?php

namespace NnShop\Domain\Templates\Entity;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use JuriBlox\Sdk\Domain\Documents\Values\DocumentId as RemoteDocumentId;
use JuriBlox\Sdk\Domain\Documents\Values\DocumentRequestId as RemoteRequestId;
use NnShop\Domain\Templates\Enumeration\DocumentStatus;
use NnShop\Domain\Templates\Value\DocumentRequestId;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Templates\Repository\DocumentRequestDoctrineRepository")
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(name="ix_document", columns={"document_id"}),
 * })
 */
class DocumentRequest
{
    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Document", inversedBy="requests")
     * @ORM\JoinColumn(referencedColumnName="document_id", nullable=false)
     *
     * @var Document
     */
    private $document;

    /**
     * @ORM\Id
     * @ORM\Column(type="templates.document_request_id")
     *
     * @var DocumentRequestId
     */
    private $id;

    /**
     * @ORM\Column(type="juriblox.document_request_id", nullable=true, unique=true)
     *
     * @var RemoteRequestId|null
     */
    private $remoteId;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon|null
     */
    private $timestampCompleted;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon|null
     */
    private $timestampFailed;

    /**
     * @ORM\Column(type="core.time.datetime")
     *
     * @var Carbon
     */
    private $timestampRequested;

    /**
     * DocumentRequest constructor.
     *
     * @param DocumentRequestId $id
     * @param Document          $document
     */
    private function __construct(DocumentRequestId $id, Document $document)
    {
        $this->id = $id;

        $this->document = $document;
        $this->document->addRequest($this);
    }

    /**
     * @param DocumentRequestId $id
     * @param Document          $document
     *
     * @return DocumentRequest
     */
    public static function create(DocumentRequestId $id, Document $document): self
    {
        $request = new static($id, $document);
        $request->setTimestampRequested(Carbon::now());

        // DocumentStatus bijwerken
        $document->setStatus(DocumentStatus::from(DocumentStatus::PENDING));

        return $request;
    }

    /**
     * Entiteit loskoppelen.
     *
     * @internal
     */
    public function detach()
    {
        if (null !== $this->document) {
            $this->document->removeRequest($this);
            $this->document = null;
        }
    }

    /**
     * @return Document
     */
    public function getDocument(): Document
    {
        return $this->document;
    }

    /**
     * @return DocumentRequestId
     */
    public function getId(): DocumentRequestId
    {
        return $this->id;
    }

    /**
     * @return RemoteRequestId|null
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampCompleted()
    {
        return $this->timestampCompleted;
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampFailed()
    {
        return $this->timestampFailed;
    }

    /**
     * @return Carbon
     */
    public function getTimestampRequested(): Carbon
    {
        return $this->timestampRequested;
    }

    /**
     * Documentaanvraag is succesvol afgerond.
     *
     * @return bool
     */
    public function isCompleted(): bool
    {
        return null !== $this->timestampCompleted;
    }

    /**
     * Documentaanvraag is mislukt.
     *
     * @return bool
     */
    public function isFailed(): bool
    {
        return null !== $this->timestampFailed;
    }

    /**
     * Documentaanvraag is nog in behandeling.
     *
     * @return bool
     */
    public function isPending(): bool
    {
        return !$this->isCompleted() && !$this->isFailed();
    }

    /**
     * Documentaanvraag is succesvol afgerond.
     *
     * @param RemoteDocumentId $documentId het DocumentId zoals dat in JuriBlox bekend is geworden
     */
    public function setCompleted(RemoteDocumentId $documentId)
    {
        if ($this->isFailed()) {
            throw new \DomainException('This document request has previously failed, so it cannot suddenly be completed');
        }

        // Status van het document bijwerken
        $this->document->setStatus(DocumentStatus::from(DocumentStatus::GENERATED));
        $this->document->setRemoteId($documentId);

        $this->setTimestampCompleted(Carbon::now());
    }

    /**
     * Documentaanvraag is mislukt.
     */
    public function setFailed()
    {
        if ($this->isCompleted()) {
            throw new \DomainException('This document request has previously been completed, so it cannot suddenly have failed');
        }

        // Status van het document bijwerken
        $this->document->setStatus(DocumentStatus::from(DocumentStatus::ERROR));

        $this->setTimestampFailed(Carbon::now());
    }

    /**
     * @param RemoteRequestId $remoteId
     */
    public function setRemoteId(RemoteRequestId $remoteId)
    {
        $this->remoteId = $remoteId;
    }

    /**
     * @param Carbon $timestamp
     */
    private function setTimestampCompleted(Carbon $timestamp)
    {
        $this->timestampCompleted = $timestamp;
    }

    /**
     * @param Carbon $timestamp
     */
    private function setTimestampFailed(Carbon $timestamp)
    {
        $this->timestampFailed = $timestamp;
    }

    /**
     * @param Carbon $timestamp
     */
    private function setTimestampRequested(Carbon $timestamp)
    {
        $this->timestampRequested = $timestamp;
    }
}
