<?php

namespace NnShop\Domain\Templates\Entity;

use Core\Annotation\Doctrine as Core;
use Core\Common\Exception\Domain\DuplicateEntityException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use NnShop\Domain\Templates\Value\CompanyActivityId;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Templates\Repository\CompanyActivityDoctrineRepository")
 * @Core\QueryBuilder(class="NnShop\Infrastructure\Templates\QueryBuilder\CompanyActivityDoctrineQueryBuilder")
 * @Gedmo\TranslationEntity(class="CompanyActivityTranslation")
 */
class CompanyActivity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="templates.company_activity_id")
     *
     * @var CompanyActivityId
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     * @Gedmo\Slug(handlers={
     *     @Gedmo\SlugHandler(class="Gedmo\Sluggable\Handler\RelativeSlugHandler", options={
     *         @Gedmo\SlugHandlerOption(name="relationField", value="type"),
     *         @Gedmo\SlugHandlerOption(name="relationSlugField", value="slug"),
     *         @Gedmo\SlugHandlerOption(name="separator", value="/")
     *     })
     * }, fields={"title"})
     * @Gedmo\Translatable
     *
     * @var string
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\CompanyActivityTemplate", mappedBy="activity", cascade={"persist", "remove"}, orphanRemoval=true, fetch="EXTRA_LAZY")
     *
     * @var Collection|CompanyActivityTemplate[]
     */
    private $templateLinks;

    /**
     * @ORM\Column(type="string", length=100)
     * @Gedmo\Translatable
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\CompanyType", inversedBy="activities")
     * @ORM\JoinColumn(referencedColumnName="type_id", nullable=false)
     *
     * @var CompanyType
     */
    private $type;

    /**
     * @ORM\OneToMany(
     *     targetEntity="CompanyActivityTranslation",
     *     mappedBy="object",
     *     cascade={"persist", "remove"}
     * )
     */
    private $translations;

    /**
     * @ORM\Column(type="integer")
     * @Gedmo\SortablePosition
     *
     * @var int
     */
    private $position;

    /**
     * CompanyActivity constructor.
     *
     * @param CompanyActivityId $id
     * @param CompanyType       $type
     */
    private function __construct(CompanyActivityId $id, CompanyType $type)
    {
        $this->id = $id;

        $this->type = $type;
        $this->type->addActivity($this);

        $this->templateLinks = new ArrayCollection();
    }

    /**
     * @param Template $template
     *
     * @return CompanyActivityTemplate
     */
    public function addTemplate(Template $template): CompanyActivityTemplate
    {
        return CompanyActivityTemplate::create($this, $template);
    }

    /**
     * @param CompanyActivityTemplate $link
     */
    public function addTemplateLink(CompanyActivityTemplate $link)
    {
        if ($this->hasTemplate($link->getTemplate())) {
            throw new DuplicateEntityException();
        }

        $this->templateLinks->add($link);
    }

    public function clearTemplates()
    {
        foreach ($this->getTemplates() as $template) {
            $this->removeTemplate($template);
        }
    }

    /**
     * @param CompanyActivityId $id
     * @param CompanyType       $type
     * @param string            $title
     *
     * @return CompanyActivity
     */
    public static function create(CompanyActivityId $id, CompanyType $type, string $title): self
    {
        $activity = new self($id, $type);
        $activity->setTitle($title);

        return $activity;
    }

    /**
     * Detach the activity from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->type) {
            $this->type->removeActivity($this);
            $this->type = null;
        }
    }

    /**
     * @return CompanyActivityId
     */
    public function getId(): CompanyActivityId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return ArrayCollection|Template[]
     */
    public function getTemplates(): ArrayCollection
    {
        $templates = new ArrayCollection();
        foreach ($this->templateLinks as $link) {
            $templates->add($link->getTemplate());
        }

        return $templates;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return CompanyType
     */
    public function getType(): CompanyType
    {
        return $this->type;
    }

    /**
     * @param Template $template
     *
     * @return bool
     */
    public function hasTemplate(Template $template): bool
    {
        return null !== $this->findTemplateLink($template);
    }

    /**
     * @param Template $template
     */
    public function removeTemplate(Template $template)
    {
        $link = $this->findTemplateLink($template);
        if (null === $link) {
            return;
        }

        $this->removeTemplateLink($link);
        $template->removeActivityLink($link);
    }

    /**
     * @param CompanyActivityTemplate $link
     */
    public function removeTemplateLink(CompanyActivityTemplate $link)
    {
        if (!$this->templateLinks->contains($link)) {
            return;
        }

        $this->templateLinks->removeElement($link);
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return PersistentCollection
     */
    public function getTranslations(): PersistentCollection
    {
        return $this->translations;
    }

    /**
     * @param CompanyActivityTranslation $t
     */
    public function addTranslation(CompanyActivityTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    /**
     * @param Template $template
     *
     * @return CompanyActivityTemplate|null
     */
    private function findTemplateLink(Template $template)
    {
        $links = $this->templateLinks->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('template', $template)
            )
        );

        return $links->isEmpty() ? null : $links->first();
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position)
    {
        $this->position = $position;
    }


}
