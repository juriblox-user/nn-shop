<?php

namespace NnShop\Domain\Templates\Entity;

use Carbon\Carbon;
use Core\Annotation\Doctrine as Core;
use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\Common\Exception\Domain\EntityNotFoundException;
use Core\Common\Validation\Assertion;
use Core\Domain\Traits\SoftDeleteableEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Comparable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JuriBlox\Sdk\Domain\Documents\Values\TemplateId as RemoteId;
use Money\Currency;
use Money\Money;
use NnShop\Common\Exceptions\TemplateWithoutCategoryException;
use NnShop\Domain\Orders\Entity\Advice;
use NnShop\Domain\Orders\Entity\AdviceTemplate;
use NnShop\Domain\Orders\Entity\Subscription;
use NnShop\Domain\Shops\Entity\LandingPage;
use NnShop\Domain\Shops\Entity\LandingPageTemplate;
use NnShop\Domain\Shops\Entity\Referrer;
use NnShop\Domain\Shops\Entity\ReferrerTemplate;
use NnShop\Domain\Shops\Entity\Shop;
use NnShop\Domain\Templates\Event\Template\TemplateCreatedEvent;
use NnShop\Domain\Templates\Value\TemplateId;
use NnShop\Domain\Templates\Value\TemplateKey;
use NnShop\Domain\Templates\Value\TemplateVersion;
use NnShop\Domain\Translation\Entity\Profile;
use SimpleBus\Message\Recorder\ContainsRecordedMessages;
use SimpleBus\Message\Recorder\PrivateMessageRecorderCapabilities;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Templates\Repository\TemplateDoctrineRepository")
 * @Core\QueryBuilder(class="NnShop\Infrastructure\Templates\QueryBuilder\TemplateDoctrineQueryBuilder")
 *
 * @ORM\Table(
 *     indexes={
 *         @ORM\Index(name="ix_deleted", columns={"timestamp_deleted"}),
 *         @ORM\Index(name="ix_partner", columns={"partner_id", "timestamp_deleted"}),
 *         @ORM\Index(name="ix_published", columns={"template_corrupted", "template_published", "timestamp_deleted"}),
 *         @ORM\Index(name="ix_remote", columns={"remote_id", "partner_id"}),
 *         @ORM\Index(name="ix_spotlight", columns={"template_spotlight", "timestamp_deleted"}),
 *         @ORM\Index(name="ft_keywords", columns={"template_title", "template_description", "remote_title", "remote_description"}, flags={"fulltext"})
 *     },
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="uq_key", columns={"template_key"})
 *     }
 * )
 *
 * @Gedmo\SoftDeleteable(fieldName="timestampDeleted")
 */
class Template implements Comparable, ContainsRecordedMessages
{
    use PrivateMessageRecorderCapabilities;
    use SoftDeleteableEntityTrait;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\CompanyActivityTemplate", mappedBy="template", cascade={"persist"})
     *
     * @var Collection|CompanyActivityTemplate[]
     */
    private $activityLinks;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\AdviceTemplate", mappedBy="template", cascade={"persist"})
     *
     * @var Collection|AdviceTemplate[]
     */
    private $adviceLinks;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\CategoryTemplate", mappedBy="template", cascade={"persist"})
     *
     * @var Collection|CategoryTemplate[]
     */
    private $categoryLinks;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $corrupted;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="NnShop\Domain\Orders\Entity\Discount", mappedBy="templates")
     */
    private $discounts;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\Document", mappedBy="template", cascade={"persist"}, fetch="EXTRA_LAZY")
     *
     * @var Collection|Document[]
     */
    private $documents;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string
     */
    private $excerpt;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $hidden;

    /**
     * @ORM\Id
     * @ORM\Column(type="templates.template_id")
     *
     * @var TemplateId
     */
    private $id;

    /**
     * @ORM\Column(type="templates.template_key", unique=true)
     *
     * @var TemplateKey
     */
    private $key;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Shops\Entity\LandingPageTemplate", mappedBy="template", cascade={"persist"})
     *
     * @var Collection|LandingPageTemplate[]
     */
    private $landingPageLinks;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $orderCount;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $orderSeconds;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Partner", inversedBy="templates")
     * @ORM\JoinColumn(referencedColumnName="partner_id", nullable=false)
     *
     * @var Partner
     */
    private $partner;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string
     */
    private $preview;

    /**
     * @ORM\Column(type="core.financial.money", nullable=true)
     *
     * @var Money
     */
    private $priceCheck;

    /**
     * @ORM\Column(type="core.financial.money", nullable=true)
     *
     * @var Money|null
     */
    private $priceMonthly;

    /**
     * @ORM\Column(type="core.financial.money", nullable=true)
     *
     * @var Money|null
     */
    private $priceUnit;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $published;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\QuestionStep", mappedBy="template", cascade={"persist"})
     * @ORM\OrderBy({"position": "ASC"})
     *
     * @var Collection|QuestionStep[]
     */
    private $questionSteps;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Shops\Entity\ReferrerTemplate", mappedBy="template", cascade={"persist"})
     *
     * @var Collection|ReferrerTemplate[]
     */
    private $referrerLinks;

    /**
     * @ORM\Column(type="core.financial.money", nullable=true, name="price_registered_email")
     *
     * @var Money|null
     */
    private $registeredEmailPrice;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\RelatedTemplate", mappedBy="template", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @var Collection|RelatedTemplate[]
     */
    private $relatedTemplateLinks;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\RelatedTemplate", mappedBy="related", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @var Collection|RelatedTemplate[]
     */
    private $relatedToTemplateLinks;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string
     */
    private $remoteDescription;

    /**
     * @ORM\Column(type="juriblox.template_id")
     *
     * @var RemoteId
     */
    private $remoteId;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $remoteTitle;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $sample;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $service;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Gedmo\Slug(fields={"title"})
     *
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $spotlight;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\Subscription", mappedBy="template", cascade={"persist"}, fetch="EXTRA_LAZY")
     *
     * @var Collection|Subscription[]
     */
    private $subscriptions;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon
     */
    private $syncedTimestamp;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\Update", mappedBy="template", cascade={"persist"}, fetch="EXTRA_LAZY")
     *
     * @var Collection|Update[]
     */
    private $updates;

    /**
     * @ORM\Column(type="templates.template_version")
     *
     * @var TemplateVersion
     */
    private $version;

    /**
     * Template constructor.
     *
     * @param TemplateId      $id
     * @param TemplateKey     $key
     * @param TemplateVersion $version
     * @param Partner         $partner
     * @param RemoteId        $remoteId
     */
    private function __construct(TemplateId $id, TemplateKey $key, TemplateVersion $version, Partner $partner, RemoteId $remoteId)
    {
        $this->id       = $id;
        $this->remoteId = $remoteId;

        $this->key     = $key;
        $this->version = $version;

        $this->partner = $partner;

        $this->hidden = false;

        $this->preview = false;

        $this->corrupted = false;
        $this->published = false;

        $this->sample    = false;
        $this->spotlight = false;

        $this->service = false;

        $this->orderCount   = 0;
        $this->orderSeconds = 0;

        $this->adviceLinks      = new ArrayCollection();
        $this->activityLinks    = new ArrayCollection();
        $this->categoryLinks    = new ArrayCollection();
        $this->landingPageLinks = new ArrayCollection();
        $this->referrerLinks    = new ArrayCollection();

        $this->relatedTemplateLinks   = new ArrayCollection();
        $this->relatedToTemplateLinks = new ArrayCollection();

        $this->documents     = new ArrayCollection();
        $this->updates       = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
        $this->questionSteps = new ArrayCollection();
    }

    /**
     * @param TemplateId      $id
     * @param TemplateKey     $key
     * @param TemplateVersion $version
     * @param Partner         $partner
     * @param RemoteId        $remoteId
     *
     * @return Template
     */
    public static function prepare(TemplateId $id, TemplateKey $key, TemplateVersion $version, Partner $partner, RemoteId $remoteId): self
    {
        $template = new self($id, $key, $version, $partner, $remoteId);
        $template->setCheckPrice(new Money(14900, new Currency('EUR')));

        $template->record(TemplateCreatedEvent::create($id));

        $partner->addTemplate($template);

        return $template;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }

    /**
     * @param CompanyActivityTemplate $link
     */
    public function addActivityLink(CompanyActivityTemplate $link)
    {
        if ($this->isAdvisedFor($link->getActivity())) {
            throw new DuplicateEntityException();
        }

        $this->activityLinks->add($link);
    }

    /**
     * @param AdviceTemplate $link
     */
    public function addAdviceLink(AdviceTemplate $link)
    {
        if ($this->isAdvisedBy($link->getAdvice())) {
            throw new DuplicateEntityException();
        }

        $this->adviceLinks->add($link);
    }

    /**
     * @param Category $category
     *
     * @return CategoryTemplate
     */
    public function addCategory(Category $category): CategoryTemplate
    {
        return CategoryTemplate::create($category, $this);
    }

    /**
     * @param CategoryTemplate $link
     */
    public function addCategoryLink(CategoryTemplate $link)
    {
        if ($this->hasCategory($link->getCategory())) {
            throw new DuplicateEntityException();
        }

        $this->categoryLinks->add($link);
    }

    /**
     * @param Document $document
     */
    public function addDocument(Document $document)
    {
        if ($this->hasDocument($document)) {
            throw new DuplicateEntityException();
        }

        $this->documents->add($document);
    }

    /**
     * @param LandingPageTemplate $link
     */
    public function addLandingPageLink(LandingPageTemplate $link)
    {
        if ($this->hasLandingPage($link->getLandingPage())) {
            return;
        }

        $this->landingPageLinks->add($link);
    }

    /**
     * @param QuestionStep $step
     */
    public function addQuestionStep(QuestionStep $step)
    {
        if ($this->hasQuestionStep($step)) {
            throw new DuplicateEntityException();
        }

        $this->questionSteps->add($step);
    }

    /**
     * @param ReferrerTemplate $link
     */
    public function addReferrerLink(ReferrerTemplate $link)
    {
        if ($this->hasReferrer($link->getReferrer())) {
            return;
        }

        $this->referrerLinks->add($link);
    }

    public function addRelated(self $related): void
    {
        RelatedTemplate::create($this, $related);
    }

    public function addRelatedTemplateLink(RelatedTemplate $link): void
    {
        $this->relatedTemplateLinks->add($link);
    }

    public function addRelatedToTemplateLink(RelatedTemplate $link): void
    {
        $this->relatedToTemplateLinks->add($link);
    }

    /**
     * @param Subscription $subscription
     */
    public function addSubscription(Subscription $subscription)
    {
        if ($this->hasSubscription($subscription)) {
            throw new DuplicateEntityException();
        }

        $this->subscriptions->add($subscription);
    }

    /**
     * @param Update $update
     */
    public function addUpdate(Update $update)
    {
        if ($this->hasUpdate($update)) {
            throw new DuplicateEntityException();
        }

        $this->updates->add($update);
    }

    /**
     * {@inheritdoc}
     */
    public function compareTo($other)
    {
        Assertion::isInstanceOf($other, self::class);

        return $other->getId() == $this->id ? 0 : -1;
    }

    /**
     * Detach the template from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->partner) {
            $this->partner->removeTemplate($this);
            $this->partner = null;
        }
    }

    /**
     * @return ArrayCollection|Category[]
     */
    public function getCategories(): ArrayCollection
    {
        $categories = new ArrayCollection();
        foreach ($this->categoryLinks as $link) {
            $category = $link->getCategory();
            $category->setPrimary($link->isPrimary());

            $categories->add($category);
        }

        return $categories;
    }

    /**
     * @param Shop $shop
     *
     * @return Category
     */
    public function getCategoryForShop(Shop $shop)
    {
        $matches = $this->getCategories()->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('shop', $shop)
            )
        );

        if (0 == $matches->count()) {
            return;
        }

        return $matches->first();
    }

    /**
     * Prijs voor een handmatige controle.
     *
     * @return Money|null
     */
    public function getCheckPrice()
    {
        return $this->priceCheck;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getDiscounts()
    {
        return $this->discounts;
    }

    /**
     * @return string|null
     */
    public function getExcerpt()
    {
        return $this->excerpt;
    }

    /**
     * @return TemplateId
     */
    public function getId(): TemplateId
    {
        return $this->id;
    }

    /**
     * @return TemplateKey
     */
    public function getKey(): TemplateKey
    {
        return $this->key;
    }

    /**
     * @return Money|null
     */
    public function getMonthlyPrice()
    {
        return $this->priceMonthly;
    }

    /**
     * Get the total number of orders for this template.
     *
     * @return int
     */
    public function getOrderCount(): int
    {
        return $this->orderCount;
    }

    /**
     * Get the number of seconds an avarage order takes.
     *
     * @return int
     */
    public function getOrderSeconds(): int
    {
        return $this->orderSeconds;
    }

    /**
     * @return Partner
     */
    public function getPartner(): Partner
    {
        return $this->partner;
    }

    /**
     * @return string|null
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * @return Category
     */
    public function getPrimaryCategory(): Category
    {
        $matches = $this->categoryLinks->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('primary', true)
            )
        );

        if (0 == $matches->count()) {
            $matches = $this->categoryLinks;

            if (0 == $matches->count()) {
                throw new TemplateWithoutCategoryException(sprintf('The template "%s" [%s] has no primary category and no fallback category could be found.', $this->title, $this->id));
            }
        }

        return $matches->first()->getCategory();
    }

    /**
     * @return null|Profile
     */
    public function getProfile()
    {
        return $this->getPartner()->getCountryProfile();
    }

    /**
     * @return Collection|QuestionStep[]
     */
    public function getQuestionSteps(): Collection
    {
        return $this->questionSteps;
    }

    public function getRegisteredEmailPrice(): ?Money
    {
        return $this->registeredEmailPrice;
    }

    /**
     * @return array|Template[]
     */
    public function getRelated(): array
    {
        $related = [];

        foreach ($this->relatedTemplateLinks as $link) {
            $related[$link->getRelated()->getTitle()][] = $link->getRelated();
        }

        ksort($related);

        $sorted = [];
        foreach ($related as $templates) {
            foreach ($templates as $template) {
                $sorted[] = $template;
            }
        }

        return $sorted;
    }

    /**
     * @return string
     */
    public function getRemoteDescription()
    {
        return $this->remoteDescription;
    }

    /**
     * @return RemoteId
     */
    public function getRemoteId(): RemoteId
    {
        return $this->remoteId;
    }

    /**
     * @return mixed
     */
    public function getRemoteTitle()
    {
        return $this->remoteTitle;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return Collection|Subscription[]
     */
    public function getSubscriptions(): Collection
    {
        return $this->subscriptions;
    }

    /**
     * @return Carbon
     */
    public function getSyncedTimestamp(): Carbon
    {
        return $this->syncedTimestamp;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Money|null
     */
    public function getUnitPrice()
    {
        return $this->priceUnit;
    }

    /**
     * @return Collection|Update[]
     */
    public function getUpdates(): Collection
    {
        return $this->updates;
    }

    /**
     * @return TemplateVersion
     */
    public function getVersion(): TemplateVersion
    {
        return $this->version;
    }

    /**
     * @return Money|null
     */
    public function getYearlyPrice()
    {
        if (null === $this->priceMonthly) {
            return;
        }

        return $this->priceMonthly->multiply(12);
    }

    /**
     * @return bool
     */
    public function hasCategories(): bool
    {
        return !$this->categoryLinks->isEmpty();
    }

    /**
     * @param Category $category
     *
     * @return bool
     */
    public function hasCategory(Category $category): bool
    {
        return null !== $this->findCategoryLink($category);
    }

    /**
     * @param Document $document
     *
     * @return bool
     */
    public function hasDocument(Document $document): bool
    {
        return $this->documents->contains($document);
    }

    /**
     * @param LandingPage $landingPage
     *
     * @return bool
     */
    public function hasLandingPage(LandingPage $landingPage): bool
    {
        return null !== $this->findLandingPageLink($landingPage);
    }

    /**
     * @param QuestionStep $step
     *
     * @return bool
     */
    public function hasQuestionStep(QuestionStep $step): bool
    {
        return $this->questionSteps->contains($step);
    }

    /**
     * @param Referrer $referrer
     *
     * @return bool
     */
    public function hasReferrer(Referrer $referrer): bool
    {
        return null !== $this->findReferrerLink($referrer);
    }

    /**
     * @return bool
     */
    public function hasSample(): bool
    {
        return $this->sample;
    }

    /**
     * @param Subscription $subscription
     *
     * @return bool
     */
    public function hasSubscription(Subscription $subscription): bool
    {
        return $this->subscriptions->contains($subscription);
    }

    /**
     * @param Update $update
     *
     * @return bool
     */
    public function hasUpdate(Update $update): bool
    {
        return $this->updates->contains($update);
    }

    /**
     * Increase the number of orders for this template.
     */
    public function increaseOrderCount()
    {
        ++$this->orderCount;
    }

    /**
     * @param Advice $advice
     *
     * @return bool
     */
    public function isAdvisedBy(Advice $advice): bool
    {
        return null !== $this->findAdviceLink($advice);
    }

    /**
     * @param CompanyActivity $activity
     *
     * @return bool
     */
    public function isAdvisedFor(CompanyActivity $activity): bool
    {
        return null !== $this->findActivityLink($activity);
    }

    /**
     * @return bool
     */
    public function isCorrupted(): bool
    {
        return $this->corrupted;
    }

    /**
     * @return bool
     */
    public function isHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * @return bool
     */
    public function isPublished(): bool
    {
        return $this->published;
    }

    public function isRelatedTo(self $related): bool
    {
        return null !== $this->findRelatedTemplateLink($related);
    }

    /**
     * @return bool
     */
    public function isService(): bool
    {
        return $this->service;
    }

    /**
     * @return bool
     */
    public function isSpotlight(): bool
    {
        return $this->spotlight;
    }

    /**
     * @param CompanyActivity $activity
     */
    public function removeActivity(CompanyActivity $activity)
    {
        $link = $this->findActivityLink($activity);
        if (null === $link) {
            return;
        }

        $this->removeActivityLink($link);
    }

    /**
     * @param CompanyActivityTemplate $link
     */
    public function removeActivityLink(CompanyActivityTemplate $link)
    {
        if (!$this->activityLinks->contains($link)) {
            return;
        }

        $this->activityLinks->removeElement($link);
    }

    /**
     * @param Advice $advice
     */
    public function removeAdvice(Advice $advice)
    {
        $link = $this->findAdviceLink($advice);
        if (null === $link) {
            return;
        }

        $this->removeAdviceLink($link);
    }

    /**
     * @param AdviceTemplate $link
     */
    public function removeAdviceLink(AdviceTemplate $link)
    {
        if (!$this->adviceLinks->contains($link)) {
            return;
        }

        $this->adviceLinks->removeElement($link);
    }

    /**
     * @param Category $category
     */
    public function removeCategory(Category $category)
    {
        $link = $this->findCategoryLink($category);
        if (null === $link) {
            throw new EntityNotFoundException($category);
        }

        $this->removeCategoryLink($link);
    }

    /**
     * @param CategoryTemplate $link
     */
    public function removeCategoryLink(CategoryTemplate $link)
    {
        if (!$this->categoryLinks->contains($link)) {
            return;
        }

        $this->categoryLinks->removeElement($link);
    }

    /**
     * @param Document $document
     */
    public function removeDocument(Document $document)
    {
        if (!$this->hasDocument($document)) {
            return;
        }

        $this->documents->removeElement($document);
    }

    /**
     * @param LandingPage $landingPage
     */
    public function removeLandingPage(LandingPage $landingPage)
    {
        $link = $this->findLandingPageLink($landingPage);
        if (null === $link) {
            throw new EntityNotFoundException($landingPage);
        }

        $this->removeLandingPageLink($link);
    }

    /**
     * @param LandingPageTemplate $link
     */
    public function removeLandingPageLink(LandingPageTemplate $link)
    {
        if (!$this->landingPageLinks->contains($link)) {
            return;
        }

        $this->landingPageLinks->removeElement($link);
    }

    /**
     * @param QuestionStep $step
     */
    public function removeQuestionStep(QuestionStep $step)
    {
        if (!$this->hasQuestionStep($step)) {
            return;
        }

        $this->questionSteps->removeElement($step);
    }

    /**
     * @param Referrer $referrer
     */
    public function removeReferrer(Referrer $referrer)
    {
        $link = $this->findReferrerLink($referrer);
        if (null === $link) {
            throw new EntityNotFoundException($referrer);
        }

        $this->removeReferrerLink($link);
    }

    /**
     * @param ReferrerTemplate $link
     */
    public function removeReferrerLink(ReferrerTemplate $link)
    {
        if (!$this->referrerLinks->contains($link)) {
            return;
        }

        $this->referrerLinks->removeElement($link);
    }

    public function removeRelatedTemplate(self $related): void
    {
        $link = $this->findRelatedTemplateLink($related);

        if (null !== $link) {
            $link->detach();
        }
    }

    public function removeRelatedTemplateLink(RelatedTemplate $link): void
    {
        $this->relatedTemplateLinks->removeElement($link);
    }

    public function removeRelatedToTemplateLink(RelatedTemplate $link): void
    {
        $this->relatedToTemplateLinks->removeElement($link);
    }

    /**
     * @param Subscription $subscription
     */
    public function removeSubscription(Subscription $subscription)
    {
        if (!$this->hasSubscription($subscription)) {
            return;
        }

        $this->subscriptions->removeElement($subscription);
    }

    /**
     * @param Update $update
     */
    public function removeUpdate(Update $update)
    {
        if (!$this->hasUpdate($update)) {
            return;
        }

        $this->updates->removeElement($update);
    }

    /**
     * Prijs voor handmatige controle instellen.
     *
     * @param Money $price
     */
    public function setCheckPrice(Money $price)
    {
        $this->priceCheck = $price;
    }

    /**
     * @param bool $corrupted
     */
    public function setCorrupted(bool $corrupted)
    {
        if (!$this->corrupted && $corrupted) {
            // TODO: TemplateSetAsCorrupted event
        }

        $this->corrupted = $corrupted;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description ?: null;
    }

    /**
     * @param mixed $discounts
     *
     * @return Template
     */
    public function setDiscounts($discounts)
    {
        $this->discounts = $discounts;

        return $this;
    }

    /**
     * @param string|null
     * @param mixed $excerpt
     */
    public function setExcerpt($excerpt)
    {
        $this->excerpt = $excerpt ?: null;
    }

    /**
     * @param bool $hidden
     */
    public function setHidden(bool $hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * @param Money|null $price
     */
    public function setMonthlyPrice($price)
    {
        Assertion::nullOrIsInstanceOf($price, Money::class);

        $this->priceMonthly = $price;
    }

    /**
     * Set the total number of orders for this template.
     *
     * @param int $orderCount
     */
    public function setOrderCount(int $orderCount)
    {
        $this->orderCount = $orderCount;
    }

    /**
     * Set the number of seconds an avarage order takes.
     *
     * @param int $orderSeconds
     */
    public function setOrderSeconds(int $orderSeconds)
    {
        $this->orderSeconds = $orderSeconds;
    }

    /**
     * @param string|null $preview
     */
    public function setPreview($preview)
    {
        $this->preview = $preview ?: null;
    }

    /**
     * @param bool $published
     */
    public function setPublished(bool $published)
    {
        $this->published = $published;
    }

    public function setRegisteredEmailPrice(?Money $registeredEmailPrice): void
    {
        $this->registeredEmailPrice = $registeredEmailPrice;
    }

    /**
     * @param string $remoteDescription
     */
    public function setRemoteDescription($remoteDescription)
    {
        $this->remoteDescription = $remoteDescription ?: null;
    }

    /**
     * @param mixed $remoteTitle
     */
    public function setRemoteTitle($remoteTitle)
    {
        $this->remoteTitle = $remoteTitle;
    }

    /**
     * @param bool $sample
     */
    public function setSample(bool $sample)
    {
        $this->sample = $sample;
    }

    /**
     * @param bool $service
     */
    public function setService(bool $service)
    {
        $this->service = $service;
    }

    /**
     * @param bool $spotlight
     */
    public function setSpotlight(bool $spotlight)
    {
        $this->spotlight = $spotlight;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @param Money $price
     */
    public function setUnitPrice(Money $price)
    {
        $this->priceUnit = $price;
    }

    /**
     * @param TemplateVersion $version
     */
    public function setVersion(TemplateVersion $version)
    {
        $this->version = $version;
    }

    /**
     * Update the sync timestamp.
     */
    public function updateSyncedTimestamp()
    {
        $this->syncedTimestamp = Carbon::now();
    }

    /**
     * @param CompanyActivity $activity
     *
     * @return CompanyActivityTemplate|null
     */
    private function findActivityLink(CompanyActivity $activity)
    {
        $links = $this->activityLinks->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('activity', $activity)
            )
        );

        return $links->isEmpty() ? null : $links->first();
    }

    /**
     * @param Advice $advice
     *
     * @return AdviceTemplate|null
     */
    private function findAdviceLink(Advice $advice)
    {
        $links = $this->adviceLinks->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('advice', $advice)
            )
        );

        return $links->isEmpty() ? null : $links->first();
    }

    /**
     * @param Category $category
     *
     * @return CategoryTemplate|null
     */
    private function findCategoryLink(Category $category)
    {
        $links = $this->categoryLinks->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('category', $category)
            )
        );

        return $links->isEmpty() ? null : $links->first();
    }

    /**
     * @param LandingPage $landingPage
     *
     * @return LandingPageTemplate|null
     */
    private function findLandingPageLink(LandingPage $landingPage)
    {
        $links = $this->landingPageLinks->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('landingPage', $landingPage)
            )
        );

        return $links->isEmpty() ? null : $links->first();
    }

    /**
     * @param Referrer $referrer
     *
     * @return ReferrerTemplate|null
     */
    private function findReferrerLink(Referrer $referrer)
    {
        $links = $this->referrerLinks->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('referrer', $referrer)
            )
        );

        return $links->isEmpty() ? null : $links->first();
    }

    /**
     * @return RelatedTemplate|null
     */
    private function findRelatedTemplateLink(self $related)
    {
        $links = $this->relatedTemplateLinks->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('related', $related)
            )
        );

        return $links->isEmpty() ? null : $links->first();
    }
}
