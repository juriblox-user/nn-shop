<?php

namespace NnShop\Domain\Templates\Entity;

use Carbon\Carbon;
use Core\Annotation\Doctrine as Core;
use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\Domain\Traits\SoftDeleteableEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use JuriBlox\Sdk\Domain\Offices\Values\OfficeId as RemoteId;
use NnShop\Domain\Templates\Event\PartnerSyncWasEnabled;
use NnShop\Domain\Templates\Event\PartnerWasCreated;
use NnShop\Domain\Templates\Value\PartnerId;
use NnShop\Domain\Translation\Entity\Profile;
use SimpleBus\Message\Recorder\ContainsRecordedMessages;
use SimpleBus\Message\Recorder\PrivateMessageRecorderCapabilities;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Templates\Repository\PartnerDoctrineRepository")
 * @Core\QueryBuilder(class="NnShop\Infrastructure\Templates\QueryBuilder\PartnerDoctrineQueryBuilder")
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(name="ix_deleted", columns={"timestamp_deleted"}),
 *     @ORM\Index(name="ix_sync", columns={"sync_enabled", "synced_timestamp", "timestamp_deleted"})
 * })
 *
 * @Gedmo\SoftDeleteable(fieldName="timestampDeleted")
 * @Gedmo\TranslationEntity(class="PartnerTranslation")
 */
class Partner implements ContainsRecordedMessages
{
    use PrivateMessageRecorderCapabilities;
    use SoftDeleteableEntityTrait;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Translatable
     *
     * @var string
     */
    private $excerpt;

    /**
     * @ORM\Id
     * @ORM\Column(type="templates.partner_id")
     *
     * @var PartnerId
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     *
     * @var string
     */
    private $logo;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Translatable
     *
     * @var string|null
     */
    private $profile;

    /**
     * @ORM\Column(type="juriblox.office_id", unique=true)
     *
     * @var RemoteId
     */
    private $remoteId;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     * @Gedmo\Slug(fields={"title"})
     * @Gedmo\Translatable
     *
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string
     */
    private $syncClientId;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string
     */
    private $syncClientKey;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $syncEnabled;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon
     */
    private $syncedTimestamp;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\Template", mappedBy="partner", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     *
     * @var Collection|Template[]
     */
    private $templates;

    /**
     * @ORM\Column(type="string", length=100)
     * @Gedmo\Translatable
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\OneToMany(
     *     targetEntity="PartnerTranslation",
     *     mappedBy="object",
     *     cascade={"persist", "remove"}
     * )
     */
    private $translations;

    /**
     * @var Profile
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Translation\Entity\Profile", inversedBy="partners")
     * @ORM\JoinColumn(name="profile_id", referencedColumnName="profile_id", onDelete="SET NULL")
     */
    private $countryProfile;

    /**
     * Partner constructor.
     *
     * @param PartnerId $id
     * @param Profile   $profile
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    private function __construct(PartnerId $id, Profile $profile)
    {
        $this->id = $id;
        $this->countryProfile = $profile;

        $this->syncEnabled = false;

        $this->countryProfile->addPartner($this);

        $this->templates = new ArrayCollection();
        $this->translations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }

    /**
     * @param Template $template
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    public function addTemplate(Template $template)
    {
        if ($this->hasTemplate($template)) {
            throw new DuplicateEntityException();
        }

        $this->templates->add($template);
    }

    /**
     * @param PartnerId $id
     * @param string    $title
     * @param Profile   $profile
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     *
     * @return Partner
     */
    public static function create(PartnerId $id, string $title, Profile $profile): self
    {
        $partner = new self($id, $profile);
        $partner->setTitle($title);

        $partner->record(new PartnerWasCreated($id));

        return $partner;
    }

    /**
     * Synchronisatie uitschakelen.
     */
    public function disableSync()
    {
        $this->syncEnabled = false;

        $this->syncClientId = null;
        $this->syncClientKey = null;
    }

    /**
     * @param string $clientId
     * @param string $clientKey
     */
    public function enableSync(string $clientId, string $clientKey)
    {
        $this->setClientId($clientId);
        $this->setClientKey($clientKey);

        if ($this->isSyncEnabled()) {
            return;
        }

        $this->syncEnabled = true;

        $this->record(new PartnerSyncWasEnabled($this->id));
    }

    /**
     * @return string|null
     */
    public function getClientId()
    {
        return $this->syncClientId;
    }

    /**
     * @return string|null
     */
    public function getClientKey()
    {
        return $this->syncClientKey;
    }

    /**
     * @return string
     */
    public function getExcerpt()
    {
        return $this->excerpt;
    }

    /**
     * @return PartnerId
     */
    public function getId(): PartnerId
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @return RemoteId
     */
    public function getRemoteId(): RemoteId
    {
        return $this->remoteId;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return Carbon|null
     */
    public function getSyncedTimestamp()
    {
        return $this->syncedTimestamp;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param Template $template
     *
     * @return bool
     */
    public function hasTemplate(Template $template): bool
    {
        return $this->templates->contains($template);
    }

    /**
     * @return bool
     */
    public function isSyncEnabled(): bool
    {
        return $this->syncEnabled;
    }

    /**
     * @param Template $template
     */
    public function removeTemplate(Template $template)
    {
        if (!$this->hasTemplate($template)) {
            return;
        }

        $this->templates->removeElement($template);
    }

    /**
     * @param string $clientId
     */
    public function setClientId(string $clientId)
    {
        $this->syncClientId = $clientId;
    }

    /**
     * @param string $clientKey
     */
    public function setClientKey(string $clientKey)
    {
        $this->syncClientKey = $clientKey;
    }

    /**
     * @param string $excerpt
     */
    public function setExcerpt($excerpt)
    {
        $this->excerpt = $excerpt;
    }

    /**
     * @param string $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo ?: null;
    }

    /**
     * @param RemoteId $remoteId
     */
    public function setRemoteId(RemoteId $remoteId)
    {
        $this->remoteId = $remoteId;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function updateSyncedTimestamp()
    {
        $this->syncedTimestamp = Carbon::now();
    }

    /**
     * @return PersistentCollection
     */
    public function getTranslations(): PersistentCollection
    {
        return $this->translations;
    }

    /**
     * @param PartnerTranslation $t
     */
    public function addTranslation(PartnerTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    /**
     * @return Profile
     */
    public function getCountryProfile(): Profile
    {
        return $this->countryProfile;
    }

    /**
     * @return null|string
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param string $profile
     */
    public function setProfile(string $profile)
    {
        $this->profile = $profile;
    }
}
