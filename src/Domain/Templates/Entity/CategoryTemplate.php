<?php

namespace NnShop\Domain\Templates\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\NnShop\Infrastructure\Templates\Repository\CategoryTemplateDoctrineRepository")
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(name="ix_primary", columns={"template_id", "category_primary"})
 * })
 */
class CategoryTemplate
{
    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Category", inversedBy="templateLinks")
     * @ORM\JoinColumn(referencedColumnName="category_id", nullable=false)
     *
     * @var Category
     */
    private $category;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="link_id")
     * @ORM\GeneratedValue
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", name="category_primary")
     *
     * @var bool
     */
    private $primary;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Template", inversedBy="categoryLinks")
     * @ORM\JoinColumn(referencedColumnName="template_id", nullable=false)
     *
     * @var Template
     */
    private $template;

    /**
     * TemplateCategory constructor.
     *
     * @param Category $category
     * @param Template $template
     */
    private function __construct(Category $category, Template $template)
    {
        $this->category = $category;
        $this->template = $template;

        $this->primary = $template->getCategories()->isEmpty();

        $this->category->addTemplateLink($this);
        $this->template->addCategoryLink($this);
    }

    /**
     * @param Category $category
     * @param Template $template
     *
     * @return CategoryTemplate
     */
    public static function create(Category $category, Template $template)
    {
        return new self($category, $template);
    }

    /**
     * Detach the link from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->category) {
            $this->category->removeTemplateLink($this);
            $this->category = null;
        }

        if (null !== $this->template) {
            $this->template->removeCategoryLink($this);
            $this->template = null;
        }
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->template;
    }

    /**
     * @return bool
     */
    public function isPrimary(): bool
    {
        return $this->primary;
    }

    /**
     * @param bool $primary
     */
    public function setPrimary(bool $primary)
    {
        $this->primary = $primary;
    }
}
