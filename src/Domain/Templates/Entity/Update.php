<?php

namespace NnShop\Domain\Templates\Entity;

use Carbon\Carbon;
use Core\Domain\Traits\SoftDeleteableEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NnShop\Domain\Templates\Event\Update\UpdateReceivedEvent;
use NnShop\Domain\Templates\Event\UpdateWasPublished;
use NnShop\Domain\Templates\Value\TemplateVersion;
use NnShop\Domain\Templates\Value\UpdateId;
use SimpleBus\Message\Recorder\ContainsRecordedMessages;
use SimpleBus\Message\Recorder\PrivateMessageRecorderCapabilities;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Templates\Repository\UpdateDoctrineRepository")
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(name="ix_deleted", columns={"timestamp_deleted"}),
 *     @ORM\Index(name="ix_template", columns={"template_id", "timestamp_deleted"})
 * })
 *
 * @Gedmo\SoftDeleteable(fieldName="timestampDeleted")
 */
class Update implements ContainsRecordedMessages
{
    use SoftDeleteableEntityTrait;
    use PrivateMessageRecorderCapabilities;

    /**
     * @ORM\Id
     * @ORM\Column(type="templates.update_id")
     *
     * @var UpdateId
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Template", inversedBy="updates")
     * @ORM\JoinColumn(referencedColumnName="template_id", nullable=false)
     *
     * @var Template
     */
    private $template;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon|null
     */
    private $timestampPublished;

    /**
     * @ORM\Column(type="core.time.datetime")
     *
     * @var Carbon
     */
    private $timestampReceived;

    /**
     * @ORM\Column(type="templates.template_version")
     *
     * @var TemplateVersion
     */
    private $version;

    /**
     * Update constructor.
     *
     * @param UpdateId $id
     * @param Template $template
     */
    private function __construct(UpdateId $id, Template $template)
    {
        $this->id = $id;

        $this->template = $template;
        $this->template->addUpdate($this);
    }

    /**
     * Detach the update from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->template) {
            $this->template->removeUpdate($this);
            $this->template = null;
        }
    }

    /**
     * @return UpdateId
     */
    public function getId(): UpdateId
    {
        return $this->id;
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->template;
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampPublished()
    {
        return $this->timestampPublished;
    }

    /**
     * @return Carbon
     */
    public function getTimestampReceived(): Carbon
    {
        return $this->timestampReceived;
    }

    /**
     * @return TemplateVersion
     */
    public function getVersion(): TemplateVersion
    {
        return $this->version;
    }

    /**
     * @return bool
     */
    public function isPublished(): bool
    {
        return null !== $this->timestampPublished;
    }

    public function publish()
    {
        if ($this->isPublished()) {
            throw new \DomainException();
        }

        $this->setTimestampPublished(Carbon::now());
        $this->record(new UpdateWasPublished($this->id));
    }

    /**
     * @param UpdateId        $id
     * @param Template        $template
     * @param TemplateVersion $version
     *
     * @return Update
     */
    public static function received(UpdateId $id, Template $template, TemplateVersion $version): self
    {
        $update = new self($id, $template);
        $update->setVersion($version);
        $update->setTimestampReceived(Carbon::now());

        // Versie overnemen in het template
        $template->setVersion($version);

        $update->record(UpdateReceivedEvent::create($id, $template->getId(), $version));

        return $update;
    }

    /**
     * @param Carbon $timestampPublished
     */
    private function setTimestampPublished(Carbon $timestampPublished)
    {
        $this->timestampPublished = $timestampPublished;
    }

    /**
     * @param Carbon $timestampReceived
     */
    private function setTimestampReceived(Carbon $timestampReceived)
    {
        $this->timestampReceived = $timestampReceived;
    }

    /**
     * @param TemplateVersion $version
     */
    private function setVersion(TemplateVersion $version)
    {
        $this->version = $version;
    }
}
