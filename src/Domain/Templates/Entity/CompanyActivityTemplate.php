<?php

namespace NnShop\Domain\Templates\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class CompanyActivityTemplate
{
    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\CompanyActivity", inversedBy="templateLinks")
     * @ORM\JoinColumn(referencedColumnName="activity_id", nullable=false)
     *
     * @var CompanyActivity
     */
    private $activity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="link_id")
     * @ORM\GeneratedValue
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Template", inversedBy="activityLinks")
     * @ORM\JoinColumn(referencedColumnName="template_id", nullable=false)
     *
     * @var Template
     */
    private $template;

    /**
     * CompanyActivityTemplate constructor.
     *
     * @param CompanyActivity $activity
     * @param Template        $template
     */
    private function __construct(CompanyActivity $activity, Template $template)
    {
        $this->activity = $activity;
        $this->template = $template;

        $this->activity->addTemplateLink($this);
        $this->template->addActivityLink($this);
    }

    /**
     * @param CompanyActivity $activity
     * @param Template        $template
     *
     * @return CompanyActivityTemplate
     */
    public static function create(CompanyActivity $activity, Template $template): self
    {
        return new self($activity, $template);
    }

    /**
     * Detach the link from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->activity) {
            $this->activity->removeTemplateLink($this);
            $this->activity = null;
        }

        if (null !== $this->template) {
            $this->template->removeActivityLink($this);
            $this->template = null;
        }
    }

    /**
     * @return CompanyActivity
     */
    public function getActivity(): CompanyActivity
    {
        return $this->activity;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->template;
    }
}
