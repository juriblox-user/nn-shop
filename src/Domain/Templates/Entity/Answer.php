<?php

namespace NnShop\Domain\Templates\Entity;

use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\Domain\Traits\SoftDeleteableEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NnShop\Domain\Templates\Value\AnswerId;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Templates\Repository\AnswerDoctrineRepository")
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(name="ix_deleted", columns={"timestamp_deleted"}),
 *     @ORM\Index(name="ix_document", columns={"document_id", "timestamp_deleted"}),
 *     @ORM\Index(name="ix_question", columns={"question_id", "timestamp_deleted"}),
 *     @ORM\Index(name="ix_answer_to", columns={"document_id", "question_id", "timestamp_deleted"}),
 * })
 *
 * @Gedmo\SoftDeleteable(fieldName="timestampDeleted")
 */
class Answer
{
    use SoftDeleteableEntityTrait;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Document", inversedBy="answers")
     * @ORM\JoinColumn(referencedColumnName="document_id", nullable=false)
     *
     * @var Document
     */
    private $document;

    /**
     * @ORM\Id
     * @ORM\Column(type="templates.answer_id")
     *
     * @var AnswerId
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="\NnShop\Domain\Templates\Entity\QuestionOption", inversedBy="answers", cascade={"persist"})
     *
     * @ORM\JoinTable(name="templates_answer_option",
     *     joinColumns={@ORM\JoinColumn(referencedColumnName="answer_id")},
     *     inverseJoinColumns={@ORM\JoinColumn(referencedColumnName="option_id")}
     * )
     *
     * @var Collection|QuestionOption[]
     */
    private $options;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Question", inversedBy="answers")
     * @ORM\JoinColumn(referencedColumnName="question_id", nullable=false)
     *
     * @var Question
     */
    private $question;

    /**
     * @ORM\Column(type="object", nullable=true)
     *
     * @var mixed
     */
    private $value;

    /**
     * Answer constructor.
     *
     * @param AnswerId $id
     * @param Document $document
     * @param Question $question
     */
    private function __construct(AnswerId $id, Document $document, Question $question)
    {
        $this->id = $id;

        $this->document = $document;
        $this->document->addAnswer($this);

        $this->question = $question;
        $this->question->addAnswer($this);

        $this->options = new ArrayCollection();
    }

    /**
     * @param QuestionOption $option
     */
    public function addOption(QuestionOption $option)
    {
        if (null !== $this->value) {
            throw new \DomainException('An answer cannot be both a QuestionOption and an explicit value. Hint: use Answer::clear().');
        }

        if ($this->hasOption($option)) {
            throw new DuplicateEntityException();
        }

        $this->options->add($option);

        $option->addAnswer($this);
    }

    public function clear()
    {
        $this->value = null;

        foreach ($this->options as $option) {
            $this->removeOption($option);
        }
    }

    /**
     * @param AnswerId $id
     * @param Document $document
     * @param Question $question
     *
     * @return Answer
     */
    public static function create(AnswerId $id, Document $document, Question $question): self
    {
        return new self($id, $document, $question);
    }

    /**
     * Detach the answer from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->document) {
            $this->document->removeAnswer($this);
            $this->document = null;
        }

        if (null !== $this->question) {
            $this->question->removeAnswer($this);
            $this->question = null;
        }

        foreach ($this->options as $option) {
            $this->removeOption($option);
        }
    }

    /**
     * @return Document
     */
    public function getDocument(): Document
    {
        return $this->document;
    }

    /**
     * @return AnswerId
     */
    public function getId(): AnswerId
    {
        return $this->id;
    }

    /**
     * @return Collection|QuestionOption[]
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    /**
     * @return Question
     */
    public function getQuestion(): Question
    {
        return $this->question;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param QuestionOption $option
     *
     * @return bool
     */
    public function hasOption(QuestionOption $option): bool
    {
        return $this->options->contains($option);
    }

    /**
     * @return bool
     */
    public function hasOptions(): bool
    {
        return !$this->options->isEmpty();
    }

    /**
     * @param QuestionOption $option
     */
    public function removeOption(QuestionOption $option)
    {
        if (!$this->hasOption($option)) {
            return;
        }

        $this->options->removeElement($option);

        $option->removeAnswer($this);
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        if (!$this->options->isEmpty()) {
            throw new \DomainException('An answer cannot be both an explicit value and a QuestionOption. Hint: use Answer::clear().');
        }

        $this->value = $value;
    }
}
