<?php

namespace NnShop\Domain\Templates\Entity;

use Carbon\Carbon;
use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\Domain\Traits\SoftDeleteableEntityTrait;
use Core\Domain\Value\Security\Token;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JuriBlox\Sdk\Domain\Documents\Values\DocumentId as RemoteId;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Entity\Subscription;
use NnShop\Domain\Orders\Entity\SubscriptionLink;
use NnShop\Domain\Orders\Value\OrderCode;
use NnShop\Domain\Orders\Value\OrderId;
use NnShop\Domain\Templates\Enumeration\DocumentStatus;
use NnShop\Domain\Templates\Event\DocumentGenerationFailedEvent;
use NnShop\Domain\Templates\Event\DocumentGenerationSucceededEvent;
use NnShop\Domain\Templates\Value\DocumentId;
use NnShop\Domain\Templates\Value\TemplateVersion;
use SimpleBus\Message\Recorder\ContainsRecordedMessages;
use SimpleBus\Message\Recorder\PrivateMessageRecorderCapabilities;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Templates\Repository\DocumentDoctrineRepository")
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(name="ix_status", columns={"document_status", "timestamp_deleted"}),
 *     @ORM\Index(name="ix_template", columns={"template_id", "timestamp_deleted"}),
 *     @ORM\Index(name="ix_remote", columns={"remote_id", "timestamp_deleted"})
 * })
 *
 * @Gedmo\SoftDeleteable(fieldName="timestampDeleted")
 */
class Document implements ContainsRecordedMessages
{
    use PrivateMessageRecorderCapabilities;
    use SoftDeleteableEntityTrait;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\Answer", mappedBy="document", cascade={"persist"}, fetch="EXTRA_LAZY")
     *
     * @var Collection|Answer[]
     */
    private $answers;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $cached;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $downloads;

    /**
     * @ORM\Id
     * @ORM\Column(type="templates.document_id")
     *
     * @var DocumentId
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="\NnShop\Domain\Orders\Entity\Order", inversedBy="document", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(referencedColumnName="order_id", nullable=false)
     *
     * @var Order
     */
    private $order;

    /**
     * @ORM\Column(type="juriblox.document_id", nullable=true, unique=true)
     *
     * @var RemoteId
     */
    private $remoteId;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\DocumentRequest", mappedBy="document", cascade={"persist"}, orphanRemoval=true, fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"timestampRequested": "DESC"})
     *
     * @var Collection|DocumentRequest[]
     */
    private $requests;

    /**
     * @ORM\Column(type="templates.document_status")
     *
     * @var DocumentStatus
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\SubscriptionLink", mappedBy="document", fetch="EXTRA_LAZY")
     *
     * @var Collection|SubscriptionLink[]
     */
    private $subscriptionLinks;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Template", inversedBy="documents", fetch="EAGER")
     * @ORM\JoinColumn(referencedColumnName="template_id", nullable=false)
     *
     * @var Template
     */
    private $template;

    /**
     * @var TemplateVersion
     * @ORM\Column(type="templates.template_version")
     */
    private $templateVersion;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon|null
     */
    private $timestampCanceled;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon|null
     */
    private $timestampError;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon|null
     */
    private $timestampGenerated;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon|null
     */
    private $timestampRequested;

    /**
     * @ORM\Column(type="core.security.short_token")
     *
     * @var Token|null
     */
    private $token;

    /**
     * Document constructor.
     *
     * @param DocumentId $id
     * @param Template   $template
     * @param Order      $order
     */
    private function __construct(DocumentId $id, Template $template, Order $order)
    {
        $this->id = $id;

        $this->cached = false;
        $this->downloads = 0;

        $this->template = $template;
        $this->template->addDocument($this);

        $this->order = $order;
        $this->order->linkDocument($this);

        $this->answers = new ArrayCollection();
        $this->requests = new ArrayCollection();
        $this->subscriptionLinks = new ArrayCollection();
    }

    /**
     * @param Answer $answer
     */
    public function addAnswer(Answer $answer)
    {
        if ($this->hasAnswer($answer)) {
            throw new DuplicateEntityException();
        }

        $this->answers->add($answer);
    }

    /**
     * @param DocumentRequest $request
     */
    public function addRequest(DocumentRequest $request)
    {
        if ($this->hasRequest($request)) {
            throw new DuplicateEntityException();
        }

        $this->requests->add($request);
    }

    /**
     * @param SubscriptionLink $link
     */
    public function addSubscriptionLink(SubscriptionLink $link)
    {
        if ($this->hasSubscriptionLink($link)) {
            throw new DuplicateEntityException();
        }

        $this->subscriptionLinks->add($link);
    }

    /**
     * Detach the document from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->template) {
            $this->template->removeDocument($this);
            $this->template = null;
        }

        if (null !== $this->order) {
            $this->order->unlinkDocument();
            $this->order = null;
        }
    }

    /**
     * @param Question $question
     *
     * @return Answer|null
     */
    public function getAnswer(Question $question)
    {
        $answers = $this->answers->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('question', $question)
            )
        );

        return $answers->isEmpty() ? null : $answers->first();
    }

    /**
     * @return Collection|Answer[]
     */
    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    /**
     * Aantal keer dat dit document is gedownload.
     *
     * @return int
     */
    public function getDownloads(): int
    {
        return $this->downloads;
    }

    /**
     * @return DocumentId
     */
    public function getId(): DocumentId
    {
        return $this->id;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return Partner
     */
    public function getPartner(): Partner
    {
        return $this->template->getPartner();
    }

    /**
     * @return Collection|DocumentRequest[]
     */
    public function getPendingRequests(): Collection
    {
        return $this->requests->matching(
            Criteria::create()->where(
                Criteria::expr()->andX(
                    Criteria::expr()->isNull('timestampCompleted'),
                    Criteria::expr()->isNull('timestampFailed')
                )
            )
        );
    }

    /**
     * @return RemoteId|null
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * @return Collection|DocumentRequest[]
     */
    public function getRequests(): Collection
    {
        return $this->requests;
    }

    /**
     * @return DocumentStatus
     */
    public function getStatus(): DocumentStatus
    {
        return $this->status;
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->template;
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampCanceled()
    {
        return $this->timestampCanceled;
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampError()
    {
        return $this->timestampError;
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampGenerated()
    {
        return $this->timestampGenerated;
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampRequested()
    {
        return $this->timestampRequested;
    }

    /**
     * @return Token|null
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param Answer $answer
     *
     * @return bool
     */
    public function hasAnswer(Answer $answer): bool
    {
        return $this->answers->contains($answer);
    }

    /**
     * @param DocumentRequest $request
     *
     * @return bool
     */
    public function hasRequest(DocumentRequest $request): bool
    {
        return $this->requests->contains($request);
    }

    /**
     * Bepalen of het document gekoppelde DocumentRequests heeft.
     *
     * @return bool
     */
    public function hasRequests(): bool
    {
        return !$this->requests->isEmpty();
    }

    /**
     * @param SubscriptionLink $link
     *
     * @return bool
     */
    public function hasSubscriptionLink(SubscriptionLink $link): bool
    {
        return $this->subscriptionLinks->contains($link);
    }

    /**
     * Downloads-teller ophogen.
     */
    public function incrementDownloads()
    {
        ++$this->downloads;
    }

    /**
     * @return bool
     */
    public function isCached(): bool
    {
        return $this->cached;
    }

    /**
     * @param DocumentId $id
     * @param Token      $token
     * @param Template   $template
     * @param Order      $order
     *
     * @return Document
     *
     * @see Template::order()       For ordering a document
     */
    public static function prepare(DocumentId $id, Token $token, Template $template, Order $order): self
    {
        $document = new self($id, $template, $order);
        $document->token = $token;

        $document->setStatus(DocumentStatus::from(DocumentStatus::DRAFT));

        return $document;
    }

    /**
     * @param Answer $answer
     */
    public function removeAnswer(Answer $answer)
    {
        if (!$this->hasAnswer($answer)) {
            return;
        }

        $this->answers->removeElement($answer);
    }

    /**
     * @param DocumentRequest $request
     */
    public function removeRequest(DocumentRequest $request)
    {
        if (!$this->hasRequest($request)) {
            return;
        }

        $this->requests->removeElement($request);
    }

    /**
     * @param SubscriptionLink $link
     */
    public function removeSubscriptionLink(SubscriptionLink $link)
    {
        if (!$this->hasSubscriptionLink($link)) {
            return;
        }

        $this->requests->removeElement($link);
    }

    /**
     * Prepare a re-order for this document.
     *
     * @param DocumentId $documentId
     * @param Token      $token
     * @param OrderId    $orderId
     * @param OrderCode  $orderNumber
     *
     * @return Document
     */
    public function reorder(DocumentId $documentId, Token $token, OrderId $orderId, OrderCode $orderNumber): self
    {
        $order = Order::prepareReorder($orderId, $orderNumber, $this->order);

        return self::prepare($documentId, $token, $this->template, $order);
    }

    /**
     * @param bool $cached
     */
    public function setCached(bool $cached)
    {
        $this->cached = $cached;
    }

    /**
     * @param RemoteId $remoteId
     */
    public function setRemoteId(RemoteId $remoteId)
    {
        $this->remoteId = $remoteId;
    }

    /**
     * @param DocumentStatus $status
     */
    public function setStatus(DocumentStatus $status)
    {
        if ($status == $this->status) {
            return;
        }

        switch ($status) {
            case DocumentStatus::DRAFT:
                if (null !== $this->status) {
                    throw new \LogicException('The document\'s DocumentStatus cannot be reverted to DRAFT');
                }

                break;

            case DocumentStatus::PENDING:
                if ($this->status->notIn([DocumentStatus::DRAFT, DocumentStatus::ERROR])) {
                    throw new \LogicException('The document\'s DocumentStatus should be DRAFT or ERROR');
                }

                $this->timestampRequested = Carbon::now();

                break;

            case DocumentStatus::GENERATED:
                if ($this->status->isNot(DocumentStatus::PENDING)) {
                    throw new \LogicException('The document\'s DocumentStatus should be PENDING');
                }

                $this->timestampGenerated = Carbon::now();
                $this->record(DocumentGenerationSucceededEvent::create($this->id));

                break;

            case DocumentStatus::ERROR:
                if ($this->status->isNot(DocumentStatus::PENDING)) {
                    throw new \LogicException('The document\'s DocumentStatus should be PENDING');
                }

                $this->timestampError = Carbon::now();
                $this->record(DocumentGenerationFailedEvent::create($this->id));

                break;

            case DocumentStatus::CANCELED:
                if ($this->status->notIn([DocumentStatus::DRAFT, DocumentStatus::PENDING])) {
                    throw new \LogicException('The document\'s DocumentStatus should be either DRAFT of PENDING');
                }

                $this->timestampCanceled = Carbon::now();

                break;

            default:
                throw new \InvalidArgumentException('Invalid status "%s"', $status->getValue());
        }

        $this->status = $status;
    }

    /**
     * Prepare an update (issued by a subscription) for this document.
     *
     * @param DocumentId   $documentId
     * @param Token        $token
     * @param OrderId      $orderId
     * @param OrderCode    $orderNumber
     * @param Subscription $subscription
     *
     * @return Document
     */
    public function update(DocumentId $documentId, Token $token, OrderId $orderId, OrderCode $orderNumber, Subscription $subscription): self
    {
        $order = Order::prepareUpdate($orderId, $orderNumber, $subscription);

        return self::prepare($documentId, $token, $this->template, $order);
    }

    /**
     * @return TemplateVersion
     */
    public function getTemplateVersion(): TemplateVersion
    {
        return $this->templateVersion;
    }

    /**
     * @param TemplateVersion $templateVersion
     */
    public function setTemplateVersion(TemplateVersion $templateVersion)
    {
        $this->templateVersion = $templateVersion;
    }
}
