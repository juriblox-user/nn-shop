<?php

namespace NnShop\Domain\Templates\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;

/**
 * @ORM\Entity(repositoryClass="Gedmo\Translatable\Entity\Repository\TranslationRepository")
 * @ORM\Table(name="templates_company_activity_translations",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_idx", columns={
 *         "translation_locale", "object_id", "translation_field"
 *     })}
 * )
 */
class CompanyActivityTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="CompanyActivity", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="activity_id", onDelete="CASCADE")
     */
    protected $object;

    /**
     * CompanyActivityTranslation constructor.
     *
     * @param string $locale
     * @param string $field
     * @param string $value
     */
    public function __construct(string $locale, string $field, string $value)
    {
        $this->setLocale($locale);
        $this->setField($field);
        $this->setContent($value);
    }
}
