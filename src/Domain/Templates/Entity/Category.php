<?php

namespace NnShop\Domain\Templates\Entity;

use Core\Annotation\Doctrine as Core;
use Core\Common\Exception\Domain\DuplicateEntityException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use NnShop\Domain\Shops\Entity\Shop;
use NnShop\Domain\Templates\Value\CategoryId;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Templates\Repository\CategoryDoctrineRepository")
 * @Core\QueryBuilder(class="NnShop\Infrastructure\Templates\QueryBuilder\CategoryDoctrineQueryBuilder")
 *
 * @ORM\Table(
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="uq_slug", columns={"shop_id", "category_slug"})
 *     }
 * )
 * @Gedmo\TranslationEntity(class="CategoryTranslation")
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\Column(type="templates.category_id")
     *
     * @var CategoryId
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string | null
     */
    private $description;

    /**
     * @ORM\Column(type="string", nullable=true, length=160)
     * @var string | null
     */
    private $metaDescription;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Shops\Entity\Shop", inversedBy="categories")
     * @ORM\JoinColumn(referencedColumnName="shop_id", nullable=false)
     *
     * @var Shop
     */
    private $shop;

    /**
     * @ORM\Column(type="string", length=100)
     * @Gedmo\Slug(fields={"title"}, unique_base="shop")
     * @Gedmo\Translatable
     *
     * @var string
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Templates\Entity\CategoryTemplate", mappedBy="category", cascade={"persist", "remove"}, orphanRemoval=true, fetch="EXTRA_LAZY")
     *
     * @var Collection|CategoryTemplate[]
     */
    private $templateLinks;

    /**
     * @ORM\Column(type="string", length=100)
     * @Gedmo\Translatable
     *
     * @var string
     */
    private $title;

    /**
     * @var bool
     */
    private $primary;

    /**
     * @ORM\OneToMany(
     *     targetEntity="CategoryTranslation",
     *     mappedBy="object",
     *     cascade={"persist", "remove"}
     * )
     */
    private $translations;

    /**
     * Category constructor.
     *
     * @param CategoryId $id
     * @param Shop       $shop
     */
    private function __construct(CategoryId $id, Shop $shop)
    {
        $this->id = $id;

        $this->shop = $shop;
        $this->shop->addCategory($this);

        $this->templateLinks = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }

    /**
     * @throws \LogicException
     *
     * @return bool
     */
    public function isPrimary(): bool
    {
        if (null === $this->primary) {
            throw new \LogicException('The Category::isPrimary() function is only available when the Category is loaded through Category::fromLink($categoryLink)');
        }

        return $this->primary;
    }

    /**
     * @internal
     *
     * @see Template::getCategories()
     *
     * @param bool $primary
     */
    public function setPrimary(bool $primary)
    {
        $this->primary = $primary;
    }

    /**
     * @param Template $template
     *
     * @return CategoryTemplate
     */
    public function addTemplate(Template $template): CategoryTemplate
    {
        return CategoryTemplate::create($this, $template);
    }

    /**
     * @param CategoryTemplate $link
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    public function addTemplateLink(CategoryTemplate $link)
    {
        if ($this->hasTemplate($link->getTemplate())) {
            throw new DuplicateEntityException();
        }

        $this->templateLinks->add($link);
    }

    /**
     * @param CategoryId $id
     * @param Shop $shop
     * @param string $title
     * @param string|null $description
     * @param string|null $metaDescription
     * @return Category
     */
    public static function create(CategoryId $id, Shop $shop, string $title, ?string $description, ?string $metaDescription): self
    {
        $category = new self($id, $shop);
        $category->setTitle($title);
        $category->setDescription($description);
        $category->setMetaDescription($metaDescription);

        return $category;
    }

    /**
     * Detach the category from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->shop) {
            $this->shop->removeCategory($this);
            $this->shop = null;
        }
    }

    /**
     * @return CategoryId
     */
    public function getId(): CategoryId
    {
        return $this->id;
    }

    /**
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return Collection|Template[]
     */
    public function getTemplates(): Collection
    {
        $templates = new ArrayCollection();
        foreach ($this->templateLinks as $link) {
            $templates->add($link->getTemplate());
        }

        return $templates;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param Template $template
     *
     * @return bool
     */
    public function hasTemplate(Template $template): bool
    {
        return null !== $this->findTemplateLink($template);
    }

    /**
     * @param Template $template
     */
    public function removeTemplate(Template $template)
    {
        $link = $this->findTemplateLink($template);
        if (null === $link) {
            return;
        }

        $this->removeTemplateLink($link);
        $template->removeCategoryLink($link);
    }

    /**
     * @param CategoryTemplate $link
     */
    public function removeTemplateLink(CategoryTemplate $link)
    {
        if (!$this->templateLinks->contains($link)) {
            return;
        }

        $this->templateLinks->removeElement($link);
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return PersistentCollection
     */
    public function getTranslations(): PersistentCollection
    {
        return $this->translations;
    }

    /**
     * @param CategoryTranslation $t
     */
    public function addTranslation(CategoryTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    /**
     * @param Template $template
     *
     * @return CategoryTemplate|null
     */
    private function findTemplateLink(Template $template)
    {
        $links = $this->templateLinks->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('template', $template)
            )
        );

        return $links->isEmpty() ? null : $links->first();
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * @param string|null $metaDescription
     */
    public function setMetaDescription(?string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }
}
