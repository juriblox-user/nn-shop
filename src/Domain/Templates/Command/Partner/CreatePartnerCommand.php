<?php

namespace NnShop\Domain\Templates\Command\Partner;

use JuriBlox\Sdk\Domain\Offices\Values\OfficeId as RemoteId;
use NnShop\Domain\Templates\Value\PartnerId;
use NnShop\Domain\Translation\Value\ProfileId;
use Symfony\Component\HttpFoundation\File\File;

class CreatePartnerCommand
{
    /**
     * @var string
     */
    private $excerpt;

    /**
     * @var PartnerId
     */
    private $id;

    /**
     * @var File
     */
    private $logo;

    /**
     * @var string
     */
    private $profile;

    /**
     * @var RemoteId
     */
    private $remoteId;

    /**
     * @var string
     */
    private $syncClientId;

    /**
     * @var string
     */
    private $syncClientKey;

    /**
     * @var bool
     */
    private $syncEnabled;

    /**
     * @var string
     */
    private $title;

    /**
     * @var ProfileId
     */
    private $profileId;

    /**
     * CreatePartnerCommand constructor.
     *
     * @param PartnerId $id
     * @param RemoteId  $remoteId
     * @param ProfileId $profileId
     */
    private function __construct(PartnerId $id, RemoteId $remoteId, ProfileId $profileId)
    {
        $this->id = $id;
        $this->remoteId = $remoteId;
        $this->profileId = $profileId;

        $this->syncEnabled = false;
    }

    /**
     * @param string $clientId
     * @param string $clientKey
     */
    public function enableSync($clientId, $clientKey)
    {
        $this->syncEnabled = true;

        $this->syncClientId = $clientId;
        $this->syncClientKey = $clientKey;
    }

    /**
     * @return string
     */
    public function getExcerpt()
    {
        return $this->excerpt;
    }

    /**
     * @return PartnerId
     */
    public function getId(): PartnerId
    {
        return $this->id;
    }

    /**
     * @return File
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @return string
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @return RemoteId
     */
    public function getRemoteId(): RemoteId
    {
        return $this->remoteId;
    }

    /**
     * @return string|null
     */
    public function getSyncClientId()
    {
        return $this->syncClientId;
    }

    /**
     * @return string|null
     */
    public function getSyncClientKey()
    {
        return $this->syncClientKey;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function hasLogo(): bool
    {
        return null !== $this->logo;
    }

    /**
     * @return bool
     */
    public function isSyncEnabled(): bool
    {
        return $this->syncEnabled;
    }

    /**
     * @param PartnerId $id
     * @param RemoteId  $remoteId
     * @param ProfileId $profileId
     *
     * @return CreatePartnerCommand
     */
    public static function prepare(PartnerId $id, RemoteId $remoteId, ProfileId $profileId): self
    {
        return new self($id, $remoteId, $profileId);
    }

    /**
     * @param string $excerpt
     */
    public function setExcerpt(string $excerpt)
    {
        $this->excerpt = $excerpt;
    }

    /**
     * @param File $logo
     */
    public function setLogo(File $logo)
    {
        $this->logo = $logo;
    }

    /**
     * @param string $profile
     */
    public function setProfile(string $profile)
    {
        $this->profile = $profile;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return ProfileId
     */
    public function getProfileId(): ProfileId
    {
        return $this->profileId;
    }
}
