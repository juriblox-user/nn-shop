<?php

namespace NnShop\Domain\Templates\Command\Partner;

use JMS\Serializer\Annotation as Serializer;
use JuriBlox\Sdk\Domain\Offices\Values\OfficeId as RemoteId;
use NnShop\Domain\Templates\Value\PartnerId as LocalId;

class SynchronizePartnerCommand
{
    /**
     * @Serializer\Type("boolean")
     *
     * @var bool
     */
    private $forced;

    /**
     * @Serializer\Type("NnShop\Domain\Templates\Value\PartnerId")
     *
     * @var LocalId
     */
    private $localId;

    /**
     * @Serializer\Type("JuriBlox\Sdk\Domain\Offices\Values\OfficeId")
     *
     * @var RemoteId
     */
    private $remoteId;

    /**
     * SynchronizeTemplateCommand constructor.
     */
    private function __construct()
    {
        $this->forced = false;
    }

    /**
     * Create a command based on a known local PartnerId.
     *
     * @param LocalId $localId
     *
     * @return SynchronizePartnerCommand
     */
    public static function fromLocalId(LocalId $localId): self
    {
        $command = new static();
        $command->localId = $localId;

        return $command;
    }

    /**
     * Create a command based on a known remote OfficeId.
     *
     * @param RemoteId $remoteId
     *
     * @return SynchronizePartnerCommand
     */
    public static function fromRemoteId(RemoteId $remoteId): self
    {
        $command = new self();
        $command->remoteId = $remoteId;

        return $command;
    }

    /**
     * @return LocalId
     */
    public function getLocalId(): LocalId
    {
        return $this->localId;
    }

    /**
     * @return RemoteId
     */
    public function getRemoteId(): RemoteId
    {
        return $this->remoteId;
    }

    /**
     * @return bool
     */
    public function isForced(): bool
    {
        return $this->forced;
    }

    /**
     * @param bool $forced
     */
    public function setForced(bool $forced)
    {
        $this->forced = $forced;
    }
}
