<?php

namespace NnShop\Domain\Templates\Command\Partner;

use JuriBlox\Sdk\Domain\Offices\Values\OfficeId as RemoteId;
use NnShop\Domain\Templates\Value\PartnerId;
use Symfony\Component\HttpFoundation\File\File;

class UpdatePartnerCommand
{
    /**
     * @var bool
     */
    private $deleteLogo;

    /**
     * @var string
     */
    private $excerpt;

    /**
     * @var PartnerId
     */
    private $id;

    /**
     * @var File
     */
    private $logo;

    /**
     * @var string
     */
    private $profile;

    /**
     * @var RemoteId
     */
    private $remoteId;

    /**
     * @var string
     */
    private $syncClientId;

    /**
     * @var string
     */
    private $syncClientKey;

    /**
     * @var bool
     */
    private $syncEnabled;

    /**
     * @var string
     */
    private $title;

    /**
     * UpdatePartnerCommand constructor.
     *
     * @param PartnerId $id
     */
    private function __construct(PartnerId $id)
    {
        $this->id = $id;

        $this->deleteLogo = false;
        $this->syncEnabled = false;
    }

    /**
     * @param bool $deleteLogo
     */
    public function setDeleteLogo(bool $deleteLogo)
    {
        $this->deleteLogo = $deleteLogo;
    }

    /**
     * @return bool
     */
    public function deleteLogo(): bool
    {
        return $this->deleteLogo;
    }

    /**
     * Synchronisatie uitschakelen.
     */
    public function disableSync()
    {
        $this->syncEnabled = false;
    }

    /**
     * @param $clientId
     * @param $clientKey
     */
    public function enableSync($clientId, $clientKey)
    {
        $this->syncEnabled = true;

        $this->syncClientId = $clientId;
        $this->syncClientKey = $clientKey;
    }

    /**
     * @return string
     */
    public function getExcerpt()
    {
        return $this->excerpt;
    }

    /**
     * @return PartnerId
     */
    public function getId(): PartnerId
    {
        return $this->id;
    }

    /**
     * @return File|null
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @return string
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @return RemoteId|null
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * @return string
     */
    public function getSyncClientId()
    {
        return $this->syncClientId;
    }

    /**
     * @return string
     */
    public function getSyncClientKey()
    {
        return $this->syncClientKey;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function hasLogo(): bool
    {
        return null !== $this->logo;
    }

    /**
     * @return bool|null
     */
    public function isSyncEnabled()
    {
        return $this->syncEnabled;
    }

    /**
     * @param PartnerId $id
     *
     * @return UpdatePartnerCommand
     */
    public static function prepare(PartnerId $id): self
    {
        return new self($id);
    }

    /**
     * @param string $excerpt
     */
    public function setExcerpt($excerpt)
    {
        $this->excerpt = $excerpt ?: null;
    }

    /**
     * @param File $logo
     */
    public function setLogo(File $logo)
    {
        $this->logo = $logo;
    }

    /**
     * @param string $profile
     */
    public function setProfile($profile)
    {
        $this->profile = $profile ?: null;
    }

    /**
     * @param RemoteId $remoteId
     */
    public function setRemoteId(RemoteId $remoteId)
    {
        $this->remoteId = $remoteId;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }
}
