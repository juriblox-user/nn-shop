<?php

namespace NnShop\Domain\Templates\Command\Partner;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Templates\Value\PartnerId;

class SynchronizePartnerAsyncCommand
{
    /**
     * @Serializer\Type("NnShop\Domain\Templates\Command\Partner\SynchronizePartnerCommand")
     *
     * @var SynchronizePartnerCommand
     */
    private $command;

    /**
     * SynchronizePartnerAsyncCommand constructor.
     */
    private function __construct()
    {
    }

    /**
     * @param PartnerId $partnerId
     *
     * @return SynchronizePartnerAsyncCommand
     */
    public static function create(PartnerId $partnerId): self
    {
        return static::from(SynchronizePartnerCommand::fromLocalId($partnerId));
    }

    /**
     * @param SynchronizePartnerCommand $synchronousCommand
     *
     * @return SynchronizePartnerAsyncCommand
     */
    public static function from(SynchronizePartnerCommand $synchronousCommand): self
    {
        $command = new self();
        $command->command = $synchronousCommand;

        return $command;
    }

    /**
     * @return SynchronizePartnerCommand
     */
    public function getCommand(): SynchronizePartnerCommand
    {
        return $this->command;
    }
}
