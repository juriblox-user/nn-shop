<?php

namespace NnShop\Domain\Templates\Command\DocumentRequest;

use NnShop\Domain\Templates\Value\DocumentRequestId;

class ProcessFailedRequestCommand
{
    /**
     * @var DocumentRequestId
     */
    private $id;

    /**
     * SetRequestFailedStatusCommand constructor.
     *
     * @param DocumentRequestId $requestId
     */
    private function __construct(DocumentRequestId $requestId)
    {
        $this->id = $requestId;
    }

    /**
     * @param DocumentRequestId $requestId
     *
     * @return ProcessFailedRequestCommand
     */
    public static function create(DocumentRequestId $requestId): self
    {
        return new self($requestId);
    }

    /**
     * @return DocumentRequestId
     */
    public function getId(): DocumentRequestId
    {
        return $this->id;
    }
}
