<?php

namespace NnShop\Domain\Templates\Command\DocumentRequest;

use NnShop\Domain\Templates\Value\DocumentId;

class SynchronizePendingRequestsCommand
{
    /**
     * @var DocumentId
     */
    private $documentId;

    /**
     * SynchronizePendingRequestsCommand constructor.
     *
     * @param DocumentId $documentId
     */
    public function __construct(DocumentId $documentId)
    {
        $this->documentId = $documentId;
    }

    /**
     * @param DocumentId $documentId
     *
     * @return SynchronizePendingRequestsCommand
     */
    public static function create(DocumentId $documentId): self
    {
        return new self($documentId);
    }

    /**
     * @return DocumentId
     */
    public function getDocumentId(): DocumentId
    {
        return $this->documentId;
    }
}
