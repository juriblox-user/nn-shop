<?php

namespace NnShop\Domain\Templates\Command\DocumentRequest;

use NnShop\Domain\Templates\Value\DocumentRequestId;

class SynchronizeRequestCommand
{
    /**
     * @var DocumentRequestId
     */
    private $id;

    /**
     * SynchronizeRequestCommand constructor.
     *
     * @param DocumentRequestId $id
     */
    private function __construct(DocumentRequestId $id)
    {
        $this->id = $id;
    }

    /**
     * @param DocumentRequestId $id
     *
     * @return SynchronizeRequestCommand
     */
    public static function create(DocumentRequestId $id): self
    {
        return new self($id);
    }

    /**
     * @return DocumentRequestId
     */
    public function getId(): DocumentRequestId
    {
        return $this->id;
    }
}
