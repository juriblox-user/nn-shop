<?php

namespace NnShop\Domain\Templates\Command\DocumentRequest;

use JuriBlox\Sdk\Domain\Documents\Values\DocumentId as RemoteDocumentId;
use NnShop\Domain\Templates\Value\DocumentRequestId;

class ProcessSuccessfulRequestCommand
{
    /**
     * @var RemoteDocumentId
     */
    private $documentId;

    /**
     * @var DocumentRequestId
     */
    private $requestId;

    /**
     * ProcessRequestSucceededStatusCommand constructor.
     *
     * @param DocumentRequestId $requestId
     * @param RemoteDocumentId  $documentId
     */
    private function __construct(DocumentRequestId $requestId, RemoteDocumentId $documentId)
    {
        $this->requestId = $requestId;
        $this->documentId = $documentId;
    }

    /**
     * @param DocumentRequestId $requestId
     *
     * @return ProcessSuccessfulRequestCommand
     */
    public static function create(DocumentRequestId $requestId, RemoteDocumentId $documentId): self
    {
        return new self($requestId, $documentId);
    }

    /**
     * @return RemoteDocumentId
     */
    public function getRemoteDocumentId(): RemoteDocumentId
    {
        return $this->documentId;
    }

    /**
     * @return DocumentRequestId
     */
    public function getRequestId(): DocumentRequestId
    {
        return $this->requestId;
    }
}
