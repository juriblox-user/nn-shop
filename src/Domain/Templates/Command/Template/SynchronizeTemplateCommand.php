<?php

namespace NnShop\Domain\Templates\Command\Template;

use JMS\Serializer\Annotation as Serializer;
use JuriBlox\Sdk\Domain\Documents\Values\TemplateId as RemoteId;
use NnShop\Domain\Templates\Value\PartnerId;
use NnShop\Domain\Templates\Value\TemplateId as LocalId;

class SynchronizeTemplateCommand
{
    /**
     * @Serializer\Type("boolean")
     *
     * @var bool
     */
    private $forced;

    /**
     * @Serializer\Type("NnShop\Domain\Templates\Value\TemplateId")
     *
     * @var LocalId
     */
    private $localId;

    /**
     * @Serializer\Type("NnShop\Domain\Templates\Value\PartnerId")
     *
     * @var PartnerId
     */
    private $partnerId;

    /**
     * @Serializer\Type("JuriBlox\Sdk\Domain\Documents\Values\TemplateId")
     *
     * @var RemoteId
     */
    private $remoteId;

    /**
     * SynchronizeTemplateCommand constructor.
     */
    private function __construct()
    {
        $this->forced = false;
    }

    /**
     * Create a command based on a known local TemplateId.
     *
     * @param LocalId   $localId
     * @param PartnerId $partnerId
     *
     * @return SynchronizeTemplateCommand
     */
    public static function fromLocalId(LocalId $localId, PartnerId $partnerId = null): self
    {
        $command = new self();
        $command->localId = $localId;
        $command->partnerId = $partnerId;

        return $command;
    }

    /**
     * Create a command based on a known remote TemplateId.
     *
     * @param RemoteId  $remoteId
     * @param PartnerId $partnerId
     *
     * @return SynchronizeTemplateCommand
     */
    public static function fromRemoteId(RemoteId $remoteId, PartnerId $partnerId): self
    {
        $command = new self();
        $command->remoteId = $remoteId;
        $command->partnerId = $partnerId;

        return $command;
    }

    /**
     * @return LocalId
     */
    public function getLocalId()
    {
        return $this->localId;
    }

    /**
     * @return PartnerId
     */
    public function getPartnerId()
    {
        if (null == $this->partnerId) {
            throw new \LogicException('No PartnerId has been provided. Use SynchronizeTemplateCommand::hasPartnerId() to check this before trying to use getPartnerId()');
        }

        return $this->partnerId;
    }

    /**
     * @return RemoteId
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * @return bool
     */
    public function hasPartnerId(): bool
    {
        return null !== $this->partnerId;
    }

    /**
     * @return bool
     */
    public function isForced(): bool
    {
        return $this->forced;
    }

    /**
     * @param bool $forced
     */
    public function setForced(bool $forced)
    {
        $this->forced = $forced;
    }
}
