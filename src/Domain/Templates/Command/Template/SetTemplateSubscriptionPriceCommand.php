<?php

namespace NnShop\Domain\Templates\Command\Template;

use NnShop\Domain\Templates\Value\TemplateId;
use Money\Money;

class SetTemplateSubscriptionPriceCommand
{
    /**
     * @var TemplateId
     */
    private $id;

    /**
     * @var Money
     */
    private $monthlyPrice;

    /**
     * SetTemplateUnitPriceCommand constructor.
     *
     * @param TemplateId $id
     * @param Money      $monthlyPrice
     */
    private function __construct(TemplateId $id, Money $monthlyPrice)
    {
        $this->id = $id;
        $this->monthlyPrice = $monthlyPrice;
    }

    /**
     * @param TemplateId $id
     * @param Money      $monthlyPrice
     *
     * @return SetTemplateSubscriptionPriceCommand
     */
    public static function create(TemplateId $id, Money $monthlyPrice): self
    {
        return new self($id, $monthlyPrice);
    }

    /**
     * @return TemplateId
     */
    public function getId(): TemplateId
    {
        return $this->id;
    }

    /**
     * @return Money
     */
    public function getMonthlyPrice(): Money
    {
        return $this->monthlyPrice;
    }
}
