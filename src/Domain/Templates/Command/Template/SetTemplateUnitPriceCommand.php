<?php

namespace NnShop\Domain\Templates\Command\Template;

use NnShop\Domain\Templates\Value\TemplateId;
use Money\Money;

class SetTemplateUnitPriceCommand
{
    /**
     * @var TemplateId
     */
    private $id;

    /**
     * @var Money
     */
    private $price;

    /**
     * SetTemplateUnitPriceCommand constructor.
     *
     * @param TemplateId $id
     * @param Money      $price
     */
    private function __construct(TemplateId $id, Money $price)
    {
        $this->id = $id;
        $this->price = $price;
    }

    /**
     * @param TemplateId $id
     * @param Money      $price
     *
     * @return SetTemplateUnitPriceCommand
     */
    public static function create(TemplateId $id, Money $price): self
    {
        return new self($id, $price);
    }

    /**
     * @return TemplateId
     */
    public function getId(): TemplateId
    {
        return $this->id;
    }

    /**
     * @return Money
     */
    public function getPrice(): Money
    {
        return $this->price;
    }
}
