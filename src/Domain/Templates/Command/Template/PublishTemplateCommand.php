<?php

namespace NnShop\Domain\Templates\Command\Template;

use NnShop\Domain\Templates\Value\TemplateId;

class PublishTemplateCommand
{
    /**
     * @var TemplateId
     */
    private $id;

    /**
     * PublishTemplateCommand constructor.
     *
     * @param TemplateId $id
     */
    private function __construct(TemplateId $id)
    {
        $this->id = $id;
    }

    /**
     * @param TemplateId $id
     *
     * @return PublishTemplateCommand
     */
    public static function create(TemplateId $id): self
    {
        return new self($id);
    }

    /**
     * @return TemplateId
     */
    public function getId(): TemplateId
    {
        return $this->id;
    }
}
