<?php

namespace NnShop\Domain\Templates\Command\Template;

use NnShop\Domain\Templates\Value\TemplateId;
use Money\Money;
use Symfony\Component\HttpFoundation\File\File;

class UpdateTemplateCommand
{
    /**
     * @var Money
     */
    private $checkPrice;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $excerpt;

    /**
     * @var bool
     */
    private $hidden;

    /**
     * @var TemplateId
     */
    private $id;

    /**
     * @var Money
     */
    private $montlyPrice;

    /**
     * @var File
     */
    private $preview;

    /**
     * @var Money|null
     */
    private $registeredEmailPrice;

    /**
     * @var File
     */
    private $sample;

    /**
     * @var bool
     */
    private $service;

    /**
     * @var bool
     */
    private $spotlight;

    /**
     * @var bool
     */
    private $subscription;

    /**
     * @var string
     */
    private $title;

    /**
     * @var Money
     */
    private $unitPrice;

    /**
     * UpdateTemplateCommand constructor.
     *
     * @param TemplateId $id
     */
    private function __construct(TemplateId $id)
    {
        $this->id = $id;
    }

    /**
     * @param TemplateId $id
     *
     * @return UpdateTemplateCommand
     */
    public static function prepare(TemplateId $id): self
    {
        return new self($id);
    }

    /**
     * @return Money
     */
    public function getCheckPrice(): Money
    {
        return $this->checkPrice;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getExcerpt()
    {
        return $this->excerpt;
    }

    /**
     * @return TemplateId
     */
    public function getId(): TemplateId
    {
        return $this->id;
    }

    /**
     * @return Money|null
     */
    public function getMontlyPrice()
    {
        return $this->montlyPrice;
    }

    /**
     * @return File|null
     */
    public function getPreview()
    {
        return $this->preview;
    }

    public function getRegisteredEmailPrice(): ?Money
    {
        return $this->registeredEmailPrice;
    }

    /**
     * @return File
     */
    public function getSample(): File
    {
        return $this->sample;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Money
     */
    public function getUnitPrice(): Money
    {
        return $this->unitPrice;
    }

    /**
     * @return bool
     */
    public function hasPreview(): bool
    {
        return null !== $this->preview;
    }

    /**
     * @return bool
     */
    public function hasSample(): bool
    {
        return null !== $this->sample;
    }

    /**
     * @return bool
     */
    public function isHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * @return bool
     */
    public function isService(): bool
    {
        return $this->service;
    }

    /**
     * @return bool|null
     */
    public function isSpotlight()
    {
        return $this->spotlight;
    }

    /**
     * @return bool|null
     */
    public function isSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param Money $checkPrice
     */
    public function setCheckPrice(Money $checkPrice)
    {
        $this->checkPrice = $checkPrice;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description ?: null;
    }

    /**
     * @param string $excerpt
     */
    public function setExcerpt(string $excerpt)
    {
        $this->excerpt = $excerpt ?: null;
    }

    /**
     * @param bool $hidden
     */
    public function setHidden(bool $hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * @param Money $montlyPrice
     */
    public function setMontlyPrice(Money $montlyPrice)
    {
        $this->montlyPrice = $montlyPrice;
    }

    /**
     * @param File $preview
     */
    public function setPreview(File $preview)
    {
        $this->preview = $preview;
    }

    public function setRegisteredEmailPrice(?Money $registeredEmailPrice): void
    {
        $this->registeredEmailPrice = $registeredEmailPrice;
    }

    /**
     * @param File $sample
     */
    public function setSample(File $sample)
    {
        $this->sample = $sample;
    }

    /**
     * @param bool $service
     */
    public function setService(bool $service)
    {
        $this->service = $service;
    }

    /**
     * @param bool $spotlight
     */
    public function setSpotlight(bool $spotlight)
    {
        $this->spotlight = $spotlight;
    }

    /**
     * @param bool $subscription
     */
    public function setSubscription(bool $subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * @param Money $unitPrice
     */
    public function setUnitPrice(Money $unitPrice)
    {
        $this->unitPrice = $unitPrice;
    }
}
