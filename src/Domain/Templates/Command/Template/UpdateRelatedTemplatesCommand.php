<?php

namespace NnShop\Domain\Templates\Command\Template;

use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Domain\Templates\Value\CategoryId;
use NnShop\Domain\Templates\Value\TemplateId;

final class UpdateRelatedTemplatesCommand
{
    /**
     * @var TemplateId
     */
    private $id;

    /**
     * @var ArrayCollection|CategoryId[]
     */
    private $selected;

    private function __construct(TemplateId $id)
    {
        $this->id       = $id;
        $this->selected = new ArrayCollection();
    }

    /**
     * @return UpdateRelatedTemplatesCommand
     */
    public static function prepare(TemplateId $id): self
    {
        return new self($id);
    }

    public function addRelated(TemplateId $id): void
    {
        $this->selected->add($id);
    }

    public function getId(): TemplateId
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection|TemplateId[]
     */
    public function getSelected()
    {
        return $this->selected;
    }

    public function isRelated(TemplateId $templateId): bool
    {
        return $this->selected->contains($templateId);
    }
}
