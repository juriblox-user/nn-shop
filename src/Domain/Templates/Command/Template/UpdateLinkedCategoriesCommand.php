<?php

namespace NnShop\Domain\Templates\Command\Template;

use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Domain\Templates\Value\CategoryId;
use NnShop\Domain\Templates\Value\TemplateId;

class UpdateLinkedCategoriesCommand
{
    /**
     * @var TemplateId
     */
    private $id;

    /**
     * @var CategoryId
     */
    private $primary;

    /**
     * @var ArrayCollection|CategoryId[]
     */
    private $selected;

    /**
     * UpdateLinkedCategoriesCommand constructor.
     *
     * @param TemplateId $id
     */
    private function __construct(TemplateId $id)
    {
        $this->id = $id;
        $this->selected = new ArrayCollection();
    }

    /**
     * @param CategoryId $id
     */
    public function addCategory(CategoryId $id)
    {
        $this->selected->add($id);
    }

    /**
     * @return TemplateId
     */
    public function getId(): TemplateId
    {
        return $this->id;
    }

    /**
     * @return CategoryId|null
     */
    public function getPrimary()
    {
        return $this->primary;
    }

    /**
     * @return ArrayCollection|CategoryId[]
     */
    public function getSelected()
    {
        return $this->selected;
    }

    /**
     * @param CategoryId $id
     *
     * @return bool
     */
    public function hasCategory(CategoryId $id): bool
    {
        return $this->selected->contains($id);
    }

    /**
     * @param TemplateId $id
     *
     * @return UpdateLinkedCategoriesCommand
     */
    public static function prepare(TemplateId $id): self
    {
        return new self($id);
    }

    /**
     * @param CategoryId $primary
     */
    public function setPrimary(CategoryId $primary)
    {
        $this->primary = $primary;
    }
}
