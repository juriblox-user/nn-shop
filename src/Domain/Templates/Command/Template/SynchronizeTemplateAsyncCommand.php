<?php

namespace NnShop\Domain\Templates\Command\Template;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Templates\Value\TemplateId;

class SynchronizeTemplateAsyncCommand
{
    /**
     * @Serializer\Type("NnShop\Domain\Templates\Command\Template\SynchronizeTemplateCommand")
     *
     * @var SynchronizeTemplateCommand
     */
    private $command;

    /**
     * SynchronizePartnerAsyncCommand constructor.
     */
    private function __construct()
    {
    }

    /**
     * @param TemplateId $templateId
     *
     * @return SynchronizeTemplateAsyncCommand
     */
    public static function create(TemplateId $templateId): self
    {
        return static::from(SynchronizeTemplateCommand::fromLocalId($templateId));
    }

    /**
     * @param SynchronizeTemplateCommand $synchronousCommand
     *
     * @return SynchronizeTemplateAsyncCommand
     */
    public static function from(SynchronizeTemplateCommand $synchronousCommand): self
    {
        $command = new self();
        $command->command = $synchronousCommand;

        return $command;
    }

    /**
     * @return SynchronizeTemplateCommand
     */
    public function getCommand(): SynchronizeTemplateCommand
    {
        return $this->command;
    }
}
