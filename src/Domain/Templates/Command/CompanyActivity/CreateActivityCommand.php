<?php

namespace NnShop\Domain\Templates\Command\CompanyActivity;

use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Domain\Templates\Value\CompanyActivityId;
use NnShop\Domain\Templates\Value\CompanyTypeId;
use NnShop\Domain\Templates\Value\TemplateId;

class CreateActivityCommand
{
    /**
     * @var CompanyActivityId
     */
    private $id;

    /**
     * @var ArrayCollection|TemplateId[]
     */
    private $templates;

    /**
     * @var string
     */
    private $title;

    /**
     * @var CompanyTypeId
     */
    private $typeId;

    /**
     * CreateActivityCommand constructor.
     *
     * @param CompanyActivityId $id
     * @param string            $title
     * @param CompanyTypeId     $typeId
     */
    private function __construct(CompanyActivityId $id, $title, CompanyTypeId $typeId)
    {
        $this->id = $id;

        $this->title = $title;
        $this->typeId = $typeId;

        $this->templates = new ArrayCollection();
    }

    /**
     * @param TemplateId $id
     */
    public function addTemplate(TemplateId $id)
    {
        $this->templates->add($id);
    }

    /**
     * @return CompanyActivityId
     */
    public function getId(): CompanyActivityId
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection|TemplateId[]
     */
    public function getTemplates()
    {
        return $this->templates;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return CompanyTypeId
     */
    public function getTypeId(): CompanyTypeId
    {
        return $this->typeId;
    }

    /**
     * @param CompanyActivityId $id
     * @param                   $title
     * @param CompanyTypeId     $typeId
     *
     * @return CreateActivityCommand
     */
    public static function prepare(CompanyActivityId $id, $title, CompanyTypeId $typeId): self
    {
        return new self($id, $title, $typeId);
    }
}
