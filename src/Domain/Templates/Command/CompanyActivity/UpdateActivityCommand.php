<?php

namespace NnShop\Domain\Templates\Command\CompanyActivity;

use NnShop\Domain\Templates\Value\CompanyActivityId;
use NnShop\Domain\Templates\Value\TemplateId;

class UpdateActivityCommand
{
    /**
     * @var CompanyActivityId
     */
    private $id;

    /**
     * @var array|TemplateId[]
     */
    private $templates;

    /**
     * @var string
     */
    private $title;

    /**
     * UpdateActivityCommand constructor.
     *
     * @param CompanyActivityId $id
     */
    private function __construct(CompanyActivityId $id)
    {
        $this->id = $id;

        $this->templates = [];
    }

    /**
     * @param TemplateId $templateId
     */
    public function addTemplate(TemplateId $templateId)
    {
        $this->templates[] = $templateId;
    }

    /**
     * @return CompanyActivityId
     */
    public function getId(): CompanyActivityId
    {
        return $this->id;
    }

    /**
     * @return array|TemplateId[]
     */
    public function getTemplates()
    {
        return $this->templates;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param CompanyActivityId $id
     *
     * @return UpdateActivityCommand
     */
    public static function prepare(CompanyActivityId $id): self
    {
        return new self($id);
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }
}
