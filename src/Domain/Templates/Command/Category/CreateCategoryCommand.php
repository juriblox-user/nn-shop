<?php

namespace NnShop\Domain\Templates\Command\Category;

use NnShop\Domain\Shops\Entity\Shop;
use NnShop\Domain\Templates\Value\CategoryId;

class CreateCategoryCommand
{
    /**
     * @var CategoryId
     */
    private $id;

    /**
     * @var Shop
     */
    private $shop;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string | null
     */
    private $description;

    /**
     * @var string | null
     */
    private $metaDescription;

    /**
     * CreateCategoryCommand constructor.
     *
     * @param CategoryId $id
     * @param Shop       $shop
     */
    private function __construct(CategoryId $id, Shop $shop)
    {
        $this->id = $id;
        $this->shop = $shop;
    }

    /**
     * @param CategoryId $id
     * @param string $title
     * @param Shop $shop
     * @param string|null $description
     * @param string|null $metaDecription
     * @return CreateCategoryCommand
     */
    public static function create(CategoryId $id, string $title, Shop $shop, ?string $description, ?string $metaDecription): CreateCategoryCommand
    {
        $category = new self($id, $shop);
        $category->setTitle($title);
        $category->setDescription($description);
        $category->setMetaDescription($metaDecription);

        return $category;
    }

    /**
     * @return CategoryId
     */
    public function getId(): CategoryId
    {
        return $this->id;
    }

    /**
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * @param string|null $metaDescription
     */
    public function setMetaDescription(?string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }
}
