<?php

namespace NnShop\Domain\Templates\Command\Category;

use NnShop\Domain\Shops\Value\ShopId;
use NnShop\Domain\Templates\Value\CategoryId;

class UpdateCategoryCommand
{
    /**
     * @var CategoryId
     */
    private $id;

    /**
     * @var ShopId
     */
    private $shop;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string | null
     */
    private $description;

    /**
     * @var string | null
     */
    private $metaDescription;

    /**
     * UpdateCategoryCommand constructor.
     *
     * @param CategoryId $id
     */
    private function __construct(CategoryId $id)
    {
        $this->id = $id;
    }

    /**
     * @return CategoryId
     */
    public function getId(): CategoryId
    {
        return $this->id;
    }

    /**
     * @return ShopId
     */
    public function getShop(): ShopId
    {
        return $this->shop;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param CategoryId $id
     *
     * @return UpdateCategoryCommand
     */
    public static function prepare(CategoryId $id): self
    {
        return new self($id);
    }

    /**
     * @param ShopId $shop
     */
    public function move(ShopId $shop)
    {
        $this->shop = $shop;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * @param string|null $metaDescription
     */
    public function setMetaDescription(?string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }
}
