<?php

namespace NnShop\Domain\Templates\Command\Category;

use NnShop\Domain\Templates\Value\CategoryId;
use NnShop\Domain\Templates\Value\TemplateId;

class SetPrimaryForTemplateCommand
{
    /**
     * @var CategoryId
     */
    private $categoryId;

    /**
     * @var TemplateId
     */
    private $templateId;

    /**
     * SetPrimaryCategoryForTemplate constructor.
     *
     * @param CategoryId $categoryId
     * @param TemplateId $templateId
     */
    private function __construct(CategoryId $categoryId, TemplateId $templateId)
    {
        $this->categoryId = $categoryId;
        $this->templateId = $templateId;
    }

    /**
     * @param CategoryId $categoryId
     * @param TemplateId $templateId
     *
     * @return SetPrimaryForTemplateCommand
     */
    public static function create(CategoryId $categoryId, TemplateId $templateId): self
    {
        return new self($categoryId, $templateId);
    }

    /**
     * @return CategoryId
     */
    public function getCategoryId(): CategoryId
    {
        return $this->categoryId;
    }

    /**
     * @return TemplateId
     */
    public function getTemplateId(): TemplateId
    {
        return $this->templateId;
    }
}
