<?php

namespace NnShop\Domain\Templates\Command\Answer;

use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Domain\Templates\Value\DocumentId;
use NnShop\Domain\Templates\Value\QuestionId;
use NnShop\Domain\Templates\Value\QuestionOptionId;

class AnswerQuestionCommand
{
    /**
     * @var DocumentId
     */
    private $documentId;

    /**
     * @var ArrayCollection|QuestionOptionId[]
     */
    private $optionIds;

    /**
     * @var QuestionId
     */
    private $questionId;

    /**
     * @var mixed
     */
    private $value;

    /**
     * GiveAnswerToQuestionCommand constructor.
     *
     * @param DocumentId $documentId
     * @param QuestionId $questionId
     */
    private function __construct(DocumentId $documentId, QuestionId $questionId)
    {
        $this->documentId = $documentId;
        $this->questionId = $questionId;

        $this->optionIds = new ArrayCollection();
    }

    /**
     * @param QuestionOptionId $optionId
     */
    public function addOptionId(QuestionOptionId $optionId)
    {
        if (null !== $this->value) {
            throw new \LogicException('Cannot answer a question in the form of a value and QuestionOptionIds');
        }

        if ($this->optionIds->contains($optionId)) {
            throw new \InvalidArgumentException('This QuestionOptionId has already been linked to this answer');
        }

        $this->optionIds->add($optionId);
    }

    /**
     * @param DocumentId $documentId
     * @param QuestionId $questionId
     * @param            $value
     *
     * @return AnswerQuestionCommand
     */
    public static function create(DocumentId $documentId, QuestionId $questionId, $value): self
    {
        $command = self::prepare($documentId, $questionId);
        $command->value = $value;

        return $command;
    }

    /**
     * @return DocumentId
     */
    public function getDocumentId(): DocumentId
    {
        return $this->documentId;
    }

    /**
     * @return ArrayCollection|QuestionOptionId[]
     */
    public function getOptionIds()
    {
        return $this->optionIds;
    }

    /**
     * @return QuestionId
     */
    public function getQuestionId(): QuestionId
    {
        return $this->questionId;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param DocumentId $documentId
     * @param QuestionId $questionId
     *
     * @return AnswerQuestionCommand
     */
    public static function prepare(DocumentId $documentId, QuestionId $questionId): self
    {
        return new self($documentId, $questionId);
    }
}
