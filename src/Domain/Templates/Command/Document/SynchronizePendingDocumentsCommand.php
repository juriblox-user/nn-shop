<?php

namespace NnShop\Domain\Templates\Command\Document;

class SynchronizePendingDocumentsCommand
{
    /**
     * SynchronizeDocumentsCommand constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return SynchronizePendingDocumentsCommand
     */
    public static function create(): self
    {
        return new self();
    }
}
