<?php

namespace NnShop\Domain\Templates\Command\Document;

use NnShop\Domain\Templates\Value\DocumentId;

class IncreaseDownloadCounterCommand
{
    /**
     * @var DocumentId
     */
    private $id;

    /**
     * IncreaseDownloadCounterCommand constructor.
     *
     * @param DocumentId $id
     */
    private function __construct(DocumentId $id)
    {
        $this->id = $id;
    }

    /**
     * @param DocumentId $id
     *
     * @return IncreaseDownloadCounterCommand
     */
    public static function create(DocumentId $id): self
    {
        return new self($id);
    }

    /**
     * @return DocumentId
     */
    public function getId(): DocumentId
    {
        return $this->id;
    }
}
