<?php

namespace NnShop\Domain\Templates\Command\Document;

use JMS\Serializer\Annotation as Serializer;

class RequestDocumentAsyncCommand
{
    /**
     * @Serializer\Type("NnShop\Domain\Templates\Command\Document\RequestDocumentCommand")
     *
     * @var RequestDocumentCommand
     */
    private $command;

    /**
     * RequestDocumentAsyncCommand constructor.
     */
    private function __construct()
    {
    }

    /**
     * @param RequestDocumentCommand $synchronousCommand
     *
     * @return RequestDocumentAsyncCommand
     */
    public static function from(RequestDocumentCommand $synchronousCommand): self
    {
        $command = new self();
        $command->command = $synchronousCommand;

        return $command;
    }

    /**
     * @return RequestDocumentCommand
     */
    public function getCommand(): RequestDocumentCommand
    {
        return $this->command;
    }
}
