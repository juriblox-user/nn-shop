<?php

namespace NnShop\Domain\Templates\Command\Document;

use NnShop\Domain\Templates\Value\DocumentId;

class CacheDocumentCommand
{
    /**
     * @var DocumentId
     */
    private $id;

    /**
     * DownloadDocumentCommand constructor.
     *
     * @param DocumentId $id
     */
    private function __construct(DocumentId $id)
    {
        $this->id = $id;
    }

    /**
     * @param DocumentId $id
     *
     * @return CacheDocumentCommand
     */
    public static function create(DocumentId $id): self
    {
        return new self($id);
    }

    /**
     * @return DocumentId
     */
    public function getId(): DocumentId
    {
        return $this->id;
    }
}
