<?php

namespace NnShop\Domain\Templates\Command\Document;

use NnShop\Domain\Templates\Value\DocumentId;

class RequestDocumentCommand
{
    /**
     * @var DocumentId
     */
    private $id;

    /**
     * RequestDocumentCommand constructor.
     *
     * @param DocumentId $id
     */
    private function __construct(DocumentId $id)
    {
        $this->id = $id;
    }

    /**
     * @param DocumentId $id
     *
     * @return RequestDocumentCommand
     */
    public static function create(DocumentId $id): self
    {
        return new self($id);
    }

    /**
     * @return DocumentId
     */
    public function getId(): DocumentId
    {
        return $this->id;
    }
}
