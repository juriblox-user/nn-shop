<?php

namespace NnShop\Domain\Templates\Command\Document;

use JuriBlox\Sdk\Domain\Documents\Values\DocumentId as RemoteId;
use NnShop\Domain\Templates\Value\DocumentId;

class SynchronizeDocumentCommand
{
    /**
     * @var DocumentId
     */
    private $localId;

    /**
     * @var RemoteId
     */
    private $remoteId;

    /**
     * SynchronizeDocumentCommand constructor.
     */
    private function __construct()
    {
    }

    /**
     * Command aanmaken op basis van een lokaal DocumentId.
     *
     * @param DocumentId $localId
     *
     * @return SynchronizeDocumentCommand
     */
    public static function fromLocal(DocumentId $localId): self
    {
        $command = new self();
        $command->localId = $localId;

        return $command;
    }

    /**
     * Command aanmaken op basis van een remote DocumentId.
     *
     * @param RemoteId $remoteId
     *
     * @return SynchronizeDocumentCommand
     */
    public static function fromRemote(RemoteId $remoteId): self
    {
        $command = new self();
        $command->remoteId = $remoteId;

        return $command;
    }

    /**
     * @return DocumentId
     */
    public function getLocalId(): DocumentId
    {
        return $this->localId;
    }

    /**
     * @return RemoteId
     */
    public function getRemoteId(): RemoteId
    {
        return $this->remoteId;
    }

    /**
     * Command op basis van een lokaal DocumentId.
     *
     * @return bool
     */
    public function isLocal(): bool
    {
        return null !== $this->localId;
    }

    /**
     * Command op basis van een remote DocumentId.
     *
     * @return bool
     */
    public function isRemote(): bool
    {
        return null !== $this->remoteId;
    }
}
