<?php

namespace NnShop\Domain\Templates\Command\CompanyType;

use NnShop\Domain\Templates\Value\CompanyTypeId;

class UpdateTypeCommand
{
    /**
     * @var string|null
     */
    private $description;

    /**
     * @var CompanyTypeId
     */
    private $id;

    /**
     * @var string|null
     */
    private $title;

    /**
     * UpdateTypeCommand constructor.
     *
     * @param CompanyTypeId $id
     */
    private function __construct(CompanyTypeId $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return CompanyTypeId
     */
    public function getId(): CompanyTypeId
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param CompanyTypeId $id
     *
     * @return UpdateTypeCommand
     */
    public static function prepare(CompanyTypeId $id): self
    {
        return new self($id);
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description ?: null;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }
}
