<?php

namespace NnShop\Domain\Templates\Command\CompanyType;

use NnShop\Domain\Templates\Value\CompanyTypeId;
use NnShop\Domain\Translation\Value\ProfileId;

class CreateTypeCommand
{
    /**
     * @var string|null
     */
    private $description;

    /**
     * @var CompanyTypeId
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var ProfileId
     */
    private $profileId;

    /**
     * CreateTypeCommand constructor.
     *
     * @param CompanyTypeId $id
     * @param ProfileId     $profileId
     */
    private function __construct(CompanyTypeId $id, ProfileId $profileId)
    {
        $this->id = $id;
        $this->profileId = $profileId;
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return CompanyTypeId
     */
    public function getId(): CompanyTypeId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param CompanyTypeId $id
     * @param string        $title
     * @param ProfileId     $profileId
     *
     * @return CreateTypeCommand
     */
    public static function prepare(CompanyTypeId $id, string $title, ProfileId $profileId): self
    {
        $command = new self($id, $profileId);
        $command->title = $title;

        return $command;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description ?: null;
    }

    /**
     * @return ProfileId
     */
    public function getProfileId(): ProfileId
    {
        return $this->profileId;
    }
}
