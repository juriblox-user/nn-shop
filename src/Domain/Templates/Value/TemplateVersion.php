<?php

namespace NnShop\Domain\Templates\Value;

use Core\Domain\Value\AbstractIntegerValue;

class TemplateVersion extends AbstractIntegerValue
{
    public function compare(self $other): int
    {
        if ($this->getInteger() > $other->getInteger()) {
            return 1;
        }

        if ($this->getInteger() < $other->getInteger()) {
            return -1;
        }

        return 0;
    }
}
