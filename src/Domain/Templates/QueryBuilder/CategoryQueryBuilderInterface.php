<?php

namespace NnShop\Domain\Templates\QueryBuilder;

use Core\Doctrine\ORM\QueryBuilderInterface;
use NnShop\Domain\Shops\Value\ShopId;

interface CategoryQueryBuilderInterface extends QueryBuilderInterface
{
    /**
     * @param ShopId $shopId
     *
     * @return CategoryQueryBuilderInterface
     */
    public function filterShop(ShopId $shopId): self;
}
