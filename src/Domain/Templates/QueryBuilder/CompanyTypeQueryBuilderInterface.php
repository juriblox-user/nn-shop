<?php

namespace NnShop\Domain\Templates\QueryBuilder;

use Core\Doctrine\ORM\QueryBuilderInterface;
use NnShop\Domain\Translation\Value\ProfileId;

interface CompanyTypeQueryBuilderInterface extends QueryBuilderInterface
{
    /**
     * @param ProfileId $profileId
     *
     * @return CompanyTypeQueryBuilderInterface
     */
    public function filterProfile(ProfileId $profileId): self;
}
