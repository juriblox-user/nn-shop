<?php

namespace NnShop\Domain\Templates\QueryBuilder;

use Core\Doctrine\ORM\QueryBuilderInterface;
use NnShop\Domain\Translation\Entity\Profile;

interface PartnerQueryBuilderInterface extends QueryBuilderInterface
{
    /**
     * @param Profile $profile
     *
     * @return PartnerQueryBuilderInterface
     */
    public function filterProfile(Profile $profile): self;

    /**
     * @return PartnerQueryBuilderInterface
     */
    public function excludeWithoutLogo(): self;

    /**
     * @param string $slug
     *
     * @return PartnerQueryBuilderInterface
     */
    public function excludeWithSlug(string $slug): self;
}
