<?php

namespace NnShop\Domain\Templates\QueryBuilder;

use Core\Doctrine\ORM\QueryBuilderInterface;
use NnShop\Domain\Shops\Value\ShopId;
use NnShop\Domain\Templates\Entity\Category;
use NnShop\Domain\Templates\Value\PartnerId;
use NnShop\Domain\Translation\Value\ProfileId;

interface TemplateQueryBuilderInterface extends QueryBuilderInterface
{
    /**
     * @return $this
     */
    public function excludeCorrupted();

    /**
     * @return $this
     */
    public function excludeUnpublished();

    /**
     * @return $this
     */
    public function filterCorrupted();

    /**
     * @return $this
     */
    public function filterPublished();

    /**
     * @param ShopId $shopId
     */
    public function filterShop(ShopId $shopId);

    /**
     * @return $this
     */
    public function includeCorrupted();

    /**
     * @return $this
     */
    public function includeUnpublished();

    /**
     * @param string $keywords
     */
    public function matchKeywords(string $keywords);

    /**
     * @param PartnerId $partnerId
     *
     * @return TemplateQueryBuilderInterface
     */
    public function filterPartner(PartnerId $partnerId): self;

    /**
     * @param ProfileId $profileId
     *
     * @return TemplateQueryBuilderInterface
     */
    public function filterProfile(ProfileId $profileId): self;

    /**
     * @return $this
     */
    public function filterHidden();

    /**
     * @param Category $category
     * @return TemplateQueryBuilderInterface
     */
    public function filterCategory(Category $category);
}
