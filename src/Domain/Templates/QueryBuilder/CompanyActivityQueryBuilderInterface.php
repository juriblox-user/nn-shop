<?php

namespace NnShop\Domain\Templates\QueryBuilder;

use Core\Doctrine\ORM\QueryBuilderInterface;
use NnShop\Domain\Templates\Entity\CompanyType;

interface CompanyActivityQueryBuilderInterface extends QueryBuilderInterface
{
    /**
     * Filteren op CompanyType.
     *
     * @return CompanyActivityQueryBuilderInterface
     */
    public function filterType(): self;

    /**
     * CompanyType toevoegen.
     *
     * @param CompanyType $type
     *
     * @return CompanyActivityQueryBuilderInterface
     */
    public function includeType(CompanyType $type): self;
}
