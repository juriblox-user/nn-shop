<?php

namespace NnShop\Domain\JuriBlox\Event\Template;

use JMS\Serializer\Annotation as Serializer;
use JuriBlox\Sdk\Domain\Documents\Values\TemplateId as RemoteTemplateId;
use JuriBlox\Sdk\Domain\Offices\Values\OfficeId as RemoteOfficeId;

class TemplatePublishedEvent
{
    /**
     * @Serializer\Type("JuriBlox\Sdk\Domain\Offices\Values\OfficeId")
     *
     * @var RemoteOfficeId
     */
    private $offceId;

    /**
     * @Serializer\Type("JuriBlox\Sdk\Domain\Documents\Values\TemplateId")
     *
     * @var RemoteTemplateId
     */
    private $templateId;

    /**
     * TemplateWasPublished constructor.
     *
     * @param RemoteTemplateId $templateId
     * @param RemoteOfficeId   $officeId
     */
    private function __construct(RemoteTemplateId $templateId, RemoteOfficeId $officeId)
    {
        $this->templateId = $templateId;
        $this->offceId = $officeId;
    }

    /**
     * @param RemoteTemplateId $templateId
     * @param RemoteOfficeId   $officeId
     *
     * @return TemplatePublishedEvent
     */
    public static function create(RemoteTemplateId $templateId, RemoteOfficeId $officeId): self
    {
        return new self($templateId, $officeId);
    }

    /**
     * @return RemoteOfficeId
     */
    public function getOffceId(): RemoteOfficeId
    {
        return $this->offceId;
    }

    /**
     * @return RemoteTemplateId
     */
    public function getTemplateId(): RemoteTemplateId
    {
        return $this->templateId;
    }
}
