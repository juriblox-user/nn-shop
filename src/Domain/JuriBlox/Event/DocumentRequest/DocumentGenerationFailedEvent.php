<?php

namespace NnShop\Domain\JuriBlox\Event\DocumentRequest;

use JMS\Serializer\Annotation as Serializer;
use JuriBlox\Sdk\Domain\Documents\Values\DocumentRequestId as RemoteId;
use NnShop\Domain\Templates\Value\DocumentRequestId;

class DocumentGenerationFailedEvent
{
    /**
     * @Serializer\Type("NnShop\Domain\Templates\Value\DocumentRequestId")
     *
     * @var DocumentRequestId
     */
    private $localId;

    /**
     * @Serializer\Type("JuriBlox\Sdk\Domain\Documents\Values\DocumentRequestId")
     *
     * @var RemoteId
     */
    private $remoteId;

    /**
     * DocumentGenerationFailed constructor.
     */
    private function __construct()
    {
    }

    /**
     * Event aanmaken op basis van een lokaal DocumentRequestId.
     *
     * @param DocumentRequestId $requestId
     *
     * @return DocumentGenerationFailedEvent
     */
    public static function fromLocal(DocumentRequestId $requestId): self
    {
        $event = new self();
        $event->localId = $requestId;

        return $event;
    }

    /**
     * Event aanmaken op basis van een extern DocumentRequestId.
     *
     * @param RemoteId $requestId
     *
     * @return DocumentGenerationFailedEvent
     */
    public static function fromRemote(RemoteId $requestId): self
    {
        $event = new self();
        $event->remoteId = $requestId;

        return $event;
    }

    /**
     * @return DocumentRequestId
     */
    public function getLocalId(): DocumentRequestId
    {
        return $this->localId;
    }

    /**
     * @return RemoteId
     */
    public function getRemoteId(): RemoteId
    {
        return $this->remoteId;
    }

    /**
     * Event op basis van een lokaal DocumentRequestId.
     *
     * @return bool
     */
    public function isLocal(): bool
    {
        return null !== $this->localId;
    }

    /**
     * Event op basis van een remote DocumentRequestId.
     *
     * @return bool
     */
    public function isRemote(): bool
    {
        return null !== $this->remoteId;
    }
}
