<?php

namespace NnShop\Domain\JuriBlox\Event\DocumentRequest;

use JMS\Serializer\Annotation as Serializer;
use JuriBlox\Sdk\Domain\Documents\Values\DocumentId as RemoteDocumentId;
use JuriBlox\Sdk\Domain\Documents\Values\DocumentRequestId as RemoteRequestId;

class DocumentGenerationSucceededEvent
{
    /**
     * @Serializer\Type("JuriBlox\Sdk\Domain\Documents\Values\DocumentRequestId")
     *
     * @var RemoteRequestId
     */
    private $requestId;

    /**
     * @Serializer\Type("JuriBlox\Sdk\Domain\Documents\Values\DocumentId")
     *
     * @var RemoteDocumentId
     */
    private $documentId;

    /**
     * DocumentGenerationSucceeded constructor.
     *
     * @param RemoteRequestId  $requestId
     * @param RemoteDocumentId $documentId
     */
    private function __construct(RemoteRequestId $requestId, RemoteDocumentId $documentId)
    {
        $this->requestId = $requestId;
        $this->documentId = $documentId;
    }

    /**
     * @param RemoteRequestId  $requestId
     * @param RemoteDocumentId $documentId
     *
     * @return DocumentGenerationSucceededEvent
     */
    public static function create(RemoteRequestId $requestId, RemoteDocumentId $documentId): self
    {
        return new self($requestId, $documentId);
    }

    /**
     * @return RemoteRequestId
     */
    public function getRequestId(): RemoteRequestId
    {
        return $this->requestId;
    }

    /**
     * @return RemoteDocumentId
     */
    public function getDocumentId(): RemoteDocumentId
    {
        return $this->documentId;
    }
}
