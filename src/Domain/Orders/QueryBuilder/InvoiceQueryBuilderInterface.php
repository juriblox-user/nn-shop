<?php

namespace NnShop\Domain\Orders\QueryBuilder;

use Core\Doctrine\ORM\QueryBuilderInterface;
use NnShop\Domain\Translation\Value\ProfileId;

interface InvoiceQueryBuilderInterface extends QueryBuilderInterface
{
    /**
     * @return $this
     */
    public function filterStatus();

    /**
     * @return $this
     */
    public function filterOverdue();

    /**
     * @return $this
     */
    public function includePaid();

    /**
     * @return $this
     */
    public function includePending();

    /**
     * @param ProfileId $profileId
     *
     * @return InvoiceQueryBuilderInterface
     */
    public function filterProfile(ProfileId $profileId): self;

    /**
     * @param string $id
     *
     * @return InvoiceQueryBuilderInterface
     */
    public function filterInvoiceNumber(string $id): self;
}
