<?php

namespace NnShop\Domain\Orders\QueryBuilder;

use Core\Doctrine\ORM\QueryBuilderInterface;
use NnShop\Domain\Orders\Enumeration\OrderItemType;
use NnShop\Domain\Orders\Value\DiscountId;
use NnShop\Domain\Translation\Value\ProfileId;

interface InvoiceItemQueryBuilderInterface extends QueryBuilderInterface
{
    /**
     * @param OrderItemType $itemType
     *
     * @return $this
     */
    public function filterType(OrderItemType $itemType): self;

    /**
     * @param ProfileId $profileId
     *
     * @return InvoiceItemQueryBuilderInterface
     */
    public function filterProfile(ProfileId $profileId): self;

    /**
     * @param DiscountId $discountId
     *
     * @return InvoiceItemQueryBuilderInterface
     */
    public function filterDiscount(DiscountId $discountId): self;
}
