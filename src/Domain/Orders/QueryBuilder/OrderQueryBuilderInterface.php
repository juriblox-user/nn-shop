<?php

namespace NnShop\Domain\Orders\QueryBuilder;

use Core\Doctrine\ORM\QueryBuilderInterface;
use NnShop\Domain\Translation\Value\ProfileId;

interface OrderQueryBuilderInterface extends QueryBuilderInterface
{
    /**
     * @return $this
     */
    public function filterStatus();

    /**
     * @return $this
     */
    public function includeCompleted();

    /**
     * @return $this
     */
    public function includeManual();

    /**
     * @return $this
     */
    public function includePending();

    /**
     * @return $this
     */
    public function includeWarning();

    /**
     * @param ProfileId $profileId
     *
     * @return OrderQueryBuilderInterface
     */
    public function filterProfile(ProfileId $profileId): self;

    /**
     * @param string $id
     *
     * @return OrderQueryBuilderInterface
     */
    public function filterOrderNumber(string $id): self;

    /**
     * @param string $templateName
     *
     * @return OrderQueryBuilderInterface
     */
    public function filterTemplate(string $templateName): self;

    /**
     * @param \DateTime $from
     *
     * @return OrderQueryBuilderInterface
     */
    public function filterFromDate(\DateTime $from): self;

    /**
     * @param \DateTime $to
     *
     * @return OrderQueryBuilderInterface
     */
    public function filterToDate(\DateTime $to): self;
}
