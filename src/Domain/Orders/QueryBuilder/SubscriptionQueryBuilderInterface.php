<?php

namespace NnShop\Domain\Orders\QueryBuilder;

use Core\Doctrine\ORM\QueryBuilderInterface;
use NnShop\Domain\Translation\Value\ProfileId;

interface SubscriptionQueryBuilderInterface extends QueryBuilderInterface
{
    /**
     * @return $this
     */
    public function filterExpired();

    /**
     * @return $this
     */
    public function filterGracePeriod();

    /**
     * @return $this
     */
    public function includeExpired();

    /**
     * @param ProfileId $profileId
     *
     * @return SubscriptionQueryBuilderInterface
     */
    public function filterProfile(ProfileId $profileId): self;
}
