<?php

namespace NnShop\Domain\Orders\QueryBuilder;

use Core\Doctrine\ORM\QueryBuilderInterface;

interface CustomerQueryBuilderInterface extends QueryBuilderInterface
{
    /**
     * @return $this
     */
    public function orderByWorth();

    /**
     * @param string $name
     *
     * @return self
     */
    public function filterName(string $name): self;

    /**
     * @param string $companyName
     *
     * @return self
     */
    public function filterCompanyName(string $companyName): self;

    /**
     * @param string $email
     *
     * @return self
     */
    public function filterEmail(string $email): self;
}
