<?php

namespace NnShop\Domain\Orders\QueryBuilder;

use Core\Doctrine\ORM\QueryBuilderInterface;

interface DiscountQueryBuilderInterface extends QueryBuilderInterface
{
}
