<?php

namespace NnShop\Domain\Orders\Services\Validator;

use Doctrine\Common\Collections\Collection;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Entity\Discount;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Enumeration\DiscountType;
use NnShop\Domain\Orders\Repository\DiscountRepositoryInterface;
use NnShop\Domain\Templates\Entity\Template;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DiscountValidator
{
    /**
     * @var DiscountRepositoryInterface
     */
    private $repository;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @param DiscountRepositoryInterface $repository
     * @param ValidatorInterface          $validator
     */
    public function __construct(DiscountRepositoryInterface $repository, ValidatorInterface $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * @param string        $code
     * @param Discount|null $existing
     *
     * @return bool
     */
    public function isUnique(string $code, Discount $existing = null): bool
    {
        $conflicting = $this->repository->findOneByCode($code);
        if (null === $conflicting) {
            return true;
        }

        if (null === $existing) {
            return false;
        }

        return 0 === $conflicting->compareTo($existing);
    }

    /**
     * Controleren of een kortingscode op dit moment geldig is.
     *
     * @param string $discountCode
     *
     * @return bool
     */
    public function isValid(string $discountCode): bool
    {
        $discount = $this->repository->findOneByCode($discountCode);

        return null !== $discount && $discount->isActive() && $discount->canUse();
    }

    /**
     * Controleren of de kortingscode geldig is voor deze bestelling.
     *
     * @param string $discountCode
     * @param Order  $order
     *
     * @return bool
     */
    public function isValidForOrder(string $discountCode, Order $order): bool
    {
        $discount = $this->repository->getByCode($discountCode);

        return !$order->hasDiscount($discount)
            && $this->isRelatedToTemplate($discount, $order->getTemplate())
            && $this->isRelatedToEmail($discount, $order->getCustomer());
    }

    /**
     * Controleren of de kortingscode niet conflicteerd met een bestaande kortingscode op deze bestelling.
     *
     * @param string $discountCode
     * @param Order  $order
     *
     * @return bool
     */
    public function isConflicting(string $discountCode, Order $order): bool
    {
        $discount = $this->repository->getByCode($discountCode);

        // Als er geen andere kortingscodes zijn kan er niks conflicterends zijn
        if ($order->getDiscounts()->isEmpty()) {
            return false;
        }

        // Er kunnen alleen conflicten zijn bij procentuele kortingen
        if ($discount->getType()->isNot(DiscountType::PERCENTAGE)) {
            return false;
        }

        return $order->getDiscounts()->filter(function (Discount $existing) {
            return $existing->getType()->is(DiscountType::PERCENTAGE);
        })->count() > 0;
    }

    /**
     * Controleren of de kortingscode geldig is voor deze klant.
     *
     * @param string   $discountCode
     * @param Customer $customer
     *
     * @return bool
     */
    public function isValidForCustomer(string $discountCode, Customer $customer): bool
    {
        $discount = $this->repository->getByCode($discountCode);

        return true;
    }

    /**
     * @param Collection $emails
     *
     * @return bool
     */
    public function isValidEmails(Collection $emails)
    {
        $constraint = new Assert\Email();

        foreach ($emails as $email) {
            if ($this->validator->validate($email, $constraint)->count() > 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param Discount $discount
     * @param Template $template
     *
     * @return bool
     */
    private function isRelatedToTemplate(Discount $discount, Template $template)
    {
        return 0 === $discount->getTemplates()->count()
            || $discount->hasTemplate($template);
    }

    /**
     * @param Discount $discount
     * @param Customer $customer
     *
     * @return bool
     */
    private function isRelatedToEmail(Discount $discount, Customer $customer)
    {
        return 0 === $discount->getEmails()->count()
            || $discount->hasEmail($customer->getEmail());
    }
}
