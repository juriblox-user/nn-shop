<?php

namespace NnShop\Domain\Orders\Services\Validator;

use NnShop\Domain\Orders\Repository\InvoiceRepositoryInterface;
use NnShop\Domain\Orders\Value\InvoiceCode;

class InvoiceCodeValidator
{
    /**
     * @var InvoiceRepositoryInterface
     */
    private $repository;

    /**
     * InvoiceCodeValidator constructor.
     *
     * @param InvoiceRepositoryInterface $repository
     */
    public function __construct(InvoiceRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param InvoiceCode $code
     *
     * @return bool
     */
    public function isUnique(InvoiceCode $code): bool
    {
        return !$this->repository->hasWithCode($code);
    }
}
