<?php

namespace NnShop\Domain\Orders\Services\Validator;

use Core\Domain\Value\Web\EmailAddress;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;

class CustomerValidator
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * Constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param EmailAddress  $email
     * @param Customer|null $exclude
     *
     * @return bool
     */
    public function isUniqueEmail(EmailAddress $email, Customer $exclude = null): bool
    {
        $duplicate = $this->customerRepository->findOneByEmail($email);
        if (null === $duplicate) {
            return true;
        }

        return null !== $exclude && $duplicate->isEqualTo($exclude);
    }
}
