<?php

namespace NnShop\Domain\Orders\Services\Validator;

use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Orders\Value\OrderCode;

class OrderCodeValidator
{
    /**
     * @var OrderRepositoryInterface
     */
    private $repository;

    /**
     * OrderNumberValidator constructor.
     *
     * @param OrderRepositoryInterface $repository
     */
    public function __construct(OrderRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param OrderCode $code
     *
     * @return bool
     */
    public function isUnique(OrderCode $code): bool
    {
        return !$this->repository->hasWithCode($code);
    }
}
