<?php

namespace NnShop\Domain\Orders\Services\Validator;

use NnShop\Domain\Orders\Repository\SubscriptionRepositoryInterface;
use NnShop\Domain\Orders\Value\SubscriptionCode;

class SubscriptionCodeValidator
{
    /**
     * @var SubscriptionRepositoryInterface
     */
    private $repository;

    /**
     * SubscriptionCodeValidator constructor.
     *
     * @param SubscriptionRepositoryInterface $repository
     */
    public function __construct(SubscriptionRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param SubscriptionCode $code
     *
     * @return bool
     */
    public function isUnique(SubscriptionCode $code): bool
    {
        return !$this->repository->hasWithCode($code);
    }
}
