<?php

namespace NnShop\Domain\Orders\Services\Validator;

use NnShop\Domain\Orders\Repository\SubscriptionLinkRepository;
use NnShop\Domain\Orders\Value\SubscriptionLinkToken;

class SubscriptionLinkTokenValidator
{
    /**
     * @var SubscriptionLinkRepository
     */
    private $repository;

    /**
     * SubscriptionLinkTokenValidator constructor.
     *
     * @param SubscriptionLinkRepository $repository
     */
    public function __construct(SubscriptionLinkRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param SubscriptionLinkToken $token
     *
     * @return bool
     */
    public function isUnique(SubscriptionLinkToken $token): bool
    {
        return !$this->repository->hasWithToken($token);
    }
}
