<?php

namespace NnShop\Domain\Orders\Services\Generator;

use NnShop\Domain\Orders\Repository\InvoiceRepositoryInterface;
use NnShop\Domain\Orders\Services\Validator\InvoiceCodeValidator;
use NnShop\Domain\Orders\Value\InvoiceCode;

class InvoiceCodeGenerator
{
    /**
     * @var InvoiceRepositoryInterface
     */
    private $repository;

    /**
     * @var InvoiceCodeValidator
     */
    private $validator;

    /**
     * InvoiceCodeGenerator constructor.
     *
     * @param InvoiceRepositoryInterface $repository
     * @param InvoiceCodeValidator       $validator
     */
    public function __construct(InvoiceRepositoryInterface $repository, InvoiceCodeValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * @return InvoiceCode
     */
    public function generate(): InvoiceCode
    {
        $highest = $this->repository->findHighestCode(date('Y'));
        $highest = (null === $highest) ? 0 : $highest->getNumber();

        do {
            $code = InvoiceCode::generate(date('Y'), $highest);
        } while (!$this->validator->isUnique($code));

        return $code;
    }
}
