<?php

namespace NnShop\Domain\Orders\Services\Generator;

use NnShop\Domain\Orders\Repository\SubscriptionRepositoryInterface;
use NnShop\Domain\Orders\Services\Validator\SubscriptionCodeValidator;
use NnShop\Domain\Orders\Value\SubscriptionCode;

class SubscriptionCodeGenerator
{
    /**
     * @var SubscriptionRepositoryInterface
     */
    private $repository;

    /**
     * @var SubscriptionCodeValidator
     */
    private $validator;

    /**
     * OrderNumberGenerator constructor.
     *
     * @param SubscriptionRepositoryInterface $repository
     * @param SubscriptionCodeValidator       $validator
     */
    public function __construct(SubscriptionRepositoryInterface $repository, SubscriptionCodeValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * @return SubscriptionCode
     */
    public function generate(): SubscriptionCode
    {
        $highest = $this->repository->findHighestCode();
        $highest = (null === $highest) ? 0 : $highest->getInteger();

        do {
            $code = SubscriptionCode::generate($highest);
        } while (!$this->validator->isUnique($code));

        return $code;
    }
}
