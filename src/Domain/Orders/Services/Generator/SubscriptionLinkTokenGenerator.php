<?php

namespace NnShop\Domain\Orders\Services\Generator;

use Core\Common\Generator\Security\TokenGenerator;
use NnShop\Domain\Orders\Repository\SubscriptionLinkRepository;
use NnShop\Domain\Orders\Services\Validator\SubscriptionLinkTokenValidator;
use NnShop\Domain\Orders\Value\SubscriptionLinkToken;

class SubscriptionLinkTokenGenerator
{
    /**
     * @var SubscriptionLinkRepository
     */
    private $repository;

    /**
     * @var SubscriptionLinkTokenValidator
     */
    private $validator;

    /**
     * SubscriptionDeliverableTokenGenerator constructor.
     *
     * @param SubscriptionLinkRepository     $repository
     * @param SubscriptionLinkTokenValidator $validator
     */
    public function __construct(SubscriptionLinkRepository $repository, SubscriptionLinkTokenValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * @return SubscriptionLinkToken
     */
    public function generate(): SubscriptionLinkToken
    {
        do {
            $token = SubscriptionLinkToken::from(TokenGenerator::generate(SubscriptionLinkToken::LENGTH, true));
        } while (!$this->validator->isUnique($token));

        return $token;
    }
}
