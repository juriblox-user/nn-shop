<?php

namespace NnShop\Domain\Orders\Services\Generator;

use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Orders\Services\Validator\OrderCodeValidator;
use NnShop\Domain\Orders\Value\OrderCode;

class OrderCodeGenerator
{
    /**
     * @var OrderRepositoryInterface
     */
    private $repository;

    /**
     * @var OrderCodeValidator
     */
    private $validator;

    /**
     * OrderNumberGenerator constructor.
     *
     * @param OrderRepositoryInterface $repository
     * @param OrderCodeValidator       $validator
     */
    public function __construct(OrderRepositoryInterface $repository, OrderCodeValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * @return OrderCode
     */
    public function generate(): OrderCode
    {
        $highest = $this->repository->findHighestCode();
        $highest = (null === $highest) ? 0 : $highest->getInteger();

        do {
            $code = OrderCode::generate($highest);
        } while (!$this->validator->isUnique($code));

        return $code;
    }
}
