<?php

namespace NnShop\Domain\Orders\Services\Resolver;

use Core\Common\Exception\DataIntegrityException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use NnShop\Domain\Templates\Entity\Answer;
use NnShop\Domain\Templates\Entity\Document;
use NnShop\Domain\Templates\Entity\Question;
use NnShop\Domain\Templates\Entity\QuestionOption;
use NnShop\Domain\Templates\Entity\QuestionStep;
use NnShop\Domain\Templates\Repository\QuestionStepRepositoryInterface;
use NnShop\Domain\Templates\Value\QuestionId;
use NnShop\Domain\Templates\Value\QuestionOptionId;
use NnShop\Domain\Templates\Value\QuestionStepId;

class QuestionsGraph
{
    /**
     * @var ArrayCollection|Answer[]
     */
    private $answers;

    /**
     * @var Document
     */
    private $document;

    /**
     * @var ArrayCollection|QuestionStep[]
     */
    private $graph;

    /**
     * @var ArrayCollection|QuestionOption[]
     */
    private $options;

    /**
     * @var ArrayCollection|Question[]
     */
    private $questions;

    /**
     * @var QuestionStepRepositoryInterface
     */
    private $stepRepository;

    /**
     * @var ArrayCollection|QuestionStep[]
     */
    private $steps;

    /**
     * Constructor.
     *
     * @param QuestionStepRepositoryInterface $stepRepository
     */
    public function __construct(QuestionStepRepositoryInterface $stepRepository)
    {
        $this->stepRepository = $stepRepository;
    }

    /**
     * @param QuestionId $questionId
     *
     * @return Answer|null
     */
    public function findAnswer(QuestionId $questionId)
    {
        return $this->answers->get($questionId->getString());
    }

    /**
     * @return Collection|Answer[]
     */
    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    /**
     * @param QuestionId $questionId
     *
     * @return Collection|QuestionCondition[]
     */
    public function getConditions(QuestionId $questionId): Collection
    {
        return $this->getQuestion($questionId)->getConditions();
    }

    /**
     * @param QuestionOptionId $optionId
     *
     * @return QuestionOption
     */
    public function getOption(QuestionOptionId $optionId): QuestionOption
    {
        $option = $this->options->get($optionId->getString());
        if (null === $option) {
            throw new DataIntegrityException(sprintf('Could not find a QuestionOption with the ID %s', $optionId));
        }

        return $option;
    }

    /**
     * @param QuestionId $questionId
     *
     * @return Collection|QuestionOption[]
     */
    public function getOptions(QuestionId $questionId): Collection
    {
        return $this->getQuestion($questionId)->getVisibleOptions();
    }

    /**
     * @param QuestionId $questionId
     *
     * @return Question
     */
    public function getQuestion(QuestionId $questionId): Question
    {
        $question = $this->questions->get($questionId->getString());
        if (null === $question) {
            throw new DataIntegrityException(sprintf('Could not find a Question with the ID %s', $questionId));
        }

        return $question;
    }

    /**
     * @param QuestionStepId $stepId
     *
     * @return Collection|Question[]
     */
    public function getQuestions(QuestionStepId $stepId): Collection
    {
        return $this->getStep($stepId)->getVisibleQuestions();
    }

    /**
     * @param QuestionStepId $stepId
     *
     * @return QuestionStep
     */
    public function getStep(QuestionStepId $stepId): QuestionStep
    {
        $step = $this->steps->get($stepId->getString());
        if (null === $step) {
            throw new DataIntegrityException(sprintf('Could not find a QuestionStep with the ID %s', $stepId));
        }

        return $step;
    }

    /**
     * @return Collection|QuestionStep[]
     */
    public function getSteps(): Collection
    {
        return $this->steps;
    }

    /**
     * @param QuestionId $questionId
     *
     * @return bool
     */
    public function hasQuestion(QuestionId $questionId): bool
    {
        return null !== $this->questions->get($questionId->getString());
    }

    /**
     * Graph opnieuw laden.
     */
    public function refresh()
    {
        if (null === $this->document) {
            throw new \RuntimeException('Cannot refresh QuestionsGraph without a previously loaded Document');
        }

        $this->warmup($this->document);
    }

    /**
     * @param Document $document
     */
    public function warmup(Document $document)
    {
        $this->document = $document;

        $this->graph = new ArrayCollection();
        $this->steps = new ArrayCollection();

        $this->questions = new ArrayCollection();
        $this->answers = new ArrayCollection();
        $this->options = new ArrayCollection();

        foreach ($this->stepRepository->getGraph($document) as $step) {
            $this->graph->add($step);
            $this->steps->set($step->getId()->getString(), $step);

            foreach ($step->getVisibleQuestions() as $question) {
                $this->questions->set($question->getId()->getString(), $question);

                foreach ($question->getAnswers() as $answer) {
                    $this->answers->set($question->getId()->getString(), $answer);
                }

                foreach ($question->getVisibleOptions() as $option) {
                    $this->options->set($option->getId()->getString(), $option);
                }
            }
        }
    }
}
