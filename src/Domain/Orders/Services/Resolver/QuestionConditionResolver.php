<?php

namespace NnShop\Domain\Orders\Services\Resolver;

use NnShop\Domain\Orders\Data\QuestionWithAnswer;
use NnShop\Domain\Orders\Data\QuestionWithAnswerBag;
use NnShop\Domain\Templates\Entity\Question;
use NnShop\Domain\Templates\Entity\QuestionOption;
use NnShop\Domain\Templates\Enumeration\QuestionType;

class QuestionConditionResolver
{
    /**
     * @var QuestionWithAnswerBag
     */
    private $answers;

    /**
     * @var QuestionsGraph
     */
    private $graph;

    /**
     * @var bool
     */
    private $loaded;

    /**
     * Constructor.
     *
     * @param QuestionsGraph $graph
     */
    public function __construct(QuestionsGraph $graph)
    {
        $this->graph = $graph;
        $this->loaded = false;

        $this->clear();
    }

    /**
     * Bepalen of een vraag aan al zijn condities voldoet.
     *
     * @param Question $question
     *
     * @return QuestionConditionResolverResult
     */
    public function evaluate(Question $question): QuestionConditionResolverResult
    {
        if (!$this->loaded) {
            throw new \LogicException('You called QuestionConditionResolver::evaluate() without having loaded any answers. Are you sure about this?');
        }

        // Een infobox is altijd goed beantwoord
        if ($question->getType()->is(QuestionType::TYPE_INFOBOX)) {
            return new QuestionConditionResolverResult(true);
        }

        // Geen condities: beantwoorden is voldoende, en is dus ook altijd zichtbaar
        if (0 === $question->getConditions()->count()) {
            $result = new QuestionConditionResolverResult($this->isAnswered($question) || !$question->isRequired());
            $result->setVisible(true);

            return $result;
        }

        $result = new QuestionConditionResolverResult(true);
        foreach ($question->getConditions() as $condition) {
            /*
             * Conditie op basis van een "parent" vraag
             */
            if (null !== $condition->getParent()) {
                $conditionResult = $this->evaluate($condition->getParent());

                // De conditie van de parent vraag wordt niet behaald, dus deze vraag is niet relevant
                if ($conditionResult->isFail()) {
                    $result->merge($conditionResult);
                }

                // De parent vraag is zichtbaar en is beantwoord, dus de huidige $question is nu relevant
                elseif ($conditionResult->isVisible() && $this->isAnswered($condition->getParent())) {
                    $result->setVisible(true);

                    // Huidige $question is niet beantwoord, maar is wel verplicht
                    if (!$this->isAnswered($question) && $question->isRequired()) {
                        $result->addFailedCondition($condition);
                    }
                }
            }

            /*
             * Conditie op basis van een optie
             */
            elseif (null !== $condition->getOption()) {
                try {
                    $conditionResult = $this->evaluate($condition->getOption()->getQuestion());
                } catch (\Exception $exception) {
                    dump($condition);
                    exit();
                }

                // De conditie van de parent vraag wordt niet behaald, dus deze vraag doet er nu niet toe
                if ($conditionResult->isFail()) {
                    $result->merge($conditionResult);
                }

                // De parent vraag is zichtbaar en beantwoord met de juiste optie, dus de huidige $question is nu relevant
                elseif ($conditionResult->isVisible() && $this->isAnsweredWithOption($condition->getOption()->getQuestion(), $condition->getOption())) {
                    $result->setVisible(true);

                    // Huidige $question is niet beantwoord maar is wel verplicht
                    if (!$this->isAnswered($question) && $question->isRequired()) {
                        $result->addFailedCondition($condition);
                    }
                }
            }

            // Onbekende conditie
            else {
                throw new \Exception(sprintf('Could not determine condition for QuestionCondition [%s]', $condition->getId()));
            }
        }

        return $result;
    }

    /**
     * @param Question $question
     *
     * @return QuestionWithAnswer|null
     */
    public function getAnswer(Question $question)
    {
        if (!$this->loaded) {
            throw new \LogicException('You called QuestionConditionResolver::getAnswer() without having loaded any answers. Are you sure about this?');
        }

        return $this->answers->get($question->getId());
    }

    /**
     * @param Question $question
     *
     * @return bool|null
     */
    public function isAnswered(Question $question)
    {
        if (!$this->loaded) {
            throw new \LogicException('You called QuestionConditionResolver::isAnswered() without having loaded any answers. Are you sure about this?');
        }

        $answer = $this->answers->get($question->getId());
        if (null !== $answer) {
            return $answer->isAnswered();
        }

        $answer = $this->graph->findAnswer($question->getId());
        if (null === $answer) {
            return;
        }

        return null !== $answer->getValue();
    }

    /**
     * @param Question       $question
     * @param QuestionOption $option
     *
     * @return bool|null
     */
    public function isAnsweredWithOption(Question $question, QuestionOption $option)
    {
        if (!$this->loaded) {
            throw new \LogicException('You called QuestionConditionResolver::isAnsweredWithOption() without having loaded any answers. Are you sure about this?');
        }

        $answer = $this->answers->get($question->getId());
        if (null !== $answer) {
            return $answer->isAnsweredWith($option->getId());
        }

        $answer = $this->graph->findAnswer($question->getId());
        if (null === $answer) {
            return;
        }

        return $answer->hasOption($option);
    }

    /**
     * Complete graph valideren.
     *
     * @return bool
     */
    public function isValid(): bool
    {
        foreach ($this->graph->getSteps() as $step) {
            foreach ($this->graph->getQuestions($step->getId()) as $question) {
                $result = $this->evaluate($question);

                if ($result->isFail()) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param Question $question
     *
     * @return bool
     */
    public function isVisible(Question $question): bool
    {
        return $this->evaluate($question)->isVisible();
    }

    /**
     * @param QuestionWithAnswerBag $answers
     */
    public function load(QuestionWithAnswerBag $answers = null)
    {
        $this->clear();

        $this->answers->import($this->graph->getAnswers());
        if (null !== $answers) {
            $this->answers->merge($answers);
        }

        $this->loaded = true;
    }

    private function clear()
    {
        $this->answers = new QuestionWithAnswerBag();
    }
}
