<?php

namespace NnShop\Domain\Orders\Services\Resolver;

use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Domain\Templates\Entity\Question;
use NnShop\Domain\Templates\Entity\QuestionStep;

class QuestionStepResolver
{
    /**
     * @var QuestionConditionResolver
     */
    private $conditionResolver;

    /**
     * @var QuestionsGraph
     */
    private $graph;

    /**
     * @var ArrayCollection|QuestionStep[]
     */
    private $activeSteps;

    /**
     * @var ArrayCollection|QuestionStep[]
     */
    private $skippedSteps;

    /**
     * Constructor.
     *
     * @param QuestionsGraph $graph
     */
    public function __construct(QuestionsGraph $graph)
    {
        $this->graph = $graph;
        $this->conditionResolver = new QuestionConditionResolver($graph);

        $this->activeSteps = null;
        $this->skippedSteps = null;
    }

    /**
     * @return ArrayCollection|QuestionStep[]
     */
    public function getActiveSteps(): ArrayCollection
    {
        $this->evaluate();

        return $this->activeSteps;
    }

    /**
     * @return ArrayCollection|QuestionStep[]
     */
    public function getSkippedSteps(): ArrayCollection
    {
        $this->evaluate();

        return $this->skippedSteps;
    }

    /**
     * @param QuestionStep $step
     *
     * @return bool
     */
    public function shouldSkip(QuestionStep $step): bool
    {
        $this->evaluate();

        return $this->skippedSteps->contains($step);
    }

    /**
     * Bepalen welke stappen moeten worden overgeslagen.
     */
    private function evaluate()
    {
        if (null !== $this->skippedSteps) {
            return;
        }

        // De validatie wordt gebaseerd op de antwoorden uit de graph (= database)
        $this->conditionResolver->load();

        $this->activeSteps = new ArrayCollection();
        $this->skippedSteps = new ArrayCollection();

        foreach ($this->graph->getSteps() as $step) {
            $skip = true;

            /** @var Question $question */
            foreach ($step->getVisibleQuestions() as $question) {
                $result = $this->conditionResolver->evaluate($question);
                if ($result->isVisible()) {
                    $skip = false;

                    break;
                }
            }

            if ($skip) {
                $this->skippedSteps->add($step);
            } else {
                $this->activeSteps->add($step);
            }
        }
    }
}
