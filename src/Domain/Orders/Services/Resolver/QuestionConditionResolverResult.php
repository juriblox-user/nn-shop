<?php

namespace NnShop\Domain\Orders\Services\Resolver;

use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Domain\Templates\Entity\Question;
use NnShop\Domain\Templates\Entity\QuestionCondition;

class QuestionConditionResolverResult
{
    /**
     * @var ArrayCollection|QuestionCondition[]
     */
    private $failedConditions;

    /**
     * @var bool
     */
    private $success;

    /**
     * @var bool
     */
    private $visible;

    /**
     * Constructor.
     *
     * @param bool $success
     */
    public function __construct(bool $success)
    {
        $this->success = $success;
        $this->visible = false;

        $this->failedConditions = new ArrayCollection();
    }

    /**
     * QuestionCondition toevoegen waaraan niet is voldaan.
     *
     * @param QuestionCondition $condition
     */
    public function addFailedCondition(QuestionCondition $condition)
    {
        $this->failedConditions->add($condition);
        $this->success = false;
    }

    /**
     * @return ArrayCollection|QuestionCondition[]
     */
    public function getFailedConditions(): ArrayCollection
    {
        return $this->failedConditions;
    }

    /**
     * @return QuestionCondition
     */
    public function getLastFailedCondition(): QuestionCondition
    {
        if ($this->failedConditions->isEmpty()) {
            throw new \LogicException('This result does not have any failed conditions, check hasFailedConditions() first.');
        }

        return $this->failedConditions->last();
    }

    /**
     * Laatste vraag ophalen waarvan niet aan de conditie(s) werd voldaan.
     *
     * @return Question
     */
    public function getLastFailedQuestion(): Question
    {
        $condition = $this->getLastFailedCondition();
        if (null !== $condition->getParent()) {
            return $condition->getParent();
        } elseif (null !== $condition->getOption()) {
            return $condition->getOption()->getQuestion();
        }

        return $condition->getQuestion();
    }

    /**
     * @return bool
     */
    public function hasFailedConditions(): bool
    {
        return !$this->failedConditions->isEmpty();
    }

    /**
     * @return bool
     */
    public function isFail(): bool
    {
        return !$this->isSuccess();
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->visible;
    }

    /**
     * Resultaat samenvoegen met een ander resultaat.
     *
     * @param QuestionConditionResolverResult $result
     */
    public function merge(self $result)
    {
        $this->success = $this->success || $result->isSuccess();
        $this->visible = $this->visible && $result->isVisible();

        foreach (array_reverse($result->getFailedConditions()->toArray()) as $condition) {
            $this->prependFailedCondition($condition);
        }
    }

    /**
     * Conditie waar niet aan wordt voldaan toevoegen bovenaan de lijst.
     *
     * @param QuestionCondition $condition
     */
    public function prependFailedCondition(QuestionCondition $condition)
    {
        $this->failedConditions = new ArrayCollection(array_merge([$condition], $this->failedConditions->toArray()));
    }

    /**
     * @param bool $visible
     */
    public function setVisible(bool $visible)
    {
        $this->visible = $visible;
    }
}
