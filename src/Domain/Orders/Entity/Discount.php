<?php

namespace NnShop\Domain\Orders\Entity;

use Core\Annotation\Doctrine as Core;
use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\Common\Exception\MissingValueException;
use Core\Common\Validation\Assertion;
use Core\Domain\Traits\SoftDeleteableEntityTrait;
use Core\Domain\Value\Web\EmailAddress;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Comparable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use NnShop\Domain\Orders\Enumeration\DiscountType;
use NnShop\Domain\Orders\Value\DiscountId;
use NnShop\Domain\Shops\Entity\LandingPage;
use NnShop\Domain\Shops\Entity\Referrer;
use NnShop\Domain\Templates\Entity\Template;
use Money\Money;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Orders\Repository\DiscountDoctrineRepository")
 * @Core\QueryBuilder(class="NnShop\Infrastructure\Orders\QueryBuilder\DiscountDoctrineQueryBuilder")
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(name="ix_code", columns={"discount_code", "timestamp_deleted"}),
 *     @ORM\Index(name="ix_deleted", columns={"timestamp_deleted"})
 * })
 *
 * @Gedmo\SoftDeleteable(fieldName="timestampDeleted")
 */
class Discount implements Comparable
{
    use SoftDeleteableEntityTrait;

    /**
     * Regex waaraan een kortingscode moet voldoen.
     */
    const CODE_REGEX = '/^[A-Z0-9\-]+$/';

    /**
     * @ORM\Column(type="core.financial.money", nullable=true)
     *
     * @var Money
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=50)
     *
     * @var string
     */
    private $code;

    /**
     * @ORM\Id
     * @ORM\Column(type="orders.discount_id")
     *
     * @var DiscountId
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\InvoiceDiscount", mappedBy="discount", cascade={"persist"}, fetch="EXTRA_LAZY")
     *
     * @var Collection|InvoiceDiscount
     */
    private $invoiceLinks;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Shops\Entity\LandingPage", mappedBy="discount")
     *
     * @var PersistentCollection|LandingPage[]
     */
    private $landingPages;

    /**
     * @ORM\ManyToMany(targetEntity="\NnShop\Domain\Orders\Entity\Order", mappedBy="discounts", cascade={"persist"}, fetch="EXTRA_LAZY")
     *
     * @var Collection|Order[]
     */
    private $orders;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     *
     * @var int
     */
    private $percentage;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Shops\Entity\Referrer", mappedBy="discount")
     *
     * @var PersistentCollection|Referrer[]
     */
    private $referrers;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="orders.discount_type")
     *
     * @var DiscountType
     */
    private $type;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     *
     * @var int|null
     */
    private $maxUses;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": true})
     *
     * @var bool
     */
    private $active;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="NnShop\Domain\Templates\Entity\Template", inversedBy="discounts")
     * @ORM\JoinTable(name="orders_discount_templates",
     *     joinColumns={@ORM\JoinColumn(name="discount_id", referencedColumnName="discount_id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="template_id", referencedColumnName="template_id")}
     * )
     */
    private $templates;

    /**
     * @ORM\Column(type="array")
     *
     * @var array
     */
    private $emails;

    /**
     * Discount constructor.
     *
     * @param DiscountId $id
     */
    private function __construct(DiscountId $id)
    {
        $this->id = $id;

        $this->invoiceLinks = new ArrayCollection();
        $this->emails = new ArrayCollection();

        $this->orders = new ArrayCollection();
        $this->landingPages = new ArrayCollection();
        $this->referrers = new ArrayCollection();
        $this->templates = new ArrayCollection();
    }

    /**
     * @param DiscountId   $id
     * @param string       $code
     * @param DiscountType $type
     *
     * @throws \Assert\AssertionFailedException
     *
     * @return Discount
     */
    public static function create(DiscountId $id, string $code, DiscountType $type): self
    {
        $discount = new self($id);
        $discount->setCode($code);
        $discount->setType($type);

        return $discount;
    }

    /**
     * @param Invoice $invoice
     *
     * @throws MissingValueException
     */
    public function addInvoice(Invoice $invoice)
    {
        InvoiceDiscount::create($invoice, $this);
    }

    /**
     * @param InvoiceDiscount $link
     */
    public function addInvoiceLink(InvoiceDiscount $link)
    {
        if ($this->isUsedByInvoice($link->getInvoice())) {
            throw new DuplicateEntityException();
        }

        $this->invoiceLinks->add($link);
    }

    /**
     * @param LandingPage $landingPage
     */
    public function addLandingPage(LandingPage $landingPage)
    {
        if ($this->landingPages->contains($landingPage)) {
            return;
        }

        $this->landingPages->add($landingPage);
    }

    /**
     * @param Order $order
     */
    public function addOrder(Order $order)
    {
        if ($this->orders->contains($order)) {
            throw new DuplicateEntityException();
        }

        $this->orders->add($order);
    }

    /**
     * @param Referrer $referrer
     */
    public function addReferrer(Referrer $referrer)
    {
        if ($this->referrers->contains($referrer)) {
            return;
        }

        $this->referrers->add($referrer);
    }

    /**
     * {@inheritdoc}
     *
     * @param $other Discount
     */
    public function compareTo($other)
    {
        return $this->getId() == $other->getId() ? 0 : -1;
    }

    /**
     * @throws MissingValueException
     *
     * @return Money
     */
    public function getAmount(): Money
    {
        if ($this->type->isNot(DiscountType::FIXED)) {
            throw new \DomainException('The discount type is incorrect for this operation');
        }

        if (null === $this->amount) {
            throw new MissingValueException();
        }

        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return DiscountId
     */
    public function getId(): DiscountId
    {
        return $this->id;
    }

    /**
     * @throws MissingValueException
     *
     * @return float
     */
    public function getMultiplier(): float
    {
        return $this->getPercentage() / 100;
    }

    /**
     * @throws MissingValueException
     *
     * @return int
     */
    public function getPercentage(): int
    {
        if ($this->type->isNot(DiscountType::PERCENTAGE)) {
            throw new \DomainException('The discount type is incorrect for this operation');
        }

        if (null === $this->percentage) {
            throw new MissingValueException();
        }

        return $this->percentage;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return DiscountType
     */
    public function getType(): DiscountType
    {
        return $this->type;
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    public function hasOrder(Order $order): bool
    {
        return $this->orders->contains($order);
    }

    /**
     * @param Invoice $invoice
     *
     * @return bool
     */
    public function isUsedByInvoice(Invoice $invoice): bool
    {
        return null !== $this->findInvoiceLink($invoice);
    }

    /**
     * @param Invoice $invoice
     */
    public function removeInvoice(Invoice $invoice)
    {
        $link = $this->findInvoiceLink($invoice);
        if (null === $link) {
            return;
        }

        $this->removeInvoiceLink($link);
        $invoice->removeDiscountLink($link);
    }

    /**
     * @param InvoiceDiscount $link
     */
    public function removeInvoiceLink(InvoiceDiscount $link)
    {
        if (!$this->invoiceLinks->contains($link)) {
            return;
        }

        $this->invoiceLinks->removeElement($link);
    }

    /**
     * @param LandingPage $landingPage
     */
    public function removeLandingPage(LandingPage $landingPage)
    {
        if (!$this->landingPages->isInitialized() || !$this->landingPages->contains($landingPage)) {
            return;
        }

        $this->landingPages->removeElement($landingPage);
    }

    /**
     * @param Order $order
     */
    public function removeOrder(Order $order)
    {
        if (!$this->orders->contains($order)) {
            return;
        }

        $this->orders->removeElement($order);
    }

    /**
     * @param Referrer $referrer
     */
    public function removeReferrer(Referrer $referrer)
    {
        if (!$this->referrers->isInitialized() || !$this->referrers->contains($referrer)) {
            return;
        }

        $this->referrers->removeElement($referrer);
    }

    /**
     * @param Money $amount
     */
    public function setAmount(Money $amount)
    {
        if ($this->type->isNot(DiscountType::FIXED)) {
            throw new \DomainException('The discount type is incorrect for this operation');
        }

        if ($amount->isNegative() || $amount->isZero()) {
            throw new \InvalidArgumentException();
        }

        $this->amount = $amount;
    }

    /**
     * @param string $code
     *
     * @throws \Assert\AssertionFailedException
     */
    public function setCode(string $code)
    {
        Assertion::maxLength($code, 50);
        if (0 == preg_match(self::CODE_REGEX, $code)) {
            throw new \InvalidArgumentException(sprintf('"%s" should consist of only A-Z, 0-9 and -', $code));
        }

        $this->code = mb_strtoupper($code);
    }

    /**
     * @param int $percentage
     */
    public function setPercentage(int $percentage)
    {
        if ($this->type->isNot(DiscountType::PERCENTAGE)) {
            throw new \DomainException('The discount type is incorrect for this operation');
        }

        Assertion::greaterThan($percentage, 0);

        $this->percentage = $percentage;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @param DiscountType $type
     */
    public function setType(DiscountType $type)
    {
        if ($type === $this->type) {
            return;
        }

        $this->amount = null;
        $this->percentage = null;

        $this->type = $type;
    }

    /**
     * @return int|null
     */
    public function getMaxUses()
    {
        return $this->maxUses;
    }

    /**
     * @param int|null $maxUses
     *
     * @return Discount
     */
    public function setMaxUses($maxUses)
    {
        $this->maxUses = $maxUses;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountUses(): int
    {
        return $this->invoiceLinks->count();
    }

    /**
     * @return bool
     */
    public function canUse(): bool
    {
        return null === $this->getMaxUses() || $this->getCountUses() < $this->getMaxUses();
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     *
     * @return Discount
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getTemplates(): Collection
    {
        return $this->templates;
    }

    /**
     * @param Collection $templates
     *
     * @return Discount
     */
    public function setTemplates(Collection $templates): self
    {
        $this->templates = $templates;

        return $this;
    }

    /**
     * @param Template $template
     *
     * @return Discount
     */
    public function addTemplate(Template $template): self
    {
        $this->templates->add($template);

        return $this;
    }

    /**
     * @param Template $template
     *
     * @return Discount
     */
    public function removeTemplate(Template $template): self
    {
        if ($this->templates->contains($template)) {
            $this->templates->remove($template);
        }

        return $this;
    }

    /**
     * @param Template $template
     *
     * @return bool
     */
    public function hasTemplate(Template $template): bool
    {
        return $this->templates->contains($template);
    }

    /**
     * @return Collection
     */
    public function getEmails(): Collection
    {
        return $this->emails;
    }

    /**
     * @param Collection $emails
     *
     * @return self
     */
    public function setEmails(Collection $emails)
    {
        $this->emails = $emails;

        return $this;
    }

    /**
     * @param EmailAddress $email
     *
     * @return Discount
     */
    public function addEmail(EmailAddress $email): self
    {
        if (!$this->emails->contains($email)) {
            $this->emails->add($email);
        }

        return $this;
    }

    /**
     * @param EmailAddress $email
     *
     * @return Discount
     */
    public function removeEmail(EmailAddress $email): self
    {
        if ($this->emails->contains($email)) {
            $this->emails->remove($email);
        }

        return $this;
    }

    /**
     * @param EmailAddress $email
     *
     * @return bool
     */
    public function hasEmail(EmailAddress $email): bool
    {
        /** @var EmailAddress $extEmail */
        foreach ($this->emails as $extEmail) {
            if ($extEmail->getString() === $email->getString()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Invoice $invoice
     *
     * @return InvoiceDiscount|null
     */
    private function findInvoiceLink(Invoice $invoice)
    {
        $links = $this->invoiceLinks->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('invoice', $invoice)
            )
        );

        return $links->isEmpty() ? null : $links->first();
    }
}
