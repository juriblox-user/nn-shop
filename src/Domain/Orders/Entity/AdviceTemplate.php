<?php

namespace NnShop\Domain\Orders\Entity;

use Doctrine\ORM\Mapping as ORM;
use NnShop\Domain\Templates\Entity\Template;

/**
 * @ORM\Entity
 */
class AdviceTemplate
{
    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Orders\Entity\Advice", inversedBy="templateLinks")
     * @ORM\JoinColumn(referencedColumnName="advice_id", nullable=false)
     *
     * @var Advice
     */
    private $advice;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="link_id")
     * @ORM\GeneratedValue
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Template", inversedBy="adviceLinks")
     * @ORM\JoinColumn(referencedColumnName="template_id", nullable=false)
     *
     * @var Template
     */
    private $template;

    /**
     * AdviceTemplate constructor.
     *
     * @param Advice   $advice
     * @param Template $template
     */
    private function __construct(Advice $advice, Template $template)
    {
        $this->advice = $advice;
        $this->template = $template;

        $this->advice->addTemplateLink($this);
        $this->template->addAdviceLink($this);
    }

    /**
     * @param Advice   $advice
     * @param Template $template
     *
     * @return AdviceTemplate
     */
    public static function create(Advice $advice, Template $template): self
    {
        return new self($advice, $template);
    }

    /**
     * Detach the link from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->advice) {
            $this->advice->removeTemplateLink($this);
            $this->advice = null;
        }

        if (null !== $this->template) {
            $this->template->removeAdviceLink($this);
            $this->template = null;
        }
    }

    /**
     * @return Advice
     */
    public function getAdvice(): Advice
    {
        return $this->advice;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->template;
    }
}
