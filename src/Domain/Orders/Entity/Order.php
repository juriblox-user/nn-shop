<?php

namespace NnShop\Domain\Orders\Entity;

use Carbon\Carbon;
use Core\Annotation\Doctrine as Core;
use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\Component\Flow\StateInterface;
use Core\Component\Flow\StateSubjectInterface;
use Core\Domain\Value\Web\EmailAddress;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use NnShop\Domain\Orders\Attribute\OrderAttribute;
use NnShop\Domain\Orders\Attribute\OrderAttributeBag;
use NnShop\Domain\Orders\Enumeration\OrderStatus;
use NnShop\Domain\Orders\Enumeration\OrderType;
use NnShop\Domain\Orders\Enumeration\PaymentMethod;
use NnShop\Domain\Orders\Event\Order\OrderAbandonedEvent;
use NnShop\Domain\Orders\Event\Order\OrderCompletedEvent;
use NnShop\Domain\Orders\Event\Order\OrderCreatedEvent;
use NnShop\Domain\Orders\Event\Order\OrderPendingManualCheckEvent;
use NnShop\Domain\Orders\Event\Order\OrderPendingManualCustomEvent;
use NnShop\Domain\Orders\Event\Order\OrderRequeuedEvent;
use NnShop\Domain\Orders\Value\OrderCode;
use NnShop\Domain\Orders\Value\OrderId;
use NnShop\Domain\Shops\Entity\Referrer;
use NnShop\Domain\Shops\Entity\Shop;
use NnShop\Domain\Shops\Event\Referrer\ReferrerOrderEvent;
use NnShop\Domain\Templates\Entity\Document;
use NnShop\Domain\Templates\Entity\QuestionStep;
use NnShop\Domain\Templates\Entity\Template;
use RuntimeException as InvalidStateException;
use SimpleBus\Message\Recorder\ContainsRecordedMessages;
use SimpleBus\Message\Recorder\PrivateMessageRecorderCapabilities;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Orders\Repository\OrderDoctrineRepository")
 * @Core\QueryBuilder(class="NnShop\Infrastructure\Orders\QueryBuilder\OrderDoctrineQueryBuilder")
 *
 * @ORM\Table(
 *     indexes={
 *         @ORM\Index(name="ix_type", columns={"order_type", "order_status"}),
 *         @ORM\Index(name="ix_customer", columns={"customer_id", "timestamp_created"}),
 *         @ORM\Index(name="ix_status", columns={"order_status", "timestamp_created"}),
 *         @ORM\Index(name="ix_step", columns={"step_id"}),
 *         @ORM\Index(name="ix_updated", columns={"timestamp_updated", "order_status"})
 *     }
 * )
 */
class Order implements ContainsRecordedMessages, StateSubjectInterface
{
    use PrivateMessageRecorderCapabilities;

    /**
     * @ORM\Column(type="orders.order_attribute_bag")
     *
     * @var OrderAttributeBag
     */
    private $attributes;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $check;

    /**
     * @ORM\Column(type="orders.order_code", unique=true)
     *
     * @var OrderCode
     */
    private $code;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $custom;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Orders\Entity\Customer", inversedBy="orders", fetch="EAGER")
     * @ORM\JoinColumn(referencedColumnName="customer_id")
     *
     * @var Customer
     */
    private $customer;

    /**
     * @ORM\ManyToMany(targetEntity="\NnShop\Domain\Orders\Entity\Discount", inversedBy="orders", cascade={"persist"})
     *
     * @ORM\JoinTable(name="orders_order_discount",
     *     joinColumns={@ORM\JoinColumn(referencedColumnName="order_id")},
     *     inverseJoinColumns={@ORM\JoinColumn(referencedColumnName="discount_id")}
     * )
     *
     * @var Collection|Discount[]
     */
    private $discounts;

    /**
     * @ORM\OneToOne(targetEntity="\NnShop\Domain\Templates\Entity\Document", mappedBy="order", fetch="EAGER")
     *
     * @var Document
     */
    private $document;

    /**
     * @ORM\Id
     * @ORM\Column(type="orders.order_id")
     *
     * @var OrderId
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\InvoiceItem", mappedBy="order", cascade={"persist", "remove"}, fetch="LAZY")
     *
     * @var Collection|InvoiceItem[]
     */
    private $invoiceItems;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Orders\Entity\Subscription", inversedBy="updateOrders", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="subscription_id", referencedColumnName="subscription_id")
     *
     * @var Subscription
     */
    private $issuingSubscription;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $locked;

    /**
     * @ORM\Column(type="orders.payment_method", nullable=true)
     *
     * @var PaymentMethod|null
     */
    private $paymentMethod;

    /**
     * @ORM\OneToOne(targetEntity="Order", fetch="LAZY")
     * @ORM\JoinColumn(name="previous_id", referencedColumnName="order_id")
     *
     * @var Order|null
     */
    private $previousOrder;

    /**
     * Proforma factuur.
     *
     * @var Invoice
     */
    private $proforma;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Shops\Entity\Referrer", inversedBy="orders", fetch="LAZY")
     * @ORM\JoinColumn(referencedColumnName="referrer_id")
     *
     * @var Referrer
     */
    private $referrer;

    /**
     * @ORM\Column(type="core.web.email_address", name="registered_address", nullable=true)
     *
     * @var EmailAddress|null
     */
    private $registeredEmailAddress;

    /**
     * @ORM\Column(type="boolean", name="registered_requested")
     *
     * @var bool
     */
    private $registeredEmailRequested;

    /**
     * @ORM\Column(type="core.time.datetime", name="registered_sent", nullable=true)
     *
     * @var Carbon|null
     */
    private $registeredEmailSentAt;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Shops\Entity\Shop", inversedBy="orders")
     * @ORM\JoinColumn(referencedColumnName="shop_id", nullable=true)
     *
     * @var Shop
     */
    private $shop;

    /**
     * @ORM\Column(type="orders.order_status")
     *
     * @var OrderStatus
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\OrderStatusChange", mappedBy="order", cascade={"persist", "remove"}, orphanRemoval=true, fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"timestamp": "DESC"})
     *
     * @var Collection|OrderStatusChange[]
     */
    private $statusChanges;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\QuestionStep", inversedBy="orders", fetch="LAZY")
     * @ORM\JoinColumn(referencedColumnName="step_id")
     *
     * @var QuestionStep
     */
    private $step;

    /**
     * @ORM\OneToOne(targetEntity="\NnShop\Domain\Orders\Entity\Subscription", mappedBy="order", fetch="EXTRA_LAZY")
     *
     * @var Subscription
     */
    private $subscription;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon|null
     */
    private $timestampAbandoned;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon|null
     */
    private $timestampAnswered;

    /**
     * @ORM\Column(type="core.time.datetime")
     *
     * @var Carbon
     */
    private $timestampCreated;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon|null
     */
    private $timestampDelivered;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon|null
     */
    private $timestampRefreshed;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon|null
     */
    private $timestampStarted;

    /**
     * @ORM\Column(type="core.time.datetime")
     *
     * @var Carbon
     */
    private $timestampUpdated;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon|null
     */
    private $timestampReferralMailSent;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon|null
     */
    private $timestampUpsellMailSent;

    /**
     * @ORM\Column(type="orders.order_type")
     *
     * @var OrderType
     */
    private $type;

    /**
     * Order constructor.
     *
     * @param OrderId   $id
     * @param OrderCode $number
     */
    private function __construct(OrderId $id, OrderCode $number)
    {
        $this->id   = $id;
        $this->code = $number;

        $this->type = OrderType::from(OrderType::DRAFT);

        $this->check  = false;
        $this->custom = false;

        $this->registeredEmailRequested = false;

        $this->locked = false;

        $this->attributes = new OrderAttributeBag();

        $this->discounts     = new ArrayCollection();
        $this->invoiceItems  = new ArrayCollection();
        $this->statusChanges = new ArrayCollection();

        $this->timestampCreated = Carbon::now();
        $this->timestampUpdated = $this->timestampCreated;

        $this->setStatus(OrderStatus::from(OrderStatus::DRAFT));
    }

    /**
     * @param OrderId   $id
     * @param OrderCode $number
     *
     * @return Order
     */
    public static function prepareAnonymous(OrderId $id, OrderCode $number): self
    {
        $order = new self($id, $number);
        $order->record(OrderCreatedEvent::create($id));

        return $order;
    }

    /**
     * Prepare a re-order of an existing document.
     *
     * @param OrderId   $id
     * @param OrderCode $number
     * @param Order     $previousOrder
     *
     * @return Order
     *
     * @see Document::reorder()     For re-ordering a document
     */
    public static function prepareReorder(OrderId $id, OrderCode $number, self $previousOrder): self
    {
        $order = self::prepareWithCustomer($id, $number, $previousOrder->getCustomer());
        $order->linkPreviousOrder($previousOrder);

        $order->record(OrderCreatedEvent::create($id));

        return $order;
    }

    /**
     * Prepare a re-order of an existing order, as requested by an active subscription.
     *
     * @param OrderId      $id
     * @param OrderCode    $number
     * @param Subscription $subscription
     *
     * @return Order
     *
     * @see Document::update()      For issuing an update based on a subscription
     */
    public static function prepareUpdate(OrderId $id, OrderCode $number, Subscription $subscription): self
    {
        $order = new self($id, $number, $subscription->getCustomer());
        $order->setType(OrderType::from(OrderType::UPDATE));

        $order->linkIssuingSubscription($subscription);

        $latestOrder = $subscription->getLatestUpdateOrder();
        if (null !== $latestOrder) {
            $order->linkPreviousOrder($latestOrder);
        }

        $order->record(OrderCreatedEvent::create($id));

        return $order;
    }

    /**
     * Prepare an order for an active customer.
     *
     * @param OrderId   $id
     * @param OrderCode $number
     * @param Customer  $customer
     *
     * @return Order
     */
    public static function prepareWithCustomer(OrderId $id, OrderCode $number, Customer $customer): self
    {
        $order = new self($id, $number);
        $order->linkCustomer($customer);

        $order->record(OrderCreatedEvent::create($id));

        return $order;
    }

    /**
     * @param InvoiceItem $item
     */
    public function addInvoiceItem(InvoiceItem $item)
    {
        if ($this->hasInvoiceItem($item)) {
            throw new DuplicateEntityException();
        }

        $this->invoiceItems->add($item);
    }

    /**
     * @param OrderStatusChange $change
     */
    public function addStatusChange(OrderStatusChange $change)
    {
        $this->statusChanges->add($change);

        $this->timestampAbandoned = null;
        $this->timestampUpdated   = $change->getTimestamp();
    }

    /**
     * @param Discount $discount
     */
    public function applyDiscount(Discount $discount)
    {
        if ($this->discounts->contains($discount)) {
            throw new DuplicateEntityException();
        }

        $this->discounts->add($discount);

        $discount->addOrder($this);
    }

    /**
     * Handmatige controle annuleren.
     */
    public function cancelCheck()
    {
        $this->check = false;
    }

    /**
     * Maatwerk aanvraag annuleren.
     */
    public function cancelCustom()
    {
        $this->custom = false;
    }

    public function cancelRegisteredEmailRequest(): void
    {
        $this->registeredEmailRequested = false;
    }

    /**
     * @param Customer $customer
     */
    public function claim(Customer $customer)
    {
        if (null !== $this->customer) {
            $this->unlinkCustomer();
        }

        $this->linkCustomer($customer);
    }

    /**
     * Detach the order from its associated entities.
     */
    public function detach()
    {
        $this->unlinkCustomer();
        $this->unlinkShop();

        foreach ($this->discounts as $discount) {
            $this->removeDiscount($discount);
        }
    }

    /**
     * @param OrderStatus $status
     *
     * @return OrderStatusChange|null
     */
    public function findFirstStatusChange(OrderStatus $status)
    {
        $matches = $this->getStatusChangesWith($status);

        return $matches->isEmpty() ? null : $matches->last();
    }

    /**
     * @return Invoice|null
     */
    public function findInvoice()
    {
        if ($this->invoiceItems->isEmpty()) {
            return;
        }

        return $this->invoiceItems->last()->getInvoice();
    }

    /**
     * @param OrderStatus $status
     *
     * @return OrderStatusChange|null
     */
    public function findLastStatusChange(OrderStatus $status)
    {
        $matches = $this->getStatusChangesWith($status);

        return $matches->isEmpty() ? null : $matches->first();
    }

    /**
     * @return OrderCode
     */
    public function getCode(): OrderCode
    {
        return $this->code;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return Collection|Discount[]
     */
    public function getDiscounts(): Collection
    {
        return $this->discounts;
    }

    /**
     * @return Document
     */
    public function getDocument(): Document
    {
        return $this->document;
    }

    /**
     * @return OrderId
     */
    public function getId(): OrderId
    {
        return $this->id;
    }

    /**
     * Factuur ophalen waar deze bestelling aan is gekoppeld.
     *
     * @return Invoice
     */
    public function getInvoice(): Invoice
    {
        $invoice = $this->findInvoice();
        if (null === $invoice) {
            throw new \LogicException('This Order is not linked to any Invoice entity');
        }

        return $invoice;
    }

    /**
     * @return Collection|InvoiceItem[]
     */
    public function getInvoiceItems(): Collection
    {
        return $this->invoiceItems;
    }

    /**
     * @return Subscription
     */
    public function getIssuingSubscription()
    {
        return $this->issuingSubscription;
    }

    /**
     * @return PaymentMethod|null
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @return Order|null
     */
    public function getPreviousOrder()
    {
        return $this->previousOrder;
    }

    /**
     * Proforma factuur ophalen/genereren voor deze bestelling.
     *
     * @return Invoice
     */
    public function getProforma(): Invoice
    {
        if (null === $this->proforma) {
            $this->proforma = Invoice::proforma($this);
        }

        return $this->proforma;
    }

    public function getRegisteredEmailAddress(): ?EmailAddress
    {
        return $this->registeredEmailAddress;
    }

    public function getRegisteredEmailSentAt(): ?Carbon
    {
        return $this->registeredEmailSentAt;
    }

    /**
     * @return Shop|null
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Huidige staat ophalen.
     *
     * @return StateInterface
     */
    public function getState(): StateInterface
    {
        return $this->status;
    }

    /**
     * @return OrderStatus|StateInterface
     */
    public function getStatus(): StateInterface
    {
        return $this->status;
    }

    /**
     * @return Collection|OrderStatusChange[]
     */
    public function getStatusChanges(): Collection
    {
        return $this->statusChanges;
    }

    /**
     * @param OrderStatus $status
     *
     * @return Collection|OrderStatus[]
     */
    public function getStatusChangesWith(OrderStatus $status): Collection
    {
        return $this->statusChanges->filter(function (OrderStatusChange $change) use ($status) {
            return $status == $change->getStatus();
        });
    }

    /**
     * @return string
     */
    public function getStatusString(): string
    {
        return $this->status->getValue();
    }

    /**
     * @return QuestionStep
     */
    public function getStep(): QuestionStep
    {
        if (null === $this->step) {
            $steps = $this->document->getTemplate()->getQuestionSteps();
            if (0 == $steps->count()) {
                throw new \OutOfBoundsException(sprintf('The template [%s] has no QuestionSteps', $this->document->getTemplate()->getId()));
            }

            // TODO: kijken welke stap nog onbeantwoorde vragen heeft

            $this->step = $steps->first();
        }

        return $this->step;
    }

    /**
     * @return Subscription|null
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->document->getTemplate();
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampAbandoned()
    {
        return $this->timestampAbandoned;
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampAnswered()
    {
        return $this->timestampAnswered;
    }

    /**
     * @return Carbon
     */
    public function getTimestampCreated(): Carbon
    {
        return $this->timestampCreated;
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampDelivered()
    {
        return $this->timestampDelivered;
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampRefreshed()
    {
        return $this->timestampRefreshed;
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampStarted()
    {
        return $this->timestampStarted;
    }

    /**
     * @return Carbon
     */
    public function getTimestampUpdated(): Carbon
    {
        return $this->timestampUpdated;
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampReferralMailSent(): ?Carbon
    {
        return $this->timestampReferralMailSent;
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampUpsellMailSent(): ?Carbon
    {
        return $this->timestampUpsellMailSent;
    }

    /**
     * @return OrderType
     */
    public function getType(): OrderType
    {
        return $this->type;
    }

    /**
     * @param OrderStatus $status
     *
     * @return bool
     */
    public function hadStatus(OrderStatus $status): bool
    {
        $previous = $this->statusChanges->filter(function (OrderStatusChange $change) {
            return $change != $this->statusChanges->last();
        });

        return !$previous->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('status', $status)
            )
        )->isEmpty();
    }

    /**
     * @return bool
     */
    public function hasCustomer(): bool
    {
        return null !== $this->customer;
    }

    /**
     * @param Discount $discount
     *
     * @return bool
     */
    public function hasDiscount(Discount $discount): bool
    {
        return $this->discounts->contains($discount);
    }

    /**
     * @return bool
     */
    public function hasDocument(): bool
    {
        return null !== $this->document;
    }

    /**
     * @return bool
     */
    public function hasInvoice(): bool
    {
        return !$this->invoiceItems->isEmpty();
    }

    /**
     * @param InvoiceItem $item
     *
     * @return bool
     */
    public function hasInvoiceItem(InvoiceItem $item): bool
    {
        return $this->invoiceItems->contains($item);
    }

    /**
     * @param OrderStatus $status
     *
     * @return bool
     */
    public function hasStatus(OrderStatus $status): bool
    {
        return !$this->statusChanges->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('status', $status)
            )
        )->isEmpty();
    }

    /**
     * @return bool
     */
    public function isAbandoned(): bool
    {
        return null !== $this->timestampAbandoned;
    }

    /**
     * Conversie tracking is doorgegeven aan Google Adwords.
     *
     * @return bool
     */
    public function isAdwordsConversionTracked(): bool
    {
        return $this->getAttribute(OrderAttribute::ADWORDS_CONVERSION_TRACKED, false);
    }

    /**
     * @return bool
     */
    public function isCheckRequested(): bool
    {
        return $this->check;
    }

    /**
     * @return bool
     */
    public function isCustomRequested(): bool
    {
        return $this->custom;
    }

    /**
     * @return bool
     */
    public function isDelivered(): bool
    {
        return null !== $this->timestampDelivered;
    }

    /**
     * Conversie tracking is doorgegeven aan Google Analytics.
     *
     * @return bool
     */
    public function isGoogleConversionTracked(): bool
    {
        return $this->getAttribute(OrderAttribute::GOOGLE_CONVERSION_TRACKED, false);
    }

    /**
     * @return bool
     */
    public function isLocked(): bool
    {
        return $this->locked;
    }

    /**
     * Controleren of er niet recent een RefreshOrderCommand is uitgevoerd op deze bestelling.
     *
     * @param int $minutes Interval die we "recent" noemen, standaard 1 minuut
     *
     * @return bool
     */
    public function isRecentlyRefreshed(int $minutes = 1): bool
    {
        return null !== $this->timestampRefreshed && $this->timestampRefreshed->gt(Carbon::now()->subMinutes($minutes));
    }

    public function isRegisteredEmailRequested(): bool
    {
        return $this->registeredEmailRequested;
    }

    public function isRegisteredEmailSent(): bool
    {
        return null !== $this->registeredEmailSentAt;
    }

    /**
     * Conversie tracking is doorgegeven aan Twitter.
     *
     * @return bool
     */
    public function isTwitterConversionTracked(): bool
    {
        return $this->getAttribute(OrderAttribute::TWITTER_CONVERSION_TRACKED, false);
    }

    /**
     * @param Customer $customer
     */
    public function linkCustomer(Customer $customer)
    {
        $this->customer = $customer;
        $this->customer->addOrder($this);
    }

    /**
     * @param Document $document
     */
    public function linkDocument(Document $document)
    {
        $this->document = $document;
    }

    /**
     * @param Referrer $referrer
     */
    public function linkReferrer(Referrer $referrer)
    {
        if (null !== $this->referrer) {
            throw new \LogicException('There already is a Referrer linked to this Order');
        }

        $this->referrer = $referrer;
        $this->referrer->addOrder($this);

        $this->record(ReferrerOrderEvent::create($referrer->getId(), $this->getId()));
    }

    /**
     * @param Shop $shop
     */
    public function linkShop(Shop $shop)
    {
        if (null !== $this->shop) {
            throw new \InvalidArgumentException('This Order has already been linked to a Shop');
        }

        $this->shop = $shop;
        $this->shop->addOrder($this);
    }

    /**
     * @param QuestionStep $step
     */
    public function linkStep(QuestionStep $step)
    {
        if (!$this->getTemplate()->getQuestionSteps()->contains($step)) {
            throw new \InvalidArgumentException('The provided step does not belong to this document\'s template');
        }

        // Controleren of we niet al gewoon op deze stap zitten
        if (null !== $this->step && $step->getId()->equals($this->step->getId())) {
            return;
        }

        if (null !== $this->step) {
            $this->unlinkStep();
        }

        $this->step = $step;
        $this->step->addOrder($this);
    }

    /**
     * @param Subscription $subscription
     */
    public function linkSubscription(Subscription $subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * Bestelling vergrendelen.
     */
    public function lock()
    {
        if (OrderStatus::PENDING_PAYMENT != $this->status) {
            throw new InvalidStateException('An order has to have the PENDING_PAYMENT status before it can be locked');
        }

        $this->locked = true;
    }

    /**
     * @param Discount $discount
     */
    public function removeDiscount(Discount $discount)
    {
        if (!$this->discounts->contains($discount)) {
            return;
        }

        $this->discounts->removeElement($discount);

        $discount->removeOrder($this);
    }

    /**
     * @param InvoiceItem $item
     */
    public function removeInvoiceItem(InvoiceItem $item)
    {
        if (!$this->hasInvoiceItem($item)) {
            return;
        }

        $this->invoiceItems->removeElement($item);
    }

    /**
     * Handmatige controle aanvragen.
     */
    public function requestCheck()
    {
        $this->check = true;
    }

    /**
     * Maatwerk aanvragen.
     */
    public function requestCustom()
    {
        $this->custom = true;
    }

    public function requestRegisteredEmail(): void
    {
        $this->registeredEmailRequested = true;
    }

    public function sentRegisteredEmail(EmailAddress $address): void
    {
        $this->registeredEmailAddress = $address;
        $this->registeredEmailSentAt  = Carbon::now();
    }

    /**
     * Bestelling is verlaten.
     */
    public function setAbandoned()
    {
        $this->timestampAbandoned = Carbon::now();

        $this->record(OrderAbandonedEvent::create($this->id));
    }

    public function setUpsellMailSent()
    {
        $this->timestampUpsellMailSent = Carbon::now();
    }

    public function setReferralMailSent()
    {
        $this->timestampReferralMailSent = Carbon::now();
    }

    /**
     * @param bool $tracked
     */
    public function setAdwordsConversionTracked(bool $tracked)
    {
        $this->setAttribute(OrderAttribute::ADWORDS_CONVERSION_TRACKED, $tracked);
    }

    /**
     * @param bool $tracked
     */
    public function setGoogleConversionTracked(bool $tracked)
    {
        $this->setAttribute(OrderAttribute::GOOGLE_CONVERSION_TRACKED, $tracked);
    }

    /**
     * @param PaymentMethod|null $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @param OrderStatus $status
     */
    public function setStatus(OrderStatus $status)
    {
        if ($this->status == $status) {
            return;
        }

        OrderStatusChange::create($this, $status);

        // Vragenlijst is ingevuld
        if (null !== $this->status && OrderStatus::STEP_QUESTIONS == $this->status && null === $this->timestampAnswered) {
            $this->timestampAnswered = Carbon::now();
        }

        $this->status = $status;

        switch ($this->status) {
            case OrderStatus::STEP_QUESTIONS:
                if (null === $this->timestampStarted) {
                    $this->timestampStarted = Carbon::now();
                }

                break;

            case OrderStatus::MANUAL_CHECK:
                $this->record(OrderPendingManualCheckEvent::create($this->id));

                break;

            case OrderStatus::MANUAL_CUSTOM:
                $this->record(OrderPendingManualCustomEvent::create($this->id));

                break;

            case OrderStatus::REQUEUE:
                $this->record(OrderRequeuedEvent::create($this->id));

                break;

            case OrderStatus::COMPLETED:
                $this->timestampDelivered = Carbon::now();
                $this->record(OrderCompletedEvent::create($this->id));

                break;

            default:
                break;
        }
    }

    /**
     * @param string $value
     */
    public function setStatusString(string $value)
    {
        $this->setStatus(OrderStatus::from($value));
    }

    /**
     * @param bool $tracked
     */
    public function setTwitterConversionTracked(bool $tracked)
    {
        $this->setAttribute(OrderAttribute::TWITTER_CONVERSION_TRACKED, $tracked);
    }

    /**
     * @param OrderType $type
     */
    public function setType(OrderType $type)
    {
        $this->type = $type;
    }

    public function unlinkCustomer()
    {
        if (null !== $this->customer) {
            $this->customer->removeOrder($this);
            $this->customer = null;
        }
    }

    public function unlinkDocument()
    {
        $this->document = null;
    }

    /**
     * Referrer ontkoppelen.
     */
    public function unlinkReferrer()
    {
        if (null === $this->referrer) {
            return;
        }

        $reference = $this->referrer;

        $this->referrer = null;
        $reference->removeOrder($this);
    }

    public function unlinkShop()
    {
        if (null === $this->shop) {
            return;
        }

        $reference = $this->shop;

        $this->shop = null;
        $reference->removeOrder($this);
    }

    /**
     * Order loskoppelen van bepaalde stap uit de vragenlijst.
     */
    public function unlinkStep()
    {
        if (null !== $this->step) {
            $this->step->removeOrder($this);
            $this->step = null;
        }
    }

    public function unlinkSubscription()
    {
        $this->subscription = null;
    }

    /**
     * Tijdstip waarop we voor het laatst een RefreshOrderCommand hebben uitgevoerd op deze bestelling.
     */
    public function updateTimestampRefreshed()
    {
        $this->timestampRefreshed = Carbon::now();
    }

    /**
     * @param string     $name
     * @param mixed|null $default
     *
     * @return mixed|null
     */
    private function getAttribute(string $name, $default = null)
    {
        return $this->attributes->getValue($name, $default);
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    private function hasAttribute(string $name): bool
    {
        return $this->attributes->has($name);
    }

    /**
     * @param Subscription $issuingSubscription
     */
    private function linkIssuingSubscription(Subscription $issuingSubscription)
    {
        $this->issuingSubscription = $issuingSubscription;
    }

    /**
     * @param Order $previousOrder
     */
    private function linkPreviousOrder(self $previousOrder)
    {
        $this->previousOrder = $previousOrder;
    }

    /**
     * @param string $name
     */
    private function removeAttribute(string $name)
    {
        $this->attributes = clone $this->attributes;
        $this->attributes->remove($name);
    }

    /**
     * @param string $name
     * @param mixed  $value
     */
    private function setAttribute(string $name, $value)
    {
        $this->attributes = clone $this->attributes;
        $this->attributes->setValue($name, $value);
    }
}
