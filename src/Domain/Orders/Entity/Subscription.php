<?php

namespace NnShop\Domain\Orders\Entity;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Core\Annotation\Doctrine as Core;
use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\Common\Exception\NotImplementedException;
use Core\Domain\Traits\SoftDeleteableEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Selectable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NnShop\Domain\Orders\Enumeration\SubscriptionLinkFormat;
use NnShop\Domain\Orders\Event\SubscriptionWasCanceled;
use NnShop\Domain\Orders\Value\SubscriptionCode;
use NnShop\Domain\Orders\Value\SubscriptionId;
use NnShop\Domain\Templates\Entity\Template;
use SimpleBus\Message\Recorder\ContainsRecordedMessages;
use SimpleBus\Message\Recorder\PrivateMessageRecorderCapabilities;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Orders\Repository\SubscriptionDoctrineRepository")
 * @Core\QueryBuilder(class="NnShop\Infrastructure\Orders\QueryBuilder\SubscriptionDoctrineQueryBuilder")
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(name="ix_customer", columns={"customer_id", "timestamp_deleted"}),
 *     @ORM\Index(name="ix_expiration", columns={"timestamp_expires", "timestamp_deleted"}),
 *     @ORM\Index(name="ix_order", columns={"order_id", "timestamp_deleted"}),
 *     @ORM\Index(name="ix_template", columns={"template_id", "timestamp_deleted"}),
 *     @ORM\Index(name="ix_deleted", columns={"timestamp_deleted", "timestamp_created"})
 * })
 *
 * @Gedmo\SoftDeleteable(fieldName="timestampDeleted")
 */
class Subscription implements ContainsRecordedMessages
{
    use PrivateMessageRecorderCapabilities;
    use SoftDeleteableEntityTrait;

    /**
     * Afkoelperiode, in dagen.
     */
    const GRACE_PERIOD_DAYS = 7;

    /**
     * @ORM\Column(type="orders.subscription_code", unique=true)
     *
     * @var SubscriptionCode
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Orders\Entity\Customer", inversedBy="subscriptions")
     * @ORM\JoinColumn(referencedColumnName="customer_id", nullable=false)
     *
     * @var Customer
     */
    private $customer;

    /**
     * @ORM\Id
     * @ORM\Column(type="orders.subscription_id")
     *
     * @var SubscriptionId
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\SubscriptionLink", mappedBy="subscription", cascade={"persist"}, orphanRemoval=true)
     *
     * @var Collection|SubscriptionLink[]
     */
    private $links;

    /**
     * @ORM\OneToOne(targetEntity="\NnShop\Domain\Orders\Entity\Order", inversedBy="subscription")
     * @ORM\JoinColumn(referencedColumnName="order_id", nullable=false)
     *
     * @var Order
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Template", inversedBy="subscriptions")
     * @ORM\JoinColumn(referencedColumnName="template_id", nullable=false)
     *
     * @var Template
     */
    private $template;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon|null
     */
    private $timestampCanceled;

    /**
     * @ORM\Column(type="core.time.datetime")
     *
     * @var Carbon
     */
    private $timestampCreated;

    /**
     * @ORM\Column(type="core.time.datetime")
     *
     * @var Carbon
     */
    private $timestampExpires;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\Order", mappedBy="issuingSubscription")
     * @ORM\OrderBy({"timestampCreated": "DESC"})
     *
     * @var Collection|Order[]
     */
    private $updateOrders;

    /**
     * Subscription constructor.
     *
     * @param SubscriptionId   $id
     * @param SubscriptionCode $code
     * @param Order            $order
     */
    private function __construct(SubscriptionId $id, SubscriptionCode $code, Order $order)
    {
        $this->id = $id;
        $this->code = $code;

        $this->customer = $order->getCustomer();
        $this->customer->addSubscription($this);

        $this->template = $order->getTemplate();
        $this->template->addSubscription($this);

        $this->order = $order;
        $this->order->linkSubscription($this);

        $this->links = new ArrayCollection();
        $this->updateOrders = new ArrayCollection();
    }

    /**
     * @param SubscriptionLink $link
     */
    public function addLink(SubscriptionLink $link)
    {
        if ($this->hasLink($link)) {
            throw new DuplicateEntityException();
        }

        $this->links->add($link);
    }

    /**
     * Cancel the subscription.
     */
    public function cancel()
    {
        if ($this->isCanceled()) {
            throw new \DomainException();
        }

        $this->setTimestampCanceled(Carbon::now());

        $this->record(new SubscriptionWasCanceled($this->id));
    }

    /**
     * Detach the subscription from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->customer) {
            $this->customer->removeSubscription($this);
            $this->customer = null;
        }

        if (null !== $this->template) {
            $this->template->removeSubscription($this);
            $this->template = null;
        }

        if (null !== $this->order) {
            $this->order->unlinkSubscription();
            $this->order = null;
        }
    }

    /**
     * Abonnement verlengen *zonder* factuur.
     *
     * @param CarbonInterval $interval
     */
    public function extend(CarbonInterval $interval)
    {
        if (null === $this->timestampExpires) {
            $this->timestampExpires = Carbon::instance($this->timestampCreated->add($interval))->setTime(0, 0);
        } else {
            $this->timestampExpires = Carbon::instance($this->timestampExpires->add($interval))->setTime(0, 0);
        }
    }

    /**
     * @param SubscriptionLinkFormat $format
     *
     * @return SubscriptionLink|null
     */
    public function findLink(SubscriptionLinkFormat $format)
    {
        $matches = $this->links->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('format', $format)
            )
        );

        return $matches->isEmpty() ? null : $matches->first();
    }

    /**
     * @return SubscriptionCode
     */
    public function getCode(): SubscriptionCode
    {
        return $this->code;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * Definitieve afsluitdatum, na de afkoelperiode.
     *
     * @return Carbon
     */
    public function getGracePeriodTimestamp(): Carbon
    {
        return $this->timestampExpires->copy()->addDays(self::GRACE_PERIOD_DAYS);
    }

    /**
     * @return SubscriptionId
     */
    public function getId(): SubscriptionId
    {
        return $this->id;
    }

    /**
     * Gets the most recent update order based on this subscription.
     *
     * @return Order
     */
    public function getLatestUpdateOrder()
    {
        if ($this->updateOrders->isEmpty()) {
            return;
        }

        return $this->updateOrders->first();
    }

    /**
     * @return Collection|SubscriptionLink[]
     */
    public function getLinks(): Collection
    {
        return $this->links;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->template;
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampCanceled()
    {
        return $this->timestampCanceled;
    }

    /**
     * @return Carbon
     */
    public function getTimestampCreated(): Carbon
    {
        return $this->timestampCreated;
    }

    /**
     * @return Carbon
     */
    public function getTimestampExpires(): Carbon
    {
        return $this->timestampExpires;
    }

    /**
     * @return Collection|Selectable|Order[]
     */
    public function getUpdateOrders(): Collection
    {
        return $this->updateOrders;
    }

    /**
     * @param SubscriptionLink $link
     *
     * @return bool
     */
    public function hasLink(SubscriptionLink $link): bool
    {
        return $this->links->contains($link);
    }

    /**
     * @return bool
     */
    public function isCanceled(): bool
    {
        return null !== $this->timestampCanceled;
    }

    /**
     * Abonnement is verlopen.
     *
     * @return bool
     */
    public function isExpired(): bool
    {
        return $this->timestampExpires->lt(Carbon::today());
    }

    /**
     * Abonnement zit in de afkoelperiode.
     *
     * @return bool
     */
    public function isInGracePeriod(): bool
    {
        return $this->isExpired() && $this->getGracePeriodTimestamp()->gte(Carbon::today());
    }

    /**
     * @param SubscriptionId   $id
     * @param SubscriptionCode $code
     * @param Order            $order
     *
     * @return Subscription
     */
    public static function prepare(SubscriptionId $id, SubscriptionCode $code, Order $order): self
    {
        $subscription = new self($id, $code, $order);

        $subscription->timestampCreated = Carbon::now();
        $subscription->extend(CarbonInterval::year());

        return $subscription;
    }

    /**
     * @param SubscriptionLink $link
     */
    public function removeLink(SubscriptionLink $link)
    {
        if (!$this->hasLink($link)) {
            return;
        }

        $this->links->removeElement($link);
    }

    /**
     * Abonnement verlengen met factuur.
     *
     * @param int $years
     */
    public function renew(int $years)
    {
        $this->extend(CarbonInterval::years($years));

        throw new NotImplementedException();
    }

    /**
     * @param Carbon $timestampCanceled
     */
    private function setTimestampCanceled(Carbon $timestampCanceled)
    {
        $this->timestampCanceled = $timestampCanceled;
    }
}
