<?php

namespace NnShop\Domain\Orders\Entity;

use Core\Annotation\Doctrine as Core;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NnShop\Domain\Orders\Enumeration\OrderItemType;
use NnShop\Domain\Orders\Value\InvoiceItemId;
use Money\Currency;
use Money\Money;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Orders\Repository\InvoiceItemDoctrineRepository")
 * @Core\QueryBuilder(class="NnShop\Infrastructure\Orders\QueryBuilder\InvoiceItemDoctrineQueryBuilder")
 */
class InvoiceItem
{
    /**
     * @ORM\Id
     * @ORM\Column(type="orders.invoice_item_id")
     *
     * @var InvoiceItemId
     */
    private $id;

    /**
     * @Gedmo\SortableGroup
     *
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Orders\Entity\Invoice", inversedBy="items")
     * @ORM\JoinColumn(referencedColumnName="invoice_id", nullable=false)
     *
     * @var Invoice
     */
    private $invoice;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Orders\Entity\Order", inversedBy="invoiceItems")
     * @ORM\JoinColumn(referencedColumnName="order_id")
     *
     * @var Order
     */
    private $order;

    /**
     * @ORM\Column(type="integer")
     * @Gedmo\SortablePosition
     *
     * @var int
     */
    private $position;

    /**
     * @ORM\Column(type="core.financial.money")
     *
     * @var Money
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $quantity;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="orders.order_item_type")
     *
     * @var OrderItemType
     */
    private $type;

    /**
     * InvoiceItem constructor.
     *
     * @param InvoiceItemId $id
     * @param Invoice       $invoice
     *
     * @throws \Exception
     */
    private function __construct(InvoiceItemId $id, Invoice $invoice)
    {
        $this->id = $id;

        $this->invoice = $invoice;
        $this->invoice->addItem($this);
    }

    /**
     * @param InvoiceItemId $id
     * @param Invoice       $invoice
     * @param int           $quantity
     * @param Money         $price
     *
     * @throws \Exception
     *
     * @return InvoiceItem
     */
    public static function create(InvoiceItemId $id, Invoice $invoice, int $quantity, Money $price)
    {
        $item = new self($id, $invoice);
        $item->setQuantity($quantity);
        $item->setPrice($price);

        return $item;
    }

    /**
     * Detach the invoice item from its associated entities.
     *
     * @throws \Exception
     */
    public function detach()
    {
        if (null !== $this->invoice) {
            $this->invoice->removeItem($this);
            $this->invoice = null;
        }

        if (null !== $this->order) {
            $this->order->removeItem($this);
            $this->order = null;
        }
    }

    /**
     * @return InvoiceItemId
     */
    public function getId(): InvoiceItemId
    {
        return $this->id;
    }

    /**
     * @return Invoice
     */
    public function getInvoice(): Invoice
    {
        return $this->invoice;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @return Money
     */
    public function getPrice(): Money
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return Money
     */
    public function getTotal(): Money
    {
        if (null === $this->price || null === $this->quantity) {
            return new Money(0, new Currency('EUR'));
        }

        return $this->price->multiply($this->quantity);
    }

    /**
     * @return bool
     */
    public function hasOrder(): bool
    {
        return null !== $this->order;
    }

    /**
     * @param Order $order
     */
    public function linkOrder(Order $order)
    {
        $this->order = $order;
        if (!$this->invoice->isProforma()) {
            $this->order->addInvoiceItem($this);
        }
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position)
    {
        $this->position = $position;
    }

    /**
     * @param Money $price
     *
     * @throws \Exception
     */
    public function setPrice(Money $price)
    {
        $this->price = $price;

        if (null !== !$this->invoice && null !== $this->quantity) {
            $this->invoice->recalculate();
        }
    }

    /**
     * @param int $quantity
     *
     * @throws \Exception
     */
    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;

        if (null !== !$this->invoice && null !== $this->price) {
            $this->invoice->recalculate();
        }
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function unlinkOrder()
    {
        if (!$this->invoice->isProforma()) {
            $this->order->removeInvoiceItem($this);
        }

        $this->order = null;
    }

    /**
     * @return OrderItemType|null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param OrderItemType $type
     *
     * @return InvoiceItem
     */
    public function setType(OrderItemType $type)
    {
        $this->type = $type;

        return $this;
    }
}
