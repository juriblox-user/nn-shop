<?php

namespace NnShop\Domain\Orders\Entity;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use NnShop\Domain\Orders\Event\RefundWasProcessed;
use NnShop\Domain\Orders\Event\RefundWasRequested;
use NnShop\Domain\Orders\Value\RefundId;
use SimpleBus\Message\Recorder\ContainsRecordedMessages;
use SimpleBus\Message\Recorder\PrivateMessageRecorderCapabilities;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Orders\Repository\RefundDoctrineRepository")
 */
class Refund implements ContainsRecordedMessages
{
    use PrivateMessageRecorderCapabilities;

    /**
     * @ORM\Id
     * @ORM\Column(type="orders.refund_id")
     *
     * @var RefundId
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="\NnShop\Domain\Orders\Entity\Payment", mappedBy="refund")
     *
     * @var Payment
     */
    private $payment;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon
     */
    private $processedTimestamp;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string
     */
    private $reason;

    /**
     * @ORM\Column(type="core.time.datetime")
     *
     * @var Carbon
     */
    private $requestedTimestamp;

    /**
     * Refund constructor.
     *
     * @param RefundId $id
     * @param Payment  $payment
     */
    private function __construct(RefundId $id, Payment $payment)
    {
        $this->id = $id;

        $this->payment = $payment;
        $this->payment->linkRefund($this);
    }

    /**
     * Detach the refund from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->payment) {
            $this->payment->unlinkRefund();
            $this->payment = null;
        }
    }

    /**
     * @return RefundId
     */
    public function getId(): RefundId
    {
        return $this->id;
    }

    /**
     * @return Payment
     */
    public function getPayment(): Payment
    {
        return $this->payment;
    }

    /**
     * @return Carbon
     */
    public function getProcessedTimestamp(): Carbon
    {
        return $this->processedTimestamp;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @return Carbon
     */
    public function getRequestedTimestamp(): Carbon
    {
        return $this->requestedTimestamp;
    }

    /**
     * @return bool
     */
    public function isProcessed(): bool
    {
        return null !== $this->processedTimestamp;
    }

    public function processed()
    {
        $this->processedTimestamp = Carbon::now();

        $this->record(new RefundWasProcessed($this->id));
    }

    /**
     * @param RefundId $id
     * @param Payment  $payment
     *
     * @return Refund
     */
    public static function request(RefundId $id, Payment $payment)
    {
        $refund = new self($id, $payment);
        $refund->setRequestedTimestamp(Carbon::now());

        $refund->record(new RefundWasRequested($refund->id));

        return $refund;
    }

    /**
     * @param string $reason
     */
    public function setReason(string $reason)
    {
        $this->reason = $reason;
    }

    /**
     * @param Carbon $timestamp
     */
    private function setRequestedTimestamp(Carbon $timestamp)
    {
        $this->requestedTimestamp = $timestamp;
    }
}
