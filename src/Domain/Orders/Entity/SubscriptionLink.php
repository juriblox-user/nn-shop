<?php

namespace NnShop\Domain\Orders\Entity;

use Doctrine\ORM\Mapping as ORM;
use NnShop\Domain\Orders\Enumeration\SubscriptionLinkFormat;
use NnShop\Domain\Orders\Value\SubscriptionLinkId;
use NnShop\Domain\Orders\Value\SubscriptionLinkToken;
use NnShop\Domain\Templates\Entity\Document;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Orders\Repository\SubscriptionLinkDoctrineRepository")
 */
class SubscriptionLink
{
    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Templates\Entity\Document", inversedBy="subscriptionLinks")
     * @ORM\JoinColumn(referencedColumnName="document_id", nullable=false)
     *
     * @var Document
     */
    private $document;

    /**
     * @ORM\Column(type="orders.subscription_link_format")
     *
     * @var SubscriptionLinkFormat
     */
    private $format;

    /**
     * @ORM\Id
     * @ORM\Column(type="orders.subscription_link_id")
     *
     * @var SubscriptionLinkId
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Orders\Entity\Subscription", inversedBy="links")
     * @ORM\JoinColumn(referencedColumnName="subscription_id", nullable=false)
     *
     * @var Subscription
     */
    private $subscription;

    /**
     * @ORM\Column(type="orders.subscription_link_token", unique=true)
     *
     * @var SubscriptionLinkToken
     */
    private $token;

    /**
     * SubscriptionLink constructor.
     *
     * @param SubscriptionLinkId $id
     * @param Subscription       $subscription
     * @param Document           $document
     */
    private function __construct(SubscriptionLinkId $id, Subscription $subscription, Document $document)
    {
        $this->id = $id;

        $this->subscription = $subscription;
        $this->subscription->addLink($this);

        $this->linkDocument($document);
    }

    /**
     * @param SubscriptionLinkId     $id
     * @param SubscriptionLinkFormat $format
     * @param SubscriptionLinkToken  $token
     * @param Subscription           $subscription
     * @param Document               $document
     *
     * @return SubscriptionLink
     */
    public static function create(SubscriptionLinkId $id, SubscriptionLinkFormat $format, SubscriptionLinkToken $token, Subscription $subscription, Document $document): self
    {
        $link = new static($id, $subscription, $document);
        $link->format = $format;
        $link->token = $token;

        return $link;
    }

    /**
     * Entiteit loskoppelen.
     */
    public function detach()
    {
        if (null !== $this->document) {
            $reference = $this->document;

            $this->document = null;
            $reference->removeSubscriptionLink($this);
        }

        if (null !== $this->subscription) {
            $reference = $this->subscription;

            $this->subscription = null;
            $reference->removeLink($this);
        }
    }

    /**
     * @return Document
     */
    public function getDocument(): Document
    {
        return $this->document;
    }

    /**
     * @return SubscriptionLinkFormat
     */
    public function getFormat(): SubscriptionLinkFormat
    {
        return $this->format;
    }

    /**
     * @return SubscriptionLinkId
     */
    public function getId(): SubscriptionLinkId
    {
        return $this->id;
    }

    /**
     * @return Subscription
     */
    public function getSubscription(): Subscription
    {
        return $this->subscription;
    }

    /**
     * @return SubscriptionLinkToken
     */
    public function getToken(): SubscriptionLinkToken
    {
        return $this->token;
    }

    /**
     * @param Document $document
     */
    public function linkDocument(Document $document)
    {
        if (null !== $this->document && $document->getId() == $this->document->getId()) {
            return;
        }

        if (null !== $this->document) {
            $this->document->removeSubscriptionLink($this);
        }

        $this->document = $document;
        $this->document->addSubscriptionLink($this);
    }
}
