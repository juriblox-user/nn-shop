<?php

namespace NnShop\Domain\Orders\Entity;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use NnShop\Domain\Orders\Enumeration\DiscountType;
use Money\Money;

/**
 * @ORM\Entity
 */
class InvoiceDiscount
{
    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Orders\Entity\Discount", inversedBy="invoiceLinks")
     * @ORM\JoinColumn(referencedColumnName="discount_id", nullable=false)
     *
     * @var Discount
     */
    private $discount;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="link_id")
     * @ORM\GeneratedValue
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Orders\Entity\Invoice", inversedBy="discountLinks")
     * @ORM\JoinColumn(referencedColumnName="invoice_id", nullable=false)
     *
     * @var Invoice
     */
    private $invoice;

    /**
     * @ORM\Column(type="core.time.datetime")
     *
     * @var Carbon
     */
    private $timestamp;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     *
     * @var int
     */
    private $percentage;

    /**
     * @ORM\Column(type="core.financial.money", nullable=true)
     *
     * @var Money
     */
    private $amount;

    /**
     * @ORM\Column(type="orders.discount_type")
     *
     * @var DiscountType
     */
    private $type;

    /**
     * InvoiceDiscount constructor.
     *
     * @param Invoice  $invoice
     * @param Discount $discount
     *
     * @throws \Core\Common\Exceptions\MissingValueException
     */
    private function __construct(Invoice $invoice, Discount $discount)
    {
        $this->invoice = $invoice;
        $this->discount = $discount;
        $this->type = $discount->getType();

        if ($this->type->is(DiscountType::PERCENTAGE)) {
            $this->percentage = $discount->getPercentage();
        } elseif ($this->type->is(DiscountType::FIXED)) {
            $this->amount = $discount->getAmount();
        }

        $this->invoice->addDiscountLink($this);

        // Let op, bij proforma facturen niet terugkoppelen
        if (!$invoice->isProforma()) {
            $this->discount->addInvoiceLink($this);
        }
    }

    /**
     * @param Invoice  $invoice
     * @param Discount $discount
     *
     * @throws \Core\Common\Exceptions\MissingValueException
     *
     * @return InvoiceDiscount
     */
    public static function create(Invoice $invoice, Discount $discount): self
    {
        $link = new self($invoice, $discount);
        $link->setTimestamp(Carbon::now());

        return $link;
    }

    /**
     * Detach the link from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->invoice) {
            $this->invoice->removeDiscountLink($this);
            $this->invoice = null;
        }

        if (null !== $this->discount) {
            $this->discount->removeInvoiceLink($this);
            $this->discount = null;
        }
    }

    /**
     * @return Discount
     */
    public function getDiscount(): Discount
    {
        return $this->discount;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Invoice
     */
    public function getInvoice(): Invoice
    {
        return $this->invoice;
    }

    /**
     * @return Carbon
     */
    public function getTimestamp(): Carbon
    {
        return $this->timestamp;
    }

    /**
     * @return float
     */
    public function getMultiplier(): float
    {
        return $this->getPercentage() / 100;
    }

    /**
     * @return int
     */
    public function getPercentage(): int
    {
        return $this->percentage;
    }

    /**
     * @return Money
     */
    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * @return DiscountType
     */
    public function getType(): DiscountType
    {
        return $this->type;
    }

    /**
     * @param Carbon $timestamp
     */
    private function setTimestamp(Carbon $timestamp)
    {
        $this->timestamp = $timestamp;
    }
}
