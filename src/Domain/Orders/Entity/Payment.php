<?php

namespace NnShop\Domain\Orders\Entity;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use NnShop\Domain\Mollie\Value\TransactionId;
use NnShop\Domain\Orders\Enumeration\PaymentMethod;
use NnShop\Domain\Orders\Enumeration\PaymentStatus;
use NnShop\Domain\Orders\Event\PaymentCanceledEvent;
use NnShop\Domain\Orders\Event\PaymentExpiredEvent;
use NnShop\Domain\Orders\Event\PaymentReceivedEvent;
use NnShop\Domain\Orders\Value\PaymentId;
use NnShop\Domain\Orders\Value\RefundId;
use Money\Money;
use SimpleBus\Message\Recorder\ContainsRecordedMessages;
use SimpleBus\Message\Recorder\PrivateMessageRecorderCapabilities;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Orders\Repository\PaymentDoctrineRepository")
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(name="ix_invoice", columns={"invoice_id", "payment_status"}),
 *     @ORM\Index(name="ix_status", columns={"payment_status"})
 * })
 */
class Payment implements ContainsRecordedMessages
{
    use PrivateMessageRecorderCapabilities;

    /**
     * @ORM\Column(type="core.financial.money")
     *
     * @var Money
     */
    private $amount;

    /**
     * @ORM\Id
     * @ORM\Column(type="orders.payment_id")
     *
     * @var PaymentId
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Orders\Entity\Invoice", inversedBy="payments")
     * @ORM\JoinColumn(referencedColumnName="invoice_id", nullable=false)
     *
     * @var Invoice
     */
    private $invoice;

    /**
     * @ORM\Column(type="orders.payment_method", nullable=true)
     *
     * @var PaymentMethod
     */
    private $method;

    /**
     * @ORM\OneToOne(targetEntity="\NnShop\Domain\Orders\Entity\Refund", inversedBy="payment", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(referencedColumnName="refund_id")
     *
     * @var Refund
     */
    private $refund;

    /**
     * @ORM\Column(type="orders.payment_status")
     *
     * @var PaymentStatus
     */
    private $status;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon
     */
    private $timestampCanceled;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon
     */
    private $timestampExpired;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon
     */
    private $timestampReceived;

    /**
     * @ORM\Column(type="core.time.datetime")
     *
     * @var Carbon
     */
    private $timestampRequested;

    /**
     * @ORM\Column(type="mollie.transaction_id")
     *
     * @var TransactionId
     */
    private $transactionId;

    /**
     * Payment constructor.
     *
     * @param PaymentId $id
     * @param Invoice   $invoice
     * @param Money     $amount
     */
    private function __construct(PaymentId $id, Invoice $invoice, Money $amount)
    {
        $this->id = $id;
        $this->amount = $amount;

        $this->invoice = $invoice;
        $this->invoice->addPayment($this);
    }

    /**
     * Detach the payment from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->invoice) {
            $this->invoice->removePayment($this);
            $this->invoice = null;
        }
    }

    /**
     * @return Money
     */
    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * @return PaymentId
     */
    public function getId(): PaymentId
    {
        return $this->id;
    }

    /**
     * @return Invoice
     */
    public function getInvoice(): Invoice
    {
        return $this->invoice;
    }

    /**
     * @return PaymentMethod|null
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return Refund
     */
    public function getRefund(): Refund
    {
        return $this->refund;
    }

    /**
     * @return PaymentStatus
     */
    public function getStatus(): PaymentStatus
    {
        return $this->status;
    }

    /**
     * @return Carbon
     */
    public function getTimestampCanceled(): Carbon
    {
        return $this->timestampCanceled;
    }

    /**
     * @return Carbon
     */
    public function getTimestampExpired(): Carbon
    {
        return $this->timestampExpired;
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampReceived()
    {
        return $this->timestampReceived;
    }

    /**
     * @return Carbon
     */
    public function getTimestampRequested(): Carbon
    {
        return $this->timestampRequested;
    }

    /**
     * @return TransactionId
     */
    public function getTransactionId(): TransactionId
    {
        return $this->transactionId;
    }

    /**
     * Bepalen of de betaling is ontvangen.
     *
     * @return bool
     */
    public function isReceived(): bool
    {
        return null !== $this->timestampReceived;
    }

    /**
     * @param Refund $refund
     */
    public function linkRefund(Refund $refund)
    {
        $this->refund = $refund;
    }

    /**
     * Betaling voorbereiden/klaarzetten.
     *
     * @param PaymentId $id
     * @param Invoice   $invoice
     *
     * @return Payment
     */
    public static function prepare(PaymentId $id, Invoice $invoice): self
    {
        $payment = new self($id, $invoice, $invoice->getTotal());
        $payment->setStatus(PaymentStatus::from(PaymentStatus::PENDING));
        $payment->timestampRequested = Carbon::now();

        return $payment;
    }

    /**
     * @return Refund
     */
    public function requestRefund(): Refund
    {
        if (null !== $this->refund) {
            throw new \DomainException('There already is a (pending) refund for this payment');
        }

        if ($this->status->isNot(PaymentStatus::RECEIVED)) {
            throw new \DomainException('You cannot request a refund for a payment that has not yet been received');
        }

        return Refund::request(RefundId::generate(), $this);
    }

    /**
     * @param PaymentMethod $method
     */
    public function setMethod(PaymentMethod $method)
    {
        $this->method = $method;
    }

    /**
     * @param PaymentStatus $status
     * @param Carbon        $dateTime
     */
    public function setStatus(PaymentStatus $status, Carbon $dateTime = null)
    {
        if ($status == $this->status) {
            return;
        }

        switch ($status) {
            case PaymentStatus::PENDING:
                if (null !== $this->status) {
                    throw new \LogicException('The payment\'s PaymentStatus cannot be reverted to PENDING');
                }

                break;

            case PaymentStatus::RECEIVED:
                if ($this->status->isNot(PaymentStatus::PENDING)) {
                    throw new \LogicException('The payment\'s PaymentStatus should be PENDING');
                }

                $this->timestampReceived = $dateTime ?: Carbon::now();
                $this->record(PaymentReceivedEvent::create($this->id));

                break;

            case PaymentStatus::EXPIRED:
                if ($this->status->isNot(PaymentStatus::PENDING)) {
                    throw new \LogicException('The payment\'s PaymentStatus should be PENDING');
                }

                $this->timestampExpired = $dateTime ?: Carbon::now();
                $this->record(PaymentExpiredEvent::create($this->id));

                break;

            case PaymentStatus::CANCELED:
                if ($this->status->isNot(PaymentStatus::PENDING)) {
                    throw new \LogicException('The payment\'s PaymentStatus should be PENDING');
                }

                $this->timestampCanceled = $dateTime ?: Carbon::now();
                $this->record(PaymentCanceledEvent::create($this->id));

                break;

            default:
                throw new \InvalidArgumentException('Invalid status "%s"', $status->getValue());
        }

        $this->status = $status;
    }

    /**
     * @param TransactionId $transactionId
     */
    public function setTransactionId(TransactionId $transactionId)
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @internal
     */
    public function unlinkRefund()
    {
        $this->refund = null;
    }
}
