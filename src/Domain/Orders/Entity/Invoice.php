<?php

namespace NnShop\Domain\Orders\Entity;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Core\Annotation\Doctrine as Core;
use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\Common\Exception\Domain\EntityNotFoundException;
use Core\Common\Exception\NotImplementedException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use NnShop\Domain\Orders\Enumeration\DiscountType;
use NnShop\Domain\Orders\Enumeration\InvoiceStatus;
use NnShop\Domain\Orders\Enumeration\OrderItemType;
use NnShop\Domain\Orders\Enumeration\OrderType;
use NnShop\Domain\Orders\Enumeration\PaymentMethod;
use NnShop\Domain\Orders\Enumeration\PaymentStatus;
use NnShop\Domain\Orders\Event\InvoiceCreditedEvent;
use NnShop\Domain\Orders\Event\InvoiceIssuedEvent;
use NnShop\Domain\Orders\Event\InvoicePaidEvent;
use NnShop\Domain\Orders\Event\InvoiceRevokedEvent;
use NnShop\Domain\Orders\Event\OrderPaidEvent;
use NnShop\Domain\Orders\Value\InvoiceCode;
use NnShop\Domain\Orders\Value\InvoiceId;
use NnShop\Domain\Orders\Value\InvoiceItemId;
use NnShop\Domain\Orders\Value\PaymentId;
use NnShop\Domain\Translation\Entity\Profile;
use Money\Currency;
use Money\Money;
use SimpleBus\Message\Recorder\ContainsRecordedMessages;
use SimpleBus\Message\Recorder\PrivateMessageRecorderCapabilities;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Orders\Repository\InvoiceDoctrineRepository")
 * @Core\QueryBuilder(class="NnShop\Infrastructure\Orders\QueryBuilder\InvoiceDoctrineQueryBuilder")
 *
 * @ORM\Table(
 *     indexes={
 *         @ORM\Index(name="ix_code_year", columns={"code_year"}),
 *         @ORM\Index(name="ix_code_number", columns={"code_number"})
 *     },
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="uq_code", columns={"code_year", "code_number"})
 *     }
 * )
 */
class Invoice implements ContainsRecordedMessages
{
    use PrivateMessageRecorderCapabilities;

    /**
     * 21% BTW.
     */
    const DEFAULT_VAT_PERCENTAGE = 21;

    /**
     * @ORM\Embedded(class="NnShop\Domain\Orders\Value\InvoiceCode")
     *
     * @var InvoiceCode
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Orders\Entity\Customer", inversedBy="invoices")
     * @ORM\JoinColumn(referencedColumnName="customer_id", nullable=false)
     *
     * @var Customer
     */
    private $customer;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\InvoiceDiscount", mappedBy="invoice", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @var Collection|InvoiceDiscount[]
     */
    private $discountLinks;

    /**
     * @ORM\Id
     * @ORM\Column(type="orders.invoice_id")
     *
     * @var InvoiceId
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\InvoiceItem", mappedBy="invoice", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position": "ASC"})
     *
     * @var Collection|InvoiceItem[]
     */
    private $items;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\Payment", mappedBy="invoice", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @var Collection|Payment[]
     */
    private $payments;

    /**
     * @var bool
     */
    private $proforma;

    /**
     * @ORM\Column(type="orders.invoice_status")
     *
     * @var InvoiceStatus
     */
    private $status;

    /**
     * @ORM\Column(type="core.financial.money")
     *
     * @var Money
     */
    private $subtotal;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon
     */
    private $timestampCredited;

    /**
     * @ORM\Column(type="core.time.datetime")
     *
     * @var Carbon
     */
    private $timestampDue;

    /**
     * @ORM\Column(type="core.time.datetime")
     *
     * @var Carbon
     */
    private $timestampIssued;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon
     */
    private $timestampPaid;

    /**
     * @ORM\Column(type="core.time.datetime", nullable=true)
     *
     * @var Carbon
     */
    private $timestampRevoked;

    /**
     * @ORM\Column(type="core.financial.money")
     *
     * @var Money
     */
    private $total;

    /**
     * @ORM\Column(type="core.financial.money")
     *
     * @var Money
     */
    private $vatAmount;

    /**
     * @ORM\Column(type="smallint")
     *
     * @var int
     */
    private $vatPercentage;

    /**
     * @var Profile
     * @ORM\ManyToOne(targetEntity="NnShop\Domain\Translation\Entity\Profile", inversedBy="invoices")
     * @ORM\JoinColumn(name="profile_id", referencedColumnName="profile_id", onDelete="SET NULL")
     */
    private $profile;

    /**
     * Invoice constructor.
     *
     * @param InvoiceId $id
     * @param Customer  $customer
     * @param bool      $proforma
     * @param Profile   $profile
     *
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    private function __construct(InvoiceId $id, Customer $customer, $proforma = false, Profile $profile)
    {
        $this->id = $id;
        $this->proforma = $proforma;

        // Voor proforma facturen: harcoded InvoiceCode en PROFORMA status
        if ($this->proforma) {
            $this->code = InvoiceCode::from(0, 0);
            $this->status = InvoiceStatus::from(InvoiceStatus::PROFORMA);
        }

        // Standaard een PENDING status
        else {
            $this->setStatus(InvoiceStatus::from(InvoiceStatus::PENDING));
        }

        $this->customer = $customer;
        if (!$this->proforma) {
            $this->customer->addInvoice($this);
        }

        $this->items = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->discountLinks = new ArrayCollection();

        $this->vatAmount = new Money(0, new Currency('EUR'));
        $this->vatPercentage = self::DEFAULT_VAT_PERCENTAGE;

        //geen BTW indien BTW-nummer is ingevuld en klant niet NL
        if (null !== $customer->getVat() && ('NL' != $customer->getAddress()->getCountry())) {
            $this->vatPercentage = 0;
        }

        $this->subtotal = new Money(0, new Currency('EUR'));
        $this->total = new Money(0, new Currency('EUR'));

        $this->profile = $profile;
        //$this->profile->addInvoice($this);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'Invoice#' . $this->id->getString();
    }

    /**
     * @param InvoiceId      $id
     * @param InvoiceCode    $code
     * @param Customer       $customer
     * @param CarbonInterval $dueDays
     * @param Profile        $profile
     *
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     *
     * @return Invoice
     */
    public static function issue(InvoiceId $id, InvoiceCode $code, Customer $customer, CarbonInterval $dueDays, Profile $profile): self
    {
        $invoice = new self($id, $customer, false, $profile);
        $invoice->code = $code;

        $invoice->timestampIssued = Carbon::now();
        $invoice->timestampDue = $invoice->getTimestampIssued()->copy()->setTime(0, 0)->add($dueDays);

        $invoice->record(InvoiceIssuedEvent::create($id));

        return $invoice;
    }

    /**
     * Factuur aanmaken voor een bepaalde order.
     *
     * @param InvoiceId   $id
     * @param InvoiceCode $code
     * @param Order       $order
     *
     * @throws \RuntimeException
     * @throws \LogicException
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     * @throws \InvalidArgumentException
     * @throws NotImplementedException
     * @throws \Exception
     *
     * @return Invoice
     */
    public static function issueForOrder(InvoiceId $id, InvoiceCode $code, Order $order): self
    {
        $invoice = static::issue($id, $code, $order->getCustomer(), CarbonInterval::days(14), $order->getTemplate()->getPartner()->getCountryProfile());
        $invoice->addOrder($order);

        return $invoice;
    }

    /**
     * Proforma factuur voor een bestelling aanmaken.
     *
     * @param Order $order
     *
     * @throws \RuntimeException
     * @throws \LogicException
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     * @throws \InvalidArgumentException
     * @throws NotImplementedException
     * @throws \Exception
     *
     * @return Invoice
     */
    public static function proforma(Order $order): self
    {
        $invoice = new static(InvoiceId::null(), $order->getCustomer(), true, $order->getTemplate()->getPartner()->getCountryProfile());
        $invoice->addOrder($order);

        return $invoice;
    }

    /**
     * @param InvoiceDiscount $link
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    public function addDiscountLink(InvoiceDiscount $link)
    {
        if ($this->hasAppliedDiscount($link->getDiscount())) {
            throw new DuplicateEntityException();
        }

        $this->discountLinks->add($link);
    }

    /**
     * @param InvoiceItem $item
     *
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     * @throws \Exception
     */
    public function addItem(InvoiceItem $item)
    {
        if ($this->hasItem($item)) {
            throw new DuplicateEntityException();
        }

        $this->items->add($item);

        $this->recalculate();
    }

    /**
     * Bestelling toevoegen aan deze factuur.
     *
     * @param Order $order
     *
     * @throws \RuntimeException
     * @throws \Core\Common\Exceptions\NotImplementedException
     * @throws \InvalidArgumentException
     * @throws NotImplementedException
     * @throws \Exception
     */
    public function addOrder(Order $order)
    {
        $template = $order->getTemplate();

        /*
         * Eenmalig document
         */
        if (OrderType::UNIT == $order->getType()) {
            $item = InvoiceItem::create(
                InvoiceItemId::generate(),
                $this,
                1,
                $template->getUnitPrice()
            );

            $item->setType(new OrderItemType(OrderItemType::DOCUMENT));

            $item->linkOrder($order);

            $item->setTitle($template->getTitle());
        }

        /*
         * Eerste bestelling van abonnement
         */
        elseif (OrderType::SUBSCRIPTION == $order->getType()) {
            $item = InvoiceItem::create(
                InvoiceItemId::generate(),
                $this,
                1,
                $template->getYearlyPrice()
            );

            $item->setType(new OrderItemType(OrderItemType::ADDITIONAL_SERVICE));

            $item->linkOrder($order);

            $item->setTitle('Jaarabonnement voor ' . $template->getTitle());
        } else {
            throw new NotImplementedException(sprintf('The order type "%s" is not yet supported', $order->getType()->getValue()));
        }

        /*
        * Toevoeging voor aangetekend mailen
        */
        if ($order->isRegisteredEmailRequested()) {
            $item = InvoiceItem::create(InvoiceItemId::generate(), $this, 1, $template->getRegisteredEmailPrice());
            $item->setType(new OrderItemType(OrderItemType::ADDITIONAL_SERVICE));
            $item->linkOrder($order);

            $item->setTitle('Aangetekend mailen®');
        }

        // Toevoeging voor handmatige controle
        if ($order->isCheckRequested()) {
            $item = InvoiceItem::create(
                InvoiceItemId::generate(),
                $this,
                1,
                $template->getCheckPrice()
            );

            $item->setType(new OrderItemType(OrderItemType::ADDITIONAL_SERVICE));

            $item->linkOrder($order);

            $item->setTitle('manual');
        }

        // Toevoeging voor maatwerk aanpassingen
        if ($order->isCustomRequested()) {
            $item = InvoiceItem::create(
                InvoiceItemId::generate(),
                $this,
                1,
                new Money(0, new Currency('EUR'))
            );

            $item->setType(new OrderItemType(OrderItemType::ADDITIONAL_SERVICE));

            $item->linkOrder($order);
            $item->setTitle('custom');
        }

        if (0 === $order->getDiscounts()->count()) {
            $this->recalculate();
        }

        // Kortingen overnemen
        foreach ($order->getDiscounts() as $discount) {
            $this->applyDiscount($discount);
        }
    }

    /**
     * @param Payment $payment
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    public function addPayment(Payment $payment)
    {
        if ($this->hasPayment($payment)) {
            throw new DuplicateEntityException();
        }

        $this->payments->add($payment);
    }

    /**
     * @param Discount $discount
     *
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     * @throws \Exception
     *
     * @return InvoiceDiscount
     */
    public function applyDiscount(Discount $discount): InvoiceDiscount
    {
        $link = InvoiceDiscount::create($this, $discount);

        $this->recalculate();

        return $link;
    }

    /**
     * @param InvoiceId   $creditInvoiceId
     * @param InvoiceCode $creditInvoiceCode
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \Exception
     *
     * @return Invoice
     */
    public function credit(InvoiceId $creditInvoiceId, InvoiceCode $creditInvoiceCode): self
    {
        if ($this->proforma) {
            throw new \LogicException('You cannot credit a proforma invoice');
        }

        if ($this->isCredited()) {
            throw new \LogicException('You cannot credit an already credited invoice');
        }

        if ($this->isCredit()) {
            throw new \LogicException('You cannot go full-circle and credit a credit invoice (that would make it a regular invoice)');
        }

        if ($this->isZero()) {
            throw new \LogicException('You cannot credit a "free" invoice');
        }

        $this->setStatus(InvoiceStatus::from(InvoiceStatus::CREDITED));

        $this->record(InvoiceCreditedEvent::create($this->id, $creditInvoiceId));

        /*
         * Creditfactuur aanmaken
         */
        $credit = self::issue($creditInvoiceId, $creditInvoiceCode, $this->customer, CarbonInterval::days(0), $this->profile);
        $credit->setStatus(InvoiceStatus::from(InvoiceStatus::PAID));

        foreach ($this->items as $item) {
            $creditItem = InvoiceItem::create(
                InvoiceItemId::generate(),
                $credit,
                $item->getQuantity(),
                $item->getPrice()->negative()
            );

            $creditItem->setType($item->getType());

            $creditItem->setTitle('Creditering: ' . $item->getTitle());

            if ($item->hasOrder()) {
                $creditItem->linkOrder($item->getOrder());
            }
        }

        foreach ($this->getDiscounts() as $discount) {
            $credit->addDiscountLink($discount);
        }

        $credit->recalculateCreditInvoice();

        return $credit;
    }

    /**
     * Detach the invoice from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->customer) {
            $this->customer->removeInvoice($this);
            $this->customer = null;
        }
    }

    /**
     * @throws \LogicException
     * @throws \DomainException
     *
     * @return Payment|null
     */
    public function findPendingPayment()
    {
        $payments = $this->getPendingPayments();
        if ($payments->isEmpty()) {
            return;
        }

        if ($payments->count() > 1) {
            throw new \DomainException('There are multiple pending payments, use Invoice::getPendingPayments() instead');
        }

        return $payments->first();
    }

    /**
     * @return InvoiceCode
     */
    public function getCode(): InvoiceCode
    {
        return $this->code;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @return Collection|InvoiceDiscount[]
     */
    public function getDiscounts(): Collection
    {
        return $this->discountLinks;
    }

    /**
     * @return InvoiceId
     */
    public function getId(): InvoiceId
    {
        return $this->id;
    }

    /**
     * @return Collection|InvoiceItem[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    /**
     * @return ArrayCollection|Order[]
     */
    public function getOrders(): ArrayCollection
    {
        $orders = new ArrayCollection();

        $this->items->matching(
            Criteria::create()->where(
                Criteria::expr()->neq('order', null)
            )
        )->map(function (InvoiceItem $item) use ($orders) {
            if (!$orders->contains($item->getOrder())) {
                $orders->add($item->getOrder());
            }
        });

        return $orders;
    }

    /**
     * @throws \LogicException
     *
     * @return Collection|Payment[]
     */
    public function getPayments(): Collection
    {
        if ($this->proforma) {
            throw new \LogicException('You cannot work with payments on a proforma invoice');
        }

        return $this->payments;
    }

    /**
     * @throws \LogicException
     * @throws \DomainException
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     *
     * @return Payment
     */
    public function getPendingPayment(): Payment
    {
        $payment = $this->findPendingPayment();
        if (null === $payment) {
            throw new EntityNotFoundException('There is no pending payment for this invoice');
        }

        return $payment;
    }

    /**
     * @throws \LogicException
     *
     * @return Collection|Payment[]
     */
    public function getPendingPayments(): Collection
    {
        if ($this->proforma) {
            throw new \LogicException('You cannot work with payments on a proforma invoice');
        }

        return $this->payments->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('status', PaymentStatus::from(PaymentStatus::PENDING))
            )
        );
    }

    /**
     * @return InvoiceStatus
     */
    public function getStatus(): InvoiceStatus
    {
        return $this->status;
    }

    /**
     * @return Money
     */
    public function getSubtotal(): Money
    {
        return $this->subtotal;
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampCredited()
    {
        return $this->timestampCredited;
    }

    /**
     * @return Carbon
     */
    public function getTimestampDue(): Carbon
    {
        return $this->timestampDue;
    }

    /**
     * @return Carbon
     */
    public function getTimestampIssued(): Carbon
    {
        return $this->timestampIssued;
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampPaid()
    {
        return $this->timestampPaid;
    }

    /**
     * @return Carbon|null
     */
    public function getTimestampRevoked()
    {
        return $this->timestampRevoked;
    }

    /**
     * @return Money
     */
    public function getTotal(): Money
    {
        return $this->total;
    }

    /**
     * @return Money
     */
    public function getVatAmount(): Money
    {
        return $this->vatAmount;
    }

    /**
     * @return int
     */
    public function getVatPercentage(): int
    {
        return $this->vatPercentage;
    }

    /**
     * @param Discount $discount
     *
     * @return bool
     */
    public function hasAppliedDiscount(Discount $discount): bool
    {
        return null !== $this->findDiscountLink($discount);
    }

    /**
     * @param InvoiceItem $item
     *
     * @return bool
     */
    public function hasItem(InvoiceItem $item): bool
    {
        return $this->items->contains($item);
    }

    /**
     * @param Payment $payment
     *
     * @return bool
     */
    public function hasPayment(Payment $payment): bool
    {
        return $this->payments->contains($payment);
    }

    /**
     * @return bool
     */
    public function hasPayments(): bool
    {
        return !$this->payments->isEmpty();
    }

    /**
     * Controleren of er betalingen in afwachting zijn voor deze factuur.
     *
     * @throws \LogicException
     *
     * @return bool
     */
    public function hasPendingPayments(): bool
    {
        return !$this->getPendingPayments()->isEmpty();
    }

    /**
     * @return bool
     */
    public function isCredit(): bool
    {
        return $this->subtotal->isNegative();
    }

    /**
     * @return bool
     */
    public function isCredited(): bool
    {
        return null !== $this->timestampCredited;
    }

    /**
     * @return bool
     */
    public function isDue(): bool
    {
        if ($this->isPaid()) {
            return false;
        }

        return $this->timestampDue->lte(Carbon::today());
    }

    /**
     * @return bool
     */
    public function isDueSoon(): bool
    {
        if ($this->isPaid()) {
            return false;
        }

        return $this->timestampDue->lte((Carbon::today())->addDays(3));
    }

    /**
     * @return bool
     */
    public function isOverdue(): bool
    {
        if ($this->isPaid()) {
            return false;
        }

        return $this->timestampDue->lt(Carbon::today());
    }

    /**
     * @return bool
     */
    public function isPaid(): bool
    {
        return null !== $this->timestampPaid;
    }

    /**
     * @return mixed
     */
    public function isProforma()
    {
        return $this->proforma;
    }

    /**
     * @return bool
     */
    public function isRevoked(): bool
    {
        return null !== $this->timestampRevoked;
    }

    /**
     * Factuur is een 0-euro factuur.
     *
     * @return bool
     */
    public function isZero(): bool
    {
        return $this->total->isZero();
    }

    /**
     * @param Customer $customer
     */
    public function linkCustomer(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * Betaling klaarzetten om te worden afgehandeld.
     *
     * @param PaymentId     $id
     * @param PaymentMethod $method
     *
     * @throws \LogicException
     * @throws \DomainException
     *
     * @return Payment
     */
    public function preparePayment(PaymentId $id, PaymentMethod $method = null): Payment
    {
        if ($this->proforma) {
            throw new \LogicException('You cannot work with payments on a proforma invoice');
        }

        if ($this->hasPendingPayments()) {
            throw new \DomainException('There is at least one pending payment for this invoice, so you cannot prepare another payment');
        }

        $payment = Payment::prepare($id, $this);
        if (null !== $method) {
            $payment->setMethod($method);
        }

        return $payment;
    }

    /**
     * Recalculate the credit invoice's totals.
     *
     * @throws \RuntimeException
     * @throws \Exception
     * @throws \InvalidArgumentException
     */
    public function recalculateCreditInvoice()
    {
        return $this->recalculate(false);
    }

    /**
     * Recalculate the invoice's totals.
     *
     * @params bool $preventNegativeSubtotal
     *
     * @throws \RuntimeException
     * @throws \Exception
     * @throws \InvalidArgumentException
     */
    public function recalculate(bool $preventNegativeSubtotal = true)
    {
        // Items bij elkaar optellen
        $subtotal = $this->getItemsTotal();

        /*
         * Kortingen met een vast bedrag verrekenen
         */
        $this->getDiscounts()->filter(function (InvoiceDiscount $discountLink) {
            return $discountLink->getType()->is(DiscountType::FIXED);
        })->map(function (InvoiceDiscount $discountLink) use (&$subtotal) {
            $subtotal = $subtotal->subtract($discountLink->getAmount());
        });

        /*
         * Procentuele kortingen verrekenen
         */
        $seenPercentual = false;

        $this->getDiscounts()->filter(function (InvoiceDiscount $discountLink) {
            return $discountLink->getType()->is(DiscountType::PERCENTAGE);
        })->map(function (InvoiceDiscount $discountLink) use (&$subtotal, &$seenPercentual) {
            if ($seenPercentual) {
                throw new \RuntimeException('Trying to apply multiple percentual discounts');
            }

            $subtotal = $subtotal->subtract($subtotal->multiply($discountLink->getDiscount()->getMultiplier()));

            $seenPercentual = true;
        });

        // Voorkomen dat we geld terug gaan geven
        if ($preventNegativeSubtotal && !$this->getDiscounts()->isEmpty()) {
            $zero = new Money(0, new Currency('EUR'));
            if ($subtotal->lessThan($zero)) {
                $subtotal = $zero;
            }
        }

        // Add additional services.
        $subtotal = $subtotal->add($this->getAdditionalServicesTotal());

        // Totalen opslaan
        $this->subtotal = $subtotal;

        $this->vatAmount = $this->subtotal->multiply($this->vatPercentage / 100);
        $this->total = $this->subtotal->add($this->vatAmount);
    }

    /**
     * @return Money
     */
    public function getItemsTotal(): Money
    {
        $subtotal = new Money(0, new Currency('EUR'));

        $this->items->map(function (InvoiceItem $item) use (&$subtotal) {
            if ($item->getType() && OrderItemType::DOCUMENT === $item->getType()->getValue()) {
                $subtotal = $subtotal->add($item->getTotal());
            }
        });

        return $subtotal;
    }

    /**
     * @return Money
     */
    public function getAdditionalServicesTotal(): Money
    {
        $subtotal = new Money(0, new Currency('EUR'));

        $this->items->map(function (InvoiceItem $item) use (&$subtotal) {
            if ($item->getType() && OrderItemType::ADDITIONAL_SERVICE === $item->getType()->getValue()) {
                $subtotal = $subtotal->add($item->getTotal());
            }
        });

        return $subtotal;
    }

    /**
     * @param Discount $discount
     */
    public function removeDiscount(Discount $discount)
    {
        $link = $this->findDiscountLink($discount);
        if (null === $link) {
            return;
        }

        $this->removeDiscountLink($link);
        $discount->removeInvoiceLink($link);
    }

    /**
     * @param InvoiceDiscount $link
     */
    public function removeDiscountLink(InvoiceDiscount $link)
    {
        if (!$this->discountLinks->contains($link)) {
            return;
        }

        $this->discountLinks->removeElement($link);
    }

    /**
     * @param InvoiceItem $item
     *
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function removeItem(InvoiceItem $item)
    {
        if (!$this->hasItem($item)) {
            return;
        }

        $this->items->removeElement($item);

        $this->recalculate();
    }

    /**
     * @param Payment $payment
     */
    public function removePayment(Payment $payment)
    {
        if (!$this->hasPayment($payment)) {
            return;
        }

        $this->payments->removeElement($payment);
    }

    /**
     * @param InvoiceStatus $status
     * @param Carbon        $dateTime
     *
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function setStatus(InvoiceStatus $status, Carbon $dateTime = null)
    {
        if ($this->proforma) {
            throw new \LogicException('You cannot change the status of a proforma invoice');
        }

        if ($status == $this->status) {
            return;
        }

        switch ($status) {
            case InvoiceStatus::PENDING:
                if (null !== $this->status) {
                    throw new \LogicException('The invoice\'s InvoiceStatus cannot be reverted to DRAFT');
                }

                break;

            case InvoiceStatus::PAID:
                if ($this->status->isNot(InvoiceStatus::PENDING)) {
                    throw new \LogicException('The invoice\'s InvoiceStatus should be PENDING');
                }

                $this->timestampPaid = $dateTime ?: Carbon::now();
                $this->record(InvoicePaidEvent::create($this->id));

                foreach ($this->getOrders() as $order) {
                    $this->record(OrderPaidEvent::create($order->getId()));
                }

                break;

            case InvoiceStatus::REVOKED:
                $this->timestampRevoked = $dateTime ?: Carbon::now();
                $this->record(InvoiceRevokedEvent::create($this->id));

                break;

            case InvoiceStatus::CREDITED:
                $this->timestampCredited = $dateTime ?: Carbon::now();

                break;

            default:
                throw new \InvalidArgumentException(sprintf('Invalid status "%s"', $status->getValue()));
        }

        $this->status = $status;
    }

    /**
     * @return Profile
     */
    public function getProfile(): Profile
    {
        return $this->profile;
    }

    /**
     * @param Discount $discount
     *
     * @return InvoiceDiscount|null
     */
    private function findDiscountLink(Discount $discount)
    {
        $links = $this->discountLinks->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('discount', $discount)
            )
        );

        return $links->isEmpty() ? null : $links->first();
    }
}
