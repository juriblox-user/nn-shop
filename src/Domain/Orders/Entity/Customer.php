<?php

namespace NnShop\Domain\Orders\Entity;

use Carbon\Carbon;
use Core\Annotation\Doctrine as Core;
use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\Common\Generator\Security\LongTokenGenerator;
use Core\Common\Generator\Security\ShortTokenGenerator;
use Core\Common\Security\SensitiveValue;
use Core\Common\Validation\Assertion;
use Core\Domain\Traits\SoftDeleteableEntityTrait;
use Core\Domain\Value\Geography\Address;
use Core\Domain\Value\Security\HashedPassword;
use Core\Domain\Value\Security\Token;
use Core\Domain\Value\Web\EmailAddress;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JuriBlox\Sdk\Domain\Customers\Values\CustomerReference as RemoteCustomerReference;
use NnShop\Domain\Orders\Enumeration\CustomerStatus;
use NnShop\Domain\Orders\Enumeration\CustomerType;
use NnShop\Domain\Orders\Event\Customer\CustomerAddressChangedEvent;
use NnShop\Domain\Orders\Event\Customer\CustomerCreatedEvent;
use NnShop\Domain\Orders\Event\Customer\CustomerUpdatedEvent;
use NnShop\Domain\Orders\Event\CustomerEmailChangeRequestedEvent;
use NnShop\Domain\Orders\Event\EmailChangeCompletedEvent;
use NnShop\Domain\Orders\Value\CustomerEmailHash;
use NnShop\Domain\Orders\Value\CustomerId;
use NnShop\Domain\Translation\Entity\Locale;
use NnShop\Domain\Translation\Entity\Profile;
use Money\Currency;
use Money\Money;
use SimpleBus\Message\Recorder\ContainsRecordedMessages;
use SimpleBus\Message\Recorder\PrivateMessageRecorderCapabilities;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Orders\Repository\CustomerDoctrineRepository")
 * @Core\QueryBuilder(class="NnShop\Infrastructure\Orders\QueryBuilder\CustomerDoctrineQueryBuilder")
 *
 * @ORM\Table(indexes={
 *     @ORM\Index(name="ix_deleted", columns={"timestamp_deleted"}),
 *     @ORM\Index(name="ix_email", columns={"customer_email", "timestamp_deleted"}),
 *     @ORM\Index(name="ix_worth", columns={"customer_worth", "timestamp_deleted"})
 * })
 *
 * @Gedmo\SoftDeleteable(fieldName="timestampDeleted")
 */
class Customer implements ContainsRecordedMessages, EquatableInterface, \Serializable, UserInterface
{
    use PrivateMessageRecorderCapabilities;
    use SoftDeleteableEntityTrait;

    /**
     * @ORM\Embedded(class="Core\Domain\Value\Geography\Address")
     *
     * @var Address
     */
    private $address;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\Advice", mappedBy="customer", cascade={"persist"}, fetch="EXTRA_LAZY")
     *
     * @var Collection|Advice[]
     */
    private $advices;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string
     */
    private $companyName;

    /**
     * @ORM\Column(type="core.web.email_address")
     *
     * @var EmailAddress
     */
    private $email;

    /**
     * @ORM\Column(type="core.web.email_address", nullable=true)
     *
     * @var EmailAddress|null
     */
    private $emailChange;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $emailVerified;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string
     */
    private $firstname;

    /**
     * @ORM\Id
     * @ORM\Column(type="orders.customer_id")
     *
     * @var CustomerId
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\Invoice", mappedBy="customer", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     *
     * @var Collection|Invoice[]
     */
    private $invoices;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string
     */
    private $lastname;

    /**
     * @ORM\ManyToOne(targetEntity="NnShop\Domain\Translation\Entity\Locale", inversedBy="customers")
     * @ORM\JoinColumn(referencedColumnName="locale_id")
     *
     * @var Locale
     */
    private $locale;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $newsletter;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\Order", mappedBy="customer", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     *
     * @var Collection|Order[]
     */
    private $orders;

    /**
     * @ORM\Column(type="core.security.hashed_password", nullable=true)
     *
     * @var HashedPassword
     */
    private $passwordHash;

    /**
     * @ORM\Column(type="core.security.short_token", nullable=true)
     *
     * @var Token
     */
    private $passwordSalt;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $phone;

    /**
     * @ORM\ManyToOne(targetEntity="NnShop\Domain\Translation\Entity\Profile", inversedBy="customers")
     * @ORM\JoinColumn(referencedColumnName="profile_id")
     *
     * @var Profile
     */
    private $profile;

    /**
     * @ORM\Column(type="juriblox.customer_reference", nullable=true, unique=true)
     *
     * @var RemoteCustomerReference|null
     */
    private $remoteReference;

    /**
     * @ORM\Column(type="orders.customer_status")
     *
     * @var CustomerStatus
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\Subscription", mappedBy="customer", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     *
     * @var Collection|Subscription[]
     */
    private $subscriptions;

    /**
     * @ORM\Column(type="core.time.datetime")
     *
     * @var Carbon
     */
    private $timestampCreated;

    /**
     * @ORM\Column(type="core.security.sensitive_value", nullable=true)
     *
     * @var SensitiveValue|null
     */
    private $token;

    /**
     * @ORM\Column(type="orders.customer_type")
     *
     * @var CustomerType
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string
     */
    private $vat;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @var string
     */
    private $cocNumber;

    /**
     * @ORM\Column(type="core.financial.money")
     *
     * @var Money
     */
    private $worth;

    /**
     * Customer constructor.
     *
     * @param CustomerId   $id
     * @param CustomerType $type
     * @param Profile      $profile
     * @param Locale       $locale
     */
    private function __construct(CustomerId $id, CustomerType $type, Profile $profile, Locale $locale)
    {
        $this->id = $id;
        $this->type = $type;

        $this->profile = $profile;
        $this->profile->addCustomer($this);

        $this->locale = $locale;
        $this->locale->addCustomer($this);

        $this->emailVerified = false;
        $this->newsletter = false;

        $this->advices = new ArrayCollection();
        $this->invoices = new ArrayCollection();
        $this->orders = new ArrayCollection();

        $this->subscriptions = new ArrayCollection();

        $this->worth = new Money(0, new Currency('EUR'));
    }

    /**
     * @param CustomerId   $id
     * @param CustomerType $type
     * @param EmailAddress $email
     * @param Profile      $profile
     * @param Locale       $locale
     *
     * @return Customer
     */
    public static function create(CustomerId $id, CustomerType $type, EmailAddress $email, Profile $profile, Locale $locale): self
    {
        $customer = new self($id, $type, $profile, $locale);
        $customer->status = new CustomerStatus(CustomerStatus::DRAFT);

        $customer->email = $email;

        $customer->generateToken();
        $customer->timestampCreated = Carbon::now();

        $customer->record(CustomerCreatedEvent::create($id));

        return $customer;
    }

    /**
     * @param Advice $advice
     */
    public function addAdvice(Advice $advice)
    {
        if ($this->hasAdvice($advice)) {
            throw new DuplicateEntityException();
        }

        $this->advices->add($advice);
    }

    /**
     * @param Invoice $invoice
     */
    public function addInvoice(Invoice $invoice)
    {
        if ($this->hasInvoice($invoice)) {
            throw new DuplicateEntityException();
        }

        $this->invoices->add($invoice);
    }

    /**
     * @param Order $order
     */
    public function addOrder(Order $order)
    {
        if ($this->hasOrder($order)) {
            throw new DuplicateEntityException();
        }

        $this->orders->add($order);
    }

    /**
     * @param Subscription $subscription
     */
    public function addSubscription(Subscription $subscription)
    {
        if ($this->hasSubscription($subscription)) {
            throw new DuplicateEntityException();
        }

        $this->subscriptions->add($subscription);
    }

    /**
     * @param Money $amount
     */
    public function addWorth(Money $amount)
    {
        $this->worth = $this->worth->add($amount);
    }

    /**
     * Wijziging e-mailadres intrekken.
     */
    public function cancelEmailChange()
    {
        $this->emailChange = null;
    }

    /**
     * Wijziging e-mailadres afronden.
     */
    public function completeEmailChange()
    {
        if (null === $this->emailChange) {
            throw new \LogicException(sprintf('There is no pending e-mail change for Customer#%s', $this->id));
        }

        $this->email = $this->emailChange;

        $this->emailChange = null;
        $this->emailVerified = true;

        // Event
        $this->record(EmailChangeCompletedEvent::create($this->id));
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
    }

    /**
     * Nieuwe token genereren.
     */
    public function generateToken()
    {
        $this->token = SensitiveValue::from(LongTokenGenerator::generate()->getString());
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * @return Collection|Advice[]
     */
    public function getAdvices(): Collection
    {
        return $this->advices;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @return EmailAddress
     */
    public function getEmail(): EmailAddress
    {
        return $this->email;
    }

    /**
     * @return EmailAddress|null
     */
    public function getEmailChange()
    {
        return $this->emailChange;
    }

    /**
     * @return CustomerEmailHash|null
     */
    public function getEmailChangeHash()
    {
        return null !== $this->emailChange ? CustomerEmailHash::generate($this->emailChange) : null;
    }

    /**
     * @return CustomerEmailHash
     */
    public function getEmailHash(): CustomerEmailHash
    {
        return CustomerEmailHash::generate($this->email);
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @return CustomerId
     */
    public function getId(): CustomerId
    {
        return $this->id;
    }

    /**
     * @return Collection|Invoice[]
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return string
     */
    public function getListingName(): string
    {
        if (null !== $this->companyName) {
            return $this->companyName;
        }

        return $this->getName();
    }

    /**
     * @return Locale
     */
    public function getLocale(): Locale
    {
        return $this->locale;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return trim($this->firstname . ' ' . $this->lastname) ?: '';
    }

    /**
     * @return bool
     */
    public function getNewsletter(): bool
    {
        return $this->newsletter;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    /**
     * @return string|null
     */
    public function getPassword()
    {
        if (null === $this->passwordHash) {
            return null;
        }

        return $this->passwordHash->getString();
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return Profile
     */
    public function getProfile(): Profile
    {
        return $this->profile;
    }

    /**
     * @return RemoteCustomerReference|null
     */
    public function getRemoteReference()
    {
        return $this->remoteReference;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return [
            'ROLE_CUSTOMER',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
        if (null === $this->passwordSalt) {
            $this->passwordSalt = ShortTokenGenerator::generate();
        }

        return $this->passwordSalt->getString();
    }

    /**
     * @return CustomerStatus
     */
    public function getStatus(): CustomerStatus
    {
        return $this->status;
    }

    /**
     * @return Collection|Subscription[]
     */
    public function getSubscriptions(): Collection
    {
        return $this->subscriptions;
    }

    /**
     * @return Carbon
     */
    public function getTimestampCreated(): Carbon
    {
        return $this->timestampCreated;
    }

    /**
     * @return SensitiveValue|null
     */
    public function getToken(): SensitiveValue
    {
        return $this->token;
    }

    /**
     * @return CustomerType
     */
    public function getType(): CustomerType
    {
        return $this->type;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->email->getString();
    }

    /**
     * @return string
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @return string|null
     */
    public function getCocNumber()
    {
        return $this->cocNumber;
    }

    /**
     * @return Money
     */
    public function getWorth(): Money
    {
        return $this->worth;
    }

    /**
     * @param Advice $advice
     *
     * @return bool
     */
    public function hasAdvice(Advice $advice): bool
    {
        return $this->advices->contains($advice);
    }

    /**
     * @return bool
     */
    public function hasEmailChange(): bool
    {
        return null !== $this->emailChange;
    }

    /**
     * @param Invoice $invoice
     *
     * @return bool
     */
    public function hasInvoice(Invoice $invoice): bool
    {
        return $this->invoices->contains($invoice);
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    public function hasOrder(Order $order): bool
    {
        return $this->orders->contains($order);
    }

    /**
     * @return bool
     */
    public function hasPassword(): bool
    {
        return null !== $this->passwordHash;
    }

    /**
     * @param Subscription $subscription
     *
     * @return bool
     */
    public function hasSubscription(Subscription $subscription): bool
    {
        return $this->subscriptions->contains($subscription);
    }

    /**
     * @return bool
     */
    public function isEmailVerified(): bool
    {
        return $this->emailVerified;
    }

    /**
     * {@inheritdoc}
     *
     * @param $customer Customer
     */
    public function isEqualTo(UserInterface $customer)
    {
        Assertion::isInstanceOf($customer, self::class);

        return $this->id->equals($customer->getId());
    }

    /**
     * @param Advice $advice
     */
    public function removeAdvice(Advice $advice)
    {
        if (!$this->hasAdvice($advice)) {
            return;
        }

        $this->advices->removeElement($advice);
    }

    /**
     * @param Invoice $invoice
     */
    public function removeInvoice(Invoice $invoice)
    {
        if (!$this->hasInvoice($invoice)) {
            return;
        }

        $this->invoices->removeElement($invoice);
    }

    /**
     * @param Order $order
     */
    public function removeOrder(Order $order)
    {
        if (!$this->hasOrder($order)) {
            return;
        }

        $this->orders->removeElement($order);
    }

    /**
     * @param Subscription $subscription
     */
    public function removeSubscription(Subscription $subscription)
    {
        if (!$this->hasSubscription($subscription)) {
            return;
        }

        $this->subscriptions->removeElement($subscription);
    }

    /**
     * @param EmailAddress|null $emailChange
     */
    public function requestEmailChange(EmailAddress $emailChange)
    {
        $this->emailChange = $emailChange;

        // Event, o.a. voor versturen e-mail
        $this->record(CustomerEmailChangeRequestedEvent::create($this->id));
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize([
            'id' => $this->id->getString(),
        ]);
    }

    /**
     * @param Address $address
     */
    public function setAddress(Address $address)
    {
        if (null !== $this->address && $this->address != $address) {
            $this->record(CustomerAddressChangedEvent::create($this->id, $this->address, $address));
            $this->record(CustomerUpdatedEvent::create($this->id));
        }

        $this->address = $address;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName($companyName)
    {
        if (null !== $this->companyName && $companyName !== $this->companyName) {
            $this->record(CustomerUpdatedEvent::create($this->id));
        }

        $this->companyName = $companyName ?: null;
    }

    /**
     * @param EmailAddress|null $email
     */
    public function setEmail($email)
    {
        Assertion::nullOrIsInstanceOf($email, EmailAddress::class);

        $this->email = $email;
        $this->emailVerified = true;

        // Wijziging aan het e-mailadres intrekken
        $this->cancelEmailChange();
    }

    /**
     * @param bool $emailVerified
     */
    public function setEmailVerified(bool $emailVerified)
    {
        $this->emailVerified = $emailVerified;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        if (null !== $this->firstname && $firstname !== $this->firstname) {
            $this->record(CustomerUpdatedEvent::create($this->id));
        }

        $this->firstname = $firstname ?: null;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        if (null !== $this->lastname && $lastname !== $this->lastname) {
            $this->record(CustomerUpdatedEvent::create($this->id));
        }

        $this->lastname = $lastname ?: null;
    }

    /**
     * @param bool $newsletter
     */
    public function setNewsletter(bool $newsletter)
    {
        $this->newsletter = $newsletter;
    }

    /**
     * @param HashedPassword|null $password
     */
    public function setPassword($password)
    {
        Assertion::nullOrIsInstanceOf($password, HashedPassword::class);

        $this->passwordHash = $password;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @param RemoteCustomerReference $remoteReference
     */
    public function setRemoteReference(RemoteCustomerReference $remoteReference)
    {
        $this->remoteReference = $remoteReference;
    }

    /**
     * @param CustomerStatus $status
     */
    public function setStatus(CustomerStatus $status)
    {
        $this->status = $status;
    }

    /**
     * @param CustomerType $type
     */
    public function setType(CustomerType $type)
    {
        if (null !== $this->type && $this->type->isNot($type)) {
            $this->record(CustomerUpdatedEvent::create($this->id));
        }

        $this->type = $type;

        if ($this->type->isNot(CustomerType::BUSINESS)) {
            $this->companyName = null;
        }
    }

    /**
     * @param string $vat|null
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        $serialized = unserialize($serialized);

        $this->id = new CustomerId($serialized['id']);
    }

    /**
     * @param string|null $cocNumber
     */
    public function setCocNumber($cocNumber)
    {
        $this->cocNumber = $cocNumber;
    }


}
