<?php

namespace NnShop\Domain\Orders\Entity;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use NnShop\Domain\Orders\Enumeration\OrderStatus;

/**
 * @ORM\Entity
 * @ORM\Table(name="orders_order_status")
 */
class OrderStatusChange
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @var int
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Orders\Entity\Order", inversedBy="statusChanges")
     * @ORM\JoinColumn(referencedColumnName="order_id", nullable=false)
     *
     * @var Order
     */
    private $order;

    /**
     * @ORM\Column(type="orders.order_status")
     *
     * @var OrderStatus
     */
    private $status;

    /**
     * @ORM\Column(type="core.time.datetime")
     *
     * @var Carbon
     */
    private $timestamp;

    /**
     * OrderStateChange constructor.
     *
     * @param Order       $order
     * @param OrderStatus $status
     */
    private function __construct(Order $order, OrderStatus $status)
    {
        $this->order = $order;
        $this->status = $status;

        $this->timestamp = Carbon::now();
    }

    /**
     * @param Order       $order
     * @param OrderStatus $status
     *
     * @return OrderStatusChange
     */
    public static function create(Order $order, OrderStatus $status): self
    {
        $change = new self($order, $status);

        $order->addStatusChange($change);

        return $change;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return OrderStatus
     */
    public function getStatus(): OrderStatus
    {
        return $this->status;
    }

    /**
     * @return Carbon
     */
    public function getTimestamp(): Carbon
    {
        return $this->timestamp;
    }
}
