<?php

namespace NnShop\Domain\Orders\Entity;

use Core\Common\Exception\Domain\DuplicateEntityException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use NnShop\Domain\Orders\Value\AdviceId;
use NnShop\Domain\Templates\Entity\Template;

/**
 * @ORM\Entity(repositoryClass="NnShop\Infrastructure\Orders\Repository\AdviceDoctrineRepository")
 */
class Advice
{
    /**
     * @ORM\ManyToOne(targetEntity="\NnShop\Domain\Orders\Entity\Customer", inversedBy="advices")
     * @ORM\JoinColumn(referencedColumnName="customer_id", nullable=false)
     *
     * @var Customer
     */
    private $customer;

    /**
     * @ORM\Id
     * @ORM\Column(type="orders.advice_id")
     *
     * @var AdviceId
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="\NnShop\Domain\Orders\Entity\AdviceTemplate", mappedBy="advice", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @var Collection|AdviceTemplate[]
     */
    private $templateLinks;

    /**
     * Advice constructor.
     *
     * @param AdviceId $id
     * @param Customer $customer
     */
    private function __construct(AdviceId $id, Customer $customer)
    {
        $this->id = $id;

        $this->customer = $customer;
        $this->customer->addAdvice($this);

        $this->templateLinks = new ArrayCollection();
    }

    /**
     * @param Template $template
     *
     * @return AdviceTemplate
     */
    public function addTemplate(Template $template): AdviceTemplate
    {
        return AdviceTemplate::create($this, $template);
    }

    /**
     * @param AdviceTemplate $link
     */
    public function addTemplateLink(AdviceTemplate $link)
    {
        if ($this->hasTemplate($link->getTemplate())) {
            throw new DuplicateEntityException();
        }

        $this->templateLinks->add($link);
    }

    /**
     * @param AdviceId $id
     * @param Customer $customer
     *
     * @return Advice
     */
    public static function create(AdviceId $id, Customer $customer): self
    {
        return new self($id, $customer);
    }

    /**
     * Detach the advice from its associated entities.
     */
    public function detach()
    {
        if (null !== $this->customer) {
            $this->customer->removeAdvice($this);
            $this->customer = null;
        }
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @return AdviceId
     */
    public function getId(): AdviceId
    {
        return $this->id;
    }

    /**
     * @return Collection|Template[]
     */
    public function getTemplates(): Collection
    {
        $templates = new ArrayCollection();
        foreach ($this->templateLinks as $link) {
            $templates->add($link->getTemplate());
        }

        return $templates;
    }

    /**
     * @param Template $template
     *
     * @return bool
     */
    public function hasTemplate(Template $template): bool
    {
        return null !== $this->findTemplateLink($template);
    }

    /**
     * @param Template $template
     */
    public function removeTemplate(Template $template)
    {
        $link = $this->findTemplateLink($template);
        if (null === $link) {
            return;
        }

        $this->removeTemplateLink($link);
        $template->removeAdviceLink($link);
    }

    /**
     * @param AdviceTemplate $link
     */
    public function removeTemplateLink(AdviceTemplate $link)
    {
        if ($this->templateLinks->contains($link)) {
            return;
        }

        $this->templateLinks->removeElement($link);
    }

    /**
     * @param Template $template
     *
     * @return AdviceTemplate|null
     */
    private function findTemplateLink(Template $template)
    {
        $links = $this->templateLinks->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('template', $template)
            )
        );

        return $links->isEmpty() ? null : $links->first();
    }
}
