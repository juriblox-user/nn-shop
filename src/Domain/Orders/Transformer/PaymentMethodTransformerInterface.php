<?php

namespace NnShop\Domain\Orders\Transformer;

use NnShop\Domain\Orders\Enumeration\PaymentMethod;

interface PaymentMethodTransformerInterface
{
    /**
     * "method" voor Mollie vertalen naar een PaymentMethod.
     *
     * @param $value
     *
     * @return PaymentMethod
     */
    public static function fromRemote($value): PaymentMethod;

    /**
     * PaymentMethod vertalen naar een "method" voor Mollie.
     *
     * @param PaymentMethod $method
     *
     * @return mixed
     */
    public static function toRemote(PaymentMethod $method);
}
