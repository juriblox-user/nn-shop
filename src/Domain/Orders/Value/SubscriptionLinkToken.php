<?php

namespace NnShop\Domain\Orders\Value;

use Core\Domain\Value\AbstractTokenValue;

class SubscriptionLinkToken extends AbstractTokenValue
{
    /**
     * {@inheritdoc}
     */
    const LENGTH = 40;

    /**
     * @param $input
     *
     * @return bool
     */
    public static function valid($input): bool
    {
        return preg_match(sprintf('/^[A-Z0-9]{%d}$/', static::LENGTH), $input) > 0;
    }
}
