<?php

namespace NnShop\Domain\Orders\Value;

use Core\Domain\Value\AbstractStringValue;
use Core\Domain\Value\Web\EmailAddress;

class CustomerEmailHash extends AbstractStringValue
{
    /**
     * @param EmailAddress $address
     *
     * @return CustomerEmailHash
     */
    public static function generate(EmailAddress $address): self
    {
        return new self(sha1($address->getString() . crc32($address->getLocal())));
    }

    /**
     * @param EmailAddress $address
     *
     * @return bool
     */
    public function matches(EmailAddress $address): bool
    {
        return $this->equals(static::generate($address));
    }
}
