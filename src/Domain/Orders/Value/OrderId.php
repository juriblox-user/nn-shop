<?php

namespace NnShop\Domain\Orders\Value;

use Core\Domain\Value\AbstractUuidValue;

class OrderId extends AbstractUuidValue
{
}
