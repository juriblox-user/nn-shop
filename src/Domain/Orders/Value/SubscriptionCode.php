<?php

namespace NnShop\Domain\Orders\Value;

use Core\Domain\Value\AbstractSequenceValue;

class SubscriptionCode extends AbstractSequenceValue
{
    /**
     * @return string
     */
    public function getString(): string
    {
        return sprintf('DSA-%06d', parent::getString());
    }
}
