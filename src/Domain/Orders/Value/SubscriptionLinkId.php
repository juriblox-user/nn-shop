<?php

namespace NnShop\Domain\Orders\Value;

use Core\Domain\Value\AbstractUuidValue;

class SubscriptionLinkId extends AbstractUuidValue
{
}
