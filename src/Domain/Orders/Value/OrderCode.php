<?php

namespace NnShop\Domain\Orders\Value;

use Core\Domain\Value\AbstractSequenceValue;

class OrderCode extends AbstractSequenceValue
{
    /**
     * @return string
     */
    public function getString(): string
    {
        return sprintf('LFD-%06d', parent::getString());
    }
}
