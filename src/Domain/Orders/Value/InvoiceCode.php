<?php

namespace NnShop\Domain\Orders\Value;

use Core\Domain\Value\AbstractYearSequenceValue;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
class InvoiceCode extends AbstractYearSequenceValue
{
    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf('DSF-%s%0' . 4 . 'd', $this->year, $this->number);
    }
}
