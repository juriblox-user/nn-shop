<?php

namespace NnShop\Domain\Orders\Repository;

use Carbon\CarbonInterval;
use Core\Domain\RepositoryInterface;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Value\OrderCode;
use NnShop\Domain\Orders\Value\OrderId;
use NnShop\Domain\Templates\Entity\QuestionStep;
use NnShop\Domain\Templates\Entity\Template;

interface OrderRepositoryInterface extends RepositoryInterface
{
    /**
     * @param int $minutesAgoDelivered
     *
     * @return array|Order[]
     */
    public function findDeliveredWithoutSentUpsellMail(int $minutesAgoDelivered): array;

    /**
     * @param int $minutesAgoDelivered
     *
     * @return array|Order[]
     */
    public function findDeliveredWithoutSentReferralMail(int $minutesAgoDelivered): array;

    /**
     * @param CarbonInterval $interval
     * @param bool           $timeframe alleen binnen een tijdvak van 1 uur kijken
     *
     * @return array|Order[]
     */
    public function findAbandoned(CarbonInterval $interval, bool $timeframe = true): array;

    /**
     * @param Customer $customer
     * @param Template $template
     *
     * @return array
     */
    public function findActiveOrdersWithTemplate(Customer $customer, Template $template): array;

    /**
     * @param QuestionStep $step
     *
     * @return array|Order[]
     */
    public function findByStep(QuestionStep $step): array;

    /**
     * @return OrderCode
     */
    public function findHighestCode();

    /**
     * @param OrderId $id
     *
     * @return Order|null
     */
    public function findOneById(OrderId $id);

    /**
     * @param Customer $customer
     * @param int      $limit
     *
     * @return array|Order[]
     */
    public function findRecentOrders(Customer $customer, int $limit = 5): array;

    /**
     * @param OrderId $id
     *
     * @return Order
     */
    public function getById(OrderId $id): Order;

    /**
     * @param OrderId $id
     *
     * @return Order
     */
    public function getReference(OrderId $id): Order;

    /**
     * @param OrderCode $code
     *
     * @return bool
     */
    public function hasWithCode(OrderCode $code): bool;
}
