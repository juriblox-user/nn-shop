<?php

namespace NnShop\Domain\Orders\Repository;

use Core\Domain\RepositoryInterface;
use NnShop\Domain\Orders\Entity\SubscriptionLink;
use NnShop\Domain\Orders\Value\SubscriptionLinkId;
use NnShop\Domain\Orders\Value\SubscriptionLinkToken;

interface SubscriptionLinkRepository extends RepositoryInterface
{
    /**
     * @param SubscriptionLinkId $id
     *
     * @return SubscriptionLink|null
     */
    public function findOneById(SubscriptionLinkId $id);

    /**
     * @param SubscriptionLinkToken $token
     *
     * @return SubscriptionLink|null
     */
    public function findOneByToken(SubscriptionLinkToken $token);

    /**
     * @param SubscriptionLinkId $id
     *
     * @return SubscriptionLink
     */
    public function getById(SubscriptionLinkId $id): SubscriptionLink;

    /**
     * @param SubscriptionLinkToken $token
     *
     * @return SubscriptionLink
     */
    public function getByToken(SubscriptionLinkToken $token): SubscriptionLink;

    /**
     * @param SubscriptionLinkId $id
     *
     * @return SubscriptionLink
     */
    public function getReference(SubscriptionLinkId $id): SubscriptionLink;

    /**
     * @param SubscriptionLinkToken $token
     *
     * @return bool
     */
    public function hasWithToken(SubscriptionLinkToken $token): bool;
}
