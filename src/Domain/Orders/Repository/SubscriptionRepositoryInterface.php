<?php

namespace NnShop\Domain\Orders\Repository;

use Core\Domain\RepositoryInterface;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Entity\Subscription;
use NnShop\Domain\Orders\Value\SubscriptionCode;
use NnShop\Domain\Orders\Value\SubscriptionId;

interface SubscriptionRepositoryInterface extends RepositoryInterface
{
    /**
     * @return SubscriptionCode
     */
    public function findHighestCode();

    /**
     * @param SubscriptionId $id
     *
     * @return Subscription|null
     */
    public function findOneById(SubscriptionId $id);

    /**
     * @param Order $order
     *
     * @return Subscription|null
     */
    public function findOneByOrder(Order $order);

    /**
     * @param SubscriptionId $id
     *
     * @return Subscription
     */
    public function getById(SubscriptionId $id): Subscription;

    /**
     * @param Order $order
     *
     * @return Subscription
     */
    public function getByOrder(Order $order): Subscription;

    /**
     * @param SubscriptionId $id
     *
     * @return Subscription
     */
    public function getReference(SubscriptionId $id): Subscription;

    /**
     * @param SubscriptionCode $code
     *
     * @return bool
     */
    public function hasWithCode(SubscriptionCode $code): bool;
}
