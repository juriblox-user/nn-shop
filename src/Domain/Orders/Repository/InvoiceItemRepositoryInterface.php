<?php

namespace NnShop\Domain\Orders\Repository;

use Core\Domain\RepositoryInterface;
use NnShop\Domain\Orders\Entity\InvoiceItem;
use NnShop\Domain\Orders\Value\InvoiceItemId;

interface InvoiceItemRepositoryInterface extends RepositoryInterface
{
    /**
     * @param InvoiceItemId $id
     *
     * @return InvoiceItem|null
     */
    public function findOneById(InvoiceItemId $id);

    /**
     * @param InvoiceItemId $id
     *
     * @return InvoiceItem
     */
    public function getById(InvoiceItemId $id): InvoiceItem;

    /**
     * @param InvoiceItemId $id
     *
     * @return InvoiceItem
     */
    public function getReference(InvoiceItemId $id): InvoiceItem;
}
