<?php

namespace NnShop\Domain\Orders\Repository;

use Core\Domain\RepositoryInterface;
use NnShop\Domain\Orders\Entity\Refund;
use NnShop\Domain\Orders\Value\RefundId;

interface RefundRepositoryInterface extends RepositoryInterface
{
    /**
     * @param RefundId $id
     *
     * @return Refund|null
     */
    public function findOneById(RefundId $id);

    /**
     * @param RefundId $id
     *
     * @return Refund
     */
    public function getById(RefundId $id): Refund;

    /**
     * @param RefundId $id
     *
     * @return Refund
     */
    public function getReference(RefundId $id): Refund;
}
