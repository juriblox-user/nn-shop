<?php

namespace NnShop\Domain\Orders\Repository;

use Core\Domain\RepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;
use NnShop\Domain\Orders\Entity\Discount;
use NnShop\Domain\Orders\Value\DiscountId;

interface DiscountRepositoryInterface extends RepositoryInterface
{
    /**
     * @return Discount[]
     */
    public function findAllForMultipleUse(): array;

    /**
     * @param string $code
     *
     * @return Discount|null
     */
    public function findOneByCode(string $code);

    /**
     * @param DiscountId $id
     *
     * @return Discount|null
     */
    public function findOneById(DiscountId $id);

    /**
     * @param string $code
     *
     * @return Discount
     */
    public function getByCode(string $code): Discount;

    /**
     * @param DiscountId $id
     *
     * @throws EntityNotFoundException
     *
     * @return Discount
     */
    public function getById(DiscountId $id): Discount;

    /**
     * @param DiscountId $id
     *
     * @return Discount
     */
    public function getReference(DiscountId $id): Discount;

    /**
     * @param string $code
     *
     * @return bool
     */
    public function hasWithCode(string $code): bool;
}
