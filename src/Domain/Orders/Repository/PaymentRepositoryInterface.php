<?php

namespace NnShop\Domain\Orders\Repository;

use Core\Domain\RepositoryInterface;
use NnShop\Domain\Mollie\Value\TransactionId;
use NnShop\Domain\Orders\Entity\Payment;
use NnShop\Domain\Orders\Value\PaymentId;

interface PaymentRepositoryInterface extends RepositoryInterface
{
    /**
     * @param PaymentId $id
     *
     * @return Payment|null
     */
    public function findOneById(PaymentId $id);

    /**
     * @return array|Payment[]
     */
    public function findPending();

    /**
     * @param PaymentId $id
     *
     * @return Payment
     */
    public function getById(PaymentId $id): Payment;

    /**
     * @param TransactionId $transactionId
     *
     * @return Payment
     */
    public function getByTransactionId(TransactionId $transactionId): Payment;

    /**
     * @param PaymentId $id
     *
     * @return Payment
     */
    public function getReference(PaymentId $id): Payment;
}
