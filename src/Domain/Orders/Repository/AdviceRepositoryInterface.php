<?php

namespace NnShop\Domain\Orders\Repository;

use Core\Domain\RepositoryInterface;
use NnShop\Domain\Orders\Entity\Advice;
use NnShop\Domain\Orders\Value\AdviceId;

interface AdviceRepositoryInterface extends RepositoryInterface
{
    /**
     * @param AdviceId $id
     *
     * @return Advice|null
     */
    public function findOneById(AdviceId $id);

    /**
     * @param AdviceId $id
     *
     * @return Advice
     */
    public function getById(AdviceId $id): Advice;

    /**
     * @param AdviceId $id
     *
     * @return Advice
     */
    public function getReference(AdviceId $id): Advice;
}
