<?php

namespace NnShop\Domain\Orders\Repository;

use Core\Domain\RepositoryInterface;
use Core\Domain\Value\Web\EmailAddress;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Value\CustomerId;

interface CustomerRepositoryInterface extends RepositoryInterface
{
    /**
     * @param EmailAddress $email
     *
     * @return Customer
     */
    public function findOneByEmail(EmailAddress $email);

    /**
     * @param CustomerId $id
     *
     * @return Customer|null
     */
    public function findOneById(CustomerId $id);

    /**
     * @param CustomerId $id
     *
     * @return Customer
     */
    public function getById(CustomerId $id): Customer;

    /**
     * @param CustomerId $id
     *
     * @return Customer
     */
    public function getReference(CustomerId $id): Customer;

    /**
     * @param EmailAddress $email
     *
     * @return bool
     */
    public function hasWithEmail(EmailAddress $email): bool;
}
