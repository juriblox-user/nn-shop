<?php

namespace NnShop\Domain\Orders\Repository;

use Core\Domain\RepositoryInterface;
use NnShop\Domain\Orders\Entity\Invoice;
use NnShop\Domain\Orders\Value\InvoiceCode;
use NnShop\Domain\Orders\Value\InvoiceId;

interface InvoiceRepositoryInterface extends RepositoryInterface
{
    /**
     * @param int $year
     *
     * @return InvoiceCode|null
     */
    public function findHighestCode(int $year);

    /**
     * @param InvoiceId $id
     *
     * @return Invoice|null
     */
    public function findOneById(InvoiceId $id);

    /**
     * @param InvoiceId $id
     *
     * @return Invoice
     */
    public function getById(InvoiceId $id): Invoice;

    /**
     * @param InvoiceId $id
     *
     * @return Invoice
     */
    public function getReference(InvoiceId $id): Invoice;

    /**
     * @param InvoiceCode $code
     *
     * @return bool
     */
    public function hasWithCode(InvoiceCode $code): bool;
}
