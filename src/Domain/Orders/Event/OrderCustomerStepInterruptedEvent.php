<?php

namespace NnShop\Domain\Orders\Event;

use NnShop\Domain\Orders\Value\OrderId;

class OrderCustomerStepInterruptedEvent
{
    /**
     * @var OrderId
     */
    private $id;

    /**
     * OrderCustomerStepInterruptedEvent constructor.
     *
     * @param OrderId $id
     */
    private function __construct(OrderId $id)
    {
        $this->id = $id;
    }

    /**
     * @param OrderId $id
     *
     * @return OrderCustomerStepInterruptedEvent
     */
    public static function create(OrderId $id): self
    {
        return new self($id);
    }

    /**
     * @return OrderId
     */
    public function getId(): OrderId
    {
        return $this->id;
    }
}
