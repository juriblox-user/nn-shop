<?php

namespace NnShop\Domain\Orders\Event;

use Carbon\Carbon;
use NnShop\Domain\Orders\Value\CustomerId;

class CustomerWentAway
{
    /**
     * @var Carbon
     */
    private $datetime;

    /**
     * @var CustomerId
     */
    private $id;

    /**
     * CustomerWentAway constructor.
     *
     * @param CustomerId $id
     * @param Carbon     $datetime
     */
    public function __construct(CustomerId $id, Carbon $datetime)
    {
        $this->id = $id;
        $this->datetime = $datetime;
    }

    /**
     * @return Carbon
     */
    public function getDatetime(): Carbon
    {
        return $this->datetime;
    }

    /**
     * @return CustomerId
     */
    public function getId(): CustomerId
    {
        return $this->id;
    }
}
