<?php

namespace NnShop\Domain\Orders\Event;

use NnShop\Domain\Orders\Value\SubscriptionId;

class SubscriptionWasCanceled
{
    /**
     * @var SubscriptionId
     */
    private $id;

    /**
     * SubscriptionWasCanceled constructor.
     *
     * @param SubscriptionId $id
     */
    public function __construct(SubscriptionId $id)
    {
        $this->id = $id;
    }

    /**
     * @return SubscriptionId
     */
    public function getId(): SubscriptionId
    {
        return $this->id;
    }
}
