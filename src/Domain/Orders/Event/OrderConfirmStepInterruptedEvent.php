<?php

namespace NnShop\Domain\Orders\Event;

use NnShop\Domain\Orders\Value\OrderId;

class OrderConfirmStepInterruptedEvent
{
    /**
     * @var OrderId
     */
    private $id;

    /**
     * OrderConfirmStepInterruptedEvent constructor.
     *
     * @param OrderId $id
     */
    private function __construct(OrderId $id)
    {
        $this->id = $id;
    }

    /**
     * @param OrderId $id
     *
     * @return OrderConfirmStepInterruptedEvent
     */
    public static function create(OrderId $id): self
    {
        return new self($id);
    }

    /**
     * @return OrderId
     */
    public function getId(): OrderId
    {
        return $this->id;
    }
}
