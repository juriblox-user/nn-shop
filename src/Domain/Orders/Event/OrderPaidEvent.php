<?php

namespace NnShop\Domain\Orders\Event;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Orders\Value\OrderId;

class OrderPaidEvent
{
    /**
     * @Serializer\Type("NnShop\Domain\Orders\Value\OrderId")
     *
     * @var OrderId
     */
    private $id;

    /**
     * OrderPaid constructor.
     *
     * @param OrderId $id
     */
    private function __construct(OrderId $id)
    {
        $this->id = $id;
    }

    /**
     * @param OrderId $id
     *
     * @return OrderPaidEvent
     */
    public static function create(OrderId $id): self
    {
        return new self($id);
    }

    /**
     * @return OrderId
     */
    public function getId(): OrderId
    {
        return $this->id;
    }
}
