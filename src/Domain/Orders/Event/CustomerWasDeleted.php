<?php

namespace NnShop\Domain\Orders\Event;

use NnShop\Domain\Orders\Value\CustomerId;

class CustomerWasDeleted
{
    /**
     * @var CustomerId
     */
    private $id;

    /**
     * CustomerWasDeleted constructor.
     *
     * @param CustomerId $id
     */
    public function __construct(CustomerId $id)
    {
        $this->id = $id;
    }

    /**
     * @return CustomerId
     */
    public function getId(): CustomerId
    {
        return $this->id;
    }
}
