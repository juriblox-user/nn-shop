<?php

namespace NnShop\Domain\Orders\Event;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Orders\Value\PaymentId;

class PaymentReceivedEvent
{
    /**
     * @Serializer\Type("NnShop\Domain\Orders\Value\PaymentId")
     *
     * @var PaymentId
     */
    private $id;

    /**
     * PaymentReceived constructor.
     *
     * @param PaymentId $id
     */
    private function __construct(PaymentId $id)
    {
        $this->id = $id;
    }

    /**
     * @param PaymentId $id
     *
     * @return PaymentReceivedEvent
     */
    public static function create(PaymentId $id): self
    {
        return new self($id);
    }

    /**
     * @return PaymentId
     */
    public function getId(): PaymentId
    {
        return $this->id;
    }
}
