<?php

namespace NnShop\Domain\Orders\Event;

use NnShop\Domain\Orders\Value\RefundId;

class RefundWasRequested
{
    /**
     * @var RefundId
     */
    private $id;

    /**
     * RefundWasRequested constructor.
     *
     * @param RefundId $id
     */
    public function __construct(RefundId $id)
    {
        $this->id = $id;
    }

    /**
     * @return RefundId
     */
    public function getId(): RefundId
    {
        return $this->id;
    }
}
