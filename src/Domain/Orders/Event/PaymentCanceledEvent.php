<?php

namespace NnShop\Domain\Orders\Event;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Orders\Value\PaymentId;

class PaymentCanceledEvent
{
    /**
     * @Serializer\Type("NnShop\Domain\Orders\Value\PaymentId")
     *
     * @var PaymentId
     */
    private $id;

    /**
     * PaymentCanceled constructor.
     *
     * @param PaymentId $id
     */
    private function __construct(PaymentId $id)
    {
        $this->id = $id;
    }

    /**
     * @param PaymentId $id
     *
     * @return PaymentCanceledEvent
     */
    public static function create(PaymentId $id): self
    {
        return new self($id);
    }

    /**
     * @return PaymentId
     */
    public function getId(): PaymentId
    {
        return $this->id;
    }
}
