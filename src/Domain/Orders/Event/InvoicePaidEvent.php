<?php

namespace NnShop\Domain\Orders\Event;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Orders\Value\InvoiceId;

class InvoicePaidEvent
{
    /**
     * @Serializer\Type("NnShop\Domain\Orders\Value\InvoiceId")
     *
     * @var InvoiceId
     */
    private $id;

    /**
     * InvoicePaid constructor.
     *
     * @param InvoiceId $id
     */
    private function __construct(InvoiceId $id)
    {
        $this->id = $id;
    }

    /**
     * @param InvoiceId $id
     *
     * @return InvoicePaidEvent
     */
    public static function create(InvoiceId $id): self
    {
        return new self($id);
    }

    /**
     * @return InvoiceId
     */
    public function getId(): InvoiceId
    {
        return $this->id;
    }
}
