<?php

namespace NnShop\Domain\Orders\Event;

use NnShop\Domain\Orders\Value\OrderId;

class OrderWasCanceled
{
    /**
     * @var OrderId
     */
    private $id;

    /**
     * OrderWasCanceled constructor.
     *
     * @param OrderId $id
     */
    public function __construct(OrderId $id)
    {
        $this->id = $id;
    }

    /**
     * @return OrderId
     */
    public function getId(): OrderId
    {
        return $this->id;
    }
}
