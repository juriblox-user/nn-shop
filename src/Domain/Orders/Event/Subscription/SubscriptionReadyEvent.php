<?php

namespace NnShop\Domain\Orders\Event\Subscription;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Orders\Value\SubscriptionId;

class SubscriptionReadyEvent
{
    /**
     * @Serializer\Type("NnShop\Domain\Orders\Value\SubscriptionId")
     *
     * @var SubscriptionId
     */
    private $id;

    /**
     * SubscriptionCreatedEvent constructor.
     *
     * @param SubscriptionId $id
     */
    private function __construct(SubscriptionId $id)
    {
        $this->id = $id;
    }

    /**
     * @param SubscriptionId $id
     *
     * @return SubscriptionReadyEvent
     */
    public static function create(SubscriptionId $id): self
    {
        return new self($id);
    }

    /**
     * @return SubscriptionId
     */
    public function getId(): SubscriptionId
    {
        return $this->id;
    }
}
