<?php

namespace NnShop\Domain\Orders\Event\Customer;

use Core\Domain\Value\Geography\Address;
use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Orders\Value\CustomerId;

class CustomerAddressChangedEvent
{
    /**
     * @Serializer\Type("NnShop\Domain\Orders\Value\CustomerId")
     *
     * @var CustomerId
     */
    private $id;

    /**
     * @Serializer\Type("Core\Domain\Value\Geography\Address")
     *
     * @var Address
     */
    private $previous;

    /**
     * @Serializer\Type("Core\Domain\Value\Geography\Address")
     *
     * @var Address
     */
    private $updated;

    /**
     * CustomerBillingAddressWasChanged constructor.
     *
     * @param CustomerId $id
     * @param Address    $previous
     * @param Address    $updated
     */
    private function __construct(CustomerId $id, Address $previous, Address $updated)
    {
        $this->id = $id;

        $this->previous = $previous;
        $this->updated = $updated;
    }

    /**
     * @return CustomerId
     */
    public function getId(): CustomerId
    {
        return $this->id;
    }

    /**
     * @return Address
     */
    public function getPrevious(): Address
    {
        return $this->previous;
    }

    /**
     * @return Address
     */
    public function getUpdated(): Address
    {
        return $this->updated;
    }

    /**
     * @param CustomerId $id
     * @param Address    $previous
     * @param Address    $updated
     *
     * @return CustomerAddressChangedEvent
     */
    public static function create(CustomerId $id, Address $previous, Address $updated): self
    {
        return new static($id, $previous, $updated);
    }
}
