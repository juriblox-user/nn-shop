<?php

namespace NnShop\Domain\Orders\Event\Customer;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Orders\Value\CustomerId;

class CustomerUpdatedEvent
{
    /**
     * @Serializer\Type("NnShop\Domain\Orders\Value\CustomerId")
     *
     * @var CustomerId
     */
    private $id;

    /**
     * @param CustomerId $id
     */
    private function __construct(CustomerId $id)
    {
        $this->id = $id;
    }

    /**
     * @param CustomerId $id
     *
     * @return CustomerUpdatedEvent
     */
    public static function create(CustomerId $id): self
    {
        return new static($id);
    }

    /**
     * @return CustomerId
     */
    public function getId(): CustomerId
    {
        return $this->id;
    }
}
