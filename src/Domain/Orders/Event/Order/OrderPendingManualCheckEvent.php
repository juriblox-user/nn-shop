<?php

namespace NnShop\Domain\Orders\Event\Order;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Orders\Value\OrderId;

class OrderPendingManualCheckEvent
{
    /**
     * @Serializer\Type("NnShop\Domain\Orders\Value\OrderId")
     *
     * @var OrderId
     */
    private $id;

    /**
     * OrderPendingManualCheckEvent constructor.
     *
     * @param OrderId $id
     */
    private function __construct(OrderId $id)
    {
        $this->id = $id;
    }

    /**
     * @param OrderId $id
     *
     * @return OrderPendingManualCheckEvent
     */
    public static function create(OrderId $id): self
    {
        return new self($id);
    }

    /**
     * @return OrderId
     */
    public function getId(): OrderId
    {
        return $this->id;
    }
}
