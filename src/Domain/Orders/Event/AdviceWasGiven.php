<?php

namespace NnShop\Domain\Orders\Event;

use NnShop\Domain\Orders\Value\AdviceId;

class AdviceWasGiven
{
    /**
     * @var AdviceId
     */
    private $id;

    /**
     * AdviceWasGiven constructor.
     *
     * @param AdviceId $id
     */
    public function __construct(AdviceId $id)
    {
        $this->id = $id;
    }

    /**
     * @return AdviceId
     */
    public function getId(): AdviceId
    {
        return $this->id;
    }
}
