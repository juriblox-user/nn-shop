<?php

namespace NnShop\Domain\Orders\Event;

use NnShop\Domain\Orders\Value\OrderId;

class OrderHasFailed
{
    /**
     * @var OrderId
     */
    private $id;

    /**
     * OrderHasFailed constructor.
     *
     * @param OrderId $id
     */
    public function __construct(OrderId $id)
    {
        $this->id = $id;
    }

    /**
     * @return OrderId
     */
    public function getId(): OrderId
    {
        return $this->id;
    }
}
