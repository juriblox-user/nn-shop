<?php

namespace NnShop\Domain\Orders\Event;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Orders\Value\CustomerId;

class EmailChangeCompletedEvent
{
    /**
     * @Serializer\Type("NnShop\Domain\Orders\Value\CustomerId")
     *
     * @var CustomerId
     */
    private $id;

    /**
     * Constructor.
     *
     * @param CustomerId $id
     */
    public function __construct(CustomerId $id)
    {
        $this->id = $id;
    }

    /**
     * @param CustomerId $id
     *
     * @return EmailChangeCompletedEvent
     */
    public static function create(CustomerId $id): self
    {
        return new self($id);
    }

    /**
     * @return CustomerId
     */
    public function getId(): CustomerId
    {
        return $this->id;
    }
}
