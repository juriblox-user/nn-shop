<?php

namespace NnShop\Domain\Orders\Event;

use NnShop\Domain\Orders\Value\OrderId;

class OrderHasErrored
{
    /**
     * @var OrderId
     */
    private $id;

    /**
     * OrderHasErrored constructor.
     *
     * @param OrderId $id
     */
    public function __construct(OrderId $id)
    {
        $this->id = $id;
    }

    /**
     * @return OrderId
     */
    public function getId(): OrderId
    {
        return $this->id;
    }
}
