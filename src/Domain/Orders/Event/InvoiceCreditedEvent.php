<?php

namespace NnShop\Domain\Orders\Event;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Orders\Value\InvoiceId;

class InvoiceCreditedEvent
{
    /**
     * @Serializer\Type("NnShop\Domain\Orders\Value\InvoiceId")
     *
     * @var InvoiceId
     */
    private $creditId;

    /**
     * @Serializer\Type("NnShop\Domain\Orders\Value\InvoiceId")
     *
     * @var InvoiceId
     */
    private $invoiceId;

    /**
     * InvoiceWasCredited constructor.
     *
     * @param InvoiceId $invoiceId
     * @param InvoiceId $creditId
     */
    private function __construct(InvoiceId $invoiceId, InvoiceId $creditId)
    {
        $this->invoiceId = $invoiceId;
        $this->creditId = $creditId;
    }

    /**
     * @param InvoiceId $invoiceId
     * @param InvoiceId $creditId
     *
     * @return InvoiceCreditedEvent
     */
    public static function create(InvoiceId $invoiceId, InvoiceId $creditId): self
    {
        return new self($invoiceId, $creditId);
    }

    /**
     * @return InvoiceId
     */
    public function getCreditId(): InvoiceId
    {
        return $this->creditId;
    }

    /**
     * @return InvoiceId
     */
    public function getInvoiceId(): InvoiceId
    {
        return $this->invoiceId;
    }
}
