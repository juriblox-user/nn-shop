<?php

namespace NnShop\Domain\Orders\Enumeration;

use Core\Domain\Enumeration\AbstractEnumeration;

class SubscriptionLinkFormat extends AbstractEnumeration
{
    /**
     * Markdown-formaat.
     */
    const MARKDOWN = 'markdown';

    /**
     * PDF-formaat.
     */
    const PDF = 'pdf';

    /**
     * Word-2007 formaat.
     */
    const WORD2007 = 'word2007';
}
