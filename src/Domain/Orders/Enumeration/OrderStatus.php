<?php

namespace NnShop\Domain\Orders\Enumeration;

use Core\Component\Flow\StateInterface;
use Core\Domain\Enumeration\AbstractEnumeration;

class OrderStatus extends AbstractEnumeration implements StateInterface
{
    /**
     * Bestelling is geannuleerd.
     */
    const CANCELED = 'canceled';

    /**
     * Bestelling is afgerond.
     */
    const COMPLETED = 'completed';

    /**
     * Document afleveren bij de klant.
     */
    const DELIVER = 'deliver';

    /**
     * Concept.
     */
    const DRAFT = 'draft';

    /**
     * Bestelling is mislukt.
     */
    const FAILED = 'failed';

    /**
     * In afwachting van de handmatige controle.
     */
    const MANUAL_CHECK = 'manual_check';

    /**
     * In afwachting van de handmatige aanpassingen.
     */
    const MANUAL_CUSTOM = 'manual_custom';

    /**
     * In afwachting van het document.
     */
    const PENDING = 'pending';

    /**
     * In afwachting van de betaling.
     */
    const PENDING_PAYMENT = 'pending_payment';

    /**
     * Document aanvragen bij JuriBlox.
     */
    const REQUEST = 'request';

    /**
     * Fout bij genereren door JuriBlox, opnieuw aanvragen.
     */
    const REQUEUE = 'requeue';

    /**
     * Aanmelden of bedrijfsgegevens opgeven.
     */
    const STEP_ACCOUNT = 'step_account';

    /**
     * Bevestigen.
     */
    const STEP_CONFIRM = 'step_confirm';

    /**
     * Opties.
     */
    const STEP_OPTIONS = 'step_options';

    /**
     * Vragen beantwoorden.
     */
    const STEP_QUESTIONS = 'step_questions';

    /**
     * Bestelling is vervallen.
     */
    const EXPIRED = 'expired';

    /**
     * Actieve statussen (status waarbij de klant nog iets moet doen dat niet automatisch gaat, of waar niet
     * automatisch aan wordt herinnerd).
     *
     * @var array
     */
    private static $active = [
        self::STEP_QUESTIONS,
        self::STEP_ACCOUNT,
        self::STEP_OPTIONS,
        self::STEP_CONFIRM,
    ];

    /**
     * Afgeronde statussen.
     *
     * @var array
     */
    private static $completed = [
        self::CANCELED,
        self::COMPLETED,
        self::EXPIRED,
        self::FAILED,
    ];

    /**
     * Statussen met handmatige actie.
     *
     * @var array
     */
    private static $manual = [
        self::MANUAL_CHECK,
        self::MANUAL_CUSTOM,
    ];

    /**
     * "In afwachting" statussen.
     *
     * @var array
     */
    private static $pending = [
        self::PENDING_PAYMENT,
        self::REQUEST,
        self::PENDING,
        self::REQUEUE,
        self::DELIVER,
        self::MANUAL_CHECK,
        self::MANUAL_CUSTOM,
    ];

    /**
     * Statussen waarbij er iets mis is gegaan.
     *
     * @var array
     */
    private static $warning = [
        self::REQUEUE,
    ];

    /**
     * Actieve statussen (status waarbij de klant nog iets moet doen dat niet automatisch gaat, of waar niet
     * automatisch aan wordt herinnerd).
     *
     * @return array|OrderStatus[]
     */
    public static function getActive(): array
    {
        return static::cast(self::$active);
    }

    /**
     * Afgeronde statussen.
     *
     * @return array|OrderStatus[]
     */
    public static function getCompleted(): array
    {
        return static::cast(self::$completed);
    }

    /**
     * Statussen waarbij een handmatige actie moet worden uitgevoerd.
     *
     * @return array|OrderStatus[]
     */
    public static function getManual(): array
    {
        return static::cast(self::$manual);
    }

    /**
     * "In afwachting" statussen.
     *
     * @return array|OrderStatus[]
     */
    public static function getPending(): array
    {
        return static::cast(self::$pending);
    }

    /**
     * Statussen waarbij er iets mis is gegaan.
     *
     * @return array|OrderStatus[]
     */
    public static function getWarning(): array
    {
        return static::cast(self::$warning);
    }

    /**
     * Huidige status is een "actieve" status.
     *
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->in(self::$active);
    }

    /**
     * Huidige status is een afgeronde status.
     *
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->in(self::$completed);
    }

    /**
     * Huidige status is een status waarbij een handmatige actie moet worden uitgevoerd.
     *
     * @return bool
     */
    public function isManual(): bool
    {
        return $this->in(self::$manual);
    }

    /**
     * Huidige status is een "in afwachting" status.
     *
     * @return bool
     */
    public function isPending(): bool
    {
        return $this->in(self::$pending);
    }

    /**
     * Huidige status is een status waarbij er iets mis is gegaan.
     *
     * @return bool
     */
    public function isWarning(): bool
    {
        return $this->in(self::$warning);
    }
}
