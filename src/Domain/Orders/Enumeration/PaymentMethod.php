<?php

namespace NnShop\Domain\Orders\Enumeration;

use Core\Domain\Enumeration\AbstractEnumeration;

class PaymentMethod extends AbstractEnumeration
{
    /**
     * Bankoverschrijving.
     */
    const BANK_TRANSFER = 'bank-transfer';

    /**
     * Creditcard.
     */
    const CREDITCARD = 'creditcard';

    /**
     * SEPA-incasso.
     */
    const DIRECT_DEBIT = 'direct-debit';

    /**
     * iDEAL.
     */
    const IDEAL = 'ideal';

    /**
     * PayPal.
     */
    const PAYPAL = 'paypal';

    /**
     * BitCoin.
     */
    const BITCOIN = 'bitcoin';

    const BANK_CONTACT = 'bancontact';
    const SOFORT = 'sofort';
    const GIROPAY = 'giropay';
    const KBC = 'kbc';
    const BELFIUS = 'belfius';
    const INGHOMEPAY = 'inghomepay';
    const EPS = 'eps';
}
