<?php

namespace NnShop\Domain\Orders\Enumeration;

use Core\Domain\Enumeration\AbstractEnumeration;

class CustomerStatus extends AbstractEnumeration
{
    const ACTIVE = 'active';

    const DELETED = 'deleted';

    const DRAFT = 'draft';

    const INVITED = 'invited';

    const RECOVER = 'recover';

    /**
     * @return bool
     */
    public function allowsInteractiveLogin(): bool
    {
        return $this->in([self::ACTIVE, self::RECOVER]);
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return 'customer_status.' . $this->getValue();
    }
}
