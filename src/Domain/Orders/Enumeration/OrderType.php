<?php

namespace NnShop\Domain\Orders\Enumeration;

use Core\Domain\Enumeration\AbstractEnumeration;

class OrderType extends AbstractEnumeration
{
    /**
     * Concept.
     */
    const DRAFT = 'draft';

    /**
     * Verlenging van een bestaand abonnement.
     */
    const RENEWAL = 'renewal';

    /**
     * Nieuw abonnement.
     */
    const SUBSCRIPTION = 'subscription';

    /**
     * Nieuw los document.
     */
    const UNIT = 'unit';

    /**
     * Update van een bestaand document.
     */
    const UPDATE = 'update';
}
