<?php

namespace NnShop\Domain\Orders\Enumeration;

use Core\Domain\Enumeration\AbstractEnumeration;

class InvoiceStatus extends AbstractEnumeration
{
    /**
     * Factuur is gecrediteerd.
     */
    const CREDITED = 'credited';

    /**
     * Factuur is betaald.
     */
    const PAID = 'paid';

    /**
     * Factuur is in afwachting van betaling.
     */
    const PENDING = 'pending';

    /**
     * Proforma factuur.
     */
    const PROFORMA = 'proforma';

    /**
     * Factuur is vervallen.
     */
    const REVOKED = 'revoked';

    /**
     * @return bool
     */
    public function isPaid(): bool
    {
        return $this->is(self::PAID);
    }

    /**
     * @return bool
     */
    public function isPending(): bool
    {
        return $this->is(self::PENDING);
    }
}
