<?php

namespace NnShop\Domain\Orders\Enumeration;

use Core\Domain\Enumeration\AbstractEnumeration;

class CustomerType extends AbstractEnumeration
{
    /**
     * Zakelijke klant.
     */
    const BUSINESS = 'business';

    /**
     * Consument.
     */
    const CONSUMER = 'consumer';

    /**
     * Onbekend.
     */
    const UNKNOWN = 'unknown';
}
