<?php

namespace NnShop\Domain\Orders\Enumeration;

use Core\Domain\Enumeration\AbstractEnumeration;

class DiscountType extends AbstractEnumeration
{
    /**
     * Vast bedrag.
     */
    const FIXED = 'fixed';

    /**
     * Percentage.
     */
    const PERCENTAGE = 'percentage';
}
