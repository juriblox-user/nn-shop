<?php

namespace NnShop\Domain\Orders\Enumeration;

use Core\Domain\Enumeration\AbstractEnumeration;

class OrderTransition extends AbstractEnumeration
{
    /**
     * Bestelling annuleren.
     */
    const CANCEL = 'cancel';

    /**
     * Van handmatige controle naar maatwerk aanpassingen.
     */
    const CHECK_TO_CUSTOM = 'check_to_custom';

    /**
     * Bestelling afronden.
     */
    const COMPLETE = 'complete';

    /**
     * Naar de stap na het aanmelden.
     */
    const COMPLETE_ACCOUNT = 'complete_account';

    /**
     * Van handmatige controle naar afgerond.
     */
    const COMPLETE_CHECK = 'complete_check';

    /**
     * Naar de stap na het bevestigen van de bestelling.
     */
    const COMPLETE_CONFIRM = 'complete_confirm';

    /**
     * Van maatwerk aanpassingen naar afgerond.
     */
    const COMPLETE_CUSTOM = 'complete_custom';

    /**
     * Naar de stap na het kiezen van de opties van de bestelling.
     */
    const COMPLETE_OPTIONS = 'complete_options';

    /**
     * Naar de stap na het beantwoorden van de vragen.
     */
    const COMPLETE_QUESTIONS = 'complete_questions';

    /**
     * Document afleveren.
     */
    const DELIVER = 'deliver';

    /**
     * Bestelling laten verlopen.
     */
    const EXPIRE = 'expire';

    /**
     * Bestelling is mislukt na 'm opnieuw te hebben geprobeerd.
     */
    const FAIL = 'fail';

    /**
     * Handmatige controle uitvoeren.
     */
    const MANUAL_CHECK = 'manual_check';

    /**
     * Handmatige aanpassingen uitvoeren.
     */
    const MANUAL_CUSTOM = 'manual_custom';

    /**
     * Document aanvragen.
     */
    const REQUEST = 'request';

    /**
     * Fout bij het genereren van het document.
     */
    const REQUEST_FAILED = 'request_failed';

    /**
     * Document is aangevraagd.
     */
    const REQUEST_SENT = 'request_sent';

    /**
     * Opnieuw proberen om het document te genereren.
     */
    const RETRY_REQUEST = 'retry_request';

    /**
     * Stap terug vanaf aanmelden/bedrijfsgegevens.
     */
    const REVERT_ACCOUNT = 'revert_account';

    /**
     * Stap terug vanaf bestelling bevestigen.
     */
    const REVERT_CONFIRM = 'revert_confirm';

    /**
     * Stap terug vanaf opties kiezen.
     */
    const REVERT_OPTIONS = 'revert_options';

    /**
     * Bestelling starten.
     */
    const START = 'start';
}
