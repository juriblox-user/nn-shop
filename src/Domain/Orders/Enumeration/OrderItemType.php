<?php

namespace NnShop\Domain\Orders\Enumeration;

use Core\Domain\Enumeration\AbstractEnumeration;

class OrderItemType extends AbstractEnumeration
{
    /**
     * Document.
     */
    const DOCUMENT = 'document';

    /**
     * Additional Service.
     */
    const ADDITIONAL_SERVICE = 'additional_service';
}
