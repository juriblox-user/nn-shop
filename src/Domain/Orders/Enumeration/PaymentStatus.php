<?php

namespace NnShop\Domain\Orders\Enumeration;

use Core\Domain\Enumeration\AbstractEnumeration;

class PaymentStatus extends AbstractEnumeration
{
    /**
     * Betaling is geannuleerd.
     */
    const CANCELED = 'canceled';

    /**
     * Betaling is verlopen.
     */
    const EXPIRED = 'expired';

    /**
     * Betaling is in afwachting.
     */
    const PENDING = 'pending';

    /**
     * Betaling is ontvangen.
     */
    const RECEIVED = 'received';
}
