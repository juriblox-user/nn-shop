<?php

namespace NnShop\Domain\Orders\Command\Payment;

use NnShop\Domain\Mollie\Value\TransactionId;

class SynchronizeTransactionCommand
{
    /**
     * @var TransactionId
     */
    private $id;

    /**
     * SynchronizeTransactionCommand constructor.
     *
     * @param TransactionId $id
     */
    public function __construct(TransactionId $id)
    {
        $this->id = $id;
    }

    /**
     * @param TransactionId $id
     *
     * @return SynchronizeTransactionCommand
     */
    public static function create(TransactionId $id): self
    {
        return new self($id);
    }

    /**
     * @return TransactionId
     */
    public function getId(): TransactionId
    {
        return $this->id;
    }
}
