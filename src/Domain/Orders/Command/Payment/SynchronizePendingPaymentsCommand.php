<?php

namespace NnShop\Domain\Orders\Command\Payment;

use NnShop\Domain\Orders\Value\InvoiceId;

class SynchronizePendingPaymentsCommand
{
    /**
     * @var InvoiceId|null
     */
    private $invoiceId;

    /**
     * SynchronizePendingPaymentsCommand constructor.
     */
    private function __construct()
    {
    }

    /**
     * @param InvoiceId $invoiceId
     *
     * @return SynchronizePendingPaymentsCommand
     */
    public static function create(InvoiceId $invoiceId = null): self
    {
        $command = new self();
        $command->invoiceId = $invoiceId;

        return $command;
    }

    /**
     * @return InvoiceId|null
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }
}
