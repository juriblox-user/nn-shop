<?php

namespace NnShop\Domain\Orders\Command\Discount;

use NnShop\Domain\Orders\Value\DiscountId;

class DeleteDiscountCommand
{
    /**
     * @var DiscountId
     */
    private $id;

    /**
     * @param DiscountId $id
     */
    private function __construct(DiscountId $id)
    {
        $this->id = $id;
    }

    /**
     * @param DiscountId $id
     *
     * @return self
     */
    public static function create(DiscountId $id): self
    {
        return new self($id);
    }

    /**
     * @return DiscountId
     */
    public function getId(): DiscountId
    {
        return $this->id;
    }
}
