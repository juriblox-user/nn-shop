<?php

namespace NnShop\Domain\Orders\Command\Discount;

use Doctrine\Common\Collections\Collection;
use NnShop\Domain\Orders\Enumeration\DiscountType;
use NnShop\Domain\Orders\Value\DiscountId;
use Money\Money;
use Stringy\Stringy;

class CreateDiscountCommand
{
    /**
     * @var Money
     */
    private $amount;

    /**
     * @var string
     */
    private $code;

    /**
     * @var DiscountId
     */
    private $id;

    /**
     * @var int
     */
    private $percentage;

    /**
     * @var string
     */
    private $title;

    /**
     * @var DiscountType
     */
    private $type;

    /**
     * @var int|null
     */
    private $maxUses;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var Collection|null
     */
    private $emails;

    /**
     * @var Collection|null
     */
    private $templates;

    /**
     * @param DiscountId $id
     */
    private function __construct(DiscountId $id)
    {
        $this->id = $id;
    }

    /**
     * @param DiscountId $id
     *
     * @return CreateDiscountCommand
     */
    public static function prepare(DiscountId $id): self
    {
        return new self($id);
    }

    /**
     * @return Money
     */
    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return DiscountId
     */
    public function getId(): DiscountId
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPercentage(): int
    {
        return $this->percentage;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return DiscountType
     */
    public function getType(): DiscountType
    {
        return $this->type;
    }

    /**
     * @param Money $amount
     */
    public function setAmount(Money $amount)
    {
        if (null !== $this->type && $this->type->isNot(DiscountType::FIXED)) {
            throw new \LogicException(sprintf('You cannot switch a discount type from %s to FIXED_AMOUNT', $this->type));
        }

        $this->type = DiscountType::from(DiscountType::FIXED);
        $this->amount = $amount;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->code = (string) Stringy::create($code)
            ->slugify()
            ->toUpperCase();
    }

    /**
     * @param int $percentage
     */
    public function setPercentage(int $percentage)
    {
        if (null !== $this->type && $this->type->isNot(DiscountType::PERCENTAGE)) {
            throw new \LogicException(sprintf('You cannot switch a discount type from %s to PERCENTAGE', $this->type));
        }

        $this->type = DiscountType::from(DiscountType::PERCENTAGE);
        $this->percentage = $percentage;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * @return int|null
     */
    public function getMaxUses()
    {
        return $this->maxUses;
    }

    /**
     * @param int|null $maxUses
     *
     * @return CreateDiscountCommand
     */
    public function setMaxUses($maxUses)
    {
        $this->maxUses = $maxUses;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     *
     * @return CreateDiscountCommand
     */
    public function setActive(bool $active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * @param Collection $emails
     *
     * @return CreateDiscountCommand
     */
    public function setEmails(Collection $emails): self
    {
        $this->emails = $emails;

        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getTemplates()
    {
        return $this->templates;
    }

    /**
     * @param Collection|null $templates
     *
     * @return self
     */
    public function setTemplates($templates): self
    {
        $this->templates = $templates;

        return $this;
    }
}
