<?php

namespace NnShop\Domain\Orders\Command\Discount;

use Doctrine\Common\Collections\Collection;
use NnShop\Domain\Orders\Entity\Discount;
use NnShop\Domain\Orders\Enumeration\DiscountType;
use NnShop\Domain\Orders\Value\DiscountId;
use Money\Money;

class UpdateDiscountCommand
{
    /**
     * @var DiscountId
     */
    private $id;

    /**
     * @var DiscountType
     */
    private $type;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $code;

    /**
     * @var Money
     */
    private $amount;

    /**
     * @var int
     */
    private $percentage;

    /**
     * @var int|null
     */
    private $maxUses;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var Collection|null
     */
    private $templates;

    /**
     * @var Collection|null
     */
    private $emails;

    /**
     * @param DiscountId $id
     */
    private function __construct(DiscountId $id)
    {
        $this->id = $id;
    }

    /**
     * @param Discount $discount
     *
     * @throws \Core\Common\Exceptions\MissingValueException
     *
     * @return UpdateDiscountCommand
     */
    public static function prepare(Discount $discount)
    {
        $command = new self($discount->getId());
        $command->type = $discount->getType();

        if ($command->type->is(DiscountType::FIXED)) {
            $command->amount = $discount->getAmount();
        } else {
            $command->percentage = $discount->getPercentage();
        }

        $command->title = $discount->getTitle();
        $command->code = $discount->getCode();

        return $command;
    }

    /**
     * @return DiscountId
     */
    public function getId(): DiscountId
    {
        return $this->id;
    }

    /**
     * @return DiscountType
     */
    public function getType(): DiscountType
    {
        return $this->type;
    }

    /**
     * @param DiscountType $type
     */
    public function setType(DiscountType $type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code ?: null;
    }

    /**
     * @return Money
     */
    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * @param Money $amount
     */
    public function setAmount(Money $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getPercentage(): int
    {
        return $this->percentage;
    }

    /**
     * @param int $percentage
     */
    public function setPercentage(int $percentage)
    {
        $this->percentage = $percentage;
    }

    /**
     * @return int|null
     */
    public function getMaxUses()
    {
        return $this->maxUses;
    }

    /**
     * @param int|null $maxUses
     *
     * @return UpdateDiscountCommand
     */
    public function setMaxUses($maxUses)
    {
        $this->maxUses = $maxUses;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     *
     * @return UpdateDiscountCommand
     */
    public function setActive(bool $active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getTemplates()
    {
        return $this->templates;
    }

    /**
     * @param Collection|null $templates
     *
     * @return UpdateDiscountCommand
     */
    public function setTemplates($templates)
    {
        $this->templates = $templates;

        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * @param Collection $emails
     *
     * @return UpdateDiscountCommand
     */
    public function setEmails(Collection $emails)
    {
        $this->emails = $emails;

        return $this;
    }
}
