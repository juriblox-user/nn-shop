<?php

namespace NnShop\Domain\Orders\Command\Subscription;

use NnShop\Domain\Orders\Value\OrderId;
use NnShop\Domain\Orders\Value\SubscriptionId;

class CreateSubscriptionCommand
{
    /**
     * @var SubscriptionId
     */
    private $id;

    /**
     * @var OrderId
     */
    private $order;

    /**
     * CreateSubscriptionCommand constructor.
     *
     * @param SubscriptionId $id
     * @param OrderId        $order
     */
    private function __construct(SubscriptionId $id, OrderId $order)
    {
        $this->id = $id;
        $this->order = $order;
    }

    /**
     * @param SubscriptionId $id
     * @param OrderId        $orderId
     *
     * @return CreateSubscriptionCommand
     */
    public static function create(SubscriptionId $id, OrderId $orderId): self
    {
        return new self($id, $orderId);
    }

    /**
     * @return SubscriptionId
     */
    public function getId(): SubscriptionId
    {
        return $this->id;
    }

    /**
     * @return OrderId
     */
    public function getOrder(): OrderId
    {
        return $this->order;
    }
}
