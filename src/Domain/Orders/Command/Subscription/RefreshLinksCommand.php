<?php

namespace NnShop\Domain\Orders\Command\Subscription;

use NnShop\Domain\Orders\Value\SubscriptionId;

class RefreshLinksCommand
{
    /**
     * @var SubscriptionId
     */
    private $id;

    /**
     * RefreshLinksCommand constructor.
     *
     * @param SubscriptionId $id
     */
    private function __construct(SubscriptionId $id)
    {
        $this->id = $id;
    }

    /**
     * @param SubscriptionId $id
     *
     * @return RefreshLinksCommand
     */
    public static function create(SubscriptionId $id): self
    {
        return new self($id);
    }

    /**
     * @return SubscriptionId
     */
    public function getId(): SubscriptionId
    {
        return $this->id;
    }
}
