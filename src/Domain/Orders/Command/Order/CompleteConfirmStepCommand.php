<?php

namespace NnShop\Domain\Orders\Command\Order;

use NnShop\Domain\Orders\Enumeration\PaymentMethod;
use NnShop\Domain\Orders\Value\OrderId;

class CompleteConfirmStepCommand
{
    /**
     * @var OrderId
     */
    private $id;

    /**
     * @var PaymentMethod
     */
    private $paymentMethod;

    /**
     * CompleteConfirmStateCommand constructor.
     *
     * @param OrderId $id
     */
    private function __construct(OrderId $id)
    {
        $this->id = $id;
    }

    /**
     * @return OrderId
     */
    public function getId(): OrderId
    {
        return $this->id;
    }

    /**
     * @return PaymentMethod
     */
    public function getPaymentMethod(): PaymentMethod
    {
        return $this->paymentMethod;
    }

    /**
     * @param OrderId $id
     *
     * @return CompleteConfirmStepCommand
     */
    public static function prepare(OrderId $id): self
    {
        return new self($id);
    }

    /**
     * @param PaymentMethod $method
     */
    public function setPaymentMethod(PaymentMethod $method)
    {
        $this->paymentMethod = $method;
    }
}
