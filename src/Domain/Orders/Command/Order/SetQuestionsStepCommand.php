<?php

namespace NnShop\Domain\Orders\Command\Order;

use NnShop\Domain\Orders\Value\OrderId;
use NnShop\Domain\Templates\Value\QuestionStepId;

class SetQuestionsStepCommand
{
    /**
     * @var OrderId
     */
    private $orderId;

    /**
     * @var QuestionStepId
     */
    private $stepId;

    /**
     * SetQuestionStepCommand constructor.
     *
     * @param OrderId        $orderId
     * @param QuestionStepId $stepId
     */
    private function __construct(OrderId $orderId, QuestionStepId $stepId)
    {
        $this->orderId = $orderId;
        $this->stepId = $stepId;
    }

    /**
     * @param OrderId        $orderId
     * @param QuestionStepId $stepId
     *
     * @return SetQuestionsStepCommand
     */
    public static function create(OrderId $orderId, QuestionStepId $stepId): self
    {
        return new self($orderId, $stepId);
    }

    /**
     * @return OrderId
     */
    public function getOrderId(): OrderId
    {
        return $this->orderId;
    }

    /**
     * @return QuestionStepId
     */
    public function getStepId(): QuestionStepId
    {
        return $this->stepId;
    }
}
