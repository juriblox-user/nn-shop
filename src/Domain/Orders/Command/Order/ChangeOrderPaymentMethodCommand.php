<?php

namespace NnShop\Domain\Orders\Command\Order;

use NnShop\Domain\Orders\Enumeration\PaymentMethod;
use NnShop\Domain\Orders\Value\OrderId;

class ChangeOrderPaymentMethodCommand
{
    /**
     * @var OrderId
     */
    private $id;

    /**
     * @var PaymentMethod|null
     */
    private $method;

    /**
     * CancelOrderCommand constructor.
     *
     * @param OrderId $id
     */
    private function __construct(OrderId $id)
    {
        $this->id = $id;
    }

    /**
     * @param OrderId $id
     *
     * @return ChangeOrderPaymentMethodCommand
     */
    public static function create(OrderId $id): self
    {
        return new self($id);
    }

    /**
     * @return OrderId
     */
    public function getId(): OrderId
    {
        return $this->id;
    }

    /**
     * @return PaymentMethod|null
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param PaymentMethod|null $method
     */
    public function setMethod(PaymentMethod $method)
    {
        $this->method = $method;
    }
}
