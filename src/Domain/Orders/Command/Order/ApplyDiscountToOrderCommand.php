<?php

namespace NnShop\Domain\Orders\Command\Order;

use NnShop\Domain\Orders\Value\DiscountId;
use NnShop\Domain\Orders\Value\OrderId;

class ApplyDiscountToOrderCommand
{
    /**
     * @var string
     */
    private $discountCode;

    /**
     * @var DiscountId
     */
    private $discountId;

    /**
     * @var OrderId
     */
    private $orderId;

    /**
     * @param OrderId $orderId
     */
    private function __construct(OrderId $orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @param OrderId $orderId
     * @param string  $discountCode
     *
     * @return ApplyDiscountToOrderCommand
     */
    public static function createWithCode(OrderId $orderId, string $discountCode): self
    {
        $command = new self($orderId);
        $command->discountCode = $discountCode;

        return $command;
    }

    /**
     * @param OrderId    $orderId
     * @param DiscountId $discountId
     *
     * @return ApplyDiscountToOrderCommand
     */
    public static function createWithId(OrderId $orderId, DiscountId $discountId): self
    {
        $command = new self($orderId);
        $command->discountId = $discountId;

        return $command;
    }

    /**
     * @return string
     */
    public function getDiscountCode()
    {
        return $this->discountCode;
    }

    /**
     * @return DiscountId
     */
    public function getDiscountId(): DiscountId
    {
        return $this->discountId;
    }

    /**
     * @return OrderId
     */
    public function getOrderId(): OrderId
    {
        return $this->orderId;
    }

    /**
     * @return bool
     */
    public function hasDiscountCode(): bool
    {
        return null !== $this->discountCode;
    }

    /**
     * @return bool
     */
    public function hasDiscountId(): bool
    {
        return null !== $this->discountId;
    }
}
