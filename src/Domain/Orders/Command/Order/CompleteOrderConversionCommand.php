<?php

namespace NnShop\Domain\Orders\Command\Order;

use Core\Common\Validation\Assertion;
use NnShop\Domain\Orders\Attribute\OrderAttribute;
use NnShop\Domain\Orders\Value\OrderId;

class CompleteOrderConversionCommand
{
    /**
     * @var string
     */
    private $attribute;

    /**
     * @var OrderId
     */
    private $id;

    /**
     * @param OrderId $id
     * @param string  $attribute
     */
    private function __construct(OrderId $id, string $attribute)
    {
        Assertion::inArray($attribute, [
            OrderAttribute::ADWORDS_CONVERSION_TRACKED,
            OrderAttribute::GOOGLE_CONVERSION_TRACKED,
            OrderAttribute::TWITTER_CONVERSION_TRACKED,
        ]);

        $this->id = $id;
        $this->attribute = $attribute;
    }

    /**
     * @param OrderId $id
     * @param string  $attribute
     *
     * @return CompleteOrderConversionCommand
     */
    public static function create(OrderId $id, string $attribute): self
    {
        return new self($id, $attribute);
    }

    /**
     * @return string
     */
    public function getAttribute(): string
    {
        return $this->attribute;
    }

    /**
     * @return OrderId
     */
    public function getId(): OrderId
    {
        return $this->id;
    }
}
