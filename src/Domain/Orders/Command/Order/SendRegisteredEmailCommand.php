<?php

namespace NnShop\Domain\Orders\Command\Order;

use Core\Domain\Value\Web\EmailAddress;
use NnShop\Domain\Orders\Value\OrderId;

final class SendRegisteredEmailCommand
{
    /**
     * @var EmailAddress
     */
    private $email;

    /**
     * @var OrderId
     */
    private $id;

    /**
     * @var string|null
     */
    private $message;

    /**
     * @var string|null
     */
    private $subject;

    /**
     * DeliverOrderCommand constructor.
     */
    private function __construct(OrderId $id)
    {
        $this->id = $id;
    }

    public static function create(OrderId $id, EmailAddress $email): self
    {
        $command = new self($id);
        $command->email = $email;

        return $command;
    }

    public function getEmail(): EmailAddress
    {
        return $this->email;
    }

    public function getId(): OrderId
    {
        return $this->id;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function setSubject(?string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }
}
