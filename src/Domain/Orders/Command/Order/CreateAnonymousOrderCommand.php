<?php

namespace NnShop\Domain\Orders\Command\Order;

use NnShop\Domain\Orders\Value\OrderId;
use NnShop\Domain\Shops\Value\ReferrerId;
use NnShop\Domain\Shops\Value\ShopId;
use NnShop\Domain\Templates\Value\TemplateId;

class CreateAnonymousOrderCommand
{
    /**
     * @var OrderId
     */
    private $orderId;

    /**
     * @var ReferrerId
     */
    private $referrerId;

    /**
     * @var TemplateId
     */
    private $templateId;

    /**
     * @var ShopId|null
     */
    private $shopId;

    /**
     * CreateAnonymousOrderCommand constructor.
     *
     * @param OrderId    $orderId
     * @param TemplateId $templateId
     */
    private function __construct(OrderId $orderId, TemplateId $templateId)
    {
        $this->orderId = $orderId;
        $this->templateId = $templateId;
    }

    /**
     * @param OrderId    $orderId
     * @param TemplateId $templateId
     *
     * @return CreateAnonymousOrderCommand
     */
    public static function create(OrderId $orderId, TemplateId $templateId): self
    {
        return new self($orderId, $templateId);
    }

    /**
     * @return OrderId
     */
    public function getOrderId(): OrderId
    {
        return $this->orderId;
    }

    /**
     * @return ReferrerId
     */
    public function getReferrerId(): ReferrerId
    {
        return $this->referrerId;
    }

    /**
     * @return TemplateId
     */
    public function getTemplateId(): TemplateId
    {
        return $this->templateId;
    }

    /**
     * @return bool
     */
    public function hasReferrerId(): bool
    {
        return null !== $this->referrerId;
    }

    /**
     * @param ReferrerId $referrerId
     */
    public function setReferrerId(ReferrerId $referrerId)
    {
        $this->referrerId = $referrerId;
    }

    /**
     * @return ShopId|null
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * @param ShopId|null $shopId
     */
    public function setShopId($shopId)
    {
        $this->shopId = $shopId;
    }
}
