<?php

namespace NnShop\Domain\Orders\Command\Order;

use NnShop\Domain\Orders\Value\OrderId;

class RevertConfirmStepCommand
{
    /**
     * @var OrderId
     */
    private $id;

    /**
     * BackFromConfirmStepCommand constructor.
     *
     * @param OrderId $id
     */
    private function __construct(OrderId $id)
    {
        $this->id = $id;
    }

    /**
     * @param OrderId $id
     *
     * @return RevertConfirmStepCommand
     */
    public static function create(OrderId $id): self
    {
        return new self($id);
    }

    /**
     * @return OrderId
     */
    public function getId(): OrderId
    {
        return $this->id;
    }
}
