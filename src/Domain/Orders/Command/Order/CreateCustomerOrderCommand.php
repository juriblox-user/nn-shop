<?php

namespace NnShop\Domain\Orders\Command\Order;

use NnShop\Domain\Orders\Value\CustomerId;
use NnShop\Domain\Orders\Value\OrderId;
use NnShop\Domain\Shops\Value\ReferrerId;
use NnShop\Domain\Shops\Value\ShopId;
use NnShop\Domain\Templates\Value\TemplateId;

class CreateCustomerOrderCommand
{
    /**
     * @var CustomerId
     */
    private $customerId;

    /**
     * @var OrderId
     */
    private $orderId;

    /**
     * @var ReferrerId
     */
    private $referrerId;

    /**
     * @var TemplateId
     */
    private $templateId;

    /**
     * @var ShopId|null
     */
    private $shopId;

    /**
     * CreateOrderCommand constructor.
     *
     * @param OrderId    $orderId
     * @param TemplateId $templateId
     * @param CustomerId $customerId
     */
    private function __construct(OrderId $orderId, TemplateId $templateId, CustomerId $customerId)
    {
        $this->orderId = $orderId;
        $this->templateId = $templateId;
        $this->customerId = $customerId;
    }

    /**
     * @return ShopId|null
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * @param ShopId|null $shopId
     */
    public function setShopId($shopId)
    {
        $this->shopId = $shopId;
    }

    /**
     * @param OrderId    $orderId
     * @param TemplateId $templateId
     * @param CustomerId $customerId
     *
     * @return CreateCustomerOrderCommand
     */
    public static function create(OrderId $orderId, TemplateId $templateId, CustomerId $customerId): self
    {
        return new self($orderId, $templateId, $customerId);
    }

    /**
     * @return CustomerId
     */
    public function getCustomerId(): CustomerId
    {
        return $this->customerId;
    }

    /**
     * @return OrderId
     */
    public function getOrderId(): OrderId
    {
        return $this->orderId;
    }

    /**
     * @return ReferrerId
     */
    public function getReferrerId(): ReferrerId
    {
        return $this->referrerId;
    }

    /**
     * @return TemplateId
     */
    public function getTemplateId(): TemplateId
    {
        return $this->templateId;
    }

    /**
     * @return bool
     */
    public function hasReferrerId(): bool
    {
        return null !== $this->referrerId;
    }

    /**
     * @param ReferrerId $referrerId
     */
    public function setReferrerId(ReferrerId $referrerId)
    {
        $this->referrerId = $referrerId;
    }
}
