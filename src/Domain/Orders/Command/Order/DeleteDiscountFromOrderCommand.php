<?php

namespace NnShop\Domain\Orders\Command\Order;

use NnShop\Domain\Orders\Value\DiscountId;
use NnShop\Domain\Orders\Value\OrderId;

class DeleteDiscountFromOrderCommand
{
    /**
     * @var DiscountId
     */
    private $discountId;

    /**
     * @var OrderId
     */
    private $orderId;

    /**
     * @param OrderId $orderId
     */
    private function __construct(OrderId $orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @param OrderId    $orderId
     * @param DiscountId $discountId
     *
     * @return DeleteDiscountFromOrderCommand
     */
    public static function create(OrderId $orderId, DiscountId $discountId): self
    {
        $command = new self($orderId);
        $command->discountId = $discountId;

        return $command;
    }

    /**
     * @return DiscountId
     */
    public function getDiscountId(): DiscountId
    {
        return $this->discountId;
    }

    /**
     * @return OrderId
     */
    public function getOrderId(): OrderId
    {
        return $this->orderId;
    }
}
