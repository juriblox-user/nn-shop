<?php

namespace NnShop\Domain\Orders\Command\Order;

use NnShop\Domain\Orders\Value\OrderId;

class RefreshOrderCommand
{
    /**
     * @var bool
     */
    private $forced;

    /**
     * @var OrderId
     */
    private $id;

    /**
     * RefreshOrderCommand constructor.
     *
     * @param OrderId $id
     */
    private function __construct(OrderId $id)
    {
        $this->id = $id;
        $this->forced = false;
    }

    /**
     * @param OrderId $id
     *
     * @return RefreshOrderCommand
     */
    public static function create(OrderId $id): self
    {
        return new self($id);
    }

    /**
     * Bestelling geforceerd vernieuwen, ook wanneer deze pasgeleden is vernieuwd.
     */
    public function force()
    {
        $this->forced = true;
    }

    /**
     * @return OrderId
     */
    public function getId(): OrderId
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isForced(): bool
    {
        return $this->forced;
    }
}
