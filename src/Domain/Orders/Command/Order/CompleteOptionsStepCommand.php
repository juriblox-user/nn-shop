<?php

namespace NnShop\Domain\Orders\Command\Order;

use NnShop\Domain\Orders\Enumeration\OrderType;
use NnShop\Domain\Orders\Value\OrderId;

class CompleteOptionsStepCommand
{
    /**
     * Handmatige controle aanvragen.
     *
     * @var bool
     */
    private $check;

    /**
     * Maatwerk aanpassingen aanvragen.
     *
     * @var bool
     */
    private $custom;

    /**
     * @var OrderId
     */
    private $id;

    /**
     * @var bool
     */
    private $registeredEmail;

    /**
     * Type document dat wordt aangevraagd.
     *
     * @var OrderType
     */
    private $type;

    /**
     * CompleteOptionsStateCommand constructor.
     *
     * @param OrderId $id
     */
    private function __construct(OrderId $id)
    {
        $this->id = $id;

        $this->registeredEmail = false;
    }

    /**
     * @param OrderId $id
     *
     * @return CompleteOptionsStepCommand
     */
    public static function prepare(OrderId $id): self
    {
        return new self($id);
    }

    /**
     * @return bool
     */
    public function getCheck(): bool
    {
        return $this->check;
    }

    /**
     * @return bool
     */
    public function getCustom(): bool
    {
        return $this->custom;
    }

    /**
     * @return OrderId
     */
    public function getId(): OrderId
    {
        return $this->id;
    }

    public function getRegisteredEmail(): bool
    {
        return $this->registeredEmail;
    }

    /**
     * @return OrderType
     */
    public function getType(): OrderType
    {
        return $this->type;
    }

    /**
     * @param bool $check
     */
    public function setCheck(bool $check)
    {
        $this->check = $check;
    }

    /**
     * @param bool $custom
     */
    public function setCustom(bool $custom)
    {
        $this->custom = $custom;
    }

    public function setRegisteredEmail(bool $registeredEmail): void
    {
        $this->registeredEmail = $registeredEmail;
    }

    /**
     * @param OrderType $type
     */
    public function setType(OrderType $type)
    {
        $this->type = $type;

        if ($this->type->is(OrderType::SUBSCRIPTION)) {
            $this->custom = false;
        }
    }
}
