<?php

namespace NnShop\Domain\Orders\Command\Order;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Orders\Value\OrderId;

class RepairOrderCommand
{
    /**
     * @Serializer\Type("NnShop\Domain\Orders\Value\OrderId")
     *
     * @var OrderId
     */
    private $id;

    /**
     * RepairOrderCommand constructor.
     *
     * @param OrderId $id
     */
    private function __construct(OrderId $id)
    {
        $this->id = $id;
    }

    /**
     * @param OrderId $id
     *
     * @return RepairOrderCommand
     */
    public static function create(OrderId $id): self
    {
        return new self($id);
    }

    /**
     * @return OrderId
     */
    public function getId(): OrderId
    {
        return $this->id;
    }
}
