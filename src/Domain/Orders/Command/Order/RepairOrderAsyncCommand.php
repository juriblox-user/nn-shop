<?php

namespace NnShop\Domain\Orders\Command\Order;

use JMS\Serializer\Annotation as Serializer;
use NnShop\Domain\Orders\Value\OrderId;

class RepairOrderAsyncCommand
{
    /**
     * @Serializer\Type("NnShop\Domain\Orders\Command\Order\RepairOrderCommand")
     *
     * @var RepairOrderCommand
     */
    private $command;

    /**
     * RepairOrderAsyncCommand constructor.
     */
    private function __construct()
    {
    }

    /**
     * @param OrderId $orderId
     *
     * @return RepairOrderAsyncCommand
     */
    public static function create(OrderId $orderId): self
    {
        return static::from(RepairOrderCommand::create($orderId));
    }

    /**
     * @param RepairOrderCommand $synchronousCommand
     *
     * @return RepairOrderAsyncCommand
     */
    public static function from(RepairOrderCommand $synchronousCommand): self
    {
        $command = new self();
        $command->command = $synchronousCommand;

        return $command;
    }

    /**
     * @return RepairOrderCommand
     */
    public function getCommand(): RepairOrderCommand
    {
        return $this->command;
    }
}
