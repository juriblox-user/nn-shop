<?php

namespace NnShop\Domain\Orders\Command\Order;

use Core\Domain\Value\Geography\Address;
use Core\Domain\Value\Web\EmailAddress;
use NnShop\Domain\Orders\Enumeration\CustomerType;
use NnShop\Domain\Orders\Value\OrderId;
use NnShop\Domain\Translation\Value\LocaleId;
use NnShop\Domain\Translation\Value\ProfileId;

class PrepareCustomerForOrderCommand
{
    /**
     * @var Address
     */
    private $address;

    /**
     * @var string
     */
    private $companyName;

    /**
     * @var CustomerType
     */
    private $customerType;

    /**
     * @var EmailAddress
     */
    private $emailAddress;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var LocaleId
     */
    private $localeId;

    /**
     * @var OrderId
     */
    private $orderId;

    /**
     * @var string|null
     */
    private $phone;

    /**
     * @var ProfileId
     */
    private $profileId;

    /**
     * @var string
     */
    private $vat;

    /**
     * @var string
     */
    private $coc;

    /**
     * AssociateCustomerWithOrderCommand constructor.
     *
     * @param OrderId   $orderId
     * @param ProfileId $profileId
     * @param LocaleId  $localeId
     */
    private function __construct(OrderId $orderId, ProfileId $profileId, LocaleId $localeId)
    {
        $this->orderId = $orderId;

        $this->profileId = $profileId;
        $this->localeId = $localeId;
    }

    /**
     * @param OrderId      $orderId
     * @param EmailAddress $emailAddress
     * @param ProfileId    $profileId
     * @param LocaleId     $localeId
     *
     * @return PrepareCustomerForOrderCommand
     */
    public static function prepare(OrderId $orderId, EmailAddress $emailAddress, ProfileId $profileId, LocaleId $localeId): self
    {
        $command = new self($orderId, $profileId, $localeId);
        $command->emailAddress = $emailAddress;

        return $command;
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @return CustomerType
     */
    public function getCustomerType(): CustomerType
    {
        return $this->customerType;
    }

    /**
     * @return EmailAddress
     */
    public function getEmailAddress(): EmailAddress
    {
        return $this->emailAddress;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return LocaleId
     */
    public function getLocaleId(): LocaleId
    {
        return $this->localeId;
    }

    /**
     * @return OrderId
     */
    public function getOrderId(): OrderId
    {
        return $this->orderId;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return ProfileId
     */
    public function getProfileId(): ProfileId
    {
        return $this->profileId;
    }

    /**
     * @return string
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @return string
     */
    public function getCoc()
    {
        return $this->coc;
    }

    /**
     * @param Address $address
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName ?: null;
    }

    /**
     * @param CustomerType $customerType
     */
    public function setCustomerType(CustomerType $customerType)
    {
        $this->customerType = $customerType;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname ?: null;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname ?: null;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone ?: null;
    }

    /**
     * @param string $vat
     */
    public function setVat($vat)
    {
        $this->vat = $vat ?: null;
    }

    /**
     * @param string|null $coc
     */
    public function setCoc($coc)
    {
        $this->coc = $coc ?: null;
    }


}
