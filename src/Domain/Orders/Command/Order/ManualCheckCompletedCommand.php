<?php

namespace NnShop\Domain\Orders\Command\Order;

use NnShop\Domain\Orders\Value\OrderId;

class ManualCheckCompletedCommand
{
    /**
     * @var OrderId
     */
    private $id;

    /**
     * ManualCheckCompletedCommand constructor.
     *
     * @param OrderId $id
     */
    private function __construct(OrderId $id)
    {
        $this->id = $id;
    }

    /**
     * @param OrderId $id
     *
     * @return ManualCheckCompletedCommand
     */
    public static function create(OrderId $id): self
    {
        return new static($id);
    }

    /**
     * @return OrderId
     */
    public function getId(): OrderId
    {
        return $this->id;
    }
}
