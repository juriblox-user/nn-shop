<?php

namespace NnShop\Domain\Orders\Command\Order;

use NnShop\Domain\Orders\Value\OrderId;

class ManualCustomCompletedCommand
{
    /**
     * @var OrderId
     */
    private $id;

    /**
     * ManualCheckCompletedCommand constructor.
     *
     * @param OrderId $id
     */
    private function __construct(OrderId $id)
    {
        $this->id = $id;
    }

    /**
     * @param OrderId $id
     *
     * @return ManualCustomCompletedCommand
     */
    public static function create(OrderId $id): self
    {
        return new static($id);
    }

    /**
     * @return OrderId
     */
    public function getId(): OrderId
    {
        return $this->id;
    }
}
