<?php

namespace NnShop\Domain\Orders\Command\Invoice;

use Carbon\Carbon;
use NnShop\Domain\Orders\Value\InvoiceId;

class ProcessInvoicePaymentCommand
{
    /**
     * @var InvoiceId
     */
    private $id;

    /**
     * @var Carbon
     */
    private $paymentDateTime;

    /**
     * ProcessInvoicePaymentCommand constructor.
     *
     * @param InvoiceId $id
     */
    private function __construct(InvoiceId $id)
    {
        $this->id = $id;
    }

    /**
     * @param InvoiceId $id
     *
     * @return ProcessInvoicePaymentCommand
     */
    public static function create(InvoiceId $id, Carbon $paymentDateTime): self
    {
        $command = new self($id);
        $command->paymentDateTime = $paymentDateTime;

        return $command;
    }

    /**
     * @return InvoiceId
     */
    public function getId(): InvoiceId
    {
        return $this->id;
    }

    /**
     * @return Carbon
     */
    public function getPaymentDateTime(): Carbon
    {
        return $this->paymentDateTime;
    }
}
