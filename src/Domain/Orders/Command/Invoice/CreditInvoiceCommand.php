<?php

namespace NnShop\Domain\Orders\Command\Invoice;

use NnShop\Domain\Orders\Value\InvoiceId;

class CreditInvoiceCommand
{
    /**
     * @var InvoiceId
     */
    private $id;

    /**
     * Constructor.
     *
     * @param InvoiceId $id
     */
    private function __construct(InvoiceId $id)
    {
        $this->id = $id;
    }

    /**
     * @param InvoiceId $id
     *
     * @return CreditInvoiceCommand
     */
    public static function create(InvoiceId $id): self
    {
        return new self($id);
    }

    /**
     * @return InvoiceId
     */
    public function getId(): InvoiceId
    {
        return $this->id;
    }
}
