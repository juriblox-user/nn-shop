<?php

namespace NnShop\Domain\Orders\Command\Customer;

use NnShop\Domain\Orders\Value\CustomerId;
use NnShop\Domain\Orders\Value\OrderId;

class ClaimOrderCommand
{
    /**
     * @var CustomerId
     */
    private $customerId;

    /**
     * @var OrderId
     */
    private $orderId;

    /**
     * Constructor.
     *
     * @param CustomerId $customerId
     * @param OrderId    $orderId
     */
    private function __construct(CustomerId $customerId, OrderId $orderId)
    {
        $this->customerId = $customerId;
        $this->orderId = $orderId;
    }

    /**
     * @param CustomerId $customerId
     * @param OrderId    $orderId
     *
     * @return ClaimOrderCommand
     */
    public static function create(CustomerId $customerId, OrderId $orderId): self
    {
        return new self($customerId, $orderId);
    }

    /**
     * @return CustomerId
     */
    public function getCustomerId(): CustomerId
    {
        return $this->customerId;
    }

    /**
     * @return OrderId
     */
    public function getOrderId(): OrderId
    {
        return $this->orderId;
    }
}
