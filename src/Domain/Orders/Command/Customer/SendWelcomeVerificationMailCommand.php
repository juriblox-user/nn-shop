<?php

namespace NnShop\Domain\Orders\Command\Customer;

use NnShop\Domain\Orders\Value\CustomerId;

class SendWelcomeVerificationMailCommand
{
    /**
     * @var CustomerId
     */
    private $id;

    /**
     * Constructor.
     *
     * @param CustomerId $id
     */
    private function __construct(CustomerId $id)
    {
        $this->id = $id;
    }

    /**
     * @param CustomerId $id
     *
     * @return SendWelcomeVerificationMailCommand
     */
    public static function create(CustomerId $id): self
    {
        $command = new self($id);

        return $command;
    }

    /**
     * @return CustomerId
     */
    public function getId(): CustomerId
    {
        return $this->id;
    }
}
