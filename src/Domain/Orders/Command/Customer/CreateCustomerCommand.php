<?php

namespace NnShop\Domain\Orders\Command\Customer;

use Core\Domain\Value\Geography\Address;
use Core\Domain\Value\Web\EmailAddress;
use NnShop\Domain\Orders\Enumeration\CustomerType;
use NnShop\Domain\Orders\Value\CustomerId;
use NnShop\Domain\Translation\Value\LocaleId;
use NnShop\Domain\Translation\Value\ProfileId;

class CreateCustomerCommand
{
    /**
     * @var Address
     */
    private $address;

    /**
     * @var string
     */
    private $companyName;

    /**
     * @var EmailAddress
     */
    private $email;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var CustomerId
     */
    private $id;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var LocaleId
     */
    private $localeId;

    /**
     * @var bool
     */
    private $newsletter;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string|null
     */
    private $phone;

    /**
     * @var ProfileId
     */
    private $profileId;

    /**
     * @var CustomerType
     */
    private $type;

    /**
     * @var string|null
     */
    private $vat;

    /**
     * @var string|null
     */
    private $coc;

    /**
     * CreateCustomerCommand constructor.
     *
     * @param CustomerId $id
     * @param ProfileId  $profileId
     * @param LocaleId   $localeId
     */
    private function __construct(CustomerId $id, ProfileId $profileId, LocaleId $localeId)
    {
        $this->id = $id;

        $this->profileId = $profileId;
        $this->localeId = $localeId;

        $this->newsletter = false;
    }

    /**
     * @param CustomerId   $id
     * @param CustomerType $type
     * @param EmailAddress $email
     * @param ProfileId    $profileId
     * @param LocaleId     $localeId
     *
     * @return CreateCustomerCommand
     */
    public static function prepare(CustomerId $id, CustomerType $type, EmailAddress $email, ProfileId $profileId, LocaleId $localeId): self
    {
        $command = new self($id, $profileId, $localeId);
        $command->type = $type;
        $command->email = $email;

        return $command;
    }

    /**
     * @return Address|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @return EmailAddress
     */
    public function getEmail(): EmailAddress
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @return CustomerId
     */
    public function getId(): CustomerId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return LocaleId
     */
    public function getLocaleId(): LocaleId
    {
        return $this->localeId;
    }

    /**
     * @return bool
     */
    public function getNewsletter(): bool
    {
        return $this->newsletter;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return ProfileId
     */
    public function getProfileId(): ProfileId
    {
        return $this->profileId;
    }

    /**
     * @return CustomerType
     */
    public function getType(): CustomerType
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @return string|null
     */
    public function getCoc()
    {
        return $this->coc;
    }

    /**
     * @param Address $address
     *
     * @return CreateCustomerCommand
     */
    public function setAddress(Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @param string $companyName
     *
     * @return CreateCustomerCommand
     */
    public function setCompanyName($companyName): self
    {
        if ($this->type->isNot(CustomerType::BUSINESS)) {
            throw new \LogicException('You cannot set company details on a non-business customer');
        }

        $this->companyName = $companyName ?: null;

        return $this;
    }

    /**
     * @param string $firstname
     *
     * @return CreateCustomerCommand
     */
    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname ?: null;

        return $this;
    }

    /**
     * @param string $lastname
     *
     * @return CreateCustomerCommand
     */
    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname ?: null;

        return $this;
    }

    /**
     * @param bool $newsletter
     *
     * @return CreateCustomerCommand
     */
    public function setNewsletter(bool $newsletter): self
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    /**
     * @param string $password
     *
     * @return CreateCustomerCommand
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @param string|null $phone
     *
     * @return CreateCustomerCommand
     */
    public function setPhone($phone): CreateCustomerCommand
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @param string|null $vat
     *
     * @return CreateCustomerCommand
     */
    public function setVat($vat): CreateCustomerCommand
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * @param string|null $coc
     *
     * @return CreateCustomerCommand
     */
    public function setCoc($coc): CreateCustomerCommand
    {
        $this->coc = $coc;

        return $this;
    }


}
