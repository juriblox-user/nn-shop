<?php

namespace NnShop\Domain\Orders\Command\Customer;

use NnShop\Domain\Orders\Value\CustomerId;

class ChangePasswordCommand
{
    /**
     * @var CustomerId
     */
    private $id;

    /**
     * @var string
     */
    private $password;

    /**
     * Constructor.
     *
     * @param CustomerId $id
     */
    private function __construct(CustomerId $id)
    {
        $this->id = $id;
    }

    /**
     * @param CustomerId $id
     * @param string     $password
     *
     * @return ChangePasswordCommand
     */
    public static function create(CustomerId $id, string $password): self
    {
        $command = new self($id);
        $command->password = $password;

        return $command;
    }

    /**
     * @return CustomerId
     */
    public function getId(): CustomerId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}
