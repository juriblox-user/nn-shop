<?php

namespace NnShop\Domain\Orders\Command\Customer;

use Core\Domain\Value\Web\EmailAddress;
use NnShop\Domain\Orders\Value\CustomerId;

class RequestEmailChangeCommand
{
    /**
     * @var EmailAddress
     */
    private $email;

    /**
     * @var CustomerId
     */
    private $id;

    /**
     * Constructor.
     *
     * @param CustomerId $id
     */
    private function __construct(CustomerId $id)
    {
        $this->id = $id;
    }

    /**
     * @param CustomerId   $id
     * @param EmailAddress $email
     *
     * @return RequestEmailChangeCommand
     */
    public static function create(CustomerId $id, EmailAddress $email): self
    {
        $command = new self($id);
        $command->email = $email;

        return $command;
    }

    /**
     * @return EmailAddress
     */
    public function getEmail(): EmailAddress
    {
        return $this->email;
    }

    /**
     * @return CustomerId
     */
    public function getId(): CustomerId
    {
        return $this->id;
    }
}
