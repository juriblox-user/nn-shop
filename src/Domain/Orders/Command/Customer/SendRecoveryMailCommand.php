<?php

namespace NnShop\Domain\Orders\Command\Customer;

use NnShop\Domain\Orders\Value\CustomerId;

class SendRecoveryMailCommand
{
    /**
     * @var CustomerId
     */
    private $id;

    /**
     * Constructor.
     *
     * @param CustomerId $id
     */
    private function __construct(CustomerId $id)
    {
        $this->id = $id;
    }

    /**
     * @param CustomerId $id
     *
     * @return SendRecoveryMailCommand
     */
    public static function create(CustomerId $id): self
    {
        return new self($id);
    }

    /**
     * @return CustomerId
     */
    public function getId(): CustomerId
    {
        return $this->id;
    }
}
