<?php

namespace NnShop\Domain\Orders\Attribute;

class OrderAttribute
{
    const TWITTER_CONVERSION_TRACKED = 'conversions.twitter_tracked';

    const GOOGLE_CONVERSION_TRACKED = 'conversions.google_tracked';

    const ADWORDS_CONVERSION_TRACKED = 'conversions.adwords_tracked';
}
