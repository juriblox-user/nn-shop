<?php

namespace NnShop\Domain\Orders\Data;

use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Domain\Templates\Value\QuestionId;
use NnShop\Domain\Templates\Value\QuestionOptionId;

class QuestionWithAnswer
{
    /**
     * @var ArrayCollection|QuestionOptionId[]
     */
    private $optionIds;

    /**
     * @var QuestionId
     */
    private $questionId;

    /**
     * @var mixed
     */
    private $value;

    /**
     * RequestQuestionAnswer constructor.
     *
     * @param QuestionId $questionId
     */
    private function __construct(QuestionId $questionId)
    {
        $this->questionId = $questionId;

        $this->optionIds = new ArrayCollection();
        $this->value = null;
    }

    /**
     * @param QuestionOptionId $optionId
     */
    public function addOption(QuestionOptionId $optionId)
    {
        if (null !== $this->value) {
            throw new \LogicException('You cannot mix explicit values and QuestionOptionIds');
        }

        $this->optionIds->add($optionId);
    }

    /**
     * @param QuestionId $questionId
     *
     * @return QuestionWithAnswer
     */
    public static function fromEmpty(QuestionId $questionId): self
    {
        return new self($questionId);
    }

    /**
     * @param QuestionId       $questionId
     * @param QuestionOptionId $optionId
     *
     * @return QuestionWithAnswer
     */
    public static function fromOption(QuestionId $questionId, QuestionOptionId $optionId): self
    {
        return self::fromOptions($questionId, [$optionId]);
    }

    /**
     * @param QuestionId $questionId
     * @param array      $options
     *
     * @return QuestionWithAnswer
     */
    public static function fromOptions(QuestionId $questionId, array $options): self
    {
        $answer = new self($questionId);
        foreach ($options as $optionId) {
            $answer->addOption($optionId);
        }

        return $answer;
    }

    /**
     * @param QuestionId $questionId
     * @param            $value
     *
     * @return QuestionWithAnswer
     */
    public static function fromValue(QuestionId $questionId, $value): self
    {
        $answer = new self($questionId);
        $answer->value = $value;

        return $answer;
    }

    /**
     * @return ArrayCollection|QuestionOptionId[]
     */
    public function getOptionIds(): ArrayCollection
    {
        return $this->optionIds;
    }

    /**
     * @return QuestionId
     */
    public function getQuestionId(): QuestionId
    {
        return $this->questionId;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return bool
     */
    public function isAnswered(): bool
    {
        return null !== $this->value || !$this->optionIds->isEmpty();
    }

    /**
     * @param QuestionOptionId|mixed $value
     *
     * @return bool
     */
    public function isAnsweredWith($value): bool
    {
        if (!$this->isAnswered()) {
            return false;
        }

        if ($value instanceof QuestionOptionId) {
            $matches = $this->optionIds->filter(function (QuestionOptionId $optionId) use ($value) {
                return $optionId->getString() === $value->getString();
            });

            return !$matches->isEmpty();
        }

        return $value == $this->getValue();
    }
}
