<?php

namespace NnShop\Domain\Orders\Data;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use NnShop\Domain\Templates\Entity\Answer;
use NnShop\Domain\Templates\Value\QuestionId;
use NnShop\Domain\Templates\Value\QuestionOptionId;

class QuestionWithAnswerBag implements \IteratorAggregate, \Countable
{
    /**
     * @var ArrayCollection
     */
    private $answers;

    /**
     * QuestionWithAnswerBag constructor.
     */
    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }

    public function clear()
    {
        $this->answers->clear();
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return $this->answers->count();
    }

    /**
     * @param QuestionId $questionId
     *
     * @return QuestionWithAnswer|null
     */
    public function get(QuestionId $questionId)
    {
        $matches = $this->answers->filter(function (QuestionWithAnswer $answer) use ($questionId) {
            return $answer->getQuestionId() == $questionId;
        });

        return $matches->isEmpty() ? null : $matches->first();
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        return $this->answers->getIterator();
    }

    /**
     * @param QuestionId $questionId
     *
     * @return bool
     */
    public function has(QuestionId $questionId): bool
    {
        return null !== $this->get($questionId);
    }

    /**
     * @param Collection|Answer[] $answers
     */
    public function import($answers)
    {
        foreach ($answers as $answer) {
            if ($answer->hasOptions()) {
                foreach ($answer->getOptions() as $option) {
                    $this->addOption($answer->getQuestion()->getId(), $option->getId());
                }
            } else {
                $this->setValue($answer->getQuestion()->getId(), $answer->getValue());
            }
        }
    }

    /**
     * @param QuestionWithAnswerBag $answers
     */
    public function merge(self $answers)
    {
        /** @var QuestionWithAnswer $answer */
        foreach ($answers as $answer) {
            $this->set($answer);
        }
    }

    /**
     * @param QuestionWithAnswer $answer
     */
    public function replace(QuestionWithAnswer $answer)
    {
        if (!$this->has($answer->getQuestionId())) {
            throw new \InvalidArgumentException(sprintf('There is no existing answer for QuestionId [%s]', $answer->getQuestionId()));
        }

        $this->remove($answer->getQuestionId());
        $this->set($answer);
    }

    /**
     * @param QuestionId       $questionId
     * @param QuestionOptionId $optionId
     */
    public function replaceOption(QuestionId $questionId, QuestionOptionId $optionId)
    {
        if (!$this->has($questionId)) {
            throw new \InvalidArgumentException(sprintf('There is no existing answer for QuestionId [%s]', $questionId));
        }

        $this->remove($questionId);
        $this->setOption($questionId, $optionId);
    }

    /**
     * @param QuestionId $questionId
     * @param            $value
     */
    public function replaceValue(QuestionId $questionId, $value)
    {
        if (!$this->has($questionId)) {
            throw new \InvalidArgumentException(sprintf('There is no existing answer for QuestionId [%s]', $questionId));
        }

        $this->remove($questionId);
        $this->setValue($questionId, $value);
    }

    /**
     * @param QuestionWithAnswer $answer
     */
    public function set(QuestionWithAnswer $answer)
    {
        if ($this->has($answer->getQuestionId())) {
            $this->replace($answer);

            return;
        }

        $this->answers->add($answer);
    }

    /**
     * @param QuestionId $questionId
     */
    public function remove(QuestionId $questionId)
    {
        $answer = $this->get($questionId);
        if (null === $answer) {
            return;
        }

        $this->answers->removeElement($answer);
    }

    /**
     * @param QuestionId $questionId
     */
    public function setEmpty(QuestionId $questionId)
    {
        $this->remove($questionId);

        $this->answers->add(QuestionWithAnswer::fromEmpty($questionId));
    }

    /**
     * @param QuestionId       $questionId
     * @param QuestionOptionId $optionId
     */
    public function setOption(QuestionId $questionId, QuestionOptionId $optionId)
    {
        if ($this->has($questionId)) {
            $this->replaceOption($questionId, $optionId);

            return;
        }

        $this->answers->add(QuestionWithAnswer::fromOption($questionId, $optionId));
    }

    /**
     * @param QuestionId       $questionId
     * @param QuestionOptionId $optionId
     */
    public function addOption(QuestionId $questionId, QuestionOptionId $optionId)
    {
        $answer = $this->get($questionId);
        if (null === $answer) {
            $answer = QuestionWithAnswer::fromOptions($questionId, [$optionId]);

            $this->answers->add($answer);
        } else {
            $answer->addOption($optionId);
        }
    }

    /**
     * @param QuestionId $questionId
     * @param            $value
     */
    public function setValue(QuestionId $questionId, $value)
    {
        if ($this->has($questionId)) {
            $this->replaceValue($questionId, $value);

            return;
        }

        $this->answers->add(QuestionWithAnswer::fromValue($questionId, $value));
    }
}
