<?php

namespace NnShop\Infrastructure\System\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\System\Entity\Counter;
use NnShop\Domain\System\Enumeration\CounterName;
use NnShop\Domain\System\Repository\CounterRepositoryInterface;
use NnShop\Domain\System\Value\CounterId;

class CounterDoctrineRepository extends AbstractDoctrineRepository implements CounterRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findOneById(CounterId $id)
    {
        /** @var Counter $counter */
        $counter = $this->findOneBy([
            'id' => $id,
        ]);

        return $counter;
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByName(CounterName $name)
    {
        /** @var Counter $counter */
        $counter = $this->findOneBy([
            'name' => $name,
        ]);

        return $counter;
    }

    /**
     * {@inheritdoc}
     */
    public function getById(CounterId $id): Counter
    {
        /** @var Counter $counter */
        $counter = $this->getBy([
            'id' => $id,
        ]);

        return $counter;
    }

    /**
     * {@inheritdoc}
     */
    public function getByName(CounterName $name): Counter
    {
        /** @var Counter $counter */
        $counter = $this->getBy([
            'name' => $name,
        ]);

        return $counter;
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(CounterId $id): Counter
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
