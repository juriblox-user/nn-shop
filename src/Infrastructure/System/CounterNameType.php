<?php

namespace NnShop\Infrastructure\System;

use Core\Doctrine\DBAL\Types\AbstractStringEnumerationType;
use NnShop\Domain\System\Enumeration\CounterName;

class CounterNameType extends AbstractStringEnumerationType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'system.counter_name';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return CounterName::class;
    }
}
