<?php

namespace NnShop\Infrastructure\System;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\System\Value\CounterId;

class CounterIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'system.counter_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return CounterId::class;
    }
}
