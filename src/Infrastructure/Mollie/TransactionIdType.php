<?php

namespace NnShop\Infrastructure\Mollie;

use Core\Doctrine\DBAL\Types\AbstractStringType;
use NnShop\Domain\Mollie\Value\TransactionId;

class TransactionIdType extends AbstractStringType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'mollie.transaction_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return TransactionId::class;
    }
}
