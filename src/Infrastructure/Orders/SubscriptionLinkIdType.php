<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Orders\Value\SubscriptionLinkId;

class SubscriptionLinkIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.subscription_link_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return SubscriptionLinkId::class;
    }
}
