<?php

namespace NnShop\Infrastructure\Orders;

use Core\Common\Generator\Security\LongTokenGenerator;
use Core\Doctrine\DBAL\Types\AbstractStringEnumerationType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use NnShop\Domain\Orders\Enumeration\SubscriptionLinkFormat;

class SubscriptionLinkFormatType extends AbstractStringEnumerationType
{
    /**
     * {@inheritdoc}
     */
    public function getDefaultLength(AbstractPlatform $platform)
    {
        return LongTokenGenerator::LENGTH;
    }

    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.subscription_link_format';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return SubscriptionLinkFormat::class;
    }
}
