<?php

namespace NnShop\Infrastructure\Orders\QueryBuilder;

use Core\Doctrine\ORM\AbstractQueryBuilder;
use Doctrine\ORM\Query;
use NnShop\Domain\Orders\QueryBuilder\DiscountQueryBuilderInterface;

class DiscountDoctrineQueryBuilder extends AbstractQueryBuilder implements DiscountQueryBuilderInterface
{
    /**
     * @return Query
     */
    public function build(): Query
    {
        $builder = $this->getBuilder();

        $builder
            ->select('d')
            ->from($this->getClassName(), 'd')

            ->where(
                $builder->expr()->isNull('d.timestampDeleted')
            )

            ->orderBy('d.title')
            ->addOrderBy('d.code');

        return $builder->getQuery();
    }

    /**
     * {@inheritdoc}
     */
    public function reset(): DiscountQueryBuilderInterface
    {
        return $this;
    }
}
