<?php

namespace NnShop\Infrastructure\Orders\QueryBuilder;

use Core\Doctrine\ORM\AbstractQueryBuilder;
use Doctrine\ORM\Query;
use NnShop\Domain\Orders\QueryBuilder\CustomerQueryBuilderInterface;

class CustomerDoctrineQueryBuilder extends AbstractQueryBuilder implements CustomerQueryBuilderInterface
{
    /**
     * @var bool
     */
    private $orderByWorth;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $companyName;

    /**
     * @var string
     */
    private $email;

    /**
     * @return Query
     */
    public function build(): Query
    {
        $builder = $this->getBuilder()->select('c')
            ->from($this->getClassName(), 'c');

        if ($this->orderByWorth) {
            $builder->orderBy('c.worth', 'DESC');
        }

        if ($this->name) {
            $builder->orWhere(
                $builder->expr()->like('c.firstname', ':name'),
                $builder->expr()->like('c.lastname', ':name')
            );
            $builder->setParameter(':name', $this->name . '%');
        }

        if ($this->companyName) {
            $builder->andWhere(
                $builder->expr()->like('c.companyName', ':companyName')
            );
            $builder->setParameter(':companyName', $this->companyName . '%');
        }

        if ($this->email) {
            $builder->andWhere(
                $builder->expr()->like('c.email', ':email')
            );
            $builder->setParameter(':email', $this->email . '%');
        }

        return $builder->getQuery();
    }

    /**
     * {@inheritdoc}
     */
    public function reset(): CustomerQueryBuilderInterface
    {
        $this->orderByWorth = false;
        $this->companyName = null;
        $this->email = null;
        $this->name = null;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function orderByWorth()
    {
        $this->orderByWorth = true;

        return $this;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function filterName(string $name): CustomerQueryBuilderInterface
    {
        $this->name = preg_replace('/[^a-zA-Z0-9-_.]/', '', trim($name));

        return $this;
    }

    /**
     * @param string $companyName
     *
     * @return self
     */
    public function filterCompanyName(string $companyName): CustomerQueryBuilderInterface
    {
        $this->companyName = preg_replace('/[^a-zA-Z0-9-_.]/', '', trim($companyName));

        return $this;
    }

    /**
     * @param string $email
     *
     * @return self
     */
    public function filterEmail(string $email): CustomerQueryBuilderInterface
    {
        $this->email = preg_replace('/[^a-zA-Z0-9-_.@]/', '', trim($email));

        return $this;
    }
}
