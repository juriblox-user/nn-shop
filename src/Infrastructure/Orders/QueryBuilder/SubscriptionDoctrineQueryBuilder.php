<?php

namespace NnShop\Infrastructure\Orders\QueryBuilder;

use Carbon\Carbon;
use Core\Doctrine\ORM\AbstractQueryBuilder;
use Doctrine\ORM\Query;
use NnShop\Domain\Orders\Entity\Subscription;
use NnShop\Domain\Orders\QueryBuilder\SubscriptionQueryBuilderInterface;
use NnShop\Domain\Translation\Value\ProfileId;

class SubscriptionDoctrineQueryBuilder extends AbstractQueryBuilder implements SubscriptionQueryBuilderInterface
{
    /**
     * @var bool
     */
    private $filterExpired;

    /**
     * @var bool
     */
    private $includeExpired;

    /**
     * @var bool
     */
    private $filterGracePeriod;

    /**
     * @var ProfileId|null
     */
    private $profileId;

    /**
     * @return Query
     */
    public function build(): Query
    {
        $builder = $this->getBuilder();

        $builder
            ->select(['s', 'c'])
            ->from($this->getClassName(), 's')

            ->join('s.customer', 'c')
            ->join('s.order', 'o')
            ->join('s.template', 't')

            ->where(
                $builder->expr()->isNull('s.timestampDeleted')
            );

        if ($this->filterExpired) {
            // Verlopen abonnementen ophalen
            if ($this->includeExpired) {
                // Abonnementen in de afkoelperiode
                if ($this->filterGracePeriod) {
                    $builder->andWhere(
                        $builder->expr()->lt('s.timestampExpires', ':today'),
                        $builder->expr()->gte('s.timestampExpires', ':grace')
                    )->setParameters([
                        'today' => Carbon::today(),
                        'grace' => Carbon::today()->subDays(Subscription::GRACE_PERIOD_DAYS),
                    ]);
                }

                // Alle verlopen abonnementen
                else {
                    $builder->andWhere(
                        $builder->expr()->lt('s.timestampExpires', ':today')
                    )->setParameter('today', Carbon::today());
                }
            }

            // Niet-verlopen abonnementen ophalen
            else {
                $builder->andWhere(
                    $builder->expr()->gte('s.timestampExpires', ':today')
                )->setParameter('today', Carbon::today());
            }
        }

        if ($this->profileId) {
            $builder->join('t.partner', 'partner')
                ->andWhere(
                    $builder->expr()->eq('partner.countryProfile', ':profile')
                )
                ->setParameter('profile', $this->profileId);
        }

        $builder->orderBy('s.timestampCreated', 'DESC');

        return $builder->getQuery();
    }

    /**
     * {@inheritdoc}
     */
    public function filterExpired()
    {
        $this->filterExpired = true;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     */
    public function includeExpired()
    {
        if (false === $this->filterExpired) {
            throw new \LogicException('SubscriptionDoctrineQueryBuilder::$filterExpired has to be enabled');
        }

        $this->includeExpired = true;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     */
    public function filterGracePeriod()
    {
        if (false === $this->filterExpired) {
            throw new \LogicException('SubscriptionDoctrineQueryBuilder::$filterExpired has to be enabled');
        }

        if (false === $this->includeExpired) {
            throw new \LogicException('SubscriptionDoctrineQueryBuilder::$includeExpired has to be enabled');
        }

        $this->filterGracePeriod = true;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterProfile(ProfileId $profileId): SubscriptionQueryBuilderInterface
    {
        $this->profileId = $profileId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function reset(): SubscriptionQueryBuilderInterface
    {
        $this->filterExpired = false;
        $this->includeExpired = false;
        $this->filterGracePeriod = false;
        $this->profileId = null;

        return $this;
    }
}
