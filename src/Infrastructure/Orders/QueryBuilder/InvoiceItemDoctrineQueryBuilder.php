<?php

namespace NnShop\Infrastructure\Orders\QueryBuilder;

use Core\Doctrine\ORM\AbstractQueryBuilder;
use Doctrine\ORM\Query;
use NnShop\Domain\Orders\Enumeration\OrderItemType;
use NnShop\Domain\Orders\QueryBuilder\InvoiceItemQueryBuilderInterface;
use NnShop\Domain\Orders\Value\DiscountId;
use NnShop\Domain\Translation\Value\ProfileId;

class InvoiceItemDoctrineQueryBuilder extends AbstractQueryBuilder implements InvoiceItemQueryBuilderInterface
{
    /**
     * @var ProfileId|null
     */
    private $profileId;

    /**
     * @var DiscountId|null
     */
    private $discountId;

    /**
     * @var OrderItemType|null
     */
    private $itemType;

    /**
     * {@inheritdoc}
     */
    public function reset(): self
    {
        $this->profileId = null;
        $this->discountId = null;
        $this->itemType = null;

        return $this;
    }

    /**
     * @return Query
     */
    public function build(): Query
    {
        $builder = $this->getBuilder()
            ->select(['ii', 'i', 'c'])
            ->from($this->getClassName(), 'ii')
            ->join('ii.invoice', 'i')
            ->join('i.customer', 'c')
            ->join('i.discountLinks', 'dl');

        if ($this->profileId) {
            $builder->andWhere(
                $builder->expr()->eq('i.profile', ':profile')
            )
                ->setParameter('profile', $this->profileId);
        }

        if ($this->discountId) {
            $builder->andWhere(
                $builder->expr()->eq('dl.discount', ':discount')
            )
                ->setParameter('discount', $this->discountId);
        }

        if ($this->itemType) {
            $builder->andWhere(
                $builder->expr()->eq('ii.type', ':type')
            )
                ->setParameter('type', $this->itemType);
        }

        $builder->orderBy('i.timestampIssued', 'DESC');

        return $builder->getQuery();
    }

    /**
     * {@inheritdoc}
     */
    public function filterProfile(ProfileId $profileId): InvoiceItemQueryBuilderInterface
    {
        $this->profileId = $profileId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterDiscount(DiscountId $discountId): InvoiceItemQueryBuilderInterface
    {
        $this->discountId = $discountId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterType(OrderItemType $itemType): InvoiceItemQueryBuilderInterface
    {
        $this->itemType = $itemType;

        return $this;
    }
}
