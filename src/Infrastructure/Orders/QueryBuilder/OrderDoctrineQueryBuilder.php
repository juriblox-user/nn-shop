<?php

namespace NnShop\Infrastructure\Orders\QueryBuilder;

use Core\Doctrine\ORM\AbstractQueryBuilder;
use Doctrine\ORM\Query;
use NnShop\Domain\Orders\Enumeration\OrderStatus;
use NnShop\Domain\Orders\QueryBuilder\OrderQueryBuilderInterface;
use NnShop\Domain\Translation\Value\ProfileId;

class OrderDoctrineQueryBuilder extends AbstractQueryBuilder implements OrderQueryBuilderInterface
{
    /**
     * @var bool
     */
    private $includeCompleted;

    /**
     * @var bool
     */
    private $includeManual;

    /**
     * @var bool
     */
    private $includePending;

    /**
     * @var bool
     */
    private $includeWarning;

    /**
     * @var bool
     */
    private $filterStatus;

    /**
     * @var ProfileId|null
     */
    private $profileId;

    /**
     * @var string
     */
    private $orderNumber;

    /**
     * @var string
     */
    private $template;

    /**
     * @var string
     */
    private $fromDate;

    /**
     * @var string
     */
    private $toDate;

    /**
     * @return Query
     */
    public function build(): Query
    {
        $builder = $this->getBuilder()->select('o')
            ->from($this->getClassName(), 'o');
        if ($this->orderNumber) {
            $builder->andWhere(
                $builder->expr()->like('o.code', ':code')
            );
            $builder->setParameter(':code', $this->orderNumber . '%');
        }

        if ($this->template) {
            $builder->leftJoin('o.document', 'd');
            $builder->leftJoin('d.template', 't');
            $builder->andWhere(
                $builder->expr()->like('t.title', ':template')
            );
            $builder->setParameter(':template', $this->template . '%');
        }

        if ($this->fromDate) {
            $builder->andWhere($builder->expr()->gte('o.timestampCreated', ':fromDate'));
            $builder->setParameter(':fromDate', $this->fromDate);
        }

        if ($this->toDate) {
            $builder->andWhere($builder->expr()->lte('o.timestampCreated', ':toDate'));
            $builder->setParameter(':toDate', $this->toDate);
        }

        /*
         * Filteren op status
         */
        if ($this->filterStatus) {
            $status = [];

            if ($this->includeCompleted) {
                $status = array_merge($status, OrderStatus::getCompleted());
            }

            if ($this->includeManual) {
                $status = array_merge($status, OrderStatus::getManual());
            }

            if ($this->includePending) {
                $status = array_merge($status, OrderStatus::getPending());
            }

            if ($this->includeWarning) {
                $status = array_merge($status, OrderStatus::getWarning());
            }

            $builder->andWhere(
                $builder->expr()->in('o.status', ':status')
            )->setParameter('status', $status);
        }

        if ($this->profileId) {
            $builder->leftJoin('o.shop', 'shop')
                ->andWhere(
                    $builder->expr()->eq('shop.profile', ':profile')
                )
                ->setParameter('profile', $this->profileId);
        }

        $builder->orderBy('o.timestampCreated', 'DESC');

        return $builder->getQuery();
    }

    /**
     * {@inheritdoc}
     */
    public function reset(): OrderQueryBuilderInterface
    {
        $this->filterStatus = false;

        $this->includeCompleted = false;
        $this->includeManual = false;
        $this->includePending = false;
        $this->includeWarning = false;
        $this->profileId = null;
        $this->fromDate = null;
        $this->toDate = null;
        $this->template = null;
        $this->orderNumber = null;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterStatus()
    {
        $this->filterStatus = true;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     */
    public function includeCompleted()
    {
        if (false === $this->filterStatus) {
            throw new \LogicException('OrderDoctrineQueryBuilder::$filterStatus has to be enabled');
        }

        $this->includeCompleted = true;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     */
    public function includeManual()
    {
        if (false === $this->filterStatus) {
            throw new \LogicException('OrderDoctrineQueryBuilder::$filterStatus has to be enabled');
        }

        $this->includeManual = true;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     */
    public function includePending()
    {
        if (false === $this->filterStatus) {
            throw new \LogicException('OrderDoctrineQueryBuilder::$filterStatus has to be enabled');
        }

        $this->includePending = true;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     */
    public function includeWarning()
    {
        if (false === $this->filterStatus) {
            throw new \LogicException('OrderDoctrineQueryBuilder::$filterStatus has to be enabled');
        }

        $this->includeWarning = true;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterProfile(ProfileId $profileId): OrderQueryBuilderInterface
    {
        $this->profileId = $profileId;

        return $this;
    }

    /**
     * @param string $id
     *
     * @return OrderQueryBuilderInterface
     */
    public function filterOrderNumber(string $id): OrderQueryBuilderInterface
    {
        $this->orderNumber = (int) trim(str_replace(['j', 'd', 'b', '-'], ['', '', '', ''], mb_strtolower($id)));

        return $this;
    }

    /**
     * @param string $templateName
     *
     * @return OrderQueryBuilderInterface
     */
    public function filterTemplate(string $templateName): OrderQueryBuilderInterface
    {
        $this->template = preg_replace('/[^a-zA-Z0-9-_.]/', '', trim($templateName));

        return $this;
    }

    /**
     * @param \DateTime $from
     *
     * @return OrderQueryBuilderInterface
     */
    public function filterFromDate(\DateTime $from): OrderQueryBuilderInterface
    {
        $this->fromDate = $from->setTime(0, 0, 0)->format('Y-m-d H:i:s');

        return $this;
    }

    /**
     * @param \DateTime $to
     *
     * @return OrderQueryBuilderInterface
     */
    public function filterToDate(\DateTime $to): OrderQueryBuilderInterface
    {
        $this->toDate = $to->setTime(23, 59, 59)->format('Y-m-d H:i:s');

        return $this;
    }
}
