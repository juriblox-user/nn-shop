<?php

namespace NnShop\Infrastructure\Orders\QueryBuilder;

use Carbon\Carbon;
use Core\Doctrine\ORM\AbstractQueryBuilder;
use Doctrine\ORM\Query;
use NnShop\Domain\Orders\Enumeration\InvoiceStatus;
use NnShop\Domain\Orders\QueryBuilder\InvoiceQueryBuilderInterface;
use NnShop\Domain\Translation\Value\ProfileId;

class InvoiceDoctrineQueryBuilder extends AbstractQueryBuilder implements InvoiceQueryBuilderInterface
{
    /**
     * @var bool
     */
    private $filterOverdue;

    /**
     * @var bool
     */
    private $filterStatus;

    /**
     * @var bool
     */
    private $includePaid;

    /**
     * @var bool
     */
    private $includePending;

    /**
     * @var ProfileId|null
     */
    private $profileId;

    /**
     * @var string|null
     */
    private $invoiceNumber;

    private $invoiceNumberYear;

    /**
     * @return Query
     */
    public function build(): Query
    {
        $builder = $this->getBuilder()
            ->select(['i', 'c'])
            ->from($this->getClassName(), 'i')
            ->join('i.customer', 'c');

        if ($this->invoiceNumberYear) {
            $builder
                ->andWhere(
                    $builder->expr()->like('i.code.year', ':codeYear')
                );
            $builder->setParameter(':codeYear', $this->invoiceNumberYear . '%');
        }

        if ($this->invoiceNumber) {
            $builder
                ->andWhere(
                    $builder->expr()->like('i.code.number', ':codeNumber')
                );
            $builder->setParameter(':codeNumber', $this->invoiceNumber . '%');
        }

        /*
         * Filteren op status
         */
        if ($this->filterStatus) {
            $status = [];

            if ($this->includePaid) {
                $status = InvoiceStatus::from(InvoiceStatus::PAID);
            }

            if ($this->includePending) {
                $status = InvoiceStatus::from(InvoiceStatus::PENDING);
            }

            $builder->andWhere(
                $builder->expr()->in('i.status', ':status')
            )->setParameter('status', $status);
        }

        /*
         * Facturen te laat
         */
        if ($this->filterOverdue) {
            $builder->andWhere(
                $builder->expr()->lt('i.timestampDue', ':today')
            )->setParameter('today', Carbon::today());
        }

        if ($this->profileId) {
            $builder->andWhere(
                $builder->expr()->eq('i.profile', ':profile')
            )
                ->setParameter('profile', $this->profileId);
        }

        $builder->orderBy('i.timestampIssued', 'DESC');

        return $builder->getQuery();
    }

    /**
     * {@inheritdoc}
     */
    public function filterOverdue()
    {
        $this->filterOverdue = true;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterStatus()
    {
        $this->filterStatus = true;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     */
    public function includePaid()
    {
        if (false === $this->filterStatus) {
            throw new \LogicException('InvoiceDoctrineQueryBuilder::$filterStatus has to be enabled');
        }

        $this->includePaid = true;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     */
    public function includePending()
    {
        if (false === $this->filterStatus) {
            throw new \LogicException('InvoiceDoctrineQueryBuilder::$filterStatus has to be enabled');
        }

        $this->includePending = true;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterProfile(ProfileId $profileId): InvoiceQueryBuilderInterface
    {
        $this->profileId = $profileId;

        return $this;
    }

    /**
     * @param string $id
     *
     * @return InvoiceQueryBuilderInterface
     */
    public function filterInvoiceNumber(string $id): InvoiceQueryBuilderInterface
    {
        $invoiceNumber = (int) trim(str_replace(['j', 'd', 'f', '-'], ['', '', '', ''], mb_strtolower($id)));

        $this->invoiceNumberYear = mb_substr($invoiceNumber, 0, 4);

        if (mb_strlen($invoiceNumber) > 4) {
            $this->invoiceNumber = (int) mb_substr($invoiceNumber, 4, mb_strlen($invoiceNumber) - 4);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function reset(): InvoiceQueryBuilderInterface
    {
        $this->filterStatus = false;

        $this->filterOverdue = false;
        $this->includePaid = false;
        $this->includePending = false;
        $this->profileId = null;
        $this->invoiceNumber = null;

        return $this;
    }
}
