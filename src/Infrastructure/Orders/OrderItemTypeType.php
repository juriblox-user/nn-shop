<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractStringEnumerationType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use NnShop\Domain\Orders\Enumeration\OrderItemType;

class OrderItemTypeType extends AbstractStringEnumerationType
{
    /**
     * {@inheritdoc}
     */
    public function getDefaultLength(AbstractPlatform $platform)
    {
        return 20;
    }

    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.order_item_type';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return OrderItemType::class;
    }
}
