<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Orders\Value\RefundId;

class RefundIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.refund_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return RefundId::class;
    }
}
