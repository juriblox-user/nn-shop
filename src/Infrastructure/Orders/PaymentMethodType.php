<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractStringEnumerationType;
use NnShop\Domain\Orders\Enumeration\PaymentMethod;

class PaymentMethodType extends AbstractStringEnumerationType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.payment_method';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return PaymentMethod::class;
    }
}
