<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Orders\Value\InvoiceItemId;

class InvoiceItemIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return InvoiceItemId::class;
    }

    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.invoice_item_id';
    }
}
