<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Orders\Value\PaymentId;

class PaymentIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return PaymentId::class;
    }

    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.payment_id';
    }
}
