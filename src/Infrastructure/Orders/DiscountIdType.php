<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Orders\Value\DiscountId;

class DiscountIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.discount_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return DiscountId::class;
    }
}
