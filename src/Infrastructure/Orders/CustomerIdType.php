<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Orders\Value\CustomerId;

class CustomerIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.customer_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return CustomerId::class;
    }
}
