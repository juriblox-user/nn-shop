<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractYearSequenceType;
use NnShop\Domain\Orders\Value\InvoiceCode;

class InvoiceCodeType extends AbstractYearSequenceType
{
    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return InvoiceCode::class;
    }

    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.invoice_code';
    }
}
