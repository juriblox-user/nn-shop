<?php

namespace NnShop\Infrastructure\Orders\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\Orders\Entity\SubscriptionLink;
use NnShop\Domain\Orders\Repository\SubscriptionLinkRepository;
use NnShop\Domain\Orders\Value\SubscriptionLinkId;
use NnShop\Domain\Orders\Value\SubscriptionLinkToken;

class SubscriptionLinkDoctrineRepository extends AbstractDoctrineRepository implements SubscriptionLinkRepository
{
    /**
     * {@inheritdoc}
     */
    public function findOneById(SubscriptionLinkId $id)
    {
        /** @var SubscriptionLink $link */
        $link = $this->findOneBy([
            'id' => $id,
        ]);

        return $link;
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByToken(SubscriptionLinkToken $token)
    {
        /** @var SubscriptionLink $link */
        $link = $this->findOneBy([
            'token' => $token,
        ]);

        return $link;
    }

    /**
     * {@inheritdoc}
     */
    public function getById(SubscriptionLinkId $id): SubscriptionLink
    {
        /** @var SubscriptionLink $link */
        $link = $this->getBy([
            'id' => $id,
        ]);

        return $link;
    }

    /**
     * {@inheritdoc}
     */
    public function getByToken(SubscriptionLinkToken $token): SubscriptionLink
    {
        /** @var SubscriptionLink $link */
        $link = $this->getBy([
            'token' => $token,
        ]);

        return $link;
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(SubscriptionLinkId $id): SubscriptionLink
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }

    /**
     * {@inheritdoc}
     */
    public function hasWithToken(SubscriptionLinkToken $token): bool
    {
        return null !== $this->findOneByToken($token);
    }
}
