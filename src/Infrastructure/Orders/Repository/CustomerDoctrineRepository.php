<?php

namespace NnShop\Infrastructure\Orders\Repository;

use Core\Domain\Value\Web\EmailAddress;
use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;
use NnShop\Domain\Orders\Value\CustomerId;

class CustomerDoctrineRepository extends AbstractDoctrineRepository implements CustomerRepositoryInterface
{
    /**
     * @param EmailAddress $email
     *
     * @return Customer
     */
    public function findOneByEmail(EmailAddress $email)
    {
        /** @var Customer $customer */
        $customer = $this->findOneBy([
            'email' => $email,
            'timestampDeleted' => null,
        ]);

        return $customer;
    }

    /**
     * @param CustomerId $id
     *
     * @return Customer|null
     */
    public function findOneById(CustomerId $id)
    {
        /** @var Customer $customer */
        $customer = $this->findOneBy([
            'id' => $id,
        ]);

        return $customer;
    }

    /**
     * @param CustomerId $id
     *
     * @return Customer
     */
    public function getById(CustomerId $id): Customer
    {
        /** @var Customer $customer */
        $customer = $this->getBy([
            'id' => $id,
        ]);

        return $customer;
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(CustomerId $id): Customer
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }

    /**
     * @param EmailAddress $email
     *
     * @return bool
     */
    public function hasWithEmail(EmailAddress $email): bool
    {
        return null !== $this->findOneByEmail($email);
    }
}
