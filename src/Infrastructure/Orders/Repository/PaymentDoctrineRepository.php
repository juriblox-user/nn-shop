<?php

namespace NnShop\Infrastructure\Orders\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\Mollie\Value\TransactionId;
use NnShop\Domain\Orders\Entity\Payment;
use NnShop\Domain\Orders\Enumeration\PaymentStatus;
use NnShop\Domain\Orders\Repository\PaymentRepositoryInterface;
use NnShop\Domain\Orders\Value\PaymentId;

class PaymentDoctrineRepository extends AbstractDoctrineRepository implements PaymentRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findOneById(PaymentId $id)
    {
        /** @var Payment $payment */
        $payment = $this->findOneBy([
            'id' => $id,
        ]);

        return $payment;
    }

    /**
     * {@inheritdoc}
     */
    public function findPending()
    {
        return $this->findBy([
           'status' => PaymentStatus::from(PaymentStatus::PENDING),
       ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getById(PaymentId $id): Payment
    {
        /** @var Payment $payment */
        $payment = $this->getBy([
            'id' => $id,
        ]);

        return $payment;
    }

    /**
     * {@inheritdoc}
     */
    public function getByTransactionId(TransactionId $transactionId): Payment
    {
        /** @var Payment $payment */
        $payment = $this->getBy([
            'transactionId' => $transactionId,
        ]);

        return $payment;
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(PaymentId $id): Payment
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
