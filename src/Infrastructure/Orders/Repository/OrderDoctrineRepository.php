<?php

namespace NnShop\Infrastructure\Orders\Repository;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Enumeration\OrderStatus;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Orders\Value\OrderCode;
use NnShop\Domain\Orders\Value\OrderId;
use NnShop\Domain\Templates\Entity\QuestionStep;
use NnShop\Domain\Templates\Entity\Template;

class OrderDoctrineRepository extends AbstractDoctrineRepository implements OrderRepositoryInterface
{
    /**
     * @param Customer $customer
     *
     * @return array
     */
    public function findActiveOrders(Customer $customer): array
    {
        $builder = $this->createQueryBuilder('o');

        $builder
            ->addSelect('d')

            ->join('o.customer', 'c')
            ->join('o.document', 'd')

            ->where(
                $builder->expr()->eq('c.id', ':customerId'),
                $builder->expr()->in('o.status', ':statuses')
            )

            ->setParameters([
                'customerId' => $customer->getId()->getString(),
                'statuses'   => OrderStatus::getActive(),
            ]);

        return $builder->getQuery()->getResult();
    }

    /**
     * @param Customer $customer
     * @param Template $template
     *
     * @return array
     */
    public function findActiveOrdersWithTemplate(Customer $customer, Template $template): array
    {
        $builder = $this->createQueryBuilder('o');

        $builder
            ->addSelect('d')
            ->addSelect('t')

            ->join('o.customer', 'c')
            ->join('o.document', 'd')
            ->join('d.template', 't')

            ->where(
                $builder->expr()->eq('c.id', ':customerId'),
                $builder->expr()->eq('t.id', ':templateId'),
                $builder->expr()->in('o.status', ':statuses')
            )

            ->setParameters([
                'customerId' => $customer->getId()->getString(),
                'templateId' => $template->getId()->getString(),
                'statuses'   => OrderStatus::getActive(),
            ]);

        return $builder->getQuery()->getResult();
    }

    /**
     * @param QuestionStep $step
     *
     * @return array|Order[]
     */
    public function findByStep(QuestionStep $step): array
    {
        return $this->findBy([
            'step' => $step,
        ]);
    }

    /**
     * @return OrderCode
     */
    public function findHighestCode()
    {
        $builder = $this->createQueryBuilder('o')
            ->orderBy('o.code', 'DESC')
            ->setMaxResults(1);

        /** @var Order $order */
        $order = $builder->getQuery()->getOneOrNullResult();

        return null !== $order ? $order->getCode() : null;
    }

    /**
     * @param OrderId $id
     *
     * @return Order|null
     */
    public function findOneById(OrderId $id)
    {
        /** @var Order $order */
        $order = $this->findOneBy([
            'id' => $id,
        ]);

        return $order;
    }

    /**
     * @param OrderId $id
     *
     * @return Order
     */
    public function getById(OrderId $id): Order
    {
        /** @var Order $order */
        $order = $this->getBy([
            'id' => $id,
        ]);

        return $order;
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(OrderId $id): Order
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }

    /**
     * @param OrderCode $code
     *
     * @return bool
     */
    public function hasWithCode(OrderCode $code): bool
    {
        return null !== $this->findOneBy([
            'code' => $code,
        ]);
    }

    /**
     * @param int $minutesAgoDelivered
     *
     * @return array|Order[]
     */
    public function findDeliveredWithoutSentUpsellMail(int $minutesAgoDelivered): array
    {
        $builder = $this->createQueryBuilder('o');

        $fromTime  = Carbon::now()->subMinutes($minutesAgoDelivered)->subDay();
        $untilTime = Carbon::now()->subMinutes($minutesAgoDelivered);

        $builder
            ->andWhere(
                $builder->expr()->isNotNull('o.timestampDelivered'),
                $builder->expr()->gte('o.timestampDelivered', ':from'),
                $builder->expr()->lt('o.timestampDelivered', ':until'),
                $builder->expr()->isNull('o.timestampUpsellMailSent')
            )
            ->setParameters([
                'from'  => $fromTime,
                'until' => $untilTime,
            ]);

        return $builder->getQuery()->getResult();
    }

    /**
     * @param int $minutesAgoDelivered
     *
     * @return array|Order[]
     */
    public function findDeliveredWithoutSentReferralMail(int $minutesAgoDelivered): array
    {
        $builder = $this->createQueryBuilder('o');

        $fromTime  = Carbon::now()->subMinutes($minutesAgoDelivered)->subDay();
        $untilTime = Carbon::now()->subMinutes($minutesAgoDelivered);

        $builder
            ->andWhere(
                $builder->expr()->isNotNull('o.timestampDelivered'),
                $builder->expr()->gte('o.timestampDelivered', ':from'),
                $builder->expr()->lt('o.timestampDelivered', ':until'),
                $builder->expr()->isNull('o.timestampReferralMailSent')
            )
            ->setParameters([
                'from'  => $fromTime,
                'until' => $untilTime,
            ]);

        return $builder->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findAbandoned(CarbonInterval $interval, bool $timeframe = true): array
    {
        $builder = $this->createQueryBuilder('o');

        // Vanaf het huidige hele uur, en dan $interval terug
        $untilTime = Carbon::now();
        $untilTime->setTime($untilTime->hour, 0, 0);
        $untilTime->sub($interval);

        // ... en dan 1 uur terugkijken
        $fromTime = $untilTime->copy();
        $fromTime->subHour();

        /*
         * Binnen een specifiek tijdvak zoeken
         */
        if ($timeframe) {
            $builder
                ->where(
                    $builder->expr()->gte('o.timestampUpdated', ':from'),
                    $builder->expr()->lt('o.timestampUpdated', ':until')
                )
                ->setParameters([
                    'from'  => $fromTime,
                    'until' => $untilTime,
                ]);
        }

        /*
         * Alle verouderde bestellingen
         */
        else {
            $builder
                ->where(
                    $builder->expr()->lt('o.timestampUpdated', ':until')
                )
                ->setParameters([
                    'until' => $untilTime,
                ]);
        }

        $builder
            ->andWhere(
                $builder->expr()->isNull('o.timestampAbandoned'),
                $builder->expr()->in('o.status', ':statuses')
            )
            ->setParameter('statuses', OrderStatus::getActive());

        return $builder->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findRecentOrders(Customer $customer, int $limit = 5): array
    {
        $builder = $this->createQueryBuilder('o');

        $builder
            ->where(
                $builder->expr()->eq('o.customer', ':customer')
            )
            ->orderBy('o.timestampCreated', 'DESC')
            ->setMaxResults($limit)

            ->setParameters([
                'customer' => $customer,
            ]);

        return $builder->getQuery()->getResult();
    }
}
