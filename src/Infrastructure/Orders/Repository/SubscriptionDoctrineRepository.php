<?php

namespace NnShop\Infrastructure\Orders\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Entity\Subscription;
use NnShop\Domain\Orders\Repository\SubscriptionRepositoryInterface;
use NnShop\Domain\Orders\Value\SubscriptionCode;
use NnShop\Domain\Orders\Value\SubscriptionId;

class SubscriptionDoctrineRepository extends AbstractDoctrineRepository implements SubscriptionRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findOneById(SubscriptionId $id)
    {
        /** @var Subscription $subscription */
        $subscription = $this->findOneBy([
            'id' => $id,
        ]);

        return $subscription;
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByOrder(Order $order)
    {
        /** @var Subscription $subscription */
        $subscription = $this->findOneBy([
            'order' => $order,
            'timestampDeleted' => null,
        ]);

        return $subscription;
    }

    /**
     * @return SubscriptionCode
     */
    public function findHighestCode()
    {
        $builder = $this->createQueryBuilder('s')
            ->orderBy('s.code', 'DESC')
            ->setMaxResults(1);

        /** @var Subscription $subscription */
        $subscription = $builder->getQuery()->getOneOrNullResult();

        return null !== $subscription ? $subscription->getCode() : null;
    }

    /**
     * @param SubscriptionCode $number
     *
     * @return bool
     */
    public function hasWithCode(SubscriptionCode $number): bool
    {
        return null !== $this->findOneBy([
            'code' => $number,
            'timestampDeleted' => null,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getById(SubscriptionId $id): Subscription
    {
        /** @var Subscription $subscription */
        $subscription = $this->getBy([
            'id' => $id,
        ]);

        return $subscription;
    }

    /**
     * @param Order $order
     *
     * @return Subscription
     */
    public function getByOrder(Order $order): Subscription
    {
        /** @var Subscription $subscription */
        $subscription = $this->getBy([
            'order' => $order,
            'timestampDeleted' => null,
        ]);

        return $subscription;
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(SubscriptionId $id): Subscription
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
