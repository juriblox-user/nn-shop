<?php

namespace NnShop\Infrastructure\Orders\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\Orders\Entity\Refund;
use NnShop\Domain\Orders\Repository\RefundRepositoryInterface;
use NnShop\Domain\Orders\Value\RefundId;

class RefundDoctrineRepository extends AbstractDoctrineRepository implements RefundRepositoryInterface
{
    /**
     * @param RefundId $id
     *
     * @return Refund|null
     */
    public function findOneById(RefundId $id)
    {
        // TODO: Implement findOneById()
    }

    /**
     * @param RefundId $id
     *
     * @return Refund
     */
    public function getById(RefundId $id): Refund
    {
        // TODO: Implement getById()
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(RefundId $id): Refund
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
