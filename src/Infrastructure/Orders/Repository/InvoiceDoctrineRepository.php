<?php

namespace NnShop\Infrastructure\Orders\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\Orders\Entity\Invoice;
use NnShop\Domain\Orders\Repository\InvoiceRepositoryInterface;
use NnShop\Domain\Orders\Value\InvoiceCode;
use NnShop\Domain\Orders\Value\InvoiceId;

class InvoiceDoctrineRepository extends AbstractDoctrineRepository implements InvoiceRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findHighestCode(int $year)
    {
        $builder = $this->createQueryBuilder('i');

        $builder
            ->where(
                $builder->expr()->eq('i.code.year', ':year')
            )
            ->setParameters([
                'year' => $year,
            ])
            ->orderBy('i.code.number', 'DESC')
            ->setMaxResults(1);

        /** @var Invoice $invoice */
        $invoice = $builder->getQuery()->getOneOrNullResult();

        return null !== $invoice ? $invoice->getCode() : null;
    }

    /**
     * {@inheritdoc}
     */
    public function findOneById(InvoiceId $id)
    {
        /** @var Invoice $invoice */
        $invoice = $this->findOneBy([
            'id' => $id,
        ]);

        return $invoice;
    }

    /**
     * {@inheritdoc}
     */
    public function getById(InvoiceId $id): Invoice
    {
        /** @var Invoice $invoice */
        $invoice = $this->getBy([
            'id' => $id,
        ]);

        return $invoice;
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(InvoiceId $id): Invoice
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }

    /**
     * {@inheritdoc}
     */
    public function hasWithCode(InvoiceCode $code): bool
    {
        return null !== $this->findOneBy([
            'code.number' => $code->getNumber(),
            'code.year' => $code->getYear(),
        ]);
    }
}
