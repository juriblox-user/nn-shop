<?php

namespace NnShop\Infrastructure\Orders\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\Orders\Entity\Discount;
use NnShop\Domain\Orders\Repository\DiscountRepositoryInterface;
use NnShop\Domain\Orders\Value\DiscountId;

class DiscountDoctrineRepository extends AbstractDoctrineRepository implements DiscountRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllForMultipleUse(): array
    {
        return $this->findBy([
            'timestampDeleted' => null,
        ], [
            'title' => 'ASC',
            'code' => 'ASC',
        ]);
    }

    /**
     * @param string $code
     *
     * @return Discount|null
     */
    public function findOneByCode(string $code)
    {
        /** @var Discount $discount */
        $discount = $this->findOneBy([
            'code' => $code,
            'timestampDeleted' => null,
        ]);

        return $discount;
    }

    /**
     * @param DiscountId $id
     *
     * @return Discount|null
     */
    public function findOneById(DiscountId $id)
    {
        /** @var Discount $discount */
        $discount = $this->findOneBy([
            'id' => $id,
        ]);

        return $discount;
    }

    /**
     * @param string $code
     *
     * @return Discount
     */
    public function getByCode(string $code): Discount
    {
        /** @var Discount $discount */
        $discount = $this->getBy([
            'code' => $code,
            'timestampDeleted' => null,
        ]);

        return $discount;
    }

    /**
     * @param DiscountId $id
     *
     * @return Discount
     */
    public function getById(DiscountId $id): Discount
    {
        /** @var Discount $discount */
        $discount = $this->getBy([
            'id' => $id,
        ]);

        return $discount;
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(DiscountId $id): Discount
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }

    /**
     * {@inheritdoc}
     */
    public function hasWithCode(string $code): bool
    {
        return null !== $this->findOneByCode($code);
    }
}
