<?php

namespace NnShop\Infrastructure\Orders\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\Orders\Entity\Advice;
use NnShop\Domain\Orders\Repository\AdviceRepositoryInterface;
use NnShop\Domain\Orders\Value\AdviceId;

class AdviceDoctrineRepository extends AbstractDoctrineRepository implements AdviceRepositoryInterface
{
    /**
     * @param AdviceId $id
     *
     * @return Advice|null
     */
    public function findOneById(AdviceId $id)
    {
        // TODO: Implement findOneById()
    }

    /**
     * @param AdviceId $id
     *
     * @return Advice
     */
    public function getById(AdviceId $id): Advice
    {
        // TODO: Implement getById()
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(AdviceId $id): Advice
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
