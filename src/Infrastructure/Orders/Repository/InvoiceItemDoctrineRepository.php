<?php

namespace NnShop\Infrastructure\Orders\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\Orders\Entity\InvoiceItem;
use NnShop\Domain\Orders\Repository\InvoiceItemRepositoryInterface;
use NnShop\Domain\Orders\Value\InvoiceItemId;

class InvoiceItemDoctrineRepository extends AbstractDoctrineRepository implements InvoiceItemRepositoryInterface
{
    /**
     * @param InvoiceItemId $id
     *
     * @return InvoiceItem|null
     */
    public function findOneById(InvoiceItemId $id)
    {
        // TODO: Implement findOneById()
    }

    /**
     * @param InvoiceItemId $id
     *
     * @return InvoiceItem
     */
    public function getById(InvoiceItemId $id): InvoiceItem
    {
        // TODO: Implement getById()
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(InvoiceItemId $id): InvoiceItem
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
