<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Orders\Value\SubscriptionId;

class SubscriptionIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.subscription_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return SubscriptionId::class;
    }
}
