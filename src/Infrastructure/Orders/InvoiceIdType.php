<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Orders\Value\InvoiceId;

class InvoiceIdType extends AbstractUuidType
{
    /**
     * Naam van de class van het value object.
     *
     * @return string
     */
    public static function getTypeName(): string
    {
        return 'orders.invoice_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return InvoiceId::class;
    }
}
