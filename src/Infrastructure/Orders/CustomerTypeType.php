<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractStringEnumerationType;
use NnShop\Domain\Orders\Enumeration\CustomerType;

class CustomerTypeType extends AbstractStringEnumerationType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.customer_type';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return CustomerType::class;
    }
}
