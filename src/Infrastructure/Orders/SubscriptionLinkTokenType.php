<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractStringType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use NnShop\Domain\Orders\Value\SubscriptionLinkToken;

class SubscriptionLinkTokenType extends AbstractStringType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.subscription_link_token';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return SubscriptionLinkToken::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultLength(AbstractPlatform $platform)
    {
        return SubscriptionLinkToken::LENGTH;
    }
}
