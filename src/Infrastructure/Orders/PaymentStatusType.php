<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractStringEnumerationType;
use NnShop\Domain\Orders\Enumeration\PaymentStatus;

class PaymentStatusType extends AbstractStringEnumerationType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.payment_status';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return PaymentStatus::class;
    }
}
