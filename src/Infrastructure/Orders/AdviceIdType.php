<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Orders\Value\AdviceId;

class AdviceIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return AdviceId::class;
    }

    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.advice_id';
    }
}
