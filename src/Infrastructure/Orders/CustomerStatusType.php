<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractStringEnumerationType;
use NnShop\Domain\Orders\Enumeration\CustomerStatus;

class CustomerStatusType extends AbstractStringEnumerationType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.customer_status';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return CustomerStatus::class;
    }
}
