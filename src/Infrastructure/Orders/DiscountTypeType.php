<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractStringEnumerationType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use NnShop\Domain\Orders\Enumeration\DiscountType;

class DiscountTypeType extends AbstractStringEnumerationType
{
    /**
     * {@inheritdoc}
     */
    public function getDefaultLength(AbstractPlatform $platform)
    {
        return 20;
    }

    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.discount_type';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return DiscountType::class;
    }
}
