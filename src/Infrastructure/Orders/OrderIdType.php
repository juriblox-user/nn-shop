<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Orders\Value\OrderId;

class OrderIdType extends AbstractUuidType
{
    /**
     * Naam van de class van het value object.
     *
     * @return string
     */
    public static function getTypeName(): string
    {
        return 'orders.order_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return OrderId::class;
    }
}
