<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractSerializableType;
use NnShop\Domain\Orders\Attribute\OrderAttributeBag;

class OrderAttributeBagType extends AbstractSerializableType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.order_attribute_bag';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return OrderAttributeBag::class;
    }
}
