<?php

namespace NnShop\Infrastructure\Orders;

use Core\Doctrine\DBAL\Types\AbstractStringEnumerationType;
use NnShop\Domain\Orders\Enumeration\InvoiceStatus;

class InvoiceStatusType extends AbstractStringEnumerationType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.invoice_status';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return InvoiceStatus::class;
    }
}
