<?php

namespace NnShop\Infrastructure\Orders;

use Core\Common\Validation\Assertion;
use Core\Doctrine\DBAL\Types\AbstractSequenceType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use NnShop\Domain\Orders\Value\SubscriptionCode;

class SubscriptionCodeType extends AbstractSequenceType
{
    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (empty($value)) {
            return;
        }

        Assertion::isInstanceOf($value, SubscriptionCode::class);

        return (int) $value->getInteger();
    }

    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'orders.subscription_code';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return SubscriptionCode::class;
    }
}
