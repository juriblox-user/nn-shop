<?php

namespace NnShop\Infrastructure\Orders\Transformer;

use Core\Common\Exception\TransformerConversionException;
use Core\Common\Validation\Assertion;
use NnShop\Domain\Orders\Enumeration\PaymentMethod;
use NnShop\Domain\Orders\Transformer\PaymentMethodTransformerInterface;

class PaymentMethodMollieTransformer implements PaymentMethodTransformerInterface
{
    const MOLLIE_BANK_CONTACT = 'bancontact';
    const MOLLIE_SOFORT = 'sofort';
    const MOLLIE_GIROPAY = 'giropay';
    const MOLLIE_KBC = 'kbc';
    const MOLLIE_BELFIUS = 'belfius';
    const MOLLIE_INGHOMEPAY = 'inghomepay';
    const MOLLIE_EPS = 'eps';

    /**
     * Mollie-waarde voor overschrijving.
     */
    const MOLLIE_BANK_TRANSFER = 'banktransfer';

    /**
     * Mollie-waarde voor creditcard.
     */
    const MOLLIE_CREDITCARD = 'creditcard';

    /**
     * Mollie-waarde voor machtiging.
     */
    const MOLLIE_DIRECT_DEBIT = 'directdebit';

    /**
     * Mollie-waarde voor iDEAL.
     */
    const MOLLIE_IDEAL = 'ideal';

    /**
     * Mollie-waarde voor PayPal.
     */
    const MOLLIE_PAYPAL = 'paypal';

    /**
     * Mollie-waarde voor BitCoin.
     */
    const MOLLIE_BITCOIN = 'bitcoin';

    /**
     * {@inheritdoc}
     */
    public static function fromRemote($value): PaymentMethod
    {
        Assertion::string($value);

        /*
         * string -> PaymentMethod mapping
         */
        $conversions = [
            self::MOLLIE_BANK_TRANSFER => PaymentMethod::BANK_TRANSFER,
            self::MOLLIE_CREDITCARD => PaymentMethod::CREDITCARD,
            self::MOLLIE_DIRECT_DEBIT => PaymentMethod::DIRECT_DEBIT,
            self::MOLLIE_IDEAL => PaymentMethod::IDEAL,
            self::MOLLIE_PAYPAL => PaymentMethod::PAYPAL,
            self::MOLLIE_BITCOIN => PaymentMethod::BITCOIN,
            self::MOLLIE_BANK_CONTACT => PaymentMethod::BANK_CONTACT,
            self::MOLLIE_BELFIUS => PaymentMethod::BELFIUS,
            self::MOLLIE_GIROPAY => PaymentMethod::GIROPAY,
            self::MOLLIE_INGHOMEPAY => PaymentMethod::INGHOMEPAY,
            self::MOLLIE_KBC => PaymentMethod::KBC,
            self::MOLLIE_SOFORT => PaymentMethod::SOFORT,
            self::MOLLIE_EPS => PaymentMethod::EPS,
        ];

        if (!isset($conversions[$value])) {
            throw TransformerConversionException::fromRemote(null, PaymentMethod::class, $value);
        }

        return PaymentMethod::from($conversions[$value]);
    }

    /**
     * {@inheritdoc}
     */
    public static function toRemote(PaymentMethod $method)
    {
        /*
         * PaymentMethod -> string mapping
         */
        $conversions = [
            PaymentMethod::BANK_TRANSFER => self::MOLLIE_BANK_TRANSFER,
            PaymentMethod::CREDITCARD => self::MOLLIE_CREDITCARD,
            PaymentMethod::DIRECT_DEBIT => self::MOLLIE_DIRECT_DEBIT,
            PaymentMethod::IDEAL => self::MOLLIE_IDEAL,
            PaymentMethod::PAYPAL => self::MOLLIE_PAYPAL,
            PaymentMethod::BITCOIN => self::MOLLIE_BITCOIN,
            PaymentMethod::BANK_CONTACT => self::MOLLIE_BANK_CONTACT,
            PaymentMethod::BELFIUS => self::MOLLIE_BELFIUS,
            PaymentMethod::GIROPAY => self::MOLLIE_GIROPAY,
            PaymentMethod::INGHOMEPAY => self::MOLLIE_INGHOMEPAY,
            PaymentMethod::KBC => self::MOLLIE_KBC,
            PaymentMethod::SOFORT => self::MOLLIE_SOFORT,
            PaymentMethod::EPS => self::MOLLIE_EPS,
        ];

        if (!isset($conversions[$method->getValue()])) {
            throw TransformerConversionException::fromLocal(PaymentMethod::class, null, $method->getValue());
        }

        return $conversions[$method->getValue()];
    }
}
