<?php

namespace NnShop\Infrastructure\Pages;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Pages\Value\PageId;

/**
 * Class PageIdType.
 */
class PageIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'pages.page_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return PageId::class;
    }
}
