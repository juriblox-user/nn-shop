<?php

namespace NnShop\Infrastructure\Pages\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\Pages\Entity\Page;
use NnShop\Domain\Pages\Repository\PageRepositoryInterface;
use NnShop\Domain\Pages\Value\PageId;

/**
 * Class PageDoctrineRepository.
 */
class PageDoctrineRepository extends AbstractDoctrineRepository implements PageRepositoryInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     */
    public function getById(PageId $pageId): Page
    {
        return $this->getBy([
            'id' => $pageId,
        ]);
    }
}
