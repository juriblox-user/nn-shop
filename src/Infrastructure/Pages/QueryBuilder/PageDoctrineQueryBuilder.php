<?php

namespace NnShop\Infrastructure\Pages\QueryBuilder;

use Core\Doctrine\ORM\AbstractQueryBuilder;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use NnShop\Domain\Pages\Entity\Page;
use NnShop\Domain\Pages\QueryBuilder\PageQueryBuilderInterface;
use NnShop\Domain\Translation\Value\ProfileId;

/**
 * Class PageDoctrineQueryBuilder.
 */
class PageDoctrineQueryBuilder extends AbstractQueryBuilder implements PageQueryBuilderInterface
{
    /**
     * @var ProfileId|null
     */
    private $profileId;

    /**
     * @return Query
     */
    public function build(): Query
    {
        $builder = $this->getBuilder()
            ->select('p')
            ->from(Page::class, 'p')
            ->orderBy('p.title');

        if ($this->profileId) {
            $builder->andWhere(
                $builder->expr()->eq('p.profile', ':profile')
            )
                ->setParameter('profile', $this->profileId);
        }

        return$builder->getQuery()->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class);
    }

    /**
     * {@inheritdoc}
     */
    public function reset(): PageQueryBuilderInterface
    {
        $this->profileId = null;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterProfile(ProfileId $profileId): PageQueryBuilderInterface
    {
        $this->profileId = $profileId;

        return $this;
    }
}
