<?php

namespace NnShop\Infrastructure\Translation\QueryBuilder;

use Core\Doctrine\ORM\AbstractQueryBuilder;
use Doctrine\ORM\Query;
use NnShop\Domain\Translation\Entity\Locale;
use NnShop\Domain\Translation\QueryBuilder\LocaleQueryBuilderInterface;

/**
 * Class LocaleDoctrineQueryBuilder.
 */
class LocaleDoctrineQueryBuilder extends AbstractQueryBuilder implements LocaleQueryBuilderInterface
{
    /**
     * @return Query
     */
    public function build(): Query
    {
        return $this->getBuilder()
            ->select('l')
            ->from(Locale::class, 'l')
            ->orderBy('l.locale')
            ->getQuery();
    }

    /**
     * {@inheritdoc}
     */
    public function reset(): LocaleQueryBuilderInterface
    {
        return $this;
    }
}
