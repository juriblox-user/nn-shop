<?php

namespace NnShop\Infrastructure\Translation\QueryBuilder;

use Core\Doctrine\ORM\AbstractQueryBuilder;
use Doctrine\ORM\Query;
use NnShop\Domain\Translation\Entity\Profile;
use NnShop\Domain\Translation\QueryBuilder\ProfileQueryBuilderInterface;

/**
 * Class ProfileDoctrineQueryBuilder.
 */
class ProfileDoctrineQueryBuilder extends AbstractQueryBuilder implements ProfileQueryBuilderInterface
{
    /**
     * @return Query
     */
    public function build(): Query
    {
        return $this->getBuilder()
            ->select('p')
            ->from(Profile::class, 'p')
            ->orderBy('p.country')
            ->getQuery();
    }

    /**
     * {@inheritdoc}
     */
    public function reset(): ProfileQueryBuilderInterface
    {
        return $this;
    }
}
