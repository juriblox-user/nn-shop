<?php

namespace NnShop\Infrastructure\Translation;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Translation\Value\LocaleId;

/**
 * Class LocaleIdType.
 */
class LocaleIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'locales.locale_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return LocaleId::class;
    }
}
