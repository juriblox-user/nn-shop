<?php

namespace NnShop\Infrastructure\Translation\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\Translation\Entity\Locale;
use NnShop\Domain\Translation\Repository\LocaleRepositoryInterface;
use NnShop\Domain\Translation\Value\LocaleId;

/**
 * Class LocaleDoctrineRepository.
 */
class LocaleDoctrineRepository extends AbstractDoctrineRepository implements LocaleRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function getById(LocaleId $id): Locale
    {
        return $this->getBy([
            'id' => $id,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(LocaleId $id): Locale
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
