<?php

namespace NnShop\Infrastructure\Translation\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\Translation\Entity\Profile;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;
use NnShop\Domain\Translation\Value\ProfileId;

/**
 * Class ProfileDoctrineRepository.
 */
class ProfileDoctrineRepository extends AbstractDoctrineRepository implements ProfileRepositoryInterface
{
    /**
     * @param string $host
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @return Profile|null
     */
    public function findByHost(string $host)
    {
        $builder = $this->createQueryBuilder('p');

        return $builder
            ->select('p, mainLocale')
            ->join('p.mainLocale', 'mainLocale')
            ->where(
                $builder->expr()->eq('p.url', ':host')
            )
            ->setParameter('host', $host)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $slug
     *
     * @return Profile|null|object
     */
    public function findOneBySlug(string $slug)
    {
        return $this->findOneBy([
            'slug' => $slug,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     */
    public function getById(ProfileId $id): Profile
    {
        return $this->getBy([
            'id' => $id,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getFirst()
    {
        $profile = $this->createQueryBuilder('p')
            ->select('p.id')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();

        if ($profile) {
            return $profile['id'];
        }
    }

    /**
     * @return array
     *
     * Return entities for backend menus
     */
    public function getMenu(): array
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.url')
            ->getQuery()
            ->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(ProfileId $id): Profile
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
