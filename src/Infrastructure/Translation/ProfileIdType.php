<?php

namespace NnShop\Infrastructure\Translation;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Translation\Value\ProfileId;

/**
 * Class ProfileIdType.
 */
class ProfileIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'profiles.profile_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return ProfileId::class;
    }
}
