<?php

namespace NnShop\Infrastructure\Shops\QueryBuilder;

use Core\Doctrine\ORM\AbstractQueryBuilder;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use NnShop\Domain\Shops\Entity\LandingPage;
use NnShop\Domain\Shops\QueryBuilder\LandingPageQueryBuilderInterface;
use NnShop\Domain\Translation\Value\ProfileId;

class LandingPageDoctrineQueryBuilder extends AbstractQueryBuilder implements LandingPageQueryBuilderInterface
{
    /**
     * @var ProfileId|null
     */
    private $profileId;

    /**
     * {@inheritdoc}
     */
    public function build(): Query
    {
        $builder = $this->getBuilder()
            ->select(['p'])
            ->from(LandingPage::class, 'p')
            ->orderBy('p.title');

        if ($this->profileId) {
            $builder->andWhere(
                $builder->expr()->eq('p.profile', ':profile')
            )
                ->setParameter('profile', $this->profileId);
        }

        return$builder->getQuery()->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class);
    }

    /**
     * {@inheritdoc}
     */
    public function reset(): self
    {
        $this->profileId = null;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterProfile(ProfileId $profileId): LandingPageQueryBuilderInterface
    {
        $this->profileId = $profileId;

        return $this;
    }
}
