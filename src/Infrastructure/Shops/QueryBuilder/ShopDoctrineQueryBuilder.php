<?php

namespace NnShop\Infrastructure\Shops\QueryBuilder;

use Core\Doctrine\ORM\AbstractQueryBuilder;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use NnShop\Domain\Shops\Entity\Shop;
use NnShop\Domain\Shops\QueryBuilder\ShopQueryBuilderInterface;
use NnShop\Domain\Translation\Value\ProfileId;

class ShopDoctrineQueryBuilder extends AbstractQueryBuilder implements ShopQueryBuilderInterface
{
    /**
     * @var ProfileId|null
     */
    private $profileId;

    /**
     * {@inheritdoc}
     */
    public function build(): Query
    {
        $builder = $this->getBuilder();

        $builder
            ->select(['s'])
            ->from(Shop::class, 's')
            ->where(
                $builder->expr()->isNull('s.timestampDeleted')
            )
            ->orderBy('s.title');

        if ($this->profileId) {
            $builder->andWhere(
                $builder->expr()->eq('s.profile', ':profile')
            )
                ->setParameter('profile', $this->profileId);
        }

        return$builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class);
    }

    /**
     * {@inheritdoc}
     */
    public function reset(): ShopQueryBuilderInterface
    {
        $this->profileId = null;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterProfile(ProfileId $profileId): ShopQueryBuilderInterface
    {
        $this->profileId = $profileId;

        return $this;
    }
}
