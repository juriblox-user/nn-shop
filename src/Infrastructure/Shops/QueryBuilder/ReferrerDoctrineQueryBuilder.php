<?php

namespace NnShop\Infrastructure\Shops\QueryBuilder;

use Core\Doctrine\ORM\AbstractQueryBuilder;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use NnShop\Domain\Shops\Entity\Referrer;
use NnShop\Domain\Shops\QueryBuilder\ReferrerQueryBuilderInterface;
use NnShop\Domain\Translation\Value\ProfileId;

class ReferrerDoctrineQueryBuilder extends AbstractQueryBuilder implements ReferrerQueryBuilderInterface
{
    /**
     * @var ProfileId|null
     */
    private $profileId;

    /**
     * {@inheritdoc}
     */
    public function build(): Query
    {
        $builder = $this->getBuilder();

        $builder
            ->select(['r'])
            ->from(Referrer::class, 'r')
            ->where(
                $builder->expr()->isNull('r.timestampDeleted')
            )
            ->orderBy('r.title');

        if ($this->profileId) {
            $builder->andWhere(
                $builder->expr()->eq('r.profile', ':profile')
            )
                ->setParameter('profile', $this->profileId);
        }

        return$builder->getQuery()->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class);
    }

    /**
     * {@inheritdoc}
     */
    public function reset(): ReferrerQueryBuilderInterface
    {
        $this->profileId = null;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterProfile(ProfileId $profileId): ReferrerQueryBuilderInterface
    {
        $this->profileId = $profileId;

        return $this;
    }
}
