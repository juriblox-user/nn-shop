<?php

namespace NnShop\Infrastructure\Shops;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Shops\Value\TestimonialId;

class TestimonialIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'shops.testimonial_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return TestimonialId::class;
    }
}
