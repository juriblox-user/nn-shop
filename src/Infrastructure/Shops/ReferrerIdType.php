<?php

namespace NnShop\Infrastructure\Shops;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Shops\Value\ReferrerId;

class ReferrerIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'shops.referrer_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return ReferrerId::class;
    }
}
