<?php

namespace NnShop\Infrastructure\Shops;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Shops\Value\ShopId;

class ShopIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'shops.shop_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return ShopId::class;
    }
}
