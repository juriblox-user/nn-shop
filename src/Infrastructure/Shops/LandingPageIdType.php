<?php

namespace NnShop\Infrastructure\Shops;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Shops\Value\LandingPageId;

class LandingPageIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'shops.landing_page_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return LandingPageId::class;
    }
}
