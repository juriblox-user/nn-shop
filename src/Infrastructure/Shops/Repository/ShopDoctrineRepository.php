<?php

namespace NnShop\Infrastructure\Shops\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use NnShop\Domain\Shops\Entity\Shop;
use NnShop\Domain\Shops\Repository\ShopRepositoryInterface;
use NnShop\Domain\Shops\Value\ShopId;
use NnShop\Domain\Translation\Entity\Profile;

/**
 * Class ShopDoctrineRepository.
 */
class ShopDoctrineRepository extends AbstractDoctrineRepository implements ShopRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAll(): array
    {
        $builder = $this->createQueryBuilder('s');

        $builder
            ->where(
                $builder->expr()->isNull('s.timestampDeleted')
            )
            ->orderBy('s.title');

        return $builder->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllVisible(Profile $profile): array
    {
        $builder = $this->createQueryBuilder('s');

        $builder
            ->where(
                $builder->expr()->eq('s.enabled', ':true'),
                $builder->expr()->isNull('s.timestampDeleted')
            )
            ->andWhere('s.profile=:profile')
            ->orderBy('s.title')
            ->setParameters([
                'true' => true,
                'profile' => $profile,
            ]);

        return$builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByHostname($hostname)
    {
        /** @var Shop $shop */
        $shop = $this->findOneBy([
            'hostname' => $hostname,
            'timestampDeleted' => null,
        ]);

        return $shop;
    }

    /**
     * {@inheritdoc}
     */
    public function findOneById(ShopId $id)
    {
        return $this->findOneBy([
            'id' => $id,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getByHostname($hostname): Shop
    {
        /** @var Shop $shop */
        $shop = $this->getBy([
            'hostname' => $hostname,
            'timestampDeleted' => null,
        ]);

        return $shop;
    }

    /**
     * {@inheritdoc}
     */
    public function getById(ShopId $id): Shop
    {
        return $this->getBy([
            'id' => $id,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function getReference(ShopId $id): Shop
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }

    /**
     * {@inheritdoc}
     */
    public function hasWithHostname($hostname): bool
    {
        return null !== $this->findOneByHostname($hostname);
    }

    /**
     * @return array
     */
    public function getMenu(): array
    {
        return $this->createQueryBuilder('s')
            ->orderBy('s.title')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Profile $profile
     *
     * @return array
     */
    public function getMenuByProfile(Profile $profile): array
    {
        $builder = $this->createQueryBuilder('s');

        return $builder
            ->where(
                $builder->expr()->eq('s.profile', ':profile')
            )
            ->setParameters([
                'profile' => $profile,
            ])
            ->setParameter('profile', $profile)
            ->orderBy('s.title')
            ->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getResult();
    }

    /**
     * @param string $host
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @return Shop|null
     */
    public function findByHost(string $host)
    {
        return $this->createQueryBuilder('s')
            ->where('s.hostname=:host')
            ->setParameter('host', $host)
            ->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getOneOrNullResult();
    }
}
