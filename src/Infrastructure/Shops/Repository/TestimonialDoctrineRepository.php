<?php

namespace NnShop\Infrastructure\Shops\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\Shops\Entity\Testimonial;
use NnShop\Domain\Shops\Repository\TestimonialRepositoryInterface;
use NnShop\Domain\Shops\Value\TestimonialId;

class TestimonialDoctrineRepository extends AbstractDoctrineRepository implements TestimonialRepositoryInterface
{
    /**
     * @param TestimonialId $id
     *
     * @return Testimonial|null
     */
    public function findOneById(TestimonialId $id)
    {
        /** @var Testimonial $testimonial */
        $testimonial = $this->findOneBy([
            'id' => $id,
        ]);

        return $testimonial;
    }

    /**
     * @param TestimonialId $id
     *
     * @return Testimonial
     */
    public function getById(TestimonialId $id): Testimonial
    {
        /** @var Testimonial $testimonial */
        $testimonial = $this->getBy([
            'id' => $id,
        ]);

        return $testimonial;
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(TestimonialId $id): Testimonial
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
