<?php

namespace NnShop\Infrastructure\Shops\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use NnShop\Domain\Shops\Entity\Referrer;
use NnShop\Domain\Shops\Repository\ReferrerRepositoryInterface;
use NnShop\Domain\Shops\Value\ReferrerId;
use NnShop\Domain\Translation\Entity\Profile;

class ReferrerDoctrineRepository extends AbstractDoctrineRepository implements ReferrerRepositoryInterface
{
    /**
     * @return Referrer[]
     */
    public function findAll(): array
    {
        $builder = $this->createQueryBuilder('r');

        $builder
            ->where(
                $builder->expr()->isNull('r.timestampDeleted')
            )
            ->orderBy('r.title');

        return $builder->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(ReferrerId $id)
    {
        $builder = $this->createQueryBuilder('r');

        $builder->where(
            $builder->expr()->eq('r.id', ':id')
        )
            ->setParameters([
                'id' => $id,
            ]);

        return$builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getOneOrNullResult();
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneBySlug(string $slug)
    {
        $builder = $this->createQueryBuilder('r');

        $builder->where(
            $builder->expr()->eq('r.slug', ':slug'),
            $builder->expr()->isNull('r.timestampDeleted')
        )
            ->setParameters([
                'slug' => $slug,
            ]);

        return$builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getOneOrNullResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findShuffledForHomepage(int $limit, Profile $profile): array
    {
        $builder = $this->createQueryBuilder('r');

        $builder
            ->where(
                $builder->expr()->eq('r.homepage', ':true'),
                $builder->expr()->isNull('r.timestampDeleted')
                        )
            ->andWhere('r.profile=:profile')
            ->orderBy('r.title')
            ->setParameters([
                'true' => true,
                'profile' => $profile,
            ]);

        $query = $builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class);

        $referrers = $query->getResult();
        shuffle($referrers);

        return \array_slice($referrers, 0, $limit);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     */
    public function getById(ReferrerId $id): Referrer
    {
        return $this->getBy([
            'id' => $id,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     */
    public function getBySlug($slug): Referrer
    {
        /** @var Referrer $referrer */
        $referrer = $this->getBy([
            'slug' => $slug,
            'timestampDeleted' => null,
        ]);

        return $referrer;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function getReference(ReferrerId $id): Referrer
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
