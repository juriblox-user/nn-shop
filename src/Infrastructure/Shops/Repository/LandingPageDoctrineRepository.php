<?php

namespace NnShop\Infrastructure\Shops\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use NnShop\Domain\Shops\Entity\LandingPage;
use NnShop\Domain\Shops\Repository\LandingPageRepositoryInterface;
use NnShop\Domain\Shops\Value\LandingPageId;

class LandingPageDoctrineRepository extends AbstractDoctrineRepository implements LandingPageRepositoryInterface
{
    /**
     * @return LandingPage[]
     */
    public function findAll(): array
    {
        $builder = $this->createQueryBuilder('p')
            ->orderBy('p.title');

        return $builder->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(LandingPageId $id)
    {
        $builder = $this->createQueryBuilder('p');

        $builder->where('p.id=:id')
            ->setParameter('id', $id);

        return $builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getOneOrNullResult();
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneBySlug(string $slug)
    {
        $builder = $this->createQueryBuilder('p');

        $builder->where('p.slug=:slug')
            ->setParameter('slug', $slug);

        return $builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getOneOrNullResult();
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     */
    public function getById(LandingPageId $id): LandingPage
    {
        return $this->getBy([
            'id' => $id,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function getReference(LandingPageId $id): LandingPage
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
