<?php

namespace NnShop\Infrastructure\JuriBlox;

use Core\Doctrine\DBAL\Types\AbstractIntegerType;
use JuriBlox\Sdk\Domain\Documents\Values\TemplateId;

class TemplateIdType extends AbstractIntegerType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'juriblox.template_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return TemplateId::class;
    }
}
