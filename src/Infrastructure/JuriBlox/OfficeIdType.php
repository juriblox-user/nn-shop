<?php

namespace NnShop\Infrastructure\JuriBlox;

use Core\Doctrine\DBAL\Types\TypeInterface;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\SmallIntType;
use JuriBlox\Sdk\Domain\Offices\Values\OfficeId;

class OfficeIdType extends SmallIntType implements TypeInterface
{
    /**
     * {@inheritdoc}
     *
     * @param $value OfficeId
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return (null === $value) ? null : $value->getInteger();
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return (null === $value) ? null : new OfficeId($value);
    }

    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'juriblox.office_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return OfficeId::class;
    }
}
