<?php

namespace NnShop\Infrastructure\JuriBlox;

use Core\Doctrine\DBAL\Types\AbstractIntegerType;
use JuriBlox\Sdk\Domain\Documents\Values\DocumentRequestId;

class DocumentRequestIdType extends AbstractIntegerType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'juriblox.document_request_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return DocumentRequestId::class;
    }
}
