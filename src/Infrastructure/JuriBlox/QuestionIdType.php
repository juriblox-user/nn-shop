<?php

namespace NnShop\Infrastructure\JuriBlox;

use Core\Doctrine\DBAL\Types\AbstractIntegerType;
use JuriBlox\Sdk\Domain\Documents\Values\QuestionId;

class QuestionIdType extends AbstractIntegerType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'juriblox.question_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return QuestionId::class;
    }
}
