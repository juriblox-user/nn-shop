<?php

namespace NnShop\Infrastructure\JuriBlox;

use Core\Doctrine\DBAL\Types\AbstractStringType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use JuriBlox\Sdk\Domain\Customers\Values\CustomerReference;

class CustomerReferenceType extends AbstractStringType
{
    /**
     * {@inheritdoc}
     *
     * @param $value CustomerReference
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return (null === $value) ? null : $value->getString();
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return (null === $value) ? null : new CustomerReference($value);
    }

    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'juriblox.customer_reference';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return CustomerReference::class;
    }
}
