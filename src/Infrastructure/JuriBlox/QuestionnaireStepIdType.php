<?php

namespace NnShop\Infrastructure\JuriBlox;

use Core\Doctrine\DBAL\Types\AbstractIntegerType;
use JuriBlox\Sdk\Domain\Documents\Values\QuestionnaireStepId;

class QuestionnaireStepIdType extends AbstractIntegerType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'juriblox.step_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return QuestionnaireStepId::class;
    }
}
