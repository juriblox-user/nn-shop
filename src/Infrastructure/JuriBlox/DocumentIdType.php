<?php

namespace NnShop\Infrastructure\JuriBlox;

use Core\Doctrine\DBAL\Types\AbstractIntegerType;
use JuriBlox\Sdk\Domain\Documents\Values\DocumentId;

class DocumentIdType extends AbstractIntegerType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'juriblox.document_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return DocumentId::class;
    }
}
