<?php

namespace NnShop\Infrastructure\Templates;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Templates\Value\QuestionOptionId;

class QuestionOptionIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'templates.question_option_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return QuestionOptionId::class;
    }
}
