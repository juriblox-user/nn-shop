<?php

namespace NnShop\Infrastructure\Templates;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Templates\Value\AnswerId;

class AnswerIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'templates.answer_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return AnswerId::class;
    }
}
