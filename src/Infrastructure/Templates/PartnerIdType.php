<?php

namespace NnShop\Infrastructure\Templates;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Templates\Value\PartnerId;

class PartnerIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'templates.partner_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return PartnerId::class;
    }
}
