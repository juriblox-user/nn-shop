<?php

namespace NnShop\Infrastructure\Templates;

use Core\Doctrine\DBAL\Types\AbstractStringEnumerationType;
use NnShop\Domain\Templates\Enumeration\QuestionType;

class QuestionTypeType extends AbstractStringEnumerationType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'templates.question_type';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return QuestionType::class;
    }
}
