<?php

namespace NnShop\Infrastructure\Templates;

use Core\Doctrine\DBAL\Types\AbstractStringEnumerationType;
use NnShop\Domain\Templates\Enumeration\DocumentStatus;

class DocumentStatusType extends AbstractStringEnumerationType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'templates.document_status';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return DocumentStatus::class;
    }
}
