<?php

namespace NnShop\Infrastructure\Templates;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Templates\Value\CategoryId;

class CategoryIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'templates.category_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return CategoryId::class;
    }
}
