<?php

namespace NnShop\Infrastructure\Templates;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Templates\Value\QuestionId;

class QuestionIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'templates.question_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return QuestionId::class;
    }
}
