<?php

namespace NnShop\Infrastructure\Templates;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Templates\Value\TemplateId;

class TemplateIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'templates.template_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return TemplateId::class;
    }
}
