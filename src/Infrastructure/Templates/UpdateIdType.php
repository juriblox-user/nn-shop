<?php

namespace NnShop\Infrastructure\Templates;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Templates\Value\UpdateId;

class UpdateIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'templates.update_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return UpdateId::class;
    }
}
