<?php

namespace NnShop\Infrastructure\Templates\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use JuriBlox\Sdk\Domain\Documents\Values\QuestionId as RemoteId;
use NnShop\Domain\Templates\Entity\Question;
use NnShop\Domain\Templates\Entity\QuestionStep;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Templates\Repository\QuestionRepositoryInterface;
use NnShop\Domain\Templates\Value\QuestionId;

class QuestionDoctrineRepository extends AbstractDoctrineRepository implements QuestionRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findByTemplate(Template $template): array
    {
        $builder = $this->createQueryBuilder('q');

        $builder
            ->join('q.step', 's')

            ->where(
                $builder->expr()->eq('s.template', ':template'),
                $builder->expr()->isNull('q.timestampDeleted')
            )

            ->setParameters([
                'template' => $template,
            ]);

        return $builder->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findOneById(QuestionId $id)
    {
        /** @var Question $question */
        $question = $this->findOneBy([
            'id' => $id,
        ]);

        return $question;
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByLookupId(RemoteId $id, QuestionStep $step = null)
    {
        /** @var Question|null $question */
        $question = null;

        if (null !== $step) {
            $question = $this->findOneBy([
                'lookupId' => $id,
                'step' => $step,
            ]);
        } else {
            trigger_error('Calling ' . __METHOD__ . ' without a QuestionStep entity is deprecated', E_USER_DEPRECATED);

            $question = $this->findOneBy([
                'lookupId' => $id,
            ]);
        }

        return $question;
    }

    /**
     * {@inheritdoc}
     */
    public function getById(QuestionId $id): Question
    {
        /** @var Question $question */
        $question = $this->getBy([
            'id' => $id,
        ]);

        return $question;
    }

    /**
     * {@inheritdoc}
     */
    public function getByLookupId(RemoteId $id, QuestionStep $step = null): Question
    {
        /** @var Question|null $question */
        $question = null;

        if (null !== $step) {
            $question = $this->getBy([
                'lookupId' => $id,
                'step' => $step,
            ]);
        } else {
            trigger_error('Calling ' . __METHOD__ . ' without a QuestionStep entity is deprecated', E_USER_DEPRECATED);

            $question = $this->getBy([
                'lookupId' => $id,
            ]);
        }

        return $question;
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(QuestionId $id): Question
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
