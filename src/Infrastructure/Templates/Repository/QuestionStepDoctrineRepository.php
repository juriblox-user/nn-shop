<?php

namespace NnShop\Infrastructure\Templates\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use Doctrine\ORM\Query\Expr\Join;
use JuriBlox\Sdk\Domain\Documents\Values\QuestionnaireStepId as RemoteId;
use NnShop\Domain\Templates\Entity\Document;
use NnShop\Domain\Templates\Entity\QuestionStep;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Templates\Repository\QuestionStepRepositoryInterface;
use NnShop\Domain\Templates\Value\QuestionStepId;

class QuestionStepDoctrineRepository extends AbstractDoctrineRepository implements QuestionStepRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findOneById(QuestionStepId $id)
    {
        /** @var QuestionStep $step */
        $step = $this->findOneBy([
            'id' => $id,
        ]);

        return $step;
    }

    /**
     * @param RemoteId      $id
     * @param Template|null $template
     *
     * @return QuestionStep|null
     */
    public function findOneByRemoteId(RemoteId $id, Template $template = null)
    {
        /** @var QuestionStep|null $step */
        $step = null;

        if (null !== $template) {
            $step = $this->findOneBy([
                'remoteId' => $id,
                'template' => $template,
            ]);
        } else {
            trigger_error('Calling ' . __METHOD__ . ' without a Template entity is deprecated', E_USER_DEPRECATED);

            $step = $this->findOneBy([
                'remoteId' => $id,
            ]);
        }

        return $step;
    }

    /**
     * {@inheritdoc}
     */
    public function getById(QuestionStepId $id): QuestionStep
    {
        /** @var QuestionStep $step */
        $step = $this->getBy([
            'id' => $id,
        ]);

        return $step;
    }

    /**
     * {@inheritdoc}
     */
    public function getGraph(Document $document): array
    {
        $builder = $this->createQueryBuilder('s');

        $builder->addSelect(['q', 'qo', 'c', 'a', 'ao'])
            ->join('s.questions', 'q')
            ->leftJoin('q.options', 'qo')
            ->leftJoin('q.conditions', 'c')
            ->leftJoin('q.answers', 'a', Join::WITH, 'a.document = :document')
            ->leftJoin('a.options', 'ao')

            ->where(
                $builder->expr()->eq('s.template', ':template')
            )

            ->orderBy('s.position')
            ->addOrderBy('q.position')
            ->addOrderBy('qo.position')

            ->setParameters([
                'document' => $document,
                'template' => $document->getTemplate(),
            ]);

        return $builder->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(QuestionStepId $id): QuestionStep
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
