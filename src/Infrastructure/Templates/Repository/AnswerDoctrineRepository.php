<?php

namespace NnShop\Infrastructure\Templates\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\Templates\Entity\Answer;
use NnShop\Domain\Templates\Entity\Document;
use NnShop\Domain\Templates\Entity\Question;
use NnShop\Domain\Templates\Repository\AnswerRepositoryInterface;
use NnShop\Domain\Templates\Value\AnswerId;

class AnswerDoctrineRepository extends AbstractDoctrineRepository implements AnswerRepositoryInterface
{
    /**
     * @param Question $question
     *
     * @return array|Answer[]
     */
    public function findByQuestion(Question $question): array
    {
        // LET OP: verwijderd of niet, als we zoeken op Question willen we álles. Zie SynchronizeTemplateHandler

        return $this->findBy([
            'question' => $question,
        ]);
    }

    /**
     * @param Document $document
     * @param Question $question
     *
     * @return Answer|null
     */
    public function findOneByDocumentAndQuestion(Document $document, Question $question)
    {
        /** @var Answer $answer */
        $answer = $this->findOneBy([
            'document' => $document,
            'question' => $question,

            'timestampDeleted' => null,
        ]);

        return $answer;
    }

    /**
     * {@inheritdoc}
     */
    public function findOneById(AnswerId $id)
    {
        /** @var Answer $answer */
        $answer = $this->findOneBy([
            'id' => $id,
        ]);

        return $answer;
    }

    /**
     * {@inheritdoc}
     */
    public function getById(AnswerId $id): Answer
    {
        /** @var Answer $answer */
        $answer = $this->getBy([
            'id' => $id,
        ]);

        return $answer;
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(AnswerId $id): Answer
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
