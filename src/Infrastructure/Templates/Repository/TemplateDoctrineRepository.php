<?php

namespace NnShop\Infrastructure\Templates\Repository;

use Core\Common\Exception\Domain\EntityNotFoundException;
use Core\Common\Exception\NotImplementedException;
use Core\Infrastructure\AbstractDoctrineRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use JuriBlox\Sdk\Domain\Documents\Values\TemplateId as RemoteId;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Enumeration\OrderStatus;
use NnShop\Domain\Shops\Entity\LandingPage;
use NnShop\Domain\Shops\Entity\Referrer;
use NnShop\Domain\Shops\Entity\Shop;
use NnShop\Domain\Templates\Entity\Category;
use NnShop\Domain\Templates\Entity\CategoryTemplate;
use NnShop\Domain\Templates\Entity\Partner;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use NnShop\Domain\Templates\Value\PartnerId;
use NnShop\Domain\Templates\Value\TemplateId;
use NnShop\Domain\Templates\Value\TemplateKey;
use NnShop\Domain\Translation\Entity\Profile;

class TemplateDoctrineRepository extends AbstractDoctrineRepository implements TemplateRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAll(): array
    {
        $builder = $this->createQueryBuilder('t');

        $builder
            ->addSelect(['cl', 'c'])

            ->leftJoin('t.categoryLinks', 'cl')
            ->leftJoin('cl.category', 'c')

            ->where(
                $builder->expr()->isNull('t.timestampDeleted')
            )

            ->orderBy('t.title');

        return $builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findByPartner(PartnerId $partnerId): array
    {
        return $this->findBy([
            'partner' => $partnerId,
            'timestampDeleted' => null,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function findPublishedByString(string $query): array
    {
        $builder = $this->createQueryBuilder('t');

        $builder
            ->select('t')
            ->where(
                $builder->expr()->eq('t.published', ':true'),
                $builder->expr()->like('t.title', ':query'),
                $builder->expr()->eq('t.corrupted', ':false')
            )
            ->setParameters([
                'query' => '%' . $query . '%',
                'true' => true,
                'false' => false,
            ])
        ->getQuery()
        ->getArrayResult();

        return $builder->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findCategories(Template $template): array
    {
        $builder = $this->getEntityManager()->createQueryBuilder()
            ->from(CategoryTemplate::class, 'cl');

        $builder->addSelect('c')
            ->addSelect('cl')
            ->addSelect('s')

            ->join('cl.category', 'c')
            ->join('c.shop', 's')

            ->where(
                $builder->expr()->eq('cl.template', ':template')
            )

            ->orderBy('s.title')
            ->addOrderBy('c.title')

            ->setParameters([
                'template' => $template->getId(),
            ]);

        return$builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getResult();
    }

    /**
     * @param Category $category
     * @param string   $slug
     *
     * @return Template|null
     */
    public function findOneByCategoryAndSlug(Category $category, string $slug)
    {
        /** @var Template $template */
        $template = $this->findOneBy([
            'category' => $category,
            'slug' => $slug,

            'timestampDeleted' => null,
        ]);

        return $template;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(TemplateId $id)
    {
        $builder = $this->createQueryBuilder('t');

        $builder->where(
            $builder->expr()->eq('t.id', ':id')
        )
            ->setParameters([
                'id' => $id,
            ]);

        return$builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getOneOrNullResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByKey(TemplateKey $key)
    {
        /** @var Template $template */
        $template = $this->findOneBy([
            'key' => $key,

            'timestampDeleted' => null,
        ]);

        return $template;
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByRemoteId(RemoteId $id, Partner $partner = null)
    {
        if (null !== $partner) {
            return $this->findOneBy([
                'remoteId' => $id,
                'partner' => $partner,
            ]);
        }

        trigger_error('Calling ' . __METHOD__ . ' without a Partner entity is deprecated', E_USER_DEPRECATED);

        return $this->findOneBy([
            'remoteId' => $id,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneBySlugs(string $categorySlug, string $templateSlug)
    {
        $builder = $this->createQueryBuilder('t');

        $builder->addSelect('cl')
            ->addSelect('c')
            ->addSelect('partner')

            ->join('t.partner', 'partner')
            ->join('t.categoryLinks', 'cl')
            ->join('cl.category', 'c')

            ->join('t.categoryLinks', 'scl')
            ->join('scl.category', 'sc')

            ->where($builder->expr()->andX(
                $builder->expr()->eq('sc.slug', ':categorySlug'),

                $builder->expr()->eq('t.slug', ':templateSlug'),

                $builder->expr()->eq('t.corrupted', ':false'),
                $builder->expr()->eq('t.published', ':true'),

                $builder->expr()->isNull('t.timestampDeleted')
            ))

            ->setParameters([
                'true' => true,
                'false' => false,
                'categorySlug' => $categorySlug,
                'templateSlug' => $templateSlug,
            ]);

        return $builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getOneOrNullResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findRecentOrders(Template $template, int $limit): array
    {
        $builder = $builder = $this->getEntityManager()->createQueryBuilder()
            ->from(Order::class, 'o');

        $builder
            ->select('o')
            ->join('o.document', 'd')

            ->where($builder->expr()->andX(
                $builder->expr()->eq('d.template', ':template'),
                $builder->expr()->eq('o.status', ':completed')
            ))

            ->orderBy('o.timestampCreated', 'DESC')

            ->setParameters([
                'template' => $template,
                'completed' => OrderStatus::COMPLETED,
            ]);

        $query = $builder->getQuery();
        $query->setMaxResults($limit);

        return $query->getResult();
    }

    /**
     * @param int|null $limit
     *
     * @return array|Template[]
     */
    public function findShuffledSpotlight(int $limit = null): array
    {
        $builder = $this->createQueryBuilder('t');

        $builder->addSelect(['cl', 'c'])
            ->leftJoin('t.categoryLinks', 'cl')
            ->leftJoin('cl.category', 'c')

            ->where(
                $builder->expr()->eq('t.corrupted', ':false'),
                $builder->expr()->eq('t.published', ':true'),
                $builder->expr()->eq('t.spotlight', ':true'),
                $builder->expr()->eq('cl.primary', ':true'),
                $builder->expr()->eq('t.hidden', ':false'),

                $builder->expr()->isNull('t.timestampDeleted')
            )
            ->orderBy('t.title')

            ->setParameters([
                'true' => true,
                'false' => false,
            ]);

        $query = $builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class);

        $templates = $query->getResult();
        shuffle($templates);

        return \array_slice($templates, 0, $limit);
    }

    /**
     * {@inheritdoc}
     */
    public function findVisible(Profile $profile): array
    {
        $builder = $this->createQueryBuilder('t');

        $builder->addSelect('cl')
            ->addSelect('c')

            ->join('t.categoryLinks', 'cl')
            ->join('cl.category', 'c')
            ->join('c.shop', 's')
            ->join('t.partner', 'partner')

            ->where(
                $builder->expr()->eq('t.corrupted', ':false'),
                $builder->expr()->eq('t.published', ':true'),
                $builder->expr()->eq('s.enabled', ':true'),
                $builder->expr()->eq('t.hidden', ':false'),

                $builder->expr()->isNull('t.timestampDeleted')
            )
            ->andWhere('partner.countryProfile=:profile')

            ->orderBy('t.title')
            ->addOrderBy('c.title')

            ->setParameters([
                'true' => true,
                'false' => false,
                'profile' => $profile,
            ]);

        return$builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findVisibleByActivityIds($activityIds): array
    {
        $builder = $this->createQueryBuilder('t');

        $builder->addSelect('tl')
            ->addSelect('partner')
            ->addSelect('cl')
            ->addSelect('c')
            ->join('t.activityLinks', 'tl')
            ->join('t.categoryLinks', 'cl')
            ->join('cl.category', 'c')
            ->join('c.shop', 's')
            ->join('t.partner', 'partner')

            ->where(
                $builder->expr()->in('tl.activity', $activityIds),
                $builder->expr()->eq('t.corrupted', ':false'),
                $builder->expr()->eq('t.published', ':true'),
                $builder->expr()->eq('s.enabled', ':true'),
                $builder->expr()->eq('t.hidden', ':false'),

                $builder->expr()->isNull('t.timestampDeleted')
            )

            ->orderBy('t.title')
            ->setParameters([
                'true' => true,
                'false' => false,
            ]);

        return $builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findVisibleByLandingPage(LandingPage $landingPage): array
    {
        $builder = $this->createQueryBuilder('t');

        $builder->addSelect('cl')
            ->addSelect('c')
            ->addSelect('s')

            ->join('t.landingPageLinks', 'pl')

            ->join('t.categoryLinks', 'cl')
            ->join('cl.category', 'c')
            ->join('c.shop', 's')

            ->where(
                $builder->expr()->eq('pl.landingPage', ':landingPage'),
                $builder->expr()->eq('t.corrupted', ':false'),
                $builder->expr()->eq('t.published', ':true'),
                $builder->expr()->eq('s.enabled', ':true'),

                $builder->expr()->isNull('t.timestampDeleted')
            )

            ->orderBy('t.title')
            ->setParameters([
                'true' => true,
                'false' => false,
                'landingPage' => $landingPage,
            ]);

        return $builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findVisibleByReferrer(Referrer $referrer): array
    {
        $builder = $this->createQueryBuilder('t');

        $builder->addSelect('cl')
            ->addSelect('c')
            ->addSelect('s')

            ->join('t.referrerLinks', 'rl')

            ->join('t.categoryLinks', 'cl')
            ->join('cl.category', 'c')
            ->join('c.shop', 's')

            ->where(
                $builder->expr()->eq('rl.referrer', ':referrer'),
                $builder->expr()->eq('t.corrupted', ':false'),
                $builder->expr()->eq('t.published', ':true'),
                $builder->expr()->eq('s.enabled', ':true'),

                $builder->expr()->isNull('t.timestampDeleted')
            )

            ->orderBy('t.title')
            ->setParameters([
                'true' => true,
                'false' => false,
                'referrer' => $referrer,
            ]);

        return $builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getResult();
    }

    /**
     * @param Profile $profile
     *
     * @return array
     */
    public function findVisibleWithCategoriesAndShops(Profile $profile): array
    {
        $builder = $this->createQueryBuilder('t');

        $builder->addSelect('cl')
            ->addSelect('c')
            ->addSelect('s')

            ->join('t.categoryLinks', 'cl')
            ->join('cl.category', 'c')
            ->join('c.shop', 's')
            ->join('t.partner', 'partner')

            ->where(
                $builder->expr()->eq('t.corrupted', ':false'),
                $builder->expr()->eq('t.published', ':true'),
                $builder->expr()->eq('s.enabled', ':true'),
                $builder->expr()->eq('t.hidden', ':false'),
                $builder->expr()->isNull('t.timestampDeleted'),
                $builder->expr()->isNull('t.timestampDeleted'),
                $builder->expr()->eq('partner.countryProfile', ':profile')
            )

            ->orderBy('s.title')
            ->addOrderBy('c.title')
            ->addOrderBy('t.title')

            ->setParameters([
                'true' => true,
                'false' => false,
                'profile' => $profile,
            ]);

        return $builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getResult();
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     */
    public function getById(TemplateId $id): Template
    {
        return $this->getBy([
            'id' => $id,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     */
    public function getByKey(TemplateKey $key): Template
    {
        $builder = $this->createQueryBuilder('t');

        $builder->where(
            $builder->expr()->eq('t.key', ':key'),
            $builder->expr()->isNull('t.timestampDeleted')
        )
            ->setParameters([
                'key' => $key,
            ]);

        return $builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getOneOrNullResult();
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Core\Common\Exceptions\NotImplementedException
     */
    public function getByLegacySlugs($categorySlug, $templateSlug): Template
    {
        throw new NotImplementedException();
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     */
    public function getByRemoteId(RemoteId $id, Partner $partner = null): Template
    {
        if (null !== $partner) {
            return $this->getBy([
                'remoteId' => $id,
                'partner' => $partner,
            ]);
        }

        trigger_error('Calling ' . __METHOD__ . ' without a Partner entity is deprecated', E_USER_DEPRECATED);

        return $this->getBy([
            'remoteId' => $id,
        ]);
    }

    /**
     * @param $slug $templateSlug
     *
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     *
     * @return Template
     */
    public function getBySlug(string $slug): Template
    {
        /** @var Template $template */
        $template = $this->getBy([
            'slug' => $slug,
            'timestampDeleted' => null,
        ]);

        return $template;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getBySlugs(string $categorySlug, string $templateSlug): Template
    {
        $template = $this->findOneBySlugs($categorySlug, $templateSlug);
        if (null === $template) {
            throw EntityNotFoundException::fromClassWithCriteria($this->getClassName(), [
                'categorySlug' => $categorySlug,
                'templateSlug' => $templateSlug,
            ]);
        }

        return $template;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function getReference(TemplateId $id): Template
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }

    /**
     * @param string $slug
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @return Template
     */
    public function getVisibleBySlug(string $slug): Template
    {
        $builder = $this->createQueryBuilder('t');

        $builder->where(
            $builder->expr()->eq('t.slug', ':slug'),
            $builder->expr()->eq('t.corrupted', 0),
            $builder->expr()->eq('t.published', true),
            $builder->expr()->isNull('t.timestampDeleted')
        )
            ->setParameters([
                'slug' => $slug,
            ]);

        return $builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getOneOrNullResult();
    }

    /**
     * {@inheritdoc}
     */
    public function hasWithKey(TemplateKey $key): bool
    {
        return null !== $this->findOneByKey($key);
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleFormTemplates(Profile $profile): QueryBuilder
    {
        $builder = $this->createQueryBuilder('t');

        $builder
            ->leftJoin('t.categoryLinks', 'categoryLinks')
            ->leftJoin('categoryLinks.category', 'category')
            ->leftJoin('category.shop', 'shop')
            ->where(
                $builder->expr()->eq('shop.profile', ':profile'),
                $builder->expr()->eq('t.published', ':published')
            )
            ->setParameters([
                'published' => true,
                'profile' => $profile,
            ])
            ->orderBy('t.title');

        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function getDiscountFormTemplates(): QueryBuilder
    {
        $builder = $this->createQueryBuilder('t');

        $builder
            ->leftJoin('t.discounts', 'discounts')
            ->where(
                $builder->expr()->eq('t.published', ':published')
            )
            ->setParameters(['published' => true])
            ->orderBy('t.title');

        return $builder;
    }
}
