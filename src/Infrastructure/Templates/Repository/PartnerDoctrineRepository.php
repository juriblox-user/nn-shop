<?php

namespace NnShop\Infrastructure\Templates\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use JuriBlox\Sdk\Domain\Offices\Values\OfficeId as RemoteId;
use NnShop\Domain\Templates\Entity\Partner;
use NnShop\Domain\Templates\Repository\PartnerRepositoryInterface;
use NnShop\Domain\Templates\Value\PartnerId;
use NnShop\Domain\Translation\Entity\Profile;

class PartnerDoctrineRepository extends AbstractDoctrineRepository implements PartnerRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllExceptSlug(string $slug): array
    {
        $builder = $this->createQueryBuilder('p');

        $builder->where(
            $builder->expr()->neq('p.slug', ':slug')
        );

        $builder->setParameter('slug', $slug);

        $builder->orderBy('p.slug', 'ASC');

        return $builder->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findBySyncEnabled(): array
    {
        $builder = $this->createQueryBuilder('p');

        $builder->where(
            $builder->expr()->eq('p.syncEnabled', true),
            $builder->expr()->isNull('p.timestampDeleted')
        );

        $builder->orderBy('p.syncedTimestamp', 'DESC');

        return $builder->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(PartnerId $id)
    {
        $builder = $this->createQueryBuilder('p');

        $builder->where(
            $builder->expr()->eq('p.id', ':id')
        )
            ->setParameters([
                'id' => $id,
            ]);

        return$builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getOneOrNullResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByRemoteId(RemoteId $id)
    {
        return $this->findOneBy([
            'remoteId' => $id,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function findOneBySlug($slug)
    {
        /** @var Partner $partner */
        $partner = $this->findOneBy([
            'slug' => $slug,
            'timestampDeleted' => null,
        ]);

        return $partner;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     */
    public function getById(PartnerId $id): Partner
    {
        return $this->getBy([
            'id' => $id,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     */
    public function getByRemoteId(RemoteId $id)
    {
        return $this->getBy([
            'remoteId' => $id,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     */
    public function getBySlug($slug): Partner
    {
        /** @var Partner $partner */
        $partner = $this->getBy([
            'slug' => $slug,
            'timestampDeleted' => null,
        ]);

        return $partner;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function getReference(PartnerId $id): Partner
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }

    /**
     * {@inheritdoc}
     */
    public function hasWithSlug($slug): bool
    {
        return null !== $this->findOneBySlug($slug);
    }

    /**
     * @param Profile $profile
     *
     * @return array
     */
    public function getMenuByProfile(Profile $profile): array
    {
        $builder = $this->createQueryBuilder('p');

        return $builder
            ->orderBy('p.title')
            ->where(
                $builder->expr()->eq('p.countryProfile', ':profile')
            )
            ->setParameters([
                    'profile' => $profile,
            ]
            )
            ->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getResult();
    }

    /**
     * @param int    $remoteId
     * @param string $partnerId
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @return bool
     */
    public function remoteIdIsDuplicate(int $remoteId, string $partnerId): bool
    {
        $count = $this->createQueryBuilder('p')
            ->select('count(p.id)')
            ->where('p.remoteId=:remote_id AND p.id<>:partner_id')
            ->setParameter('remote_id', $remoteId)
            ->setParameter('partner_id', $partnerId)
            ->getQuery()
            ->getSingleScalarResult();

        return $count > 0;
    }

    /**
     * @param Profile $profile
     *
     * @return array
     */
    public function getByProfile(Profile $profile): array
    {
        $builder = $this->createQueryBuilder('p');

        return $builder
            ->where(
                $builder->expr()->eq('p.countryProfile', ':profile')
            )
            ->setParameters([
                    'profile' => $profile,
                ]
            )
            ->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getResult();
    }
}
