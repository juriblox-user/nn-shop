<?php

namespace NnShop\Infrastructure\Templates\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\Templates\Entity\Category;
use NnShop\Domain\Templates\Entity\CategoryTemplate;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Templates\Repository\CategoryTemplateRepositoryInterface;

class CategoryTemplateDoctrineRepository extends AbstractDoctrineRepository implements CategoryTemplateRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findByTemplate(Template $template): array
    {
        return $this->findBy([
            'template' => $template,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getByCategoryAndTemplate(Category $category, Template $template): CategoryTemplate
    {
        /** @var CategoryTemplate $link */
        $link = $this->getBy([
            'category' => $category,
            'template' => $template,
        ]);

        return $link;
    }
}
