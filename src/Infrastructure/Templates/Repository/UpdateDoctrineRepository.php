<?php

namespace NnShop\Infrastructure\Templates\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\Templates\Entity\Update;
use NnShop\Domain\Templates\Repository\UpdateRepositoryInterface;
use NnShop\Domain\Templates\Value\UpdateId;

class UpdateDoctrineRepository extends AbstractDoctrineRepository implements UpdateRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findOneById(UpdateId $id)
    {
        /** @var Update $update */
        $update = $this->findOneBy([
            'id' => $id,
        ]);

        return $update;
    }

    /**
     * {@inheritdoc}
     */
    public function getById(UpdateId $id): Update
    {
        /** @var Update $update */
        $update = $this->getBy([
            'id' => $id,
        ]);

        return $update;
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(UpdateId $id): Update
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
