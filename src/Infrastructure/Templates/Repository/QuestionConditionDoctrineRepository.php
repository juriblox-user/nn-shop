<?php

namespace NnShop\Infrastructure\Templates\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use NnShop\Domain\Templates\Entity\QuestionCondition;
use NnShop\Domain\Templates\Repository\QuestionConditionRepositoryInterface;
use NnShop\Domain\Templates\Value\QuestionConditionId;

class QuestionConditionDoctrineRepository extends AbstractDoctrineRepository implements QuestionConditionRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findOneById(QuestionConditionId $id)
    {
        /** @var QuestionCondition $condition */
        $condition = $this->findOneBy([
            'id' => $id,
        ]);

        return $condition;
    }

    /**
     * {@inheritdoc}
     */
    public function getById(QuestionConditionId $id): QuestionCondition
    {
        /** @var QuestionCondition $condition */
        $condition = $this->getBy([
            'id' => $id,
        ]);

        return $condition;
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(QuestionConditionId $id): QuestionCondition
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
