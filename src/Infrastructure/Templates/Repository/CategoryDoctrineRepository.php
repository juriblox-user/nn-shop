<?php

namespace NnShop\Infrastructure\Templates\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use NnShop\Domain\Templates\Entity\Category;
use NnShop\Domain\Templates\Repository\CategoryRepositoryInterface;
use NnShop\Domain\Templates\Value\CategoryId;
use NnShop\Domain\Translation\Entity\Profile;

class CategoryDoctrineRepository extends AbstractDoctrineRepository implements CategoryRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllWithShop(): array
    {
        $builder = $this->createQueryBuilder('c')
            ->select('c', 's')
            ->join('c.shop', 's')

            ->orderBy('s.title')
            ->addOrderBy('c.title');

        return$builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getResult();
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(CategoryId $id)
    {
        $builder = $this->createQueryBuilder('c');

        $builder->where(
            $builder->expr()->eq('c.id', ':id')
        )
            ->setParameters([
                'id' => $id,
            ]);

        return$builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getOneOrNullResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findOneBySlug($slug)
    {
        return $this->findOneBy([
            'slug' => $slug,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     */
    public function getById(CategoryId $id): Category
    {
        return $this->getBy([
            'id' => $id,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     */
    public function getBySlug($slug): Category
    {
        return $this->getBy([
            'slug' => $slug,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function getReference(CategoryId $id): Category
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }

    /**
     * {@inheritdoc}
     */
    public function hasWithSlug($slug): bool
    {
        return null !== $this->findOneBySlug($slug);
    }

    /**
     * {@inheritdoc}
     */
    public function findAllWithShopAndProfile(Profile $profile): array
    {
        $builder = $this->createQueryBuilder('c');

        $builder
            ->select('c', 's')
            ->join('c.shop', 's')
            ->where(
                $builder->expr()->eq('s.profile', ':profile')
            )
            ->setParameters([
                'profile' => $profile,
            ])

            ->orderBy('s.title')
            ->addOrderBy('c.title');

        return$builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getResult();
    }
}
