<?php

namespace NnShop\Infrastructure\Templates\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use JuriBlox\Sdk\Domain\Documents\Values\DocumentRequestId as RemoteId;
use NnShop\Domain\Templates\Entity\DocumentRequest;
use NnShop\Domain\Templates\Repository\DocumentRequestRepositoryInterface;
use NnShop\Domain\Templates\Value\DocumentRequestId;

class DocumentRequestDoctrineRepository extends AbstractDoctrineRepository implements DocumentRequestRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findOneById(DocumentRequestId $id)
    {
        /** @var DocumentRequest $request */
        $request = $this->findOneBy([
            'id' => $id,
        ]);

        return $request;
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByRemoteId(RemoteId $id)
    {
        /** @var DocumentRequest $request */
        $request = $this->findOneBy([
            'remoteId' => $id,
        ]);

        return $request;
    }

    /**
     * {@inheritdoc}
     */
    public function getById(DocumentRequestId $id): DocumentRequest
    {
        /** @var DocumentRequest $request */
        $request = $this->getBy([
            'id' => $id,
        ]);

        return $request;
    }

    /**
     * {@inheritdoc}
     */
    public function getByRemoteId(RemoteId $id): DocumentRequest
    {
        /** @var DocumentRequest $request */
        $request = $this->getBy([
            'remoteId' => $id,
        ]);

        return $request;
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(DocumentRequestId $id): DocumentRequest
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
