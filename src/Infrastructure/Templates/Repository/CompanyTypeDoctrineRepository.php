<?php

namespace NnShop\Infrastructure\Templates\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use NnShop\Domain\Templates\Entity\CompanyType;
use NnShop\Domain\Templates\Repository\CompanyTypeRepositoryInterface;
use NnShop\Domain\Templates\Value\CompanyTypeId;
use NnShop\Domain\Translation\Entity\Profile;

class CompanyTypeDoctrineRepository extends AbstractDoctrineRepository implements CompanyTypeRepositoryInterface
{
    /**
     * @param Profile $profile
     *
     * @return array|CompanyType[]
     */
    public function findAllOrderedByTitle(Profile $profile): array
    {
        $builder = $this->createQueryBuilder('t');

        return $builder
            ->orderBy('t.title')
            ->where(
                $builder->expr()->eq('t.profile', ':profile')
            )
            ->setParameters([
                'profile' => $profile,
            ])
            ->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getResult();
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(CompanyTypeId $id)
    {
        $builder = $this->createQueryBuilder('t');

        $builder->where(
            $builder->expr()->eq('t.id', ':id')
        )
            ->setParameters([
                'id' => $id,
            ]);

        return$builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getOneOrNullResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findOneBySlug(string $slug)
    {
        return $this->findOneBy([
            'slug' => $slug,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     */
    public function getById(CompanyTypeId $id): CompanyType
    {
        return $this->getBy([
            'id' => $id,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     */
    public function getBySlug(string $slug): CompanyType
    {
        $builder = $this->createQueryBuilder('t');

        $builder->select('t, activities')
            ->join('t.activities', 'activities')
            ->where(
            $builder->expr()->eq('t.slug', ':slug')
        )
            ->setParameters([
                'slug' => $slug,
            ]);

        return$builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getOneOrNullResult();
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function getReference(CompanyTypeId $id): CompanyType
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }

    /**
     * {@inheritdoc}
     */
    public function hasWithSlug(string $slug): bool
    {
        return null !== $this->findOneBySlug($slug);
    }

    /**
     * @param Profile $profile
     *
     * @return array
     */
    public function getMenuByProfile(Profile $profile): array
    {
        $builder = $this->createQueryBuilder('t');

        return $builder
            ->orderBy('t.title')
            ->where(
                $builder->expr()->eq('t.profile', ':profile')
            )
            ->setParameters([
                'profile' => $profile,
            ])
            ->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getResult();
    }
}
