<?php

namespace NnShop\Infrastructure\Templates\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use JuriBlox\Sdk\Domain\Documents\Values\DocumentId as RemoteId;
use NnShop\Domain\Templates\Entity\Document;
use NnShop\Domain\Templates\Enumeration\DocumentStatus;
use NnShop\Domain\Templates\Repository\DocumentRepositoryInterface;
use NnShop\Domain\Templates\Value\DocumentId;
use function Doctrine\ORM\QueryBuilder;

class DocumentDoctrineRepository extends AbstractDoctrineRepository implements DocumentRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findOneById(DocumentId $id)
    {
        /** @var Document $document */
        $document = $this->findOneBy([
            'id' => $id,
        ]);

        return $document;
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByRemoteId(RemoteId $id)
    {
        /** @var Document $document */
        $document = $this->findOneBy([
            'remoteId' => $id,
        ]);

        return $document;
    }

    /**
     * @return array|Document[]
     */
    public function findPending()
    {
        return $this->findBy([
            'status' => DocumentStatus::from(DocumentStatus::PENDING),
            'timestampDeleted' => null,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getById(DocumentId $id): Document
    {
        /** @var Document $document */
        $document = $this->getBy([
            'id' => $id,
        ]);

        return $document;
    }

    /**
     * {@inheritdoc}
     */
    public function getByRemoteId(RemoteId $id): Document
    {
        /** @var Document $document */
        $document = $this->getBy([
            'remoteId' => $id,
        ]);

        return $document;
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(DocumentId $id): Document
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
