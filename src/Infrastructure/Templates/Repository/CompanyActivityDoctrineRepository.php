<?php

namespace NnShop\Infrastructure\Templates\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use NnShop\Domain\Templates\Entity\CompanyActivity;
use NnShop\Domain\Templates\Repository\CompanyActivityRepositoryInterface;
use NnShop\Domain\Templates\Value\CompanyActivityId;

class CompanyActivityDoctrineRepository extends AbstractDoctrineRepository implements CompanyActivityRepositoryInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(CompanyActivityId $id)
    {
        $builder = $this->createQueryBuilder('c');

        $builder->where(
            $builder->expr()->eq('c.id', ':id')
        )
            ->setParameters([
                'id' => $id,
            ]);

        return$builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getOneOrNullResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findOneBySlug($slug)
    {
        return $this->findOneBy([
            'slug' => $slug,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     */
    public function getById(CompanyActivityId $id): CompanyActivity
    {
        return $this->getBy([
            'id' => $id,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Core\Common\Exceptions\Domain\EntityNotFoundException
     */
    public function getBySlug($slug): CompanyActivity
    {
        return $this->getBy([
            'slug' => $slug,
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function getReference(CompanyActivityId $id): CompanyActivity
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }

    /**
     * {@inheritdoc}
     */
    public function hasWithSlug($slug): bool
    {
        return null !== $this->findOneBySlug($slug);
    }
}
