<?php

namespace NnShop\Infrastructure\Templates\Repository;

use Core\Infrastructure\AbstractDoctrineRepository;
use JuriBlox\Sdk\Domain\Documents\Values\QuestionOptionId as RemoteId;
use NnShop\Domain\Templates\Entity\Question;
use NnShop\Domain\Templates\Entity\QuestionOption;
use NnShop\Domain\Templates\Repository\QuestionOptionRepositoryInterface;
use NnShop\Domain\Templates\Value\QuestionOptionId;

class QuestionOptionDoctrineRepository extends AbstractDoctrineRepository implements QuestionOptionRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findOneById(QuestionOptionId $id)
    {
        /** @var QuestionOption $option */
        $option = $this->findOneBy([
            'id' => $id,
        ]);

        return $option;
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByLookupId(RemoteId $id, Question $question = null)
    {
        /** @var QuestionOption|null $option */
        $option = null;

        if (null !== $question) {
            $option = $this->findOneBy([
                'lookupId' => $id,
                'question' => $question,
            ]);
        } else {
            trigger_error('Calling ' . __METHOD__ . ' without a Question entity is deprecated', E_USER_DEPRECATED);

            $option = $this->findOneBy([
                'lookupId' => $id,
            ]);
        }

        return $option;
    }

    /**
     * {@inheritdoc}
     */
    public function getById(QuestionOptionId $id): QuestionOption
    {
        /** @var QuestionOption $option */
        $option = $this->getBy([
            'id' => $id,
        ]);

        return $option;
    }

    /**
     * {@inheritdoc}
     */
    public function getByLookupId(RemoteId $lookupId): QuestionOption
    {
        /** @var QuestionOption $option */
        $option = $this->getBy([
            'lookupId' => $lookupId,
        ]);

        return $option;
    }

    /**
     * {@inheritdoc}
     */
    public function getReference(QuestionOptionId $id): QuestionOption
    {
        return $this->getEntityManager()->getReference($this->getClassName(), $id);
    }
}
