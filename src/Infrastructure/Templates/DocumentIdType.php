<?php

namespace NnShop\Infrastructure\Templates;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Templates\Value\DocumentId;

class DocumentIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'templates.document_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return DocumentId::class;
    }
}
