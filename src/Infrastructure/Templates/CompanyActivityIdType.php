<?php

namespace NnShop\Infrastructure\Templates;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Templates\Value\CompanyActivityId;

class CompanyActivityIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'templates.company_activity_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return CompanyActivityId::class;
    }
}
