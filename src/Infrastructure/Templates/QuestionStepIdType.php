<?php

namespace NnShop\Infrastructure\Templates;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Templates\Value\QuestionStepId;

class QuestionStepIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'templates.question_step_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return QuestionStepId::class;
    }
}
