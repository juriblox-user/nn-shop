<?php

namespace NnShop\Infrastructure\Templates;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Templates\Value\CompanyTypeId;

class CompanyTypeIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'templates.company_type_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return CompanyTypeId::class;
    }
}
