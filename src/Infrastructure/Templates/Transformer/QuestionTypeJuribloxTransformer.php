<?php

namespace NnShop\Infrastructure\Templates\Transformer;

use Core\Common\Exception\TransformerConversionException;
use Core\Common\Validation\Assertion;
use JuriBlox\Sdk\Domain\Documents\Values\QuestionType as RemoteType;
use NnShop\Domain\Templates\Enumeration\QuestionType as LocalType;
use NnShop\Domain\Templates\Transformer\QuestionTypeTransformerInterface;

class QuestionTypeJuribloxTransformer implements QuestionTypeTransformerInterface
{
    /**
     * Convert a remote value to a QuestionType instance.
     *
     * @param RemoteType $value
     *
     * @return LocalType
     */
    public static function fromRemote($value): LocalType
    {
        Assertion::isInstanceOf($value, RemoteType::class);

        /*
         * RemoteType -> LocalType mapping
         */
        $conversions = [
            RemoteType::TYPE_CHECKBOX => LocalType::TYPE_MULTIPLE_BOOLEANS,
            RemoteType::TYPE_COC => LocalType::TYPE_COC,
            RemoteType::TYPE_DATE => LocalType::TYPE_DATE,
            RemoteType::TYPE_LONG_TEXT => LocalType::TYPE_MULTILINE_TEXT,
            RemoteType::TYPE_NUMERIC => LocalType::TYPE_NUMERIC,
            RemoteType::TYPE_PRICE => LocalType::TYPE_PRICE,
            RemoteType::TYPE_RADIO => LocalType::TYPE_CHOICE,
            RemoteType::TYPE_SELECTBOX => LocalType::TYPE_DROPDOWN,
            RemoteType::TYPE_SHORT_TEXT => LocalType::TYPE_SINGLE_LINE_TEXT,
            RemoteType::TYPE_STATEMENT => LocalType::TYPE_STATEMENT,
            RemoteType::TYPE_YES_NO => LocalType::TYPE_BOOLEAN,
        ];

        if (!isset($conversions[$value->getType()])) {
            throw TransformerConversionException::fromRemote(RemoteType::class, LocalType::class, $value);
        }

        return new LocalType($conversions[$value->getType()]);
    }

    /**
     * Convert a QuestionType instance to a remote value.
     *
     * @param LocalType $type
     *
     * @return mixed
     */
    public static function toRemote(LocalType $type)
    {
        /*
         * LocalType -> RemoteType mapping
         */
        $conversions = [
            LocalType::TYPE_BOOLEAN => RemoteType::TYPE_YES_NO,
            LocalType::TYPE_CHOICE => RemoteType::TYPE_RADIO,
            LocalType::TYPE_COC => RemoteType::TYPE_COC,
            LocalType::TYPE_DATE => RemoteType::TYPE_DATE,
            LocalType::TYPE_DROPDOWN => RemoteType::TYPE_SELECTBOX,
            LocalType::TYPE_MULTILINE_TEXT => RemoteType::TYPE_LONG_TEXT,
            LocalType::TYPE_MULTIPLE_BOOLEANS => RemoteType::TYPE_CHECKBOX,
            LocalType::TYPE_NUMERIC => RemoteType::TYPE_NUMERIC,
            LocalType::TYPE_PRICE => RemoteType::TYPE_PRICE,
            LocalType::TYPE_SINGLE_LINE_TEXT => RemoteType::TYPE_SHORT_TEXT,
            LocalType::TYPE_STATEMENT => RemoteType::TYPE_SHORT_TEXT,
        ];

        if (!isset($conversions[$type->getValue()])) {
            throw TransformerConversionException::fromLocal(LocalType::class, RemoteType::class, $type->getValue());
        }

        return new RemoteType($conversions[$type->getValue()]);
    }
}
