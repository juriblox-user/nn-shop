<?php

namespace NnShop\Infrastructure\Templates;

use Core\Doctrine\DBAL\Types\AbstractUuidType;
use NnShop\Domain\Templates\Value\DocumentRequestId;

class DocumentRequestIdType extends AbstractUuidType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'templates.document_request_id';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return DocumentRequestId::class;
    }
}
