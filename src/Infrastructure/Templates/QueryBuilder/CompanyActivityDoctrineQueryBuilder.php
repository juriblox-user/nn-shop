<?php

namespace NnShop\Infrastructure\Templates\QueryBuilder;

use Core\Doctrine\ORM\AbstractQueryBuilder;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use NnShop\Domain\Templates\Entity\CompanyActivity;
use NnShop\Domain\Templates\Entity\CompanyType;
use NnShop\Domain\Templates\QueryBuilder\CompanyActivityQueryBuilderInterface;

class CompanyActivityDoctrineQueryBuilder extends AbstractQueryBuilder implements CompanyActivityQueryBuilderInterface
{
    /**
     * @var bool
     */
    private $filterType;

    /**
     * @var array|CompanyType[]
     */
    private $filterTypes;

    /**
     * @return Query
     */
    public function build(): Query
    {
        $builder = $this->getBuilder()
            ->select(['a', 't'])
            ->from(CompanyActivity::class, 'a')
            ->join('a.type', 't');

        // Filteren op CompanyType
        if ($this->filterType) {
            $builder->andWhere(
                $builder->expr()->in('t.id', ':types')
            )->setParameter('types', $this->filterTypes);
        }

        $builder->orderBy('a.title');

        return$builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class);
    }

    /**
     * {@inheritdoc}
     */
    public function reset(): CompanyActivityQueryBuilderInterface
    {
        $this->filterType = false;
        $this->filterTypes = [];

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterType(): CompanyActivityQueryBuilderInterface
    {
        $this->filterType = true;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     */
    public function includeType(CompanyType $type): CompanyActivityQueryBuilderInterface
    {
        if (false === $this->filterType) {
            throw new \LogicException('CompanyActivityDoctrineQueryBuilder::$filterType has to be enabled');
        }

        $this->filterTypes[] = $type->getId();

        return $this;
    }
}
