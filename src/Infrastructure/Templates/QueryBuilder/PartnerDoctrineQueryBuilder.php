<?php

namespace NnShop\Infrastructure\Templates\QueryBuilder;

use Core\Doctrine\ORM\AbstractQueryBuilder;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use NnShop\Domain\Templates\Entity\Partner;
use NnShop\Domain\Templates\QueryBuilder\PartnerQueryBuilderInterface;
use NnShop\Domain\Translation\Entity\Profile;

class PartnerDoctrineQueryBuilder extends AbstractQueryBuilder implements PartnerQueryBuilderInterface
{
    /**
     * @var bool
     */
    private $excludeWithoutLogo;

    /**
     * @var bool
     */
    private $excludeWithSlug;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var Profile|null
     */
    private $profile;

    /**
     * {@inheritdoc}
     */
    public function build(): Query
    {
        $builder = $this->getBuilder();

        $builder
            ->select(['p'])
            ->from(Partner::class, 'p')
            ->where(
                $builder->expr()->isNull('p.timestampDeleted')
            )
            ->orderBy('p.title');

        if ($this->excludeWithoutLogo) {
            $builder->andWhere(
                $builder->expr()->isNotNull('p.logo')
            );
        }

        if ($this->excludeWithSlug) {
            $builder->andWhere(
                $builder->expr()->neq('p.slug', ':slug')
            )
                ->setParameter('slug', $this->slug);
        }

        if ($this->profile) {
            $builder->andWhere(
                $builder->expr()->eq('p.countryProfile', ':profile')
            )
                ->setParameter('profile', $this->profile);
        }

        return $builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class);
    }

    /**
     * @return PartnerQueryBuilderInterface
     */
    public function excludeWithoutLogo(): PartnerQueryBuilderInterface
    {
        $this->excludeWithoutLogo = true;

        return $this;
    }

    public function excludeWithSlug(string $slug): PartnerQueryBuilderInterface
    {
        $this->slug = $slug;
        $this->excludeWithSlug = true;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterProfile(Profile $profile): PartnerQueryBuilderInterface
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function reset(): PartnerQueryBuilderInterface
    {
        $this->profile = null;
        $this->excludeWithoutLogo = false;
        $this->excludeWithSlug = false;

        return $this;
    }
}
