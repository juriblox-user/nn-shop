<?php

namespace NnShop\Infrastructure\Templates\QueryBuilder;

use Core\Doctrine\ORM\AbstractQueryBuilder;
use NnShop\Domain\Templates\Entity\Category;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use NnShop\Domain\Shops\Value\ShopId;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Templates\QueryBuilder\TemplateQueryBuilderInterface;
use NnShop\Domain\Templates\Value\PartnerId;
use NnShop\Domain\Translation\Value\ProfileId;

class TemplateDoctrineQueryBuilder extends AbstractQueryBuilder implements TemplateQueryBuilderInterface
{
    /**
     * @var bool
     */
    private $excludeCorrupted;

    /**
     * @var bool
     */
    private $excludeUnpublished;

    /**
     * @var bool
     */
    private $excludeHidden;

    /**
     * @var bool
     */
    private $filterCorrupted;

    /**
     * @var bool
     */
    private $filterHidden;

    /**
     * @var bool
     */
    private $filterPublished;

    /**
     * @var Category
     */
    private $category;

    /**
     * @var ShopId
     */
    private $shopId;

    /**
     * @var string|null
     */
    private $matchKeywords;

    /**
     * @var PartnerId|null
     */
    private $partnerId;

    /**
     * @var ProfileId|null
     */
    private $profileId;

    /**
     * {@inheritdoc}
     */
    public function build(): Query
    {
        $builder = $this->getBuilder()
            ->select(['t', 'p', 'cl', 'c'])
            ->from(Template::class, 't')

            ->join('t.partner', 'p')

            ->leftJoin('t.categoryLinks', 'cl')
            ->leftJoin('cl.category', 'c');

        /*
         * Corrupte templates
         */
        if ($this->filterCorrupted) {
            if ($this->excludeCorrupted) {
                $builder->andWhere(
                    $builder->expr()->eq('t.corrupted', ':false')
                )->setParameter('false', false);
            } else {
                $builder->andWhere(
                    $builder->expr()->eq('t.corrupted', ':true')
                )->setParameter('true', true);
            }
        }

        /*
         * Verborgen templates
         */
        if ($this->filterHidden) {
            if ($this->excludeHidden) {
                $builder->andWhere(
                    $builder->expr()->eq('t.hidden', ':false')
                )->setParameter('false', false);
            } else {
                $builder->andWhere(
                    $builder->expr()->eq('t.hidden', ':true')
                )->setParameter('true', true);
            }
        }

        /*
         * Gepubliceerde templates
         */
        if ($this->filterPublished) {
            if ($this->excludeUnpublished) {
                $builder->andWhere(
                    $builder->expr()->eq('t.published', ':true')
                )->setParameter('true', true);
            } else {
                $builder->andWhere(
                    $builder->expr()->eq('t.published', ':false')
                )->setParameter('false', false);
            }
        }

        /*
         * Beperken op shop
         */
        if (null !== $this->shopId) {
            $builder->join('t.categoryLinks', 'tcl')
                ->join('tcl.category', 'tc')

                ->andWhere(
                    $builder->expr()->eq('sc.shop', ':shopId')
                )->setParameter('shopId', $this->shopId);
        }

        /*
         * Beperken op category
         */
        if (null !== $this->category) {
            $builder->join('t.categoryLinks', 'tcl')

            ->andWhere(
                $builder->expr()->eq('tcl.category', ':category')
            )->setParameter('category', $this->category);
        }

        /*
         * Zoeken op sleutelwoorden
         */
        if (null !== $this->matchKeywords) {
            $keywords = explode(' ', $this->matchKeywords);
            for ($i = 0, $_i = count($keywords); $i < $_i; ++$i) {
                $keywords[$i] = preg_replace('/[^\p{L}\p{N}_]+/u', '', $keywords[$i]);
                $keywords[$i] = '+' . $keywords[$i] . '*';
            }

            /* @noinspection PhpParamsInspection */
            $builder->andWhere('MATCH_AGAINST (t.title, t.description, t.remoteTitle, t.remoteDescription, :keywords \'IN BOOLEAN MODE\') > 0')
                ->setParameter('keywords', implode(' ', $keywords));
        }

        if ($this->partnerId) {
            $builder->andWhere(
                $builder->expr()->eq('t.partner', ':partner')
            )
                ->setParameter('partner', $this->partnerId);
        }

        if ($this->profileId) {
            $builder->andWhere(
                $builder->expr()->eq('p.countryProfile', ':profile')
            )
                ->setParameter('profile', $this->profileId);
        }

        $builder
            ->andWhere(
                $builder->expr()->isNull('t.timestampDeleted')
            )
            ->orderBy('t.title');

        return $builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     */
    public function excludeCorrupted()
    {
        if (false === $this->filterCorrupted) {
            throw new \LogicException('TemplateDoctrineQueryBuilder::$filterCorrupted has to be enabled');
        }

        $this->excludeCorrupted = true;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     */
    public function excludeHidden()
    {
        if (false === $this->filterHidden) {
            throw new \LogicException('TemplateDoctrineQueryBuilder::$filterHidden has to be enabled');
        }

        $this->excludeHidden = true;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     */
    public function excludeUnpublished()
    {
        if (false === $this->filterPublished) {
            throw new \LogicException('TemplateDoctrineQueryBuilder::$filterPublished has to be enabled');
        }

        $this->excludeUnpublished = true;

        return $this;
    }

    /**
     * @throws \LogicException
     *
     * @return $this
     */
    public function filterCorrupted()
    {
        $this->filterCorrupted = true;
        $this->excludeCorrupted();

        return $this;
    }

    /**
     * @throws \LogicException
     *
     * @return $this
     */
    public function filterHidden()
    {
        $this->filterHidden = true;
        $this->excludeHidden();

        return $this;
    }

    /**
     * @throws \LogicException
     *
     * @return $this
     */
    public function filterPublished()
    {
        $this->filterPublished = true;
        $this->excludeUnpublished();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterShop(ShopId $shopId)
    {
        $this->shopId = $shopId;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     */
    public function includeCorrupted()
    {
        if (false === $this->filterCorrupted) {
            throw new \LogicException('TemplateDoctrineQueryBuilder::$filterCorrupted has to be enabled');
        }

        $this->excludeCorrupted = false;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     */
    public function includeUnpublished()
    {
        if (false === $this->filterPublished) {
            throw new \LogicException('TemplateDoctrineQueryBuilder::$filterPublished has to be enabled');
        }

        $this->excludeUnpublished = false;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterPartner(PartnerId $partnerId): TemplateQueryBuilderInterface
    {
        $this->partnerId = $partnerId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterProfile(ProfileId $profileId): TemplateQueryBuilderInterface
    {
        $this->profileId = $profileId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterCategory(Category $category)
    {
       $this->category = $category;

       return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function reset(): TemplateQueryBuilderInterface
    {
        $this->filterCorrupted = false;
        $this->filterPublished = false;
        $this->filterHidden = false;

        $this->shopId = null;
        $this->partnerId = null;
        $this->profileId = null;

        return $this;
    }

    /**
     * @param string $keywords
     */
    public function matchKeywords(string $keywords)
    {
        $this->matchKeywords = trim($keywords);
    }
}
