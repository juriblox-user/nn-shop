<?php

namespace NnShop\Infrastructure\Templates\QueryBuilder;

use Core\Doctrine\ORM\AbstractQueryBuilder;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use NnShop\Domain\Templates\Entity\CompanyType;
use NnShop\Domain\Templates\QueryBuilder\CompanyTypeQueryBuilderInterface;
use NnShop\Domain\Translation\Value\ProfileId;

class CompanyTypeDoctrineQueryBuilder extends AbstractQueryBuilder implements CompanyTypeQueryBuilderInterface
{
    /**
     * @var ProfileId|null
     */
    private $profileId;

    /**
     * @return Query
     */
    public function build(): Query
    {
        $builder = $this->getBuilder()
            ->select(['t'])
            ->from(CompanyType::class, 't')
            ->orderBy('t.title');

        if ($this->profileId) {
            $builder->andWhere(
                $builder->expr()->eq('t.profile', ':profile')
            )
                ->setParameter('profile', $this->profileId);
        }

        return$builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class);
    }

    /**
     * {@inheritdoc}
     */
    public function reset(): CompanyTypeQueryBuilderInterface
    {
        $this->profileId = null;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterProfile(ProfileId $profileId): CompanyTypeQueryBuilderInterface
    {
        $this->profileId = $profileId;

        return $this;
    }
}
