<?php

namespace NnShop\Infrastructure\Templates\QueryBuilder;

use Core\Doctrine\ORM\AbstractQueryBuilder;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use NnShop\Domain\Shops\Value\ShopId;
use NnShop\Domain\Templates\Entity\Category;
use NnShop\Domain\Templates\QueryBuilder\CategoryQueryBuilderInterface;

class CategoryDoctrineQueryBuilder extends AbstractQueryBuilder implements CategoryQueryBuilderInterface
{
    /**
     * @var ShopId|null
     */
    private $shopId;

    /**
     * @return Query
     */
    public function build(): Query
    {
        $builder = $this->getBuilder()
            ->select(['c'])
            ->from(Category::class, 'c')
            ->orderBy('c.title');

        if ($this->shopId) {
            $builder->andWhere(
                $builder->expr()->eq('c.shop', ':shop')
            )
                ->setParameter('shop', $this->shopId);
        }

        return$builder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class);
    }

    /**
     * {@inheritdoc}
     */
    public function reset(): CategoryQueryBuilderInterface
    {
        $this->shopId = null;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filterShop(ShopId $shopId): CategoryQueryBuilderInterface
    {
        $this->shopId = $shopId;

        return $this;
    }
}
