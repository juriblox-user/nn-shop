<?php

namespace NnShop\Infrastructure\Templates;

use Core\Doctrine\DBAL\Types\AbstractIntegerType;
use NnShop\Domain\Templates\Value\TemplateVersion;

class TemplateVersionType extends AbstractIntegerType
{
    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'templates.template_version';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return TemplateVersion::class;
    }
}
