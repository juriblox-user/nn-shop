<?php

namespace NnShop\Infrastructure\Templates;

use Core\Doctrine\DBAL\Types\AbstractStringType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use NnShop\Domain\Templates\Value\TemplateKey;

class TemplateKeyType extends AbstractStringType
{
    /**
     * @var int
     */
    const LENGTH = 6;

    /**
     * {@inheritdoc}
     */
    public static function getTypeName(): string
    {
        return 'templates.template_key';
    }

    /**
     * {@inheritdoc}
     */
    public static function getValueClass(): string
    {
        return TemplateKey::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultLength(AbstractPlatform $platform)
    {
        return self::LENGTH;
    }
}
