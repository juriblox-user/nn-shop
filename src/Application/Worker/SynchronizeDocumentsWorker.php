<?php

namespace NnShop\Application\Worker;

use Core\Component\Supervisor\AbstractWorker;
use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use NnShop\Domain\Templates\Command\Document\SynchronizePendingDocumentsCommand;

class SynchronizeDocumentsWorker extends AbstractWorker implements CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $this->commandBus->handle(SynchronizePendingDocumentsCommand::create());
    }
}
