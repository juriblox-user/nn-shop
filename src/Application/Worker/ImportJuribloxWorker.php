<?php

namespace NnShop\Application\Worker;

use Core\Component\Supervisor\AbstractWorker;
use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use NnShop\Application\Traits\JuribloxApiAwareTrait;
use NnShop\Domain\Templates\Command\Partner\SynchronizePartnerCommand;
use NnShop\Domain\Templates\Entity\Partner;
use NnShop\Domain\Templates\Repository\PartnerRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;

class ImportJuribloxWorker extends AbstractWorker implements CommandBusAwareInterface
{
    use CommandBusAwareTrait;
    use JuribloxApiAwareTrait;

    /**
     * @var PartnerRepositoryInterface
     */
    private $partnerRepository;

    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * ImportTemplatesWorker constructor.
     *
     * @param PartnerRepositoryInterface  $partnerRepository
     * @param TemplateRepositoryInterface $templateRepository
     */
    public function __construct(PartnerRepositoryInterface $partnerRepository, TemplateRepositoryInterface $templateRepository)
    {
        $this->partnerRepository = $partnerRepository;
        $this->templateRepository = $templateRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        // Geheugenlimiet ophogen
        ini_set('memory_limit', '1536M');

        foreach ($this->partnerRepository->findBySyncEnabled() as $partner) {
            $this->syncPartner($partner);
        }
    }

    /**
     * @param Partner $partner
     */
    private function syncPartner(Partner $partner)
    {
        $this->logger->debug('Starting sync for partner {title}', [
            'id' => $partner->getId()->getString(),
            'title' => $partner->getTitle(),
        ]);

        $this->commandBus->handle(SynchronizePartnerCommand::fromLocalId($partner->getId()));
    }
}
