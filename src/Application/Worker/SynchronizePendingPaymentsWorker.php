<?php

namespace NnShop\Application\Worker;

use Core\Component\Supervisor\AbstractWorker;
use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use NnShop\Domain\Orders\Command\Payment\SynchronizePendingPaymentsCommand;

class SynchronizePendingPaymentsWorker extends AbstractWorker implements CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $this->commandBus->handle(SynchronizePendingPaymentsCommand::create());
    }
}
