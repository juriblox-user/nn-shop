<?php

namespace NnShop\Application\Worker;

use Core\Component\Supervisor\AbstractWorker;
use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use NnShop\Domain\Orders\Command\Order\SendReferralMailCommand;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;

class SendReferralMailForDeliveredOrdersWorker extends AbstractWorker implements CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var int
     */
    private $delayInMinutes;

    /**
     * Constructor.
     *
     * @param $orderRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository, int $delayInMinutes)
    {
        $this->orderRepository = $orderRepository;
        $this->delayInMinutes  = $delayInMinutes;
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $orders = $this->orderRepository->findDeliveredWithoutSentReferralMail($this->delayInMinutes);

        foreach ($orders as $order) {
            $this->commandBus->handle(SendReferralMailCommand::create($order->getId()));
        }
    }
}
