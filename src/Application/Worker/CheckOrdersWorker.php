<?php

namespace NnShop\Application\Worker;

use Carbon\CarbonInterval;
use Core\Component\Supervisor\AbstractWorker;
use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use NnShop\Domain\Orders\Command\Order\AbandonOrderCommand;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;

class CheckOrdersWorker extends AbstractWorker implements CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * Constructor.
     *
     * @param $orderRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $abandonedOrders = $this->orderRepository->findAbandoned(CarbonInterval::hours(2));

        foreach ($abandonedOrders as $order) {
            $this->commandBus->handle(AbandonOrderCommand::create($order->getId()));
        }
    }
}
