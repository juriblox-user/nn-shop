<?php

namespace NnShop\Application\Listeners\Templates\Template;

use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Domain\Orders\Event\Order\OrderCompletedEvent;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;

class UpdateTemplateStatisticsAfterOrderListener implements AsynchronousEventListenerInterface
{
    const CUTOFF_SECONDS = 30 * 60;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * UpdateTemplateStatisticsAfterOrderListener constructor.
     *
     * @param OrderRepositoryInterface    $orderRepository
     * @param TemplateRepositoryInterface $templateRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository, TemplateRepositoryInterface $templateRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->templateRepository = $templateRepository;
    }

    /**
     * @param OrderCompletedEvent $event
     */
    public function __invoke(OrderCompletedEvent $event)
    {
        $order = $this->orderRepository->getById($event->getId());

        $template = $order->getTemplate();

        $totalSamples = 0;
        $totalSeconds = 0;

        foreach ($this->templateRepository->findRecentOrders($template, 20) as $recent) {
            if (null === $recent->getTimestampStarted()) {
                continue;
            }

            $duration = $recent->getTimestampStarted()->diffInSeconds($recent->getTimestampAnswered());
            if ($duration >= self::CUTOFF_SECONDS) {
                continue;
            }

            $totalSeconds += $duration;
            ++$totalSamples;
        }

        if ($totalSamples > 0) {
            $template->setOrderSeconds(round($totalSeconds / $totalSamples));
        }

        $template->increaseOrderCount();

        $this->templateRepository->save($template);
    }
}
