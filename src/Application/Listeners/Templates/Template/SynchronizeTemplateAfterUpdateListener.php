<?php

namespace NnShop\Application\Listeners\Templates\Template;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Domain\JuriBlox\Event\Template\TemplatePublishedEvent;
use NnShop\Domain\Templates\Command\Template\SynchronizeTemplateCommand;
use NnShop\Domain\Templates\Repository\PartnerRepositoryInterface;

class SynchronizeTemplateAfterUpdateListener implements AsynchronousEventListenerInterface, CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @var PartnerRepositoryInterface
     */
    private $partnerRepository;

    /**
     * @param PartnerRepositoryInterface $partnerRepository
     */
    public function __construct(PartnerRepositoryInterface $partnerRepository)
    {
        $this->partnerRepository = $partnerRepository;
    }

    /**
     * @param TemplatePublishedEvent $event
     */
    public function __invoke(TemplatePublishedEvent $event)
    {
        $partner = $this->partnerRepository->findOneByRemoteId($event->getOffceId());
        if (null === $partner) {
            return;
        }

        $this->commandBus->handle(SynchronizeTemplateCommand::fromRemoteId($event->getTemplateId(), $partner->getId()));
    }
}
