<?php

namespace NnShop\Application\Listeners\Templates\Document;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousEventListenerInterface;
use NnShop\Domain\Templates\Command\Document\IncreaseDownloadCounterCommand;
use NnShop\Domain\Templates\Event\Document\DocumentDownloadedEvent;

class IncreaseCounterAfterDownloadListener implements SynchronousEventListenerInterface, CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @param DocumentDownloadedEvent $event
     */
    public function __invoke(DocumentDownloadedEvent $event)
    {
        $this->commandBus->handle(IncreaseDownloadCounterCommand::create($event->getId()));
    }
}
