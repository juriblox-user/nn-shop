<?php

namespace NnShop\Application\Listeners\Templates\Document;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Domain\Templates\Command\Document\SynchronizeDocumentCommand;
use NnShop\Domain\Templates\Event\DocumentGenerationSucceededEvent;

class SynchronizeAfterGenerationListener implements AsynchronousEventListenerInterface, CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @param DocumentGenerationSucceededEvent $event
     */
    public function __invoke(DocumentGenerationSucceededEvent $event)
    {
        $this->commandBus->handle(
            SynchronizeDocumentCommand::fromLocal($event->getId()));
    }
}
