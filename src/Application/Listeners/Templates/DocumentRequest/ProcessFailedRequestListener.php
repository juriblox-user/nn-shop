<?php

namespace NnShop\Application\Listeners\Templates\DocumentRequest;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Domain\JuriBlox\Event\DocumentRequest\DocumentGenerationFailedEvent;
use NnShop\Domain\Templates\Command\DocumentRequest\ProcessFailedRequestCommand;
use NnShop\Domain\Templates\Repository\DocumentRequestRepositoryInterface;

class ProcessFailedRequestListener implements AsynchronousEventListenerInterface, CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @var DocumentRequestRepositoryInterface
     */
    private $requestRepository;

    /**
     * UpdateDocumentRequestOnFailureListener constructor.
     *
     * @param DocumentRequestRepositoryInterface $requestRepository
     */
    public function __construct(DocumentRequestRepositoryInterface $requestRepository)
    {
        $this->requestRepository = $requestRepository;
    }

    /**
     * @param DocumentGenerationFailedEvent $event
     */
    public function __invoke(DocumentGenerationFailedEvent $event)
    {
        if ($event->isLocal()) {
            $request = $this->requestRepository->findOneById($event->getLocalId());
        } elseif ($event->isRemote()) {
            $request = $this->requestRepository->findOneByRemoteId($event->getRemoteId());
        } else {
            throw new \LogicException('DocumentGenerationFailedEvent was triggered without a local and without a remote ID');
        }

        if (null === $request) {
            return;
        }

        $this->commandBus->handle(
            ProcessFailedRequestCommand::create($request->getId())
        );
    }
}
