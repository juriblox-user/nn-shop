<?php

namespace NnShop\Application\Listeners\Templates\DocumentRequest;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Domain\JuriBlox\Event\DocumentRequest\DocumentGenerationSucceededEvent;
use NnShop\Domain\Templates\Command\DocumentRequest\ProcessSuccessfulRequestCommand;
use NnShop\Domain\Templates\Repository\DocumentRequestRepositoryInterface;

class ProcessSuccessfulRequestListener implements AsynchronousEventListenerInterface, CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @var DocumentRequestRepositoryInterface
     */
    private $requestRepository;

    /**
     * UpdateDocumentStatusAfterSuccessfulGenerationListener constructor.
     *
     * @param DocumentRequestRepositoryInterface $requestRepository
     */
    public function __construct(DocumentRequestRepositoryInterface $requestRepository)
    {
        $this->requestRepository = $requestRepository;
    }

    /**
     * @param DocumentGenerationSucceededEvent $event
     */
    public function __invoke(DocumentGenerationSucceededEvent $event)
    {
        $request = $this->requestRepository->findOneByRemoteId($event->getRequestId());
        if (null === $request) {
            return;
        }

        $this->commandBus->handle(
            ProcessSuccessfulRequestCommand::create($request->getId(), $event->getDocumentId())
        );
    }
}
