<?php

namespace NnShop\Application\Listeners\System\Counter;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousEventListenerInterface;
use NnShop\Domain\System\Command\Counter\IncreaseCounterCommand;
use NnShop\Domain\System\Enumeration\CounterName;
use NnShop\Domain\Templates\Event\DocumentGenerationSucceededEvent;

class IncreaseGeneratedDocumentsListener implements SynchronousEventListenerInterface, CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @param DocumentGenerationSucceededEvent $event
     */
    public function __invoke(DocumentGenerationSucceededEvent $event)
    {
        $this->commandBus->handle(
            IncreaseCounterCommand::fromName(CounterName::from(CounterName::GENERATED_DOCUMENTS))
        );
    }
}
