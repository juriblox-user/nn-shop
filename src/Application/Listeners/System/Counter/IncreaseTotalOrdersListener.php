<?php

namespace NnShop\Application\Listeners\System\Counter;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousEventListenerInterface;
use NnShop\Domain\Orders\Event\Order\OrderCreatedEvent;
use NnShop\Domain\System\Command\Counter\IncreaseCounterCommand;
use NnShop\Domain\System\Enumeration\CounterName;

class IncreaseTotalOrdersListener implements SynchronousEventListenerInterface, CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @param OrderCreatedEvent $event
     */
    public function __invoke(OrderCreatedEvent $event)
    {
        $this->commandBus->handle(
            IncreaseCounterCommand::fromName(CounterName::from(CounterName::TOTAL_ORDERS))
        );
    }
}
