<?php

namespace NnShop\Application\Listeners\Orders\Customer;

use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Domain\Orders\Event\InvoiceIssuedEvent;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;
use NnShop\Domain\Orders\Repository\InvoiceRepositoryInterface;

class IncreaseCustomerWorthListener implements AsynchronousEventListenerInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * IncreaseCustomerWorthListener constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     * @param InvoiceRepositoryInterface  $invoiceRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository, InvoiceRepositoryInterface $invoiceRepository)
    {
        $this->customerRepository = $customerRepository;
        $this->invoiceRepository = $invoiceRepository;
    }

    /**
     * @param InvoiceIssuedEvent $event
     */
    public function __invoke(InvoiceIssuedEvent $event)
    {
        $invoice = $this->invoiceRepository->getById($event->getId());

        $customer = $invoice->getCustomer();
        $customer->addWorth($invoice->getSubtotal());

        $this->customerRepository->save($customer);
    }
}
