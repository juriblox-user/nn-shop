<?php

namespace NnShop\Application\Listeners\Orders\Order;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Domain\Orders\Command\Order\RequeueOrderCommand;
use NnShop\Domain\Templates\Event\DocumentGenerationFailedEvent;
use NnShop\Domain\Templates\Repository\DocumentRepositoryInterface;

class RequeueOrderListener implements AsynchronousEventListenerInterface, CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @var DocumentRepositoryInterface
     */
    private $documentRepository;

    /**
     * RetryOrderRequestListener constructor.
     *
     * @param DocumentRepositoryInterface $documentRepository
     */
    public function __construct(DocumentRepositoryInterface $documentRepository)
    {
        $this->documentRepository = $documentRepository;
    }

    /**
     * @param DocumentGenerationFailedEvent $event
     *
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function __invoke(DocumentGenerationFailedEvent $event)
    {
        $document = $this->documentRepository->getById($event->getId());

        $this->commandBus->handle(RequeueOrderCommand::create($document->getOrder()->getId()));
    }
}
