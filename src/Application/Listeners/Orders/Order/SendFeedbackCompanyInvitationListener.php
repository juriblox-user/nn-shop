<?php

namespace NnShop\Application\Listeners\Orders\Order;

use Core\Application\AbstractKernel;
use Core\Application\CoreBundle\Service\DependencyInjection\ApplicationParameters;
use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Domain\Orders\Event\Order\OrderCompletedEvent;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

class SendFeedbackCompanyInvitationListener implements AsynchronousEventListenerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var ApplicationParameters
     */
    private $parameters;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(OrderRepositoryInterface $orderRepository, ApplicationParameters $parameters, LoggerInterface $logger)
    {
        $this->orderRepository = $orderRepository;
        $this->parameters = $parameters;
        $this->logger = $logger;
    }

    public function __invoke(OrderCompletedEvent $event)
    {

        if (AbstractKernel::ENVIRONMENT_PRODUCTION !== $this->parameters->getEnvironment()) {
            return;
        }

        $order = $this->orderRepository->getById($event->getId());

        $client = new Client();

        try {
            $tokenResponse = $client->request('GET', 'https://www.feedbackcompany.com/api/v2/oauth2/token', [
                'query' => [
                    'client_id' => getenv('FEEDBACK_COMPANY_ID'),
                    'client_secret' => getenv('FEEDBACK_COMPANY_SECRET'),
                    'grant_type' => 'authorization_code',
                ]
            ]);
        } catch (\Exception $exception) {
            $this->logger->warning($exception->getMessage());
            return null;
        }

        $tokenResult = json_decode($tokenResponse->getBody()->getContents());

        $token = $tokenResult->access_token;

        if (!$token) {
            return null;
        }

        try {
            $client->request('POST','https://www.feedbackcompany.com/api/v2/orders', [
                'headers' => [
                    'Authorization' => 'Bearer '.$token,
                ],
                'json' => [
                    'external_id' => $order->getId()->getString(),
                    'customer' => [
                        'email' => $order->getCustomer()->getEmail()->getString(),
                        'fullname' => $order->getCustomer()->getName(),
                    ],
                    'invitation' => [
                        'delay' => [
                            'unit' => 'days',
                            'amount' => 1,
                        ],
                        'reminder' => [
                            'unit' => 'days',
                            'amount' => 4,
                        ]
                    ]
                ]
            ]);
        } catch (\Exception $exception) {
            $this->logger->warning($exception->getMessage());
            return null;
        }
    }
}
