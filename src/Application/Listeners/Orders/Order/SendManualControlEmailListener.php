<?php

namespace NnShop\Application\Listeners\Orders\Order;

use Core\Application\CoreBundle\Service\Mail\Mailer;
use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Application\AppBundle\Mail\DeliverManualControlMail;
use NnShop\Domain\Orders\Event\Order\OrderPendingManualCheckEvent;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;

class SendManualControlEmailListener implements AsynchronousEventListenerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * Constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     * @param Mailer                   $mailer
     */
    public function __construct(OrderRepositoryInterface $orderRepository, Mailer $mailer)
    {
        $this->orderRepository = $orderRepository;
        $this->mailer = $mailer;
    }

    public function __invoke(OrderPendingManualCheckEvent $event)
    {
        $order = $this->orderRepository->getById($event->getId());

        $this->mailer->send(DeliverManualControlMail::fromOrder($order));
    }
}
