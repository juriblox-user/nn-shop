<?php

namespace NnShop\Application\Listeners\Orders\Order;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Domain\Orders\Command\Customer\InviteCustomerCommand;
use NnShop\Domain\Orders\Enumeration\CustomerStatus;
use NnShop\Domain\Orders\Event\Order\OrderCompletedEvent;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;

class InviteCustomerAfterOrderCompletedListener implements AsynchronousEventListenerInterface, CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * Constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param OrderCompletedEvent $event
     */
    public function __invoke(OrderCompletedEvent $event)
    {
        $order = $this->orderRepository->findOneById($event->getId());
        if (null === $order) {
            return;
        }

        if ($order->getCustomer()->getStatus()->isNot(CustomerStatus::DRAFT)) {
            return;
        }

//        $this->commandBus->handle(InviteCustomerCommand::create($order->getCustomer()->getId()));
    }
}
