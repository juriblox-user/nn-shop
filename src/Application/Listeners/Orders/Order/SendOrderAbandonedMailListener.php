<?php

namespace NnShop\Application\Listeners\Orders\Order;

use Core\Application\CoreBundle\Service\Mail\Mailer;
use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Application\AppBundle\Mail\OrderAbandonedMail;
use NnShop\Domain\Orders\Enumeration\OrderStatus;
use NnShop\Domain\Orders\Event\Order\OrderAbandonedEvent;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use Symfony\Component\Translation\TranslatorInterface;

class SendOrderAbandonedMailListener implements AsynchronousEventListenerInterface
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * SendOrderAbandonedMailListener constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     * @param Mailer                   $mailer
     * @param TranslatorInterface      $translator
     */
    public function __construct(OrderRepositoryInterface $orderRepository, Mailer $mailer, TranslatorInterface $translator)
    {
        $this->orderRepository = $orderRepository;
        $this->mailer = $mailer;
        $this->translator = $translator;
    }

    /**
     * @param OrderAbandonedEvent $event
     *
     * @throws \Core\Common\Exceptions\MailerException
     */
    public function __invoke(OrderAbandonedEvent $event)
    {
        $order = $this->orderRepository->getById($event->getId());
        if (null === $order->getCustomer()) {
            return;
        }

        // Als er nog betaald moet worden dan gaat de herinnering via het proces van de betalingen
        if ($order->getStatus()->is(OrderStatus::PENDING_PAYMENT) && $order->getInvoice()->hasPayments()) {
            return;
        }

        $this->mailer->send(OrderAbandonedMail::fromOrder($order, $this->translator));
    }
}
