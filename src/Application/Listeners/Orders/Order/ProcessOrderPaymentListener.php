<?php

namespace NnShop\Application\Listeners\Orders\Order;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Domain\Orders\Command\Order\ProcessOrderPaymentCommand;
use NnShop\Domain\Orders\Event\OrderPaidEvent;

class ProcessOrderPaymentListener implements AsynchronousEventListenerInterface, CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @param OrderPaidEvent $event
     */
    public function __invoke(OrderPaidEvent $event)
    {
        $this->commandBus->handle(
            ProcessOrderPaymentCommand::create($event->getId())
        );
    }
}
