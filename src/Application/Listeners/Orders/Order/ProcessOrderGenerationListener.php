<?php

namespace NnShop\Application\Listeners\Orders\Order;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Domain\Orders\Command\Order\ProcessOrderGenerationCommand;
use NnShop\Domain\Templates\Event\DocumentGenerationSucceededEvent;
use NnShop\Domain\Templates\Repository\DocumentRepositoryInterface;

class ProcessOrderGenerationListener implements AsynchronousEventListenerInterface, CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @var DocumentRepositoryInterface
     */
    private $documentRepository;

    /**
     * ProcessOrderGenerationListener constructor.
     *
     * @param DocumentRepositoryInterface $documentRepository
     */
    public function __construct(DocumentRepositoryInterface $documentRepository)
    {
        $this->documentRepository = $documentRepository;
    }

    /**
     * @param DocumentGenerationSucceededEvent $event
     *
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function __invoke(DocumentGenerationSucceededEvent $event)
    {
        $document = $this->documentRepository->getById($event->getId());

        $this->commandBus->handle(ProcessOrderGenerationCommand::create($document->getOrder()->getId()));
    }
}
