<?php

namespace NnShop\Application\Listeners\Orders\Order;

use Core\Application\CoreBundle\Service\Mail\Mailer;
use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Application\AppBundle\Mail\OrderRequeuedMail;
use NnShop\Domain\Orders\Event\Order\OrderRequeuedEvent;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use Symfony\Component\Translation\TranslatorInterface;

class SendOrderRequeuedMailListener implements AsynchronousEventListenerInterface
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * SendOrderRequeuedMailListener constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     * @param Mailer                   $mailer
     * @param TranslatorInterface      $translator
     */
    public function __construct(OrderRepositoryInterface $orderRepository, Mailer $mailer, TranslatorInterface $translator)
    {
        $this->orderRepository = $orderRepository;
        $this->mailer = $mailer;
        $this->translator = $translator;
    }

    /**
     * @param OrderRequeuedEvent $event
     */
    public function __invoke(OrderRequeuedEvent $event)
    {
        $order = $this->orderRepository->getById($event->getId());

        $this->mailer->send(OrderRequeuedMail::fromOrder($order, $this->translator));
    }
}
