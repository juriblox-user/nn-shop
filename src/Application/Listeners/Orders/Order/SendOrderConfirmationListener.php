<?php

namespace NnShop\Application\Listeners\Orders\Order;

use Core\Application\CoreBundle\Service\Mail\Mailer;
use Core\Component\Mail\Attachment;
use Core\Component\Mail\ContentType;
use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Application\AppBundle\Mail\OrderConfirmationMail;
use NnShop\Application\AppBundle\Services\Manager\InvoicePdfManager;
use NnShop\Domain\Orders\Enumeration\PaymentMethod;
use NnShop\Domain\Orders\Event\Order\OrderConfirmedEvent;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use Symfony\Component\Translation\TranslatorInterface;

class SendOrderConfirmationListener implements AsynchronousEventListenerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var InvoicePdfManager
     */
    private $pdfManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * SendOrderConfirmationListener constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     * @param Mailer                   $mailer
     * @param InvoicePdfManager        $pdfManager
     * @param TranslatorInterface      $translator
     */
    public function __construct(OrderRepositoryInterface $orderRepository, Mailer $mailer, InvoicePdfManager $pdfManager, TranslatorInterface $translator)
    {
        $this->orderRepository = $orderRepository;
        $this->translator = $translator;

        $this->mailer = $mailer;
        $this->pdfManager = $pdfManager;
    }

    /**
     * @param OrderConfirmedEvent $event
     */
    public function __invoke(OrderConfirmedEvent $event)
    {
        $order = $this->orderRepository->getById($event->getId());

        // Alleen bij bestellingen met handmatige actie of een langzamere betaalmethode
        if ($order->getPaymentMethod()->isNot(PaymentMethod::BANK_TRANSFER)) {
            return;
        }

        $invoice = $order->getInvoice();
        if ($invoice->isPaid()) {
            $invoiceName = sprintf('Factuur %s (voldaan).pdf', $invoice->getCode());
        } else {
            $invoiceName = sprintf('Factuur %s.pdf', $invoice->getCode());
        }

        $mail = OrderConfirmationMail::fromOrder($order, $this->translator);
        $mail->attach(Attachment::fromRaw($invoiceName, ContentType::from('application/pdf'), $this->pdfManager->get($invoice)));

        $this->mailer->send($mail);
    }
}
