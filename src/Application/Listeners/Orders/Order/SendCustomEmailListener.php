<?php

namespace NnShop\Application\Listeners\Orders\Order;

use Core\Application\CoreBundle\Service\Mail\Mailer;
use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Application\AppBundle\Mail\DeliverCustomMail;
use NnShop\Domain\Orders\Event\Order\OrderPendingManualCustomEvent;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use Symfony\Component\Translation\TranslatorInterface;

class SendCustomEmailListener implements AsynchronousEventListenerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * SendCustomEmailListener constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     * @param Mailer                   $mailer
     * @param TranslatorInterface      $translator
     */
    public function __construct(OrderRepositoryInterface $orderRepository, Mailer $mailer, TranslatorInterface $translator)
    {
        $this->translator = $translator;
        $this->orderRepository = $orderRepository;
        $this->mailer = $mailer;
    }

    /**
     * @param OrderPendingManualCustomEvent $event
     *
     * @throws \Core\Common\Exceptions\MailerException
     */
    public function __invoke(OrderPendingManualCustomEvent $event)
    {
        $order = $this->orderRepository->getById($event->getId());

        $this->mailer->send(DeliverCustomMail::fromOrder($order, $this->translator));
    }
}
