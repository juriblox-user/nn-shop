<?php

namespace NnShop\Application\Listeners\Orders\Subscription;

use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Domain\Orders\Event\Subscription\SubscriptionReadyEvent;

class IssueFirstOrderAfterSubscription implements AsynchronousEventListenerInterface
{
    public function __invoke(SubscriptionReadyEvent $event)
    {
    }
}
