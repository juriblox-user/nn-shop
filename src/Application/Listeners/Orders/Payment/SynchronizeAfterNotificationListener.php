<?php

namespace NnShop\Application\Listeners\Orders\Payment;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Domain\Mollie\Event\Transaction\ReceivedTransactionNotificationEvent;
use NnShop\Domain\Orders\Command\Payment\SynchronizeTransactionCommand;

class SynchronizeAfterNotificationListener implements AsynchronousEventListenerInterface, CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @param ReceivedTransactionNotificationEvent $event
     */
    public function __invoke(ReceivedTransactionNotificationEvent $event)
    {
        $this->commandBus->handle(SynchronizeTransactionCommand::create($event->getTransactionId()));
    }
}
