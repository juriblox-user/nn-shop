<?php

namespace NnShop\Application\Listeners\Orders\Invoice;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\AsynchronousEventListenerInterface;
use NnShop\Domain\Orders\Command\Invoice\ProcessInvoicePaymentCommand;
use NnShop\Domain\Orders\Event\PaymentReceivedEvent;
use NnShop\Domain\Orders\Repository\PaymentRepositoryInterface;

class ProcessInvoicePaymentListener implements AsynchronousEventListenerInterface, CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @var PaymentRepositoryInterface
     */
    private $paymentRepository;

    /**
     * UpdateStatusAfterReceivingPaymentListener constructor.
     *
     * @param PaymentRepositoryInterface $paymentRepository
     */
    public function __construct(PaymentRepositoryInterface $paymentRepository)
    {
        $this->paymentRepository = $paymentRepository;
    }

    /**
     * @param PaymentReceivedEvent $event
     */
    public function __invoke(PaymentReceivedEvent $event)
    {
        $payment = $this->paymentRepository->getById($event->getId());

        $this->commandBus->handle(
            ProcessInvoicePaymentCommand::create($payment->getInvoice()->getId(), $payment->getTimestampReceived())
        );
    }
}
