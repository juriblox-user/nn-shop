<?php

namespace NnShop\Application\Handlers\Templates\Answer;

use Core\Common\Exception\DataIntegrityException;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\Answer\AnswerQuestionCommand;
use NnShop\Domain\Templates\Entity\Answer;
use NnShop\Domain\Templates\Repository\AnswerRepositoryInterface;
use NnShop\Domain\Templates\Repository\DocumentRepositoryInterface;
use NnShop\Domain\Templates\Repository\QuestionRepositoryInterface;
use NnShop\Domain\Templates\Value\AnswerId;

class AnswerQuestionHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var DocumentRepositoryInterface
     */
    private $documentRepository;

    /**
     * @var QuestionRepositoryInterface
     */
    private $questionRepository;

    /**
     * @var AnswerRepositoryInterface
     */
    private $answerRepository;

    /**
     * GiveAnswerToQuestionHandler constructor.
     *
     * @param DocumentRepositoryInterface $documentRepository
     * @param QuestionRepositoryInterface $questionRepository
     * @param AnswerRepositoryInterface   $answerRepository
     */
    public function __construct(DocumentRepositoryInterface $documentRepository, QuestionRepositoryInterface $questionRepository, AnswerRepositoryInterface $answerRepository)
    {
        $this->documentRepository = $documentRepository;
        $this->questionRepository = $questionRepository;
        $this->answerRepository = $answerRepository;
    }

    /**
     * @param AnswerQuestionCommand $command
     */
    public function __invoke(AnswerQuestionCommand $command)
    {
        $document = $this->documentRepository->getById($command->getDocumentId());

        $question = $this->questionRepository->getById($command->getQuestionId());
        if ($question->getStep()->getTemplate() !== $document->getTemplate()) {
            throw new DataIntegrityException('The question does not belong to this documents\'s template');
        }

        $answer = $this->answerRepository->findOneByDocumentAndQuestion($document, $question);
        if (null === $answer) {
            $answer = Answer::create(AnswerId::generate(), $document, $question);
        } else {
            $answer->clear();
        }

        if (null !== $command->getValue()) {
            $answer->setValue($command->getValue());
        } else {
            foreach ($command->getOptionIds() as $optionId) {
                $answer->addOption($question->getOption($optionId));
            }
        }

        $this->answerRepository->persist($answer);
    }
}
