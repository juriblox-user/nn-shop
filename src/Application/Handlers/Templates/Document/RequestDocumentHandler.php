<?php

namespace NnShop\Application\Handlers\Templates\Document;

use Core\SimpleBus\EventBusAwareInterface;
use Core\SimpleBus\EventBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use JuriBlox\Sdk\Domain\Customers\Entities\Customer as RemoteCustomer;
use JuriBlox\Sdk\Domain\Customers\Values\Contact as RemoteContact;
use JuriBlox\Sdk\Domain\Documents\Entities\DocumentRequest as RemoteDocumentRequest;
use JuriBlox\Sdk\Domain\Documents\Entities\QuestionAnswer as RemoteQuestionAnswer;
use JuriBlox\Sdk\Domain\Documents\Values\DocumentReference;
use JuriBlox\Sdk\Exceptions\EngineOperationException;
use NnShop\Application\Traits\JuribloxApiAwareTrait;
use NnShop\Domain\JuriBlox\Event\DocumentRequest\DocumentGenerationFailedEvent;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;
use NnShop\Domain\Templates\Command\Document\RequestDocumentCommand;
use NnShop\Domain\Templates\Entity\DocumentRequest;
use NnShop\Domain\Templates\Enumeration\DocumentStatus;
use NnShop\Domain\Templates\Repository\DocumentRepositoryInterface;
use NnShop\Domain\Templates\Repository\DocumentRequestRepositoryInterface;
use NnShop\Domain\Templates\Value\DocumentRequestId;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class RequestDocumentHandler implements EventBusAwareInterface, LoggerAwareInterface, SynchronousCommandHandlerInterface
{
    use EventBusAwareTrait;
    use LoggerAwareTrait;
    use JuribloxApiAwareTrait;

    /**
     * @var DocumentRepositoryInterface
     */
    private $documentRepository;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var DocumentRequestRepositoryInterface
     */
    private $requestRepository;

    /**
     * RequestDocumentHandler constructor.
     *
     * @param DocumentRepositoryInterface        $documentRepository
     * @param DocumentRequestRepositoryInterface $requestRepository
     * @param CustomerRepositoryInterface        $customerRepository
     */
    public function __construct(DocumentRepositoryInterface $documentRepository, DocumentRequestRepositoryInterface $requestRepository, CustomerRepositoryInterface $customerRepository)
    {
        $this->documentRepository = $documentRepository;
        $this->requestRepository = $requestRepository;

        $this->customerRepository = $customerRepository;
    }

    /**
     * @param RequestDocumentCommand $command
     */
    public function __invoke(RequestDocumentCommand $command)
    {
        $document = $this->documentRepository->getById($command->getId());

        if (!$document->getStatus()->in([DocumentStatus::DRAFT, DocumentStatus::ERROR])) {
            throw new \LogicException('Document should have the DRAFT or ERROR status');
        }

        $order = $document->getOrder();
        $customer = $order->getCustomer();

        $template = $document->getTemplate();

        $this->connect($template->getPartner()->getClientId(), $template->getPartner()->getClientKey());

        /*
         * Klant aanmaken/updaten
         */
        $this->logger->info('Synchronizing customer information to JuriBlox', [
            'id' => $customer->getId()->getString(),
        ]);

//        if (null === $customer->getRemoteReference()) {
        $remoteCustomer = new RemoteCustomer();
        $remoteCustomer->setCompany($customer->getCompanyName() ?: '[consument]');
        $remoteCustomer->setContact(new RemoteContact($customer->getName(), $customer->getEmail()->getString()));

        $remoteCustomer = $this->client->customers()->create($remoteCustomer);

        $customer->setRemoteReference($remoteCustomer->getReference());

        $this->customerRepository->persist($customer);
//        } else {
//            $remoteCustomer = $this->client->customers()->findOneByReference($customer->getRemoteReference());
//            $remoteCustomer->setCompany($customer->getCompanyName() ?: '[consument]');
//            $remoteCustomer->setContact(new RemoteContact($customer->getName(), $customer->getEmail()->getString()));
//
//            $this->client->customers()->update($remoteCustomer);
//        }

        /*
         * Document aanvragen
         */
        $requestId = DocumentRequestId::generate();

        $this->logger->info('Requesting document generation for "{templateTitle}"', [
            'documentId' => $document->getId()->getString(),
            'templateTitle' => $template->getTitle(),
        ]);

        $request = RemoteDocumentRequest::prepare($template->getRemoteId());
        $request->setCustomer($remoteCustomer->getReference());
        $request->setTemplateVersion($document->getTemplateVersion()->getInteger());

        $request->setTitle(sprintf('%s [%d]', $template->getTitle(), $order->getCode()->getInteger()));
        $request->setReference(new DocumentReference($requestId->getString()));

        foreach ($document->getAnswers() as $answer) {
            $question = $answer->getQuestion();

            if ($question->isDeleted()) {
                $this->logger->info('Excluding soft-deleted question from API call', [
                    'answerId' => $answer->getId()->getString(),
                    'questionId' => $question->getId()->getString(),
                ]);

 //               continue;
            }

            $remoteAnswer = RemoteQuestionAnswer::createForQuestionId($question->getRemoteId());

            if ($answer->hasOptions()) {
                $options = [];
                foreach ($answer->getOptions() as $option) {
                    $options[] = $option->getRemoteId();
                }

                $remoteAnswer->setValue($options);
            } else {
                $remoteAnswer->setValue($answer->getValue());
            }

            $request->addAnswer($remoteAnswer);
        }

        $documentRequest = DocumentRequest::create($requestId, $document);

        try {
            $request = $this->client->documents()->generate($request);

            $documentRequest->setRemoteId($request->getId());
        } catch (EngineOperationException $exception) {
            $this->logger->error('Exception while requesting document: ' . $exception->getMessage(), [
                'exception' => $exception,
            ]);

            $this->eventBus->handle(
                DocumentGenerationFailedEvent::fromLocal($requestId)
            );
        } finally {
            $this->requestRepository->persist($documentRequest);
        }
    }
}
