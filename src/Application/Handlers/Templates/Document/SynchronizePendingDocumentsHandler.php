<?php

namespace NnShop\Application\Handlers\Templates\Document;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\Document\SynchronizeDocumentCommand;
use NnShop\Domain\Templates\Command\Document\SynchronizePendingDocumentsCommand;
use NnShop\Domain\Templates\Repository\DocumentRepositoryInterface;

class SynchronizePendingDocumentsHandler implements CommandBusAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;

    /**
     * @var DocumentRepositoryInterface
     */
    private $repository;

    /**
     * SynchronizeDocumentsHandler constructor.
     *
     * @param DocumentRepositoryInterface $repository
     */
    public function __construct(DocumentRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param SynchronizePendingDocumentsCommand $command
     */
    public function __invoke(SynchronizePendingDocumentsCommand $command)
    {
        foreach ($this->repository->findPending() as $document) {
            $this->commandBus->handle(
                SynchronizeDocumentCommand::fromLocal($document->getId())
            );
        }
    }
}
