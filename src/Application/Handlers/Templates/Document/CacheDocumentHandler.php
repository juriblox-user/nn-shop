<?php

namespace NnShop\Application\Handlers\Templates\Document;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use JuriBlox\Sdk\Domain\Documents\Values\FileType;
use JuriBlox\Sdk\Infrastructure\Downloaders\DocumentDownloader;
use NnShop\Application\AppBundle\Services\Manager\DocumentFileManager;
use NnShop\Application\Traits\JuribloxApiAwareTrait;
use NnShop\Domain\Templates\Command\Document\CacheDocumentCommand;
use NnShop\Domain\Templates\Enumeration\DocumentStatus;
use NnShop\Domain\Templates\Repository\DocumentRepositoryInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class CacheDocumentHandler implements LoggerAwareInterface, SynchronousCommandHandlerInterface
{
    use JuribloxApiAwareTrait;
    use LoggerAwareTrait;

    /**
     * @var DocumentFileManager
     */
    private $fileManager;

    /**
     * @var DocumentRepositoryInterface
     */
    private $repository;

    /**
     * CacheDocumentHandler constructor.
     *
     * @param DocumentRepositoryInterface $repository
     * @param DocumentFileManager         $fileManager
     */
    public function __construct(DocumentRepositoryInterface $repository, DocumentFileManager $fileManager)
    {
        $this->repository = $repository;
        $this->fileManager = $fileManager;
    }

    /**
     * @param CacheDocumentCommand $command
     */
    public function __invoke(CacheDocumentCommand $command)
    {
        $document = $this->repository->getById($command->getId());

        if (DocumentStatus::GENERATED != $document->getStatus()) {
            throw new \LogicException('DocumentStatus should be GENERATED');
        }

        $this->logger->info('Downloading files for document [{id}]', [
            'id' => $document->getId()->getString(),
        ]);

        if ($document->isCached()) {
            $this->logger->info('Skipping download, Document::$cached flag is true');

            return;
        }

        // JuriBlox
        $this->connect($document->getPartner()->getClientId(), $document->getPartner()->getClientKey());

        $remoteDocument = $this->client->documents()->findOneById($document->getRemoteId());

        // Word en PDF opslaan
        $this->fileManager->storeWord2007($document, DocumentDownloader::download($this->driver, $remoteDocument->getId(), new FileType(FileType::TYPE_WORD2007)));
        $this->fileManager->storePdf($document, DocumentDownloader::download($this->driver, $remoteDocument->getId(), new FileType(FileType::TYPE_PDF)));

        // Markdown downloaden
        $this->fileManager->storeMarkdown($document, $remoteDocument->getMarkdown());

        $document->setCached(true);

        $this->repository->persist($document);
    }
}
