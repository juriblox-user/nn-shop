<?php

namespace NnShop\Application\Handlers\Templates\Document;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\Document\IncreaseDownloadCounterCommand;
use NnShop\Domain\Templates\Repository\DocumentRepositoryInterface;

class IncreaseDownloadCounterHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var DocumentRepositoryInterface
     */
    private $repository;

    /**
     * IncreaseDownloadCounterHandler constructor.
     *
     * @param DocumentRepositoryInterface $repository
     */
    public function __construct(DocumentRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param IncreaseDownloadCounterCommand $command
     *
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function __invoke(IncreaseDownloadCounterCommand $command)
    {
        $document = $this->repository->getById($command->getId());
        $document->incrementDownloads();

        $this->repository->persist($document);
    }
}
