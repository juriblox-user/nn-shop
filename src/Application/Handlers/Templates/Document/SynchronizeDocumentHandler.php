<?php

namespace NnShop\Application\Handlers\Templates\Document;

use Core\Common\Exception\DataIntegrityException;
use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Application\Traits\JuribloxApiAwareTrait;
use NnShop\Domain\Templates\Command\Document\CacheDocumentCommand;
use NnShop\Domain\Templates\Command\Document\SynchronizeDocumentCommand;
use NnShop\Domain\Templates\Command\DocumentRequest\SynchronizeRequestCommand;
use NnShop\Domain\Templates\Enumeration\DocumentStatus;
use NnShop\Domain\Templates\Repository\DocumentRepositoryInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class SynchronizeDocumentHandler implements CommandBusAwareInterface, LoggerAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;
    use LoggerAwareTrait;
    use JuribloxApiAwareTrait;

    /**
     * @var DocumentRepositoryInterface
     */
    private $repository;

    /**
     * SynchronizeDocumentHandler constructor.
     *
     * @param DocumentRepositoryInterface $repository
     */
    public function __construct(DocumentRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param SynchronizeDocumentCommand $command
     *
     * @throws DataIntegrityException
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function __invoke(SynchronizeDocumentCommand $command)
    {
        $document = $command->isLocal() ? $this->repository->getById($command->getLocalId()) : $this->repository->getByRemoteId($command->getRemoteId());

        $this->logger->info('Synchronizing document status for "{templateTitle}" with JuriBlox', [
            'documentId' => $document->getId()->getString(),
            'templateTitle' => $document->getTemplate()->getTitle(),
        ]);

        /*
         * DocumentRequests synchroniseren
         */
        if (DocumentStatus::PENDING == $document->getStatus()) {
            $hasPending = false;

            foreach ($document->getPendingRequests() as $request) {
                $this->commandBus->handle(SynchronizeRequestCommand::create($request->getId()));
                $hasPending = true;
            }

            // We hebben al een RemoteId, dus dan is het genereren al goedgegaan
            if (null !== $document->getRemoteId()) {
                $document->setStatus(DocumentStatus::from(DocumentStatus::GENERATED));
                $document->setCached(false);

                $this->repository->persist($document);
            }

            // Corrupte status
            elseif (!$hasPending) {
                throw new DataIntegrityException('The document has no pending requests but its status does not reflect a success or error state');
            }
        }

        /*
         * Bestanden downloaden
         */
        if (DocumentStatus::GENERATED == $document->getStatus()) {
            // Corrupte status
            if (null === $document->getRemoteId()) {
                throw new DataIntegrityException('The document has a GENERATED status but has an empty Document::$remoteId');
            }

            $this->commandBus->handle(CacheDocumentCommand::create($document->getId()));
        }
    }
}
