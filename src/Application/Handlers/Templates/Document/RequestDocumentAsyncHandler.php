<?php

namespace NnShop\Application\Handlers\Templates\Document;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\AsynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\Document\RequestDocumentAsyncCommand;

class RequestDocumentAsyncHandler implements CommandBusAwareInterface, AsynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;

    /**
     * @param RequestDocumentAsyncCommand $command
     */
    public function __invoke(RequestDocumentAsyncCommand $command)
    {
        $this->commandBus->handle($command->getCommand());
    }
}
