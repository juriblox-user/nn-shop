<?php

namespace NnShop\Application\Handlers\Templates\CompanyType;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\CompanyType\CreateTypeCommand;
use NnShop\Domain\Templates\Entity\CompanyType;
use NnShop\Domain\Templates\Repository\CompanyTypeRepositoryInterface;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;

class CreateTypeHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CompanyTypeRepositoryInterface
     */
    private $typeRepository;

    /**
     * @var ProfileRepositoryInterface
     */
    private $profileRepository;

    /**
     * CreateTypeHandler constructor.
     *
     * @param CompanyTypeRepositoryInterface $typeRepository
     * @param ProfileRepositoryInterface     $profileRepository
     */
    public function __construct(CompanyTypeRepositoryInterface $typeRepository, ProfileRepositoryInterface $profileRepository)
    {
        $this->typeRepository = $typeRepository;
        $this->profileRepository = $profileRepository;
    }

    /**
     * @param CreateTypeCommand $command
     */
    public function __invoke(CreateTypeCommand $command)
    {
        $profile = $this->profileRepository->getById($command->getProfileId());

        $type = CompanyType::create($command->getId(), $command->getTitle(), $profile);
        $type->setDescription($command->getDescription());

        $this->typeRepository->persist($type);
    }
}
