<?php

namespace NnShop\Application\Handlers\Templates\CompanyType;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\CompanyType\UpdateTypeCommand;
use NnShop\Domain\Templates\Repository\CompanyTypeRepositoryInterface;

class UpdateTypeHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CompanyTypeRepositoryInterface
     */
    private $typeRepository;

    /**
     * UpdateTypeHandler constructor.
     *
     * @param CompanyTypeRepositoryInterface $typeRepository
     */
    public function __construct(CompanyTypeRepositoryInterface $typeRepository)
    {
        $this->typeRepository = $typeRepository;
    }

    /**
     * @param UpdateTypeCommand $command
     *
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function __invoke(UpdateTypeCommand $command)
    {
        $type = $this->typeRepository->getById($command->getId());

        $type->setTitle($command->getTitle() ?: $type->getTitle());
        $type->setDescription($command->getDescription() ?: $type->getDescription());

        $this->typeRepository->persist($type);
    }
}
