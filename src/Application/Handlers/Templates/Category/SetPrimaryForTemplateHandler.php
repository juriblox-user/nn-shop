<?php

namespace NnShop\Application\Handlers\Templates\Category;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\Category\SetPrimaryForTemplateCommand;
use NnShop\Domain\Templates\Repository\CategoryRepositoryInterface;
use NnShop\Domain\Templates\Repository\CategoryTemplateRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class SetPrimaryForTemplateHandler implements LoggerAwareInterface, SynchronousCommandHandlerInterface
{
    use LoggerAwareTrait;

    /**
     * @var CategoryTemplateRepositoryInterface
     */
    private $linkRepository;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * SetPrimaryCategoryHandler constructor.
     *
     * @param CategoryTemplateRepositoryInterface $linkRepository
     */
    public function __construct(CategoryTemplateRepositoryInterface $linkRepository, CategoryRepositoryInterface $categoryRepository, TemplateRepositoryInterface $templateRepository)
    {
        $this->linkRepository = $linkRepository;

        $this->categoryRepository = $categoryRepository;
        $this->templateRepository = $templateRepository;
    }

    /**
     * @param SetPrimaryForTemplateCommand $command
     */
    public function __invoke(SetPrimaryForTemplateCommand $command)
    {
        $category = $this->categoryRepository->getById($command->getCategoryId());
        $template = $this->templateRepository->getById($command->getTemplateId());

        $this->logger->info('Setting category "{categoryTitle}" as primary category for template "{templateTitle}"', [
            'categoryId' => $category->getId()->getString(),
            'categoryTitle' => $category->getTitle(),
            'templateId' => $template->getId()->getString(),
            'templateTitle' => $template->getTitle(),
        ]);

        foreach ($this->linkRepository->findByTemplate($template) as $link) {
            $link->setPrimary(false);

            $this->linkRepository->persist($link);
        }

        $link = $this->linkRepository->getByCategoryAndTemplate($category, $template);
        $link->setPrimary(true);

        $this->linkRepository->persist($link);
    }
}
