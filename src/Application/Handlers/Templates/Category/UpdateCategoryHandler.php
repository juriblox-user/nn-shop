<?php

namespace NnShop\Application\Handlers\Templates\Category;

use Core\Common\Exception\NotImplementedException;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\Category\UpdateCategoryCommand;
use NnShop\Domain\Templates\Repository\CategoryRepositoryInterface;

class UpdateCategoryHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    /**
     * UpdateCategoryHandler constructor.
     *
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param UpdateCategoryCommand $command
     *
     * @throws NotImplementedException
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function __invoke(UpdateCategoryCommand $command)
    {
        $category = $this->categoryRepository->getById($command->getId());
        $category->setTitle($command->getTitle() ?: $category->getTitle());
        $category->setDescription($command->getDescription());
        $category->setMetaDescription($command->getMetaDescription());

        // Verhuizen van categorieën is nog niet mogelijk
        if ($category->getShop()->getId() != $command->getShop()) {
            throw new NotImplementedException('It is not yet possible to move categories from one shop to another');
        }

        $this->categoryRepository->persist($category);
    }
}
