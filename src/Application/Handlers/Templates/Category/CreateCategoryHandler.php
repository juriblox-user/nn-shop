<?php

namespace NnShop\Application\Handlers\Templates\Category;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\Category\CreateCategoryCommand;
use NnShop\Domain\Templates\Entity\Category;
use NnShop\Domain\Templates\Repository\CategoryRepositoryInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class CreateCategoryHandler implements LoggerAwareInterface, SynchronousCommandHandlerInterface
{
    use LoggerAwareTrait;

    /**
     * @var CategoryRepositoryInterface
     */
    private $repository;

    /**
     * CreateCategoryHandler constructor.
     *
     * @param CategoryRepositoryInterface $repository
     */
    public function __construct(CategoryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param CreateCategoryCommand $command
     */
    public function __invoke(CreateCategoryCommand $command)
    {
        $category = Category::create($command->getId(), $command->getShop(), $command->getTitle(), $command->getDescription(), $command->getMetaDescription());

        $this->logger->info('Creating category "{title}"', [
            'id' => $category->getId(),
            'title' => $category->getTitle(),
        ]);

        $this->repository->persist($category);
    }
}
