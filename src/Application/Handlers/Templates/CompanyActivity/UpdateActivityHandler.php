<?php

namespace NnShop\Application\Handlers\Templates\CompanyActivity;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\CompanyActivity\UpdateActivityCommand;
use NnShop\Domain\Templates\Repository\CompanyActivityRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;

class UpdateActivityHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CompanyActivityRepositoryInterface
     */
    private $activityRepository;

    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * UpdateActivityHandler constructor.
     *
     * @param CompanyActivityRepositoryInterface $activityRepository
     * @param TemplateRepositoryInterface        $templateRepository
     */
    public function __construct(CompanyActivityRepositoryInterface $activityRepository, TemplateRepositoryInterface $templateRepository)
    {
        $this->activityRepository = $activityRepository;
        $this->templateRepository = $templateRepository;
    }

    /**
     * @param UpdateActivityCommand $command
     */
    public function __invoke(UpdateActivityCommand $command)
    {
        $activity = $this->activityRepository->getById($command->getId());
        $activity->setTitle($command->getTitle() ?: $activity->getTitle());

        $seenIds = [];
        foreach ($command->getTemplates() as $templateId) {
            $template = $this->templateRepository->getReference($templateId);
            if (!$activity->hasTemplate($template)) {
                $activity->addTemplate($template);
            }

            $seenIds[] = $templateId->getString();
        }

        // Overbodige koppelingen opschonen
        foreach ($activity->getTemplates() as $template) {
            if (!in_array($template->getId()->getString(), $seenIds)) {
                $activity->removeTemplate($template);
            }
        }

        $this->activityRepository->persist($activity);
    }
}
