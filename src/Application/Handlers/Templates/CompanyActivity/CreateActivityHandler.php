<?php

namespace NnShop\Application\Handlers\Templates\CompanyActivity;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\CompanyActivity\CreateActivityCommand;
use NnShop\Domain\Templates\Entity\CompanyActivity;
use NnShop\Domain\Templates\Repository\CompanyActivityRepositoryInterface;
use NnShop\Domain\Templates\Repository\CompanyTypeRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;

class CreateActivityHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CompanyActivityRepositoryInterface
     */
    private $activityRepository;

    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * @var CompanyTypeRepositoryInterface
     */
    private $typeRepository;

    /**
     * CreateActivityHandler constructor.
     *
     * @param CompanyActivityRepositoryInterface $activityRepository
     * @param CompanyTypeRepositoryInterface     $typeRepository
     * @param TemplateRepositoryInterface        $templateRepository
     */
    public function __construct(CompanyActivityRepositoryInterface $activityRepository, CompanyTypeRepositoryInterface $typeRepository, TemplateRepositoryInterface $templateRepository)
    {
        $this->activityRepository = $activityRepository;

        $this->typeRepository = $typeRepository;
        $this->templateRepository = $templateRepository;
    }

    /**
     * @param CreateActivityCommand $command
     */
    public function __invoke(CreateActivityCommand $command)
    {
        $activity = CompanyActivity::create($command->getId(), $this->typeRepository->getReference($command->getTypeId()), $command->getTitle());

        foreach ($command->getTemplates() as $templateId) {
            $activity->addTemplate($this->templateRepository->getReference($templateId));
        }

        $this->activityRepository->persist($activity);
    }
}
