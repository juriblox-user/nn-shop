<?php

namespace NnShop\Application\Handlers\Templates\Partner;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Application\AppBundle\Services\Manager\PartnerLogoManager;
use NnShop\Domain\Templates\Command\Partner\UpdatePartnerCommand;
use NnShop\Domain\Templates\Repository\PartnerRepositoryInterface;

class UpdatePartnerHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var PartnerRepositoryInterface
     */
    private $partnerRepository;

    /**
     * @var PartnerLogoManager
     */
    private $logoManager;

    /**
     * UpdatePartnerHandler constructor.
     *
     * @param PartnerRepositoryInterface $partnerRepository
     * @param PartnerLogoManager         $logoManager
     */
    public function __construct(PartnerRepositoryInterface $partnerRepository, PartnerLogoManager $logoManager)
    {
        $this->partnerRepository = $partnerRepository;
        $this->logoManager = $logoManager;
    }

    /**
     * @param UpdatePartnerCommand $command
     *
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function __invoke(UpdatePartnerCommand $command)
    {
        $partner = $this->partnerRepository->getById($command->getId());

        $partner->setTitle($command->getTitle() ?: $partner->getTitle());
        $partner->setExcerpt($command->getExcerpt() ?: $partner->getExcerpt());
        $partner->setProfile($command->getProfile() ?: $partner->getProfile());

        $partner->setRemoteId($command->getRemoteId() ?: $partner->getRemoteId());

        if ($command->isSyncEnabled()) {
            $partner->enableSync($command->getSyncClientId(), $command->getSyncClientKey());
        } else {
            $partner->disableSync();
        }

        // Logo wijzigen
        if ($command->hasLogo()) {
            $this->logoManager->setLogo($partner, $command->getLogo());
        }

        // Logo verwijderen
        elseif ($command->deleteLogo()) {
            $this->logoManager->deleteLogo($partner);
        }

        $this->partnerRepository->persist($partner);
    }
}
