<?php

namespace NnShop\Application\Handlers\Templates\Partner;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\AsynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\Partner\SynchronizePartnerAsyncCommand;

class SynchronizePartnerAsyncHandler implements CommandBusAwareInterface, AsynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;

    /**
     * @param SynchronizePartnerAsyncCommand $command
     */
    public function __invoke(SynchronizePartnerAsyncCommand $command)
    {
        $this->commandBus->handle($command->getCommand());
    }
}
