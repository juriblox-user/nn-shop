<?php

namespace NnShop\Application\Handlers\Templates\Partner;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\EventBusAwareInterface;
use Core\SimpleBus\EventBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use JuriBlox\Sdk\Domain\Documents\Entities\Template as RemoteTemplate;
use NnShop\Application\Traits\JuribloxApiAwareTrait;
use NnShop\Domain\Templates\Command\Partner\SynchronizePartnerCommand;
use NnShop\Domain\Templates\Command\Template\ArchiveTemplateCommand;
use NnShop\Domain\Templates\Command\Template\SynchronizeTemplateCommand;
use NnShop\Domain\Templates\Event\PartnerWasSynchronized;
use NnShop\Domain\Templates\Repository\PartnerRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use NnShop\Domain\Templates\Value\TemplateVersion;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class SynchronizePartnerHandler implements CommandBusAwareInterface, EventBusAwareInterface, LoggerAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;
    use EventBusAwareTrait;
    use LoggerAwareTrait;

    use JuribloxApiAwareTrait;

    /**
     * @var PartnerRepositoryInterface
     */
    private $partnerRepository;

    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * SynchronizePartnerHandler constructor.
     *
     * @param PartnerRepositoryInterface  $partnerRepository
     * @param TemplateRepositoryInterface $templateRepository
     */
    public function __construct(
        PartnerRepositoryInterface $partnerRepository,
        TemplateRepositoryInterface $templateRepository)
    {
        $this->partnerRepository = $partnerRepository;
        $this->templateRepository = $templateRepository;
    }

    /**
     * @param SynchronizePartnerCommand $command
     *
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function __invoke(SynchronizePartnerCommand $command)
    {
        if (null !== $command->getLocalId()) {
            $partner = $this->partnerRepository->getById($command->getLocalId());
        } elseif (null !== $command->getRemoteId()) {
            $partner = $this->partnerRepository->getByRemoteId($command->getRemoteId());
        } else {
            throw new \InvalidArgumentException();
        }

        $this->logger->debug('Synchronizing partner "{title}"', [
            'id' => $partner->getId()->getString(),
            'title' => $partner->getTitle(),
        ]);

        // Verbinding maken met JuriBlox
        $this->connect($partner->getClientId(), $partner->getClientKey());

        /*
         * Templates synchroniseren
         */
        $seenIds = new ArrayCollection();

        /** @var RemoteTemplate $remoteTemplate */
        foreach ($this->client->templates()->findAll() as $remoteTemplate) {
            $seenIds->add($remoteTemplate->getId()->getInteger());

            $remoteVersion = new TemplateVersion($remoteTemplate->getRevision()->getVersion());

            /*
             * Alleen delta synchroniseren
             */
            $localTemplate = $this->templateRepository->findOneByRemoteId($remoteTemplate->getId());
            if (null !== $localTemplate && $localTemplate->getVersion()->compare($remoteVersion) >= 0) {
                $this->logger->notice(sprintf('Skippping template "%s": local version %s, remote version %s', $localTemplate->getTitle(), $localTemplate->getVersion(), $remoteVersion), [
                    'id' => $localTemplate->getId()->getString(),
                ]);

                continue;
            }

            $this->commandBus->handle(SynchronizeTemplateCommand::fromRemoteId($remoteTemplate->getId(), $partner->getId()));
        }

        /*
         * Templates archiveren
         */
        foreach ($this->templateRepository->findByPartner($partner->getId()) as $localTemplate) {
            if ($seenIds->contains($localTemplate->getRemoteId()->getInteger())) {
                continue;
            }

            $this->commandBus->handle(ArchiveTemplateCommand::create($localTemplate->getId()));
        }

        // Update synced timestamp
        $partner->updateSyncedTimestamp();
        $this->partnerRepository->persist($partner);

        // Trigger event
        $this->eventBus->handle(new PartnerWasSynchronized($partner->getId()));
    }
}
