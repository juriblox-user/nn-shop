<?php

namespace NnShop\Application\Handlers\Templates\Partner;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Application\AppBundle\Services\Manager\PartnerLogoManager;
use NnShop\Domain\Templates\Command\Partner\CreatePartnerCommand;
use NnShop\Domain\Templates\Entity\Partner;
use NnShop\Domain\Templates\Repository\PartnerRepositoryInterface;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * Class CreatePartnerHandler.
 */
class CreatePartnerHandler implements LoggerAwareInterface, SynchronousCommandHandlerInterface
{
    use LoggerAwareTrait;

    /**
     * @var PartnerRepositoryInterface
     */
    private $repository;

    /**
     * @var PartnerLogoManager
     */
    private $logoManager;

    /**
     * @var ProfileRepositoryInterface
     */
    private $profileRepository;

    /**
     * CreatePartnerHandler constructor.
     *
     * @param PartnerRepositoryInterface $repository
     * @param PartnerLogoManager         $logoManager
     * @param ProfileRepositoryInterface $profileRepository
     */
    public function __construct(PartnerRepositoryInterface $repository, PartnerLogoManager $logoManager, ProfileRepositoryInterface $profileRepository)
    {
        $this->repository = $repository;
        $this->logoManager = $logoManager;
        $this->profileRepository = $profileRepository;
    }

    /**
     * @param CreatePartnerCommand $command
     *
     * @throws \Core\Common\Exceptions\Domain\DuplicateEntityException
     */
    public function __invoke(CreatePartnerCommand $command)
    {
        $countryProfile = $this->profileRepository->getById($command->getProfileId());

        $partner = Partner::create($command->getId(), $command->getTitle(), $countryProfile);
        $partner->setRemoteId($command->getRemoteId());

        $partner->setExcerpt($command->getExcerpt());
        $partner->setProfile($command->getProfile());

        if ($command->isSyncEnabled()) {
            $partner->enableSync($command->getSyncClientId(), $command->getSyncClientKey());
        }

        if ($command->hasLogo()) {
            $this->logoManager->setLogo($partner, $command->getLogo());
        }

        $this->logger->info('Creating partner "{title}"', [
            'id' => $partner->getId()->getString(),
            'title' => $partner->getTitle(),
            'remoteId' => $partner->getRemoteId()->getInteger(),
        ]);

        $this->repository->persist($partner);
    }
}
