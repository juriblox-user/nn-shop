<?php

namespace NnShop\Application\Handlers\Templates\Template;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\Template\UnpublishTemplateCommand;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class UnpublishTemplateHandler implements LoggerAwareInterface, SynchronousCommandHandlerInterface
{
    use LoggerAwareTrait;

    /**
     * @var TemplateRepositoryInterface
     */
    private $repository;

    /**
     * UnpublishTemplateHandler constructor.
     *
     * @param TemplateRepositoryInterface $repository
     */
    public function __construct(TemplateRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param UnpublishTemplateCommand $command
     */
    public function __invoke(UnpublishTemplateCommand $command)
    {
        $template = $this->repository->getById($command->getId());
        $template->setPublished(false);

        $this->logger->info('Unpublishing template "{title}"', [
            'id' => $template->getId()->getString(),
            'title' => $template->getTitle(),
        ]);

        $this->repository->persist($template);
    }
}
