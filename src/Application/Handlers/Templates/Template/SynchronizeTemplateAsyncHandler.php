<?php

namespace NnShop\Application\Handlers\Templates\Template;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\AsynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\Template\SynchronizeTemplateAsyncCommand;

class SynchronizeTemplateAsyncHandler implements CommandBusAwareInterface, AsynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;

    /**
     * @param SynchronizeTemplateAsyncCommand $command
     */
    public function __invoke(SynchronizeTemplateAsyncCommand $command)
    {
        $this->commandBus->handle($command->getCommand());
    }
}
