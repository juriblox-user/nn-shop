<?php

namespace NnShop\Application\Handlers\Templates\Template;

use Core\SimpleBus\EventBusAwareInterface;
use Core\SimpleBus\EventBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\Template\SetTemplateUnitPriceCommand;
use NnShop\Domain\Templates\Event\TemplateUnitPriceDecrease;
use NnShop\Domain\Templates\Event\TemplateUnitPriceIncrease;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use Money\Currencies\ISOCurrencies;
use Money\Formatter\DecimalMoneyFormatter;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class SetTemplateUnitPriceHandler implements EventBusAwareInterface, LoggerAwareInterface, SynchronousCommandHandlerInterface
{
    use EventBusAwareTrait;
    use LoggerAwareTrait;

    /**
     * @var TemplateRepositoryInterface
     */
    private $repository;

    /**
     * SetTemplateUnitPriceHandler constructor.
     *
     * @param TemplateRepositoryInterface $repository
     */
    public function __construct(TemplateRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param SetTemplateUnitPriceCommand $command
     */
    public function __invoke(SetTemplateUnitPriceCommand $command)
    {
        $template = $this->repository->getById($command->getId());

        $moneyFormatter = new DecimalMoneyFormatter(new ISOCurrencies());

        $this->logger->info('Changing unit price for "{title}" from {oldPrice} to {newPrice}', [
            'id' => $template->getId(),
            'title' => $template->getTitle(),

            'oldPrice' => null !== $template->getUnitPrice() ? $moneyFormatter->format($template->getUnitPrice()) : 'NULL',
            'newPrice' => null !== $command->getPrice() ? $moneyFormatter->format($command->getPrice()) : 'NULL',
        ]);

        // Abort if the price hasn't changed
        if (null !== $template->getUnitPrice() && $template->getUnitPrice()->equals($command->getPrice())) {
            return;
        }

        $oldPrice = $template->getUnitPrice();

        // Change the price
        $template->setUnitPrice($command->getPrice());

        $this->repository->save($template);

        if (null !== $oldPrice) {
            if ($oldPrice->greaterThan($command->getPrice())) {
                $this->eventBus->handle(new TemplateUnitPriceDecrease($template->getId(), $oldPrice));
            } else {
                $this->eventBus->handle(new TemplateUnitPriceIncrease($template->getId(), $oldPrice));
            }
        }
    }
}
