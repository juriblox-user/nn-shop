<?php

namespace NnShop\Application\Handlers\Templates\Template;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\Template\PublishTemplateCommand;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class PublishTemplateHandler implements LoggerAwareInterface, SynchronousCommandHandlerInterface
{
    use LoggerAwareTrait;

    /**
     * @var TemplateRepositoryInterface
     */
    private $repository;

    /**
     * PublishTemplateHandler constructor.
     *
     * @param TemplateRepositoryInterface $repository
     */
    public function __construct(TemplateRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param PublishTemplateCommand $command
     */
    public function __invoke(PublishTemplateCommand $command)
    {
        $template = $this->repository->getById($command->getId());
        $template->setPublished(true);

        $this->logger->info('Publishing template "{title}"', [
            'id' => $template->getId()->getString(),
            'title' => $template->getTitle(),
        ]);

        // Template moet een prijs hebben
        if (null === $template->getUnitPrice()) {
            throw new \DomainException(sprintf('The template "%s" [%s] cannot be published without a unit price', $template->getTitle(), $template->getId()));
        }

        $this->repository->persist($template);
    }
}
