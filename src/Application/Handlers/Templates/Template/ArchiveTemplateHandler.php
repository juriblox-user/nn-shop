<?php

namespace NnShop\Application\Handlers\Templates\Template;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\Template\ArchiveTemplateCommand;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class ArchiveTemplateHandler implements LoggerAwareInterface, SynchronousCommandHandlerInterface
{
    use LoggerAwareTrait;

    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * ArchiveTemplateHandler constructor.
     *
     * @param TemplateRepositoryInterface $templateRepository
     */
    public function __construct(TemplateRepositoryInterface $templateRepository)
    {
        $this->templateRepository = $templateRepository;
    }

    /**
     * @param ArchiveTemplateCommand $command
     */
    public function __invoke(ArchiveTemplateCommand $command)
    {
        $template = $this->templateRepository->getById($command->getId());

        $this->logger->debug('Archiving template "{title}"', [
            'id' => $template->getId()->getString(),
            'title' => $template->getTitle(),
        ]);

        $this->templateRepository->remove($template);
        $this->templateRepository->flush($template);
    }
}
