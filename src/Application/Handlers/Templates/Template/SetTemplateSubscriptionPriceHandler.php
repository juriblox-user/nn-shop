<?php

namespace NnShop\Application\Handlers\Templates\Template;

use Core\SimpleBus\EventBusAwareInterface;
use Core\SimpleBus\EventBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\Template\SetTemplateSubscriptionPriceCommand;
use NnShop\Domain\Templates\Event\TemplateSubscriptionPriceDecrease;
use NnShop\Domain\Templates\Event\TemplateSubscriptionPriceIncrease;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use Money\Currencies\ISOCurrencies;
use Money\Formatter\DecimalMoneyFormatter;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class SetTemplateSubscriptionPriceHandler implements EventBusAwareInterface, LoggerAwareInterface, SynchronousCommandHandlerInterface
{
    use EventBusAwareTrait;
    use LoggerAwareTrait;

    /**
     * @var TemplateRepositoryInterface
     */
    private $repository;

    /**
     * SetTemplateSubscriptionPriceHandler constructor.
     *
     * @param TemplateRepositoryInterface $repository
     */
    public function __construct(TemplateRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param SetTemplateSubscriptionPriceCommand $command
     */
    public function __invoke(SetTemplateSubscriptionPriceCommand $command)
    {
        $template = $this->repository->getById($command->getId());

        $moneyFormatter = new DecimalMoneyFormatter(new ISOCurrencies());

        $this->logger->info('Changing subscription price for "{title}" from {oldPrice} to {newPrice}', [
            'id' => $template->getId(),
            'title' => $template->getTitle(),

            'oldPrice' => null !== $template->getMonthlyPrice() ? $moneyFormatter->format($template->getMonthlyPrice()) : 'NULL',
            'newPrice' => null !== $command->getMonthlyPrice() ? $moneyFormatter->format($command->getMonthlyPrice()) : 'NULL',
        ]);

        // Abort if the price hasn't changed
        if (null !== $template->getMonthlyPrice() && $template->getMonthlyPrice()->equals($command->getMonthlyPrice())) {
            return;
        }

        $oldPrice = $template->getMonthlyPrice();

        // Prijswijziging doorvoeren
        $template->setMonthlyPrice($command->getMonthlyPrice());

        $this->repository->save($template);

        if (null !== $oldPrice) {
            if ($oldPrice->greaterThan($command->getMonthlyPrice())) {
                $this->eventBus->handle(new TemplateSubscriptionPriceDecrease($template->getId(), $oldPrice));
            } else {
                $this->eventBus->handle(new TemplateSubscriptionPriceIncrease($template->getId(), $oldPrice));
            }
        }
    }
}
