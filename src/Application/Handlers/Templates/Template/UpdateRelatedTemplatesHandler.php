<?php

namespace NnShop\Application\Handlers\Templates\Template;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\Template\UpdateRelatedTemplatesCommand;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;

final class UpdateRelatedTemplatesHandler implements CommandBusAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;

    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * UpdateRelatedTemplatesHandler constructor.
     */
    public function __construct(TemplateRepositoryInterface $templateRepository)
    {
        $this->templateRepository = $templateRepository;
    }

    public function __invoke(UpdateRelatedTemplatesCommand $command): void
    {
        $template = $this->templateRepository->getById($command->getId());

        /*
         * Overbodige templates ontkoppelen
         */
        foreach ($template->getRelated() as $related) {
            if (!$command->isRelated($related->getId())) {
                $template->removeRelatedTemplate($related);
            }
        }

        /*
         * Nieuwe templates koppelen
         */
        foreach ($command->getSelected() as $relatedId) {
            $related = $this->templateRepository->getReference($relatedId);

            if (!$template->isRelatedTo($related)) {
                $template->addRelated($related);
            }
        }

        $this->templateRepository->persist($template);
    }
}
