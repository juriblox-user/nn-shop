<?php

namespace NnShop\Application\Handlers\Templates\Template;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Application\AppBundle\Services\Manager\SampleFileManager;
use NnShop\Application\AppBundle\Services\Manager\TemplatePreviewManager;
use NnShop\Domain\Templates\Command\Template\UpdateTemplateCommand;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;

class UpdateTemplateHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var SampleFileManager
     */
    private $sampleManager;

    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * @var TemplatePreviewManager
     */
    private $previewManager;

    /**
     * UpdateTemplateHandler constructor.
     *
     * @param TemplateRepositoryInterface $templateRepository
     * @param SampleFileManager $sampleManager
     * @param TemplatePreviewManager $previewManager
     */
    public function __construct(TemplateRepositoryInterface $templateRepository, SampleFileManager $sampleManager, TemplatePreviewManager $previewManager)
    {
        $this->templateRepository = $templateRepository;
        $this->sampleManager = $sampleManager;
        $this->previewManager = $previewManager;
    }

    /**
     * @param UpdateTemplateCommand $command
     */
    public function __invoke(UpdateTemplateCommand $command)
    {
        $template = $this->templateRepository->getById($command->getId());

        $template->setTitle($command->getTitle() ?: $template->getTitle());
        $template->setDescription($command->getDescription() ?: $template->getDescription());
        $template->setExcerpt($command->getExcerpt() ?: $template->getExcerpt());

        if (null !== $command->isSpotlight()) {
            $template->setSpotlight($command->isSpotlight());
        }

        $template->setUnitPrice($command->getUnitPrice());
        $template->setCheckPrice($command->getCheckPrice());
        $template->setRegisteredEmailPrice($command->getRegisteredEmailPrice());

        $template->setHidden($command->isHidden());

        $template->setService($command->isService());

        if (null !== $command->isSubscription()) {
            $template->setMonthlyPrice($command->isSubscription() ? $command->getMontlyPrice() : null);
        }

        if ($command->hasSample()) {
            $template->setSample(true);

            $this->sampleManager->store($template, $command->getSample());
        }

        if ($command->hasPreview()) {
            $this->previewManager->setPreview($template, $command->getPreview());
        }

        $this->templateRepository->save($template);
    }
}
