<?php

namespace NnShop\Application\Handlers\Templates\Template;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\Template\UpdateLinkedCategoriesCommand;
use NnShop\Domain\Templates\Repository\CategoryRepositoryInterface;
use NnShop\Domain\Templates\Repository\CategoryTemplateRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class UpdateLinkedCategoriesHandler implements CommandBusAwareInterface, LoggerAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;
    use LoggerAwareTrait;

    /**
     * @var CategoryTemplateRepositoryInterface
     */
    private $linkRepository;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * UpdateLinkedCategoriesHandler constructor.
     *
     * @param TemplateRepositoryInterface         $templateRepository
     * @param CategoryRepositoryInterface         $categoryRepository
     * @param CategoryTemplateRepositoryInterface $linkRepository
     */
    public function __construct(TemplateRepositoryInterface $templateRepository, CategoryRepositoryInterface $categoryRepository, CategoryTemplateRepositoryInterface $linkRepository)
    {
        $this->templateRepository = $templateRepository;
        $this->categoryRepository = $categoryRepository;

        $this->linkRepository = $linkRepository;
    }

    /**
     * @param UpdateLinkedCategoriesCommand $command
     */
    public function __invoke(UpdateLinkedCategoriesCommand $command)
    {
        $template = $this->templateRepository->getById($command->getId());

        /*
         * Overbodige templates ontkoppelen
         */
        foreach ($template->getCategories() as $category) {
            if (!$command->hasCategory($category->getId())) {
                $this->logger->info('Removing template [{templateId}] from category "{categoryTitle}"', [
                    'templateId' => $template->getId()->getString(),
                    'categoryId' => $category->getId()->getString(),
                    'categoryTitle' => $category->getTitle(),
                ]);

                $category->removeTemplate($template);

                $this->categoryRepository->persist($category);
            }
        }

        /*
         * Nieuwe templates koppelen
         */
        foreach ($command->getSelected() as $categoryId) {
            $category = $this->categoryRepository->getReference($categoryId);

            if (!$template->hasCategory($category)) {
                $primary = $category->getId() == $command->getPrimary();

                $this->logger->info(sprintf('Adding category "{categoryTitle}" to template [{templateId}], primary = %d', $primary), [
                    'categoryId' => $category->getId()->getString(),
                    'categoryTitle' => $category->getTitle(),
                    'templateId' => $template->getId()->getString(),
                ]);

                if ($primary) {
                    foreach ($this->linkRepository->findByTemplate($template) as $link) {
                        $link->setPrimary(false);

                        $this->linkRepository->persist($link);
                    }
                }

                $link = $category->addTemplate($template);
                $link->setPrimary($primary);

                $this->categoryRepository->persist($category);
            }

            $this->categoryRepository->flush();
        }

        /*
         * Wisselen van primaire categorie
         */
        if ($command->getPrimary() != $template->getPrimaryCategory()->getId()) {
            $this->logger->info('Changing primary category for template [{templateId}] from [{currentId}] to [{updatedId}]', [
                'currentId' => $template->getPrimaryCategory()->getId()->getString(),
                'updatedId' => $command->getPrimary()->getString(),
                'templateId' => $template->getId()->getString(),
            ]);

            // Andere categorieën resetten
            foreach ($this->linkRepository->findByTemplate($template) as $link) {
                $link->setPrimary(false);
                $this->linkRepository->persist($link);
            }

            $category = $this->categoryRepository->getReference($command->getPrimary());

            // Juiste categorie op primair zetten
            $link = $this->linkRepository->getByCategoryAndTemplate($category, $template);
            $link->setPrimary(true);

            $this->categoryRepository->save($category);
        }
    }
}
