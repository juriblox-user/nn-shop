<?php

namespace NnShop\Application\Handlers\Templates\Template;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\EventBusAwareInterface;
use Core\SimpleBus\EventBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use JuriBlox\Sdk\Domain\Documents\Entities\Question as RemoteQuestion;
use NnShop\Application\Traits\JuribloxApiAwareTrait;
use NnShop\Common\Exceptions\CorruptRemoteDataException;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Templates\Command\Template\SynchronizeTemplateCommand;
use NnShop\Domain\Templates\Entity\Question as LocalQuestion;
use NnShop\Domain\Templates\Entity\QuestionCondition;
use NnShop\Domain\Templates\Entity\QuestionOption;
use NnShop\Domain\Templates\Entity\QuestionStep;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Templates\Entity\Update;
use NnShop\Domain\Templates\Enumeration\QuestionType;
use NnShop\Domain\Templates\Event\TemplateWasSynchronized;
use NnShop\Domain\Templates\Repository\AnswerRepositoryInterface;
use NnShop\Domain\Templates\Repository\PartnerRepositoryInterface;
use NnShop\Domain\Templates\Repository\QuestionConditionRepositoryInterface;
use NnShop\Domain\Templates\Repository\QuestionOptionRepositoryInterface;
use NnShop\Domain\Templates\Repository\QuestionRepositoryInterface;
use NnShop\Domain\Templates\Repository\QuestionStepRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use NnShop\Domain\Templates\Services\Generator\TemplateKeyGenerator;
use NnShop\Domain\Templates\Value\QuestionConditionId;
use NnShop\Domain\Templates\Value\QuestionId;
use NnShop\Domain\Templates\Value\QuestionOptionId;
use NnShop\Domain\Templates\Value\QuestionStepId;
use NnShop\Domain\Templates\Value\TemplateId;
use NnShop\Domain\Templates\Value\TemplateVersion;
use NnShop\Domain\Templates\Value\UpdateId;
use NnShop\Infrastructure\Templates\Transformer\QuestionTypeJuribloxTransformer;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class SynchronizeTemplateHandler implements CommandBusAwareInterface, EventBusAwareInterface, LoggerAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;
    use EventBusAwareTrait;
    use LoggerAwareTrait;

    use JuribloxApiAwareTrait;

    /**
     * @var AnswerRepositoryInterface
     */
    private $answerRepository;

    /**
     * @var QuestionConditionRepositoryInterface
     */
    private $conditionRepository;

    /**
     * @var TemplateKeyGenerator
     */
    private $keyGenerator;

    /**
     * @var QuestionOptionRepositoryInterface
     */
    private $optionRepository;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var PartnerRepositoryInterface
     */
    private $partnerRepository;

    /**
     * @var QuestionRepositoryInterface
     */
    private $questionRepository;

    /**
     * @var QuestionStepRepositoryInterface
     */
    private $stepRepository;

    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * SynchronizeTemplateHandler constructor.
     *
     * @param PartnerRepositoryInterface           $partnerRepository
     * @param TemplateRepositoryInterface          $templateRepository
     * @param OrderRepositoryInterface             $orderRepository
     * @param QuestionStepRepositoryInterface      $stepRepository
     * @param QuestionRepositoryInterface          $questionRepository
     * @param QuestionOptionRepositoryInterface    $optionRepository
     * @param QuestionConditionRepositoryInterface $conditionRepository
     * @param AnswerRepositoryInterface            $answerRepository
     * @param TemplateKeyGenerator                 $keyGenerator
     */
    public function __construct(
        PartnerRepositoryInterface $partnerRepository,
        TemplateRepositoryInterface $templateRepository,
        OrderRepositoryInterface $orderRepository,
        QuestionStepRepositoryInterface $stepRepository,
        QuestionRepositoryInterface $questionRepository,
        QuestionOptionRepositoryInterface $optionRepository,
        QuestionConditionRepositoryInterface $conditionRepository,
        AnswerRepositoryInterface $answerRepository,
        TemplateKeyGenerator $keyGenerator)
    {
        $this->partnerRepository = $partnerRepository;
        $this->templateRepository = $templateRepository;
        $this->orderRepository = $orderRepository;

        $this->stepRepository = $stepRepository;
        $this->questionRepository = $questionRepository;
        $this->optionRepository = $optionRepository;
        $this->conditionRepository = $conditionRepository;

        $this->answerRepository = $answerRepository;

        $this->keyGenerator = $keyGenerator;
    }

    /**
     * @param SynchronizeTemplateCommand $command
     */
    public function __invoke(SynchronizeTemplateCommand $command)
    {
        $remoteId = null;

        if ($command->hasPartnerId()) {
            $partner = $this->partnerRepository->getById($command->getPartnerId());
        } else {
            $localTemplate = $this->templateRepository->findOneById($command->getLocalId());
            if (null === $localTemplate) {
                throw new \LogicException('SynchronizeTemplateCommand was invoked without a PartnerId but also without a valid (local) TemplateId');
            }

            $partner = $localTemplate->getPartner();
        }

        // Verbinding maken met JuriBlox
        $this->connect($partner->getClientId(), $partner->getClientKey());

        /** @var TemplateId $localTemplateId */
        $localTemplateId = null;

        $localTemplate = null;
        if (null !== $command->getLocalId()) {
            $localTemplate = $this->templateRepository->getById($command->getLocalId());
        }

        if (null !== $localTemplate) {
            $remoteId = $localTemplate->getRemoteId();
        } elseif (null !== $command->getRemoteId()) {
            $remoteId = $command->getRemoteId();
        } else {
            throw new \LogicException('SynchronizeTemplateCommand was invoked without a RemoteId but also without a (local) TemplateId');
        }

        /*
         * Gegevens bij JuriBlox ophalen
         */
        $remoteTemplate = $this->client->templates()->findOneById($remoteId);

        $this->logger->info('Checking template "{remoteName}" with ID {remoteId}', [
            'remoteId' => $remoteTemplate->getId()->getInteger(),
            'remoteName' => $remoteTemplate->getName(),
        ]);

        $remoteSteps = $this->client->templates()->questionnaire($remoteTemplate->getId())->get();
        if (0 === $remoteSteps->count()) {
            $this->logger->warning('The template "{remoteName}" has no Steps, skipping template', [
                'remoteId' => $remoteTemplate->getId()->getInteger(),
                'remoteName' => $remoteTemplate->getName(),
            ]);

            return;
        }

        $remoteRevision = $remoteTemplate->getRevision();

        /*
         * Nieuw template toevoegen
         */
        if (null === $localTemplate) {
            $localTemplate = $this->templateRepository->findOneByRemoteId($remoteId, $partner);

            if (null === $localTemplate) {
                $localTemplateId = TemplateId::generate();

                $this->logger->info('Creating new template entity {id}', [
                    'id' => $localTemplateId->getString(),
                ]);

                $localTemplate = Template::prepare(TemplateId::generate(), $this->keyGenerator->generate(), TemplateVersion::from($remoteRevision->getVersion()), $partner, $command->getRemoteId());
                $localTemplate->setPublished(false);

                $localTemplate->setTitle($remoteTemplate->getName());
                $localTemplate->setDescription($remoteTemplate->getDescription());

                $localTemplate->setRemoteTitle($remoteTemplate->getName());
                $localTemplate->setRemoteDescription($remoteTemplate->getDescription());

                $localTemplate->updateSyncedTimestamp();

                $this->templateRepository->persist($localTemplate);
            }
        }

        /*
         * Bestaand template bijwerken
         */
        if (null !== $localTemplate) {
            $localTemplateId = $localTemplate->getId();

            // Zo lang het template nog niet is gepubliceerd nemen we de titel en omschrijving gewoon over
            if (!$localTemplate->isPublished()) {
                $localTemplate->setTitle($remoteTemplate->getName());
                $localTemplate->setDescription($remoteTemplate->getDescription());
            }

            $localTemplate->setRemoteTitle($remoteTemplate->getName());
            $localTemplate->setRemoteDescription($remoteTemplate->getDescription());

            $localTemplate->updateSyncedTimestamp();

            // Template update
            if ($remoteRevision->getVersion() > $localTemplate->getVersion()->getInteger()) {
                Update::received(UpdateId::generate(), $localTemplate, TemplateVersion::from($remoteRevision->getVersion()));
            }

            // Template eventueel herstellen
            if ($localTemplate->isDeleted()) {
                $this->logger->info('Restoring template {id}', [
                    'id' => $localTemplateId->getString(),
                ]);

                $localTemplate->restore();
            }

            $this->templateRepository->persist($localTemplate);
        }

        /** @var QuestionId $questionsLookup */
        $questionsLookup = [];

        /** @var QuestionOptionId $optionsLookup */
        $optionsLookup = [];

        /** @var QuestionStep[] $stepPositions */
        $stepPositions = [];

        /** @var LocalQuestion[] $questionPositions */
        $questionPositions = [];

        /** @var LocalQuestion[] $orphanQuestions */
        $orphanQuestions = [];

        /** @var RemoteQuestion[] $remoteQuestions */
        $remoteQuestions = [];

        $seenStepIds = [];
        $seenQuestionIds = [];

        $seenDerivedOfQuestionIds = [];
        $seenDerivedOfOptionIds = [];

        /*
         * -- Stap 1: stappen en vragen synchroniseren, maar zonder condities
         */

        foreach ($remoteSteps as $remoteStep) {
            $localStep = $this->stepRepository->findOneByRemoteId($remoteStep->getId(), $localTemplate);
            $localStepId = null;

            /*
             * Nieuwe stap aanmaken
             */
            if (null === $localStep) {
                $localStepId = QuestionStepId::generate();

                $this->logger->debug('Creating question step "{title}"', [
                    'remoteId' => $remoteStep->getId()->getInteger(),
                    'title' => $remoteStep->getName(),
                ]);

                $localStep = QuestionStep::create($localStepId, $localTemplate, $remoteStep->getName(), $remoteStep->getId());
                $localStep->setDescription($remoteStep->getDescription());

                $this->stepRepository->persist($localStep);

                $stepPositions[] = $localStep;
            }

            /*
             * Bestaande stap bijwerken
             */
            else {
                $localStepId = $localStep->getId();

                /*
                 * Stap eventueel verhuizen naar het juiste template
                 */
                if ($localStep->getTemplate()->getId()->notEquals($localTemplate->getId())) {
                    $this->logger->info('Moving question step "{title}" from template {currentId} to template {updatedId}', [
                        'remoteId' => $remoteStep->getId()->getInteger(),
                        'title' => $remoteStep->getName(),

                        'currentId' => $localStep->getTemplate()->getId()->getString(),
                        'updatedId' => $localTemplate->getId()->getString(),
                    ]);

                    // Vragen die nu worden losgekoppeld kunnen wezen worden
                    foreach ($localStep->getQuestions() as $conditionsQuestion) {
                        $orphanQuestions[$conditionsQuestion->getId()->getString()] = $conditionsQuestion;
                    }

                    // Stap verplaatsen
                    $localStep->move($localTemplate);
                }

                $this->logger->debug('Updating question step "{title}"', [
                    'remoteId' => $remoteStep->getId()->getInteger(),
                    'title' => $remoteStep->getName(),
                ]);

                $localStep->setTitle($remoteStep->getName());
                $localStep->setDescription($remoteStep->getDescription());

                $this->stepRepository->persist($localStep);

                $stepPositions[] = $localStep;
            }

            /*
             * -- Vragen synchroniseren
             */
            foreach ($remoteStep->getQuestions() as $remoteQuestion) {
                $localQuestion = $this->questionRepository->findOneByLookupId($remoteQuestion->getFirstId(), $localStep);
                $localQuestionId = null;

                $ignoredTypes = [
                    QuestionType::TYPE_INFOBOX,
                ];

                if (in_array($remoteQuestion->getType()->getType(), $ignoredTypes)) {
                    continue;
                }

                $questionType = QuestionTypeJuriBloxTransformer::fromRemote($remoteQuestion->getType());

                /*
                 * Nieuwe vraag toevoegen
                 */
                if (null === $localQuestion) {
                    $localQuestionId = QuestionId::generate();

                    $this->logger->debug('Creating question "{title}"', [
                        'lookupId' => $remoteQuestion->getFirstId()->getInteger(),
                        'remoteId' => $remoteQuestion->getId()->getInteger(),
                        'title' => $remoteQuestion->getName(),
                    ]);

                    $localQuestion = LocalQuestion::create($localQuestionId, $localStep, $questionType, $remoteQuestion->getName(), $remoteQuestion->getFirstId(), $remoteQuestion->getId());
                    $localQuestion->setHelp($remoteQuestion->getInfo());
                    $localQuestion->setRequired($remoteQuestion->isRequired());

                    $this->questionRepository->persist($localQuestion);

                    $questionPositions[$localStepId->getString()][] = $localQuestion;
                }

                /*
                 * Bestaande vraag bijwerken
                 */
                else {
                    $localQuestionId = $localQuestion->getId();

                    // Controleren of het ID niet dubbel wordt gebruikt
                    if (in_array($remoteQuestion->getId()->getInteger(), $seenDerivedOfQuestionIds)) {
                        throw new CorruptRemoteDataException(sprintf('Question "%s" (remote ID %d) in Template#%s has the lookupId %s, but this ID has been seen for another question in this Template', $remoteQuestion->getName(), $remoteQuestion->getId()->getInteger(), $localTemplate->getId(), $remoteQuestion->getFirstId()->getInteger()));
                    }

                    // Vraag eventueel herstellen
                    if ($localQuestion->isDeleted()) {
                        $this->logger->info('Restoring question {id}', [
                            'id' => $localQuestionId->getString(),
                        ]);

                        $localQuestion->restore();
                    }

                    // Vraag verplaatsen naar een andere stap
                    if (null === $localQuestion->getStep() || $localQuestion->getStep()->getId()->notEquals($localStepId)) {
                        $this->logger->notice('Moving question from "{currentTitle}" to "{updatedTitle}"', [
                            'id' => $localQuestion->getId()->getString(),
                            'title' => $remoteStep->getName(),

                            'remoteId' => $remoteQuestion->getId()->getInteger(),

                            'currentId' => null === $localQuestion->getStep() ? 'NULL' : $localQuestion->getStep()->getId()->getString(),
                            'currentTitle' => null === $localQuestion->getStep() ? 'NULL' : $localQuestion->getStep()->getTitle(),

                            'updatedId' => $localStepId->getString(),
                            'updatedTitle' => $localStep->getTitle(),
                        ]);

                        $localQuestion->move($localStep);

                        unset($orphanQuestions[$localQuestionId->getString()]);
                    }

                    /*
                     * Vraagtype wijzigen
                     */
                    if ($localQuestion->getType()->isNot($questionType)) {
                        $this->logger->info('Converting QuestionType for "{title}" from {currentType} to {updatedType}', [
                            'id' => $localQuestion->getId()->getString(),
                            'title' => $localQuestion->getTitle(),

                            'currentType' => $localQuestion->getType()->getName(),
                            'updatedType' => $questionType->getName(),
                        ]);

                        $this->logger->debug('Allowed implicit conversions for "{type}" are {allowed}. Result: {result}', [
                            'type' => $localQuestion->getType()->getValue(),
                            'allowed' => implode(', ', $localQuestion->getType()->getAllowedConversions()),
                            'result' => $localQuestion->getType()->isAllowedImplicitConversion($questionType),
                        ]);

                        /*
                         * Conversie: TYPE_CHOICE -> *
                         */
                        if ($localQuestion->getType()->is(QuestionType::TYPE_CHOICE) && !$localQuestion->getType()->isAllowedImplicitConversion($questionType)) {
                            foreach ($this->answerRepository->findByQuestion($localQuestion) as $answer) {
                                $optionValues = [];
                                foreach ($answer->getOptions() as $option) {
                                    $optionValues[] = $option->getValue();
                                }

                                $answer->clear();
                                $answer->setValue(implode(', ', $optionValues));

                                $this->answerRepository->save($answer);
                                $this->answerRepository->detach($answer);

                                unset($answer);
                            }

                            $localQuestion->setType($questionType);
                        }

                        // Als de conversie niet wordt ondersteund slaan we de vraag gewoon over
                        elseif (!$localQuestion->getType()->isAllowedImplicitConversion($questionType)) {
                            $this->logger->error('Cannot convert question from "{fromType}" to "{toType}". Allowed: {allowed}', [
                                'fromType' => $localQuestion->getType()->getValue(),
                                'toType' => $questionType->getValue(),
                                'allowed' => implode(', ', $localQuestion->getType()->getAllowedConversions()),
                            ]);

                            continue;
                        }

                        // Andere conversies
                        else {
                            $localQuestion->convert($questionType);
                        }
                    }

                    $this->logger->debug('Updating step "{title}"', [
                        'remoteId' => $remoteStep->getId()->getInteger(),
                        'title' => $remoteStep->getName(),
                    ]);

                    $localQuestion->setRemoteId($remoteQuestion->getId());

                    $localQuestion->setTitle($remoteQuestion->getName());
                    $localQuestion->setHelp($remoteQuestion->getInfo());
                    $localQuestion->setRequired($remoteQuestion->isRequired());

                    // Opslaan mét flush zodat een eventuele Question::move() ook effect heeft
                    $this->questionRepository->save($localQuestion);

                    $questionPositions[$localStepId->getString()][] = $localQuestion;
                }

                // Lookup tabel RemoteQuestionId -> QuestionId
                $questionsLookup[$remoteQuestion->getId()->getInteger()] = $localQuestion;
                $questionsLookup[$remoteQuestion->getFirstId()->getInteger()] = $localQuestion;

                /*
                 * -- Opties synchroniseren
                 */

                /** @var QuestionOption[] $optionPositions */
                $optionPositions = [];

                $seenOptionIds = [];
                foreach ($remoteQuestion->getOptions() as $remoteOption) {
                    $localOption = $this->optionRepository->findOneByLookupId($remoteOption->getFirstId(), $localQuestion);
                    $localOptionId = null;

                    /*
                     * Nieuwe optie toevoegen
                     */
                    if (null === $localOption) {
                        $localOptionId = QuestionOptionId::generate();

                        $this->logger->debug('Adding question option "{value}" to question "{title}', [
                            'lookupId' => $remoteOption->getFirstId()->getInteger(),
                            'remoteId' => $remoteOption->getId()->getInteger(),
                            'value' => $remoteOption->getValue(),
                            'title' => $remoteQuestion->getName(),
                        ]);

                        $localOption = QuestionOption::create($localOptionId, $localQuestion, $remoteOption->getValue(), $remoteOption->getFirstId(), $remoteOption->getId());
                        $localOption->setTitle($remoteOption->getTitle());
                        $localOption->setDefault($remoteOption->isDefault());

                        $this->optionRepository->persist($localOption);

                        $optionPositions[] = $localOption;
                    }

                    /*
                     * Bestaande optie bijwerken
                     */
                    else {
                        $localOptionId = $localOption->getId();

                        // Controleren of het ID niet dubbel wordt gebruikt
                        if (in_array($remoteQuestion->getId()->getInteger(), $seenDerivedOfOptionIds)) {
                            throw new CorruptRemoteDataException(sprintf('The option with remote ID %d in question "%s" (remote ID %d) and Template#%s has the lookupId %s, but this ID has been seen for another question in this Template', $remoteOption->getId()->getInteger(), $remoteQuestion->getName(), $remoteQuestion->getId()->getInteger(), $localTemplate->getId(), $remoteQuestion->getFirstId()->getInteger()));
                        }

                        $this->logger->debug('Updating question option "{value}"', [
                            'lookupId' => $remoteOption->getFirstId()->getInteger(),
                            'remoteId' => $remoteOption->getId()->getInteger(),
                            'value' => $remoteOption->getValue(),
                        ]);

                        $localOption->setRemoteId($remoteOption->getId());

                        $localOption->setTitle($remoteOption->getTitle());
                        $localOption->setValue($remoteOption->getValue());

                        $localOption->setDefault($remoteOption->isDefault());

                        /*
                         * Controleren of de relatie naar de Question goed ligt
                         */
                        if ($localOption->getQuestion()->getId()->notEquals($localQuestion->getId())) {
                            $this->logger->notice('Moving QuestionOption {optionId} from Question {currentQuestionId} to {updatedQuestionId}', [
                                'optionId' => $localOption->getId()->getString(),
                                'remoteOptionId' => $remoteOption->getId()->getInteger(),

                                'currentQuestionId' => $localOption->getQuestion()->getId()->getString(),
                                'updatedQuestionId' => $localQuestion->getId()->getString(),
                            ]);

                            $localOption->move($localQuestion);
                        }

                        $this->optionRepository->persist($localOption);

                        $optionPositions[] = $localOption;
                    }

                    // Lookup tabel RemoteQuestionId -> QuestionId
                    $optionsLookup[$remoteOption->getId()->getInteger()] = $localOption;
                    $optionsLookup[$remoteOption->getFirstId()->getInteger()] = $localOption;

                    $seenOptionIds[] = $localOptionId->getString();
                    $seenDerivedOfOptionIds[] = $remoteOption->getFirstId()->getInteger();
                }

                /*
                 * Overbodige opties verwijderen
                 */
                if (null !== $localQuestion) {
                    // Als we deze optie niet hebben gezien -> verwijderen
                    foreach ($localQuestion->getVisibleOptions() as $deleteOption) {
                        if (in_array($deleteOption->getId()->getString(), $seenOptionIds)) {
                            continue;
                        }

                        $this->logger->info('Deleting question option "{value}"', [
                            'id' => $deleteOption->getId()->getString(),
                            'value' => $deleteOption->getValue(),
                        ]);

                        // Verwijderen zonder detach zodat de (soft-deleted) relaties kunnen blijven bestaan
                        if (!$deleteOption->isDeleted()) {
                            $this->optionRepository->remove($deleteOption);
                        }
                    }
                }

                if ($localQuestion->getType()->usesOptions() && 0 === count($localQuestion->getVisibleOptions())) {
                    throw new CorruptRemoteDataException(sprintf('Question#%s ("%s") with remote ID %d in Template#%s ("%s") should be having options but none are available', $localQuestion->getId(), $localQuestion->getTitle(), $remoteQuestion->getId()->getInteger(), $localTemplate->getId(), $localTemplate->getTitle()));
                }

                /*
                 * Opties op de juiste posities zetten
                 */
                foreach ($optionPositions as $position => $reorderPosition) {
                    $reorderPosition->setPosition($position);

                    $this->optionRepository->persist($reorderPosition);
                }

                $remoteQuestions[] = $remoteQuestion;

                $seenQuestionIds[] = $localQuestionId->getString();
                $seenDerivedOfQuestionIds[] = $remoteQuestion->getFirstId()->getInteger();
            }

            $seenStepIds[] = $localStepId->getString();
        }

        /*
         * Vragen verwijderen die aan dit template hingen, maar niet meer zijn langsgekomen
         */
        $checkQuestions = $orphanQuestions;

        // Alle vragen die (via een stap) aan het template hingen nemen we mee
        if (null !== $localTemplate) {
            $checkQuestions = array_merge($checkQuestions, $this->questionRepository->findByTemplate($localTemplate));
        }

        /** @var LocalQuestion $orphanQuestion */
        foreach ($checkQuestions as $orphanQuestion) {
            if (in_array($orphanQuestion->getId()->getString(), $seenQuestionIds)) {
                continue;
            }

            $this->logger->info('Removing orphaned question "{title}"', [
                'id' => $orphanQuestion->getId()->getString(),
                'title' => $orphanQuestion->getTitle(),
            ]);

            $orphanQuestion->detach();

            // Eerst opslaan mét flush zodat Question::$step op NULL staat
            $this->questionRepository->save($orphanQuestion);

            if (!$orphanQuestion->isDeleted()) {
                $this->questionRepository->remove($orphanQuestion);
            }
        }

        /*
         * Vragen op de juiste posities zetten
         */
        foreach ($questionPositions as $stepId => $positions) {
            /** @var $reorderQuestion LocalQuestion */
            foreach ($positions as $position => $reorderQuestion) {
                $this->logger->debug('Setting question {id} to position {position}', [
                    'id' => $reorderQuestion->getId()->getString(),
                    'position' => $position,
                ]);

                $reorderQuestion->setPosition($position);

                $this->questionRepository->persist($reorderQuestion);
            }
        }

        /*
         * Overbodige stappen verwijderen
         */
        if (null !== $localTemplate) {
            // Als we deze stap niet hebben gezien -> verwijderen
            foreach ($localTemplate->getQuestionSteps() as $deleteStep) {
                if (in_array($deleteStep->getId()->getString(), $seenStepIds)) {
                    continue;
                }

                // Aangezien dit een echte DELETE FROM actie wordt, moeten we wel zorgen dat alles in sync is met de database
                $this->stepRepository->refresh($deleteStep);

                $this->logger->info('Deleting question step "{title}"', [
                    'id' => $deleteStep->getId()->getString(),
                    'title' => $deleteStep->getTitle(),
                    'questions' => count($deleteStep->getQuestions()),
                ]);

                // Vragen die niet zijn verplaatst naar een andere stap hangen nu nog altijd onder de stap
                foreach ($deleteStep->getQuestions() as $deleteQuestion) {
                    $deleteQuestion->detach();

                    // Eerst opslaan mét flush zodat Question::$step op NULL staat
                    $this->questionRepository->save($deleteQuestion);

                    if (!$deleteQuestion->isDeleted()) {
                        $this->questionRepository->remove($deleteQuestion);
                    }
                }

                // Orders die op deze stap zaten loskoppelen
                foreach ($this->orderRepository->findByStep($deleteStep) as $order) {
                    $order->unlinkStep();

                    // Opslaan mét flush zodat Order::$step op NULL staat
                    $this->orderRepository->save($order);
                }

                $deleteStep->detach();

                $this->stepRepository->remove($deleteStep);
            }
        }

        /*
         * Stappen op de juiste posities zetten
         */
        foreach ($stepPositions as $position => $reorderStep) {
            $reorderStep->setPosition($position);

            $this->stepRepository->persist($reorderStep);
        }

        /*
         * -- Stap 2: condities opslaan
         */
        foreach ($remoteQuestions as $remoteQuestion) {
            /** @var LocalQuestion $conditionsQuestion */
            $conditionsQuestion = $questionsLookup[$remoteQuestion->getId()->getInteger()];

            $this->logger->info('Updating conditions for question "{title}"', [
                'id' => $conditionsQuestion->getId()->getString(),
                'title' => $conditionsQuestion->getTitle(),
            ]);

            // Alle bestaande condities wissen
            foreach ($conditionsQuestion->getConditions() as $condition) {
                $conditionsQuestion->removeCondition($condition);

                $this->conditionRepository->remove($condition);
            }

            // Condities op basis van andere vragen
            foreach ($remoteQuestion->getQuestionConditions() as $remoteQuestionCondition) {
                if (!isset($questionsLookup[$remoteQuestionCondition->getId()->getInteger()])) {
                    $this->logger->error('Remote question "{questionName}" in template "{templateTitle}" has a condition based on the remote parent question {remoteQuestionId}, but this question is not available', [
                        'questionId' => $remoteQuestion->getId()->getInteger(),
                        'questionName' => $remoteQuestion->getName(),
                        'templateId' => $localTemplate->getId()->getString(),
                        'templateTitle' => $localTemplate->getTitle(),
                        'remoteQuestionId' => $remoteQuestionCondition->getId()->getInteger(),
                    ]);

                    continue;
                }

                $condition = QuestionCondition::createWithParent(QuestionConditionId::generate(), $conditionsQuestion, $questionsLookup[$remoteQuestionCondition->getId()->getInteger()]);

                $this->conditionRepository->persist($condition);
            }

            // Condities op basis van opties
            foreach ($remoteQuestion->getOptionConditions() as $remoteOptionCondition) {
                if (!isset($optionsLookup[$remoteOptionCondition->getId()->getInteger()])) {
                    $this->logger->error('Remote question "{questionName}" in template "{templateTitle}" has a condition based on the remote option {remoteOptionId}, but this option is not available', [
                        'questionId' => $remoteQuestion->getId()->getInteger(),
                        'questionName' => $remoteQuestion->getName(),
                        'templateId' => $localTemplate->getId()->getString(),
                        'templateTitle' => $localTemplate->getTitle(),
                        'remoteOptionId' => $remoteOptionCondition->getId()->getInteger(),
                    ]);

                    continue;
                }

                $condition = QuestionCondition::createWithOption(QuestionConditionId::generate(), $conditionsQuestion, $optionsLookup[$remoteOptionCondition->getId()->getInteger()]);

                $this->conditionRepository->persist($condition);
            }
        }

        $this->logger->info('Done synchronizing template, completing transaction', [
            'templateId' => $localTemplateId->getString(),
        ]);

        $this->eventBus->handle(new TemplateWasSynchronized($localTemplateId));
    }
}
