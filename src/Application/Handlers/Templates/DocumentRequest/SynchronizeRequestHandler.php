<?php

namespace NnShop\Application\Handlers\Templates\DocumentRequest;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use JuriBlox\Sdk\Domain\Documents\Values\DocumentStatus as RemoteDocumentStatus;
use NnShop\Application\Traits\JuribloxApiAwareTrait;
use NnShop\Domain\Templates\Command\DocumentRequest\ProcessFailedRequestCommand;
use NnShop\Domain\Templates\Command\DocumentRequest\ProcessSuccessfulRequestCommand;
use NnShop\Domain\Templates\Command\DocumentRequest\SynchronizeRequestCommand;
use NnShop\Domain\Templates\Enumeration\DocumentStatus;
use NnShop\Domain\Templates\Repository\DocumentRequestRepositoryInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class SynchronizeRequestHandler implements CommandBusAwareInterface, LoggerAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;
    use JuribloxApiAwareTrait;
    use LoggerAwareTrait;

    /**
     * @var DocumentRequestRepositoryInterface
     */
    private $requestRepository;

    /**
     * SynchronizeRequestHandler constructor.
     *
     * @param DocumentRequestRepositoryInterface $requestRepository
     */
    public function __construct(DocumentRequestRepositoryInterface $requestRepository)
    {
        $this->requestRepository = $requestRepository;
    }

    /**
     * @param SynchronizeRequestCommand $command
     */
    public function __invoke(SynchronizeRequestCommand $command)
    {
        $request = $this->requestRepository->getById($command->getId());
        if (!$request->isPending()) {
            return;
        }

        $document = $request->getDocument();
        if (DocumentStatus::PENDING != $document->getStatus()) {
            throw new \LogicException('The document should have the PENDING status');
        }

        /*
         * Als we geen RemoteId hebben dan houdt het hier sowieso op
         */
        if (null == $request->getRemoteId()) {
            $this->commandBus->handle(
                ProcessFailedRequestCommand::create($request->getId())
            );

            return;
        }

        /*
         * Status ophalen bij JuriBlox
         */
        $this->connect($document->getPartner()->getClientId(), $document->getPartner()->getClientKey());

        $status = $this->client->documents()->getRequestStatus($request->getRemoteId());

        $this->logger->info('Requested current state for request [{requestId}], result: {statusName} [{statusCode}]', [
            'requestId' => $request->getId()->getString(),
            'statusName' => $status->getStatus()->getName(),
            'statusCode' => $status->getStatus()->getCode(),
        ]);

        // Request is succesvol afgerond
        if (RemoteDocumentStatus::STATUS_GENERATED == $status->getStatus()->getCode()) {
            $this->commandBus->handle(
                ProcessSuccessfulRequestCommand::create($request->getId(), $status->getDocumentId())
            );
        }

        // Request is mislukt
        elseif (RemoteDocumentStatus::STATUS_FAILED == $status->getStatus()->getCode()) {
            $this->commandBus->handle(
                ProcessFailedRequestCommand::create($request->getId())
            );
        }
    }
}
