<?php

namespace NnShop\Application\Handlers\Templates\DocumentRequest;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\DocumentRequest\SynchronizePendingRequestsCommand;
use NnShop\Domain\Templates\Command\DocumentRequest\SynchronizeRequestCommand;
use NnShop\Domain\Templates\Repository\DocumentRepositoryInterface;

class SynchronizePendingRequestsHandler implements CommandBusAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;

    /**
     * @var DocumentRepositoryInterface
     */
    private $documentRepository;

    /**
     * SynchronizePendingRequestsHandler constructor.
     *
     * @param DocumentRepositoryInterface $documentRepository
     */
    public function __construct(DocumentRepositoryInterface $documentRepository)
    {
        $this->documentRepository = $documentRepository;
    }

    /**
     * @param SynchronizePendingRequestsCommand $command
     */
    public function __invoke(SynchronizePendingRequestsCommand $command)
    {
        $document = $this->documentRepository->getById($command->getDocumentId());

        foreach ($document->getPendingRequests() as $request) {
            $this->commandBus->handle(SynchronizeRequestCommand::create($request->getId()));
        }
    }
}
