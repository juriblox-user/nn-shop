<?php

namespace NnShop\Application\Handlers\Templates\DocumentRequest;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\Document\CacheDocumentCommand;
use NnShop\Domain\Templates\Command\DocumentRequest\ProcessSuccessfulRequestCommand;
use NnShop\Domain\Templates\Repository\DocumentRepositoryInterface;
use NnShop\Domain\Templates\Repository\DocumentRequestRepositoryInterface;

class ProcessSuccessfulRequestHandler implements CommandBusAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;

    /**
     * @var DocumentRequestRepositoryInterface
     */
    private $requestRepository;

    /**
     * @var DocumentRepositoryInterface
     */
    private $documentRepository;

    /**
     * SetRequestFailedStatusHandler constructor.
     *
     * @param DocumentRequestRepositoryInterface $requestRepository
     * @param DocumentRepositoryInterface        $documentRepository
     */
    public function __construct(DocumentRequestRepositoryInterface $requestRepository, DocumentRepositoryInterface $documentRepository)
    {
        $this->requestRepository = $requestRepository;
        $this->documentRepository = $documentRepository;
    }

    /**
     * @param ProcessSuccessfulRequestCommand $command
     */
    public function __invoke(ProcessSuccessfulRequestCommand $command)
    {
        $request = $this->requestRepository->getById($command->getRequestId());
        $document = $request->getDocument();

        // Request is afgerond
        $request->setCompleted($command->getRemoteDocumentId());

        $this->commandBus->handle(
            CacheDocumentCommand::create($document->getId())
        );

        $this->requestRepository->persist($request);
        $this->documentRepository->persist($document);
    }
}
