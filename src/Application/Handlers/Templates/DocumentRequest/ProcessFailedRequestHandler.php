<?php

namespace NnShop\Application\Handlers\Templates\DocumentRequest;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Templates\Command\DocumentRequest\ProcessFailedRequestCommand;
use NnShop\Domain\Templates\Repository\DocumentRepositoryInterface;
use NnShop\Domain\Templates\Repository\DocumentRequestRepositoryInterface;

class ProcessFailedRequestHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var DocumentRequestRepositoryInterface
     */
    private $requestRepository;

    /**
     * @var DocumentRepositoryInterface
     */
    private $documentRepository;

    /**
     * SetRequestFailedStatusHandler constructor.
     *
     * @param DocumentRequestRepositoryInterface $requestRepository
     * @param DocumentRepositoryInterface        $documentRepository
     */
    public function __construct(DocumentRequestRepositoryInterface $requestRepository, DocumentRepositoryInterface $documentRepository)
    {
        $this->requestRepository = $requestRepository;
        $this->documentRepository = $documentRepository;
    }

    /**
     * @param ProcessFailedRequestCommand $command
     */
    public function __invoke(ProcessFailedRequestCommand $command)
    {
        $request = $this->requestRepository->getById($command->getId());
        $request->setFailed();

        $this->requestRepository->persist($request);
        $this->documentRepository->persist($request->getDocument());
    }
}
