<?php

namespace NnShop\Application\Handlers\System\Counter;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\System\Command\Counter\CreateCounterCommand;
use NnShop\Domain\System\Entity\Counter;
use NnShop\Domain\System\Repository\CounterRepositoryInterface;

class CreateCounterHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CounterRepositoryInterface
     */
    private $repository;

    /**
     * CreateCounterHandler constructor.
     *
     * @param CounterRepositoryInterface $repository
     */
    public function __construct(CounterRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param CreateCounterCommand $command
     */
    public function __invoke(CreateCounterCommand $command)
    {
        $counter = Counter::create($command->getId(), $command->getName());

        $this->repository->persist($counter);
    }
}
