<?php

namespace NnShop\Application\Handlers\System\Counter;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\System\Command\Counter\IncreaseCounterCommand;
use NnShop\Domain\System\Repository\CounterRepositoryInterface;

class IncreaseCounterHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CounterRepositoryInterface
     */
    private $repository;

    /**
     * IncreaseCounterHandler constructor.
     *
     * @param CounterRepositoryInterface $repository
     */
    public function __construct(CounterRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param IncreaseCounterCommand $command
     */
    public function __invoke(IncreaseCounterCommand $command)
    {
        if ($command->hasName()) {
            $counter = $this->repository->getByName($command->getName());
        } elseif ($command->hasId()) {
            $counter = $this->repository->getById($command->getId());
        } else {
            throw new \InvalidArgumentException('The IncreaseCounterHandler needs either a CounterId or a CounterName');
        }

        $counter->increase();

        $this->repository->persist($counter);
    }
}
