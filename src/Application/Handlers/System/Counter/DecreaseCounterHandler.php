<?php

namespace NnShop\Application\Handlers\System\Counter;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\System\Command\Counter\DecreaseCounterCommand;
use NnShop\Domain\System\Repository\CounterRepositoryInterface;

class DecreaseCounterHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CounterRepositoryInterface
     */
    private $repository;

    /**
     * DecreaseCounterHandler constructor.
     *
     * @param CounterRepositoryInterface $repository
     */
    public function __construct(CounterRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param DecreaseCounterCommand $command
     */
    public function __invoke(DecreaseCounterCommand $command)
    {
        if ($command->hasName()) {
            $counter = $this->repository->getByName($command->getName());
        } elseif ($command->hasId()) {
            $counter = $this->repository->getById($command->getId());
        } else {
            throw new \InvalidArgumentException('The IncreaseCounterHandler needs either a CounterId or a CounterName');
        }

        $counter->decrease();

        $this->repository->persist($counter);
    }
}
