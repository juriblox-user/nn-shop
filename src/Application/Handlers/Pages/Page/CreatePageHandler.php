<?php

namespace NnShop\Application\Handlers\Pages\Page;

use Core\Domain\RepositoryInterface;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Application\AppBundle\Services\Manager\PagePdfFileManager;
use NnShop\Domain\Pages\Command\Page\CreatePageCommand;
use NnShop\Domain\Pages\Entity\Page;
use NnShop\Domain\Pages\Repository\PageRepositoryInterface;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;

/**
 * Class CreatePageHandler.
 */
class CreatePageHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var PagePdfFileManager
     */
    private $fileManager;

    /**
     * @var RepositoryInterface
     */
    private $repository;

    /**
     * @var ProfileRepositoryInterface
     */
    private $profileRepository;

    /**
     * CreatePageHandler constructor.
     *
     * @param PageRepositoryInterface    $repository
     * @param PagePdfFileManager         $fileManager
     * @param ProfileRepositoryInterface $profileRepository
     */
    public function __construct(PageRepositoryInterface $repository, PagePdfFileManager $fileManager, ProfileRepositoryInterface $profileRepository)
    {
        $this->repository = $repository;
        $this->fileManager = $fileManager;
        $this->profileRepository = $profileRepository;
    }

    /**
     * @param CreatePageCommand $command
     *
     * @throws \Core\Common\Exception\Domain\DuplicateEntityException
     * @throws \InvalidArgumentException
     */
    public function __invoke(CreatePageCommand $command)
    {
        $profile = $this->profileRepository->getById($command->getProfileId());
        $page = Page::create($command->getId(), $profile);

        $page->setCode($command->getCode());
        $page->setTitle($command->getTitle());
        $page->setLead($command->getLead());
        $page->setContent($command->getContent());
        $page->setFooter($command->getFooter());
        $page->setLastUpdated($command->getLastUpdated());

        if ($command->hasPdf()) {
            $this->fileManager->store($page, $command->getPdf());
        }

        $this->repository->persist($page);
    }
}
