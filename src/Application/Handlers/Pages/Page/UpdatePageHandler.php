<?php

namespace NnShop\Application\Handlers\Pages\Page;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Application\AppBundle\Services\Manager\PagePdfFileManager;
use NnShop\Domain\Pages\Command\Page\UpdatePageCommand;
use NnShop\Domain\Pages\Repository\PageRepositoryInterface;

/**
 * Class UpdatePageHandler.
 */
class UpdatePageHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var PagePdfFileManager
     */
    private $fileManager;

    /**
     * @var PageRepositoryInterface
     */
    private $repository;

    /**
     * UpdatePageHandler constructor.
     *
     * @param PageRepositoryInterface $repository
     * @param PagePdfFileManager      $fileManager
     */
    public function __construct(PageRepositoryInterface $repository, PagePdfFileManager $fileManager)
    {
        $this->repository = $repository;
        $this->fileManager = $fileManager;
    }

    /**
     * @param UpdatePageCommand $command
     *
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     * @throws \Gaufrette\Exception\FileNotFound
     */
    public function __invoke(UpdatePageCommand $command)
    {
        $page = $this->repository->getById($command->getId());

        $page->setCode($command->getCode());
        $page->setTitle($command->getTitle());
        $page->setLead($command->getLead());
        $page->setContent($command->getContent());
        $page->setFooter($command->getFooter());
        $page->setLastUpdated($command->getLastUpdated());

        if ($command->hasPdf()) {
            $this->fileManager->store($page, $command->getPdf());
        } elseif ($command->deletePdf()) {
            $this->fileManager->delete($page);
        }

        $this->repository->persist($page);
    }
}
