<?php

namespace NnShop\Application\Handlers\Pages\Page;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Application\AppBundle\Services\Manager\PagePdfFileManager;
use NnShop\Domain\Pages\Command\Page\DeletePageCommand;
use NnShop\Domain\Pages\Repository\PageRepositoryInterface;

/**
 * Class DeletePageHandler.
 */
class DeletePageHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var PagePdfFileManager
     */
    private $fileManager;

    /**
     * @var PageRepositoryInterface
     */
    private $repository;

    /**
     * DeletePageHandler constructor.
     *
     * @param PageRepositoryInterface $repository
     * @param PagePdfFileManager      $fileManager
     */
    public function __construct(PageRepositoryInterface $repository, PagePdfFileManager $fileManager)
    {
        $this->repository = $repository;
        $this->fileManager = $fileManager;
    }

    /**
     * @param DeletePageCommand $command
     *
     * @throws \Exception
     */
    public function __invoke(DeletePageCommand $command)
    {
        $page = $this->repository->getById($command->getId());

        if ($page->getPdf()) {
            $this->fileManager->delete($page);
        }
        $this->repository->remove($page);
    }
}
