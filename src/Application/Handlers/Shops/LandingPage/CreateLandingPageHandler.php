<?php

namespace NnShop\Application\Handlers\Shops\LandingPage;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Repository\DiscountRepositoryInterface;
use NnShop\Domain\Shops\Command\LandingPage\CreateLandingPageCommand;
use NnShop\Domain\Shops\Entity\LandingPage;
use NnShop\Domain\Shops\Repository\LandingPageRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;

class CreateLandingPageHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var DiscountRepositoryInterface
     */
    private $discountRepository;

    /**
     * @var LandingPageRepositoryInterface
     */
    private $pageRepository;

    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * @var ProfileRepositoryInterface
     */
    private $profileRepository;

    /**
     * CreateLandingPageHandler constructor.
     *
     * @param LandingPageRepositoryInterface $pageRepository
     * @param DiscountRepositoryInterface    $discountRepository
     * @param TemplateRepositoryInterface    $templateRepository
     * @param ProfileRepositoryInterface     $profileRepository
     */
    public function __construct(LandingPageRepositoryInterface $pageRepository, DiscountRepositoryInterface $discountRepository, TemplateRepositoryInterface $templateRepository, ProfileRepositoryInterface $profileRepository)
    {
        $this->pageRepository = $pageRepository;
        $this->discountRepository = $discountRepository;
        $this->templateRepository = $templateRepository;
        $this->profileRepository = $profileRepository;
    }

    /**
     * @param CreateLandingPageCommand $command
     */
    public function __invoke(CreateLandingPageCommand $command)
    {
        $profile = $this->profileRepository->getById($command->getProfileId());

        $landingPage = LandingPage::create($command->getId(), $command->getTitle(), $profile);
        $landingPage->setText($command->getText());

        /*
         * Kortingscode koppelen
         */
        if ($command->hasDiscount()) {
            if ($landingPage->hasDiscount()) {
                $landingPage->unlinkDiscount();
            }

            $landingPage->linkDiscount($this->discountRepository->getReference($command->getDiscountId()));
        } else {
            $landingPage->unlinkDiscount();
        }

        /*
         * Template(s) koppelen
         */
        $seenIds = [];
        foreach ($command->getTemplates() as $templateId) {
            $template = $this->templateRepository->getReference($templateId);
            if (!$landingPage->hasTemplate($template)) {
                $landingPage->addTemplate($template);
            }

            $seenIds[] = $templateId->getString();
        }

        // Overbodige koppelingen opschonen
        foreach ($landingPage->getTemplates() as $template) {
            if (!in_array($template->getId()->getString(), $seenIds)) {
                $landingPage->removeTemplate($template);
            }
        }

        $this->pageRepository->persist($landingPage);
    }
}
