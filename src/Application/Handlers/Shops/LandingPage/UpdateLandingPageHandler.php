<?php

namespace NnShop\Application\Handlers\Shops\LandingPage;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Repository\DiscountRepositoryInterface;
use NnShop\Domain\Shops\Command\LandingPage\UpdateLandingPageCommand;
use NnShop\Domain\Shops\Repository\LandingPageRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;

class UpdateLandingPageHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var DiscountRepositoryInterface
     */
    private $discountRepository;

    /**
     * @var LandingPageRepositoryInterface
     */
    private $pageRepository;

    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * @param LandingPageRepositoryInterface $referrerRepository
     * @param DiscountRepositoryInterface    $discountRepository
     * @param TemplateRepositoryInterface    $templateRepository
     */
    public function __construct(LandingPageRepositoryInterface $referrerRepository, DiscountRepositoryInterface $discountRepository, TemplateRepositoryInterface $templateRepository)
    {
        $this->pageRepository = $referrerRepository;
        $this->discountRepository = $discountRepository;
        $this->templateRepository = $templateRepository;
    }

    /**
     * @param UpdateLandingPageCommand $command
     */
    public function __invoke(UpdateLandingPageCommand $command)
    {
        $referrer = $this->pageRepository->getById($command->getId());
        $referrer->setTitle($command->getTitle());
        $referrer->setText($command->getText());

        /*
         * Kortingscode koppelen
         */
        if ($command->hasDiscount()) {
            if ($referrer->hasDiscount()) {
                $referrer->unlinkDiscount();
            }

            $referrer->linkDiscount($this->discountRepository->getReference($command->getDiscountId()));
        } else {
            $referrer->unlinkDiscount();
        }

        /*
         * Template(s) koppelen
         */
        $seenIds = [];
        foreach ($command->getTemplates() as $templateId) {
            $template = $this->templateRepository->getReference($templateId);
            if (!$referrer->hasTemplate($template)) {
                $referrer->addTemplate($template);
            }

            $seenIds[] = $templateId->getString();
        }

        // Overbodige koppelingen opschonen
        foreach ($referrer->getTemplates() as $template) {
            if (!in_array($template->getId()->getString(), $seenIds)) {
                $referrer->removeTemplate($template);
            }
        }

        $this->pageRepository->persist($referrer);
    }
}
