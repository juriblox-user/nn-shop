<?php

namespace NnShop\Application\Handlers\Shops\Referrer;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Application\AppBundle\Services\Manager\ReferrerLogoManager;
use NnShop\Domain\Orders\Repository\DiscountRepositoryInterface;
use NnShop\Domain\Shops\Command\Referrer\CreateReferrerCommand;
use NnShop\Domain\Shops\Entity\Referrer;
use NnShop\Domain\Shops\Repository\ReferrerRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;

class CreateReferrerHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var DiscountRepositoryInterface
     */
    private $discountRepository;

    /**
     * @var ReferrerRepositoryInterface
     */
    private $referrerRepository;

    /**
     * @var ReferrerLogoManager
     */
    private $logoManager;

    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * @var ProfileRepositoryInterface
     */
    private $profileRepository;

    /**
     * CreateReferrerHandler constructor.
     *
     * @param ReferrerRepositoryInterface $referrerRepository
     * @param DiscountRepositoryInterface $discountRepository
     * @param TemplateRepositoryInterface $templateRepository
     * @param ReferrerLogoManager         $logoManager
     * @param ProfileRepositoryInterface  $profileRepository
     */
    public function __construct(ReferrerRepositoryInterface $referrerRepository, DiscountRepositoryInterface $discountRepository, TemplateRepositoryInterface $templateRepository, ReferrerLogoManager $logoManager, ProfileRepositoryInterface $profileRepository)
    {
        $this->referrerRepository = $referrerRepository;
        $this->discountRepository = $discountRepository;
        $this->templateRepository = $templateRepository;
        $this->profileRepository = $profileRepository;

        $this->logoManager = $logoManager;
    }

    /**
     * @param CreateReferrerCommand $command
     */
    public function __invoke(CreateReferrerCommand $command)
    {
        $profile = $this->profileRepository->getById($command->getProfileId());
        $referrer = Referrer::create($command->getId(), $command->getTitle(), $profile);

        $referrer->setHomepage($command->hasHomepage());
        $referrer->setKickback($command->hasKickback());
        $referrer->setLanding($command->hasLanding());

        $referrer->setText($command->getText());
        $referrer->setWebsite($command->getWebsite());

        /*
         * Kortingscode koppelen
         */
        if ($command->hasDiscount()) {
            if ($referrer->hasDiscount()) {
                $referrer->unlinkDiscount();
            }

            $referrer->linkDiscount($this->discountRepository->getReference($command->getDiscountId()));
        } else {
            $referrer->unlinkDiscount();
        }

        /*
         * Template(s) koppelen
         */
        $seenIds = [];
        foreach ($command->getTemplates() as $templateId) {
            $template = $this->templateRepository->getReference($templateId);
            if (!$referrer->hasTemplate($template)) {
                $referrer->addTemplate($template);
            }

            $seenIds[] = $templateId->getString();
        }

        // Overbodige koppelingen opschonen
        foreach ($referrer->getTemplates() as $template) {
            if (!in_array($template->getId()->getString(), $seenIds)) {
                $referrer->removeTemplate($template);
            }
        }

        /*
         * Logo toevoegen
         */
        if ($command->hasLogo()) {
            $this->logoManager->setLogo($referrer, $command->getLogo());
        }

        $this->referrerRepository->persist($referrer);
    }
}
