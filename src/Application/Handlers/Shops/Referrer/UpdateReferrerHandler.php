<?php

namespace NnShop\Application\Handlers\Shops\Referrer;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Application\AppBundle\Services\Manager\ReferrerLogoManager;
use NnShop\Domain\Orders\Repository\DiscountRepositoryInterface;
use NnShop\Domain\Shops\Command\Referrer\UpdateReferrerCommand;
use NnShop\Domain\Shops\Repository\ReferrerRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;

class UpdateReferrerHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var DiscountRepositoryInterface
     */
    private $discountRepository;

    /**
     * @var ReferrerLogoManager
     */
    private $logoManager;

    /**
     * @var ReferrerRepositoryInterface
     */
    private $referrerRepository;

    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * @param ReferrerRepositoryInterface $referrerRepository
     * @param DiscountRepositoryInterface $discountRepository
     * @param TemplateRepositoryInterface $templateRepository
     * @param ReferrerLogoManager         $logoManager
     */
    public function __construct(ReferrerRepositoryInterface $referrerRepository, DiscountRepositoryInterface $discountRepository, TemplateRepositoryInterface $templateRepository, ReferrerLogoManager $logoManager)
    {
        $this->referrerRepository = $referrerRepository;
        $this->discountRepository = $discountRepository;
        $this->templateRepository = $templateRepository;

        $this->logoManager = $logoManager;
    }

    /**
     * @param UpdateReferrerCommand $command
     */
    public function __invoke(UpdateReferrerCommand $command)
    {
        $referrer = $this->referrerRepository->getById($command->getId());

        $referrer->setHomepage($command->hasHomepage());
        $referrer->setKickback($command->hasKickback());
        $referrer->setLanding($command->hasLanding());

        $referrer->setTitle($command->getTitle());
        $referrer->setText($command->getText());
        $referrer->setWebsite($command->getWebsite());

        /*
         * Kortingscode koppelen
         */
        if ($command->hasDiscount()) {
            if ($referrer->hasDiscount()) {
                $referrer->unlinkDiscount();
            }

            $referrer->linkDiscount($this->discountRepository->getReference($command->getDiscountId()));
        } else {
            $referrer->unlinkDiscount();
        }

        /*
         * Template(s) koppelen
         */
        $seenIds = [];
        foreach ($command->getTemplates() as $templateId) {
            $template = $this->templateRepository->getReference($templateId);
            if (!$referrer->hasTemplate($template)) {
                $referrer->addTemplate($template);
            }

            $seenIds[] = $templateId->getString();
        }

        // Overbodige koppelingen opschonen
        foreach ($referrer->getTemplates() as $template) {
            if (!in_array($template->getId()->getString(), $seenIds)) {
                $referrer->removeTemplate($template);
            }
        }

        /*
         * Logo wijzigen/verwijderen
         */
        if ($command->shouldDeleteLogo()) {
            $this->logoManager->deleteLogo($referrer);
        } elseif ($command->hasLogo()) {
            $this->logoManager->setLogo($referrer, $command->getLogo());
        }

        $this->referrerRepository->persist($referrer);
    }
}
