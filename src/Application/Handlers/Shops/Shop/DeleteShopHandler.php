<?php

namespace NnShop\Application\Handlers\Shops\Shop;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Shops\Command\Shop\DeleteShopCommand;
use NnShop\Domain\Shops\Repository\ShopRepositoryInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class DeleteShopHandler implements LoggerAwareInterface, SynchronousCommandHandlerInterface
{
    use LoggerAwareTrait;

    /**
     * @var ShopRepositoryInterface
     */
    private $repository;

    /**
     * DeleteShopCommandHandler constructor.
     *
     * @param ShopRepositoryInterface $repository
     */
    public function __construct(ShopRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param DeleteShopCommand $command
     */
    public function __invoke(DeleteShopCommand $command)
    {
        $shop = $this->repository->getById($command->getId());

        $this->logger->info('Deleting shop "{title}"', [
            'id' => $shop->getId()->getString(),
            'title' => $shop->getTitle(),
        ]);

        $this->repository->remove($shop);
    }
}
