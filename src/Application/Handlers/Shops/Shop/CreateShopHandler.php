<?php

namespace NnShop\Application\Handlers\Shops\Shop;

use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Shops\Command\Shop\CreateShopCommand;
use NnShop\Domain\Shops\Entity\Shop;
use NnShop\Domain\Shops\Repository\ShopRepositoryInterface;
use NnShop\Domain\Shops\Services\Validator\ShopHostnameValidator;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;

class CreateShopHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var ShopRepositoryInterface
     */
    private $repository;

    /**
     * @var ShopHostnameValidator
     */
    private $validator;

    /**
     * @var ProfileRepositoryInterface
     */
    private $profileRepository;

    /**
     * CreateShopHandler constructor.
     *
     * @param ShopRepositoryInterface    $repository
     * @param ShopHostnameValidator      $validator
     * @param ProfileRepositoryInterface $profileRepository
     */
    public function __construct(ShopRepositoryInterface $repository, ShopHostnameValidator $validator, ProfileRepositoryInterface $profileRepository)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->profileRepository = $profileRepository;
    }

    /**
     * @param CreateShopCommand $command
     */
    public function __invoke(CreateShopCommand $command)
    {
        $profile = $this->profileRepository->getById($command->getProfileId());

        if (!$this->validator->isUnique($command->getHostname())) {
            throw new DuplicateEntityException(Shop::class);
        }

        $shop = Shop::create($command->getId(), $command->getTitle(), $command->getHostname(), $profile);
        $shop->setMetaDescription($command->getMetaDescription());
        $shop->setDescription($command->getDescription());

        $this->repository->persist($shop);
    }
}
