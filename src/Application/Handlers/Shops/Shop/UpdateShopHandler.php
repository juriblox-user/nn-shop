<?php

namespace NnShop\Application\Handlers\Shops\Shop;

use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Shops\Command\Shop\UpdateShopCommand;
use NnShop\Domain\Shops\Entity\Shop;
use NnShop\Domain\Shops\Repository\ShopRepositoryInterface;
use NnShop\Domain\Shops\Services\Validator\ShopHostnameValidator;

class UpdateShopHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var ShopRepositoryInterface
     */
    private $repository;

    /**
     * @var ShopHostnameValidator
     */
    private $validator;

    /**
     * UpdateShopHandler constructor.
     *
     * @param ShopRepositoryInterface $repository
     * @param ShopHostnameValidator   $validator
     */
    public function __construct(ShopRepositoryInterface $repository, ShopHostnameValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * @param UpdateShopCommand $command
     */
    public function __invoke(UpdateShopCommand $command)
    {
        $shop = $this->repository->getById($command->getId());

        if (!$this->validator->isUnique($command->getHostname(), $shop)) {
            throw new DuplicateEntityException(Shop::class);
        }

        if ($command->isEnabled()) {
            $shop->enable();
        } else {
            $shop->disable();
        }

        $shop->setTitle($command->getTitle());
        $shop->setHostname($command->getHostname());
        $shop->setMetaDescription($command->getMetaDescription());
        $shop->setDescription($command->getDescription());

        $this->repository->persist($shop);
    }
}
