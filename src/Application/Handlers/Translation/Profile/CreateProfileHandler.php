<?php

namespace NnShop\Application\Handlers\Translation\Profile;

use Core\Domain\RepositoryInterface;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Translation\Command\Profile\CreateProfileCommand;
use NnShop\Domain\Translation\Entity\Profile;
use NnShop\Domain\Translation\Repository\LocaleRepositoryInterface;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;

/**
 * Class CreateProfileHandler.
 */
class CreateProfileHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var RepositoryInterface
     */
    private $repository;

    /**
     * @var LocaleRepositoryInterface
     */
    private $localeRepository;

    /**
     * CreateProfileHandler constructor.
     *
     * @param ProfileRepositoryInterface $repository
     * @param LocaleRepositoryInterface  $localeRepository
     */
    public function __construct(ProfileRepositoryInterface $repository, LocaleRepositoryInterface $localeRepository)
    {
        $this->repository = $repository;
        $this->localeRepository = $localeRepository;
    }

    /**
     * @param CreateProfileCommand $command
     *
     * @throws \Exception
     */
    public function __invoke(CreateProfileCommand $command)
    {
        $locale = $this->localeRepository->getById($command->getMainLocaleId());

        $country = mb_strtolower($command->getCountry());

        $slug = $country . '-' . $locale->getLocale();

        $number = 1;

        while (null !== $this->repository->findOneBySlug($slug)) {
            $slug = $country . '-' . $locale->getLocale();
            $slug = $slug . $number;
            ++$number;
        }

        $profile = Profile::create($command->getId(), $locale);
        $profile->setCountry($command->getCountry());
        $profile->setCurrency($command->getCurrency());
        $profile->setTimezone($command->getTimezone());
        $profile->setUrl($command->getUrl());
        $profile->setMollieKey($command->getMollieKey());
        $profile->setSlug($slug);
        $profile->setEmail($command->getEmail());
        $profile->setPhone($command->getPhone());
        $profile->setEntityName($command->getEntityName());
        $profile->setEntityCountry($command->getEntityCountry());
        $profile->setAddressLine1($command->getAddressLine1());
        $profile->setAddressLine2($command->getAddressLine2());
        $profile->setZipcode($command->getZipcode());
        $profile->setCity($command->getCity());
        $profile->setProvince($command->getProvince());
        $profile->setCocLabel($command->getCocLabel());
        $profile->setCocNumber($command->getCocNumber());
        $profile->setVatLabel($command->getVatLabel());
        $profile->setVatNumber($command->getVatNumber());

        $this->repository->persist($profile);
    }
}
