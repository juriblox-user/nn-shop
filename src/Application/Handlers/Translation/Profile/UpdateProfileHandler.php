<?php

namespace NnShop\Application\Handlers\Translation\Profile;

use Core\Domain\RepositoryInterface;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Translation\Command\Profile\UpdateProfileCommand;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;

/**
 * Class UpdateProfileHandler.
 */
class UpdateProfileHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var RepositoryInterface
     */
    private $repository;

    /**
     * UpdateProfileHandler constructor.
     *
     * @param ProfileRepositoryInterface $repository
     */
    public function __construct(ProfileRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param UpdateProfileCommand $command
     *
     * @throws \Exception
     */
    public function __invoke(UpdateProfileCommand $command)
    {
        $profile = $this->repository->getById($command->getId());
        $profile->setCountry($command->getCountry());
        $profile->setCurrency($command->getCurrency());
        $profile->setTimezone($command->getTimezone());
        $profile->setMainLocale($command->getMainLocale());
        $profile->setUrl($command->getUrl());
        $profile->setMollieKey($command->getMollieKey());
        $profile->setEmail($command->getEmail());
        $profile->setPhone($command->getPhone());
        $profile->setEntityName($command->getEntityName());
        $profile->setEntityCountry($command->getEntityCountry());
        $profile->setAddressLine1($command->getAddressLine1());
        $profile->setAddressLine2($command->getAddressLine2());
        $profile->setZipcode($command->getZipcode());
        $profile->setCity($command->getCity());
        $profile->setProvince($command->getProvince());
        $profile->setCocLabel($command->getCocLabel());
        $profile->setCocNumber($command->getCocNumber());
        $profile->setVatLabel($command->getVatLabel());
        $profile->setVatNumber($command->getVatNumber());

        $this->repository->persist($profile);
    }
}
