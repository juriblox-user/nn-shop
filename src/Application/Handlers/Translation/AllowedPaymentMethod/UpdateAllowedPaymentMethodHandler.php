<?php

namespace NnShop\Application\Handlers\Translation\AllowedPaymentMethod;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Translation\Command\AllowedPaymentMethod\UpdateAllowedPaymentMethodCommand;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;

/**
 * Class UpdateAllowedPaymentMethodHandler.
 */
class UpdateAllowedPaymentMethodHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var ProfileRepositoryInterface
     */
    private $repository;

    /**
     * UpdateAllowedPaymentMethodHandler constructor.
     *
     * @param ProfileRepositoryInterface $repository
     */
    public function __construct(ProfileRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param UpdateAllowedPaymentMethodCommand $command
     *
     * @throws \Exception
     */
    public function __invoke(UpdateAllowedPaymentMethodCommand $command)
    {
        $profile = $this->repository->find($command->getProfileId());

        $allowedPaymentMethods = $profile->getAllowedPaymentMethods();
        $method = $command->getMethodObject()->getId();

        if ($command->isEnabled()) {
            if ($this->hasMethod($allowedPaymentMethods, $method)) {
                return;
            }
            $allowedPaymentMethods[] = $method;
        } else {
            foreach ($allowedPaymentMethods as $key => $allowedMethod) {
                if ($method === $allowedMethod) {
                    unset($allowedPaymentMethods[$key]);
                }
            }
        }

        $profile->setAllowedPaymentMethods($allowedPaymentMethods);

        $this->repository->persist($profile);
    }

    /**
     * @param array  $allowedMethods
     * @param string $newMethod
     *
     * @return bool
     */
    private function hasMethod($allowedMethods, $newMethod)
    {
        foreach ($allowedMethods as $allowedMethod) {
            if ($newMethod === $allowedMethod) {
                return true;
            }
        }

        return false;
    }
}
