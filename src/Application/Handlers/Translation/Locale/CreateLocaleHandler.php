<?php

namespace NnShop\Application\Handlers\Translation\Locale;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Translation\Command\Locale\CreateLocaleCommand;
use NnShop\Domain\Translation\Entity\Locale;
use NnShop\Domain\Translation\Repository\LocaleRepositoryInterface;
use NnShop\Domain\Translation\Value\LocaleId;

/**
 * Class CreateLocaleHandler.
 */
class CreateLocaleHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var LocaleRepositoryInterface
     */
    private $repository;

    /**
     * CreateLocaleHandler constructor.
     *
     * @param LocaleRepositoryInterface $repository
     */
    public function __construct(LocaleRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param CreateLocaleCommand $command
     */
    public function __invoke(CreateLocaleCommand $command)
    {
        $locale = Locale::create(LocaleId::generate());
        $locale->setLocale($command->getLocale());

        $this->repository->persist($locale);
    }
}
