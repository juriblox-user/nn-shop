<?php

namespace NnShop\Application\Handlers\Translation\Locale;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Translation\Command\Locale\UpdateLocaleCommand;
use NnShop\Domain\Translation\Repository\LocaleRepositoryInterface;

/**
 * Class UpdateLocaleHandler.
 */
class UpdateLocaleHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var LocaleRepositoryInterface
     */
    private $repository;

    /**
     * UpdateLocaleHandler constructor.
     *
     * @param LocaleRepositoryInterface $repository
     */
    public function __construct(LocaleRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param UpdateLocaleCommand $command
     *
     * @throws \Exception
     */
    public function __invoke(UpdateLocaleCommand $command)
    {
        $locale = $this->repository->getById($command->getId());
        $locale->setLocale($command->getLocale());

        $this->repository->persist($locale);
    }
}
