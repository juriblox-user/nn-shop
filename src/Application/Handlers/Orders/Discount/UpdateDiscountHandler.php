<?php

namespace NnShop\Application\Handlers\Orders\Discount;

use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Discount\UpdateDiscountCommand;
use NnShop\Domain\Orders\Enumeration\DiscountType;
use NnShop\Domain\Orders\Repository\DiscountRepositoryInterface;
use NnShop\Domain\Orders\Services\Validator\DiscountValidator;

class UpdateDiscountHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var DiscountRepositoryInterface
     */
    private $discountRepository;

    /**
     * @var DiscountValidator
     */
    private $discountValidator;

    /**
     * @param DiscountRepositoryInterface $discountRepository
     * @param DiscountValidator           $discountValidator
     */
    public function __construct(DiscountRepositoryInterface $discountRepository, DiscountValidator $discountValidator)
    {
        $this->discountRepository = $discountRepository;
        $this->discountValidator = $discountValidator;
    }

    /**
     * @param UpdateDiscountCommand $command
     */
    public function __invoke(UpdateDiscountCommand $command)
    {
        $discount = $this->discountRepository->getById($command->getId());

        if (!$this->discountValidator->isUnique($command->getCode(), $discount)) {
            throw new DuplicateEntityException();
        }

        $discount->setType($command->getType());

        $discount->setTitle($command->getTitle());
        $discount->setCode($command->getCode());
        $discount->setMaxUses($command->getMaxUses());
        $discount->setActive($command->isActive());

        $discount->setTemplates($command->getTemplates());
        $discount->setEmails($command->getEmails());

        // Kortingspercentage/-bedrag
        if ($command->getType()->is(DiscountType::PERCENTAGE)) {
            $discount->setPercentage($command->getPercentage());
        } else {
            $discount->setAmount($command->getAmount());
        }

        $this->discountRepository->persist($discount);
    }
}
