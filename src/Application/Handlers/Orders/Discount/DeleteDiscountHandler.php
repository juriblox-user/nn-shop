<?php

namespace NnShop\Application\Handlers\Orders\Discount;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Discount\DeleteDiscountCommand;
use NnShop\Domain\Orders\Repository\DiscountRepositoryInterface;

class DeleteDiscountHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var DiscountRepositoryInterface
     */
    private $discountRepository;

    /**
     * @param DiscountRepositoryInterface $discountRepository
     */
    public function __construct(DiscountRepositoryInterface $discountRepository)
    {
        $this->discountRepository = $discountRepository;
    }

    /**
     * @param DeleteDiscountCommand $command
     */
    public function __invoke(DeleteDiscountCommand $command)
    {
        $discount = $this->discountRepository->find($command->getId());

        $this->discountRepository->remove($discount);
    }
}
