<?php

namespace NnShop\Application\Handlers\Orders\Discount;

use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Discount\CreateDiscountCommand;
use NnShop\Domain\Orders\Entity\Discount;
use NnShop\Domain\Orders\Enumeration\DiscountType;
use NnShop\Domain\Orders\Repository\DiscountRepositoryInterface;
use NnShop\Domain\Orders\Services\Validator\DiscountValidator;
use NnShop\Domain\Orders\Value\DiscountId;

class CreateDiscountHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var DiscountRepositoryInterface
     */
    private $discountRepository;

    /**
     * @var DiscountValidator
     */
    private $validator;

    /**
     * @param DiscountRepositoryInterface $discountRepository
     * @param DiscountValidator           $validator
     */
    public function __construct(DiscountRepositoryInterface $discountRepository, DiscountValidator $validator)
    {
        $this->discountRepository = $discountRepository;
        $this->validator = $validator;
    }

    /**
     * @param CreateDiscountCommand $command
     */
    public function __invoke(CreateDiscountCommand $command)
    {
        if (!$this->validator->isUnique($command->getCode())) {
            throw new DuplicateEntityException();
        }

        $discount = Discount::create(DiscountId::generate(), $command->getCode(), $command->getType());
        $discount->setTitle($command->getTitle());
        $discount->setMaxUses($command->getMaxUses());
        $discount->setActive($command->isActive());
        $discount->setTemplates($command->getTemplates());
        $discount->setEmails($command->getEmails());

        // Kortingspercentage/-bedrag
        if ($command->getType()->is(DiscountType::PERCENTAGE)) {
            $discount->setPercentage($command->getPercentage());
        } else {
            $discount->setAmount($command->getAmount());
        }

        $this->discountRepository->persist($discount);
    }
}
