<?php

namespace NnShop\Application\Handlers\Orders\Invoice;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Invoice\ProcessInvoicePaymentCommand;
use NnShop\Domain\Orders\Enumeration\InvoiceStatus;
use NnShop\Domain\Orders\Repository\InvoiceRepositoryInterface;

class ProcessInvoicePaymentHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * ProcessInvoicePaymentHandler constructor.
     *
     * @param InvoiceRepositoryInterface $invoiceRepository
     */
    public function __construct(InvoiceRepositoryInterface $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
    }

    /**
     * @param ProcessInvoicePaymentCommand $command
     */
    public function __invoke(ProcessInvoicePaymentCommand $command)
    {
        $invoice = $this->invoiceRepository->getById($command->getId());
        $invoice->setStatus(InvoiceStatus::from(InvoiceStatus::PAID), $command->getPaymentDateTime());

        $this->invoiceRepository->persist($invoice);
    }
}
