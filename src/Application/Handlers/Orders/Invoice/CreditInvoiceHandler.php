<?php

namespace NnShop\Application\Handlers\Orders\Invoice;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Invoice\CreditInvoiceCommand;
use NnShop\Domain\Orders\Repository\InvoiceRepositoryInterface;
use NnShop\Domain\Orders\Services\Generator\InvoiceCodeGenerator;
use NnShop\Domain\Orders\Value\InvoiceId;

class CreditInvoiceHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * @var InvoiceCodeGenerator
     */
    private $invoiceCodeGenerator;

    /**
     * Constructor.
     *
     * @param InvoiceRepositoryInterface $invoiceRepository
     * @param InvoiceCodeGenerator       $invoiceCodeGenerator
     */
    public function __construct(InvoiceRepositoryInterface $invoiceRepository, InvoiceCodeGenerator $invoiceCodeGenerator)
    {
        $this->invoiceRepository = $invoiceRepository;
        $this->invoiceCodeGenerator = $invoiceCodeGenerator;
    }

    /**
     * @param CreditInvoiceCommand $command
     */
    public function __invoke(CreditInvoiceCommand $command)
    {
        $invoice = $this->invoiceRepository->getById($command->getId());

        $credit = $invoice->credit(InvoiceId::generate(), $this->invoiceCodeGenerator->generate());

        $this->invoiceRepository->persist($invoice);
        $this->invoiceRepository->persist($credit);
    }
}
