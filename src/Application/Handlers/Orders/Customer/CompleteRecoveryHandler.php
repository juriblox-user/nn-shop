<?php

namespace NnShop\Application\Handlers\Orders\Customer;

use Core\Common\Exception\Domain\DeletedEntityException;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Common\Exceptions\InvalidCustomerStatusException;
use NnShop\Domain\Orders\Command\Customer\CompleteRecoveryCommand;
use NnShop\Domain\Orders\Enumeration\CustomerStatus;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;

class CompleteRecoveryHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * Constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param CompleteRecoveryCommand $command
     */
    public function __invoke(CompleteRecoveryCommand $command)
    {
        $customer = $this->customerRepository->getById($command->getId());
        if ($customer->isDeleted()) {
            throw new DeletedEntityException();
        }

        if ($customer->getStatus()->isNot(CustomerStatus::RECOVER)) {
            throw new InvalidCustomerStatusException();
        }

        $customer->setStatus(new CustomerStatus(CustomerStatus::ACTIVE));

        $this->customerRepository->persist($customer);
    }
}
