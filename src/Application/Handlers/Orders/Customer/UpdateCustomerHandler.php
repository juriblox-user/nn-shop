<?php

namespace NnShop\Application\Handlers\Orders\Customer;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Application\AppBundle\Command\UpdateCustomerCommand;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;

class UpdateCustomerHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * Constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param UpdateCustomerCommand $command
     */
    public function __invoke(UpdateCustomerCommand $command)
    {
        $customer = $this->customerRepository->getById($command->getId());

        $customer->setType($command->getType());

        $customer->setCompanyName($command->getCompanyName());
        $customer->setFirstname($command->getFirstname());
        $customer->setLastname($command->getLastname());

        $customer->setAddress($command->getAddress());
        $customer->setPhone($command->getPhone());
        $customer->setVat($command->getVat());
        $customer->setCocNumber($command->getCoc());

        $this->customerRepository->persist($customer);
    }
}
