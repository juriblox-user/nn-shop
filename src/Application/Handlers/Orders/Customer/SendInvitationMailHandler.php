<?php

namespace NnShop\Application\Handlers\Orders\Customer;

use Core\Application\CoreBundle\Service\Mail\Mailer;
use Core\Component\Mail\Recipient;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Application\AppBundle\Mail\Customer\InvitationMail;
use NnShop\Application\AppBundle\Services\Security\MagicLink\UrlSigner;
use NnShop\Common\Exceptions\InvalidCustomerStatusException;
use NnShop\Domain\Orders\Command\Customer\SendInvitationMailCommand;
use NnShop\Domain\Orders\Enumeration\CustomerStatus;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;

class SendInvitationMailHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var UrlSigner
     */
    private $urlSigner;

    /**
     * Constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     * @param UrlSigner                   $urlSigner
     * @param Mailer                      $mailer
     */
    public function __construct(CustomerRepositoryInterface $customerRepository, UrlSigner $urlSigner, Mailer $mailer)
    {
        $this->customerRepository = $customerRepository;

        $this->urlSigner = $urlSigner;
        $this->mailer = $mailer;
    }

    /**
     * @param SendInvitationMailCommand $command
     */
    public function __invoke(SendInvitationMailCommand $command)
    {
        $customer = $this->customerRepository->getById($command->getId());
        if ($customer->getStatus()->isNot(CustomerStatus::INVITED)) {
            throw new InvalidCustomerStatusException();
        }

        $mail = InvitationMail::create($customer, $customer->getProfile(), $this->urlSigner);
        $mail->setLocale($customer->getLocale()->getLocale());

        $mail->addTo(Recipient::from($customer->getEmail()));

        $this->mailer->send($mail);
    }
}
