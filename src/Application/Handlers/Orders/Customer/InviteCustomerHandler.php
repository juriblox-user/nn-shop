<?php

namespace NnShop\Application\Handlers\Orders\Customer;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Common\Exceptions\InvalidCustomerStatusException;
use NnShop\Domain\Orders\Command\Customer\InviteCustomerCommand;
use NnShop\Domain\Orders\Command\Customer\SendInvitationMailCommand;
use NnShop\Domain\Orders\Enumeration\CustomerStatus;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;

class InviteCustomerHandler implements CommandBusAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * Constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param InviteCustomerCommand $command
     */
    public function __invoke(InviteCustomerCommand $command)
    {
        $customer = $this->customerRepository->getById($command->getId());
        if ($customer->getStatus()->isNot(CustomerStatus::DRAFT)) {
            throw new InvalidCustomerStatusException();
        }

        // Status aanpassen
        $customer->setStatus(new CustomerStatus(CustomerStatus::INVITED));

        // Mail versturen
        $this->commandBus->handle(SendInvitationMailCommand::create($customer->getId()));

        $this->customerRepository->persist($customer);
    }
}
