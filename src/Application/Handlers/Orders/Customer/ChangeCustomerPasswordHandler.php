<?php

namespace NnShop\Application\Handlers\Orders\Customer;

use Core\Application\CoreBundle\Service\Security\HashedUserPasswordEncoder;
use Core\Common\Exception\Domain\DeletedEntityException;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Customer\ChangePasswordCommand;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;

class ChangeCustomerPasswordHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var HashedUserPasswordEncoder
     */
    private $passwordEncoder;

    /**
     * Constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     * @param HashedUserPasswordEncoder   $passwordEncoder
     */
    public function __construct(CustomerRepositoryInterface $customerRepository, HashedUserPasswordEncoder $passwordEncoder)
    {
        $this->customerRepository = $customerRepository;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ChangePasswordCommand $command
     */
    public function __invoke(ChangePasswordCommand $command)
    {
        $customer = $this->customerRepository->getById($command->getId());
        if ($customer->isDeleted()) {
            throw new DeletedEntityException();
        }

        $customer->setPassword($this->passwordEncoder->encode($customer, $command->getPassword()));

        $this->customerRepository->persist($customer);
    }
}
