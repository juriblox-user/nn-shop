<?php

namespace NnShop\Application\Handlers\Orders\Customer;

use Core\Application\CoreBundle\Service\Security\HashedUserPasswordEncoder;
use Core\Common\Exception\Domain\DuplicateEntityException;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Customer\CreateCustomerCommand;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;
use NnShop\Domain\Translation\Repository\LocaleRepositoryInterface;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;

class CreateCustomerHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var LocaleRepositoryInterface
     */
    private $localeRepository;

    /**
     * @var HashedUserPasswordEncoder
     */
    private $encoder;

    /**
     * @var ProfileRepositoryInterface
     */
    private $profileRepository;

    /**
     * CreateCustomerHandler constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     * @param ProfileRepositoryInterface  $profileRepository
     * @param LocaleRepositoryInterface   $localeRepository
     * @param HashedUserPasswordEncoder   $encoder
     */
    public function __construct(CustomerRepositoryInterface $customerRepository, ProfileRepositoryInterface $profileRepository, LocaleRepositoryInterface $localeRepository, HashedUserPasswordEncoder $encoder)
    {
        $this->customerRepository = $customerRepository;

        $this->profileRepository = $profileRepository;
        $this->localeRepository = $localeRepository;

        $this->encoder = $encoder;
    }

    /**
     * @param CreateCustomerCommand $command
     */
    public function __invoke(CreateCustomerCommand $command)
    {
        if ($this->customerRepository->hasWithEmail($command->getEmail())) {
            throw new DuplicateEntityException('There already is a customer with the e-mail address ' . $command->getEmail()->getString());
        }

        $profile = $this->profileRepository->getReference($command->getProfileId());
        $locale = $this->localeRepository->getReference($command->getLocaleId());

        $customer = Customer::create($command->getId(), $command->getType(), $command->getEmail(), $profile, $locale);
        $customer->setFirstname($command->getFirstname());
        $customer->setLastname($command->getLastname());

        $customer->setCompanyName($command->getCompanyName());
        $customer->setVat($command->getVat());

        $customer->setPhone($command->getPhone());

        if (null !== $command->getAddress()) {
            $customer->setAddress($command->getAddress());
        }

        if (null !== $command->getPassword()) {
            $customer->setPassword($this->encoder->encode($customer, $command->getPassword()));
        }

        $customer->setNewsletter($command->getNewsletter());

        $this->customerRepository->persist($customer);
    }
}
