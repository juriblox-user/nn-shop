<?php

namespace NnShop\Application\Handlers\Orders\Customer;

use Core\Common\Exception\Domain\DeletedEntityException;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Common\Exceptions\InvalidCustomerStatusException;
use NnShop\Domain\Orders\Command\Customer\CompleteInvitationCommand;
use NnShop\Domain\Orders\Enumeration\CustomerStatus;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;

class CompleteCustomerInvitationHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * Constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param CompleteInvitationCommand $command
     */
    public function __invoke(CompleteInvitationCommand $command)
    {
        $customer = $this->customerRepository->getById($command->getId());
        if ($customer->isDeleted()) {
            throw new DeletedEntityException();
        }

        if ($customer->getStatus()->isNot(CustomerStatus::INVITED)) {
            throw new InvalidCustomerStatusException();
        }

        if (!$customer->hasPassword()) {
            throw new \DomainException(sprintf('Cannot complete invitation for Customer#%s: no password set', $customer->getId()));
        }

        $customer->setStatus(new CustomerStatus(CustomerStatus::ACTIVE));
        $customer->setEmailVerified(true);

        $this->customerRepository->persist($customer);
    }
}
