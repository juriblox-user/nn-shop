<?php

namespace NnShop\Application\Handlers\Orders\Customer;

use Core\Common\Exception\Domain\DeletedEntityException;
use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Common\Exceptions\InvalidCustomerStatusException;
use NnShop\Domain\Orders\Command\Customer\RequestRecoveryCommand;
use NnShop\Domain\Orders\Command\Customer\SendRecoveryMailCommand;
use NnShop\Domain\Orders\Enumeration\CustomerStatus;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;

class RequestRecoveryHandler implements CommandBusAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * Constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param RequestRecoveryCommand $command
     */
    public function __invoke(RequestRecoveryCommand $command)
    {
        $customer = $this->customerRepository->getById($command->getId());
        if ($customer->isDeleted()) {
            throw new DeletedEntityException();
        }

        if ($customer->getStatus()->notIn([CustomerStatus::ACTIVE, CustomerStatus::RECOVER])) {
            throw new InvalidCustomerStatusException();
        }

        $customer->setStatus(new CustomerStatus(CustomerStatus::RECOVER));

        $this->customerRepository->persist($customer);

        // E-mail versturen
        $this->commandBus->handle(SendRecoveryMailCommand::create($customer->getId()));
    }
}
