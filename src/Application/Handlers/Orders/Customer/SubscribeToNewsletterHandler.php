<?php

namespace NnShop\Application\Handlers\Orders\Customer;

use Core\Common\Exception\Domain\DeletedEntityException;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Customer\SubscribeToNewsletterCommand;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;

class SubscribeToNewsletterHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * Constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param SubscribeToNewsletterCommand $command
     */
    public function __invoke(SubscribeToNewsletterCommand $command)
    {
        $customer = $this->customerRepository->getById($command->getId());
        if ($customer->isDeleted()) {
            throw new DeletedEntityException();
        }

        $customer->setNewsletter(true);

        $this->customerRepository->persist($customer);
    }
}
