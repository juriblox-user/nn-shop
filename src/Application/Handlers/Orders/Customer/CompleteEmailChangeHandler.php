<?php

namespace NnShop\Application\Handlers\Orders\Customer;

use Core\Common\Exception\Domain\DeletedEntityException;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Common\Exceptions\InvalidCustomerStatusException;
use NnShop\Domain\Orders\Command\Customer\CompleteEmailChangeCommand;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;

class CompleteEmailChangeHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * Constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param CompleteEmailChangeCommand $command
     */
    public function __invoke(CompleteEmailChangeCommand $command)
    {
        $customer = $this->customerRepository->getById($command->getId());
        if ($customer->isDeleted()) {
            throw new DeletedEntityException();
        }

        if (!$customer->hasEmailChange()) {
            throw new InvalidCustomerStatusException();
        }

        $customer->completeEmailChange();

        $this->customerRepository->persist($customer);
    }
}
