<?php

namespace NnShop\Application\Handlers\Orders\Customer;

use Core\Common\Exception\Domain\DeletedEntityException;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Customer\UnsubscribeFromNewsletterCommand;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;

class UnsubscribeFromNewsletterHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * Constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param UnsubscribeFromNewsletterCommand $command
     */
    public function __invoke(UnsubscribeFromNewsletterCommand $command)
    {
        $customer = $this->customerRepository->getById($command->getId());
        if ($customer->isDeleted()) {
            throw new DeletedEntityException();
        }

        $customer->setNewsletter(false);

        $this->customerRepository->persist($customer);
    }
}
