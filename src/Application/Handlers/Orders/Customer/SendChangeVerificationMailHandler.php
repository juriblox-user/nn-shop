<?php

namespace NnShop\Application\Handlers\Orders\Customer;

use Core\Application\CoreBundle\Service\Mail\Mailer;
use Core\Common\Exception\Domain\DeletedEntityException;
use Core\Component\Mail\Recipient;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Application\AppBundle\Mail\Customer\ChangeVerificationMail;
use NnShop\Application\AppBundle\Services\Security\MagicLink\UrlSigner;
use NnShop\Domain\Orders\Command\Customer\SendChangeVerificationMailCommand;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;

class SendChangeVerificationMailHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var UrlSigner
     */
    private $urlSigner;

    /**
     * Constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     * @param UrlSigner                   $urlSigner
     * @param Mailer                      $mailer
     */
    public function __construct(CustomerRepositoryInterface $customerRepository, UrlSigner $urlSigner, Mailer $mailer)
    {
        $this->customerRepository = $customerRepository;

        $this->urlSigner = $urlSigner;
        $this->mailer = $mailer;
    }

    /**
     * @param SendChangeVerificationMailCommand $command
     */
    public function __invoke(SendChangeVerificationMailCommand $command)
    {
        $customer = $this->customerRepository->getById($command->getId());
        if ($customer->isDeleted()) {
            throw new DeletedEntityException();
        }

        $mail = ChangeVerificationMail::create($customer, $customer->getProfile(), $this->urlSigner);
        $mail->setLocale($customer->getLocale()->getLocale());

        $mail->addTo(Recipient::from($customer->getEmailChange()));

        $this->mailer->send($mail);
    }
}
