<?php

namespace NnShop\Application\Handlers\Orders\Customer;

use Core\Common\Exception\Domain\DeletedEntityException;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Customer\ClaimOrderCommand;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;

class ClaimOrderHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * Constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     * @param OrderRepositoryInterface    $orderRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository, OrderRepositoryInterface $orderRepository)
    {
        $this->customerRepository = $customerRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param ClaimOrderCommand $command
     */
    public function __invoke(ClaimOrderCommand $command)
    {
        $customer = $this->customerRepository->getById($command->getCustomerId());
        if ($customer->isDeleted()) {
            throw new DeletedEntityException();
        }

        $order = $this->orderRepository->getById($command->getOrderId());
        if (!$order->getStatus()->isActive()) {
            throw new \DomainException(sprintf('Cannot claim Order#%s for Customer#%s with the %s status', $order->getId(), $customer->getId(), $order->getStatus()));
        }

        if ($order->hasCustomer()) {
            throw new \DomainException(sprintf('Cannot claim Order#%s for Customer#%s: Order is owned by Customer#%s', $order->getId(), $customer->getId(), $order->getCustomer()->getId()));
        }

        $order->claim($customer);

        $this->orderRepository->persist($order);
    }
}
