<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\AbandonOrderCommand;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;

class AbandonOrderHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * Constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param AbandonOrderCommand $command
     */
    public function __invoke(AbandonOrderCommand $command)
    {
        $order = $this->orderRepository->getById($command->getId());

        if (!$order->getStatus()->isActive()) {
            throw new \LogicException(sprintf('An order cannot be abandoned in the %s state', $order->getStatus()->getName()));
        }

        $order->setAbandoned();

        $this->orderRepository->persist($order);
    }
}
