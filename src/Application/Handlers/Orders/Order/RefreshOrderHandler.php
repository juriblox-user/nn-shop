<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\RefreshOrderCommand;
use NnShop\Domain\Orders\Command\Payment\SynchronizePendingPaymentsCommand;
use NnShop\Domain\Orders\Enumeration\OrderStatus;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Templates\Command\DocumentRequest\SynchronizePendingRequestsCommand;

class RefreshOrderHandler implements CommandBusAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * RefreshOrderHandler constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param RefreshOrderCommand $command
     */
    public function __invoke(RefreshOrderCommand $command)
    {
        $order = $this->orderRepository->getById($command->getId());

        // Openstaande betalingen synchroniseren
        if (OrderStatus::PENDING_PAYMENT == $order->getStatus() && $order->hasInvoice()) {
            $this->commandBus->handle(
                SynchronizePendingPaymentsCommand::create($order->getInvoice()->getId())
            );
        }

        // DocumentRequests synchroniseren
        if (OrderStatus::REQUEST == $order->getStatus() && (!$order->isRecentlyRefreshed() || $command->isForced())) {
            $this->commandBus->handle(
                SynchronizePendingRequestsCommand::create($order->getDocument()->getId())
            );
        }

        $order->updateTimestampRefreshed();

        $this->orderRepository->persist($order);
    }
}
