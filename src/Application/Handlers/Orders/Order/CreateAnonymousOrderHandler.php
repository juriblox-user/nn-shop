<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\Common\Generator\Security\ShortTokenGenerator;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\CreateAnonymousOrderCommand;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Orders\Services\Generator\OrderCodeGenerator;
use NnShop\Domain\Shops\Repository\ReferrerRepositoryInterface;
use NnShop\Domain\Shops\Repository\ShopRepositoryInterface;
use NnShop\Domain\Templates\Entity\Document;
use NnShop\Domain\Templates\Repository\DocumentRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use NnShop\Domain\Templates\Value\DocumentId;

class CreateAnonymousOrderHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var DocumentRepositoryInterface
     */
    private $documentRepository;

    /**
     * @var ReferrerRepositoryInterface
     */
    private $referrerRepository;

    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * @var OrderCodeGenerator
     */
    private $numberGenerator;

    /**
     * @var ShopRepositoryInterface
     */
    private $shopRepository;

    /**
     * CreateAnonymousOrderHandler constructor.
     *
     * @param OrderRepositoryInterface    $orderRepository
     * @param TemplateRepositoryInterface $templateRepository
     * @param DocumentRepositoryInterface $documentRepository
     * @param ReferrerRepositoryInterface $referrerRepository
     * @param ShopRepositoryInterface     $shopRepository
     * @param OrderCodeGenerator          $codeGenerator
     */
    public function __construct(OrderRepositoryInterface $orderRepository, TemplateRepositoryInterface $templateRepository, DocumentRepositoryInterface $documentRepository, ReferrerRepositoryInterface $referrerRepository, ShopRepositoryInterface $shopRepository, OrderCodeGenerator $codeGenerator)
    {
        $this->orderRepository = $orderRepository;

        $this->templateRepository = $templateRepository;
        $this->documentRepository = $documentRepository;
        $this->referrerRepository = $referrerRepository;
        $this->shopRepository = $shopRepository;

        $this->numberGenerator = $codeGenerator;
    }

    /**
     * @param CreateAnonymousOrderCommand $command
     */
    public function __invoke(CreateAnonymousOrderCommand $command)
    {
        $template = $this->templateRepository->getById($command->getTemplateId());

        // Bestelling aanmaken
        $order = Order::prepareAnonymous($command->getOrderId(), $this->numberGenerator->generate());
        if ($command->hasReferrerId()) {
            $order->linkReferrer($this->referrerRepository->getReference($command->getReferrerId()));
        }

        if (null !== $command->getShopId()) {
            $order->linkShop($this->shopRepository->getReference($command->getShopId()));
        }

        $this->orderRepository->persist($order);

        // Document aanmaken
        $document = Document::prepare(DocumentId::generate(), ShortTokenGenerator::generate(), $template, $order);
        $document->setTemplateVersion($template->getVersion());

        $this->documentRepository->persist($document);
    }
}
