<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\CompleteQuestionsStepCommand;
use NnShop\Domain\Orders\Enumeration\OrderTransition;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use Symfony\Component\Workflow\Workflow;

class CompleteQuestionsStepHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $repository;

    /**
     * @var Workflow
     */
    private $workflow;

    /**
     * CompleteQuestionsStateHandler constructor.
     *
     * @param OrderRepositoryInterface $repository
     * @param Workflow                 $workflow
     */
    public function __construct(OrderRepositoryInterface $repository, Workflow $workflow)
    {
        $this->repository = $repository;
        $this->workflow = $workflow;
    }

    /**
     * @param CompleteQuestionsStepCommand $command
     */
    public function __invoke(CompleteQuestionsStepCommand $command)
    {
        $order = $this->repository->getById($command->getId());
        $order->unlinkStep();

        $this->workflow->apply($order, OrderTransition::COMPLETE_QUESTIONS);

        $this->repository->persist($order);
    }
}
