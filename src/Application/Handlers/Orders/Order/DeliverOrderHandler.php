<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\Application\CoreBundle\Service\Mail\Mailer;
use Core\Component\Mail\Attachment;
use Core\Component\Mail\ContentType;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Application\AppBundle\Mail\DeliverSubscriptionOrderMail;
use NnShop\Application\AppBundle\Mail\DeliverUnitOrderMail;
use NnShop\Application\AppBundle\Services\Manager\InvoicePdfManager;
use NnShop\Domain\Orders\Command\Order\DeliverOrderCommand;
use NnShop\Domain\Orders\Enumeration\OrderStatus;
use NnShop\Domain\Orders\Enumeration\OrderType;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use Symfony\Component\Translation\TranslatorInterface;

class DeliverOrderHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var InvoicePdfManager
     */
    private $pdfManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * DeliverOrderHandler constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     * @param Mailer                   $mailer
     * @param InvoicePdfManager        $pdfManager
     * @param TranslatorInterface      $translator
     */
    public function __construct(OrderRepositoryInterface $orderRepository, Mailer $mailer, InvoicePdfManager $pdfManager, TranslatorInterface $translator)
    {
        $this->orderRepository = $orderRepository;
        $this->translator = $translator;

        $this->mailer = $mailer;
        $this->pdfManager = $pdfManager;
    }

    /**
     * @param DeliverOrderCommand $command
     */
    public function __invoke(DeliverOrderCommand $command)
    {
        $order = $this->orderRepository->getById($command->getId());

        // Controleren of er een "afleverbare" status is
        if ($order->getStatus()->notIn([OrderStatus::REQUEST, OrderStatus::DELIVER, OrderStatus::MANUAL_CHECK, OrderStatus::MANUAL_CUSTOM, OrderStatus::COMPLETED])) {
            throw new \LogicException(sprintf('It is not possible to deliver Order [%s] with a %s status', $order->getId(), $order->getStatus()->getName()));
        }

        $invoice = $order->getInvoice();
        if ($invoice->isPaid()) {
            $invoiceName = sprintf('Factuur %s (voldaan).pdf', $invoice->getCode());
        } else {
            $invoiceName = sprintf('Factuur %s.pdf', $invoice->getCode());
        }

        /*
         * Mail voor eenmalig document
         */
        if ($order->getType()->is(OrderType::UNIT)) {
            $mail = DeliverUnitOrderMail::fromOrder($order, $this->translator);
            $mail->attach(Attachment::fromRaw($invoiceName, ContentType::from('application/pdf'), $this->pdfManager->get($invoice)));

            $this->mailer->send($mail);
        }

        // Mail voor abonnement
        elseif ($order->getType()->is(OrderType::SUBSCRIPTION)) {
            $mail = DeliverSubscriptionOrderMail::fromOrder($order, $this->translator);
            $mail->attach(Attachment::fromRaw($invoiceName, ContentType::from('application/pdf'), $this->pdfManager->get($invoice)));

            $this->mailer->send($mail);
        }

        $this->orderRepository->persist($order);
    }
}
