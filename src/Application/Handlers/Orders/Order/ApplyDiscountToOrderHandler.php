<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\ApplyDiscountToOrderCommand;
use NnShop\Domain\Orders\Repository\DiscountRepositoryInterface;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;

class ApplyDiscountToOrderHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var DiscountRepositoryInterface
     */
    private $discountRepository;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @param DiscountRepositoryInterface $discountRepository
     * @param OrderRepositoryInterface    $orderRepository
     */
    public function __construct(DiscountRepositoryInterface $discountRepository, OrderRepositoryInterface $orderRepository)
    {
        $this->discountRepository = $discountRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param ApplyDiscountToOrderCommand $command
     */
    public function __invoke(ApplyDiscountToOrderCommand $command)
    {
        if ($command->hasDiscountId()) {
            $discount = $this->discountRepository->getById($command->getDiscountId());
        } elseif ($command->hasDiscountCode()) {
            $discount = $this->discountRepository->getByCode($command->getDiscountCode());
        } else {
            throw new \LogicException('ApplyDiscountToOrderCommand called without an ID and code');
        }

        $order = $this->orderRepository->getById($command->getOrderId());

        $order->applyDiscount($discount);

        $this->orderRepository->persist($order);
    }
}
