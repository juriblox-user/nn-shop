<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\RevertAccountStepCommand;
use NnShop\Domain\Orders\Enumeration\OrderTransition;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Orders\Services\Resolver\QuestionsGraph;
use NnShop\Domain\Orders\Services\Resolver\QuestionStepResolver;
use NnShop\Domain\Templates\Repository\QuestionStepRepositoryInterface;
use Symfony\Component\Workflow\Workflow;

class RevertAccountStepHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var QuestionStepRepositoryInterface
     */
    private $stepRepository;

    /**
     * @var Workflow
     */
    private $workflow;

    /**
     * RevertAccountStepHandler constructor.
     *
     * @param OrderRepositoryInterface        $orderRepository
     * @param QuestionStepRepositoryInterface $stepRepository
     * @param Workflow                        $workflow
     */
    public function __construct(OrderRepositoryInterface $orderRepository, QuestionStepRepositoryInterface $stepRepository, Workflow $workflow)
    {
        $this->orderRepository = $orderRepository;
        $this->stepRepository = $stepRepository;

        $this->workflow = $workflow;
    }

    /**
     * @param RevertAccountStepCommand $command
     */
    public function __invoke(RevertAccountStepCommand $command)
    {
        $order = $this->orderRepository->getById($command->getId());

        $this->workflow->apply($order, OrderTransition::REVERT_ACCOUNT);

        // Cache met alle vragen
        $questionsGraph = new QuestionsGraph($this->stepRepository);
        $questionsGraph->warmup($order->getDocument());

        // Resolver voor het overslaan van stappen
        $stepResolver = new QuestionStepResolver($questionsGraph);

        // Vragenlijst op de laatste stap zetten
        $order->linkStep($stepResolver->getActiveSteps()->last());

        $this->orderRepository->persist($order);
    }
}
