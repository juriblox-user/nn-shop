<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\Application\CoreBundle\Service\Mail\Mailer;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Application\AppBundle\Mail\OrderUpsellMail;
use NnShop\Domain\Orders\Command\Order\SendUpsellMailCommand;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use Symfony\Component\Translation\TranslatorInterface;

class SendUpsellMailHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * DeliverOrderHandler constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     * @param Mailer                   $mailer
     * @param TranslatorInterface      $translator
     */
    public function __construct(OrderRepositoryInterface $orderRepository, Mailer $mailer, TranslatorInterface $translator)
    {
        $this->orderRepository = $orderRepository;
        $this->translator      = $translator;
        $this->mailer          = $mailer;
    }

    /**
     * @param SendUpsellMailCommand $command
     */
    public function __invoke(SendUpsellMailCommand $command)
    {
        $order = $this->orderRepository->getById($command->getId());

        if ($order->getTimestampUpsellMailSent() !== null) {
            throw new \LogicException(sprintf('The upsell mail for order %s has already been sent', $order->getId()));
        }

        if (count($order->getTemplate()->getRelated()) === 0) {
            return;
        }

        $mail = OrderUpsellMail::fromOrder($order, $this->translator);
        $this->mailer->dontConvertCssToInlineStyles();
        $this->mailer->send($mail);
        $this->mailer->convertCssToInlineStyles();

        $order->setUpsellMailSent();
        $this->orderRepository->persist($order);
    }
}
