<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\Common\Exception\Domain\EntityLockedException;
use Core\SimpleBus\EventBusAwareInterface;
use Core\SimpleBus\EventBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Application\AppBundle\Services\Payment\MollieGateway;
use NnShop\Domain\Orders\Command\Order\FinalizeOrderCommand;
use NnShop\Domain\Orders\Entity\Invoice;
use NnShop\Domain\Orders\Enumeration\InvoiceStatus;
use NnShop\Domain\Orders\Enumeration\OrderStatus;
use NnShop\Domain\Orders\Event\Order\OrderConfirmedEvent;
use NnShop\Domain\Orders\Repository\InvoiceRepositoryInterface;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Orders\Repository\PaymentRepositoryInterface;
use NnShop\Domain\Orders\Services\Generator\InvoiceCodeGenerator;
use NnShop\Domain\Orders\Value\InvoiceId;
use RuntimeException as InvalidStateException;

class FinalizeOrderHandler implements EventBusAwareInterface, SynchronousCommandHandlerInterface
{
    use EventBusAwareTrait;

    /**
     * @var InvoiceCodeGenerator
     */
    private $invoiceCodeGenerator;

    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var PaymentRepositoryInterface
     */
    private $paymentRepository;

    /**
     * @var MollieGateway
     */
    private $mollieGateway;

    /**
     * FinalizeOrderHandler constructor.
     *
     * @param OrderRepositoryInterface   $orderRepository
     * @param InvoiceRepositoryInterface $invoiceRepository
     * @param PaymentRepositoryInterface $paymentRepository
     * @param InvoiceCodeGenerator       $invoiceCodeGenerator
     * @param MollieGateway              $mollieGateway
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        InvoiceRepositoryInterface $invoiceRepository,
        PaymentRepositoryInterface $paymentRepository,
        InvoiceCodeGenerator $invoiceCodeGenerator,
        MollieGateway $mollieGateway)
    {
        $this->orderRepository = $orderRepository;
        $this->invoiceRepository = $invoiceRepository;
        $this->paymentRepository = $paymentRepository;

        $this->invoiceCodeGenerator = $invoiceCodeGenerator;

        $this->mollieGateway = $mollieGateway;
    }

    /**
     * @param FinalizeOrderCommand $command
     *
     * @throws InvalidStateException
     * @throws \Core\Common\Exception\Domain\EntityLockedException
     * @throws \RuntimeException
     * @throws \Core\Common\Exception\NotImplementedException
     * @throws \Core\Common\Exception\Domain\DuplicateEntityException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function __invoke(FinalizeOrderCommand $command)
    {
        $order = $this->orderRepository->getById($command->getId());
        if (OrderStatus::PENDING_PAYMENT != $order->getStatus()) {
            throw new InvalidStateException('The order should have the PENDING_PAYMENT status before it can be finalized');
        }

        if ($order->isLocked()) {
            throw new EntityLockedException('The order has already been locked');
        }

        // Bestelling vergrendelen
        $order->lock();

        // Factuur aanmaken
        $invoice = Invoice::issueForOrder(InvoiceId::generate(), $this->invoiceCodeGenerator->generate(), $order);

        // Gratis bestelling
        if ($invoice->isZero()) {
            $invoice->setStatus(InvoiceStatus::from(InvoiceStatus::PAID));
        }

        $this->invoiceRepository->persist($invoice);

        // Event
        $this->eventBus->handle(OrderConfirmedEvent::create($order->getId()));

        $this->orderRepository->persist($order);
    }
}
