<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\RetryOrderCommand;
use NnShop\Domain\Orders\Enumeration\OrderStatus;
use NnShop\Domain\Orders\Enumeration\OrderTransition;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Templates\Command\Document\RequestDocumentCommand;
use Symfony\Component\Workflow\Workflow;

class RetryOrderHandler implements CommandBusAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;

    /**
     * @var OrderRepositoryInterface
     */
    private $repository;

    /**
     * @var Workflow
     */
    private $workflow;

    /**
     * ApplyOrderTransitionHandler constructor.
     *
     * @param OrderRepositoryInterface $repository
     * @param Workflow                 $workflow
     */
    public function __construct(OrderRepositoryInterface $repository, Workflow $workflow)
    {
        $this->repository = $repository;
        $this->workflow = $workflow;
    }

    /**
     * @param RetryOrderCommand $command
     */
    public function __invoke(RetryOrderCommand $command)
    {
        $order = $this->repository->getById($command->getId());

        // Document is al een keer eerder opnieuw aangevraagd
        if ($order->hadStatus(OrderStatus::from(OrderStatus::REQUEUE))) {
            $this->workflow->apply($order, OrderTransition::FAIL);
        }

        // Volgende poging om het document aan te vragen
        else {
            $this->workflow->apply($order, OrderTransition::RETRY_REQUEST);

            // Document opnieuw aanvragen
            $this->commandBus->handle(RequestDocumentCommand::create($order->getDocument()->getId()));
        }

        $this->repository->persist($order);
    }
}
