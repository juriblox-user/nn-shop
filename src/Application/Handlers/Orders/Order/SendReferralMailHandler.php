<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\Application\CoreBundle\Service\Mail\Mailer;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Application\AppBundle\Mail\OrderReferralMail;
use NnShop\Domain\Orders\Command\Order\SendReferralMailCommand;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use Symfony\Component\Translation\TranslatorInterface;

class SendReferralMailHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * DeliverOrderHandler constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     * @param Mailer                   $mailer
     * @param TranslatorInterface      $translator
     */
    public function __construct(OrderRepositoryInterface $orderRepository, Mailer $mailer, TranslatorInterface $translator)
    {
        $this->orderRepository = $orderRepository;
        $this->translator      = $translator;
        $this->mailer          = $mailer;
    }

    /**
     * @param SendReferralMailCommand $command
     */
    public function __invoke(SendReferralMailCommand $command)
    {
        $order = $this->orderRepository->getById($command->getId());

        if ($order->getTimestampReferralMailSent() !== null) {
            throw new \LogicException(sprintf('The Referral mail for order %s has already been sent', $order->getId()));
        }

        $mail = OrderReferralMail::fromOrder($order, $this->translator);
        $this->mailer->dontConvertCssToInlineStyles();
        $this->mailer->send($mail);
        $this->mailer->convertCssToInlineStyles();

        $order->setReferralMailSent();
        $this->orderRepository->persist($order);
    }
}
