<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\DeliverOrderCommand;
use NnShop\Domain\Orders\Command\Order\ProcessOrderDeliveryCommand;
use NnShop\Domain\Orders\Command\Order\ProcessOrderGenerationCommand;
use NnShop\Domain\Orders\Command\Order\RefreshOrderCommand;
use NnShop\Domain\Orders\Command\Order\RepairOrderCommand;
use NnShop\Domain\Orders\Command\Subscription\CreateSubscriptionCommand;
use NnShop\Domain\Orders\Command\Subscription\RefreshLinksCommand;
use NnShop\Domain\Orders\Enumeration\OrderStatus;
use NnShop\Domain\Orders\Enumeration\OrderType;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Orders\Repository\SubscriptionRepositoryInterface;
use NnShop\Domain\Orders\Value\SubscriptionId;
use NnShop\Domain\Templates\Command\Document\CacheDocumentCommand;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class RepairOrderHandler implements CommandBusAwareInterface, LoggerAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;
    use LoggerAwareTrait;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var SubscriptionRepositoryInterface
     */
    private $subscriptionRepository;

    /**
     * RepairOrderHandler constructor.
     *
     * @param OrderRepositoryInterface        $orderRepository
     * @param SubscriptionRepositoryInterface $subscriptionRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository, SubscriptionRepositoryInterface $subscriptionRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->subscriptionRepository = $subscriptionRepository;
    }

    /**
     * @param RepairOrderCommand $command
     */
    public function __invoke(RepairOrderCommand $command)
    {
        $order = $this->orderRepository->getById($command->getId());
        $document = $order->getDocument();

        // Geforceerde refresh uitvoeren
        $command = RefreshOrderCommand::create($order->getId());
        $command->force();

        $this->commandBus->handle($command);

        /*
         * Controleren of er een afgerond DocumentRequest is
         */
        if ($document->hasRequests()) {
            $completed = false;

            foreach ($document->getRequests() as $request) {
                if ($request->isCompleted()) {
                    $completed = true;

                    break;
                }
            }

            if ($completed) {
                $this->commandBus->handle(ProcessOrderGenerationCommand::create($order->getId()));
            }
        }

        // Bestelling downloaden, als dat mogelijk is
        if ($document->getStatus()->isSuccess() && !$document->isCached()) {
            $this->commandBus->handle(CacheDocumentCommand::create($document->getId()));
        }

        /*
         * Bestelling afleveren
         */
        if ($order->getStatus()->is(OrderStatus::DELIVER) && !$order->isDelivered()) {
            $this->commandBus->handle(DeliverOrderCommand::create($order->getId()));

            // Controleren of het abonnement is aangemaakt
            if ($order->getType()->is(OrderType::SUBSCRIPTION)) {
                $subscription = $this->subscriptionRepository->findOneByOrder($order);

                // Abonnement
                if (null === $subscription) {
                    $this->commandBus->handle(CreateSubscriptionCommand::create(SubscriptionId::generate(), $order->getId()));
                }
            }

            // Vervolgstap na het afleveren
            $this->commandBus->handle(ProcessOrderDeliveryCommand::create($order->getId()));
        }

        // Download-links voor abonnementen vernieuwen
        if ($order->getType()->is(OrderType::SUBSCRIPTION)) {
            $subscription = $this->subscriptionRepository->getByOrder($order);

            $this->commandBus->handle(RefreshLinksCommand::create($subscription->getId()));
        }
    }
}
