<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\CompleteConfirmStepCommand;
use NnShop\Domain\Orders\Command\Order\FinalizeOrderCommand;
use NnShop\Domain\Orders\Enumeration\OrderTransition;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use Symfony\Component\Workflow\Workflow;

class CompleteConfirmStepHandler implements CommandBusAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;

    /**
     * @var OrderRepositoryInterface
     */
    private $repository;

    /**
     * @var Workflow
     */
    private $workflow;

    /**
     * CompleteIdentifyStateHandler constructor.
     *
     * @param OrderRepositoryInterface $repository
     * @param Workflow                 $workflow
     */
    public function __construct(OrderRepositoryInterface $repository, Workflow $workflow)
    {
        $this->repository = $repository;
        $this->workflow = $workflow;
    }

    /**
     * @param CompleteConfirmStepCommand $command
     */
    public function __invoke(CompleteConfirmStepCommand $command)
    {
        $order = $this->repository->getById($command->getId());

        // Controleren of we naar de volgende stap mogen
        $this->workflow->apply($order, OrderTransition::COMPLETE_CONFIRM);

        // Betaalmethode instellen
        $order->setPaymentMethod($command->getPaymentMethod());
        $this->repository->persist($order);

        // Bestelling afronden
        $this->commandBus->handle(FinalizeOrderCommand::create($order->getId()));
    }
}
