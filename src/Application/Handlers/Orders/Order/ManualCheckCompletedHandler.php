<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\ManualCheckCompletedCommand;
use NnShop\Domain\Orders\Enumeration\OrderTransition;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use Symfony\Component\Workflow\Workflow;

class ManualCheckCompletedHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var Workflow
     */
    private $workflow;

    /**
     * ManualCheckCompletedHandler constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     * @param Workflow                 $workflow
     */
    public function __construct(OrderRepositoryInterface $orderRepository, Workflow $workflow)
    {
        $this->orderRepository = $orderRepository;
        $this->workflow = $workflow;
    }

    /**
     * @param ManualCheckCompletedCommand $command
     */
    public function __invoke(ManualCheckCompletedCommand $command)
    {
        $order = $this->orderRepository->getById($command->getId());

        // Door naar de maatwerk aanpassingen
        if ($order->isCustomRequested()) {
            $this->workflow->apply($order, OrderTransition::CHECK_TO_CUSTOM);
        }

        // Afronden
        else {
            $this->workflow->apply($order, OrderTransition::COMPLETE_CHECK);
        }

        $this->orderRepository->persist($order);
    }
}
