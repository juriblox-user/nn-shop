<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\ChangeOrderPaymentMethodCommand;
use NnShop\Domain\Orders\Enumeration\OrderStatus;
use NnShop\Domain\Orders\Enumeration\PaymentStatus;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Orders\Repository\PaymentRepositoryInterface;

class ChangeOrderPaymentMethodHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var PaymentRepositoryInterface
     */
    private $paymentRepository;

    /**
     * ApplyOrderTransitionHandler constructor.
     *
     * @param OrderRepositoryInterface   $orderRepository
     * @param PaymentRepositoryInterface $paymentRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository, PaymentRepositoryInterface $paymentRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->paymentRepository = $paymentRepository;
    }

    /**
     * @param ChangeOrderPaymentMethodCommand $command
     */
    public function __invoke(ChangeOrderPaymentMethodCommand $command)
    {
        $order = $this->orderRepository->getById($command->getId());
        if ($order->getStatus()->isNot(OrderStatus::PENDING_PAYMENT)) {
            throw new \DomainException('Cannot change the payment method for an Order with status ' . $order->getStatus()->getName());
        }

        /*
         * Lopende betalingen afbreken
         */
        $payments = $order->getInvoice()->getPendingPayments();

        foreach ($payments as $payment) {
            $payment->setStatus(PaymentStatus::from(PaymentStatus::CANCELED));

            $this->paymentRepository->persist($payment);
        }

        // Betaalmethode van bestelling op NULL
        $order->setPaymentMethod(null);

        $this->orderRepository->persist($order);
    }
}
