<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use Doctrine\ORM\EntityNotFoundException;
use NnShop\Domain\Orders\Command\Order\DeleteDiscountFromOrderCommand;
use NnShop\Domain\Orders\Repository\DiscountRepositoryInterface;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;

class DeleteDiscountFromOrderHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var DiscountRepositoryInterface
     */
    private $discountRepository;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @param DiscountRepositoryInterface $discountRepository
     * @param OrderRepositoryInterface    $orderRepository
     */
    public function __construct(DiscountRepositoryInterface $discountRepository, OrderRepositoryInterface $orderRepository)
    {
        $this->discountRepository = $discountRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param DeleteDiscountFromOrderCommand $command
     *
     * @throws EntityNotFoundException
     */
    public function __invoke(DeleteDiscountFromOrderCommand $command)
    {
        $discount = $this->discountRepository->getById($command->getDiscountId());

        $order = $this->orderRepository->getById($command->getOrderId());

        $order->removeDiscount($discount);

        $this->orderRepository->persist($order);
    }
}
