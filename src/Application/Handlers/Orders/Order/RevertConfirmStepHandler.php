<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\RevertConfirmStepCommand;
use NnShop\Domain\Orders\Enumeration\OrderTransition;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use Symfony\Component\Workflow\Workflow;

class RevertConfirmStepHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $repository;

    /**
     * @var Workflow
     */
    private $workflow;

    /**
     * RevertConfirmStepHandler constructor.
     *
     * @param OrderRepositoryInterface $repository
     * @param Workflow                 $workflow
     */
    public function __construct(OrderRepositoryInterface $repository, Workflow $workflow)
    {
        $this->repository = $repository;
        $this->workflow = $workflow;
    }

    /**
     * @param RevertConfirmStepCommand $command
     */
    public function __invoke(RevertConfirmStepCommand $command)
    {
        $order = $this->repository->getById($command->getId());

        if ($order->isLocked()) {
            throw new \LogicException('It is not possible to edit a locked order');
        }

        $this->workflow->apply($order, OrderTransition::REVERT_CONFIRM);

        $this->repository->persist($order);
    }
}
