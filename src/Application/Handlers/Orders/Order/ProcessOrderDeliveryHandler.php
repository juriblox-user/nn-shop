<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\ProcessOrderDeliveryCommand;
use NnShop\Domain\Orders\Enumeration\OrderStatus;
use NnShop\Domain\Orders\Enumeration\OrderTransition;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use Symfony\Component\Workflow\Workflow;

class ProcessOrderDeliveryHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var Workflow
     */
    private $workflow;

    /**
     * ProcessOrderDeliveryHandler constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     * @param Workflow                 $workflow
     */
    public function __construct(OrderRepositoryInterface $orderRepository, Workflow $workflow)
    {
        $this->orderRepository = $orderRepository;
        $this->workflow = $workflow;
    }

    /**
     * @param ProcessOrderDeliveryCommand $command
     */
    public function __invoke(ProcessOrderDeliveryCommand $command)
    {
        $order = $this->orderRepository->getById($command->getId());

        // Status controleren
        if ($order->getStatus()->isNot(OrderStatus::DELIVER)) {
            throw new \LogicException(sprintf('It is not possible to process a delivery for Order [%s] with a %s status', $order->getId(), $order->getStatus()->getName()));
        }

        // Door naar handmatige controle
        if ($order->isCheckRequested()) {
            $this->workflow->apply($order, OrderTransition::MANUAL_CHECK);
        }

        // Door naar maatwerk aanpassingen
        elseif ($order->isCustomRequested()) {
            $this->workflow->apply($order, OrderTransition::MANUAL_CUSTOM);
        }

        // Geen vervolgstap meer: afronden
        else {
            $this->workflow->apply($order, OrderTransition::COMPLETE);
        }

        $this->orderRepository->persist($order);
    }
}
