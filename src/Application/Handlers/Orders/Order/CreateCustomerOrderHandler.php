<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\Common\Generator\Security\ShortTokenGenerator;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\CreateCustomerOrderCommand;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Orders\Services\Generator\OrderCodeGenerator;
use NnShop\Domain\Shops\Repository\ReferrerRepositoryInterface;
use NnShop\Domain\Shops\Repository\ShopRepositoryInterface;
use NnShop\Domain\Templates\Entity\Document;
use NnShop\Domain\Templates\Repository\DocumentRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use NnShop\Domain\Templates\Value\DocumentId;

class CreateCustomerOrderHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var DocumentRepositoryInterface
     */
    private $documentRepository;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var ReferrerRepositoryInterface
     */
    private $referrerRepository;

    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * @var OrderCodeGenerator
     */
    private $numberGenerator;

    /**
     * @var ShopRepositoryInterface
     */
    private $shopRepository;

    /**
     * CreateOrderHandler constructor.
     *
     * @param OrderRepositoryInterface    $orderRepository
     * @param CustomerRepositoryInterface $customerRepository
     * @param TemplateRepositoryInterface $templateRepository
     * @param DocumentRepositoryInterface $documentRepository
     * @param ReferrerRepositoryInterface $referrerRepository
     * @param ShopRepositoryInterface     $shopRepository
     * @param OrderCodeGenerator          $codeGenerator
     */
    public function __construct(OrderRepositoryInterface $orderRepository, CustomerRepositoryInterface $customerRepository, TemplateRepositoryInterface $templateRepository, DocumentRepositoryInterface $documentRepository, ReferrerRepositoryInterface $referrerRepository, ShopRepositoryInterface $shopRepository, OrderCodeGenerator $codeGenerator)
    {
        $this->orderRepository = $orderRepository;

        $this->customerRepository = $customerRepository;
        $this->templateRepository = $templateRepository;
        $this->documentRepository = $documentRepository;
        $this->referrerRepository = $referrerRepository;
        $this->shopRepository = $shopRepository;

        $this->numberGenerator = $codeGenerator;
    }

    /**
     * @param CreateCustomerOrderCommand $command
     */
    public function __invoke(CreateCustomerOrderCommand $command)
    {
        $customer = $this->customerRepository->getById($command->getCustomerId());
        $template = $this->templateRepository->getById($command->getTemplateId());

        // Bestelling aanmaken
        $order = Order::prepareWithCustomer($command->getOrderId(), $this->numberGenerator->generate(), $customer);
        if ($command->hasReferrerId()) {
            $order->linkReferrer($this->referrerRepository->getReference($command->getReferrerId()));
        }

        if (null !== $command->getShopId()) {
            $order->linkShop($this->shopRepository->getReference($command->getShopId()));
        }

        $this->orderRepository->persist($order);

        // Document aanmaken
        $document = Document::prepare(DocumentId::generate(), ShortTokenGenerator::generate(), $template, $order);
        $document->setTemplateVersion($template->getVersion());

        $this->documentRepository->persist($document);
    }
}
