<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\SetQuestionsStepCommand;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Templates\Repository\QuestionStepRepositoryInterface;

class SetQuestionsStepHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var QuestionStepRepositoryInterface
     */
    private $stepRepository;

    /**
     * SetQuestionsStepHandler constructor.
     *
     * @param OrderRepositoryInterface        $orderRepository
     * @param QuestionStepRepositoryInterface $stepRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository, QuestionStepRepositoryInterface $stepRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->stepRepository = $stepRepository;
    }

    /**
     * @param SetQuestionsStepCommand $command
     */
    public function __invoke(SetQuestionsStepCommand $command)
    {
        $order = $this->orderRepository->getById($command->getOrderId());
        $step = $this->stepRepository->getReference($command->getStepId());

        $order->linkStep($step);

        $this->orderRepository->persist($order);
    }
}
