<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\CompleteOptionsStepCommand;
use NnShop\Domain\Orders\Enumeration\OrderTransition;
use NnShop\Domain\Orders\Enumeration\OrderType;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use Symfony\Component\Workflow\Workflow;

class CompleteOptionsStepHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $repository;

    /**
     * @var Workflow
     */
    private $workflow;

    /**
     * CompleteIdentifyStateHandler constructor.
     *
     * @param OrderRepositoryInterface $repository
     * @param Workflow                 $workflow
     */
    public function __construct(OrderRepositoryInterface $repository, Workflow $workflow)
    {
        $this->repository = $repository;
        $this->workflow = $workflow;
    }

    /**
     * @param CompleteOptionsStepCommand $command
     */
    public function __invoke(CompleteOptionsStepCommand $command)
    {
        if ($command->getType()->notIn([OrderType::UNIT, OrderType::SUBSCRIPTION])) {
            throw new \DomainException('The order type has to be set to either "UNIT" or "SUBSCRIPTION"');
        }

        $order = $this->repository->getById($command->getId());
        $order->setType($command->getType());

        // Abonnementen kunnen geen aangepaste documenten krijgen
        if ($command->getType()->is(OrderType::SUBSCRIPTION) && $command->getCustom()) {
            throw new \DomainException('It is not possible to request custom modifications for "SUBSCRIPTION" type orders');
        }

        $command->getCheck() ? $order->requestCheck() : $order->cancelCheck();
        $command->getCustom() ? $order->requestCustom() : $order->cancelCustom();

        $command->getRegisteredEmail() ? $order->requestRegisteredEmail() : $order->cancelRegisteredEmailRequest();

        $this->workflow->apply($order, OrderTransition::COMPLETE_OPTIONS);

        $this->repository->persist($order);
    }
}
