<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\PrepareCustomerForOrderCommand;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Enumeration\CustomerType;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Orders\Value\CustomerId;
use NnShop\Domain\Translation\Repository\LocaleRepositoryInterface;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;

class PrepareCustomerForOrderHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var LocaleRepositoryInterface
     */
    private $localeRepository;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var ProfileRepositoryInterface
     */
    private $profileRepository;

    /**
     * AssociateCustomerWithOrderHandler constructor.
     *
     * @param OrderRepositoryInterface    $orderRepository
     * @param CustomerRepositoryInterface $customerRepository
     * @param ProfileRepositoryInterface  $profileRepository
     * @param LocaleRepositoryInterface   $localeRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository, CustomerRepositoryInterface $customerRepository, ProfileRepositoryInterface $profileRepository, LocaleRepositoryInterface $localeRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->customerRepository = $customerRepository;

        $this->profileRepository = $profileRepository;
        $this->localeRepository = $localeRepository;
    }

    /**
     * @param PrepareCustomerForOrderCommand $command
     */
    public function __invoke(PrepareCustomerForOrderCommand $command)
    {
        $order = $this->orderRepository->getById($command->getOrderId());

        $profile = $this->profileRepository->getReference($command->getProfileId());
        $locale = $this->localeRepository->getReference($command->getLocaleId());

        $customer = $this->customerRepository->findOneByEmail($command->getEmailAddress());
        if (null === $customer) {
            $customer = Customer::create(CustomerId::generate(), $command->getCustomerType(), $command->getEmailAddress(), $profile, $locale);
        }

        $customer->setType($command->getCustomerType());

        if ($customer->getType()->is(CustomerType::BUSINESS)) {
            $customer->setCompanyName($command->getCompanyName());
        }

        $customer->setFirstname($command->getFirstname());
        $customer->setLastname($command->getLastname());
        $customer->setAddress($command->getAddress());
        $customer->setPhone($command->getPhone());
        $customer->setVat($command->getVat());
        $customer->setCocNumber($command->getCoc());

        // Bestelling koppelen aan deze klant (let op, hier niet Order::claim() gebruiken!)
        if (!$customer->hasOrder($order)) {
            $order->linkCustomer($customer);
        }

        $this->customerRepository->persist($customer);
        $this->orderRepository->persist($order);
    }
}
