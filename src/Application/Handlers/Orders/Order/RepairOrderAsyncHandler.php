<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\AsynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\RepairOrderAsyncCommand;

class RepairOrderAsyncHandler implements CommandBusAwareInterface, AsynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;

    /**
     * @param RepairOrderAsyncCommand $command
     */
    public function __invoke(RepairOrderAsyncCommand $command)
    {
        $this->commandBus->handle($command->getCommand());
    }
}
