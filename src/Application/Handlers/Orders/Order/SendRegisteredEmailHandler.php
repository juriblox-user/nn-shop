<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Application\AppBundle\Services\Manager\DocumentFileManager;
use NnShop\Domain\Orders\Command\Order\SendRegisteredEmailCommand;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;

final class SendRegisteredEmailHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var DocumentFileManager
     */
    private $documentManager;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var string|null
     */
    private $relayHost;

    public function __construct(OrderRepositoryInterface $orderRepository, DocumentFileManager $documentManager, \Swift_Mailer $mailer)
    {
        $this->orderRepository = $orderRepository;

        $this->documentManager = $documentManager;
        $this->mailer = $mailer;
    }

    public function setRelayHost(?string $relayHost): void
    {
        $this->relayHost = $relayHost ?: null;
    }

    public function __invoke(SendRegisteredEmailCommand $command): void
    {
        $order = $this->orderRepository->getById($command->getId());

        if (null === $this->relayHost || 0 === preg_match('/^[a-z0-9\.\-]+$/', $this->relayHost)) {
            throw new \RuntimeException('No relay host has been configured for registered e-mail.');
        }

        if (!$order->isRegisteredEmailRequested()) {
            throw new \DomainException(sprintf('Cannot send registered e-mail for Order#%s: no registered e-mail requested while ordering.', $order->getId()));
        }

        if ($order->isRegisteredEmailSent()) {
            throw new \DomainException(sprintf('Cannot send registered e-mail for Order#%s: registered e-mail has already been sent.', $order->getId()));
        }

        $document = $order->getDocument();
        $template = $order->getTemplate();

        if (!$document->isCached()) {
            throw new \DomainException(sprintf('Cannot send registered e-mail for Order#%s: document has not yet been cached.', $order->getId()));
        }

        $message = (new \Swift_Message($command->getSubject()))
            ->setFrom('no-reply@legalfit.eu')
            ->setTo($command->getEmail()->getString() . '.' . $this->relayHost)

            ->setReplyTo($order->getCustomer()->getEmail()->getString(), $order->getCustomer()->getCompanyName() ?: $order->getCustomer()->getName())

            ->setBody($command->getMessage(), 'text/plain')
            ->attach(new \Swift_Attachment($this->documentManager->getPdf($document), $template->getTitle() . '.pdf'));

        $this->mailer->send($message);

        // Opslaan dat aangetekende mail is verstuurd
        $order->sentRegisteredEmail($command->getEmail());

        $this->orderRepository->persist($order);
    }
}
