<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\EventBusAwareInterface;
use Core\SimpleBus\EventBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\CancelOrderCommand;
use NnShop\Domain\Orders\Enumeration\OrderTransition;
use NnShop\Domain\Orders\Event\OrderWasCanceled;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use Symfony\Component\Workflow\Workflow;

class CancelOrderHandler implements EventBusAwareInterface, SynchronousCommandHandlerInterface
{
    use EventBusAwareTrait;

    /**
     * @var OrderRepositoryInterface
     */
    private $repository;

    /**
     * @var Workflow
     */
    private $workflow;

    /**
     * ApplyOrderTransitionHandler constructor.
     *
     * @param OrderRepositoryInterface $repository
     * @param Workflow                 $workflow
     */
    public function __construct(OrderRepositoryInterface $repository, Workflow $workflow)
    {
        $this->repository = $repository;
        $this->workflow = $workflow;
    }

    public function __invoke(CancelOrderCommand $command)
    {
        $order = $this->repository->getById($command->getId());

        $this->workflow->apply($order, OrderTransition::CANCEL);

        // Event trigger
        $this->eventBus->handle(new OrderWasCanceled($order->getId()));

        $this->repository->persist($order);
    }
}
