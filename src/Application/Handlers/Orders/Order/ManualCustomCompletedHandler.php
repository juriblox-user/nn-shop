<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\ManualCustomCompletedCommand;
use NnShop\Domain\Orders\Enumeration\OrderTransition;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use Symfony\Component\Workflow\Workflow;

class ManualCustomCompletedHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var Workflow
     */
    private $workflow;

    /**
     * ManualCustomCompletedHandler constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     * @param Workflow                 $workflow
     */
    public function __construct(OrderRepositoryInterface $orderRepository, Workflow $workflow)
    {
        $this->orderRepository = $orderRepository;
        $this->workflow = $workflow;
    }

    /**
     * @param ManualCustomCompletedCommand $command
     */
    public function __invoke(ManualCustomCompletedCommand $command)
    {
        $order = $this->orderRepository->getById($command->getId());

        $this->workflow->apply($order, OrderTransition::COMPLETE_CUSTOM);

        $this->orderRepository->persist($order);
    }
}
