<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\DeliverOrderCommand;
use NnShop\Domain\Orders\Command\Order\ProcessOrderDeliveryCommand;
use NnShop\Domain\Orders\Command\Order\ProcessOrderGenerationCommand;
use NnShop\Domain\Orders\Command\Subscription\CreateSubscriptionCommand;
use NnShop\Domain\Orders\Enumeration\OrderTransition;
use NnShop\Domain\Orders\Enumeration\OrderType;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Orders\Value\SubscriptionId;
use Symfony\Component\Workflow\Workflow;

class ProcessOrderGenerationHandler implements CommandBusAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var Workflow
     */
    private $workflow;

    /**
     * ProcessOrderGenerationHandler constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     * @param Workflow                 $workflow
     */
    public function __construct(OrderRepositoryInterface $orderRepository, Workflow $workflow)
    {
        $this->orderRepository = $orderRepository;
        $this->workflow = $workflow;
    }

    /**
     * @param ProcessOrderGenerationCommand $command
     */
    public function __invoke(ProcessOrderGenerationCommand $command)
    {
        $order = $this->orderRepository->getById($command->getId());

        // Status naar afleveren
        $this->workflow->apply($order, OrderTransition::DELIVER);

        // Mail versturen (deze doet niks met de workflow, kan namelijk door klantenservice meerdere malen worden verstuurd)
        $this->commandBus->handle(DeliverOrderCommand::create($order->getId()));

        // Abonnement aanmaken
        if ($order->getType()->is(OrderType::SUBSCRIPTION)) {
            $this->commandBus->handle(CreateSubscriptionCommand::create(SubscriptionId::generate(), $order->getId()));
        }

        // Vervolgstap na het afleveren
        $this->commandBus->handle(ProcessOrderDeliveryCommand::create($order->getId()));

        $this->orderRepository->persist($order);
    }
}
