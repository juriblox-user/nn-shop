<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\ProcessOrderPaymentCommand;
use NnShop\Domain\Orders\Enumeration\OrderTransition;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Templates\Command\Document\RequestDocumentCommand;
use Symfony\Component\Workflow\Workflow;

class ProcessOrderPaymentHandler implements CommandBusAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var Workflow
     */
    private $workflow;

    /**
     * ProcessOrderPaymentHandler constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     * @param Workflow                 $workflow
     */
    public function __construct(OrderRepositoryInterface $orderRepository, Workflow $workflow)
    {
        $this->orderRepository = $orderRepository;
        $this->workflow = $workflow;
    }

    /**
     * @param ProcessOrderPaymentCommand $command
     */
    public function __invoke(ProcessOrderPaymentCommand $command)
    {
        $order = $this->orderRepository->getById($command->getId());

        // Documentaanvraag wordt ingediend
        $this->workflow->apply($order, OrderTransition::REQUEST);

        // Document aanvragen
        $this->commandBus->handle(RequestDocumentCommand::create($order->getDocument()->getId()));

        // Documentaanvraag is nu verstuurd
        $this->workflow->apply($order, OrderTransition::REQUEST_SENT);

        $this->orderRepository->persist($order);
    }
}
