<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Order\RevertOptionsStepCommand;
use NnShop\Domain\Orders\Enumeration\OrderTransition;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use Symfony\Component\Workflow\Workflow;

class RevertOptionsStepHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $repository;

    /**
     * @var Workflow
     */
    private $workflow;

    /**
     * RevertOptionsStepHandler constructor.
     *
     * @param OrderRepositoryInterface $repository
     * @param Workflow                 $workflow
     */
    public function __construct(OrderRepositoryInterface $repository, Workflow $workflow)
    {
        $this->repository = $repository;
        $this->workflow = $workflow;
    }

    /**
     * @param RevertOptionsStepCommand $command
     */
    public function __invoke(RevertOptionsStepCommand $command)
    {
        $order = $this->repository->getById($command->getId());

        $this->workflow->apply($order, OrderTransition::REVERT_OPTIONS);

        $this->repository->persist($order);
    }
}
