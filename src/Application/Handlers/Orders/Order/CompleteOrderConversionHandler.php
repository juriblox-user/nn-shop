<?php

namespace NnShop\Application\Handlers\Orders\Order;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Attribute\OrderAttribute;
use NnShop\Domain\Orders\Command\Order\CompleteOrderConversionCommand;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;

class CompleteOrderConversionHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param CompleteOrderConversionCommand $command
     */
    public function __invoke(CompleteOrderConversionCommand $command)
    {
        $order = $this->orderRepository->getById($command->getId());

        switch ($command->getAttribute()) {
            case OrderAttribute::ADWORDS_CONVERSION_TRACKED:
                $order->setAdwordsConversionTracked(true);

                break;

            case OrderAttribute::GOOGLE_CONVERSION_TRACKED:
                $order->setGoogleConversionTracked(true);

                break;

            case OrderAttribute::TWITTER_CONVERSION_TRACKED:
                $order->setTwitterConversionTracked(true);

                break;

            default:
                throw new \InvalidArgumentException(sprintf('Unknown conversion "%s"', $command->getAttribute()));
        }

        $this->orderRepository->persist($order);
    }
}
