<?php

namespace NnShop\Application\Handlers\Orders\Payment;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Payment\SynchronizePendingPaymentsCommand;
use NnShop\Domain\Orders\Command\Payment\SynchronizeTransactionCommand;
use NnShop\Domain\Orders\Repository\InvoiceRepositoryInterface;
use NnShop\Domain\Orders\Repository\PaymentRepositoryInterface;

class SynchronizePendingPaymentsHandler implements CommandBusAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;

    /**
     * @var PaymentRepositoryInterface
     */
    private $paymentRepository;

    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * SynchronizePendingTransactionsHandler constructor.
     *
     * @param PaymentRepositoryInterface $paymentRepository
     * @param InvoiceRepositoryInterface $invoiceRepository
     */
    public function __construct(PaymentRepositoryInterface $paymentRepository, InvoiceRepositoryInterface $invoiceRepository)
    {
        $this->paymentRepository = $paymentRepository;
        $this->invoiceRepository = $invoiceRepository;
    }

    /**
     * @param SynchronizePendingPaymentsCommand $command
     */
    public function __invoke(SynchronizePendingPaymentsCommand $command)
    {
        /*
         * Betalingen van een bepaalde factuur synchroniseren
         */
        if (null !== $command->getInvoiceId()) {
            $invoice = $this->invoiceRepository->getById($command->getInvoiceId());

            foreach ($invoice->getPendingPayments() as $payment) {
                $this->commandBus->handle(SynchronizeTransactionCommand::create($payment->getTransactionId()));
            }
        }

        /*
         * Alle betalingen synchroniseren
         */
        else {
            foreach ($this->paymentRepository->findPending() as $payment) {
                $this->commandBus->handle(SynchronizeTransactionCommand::create($payment->getTransactionId()));
            }
        }
    }
}
