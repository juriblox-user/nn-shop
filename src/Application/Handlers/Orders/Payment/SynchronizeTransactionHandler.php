<?php

namespace NnShop\Application\Handlers\Orders\Payment;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Application\AppBundle\Services\Payment\MollieGateway;
use NnShop\Domain\Orders\Command\Payment\SynchronizeTransactionCommand;
use NnShop\Domain\Orders\Repository\PaymentRepositoryInterface;

class SynchronizeTransactionHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var PaymentRepositoryInterface
     */
    private $repository;

    /**
     * @var MollieGateway
     */
    private $mollie;

    /**
     * SynchronizeTransactionHandler constructor.
     *
     * @param PaymentRepositoryInterface $repository
     * @param MollieGateway              $mollie
     */
    public function __construct(PaymentRepositoryInterface $repository, MollieGateway $mollie)
    {
        $this->repository = $repository;
        $this->mollie = $mollie;
    }

    /**
     * @param SynchronizeTransactionCommand $command
     *
     * @throws \Core\Common\Exception\NotImplementedException
     * @throws \Core\Common\Exception\SecurityException
     * @throws \Mollie\Api\Exceptions\ApiException
     */
    public function __invoke(SynchronizeTransactionCommand $command)
    {
        $payment = $this->repository->getByTransactionId($command->getId());

        $this->mollie->synchronizePayment($payment);

        $this->repository->persist($payment);
    }
}
