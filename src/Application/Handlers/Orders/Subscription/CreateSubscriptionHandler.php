<?php

namespace NnShop\Application\Handlers\Orders\Subscription;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use Core\SimpleBus\EventBusAwareInterface;
use Core\SimpleBus\EventBusAwareTrait;
use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use NnShop\Domain\Orders\Command\Subscription\CreateSubscriptionCommand;
use NnShop\Domain\Orders\Command\Subscription\RefreshLinksCommand;
use NnShop\Domain\Orders\Entity\Subscription;
use NnShop\Domain\Orders\Enumeration\OrderType;
use NnShop\Domain\Orders\Event\Subscription\SubscriptionReadyEvent;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Orders\Repository\SubscriptionRepositoryInterface;
use NnShop\Domain\Orders\Services\Generator\SubscriptionCodeGenerator;

class CreateSubscriptionHandler implements CommandBusAwareInterface, EventBusAwareInterface, SynchronousCommandHandlerInterface
{
    use CommandBusAwareTrait;
    use EventBusAwareTrait;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var SubscriptionRepositoryInterface
     */
    private $subscriptionRepository;

    /**
     * @var SubscriptionCodeGenerator
     */
    private $codeGenerator;

    /**
     * CreateSubscriptionHandler constructor.
     *
     * @param SubscriptionRepositoryInterface $subscriptionRepository
     * @param OrderRepositoryInterface        $orderRepository
     * @param SubscriptionCodeGenerator       $generator
     */
    public function __construct(SubscriptionRepositoryInterface $subscriptionRepository, OrderRepositoryInterface $orderRepository, SubscriptionCodeGenerator $generator)
    {
        $this->subscriptionRepository = $subscriptionRepository;
        $this->orderRepository = $orderRepository;

        $this->codeGenerator = $generator;
    }

    /**
     * @param CreateSubscriptionCommand $command
     */
    public function __invoke(CreateSubscriptionCommand $command)
    {
        $order = $this->orderRepository->getById($command->getOrder());
        if ($order->getType()->isNot(OrderType::SUBSCRIPTION)) {
            throw new \LogicException(sprintf('You cannot create a subscription for a "%s" type order', $order->getType()));
        }

        // Abonnement aanmaken
        $subscription = Subscription::prepare($command->getId(), $this->codeGenerator->generate(), $order);

        // Links laten aanmaken
        $this->commandBus->handle(RefreshLinksCommand::create($subscription->getId()));

        // Event pas triggeren nadat alles compleet is
        $this->eventBus->handle(SubscriptionReadyEvent::create($subscription->getId()));

        $this->subscriptionRepository->persist($subscription);
    }
}
