<?php

namespace NnShop\Application\Handlers\Orders\Subscription;

use Core\SimpleBus\Marker\SynchronousCommandHandlerInterface;
use Doctrine\Common\Collections\Criteria;
use NnShop\Domain\Orders\Command\Subscription\RefreshLinksCommand;
use NnShop\Domain\Orders\Entity\SubscriptionLink;
use NnShop\Domain\Orders\Enumeration\OrderStatus;
use NnShop\Domain\Orders\Enumeration\SubscriptionLinkFormat;
use NnShop\Domain\Orders\Repository\SubscriptionRepositoryInterface;
use NnShop\Domain\Orders\Services\Generator\SubscriptionLinkTokenGenerator;
use NnShop\Domain\Orders\Value\SubscriptionLinkId;

class RefreshLinksHandler implements SynchronousCommandHandlerInterface
{
    /**
     * @var SubscriptionRepositoryInterface
     */
    private $subscriptionRepository;

    /**
     * @var SubscriptionLinkTokenGenerator
     */
    private $tokenGenerator;

    /**
     * RefreshLinksHandler constructor.
     *
     * @param SubscriptionRepositoryInterface $subscriptionRepository
     * @param SubscriptionLinkTokenGenerator  $tokenGenerator
     */
    public function __construct(SubscriptionRepositoryInterface $subscriptionRepository, SubscriptionLinkTokenGenerator $tokenGenerator)
    {
        $this->subscriptionRepository = $subscriptionRepository;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @param RefreshLinksCommand $command
     */
    public function __invoke(RefreshLinksCommand $command)
    {
        $subscription = $this->subscriptionRepository->getById($command->getId());

        $order = $subscription->getOrder();

        // Laatste afgeronde order bepalen
        $matches = $subscription->getUpdateOrders()->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('status', OrderStatus::from(OrderStatus::COMPLETED))
            )
        );

        if (!$matches->isEmpty()) {
            $order = $matches->first();
        }

        foreach (SubscriptionLinkFormat::all() as $format) {
            $link = $subscription->findLink($format);
            if (null === $link) {
                SubscriptionLink::create(SubscriptionLinkId::generate(), $format, $this->tokenGenerator->generate(), $subscription, $order->getDocument());
            } else {
                $link->linkDocument($order->getDocument());
            }
        }

        // Overbodige links opschonen
        foreach ($subscription->getLinks() as $link) {
            if ($link->getFormat()->notIn(SubscriptionLinkFormat::all())) {
                $subscription->removeLink($link);
            }
        }

        $this->subscriptionRepository->persist($subscription);
    }
}
