<?php

namespace NnShop\Application\AppBundle\Menu;

use NnShop\Application\AppBundle\Controller\Backend\Customers\DefaultController as CustomersDefaultController;
use NnShop\Application\AppBundle\Controller\Backend\Financial\InvoicesController as FinancialInvoicesController;
use NnShop\Application\AppBundle\Controller\Backend\Orders\DefaultController as OrdersDefaultController;
use NnShop\Application\AppBundle\Controller\Backend\Subscriptions\DefaultController as SubscriptionsDefaultController;
use NnShop\Application\AppBundle\Controller\Backend\Templates\DefaultController as TemplatesDefaultController;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class BackendMenuBuilder.
 */
class BackendMenuBuilder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param FactoryInterface $factory
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     *
     * @return ItemInterface
     */
    public function buildMenu(FactoryInterface $factory): ItemInterface
    {
        $profile_id = $this->getProfileId();
        $shop_id = $this->getParamOrSession('shop_id');
        $partner_id = $this->getParamOrSession('partner_id');

        $menu = $factory->createItem('root');

        $menu = $menu->addChild('Dashboard', ['route' => 'app.backend.dashboard.default.index'])
            ->setAttribute('icon', 'icon-home');

        /*
        * Bestellingen
        */
        $item = $menu->addChild('Bestellingen', [
            'route' => 'app.backend.orders.default.index',
            'routeParameters' => [
                'view' => OrdersDefaultController::VIEW_OVERVIEW,
                'profile_id' => $profile_id,
            ],
        ])->setAttribute('icon', 'icon-shopping-cart');

        $item->addChild('Overzicht', [
            'route' => 'app.backend.orders.default.index',
            'routeParameters' => [
                'view' => OrdersDefaultController::VIEW_OVERVIEW,
                'profile_id' => $profile_id,
            ],
        ]);

        $item->addChild('Handmatige actie', [
            'route' => 'app.backend.orders.default.index',
            'routeParameters' => [
                'view' => OrdersDefaultController::VIEW_MANUAL,
                'profile_id' => $profile_id,
            ],
        ]);

        $item->addChild('Wachtrij', [
            'route' => 'app.backend.orders.default.index',
            'routeParameters' => [
                'view' => OrdersDefaultController::VIEW_PENDING,
                'profile_id' => $profile_id,
            ],
        ]);

        $item->addChild('Foutmelding', [
            'route' => 'app.backend.orders.default.index',
            'routeParameters' => [
                'view' => OrdersDefaultController::VIEW_WARNING,
                'profile_id' => $profile_id,
            ],
        ]);

        $item->addChild('Afgerond', [
            'route' => 'app.backend.orders.default.index',
            'routeParameters' => [
                'view' => OrdersDefaultController::VIEW_COMPLETED,
                'profile_id' => $profile_id,
            ],
        ]);

        /*
        * Abonnementen
        */
        $item = $menu->addChild('Abonnementen', [
            'route' => 'app.backend.subscriptions.default.index',
            'routeParameters' => [
                'view' => SubscriptionsDefaultController::VIEW_OVERVIEW,
                'profile_id' => $profile_id,
            ],
        ])->setAttribute('icon', 'icon-cloud');

        $item->addChild('Overzicht', [
            'route' => 'app.backend.subscriptions.default.index',
            'routeParameters' => [
                'view' => SubscriptionsDefaultController::VIEW_OVERVIEW,
                'profile_id' => $profile_id,
            ],
        ]);

        $item->addChild('Afkoelperiode', [
            'route' => 'app.backend.subscriptions.default.index',
            'routeParameters' => [
                'view' => SubscriptionsDefaultController::VIEW_GRACE_PERIOD,
                'profile_id' => $profile_id,
            ],
        ]);

        $item->addChild('Verlopen', [
            'route' => 'app.backend.subscriptions.default.index',
            'routeParameters' => [
                'view' => SubscriptionsDefaultController::VIEW_EXPIRED,
                'profile_id' => $profile_id,
            ],
        ]);

        /*
        * Klanten
        */
        $item = $menu->addChild('Klanten', [
            'route' => 'app.backend.customers.default.index',
            'routeParameters' => [
                'view' => CustomersDefaultController::VIEW_OVERVIEW,
            ],
        ])->setAttribute('icon', 'icon-users');

        $item->addChild('Overzicht', [
            'route' => 'app.backend.customers.default.index',
            'routeParameters' => [
                'view' => CustomersDefaultController::VIEW_OVERVIEW,
            ],
        ]);

        $item->addChild('Omzet', [
            'route' => 'app.backend.customers.default.index',
            'routeParameters' => [
                'view' => CustomersDefaultController::VIEW_REVENUE,
            ],
        ]);

        $item->addChild('Referenties', ['route' => 'app.backend.customers.references.index']);

        /*
        * Templates
        */
        if ($partner_id) {
            $templateRoute = 'app.backend.templates.default.index';
            $templateParamName = 'partner_id';
        } else {
            $templateRoute = 'app.backend.templates.default.index_by_profile';
            $templateParamName = 'profile_id';
        }

        $item = $menu->addChild('Templates', [
            'route' => $templateRoute,
            'routeParameters' => [
                'view' => TemplatesDefaultController::VIEW_OVERVIEW,
                $templateParamName => ${$templateParamName},
            ],
        ])->setAttribute('icon', 'icon-archive');

        $item->addChild('Overzicht', [
            'route' => $templateRoute,
            'routeParameters' => [
                'view' => TemplatesDefaultController::VIEW_OVERVIEW,
                $templateParamName => ${$templateParamName},
            ],
        ]);

        $item->addChild('Geïmporteerd', [
            'route' => $templateRoute,
            'routeParameters' => [
                'view' => TemplatesDefaultController::VIEW_IMPORTED,
                $templateParamName => ${$templateParamName},
            ],
        ]);

        $item->addChild('Corrupt', [
            'route' => $templateRoute,
            'routeParameters' => [
                'view' => TemplatesDefaultController::VIEW_CORRUPT,
                $templateParamName => ${$templateParamName},
            ],
        ]);

        $item->addChild('Updates', ['route' => 'app.backend.templates.updates.index']);
        /*
        * Financieel
        */
        $item = $menu->addChild('Financieel', [
            'route' => 'app.backend.financial.invoices.index',
            'routeParameters' => [
                'view' => FinancialInvoicesController::VIEW_OVERVIEW,
                'profile_id' => $profile_id,
            ],
        ])->setAttribute('icon', 'icon-line-graph');

        $item->addChild('Facturen', [
            'route' => 'app.backend.financial.invoices.index',
            'routeParameters' => [
                'view' => FinancialInvoicesController::VIEW_OVERVIEW,
                'profile_id' => $profile_id,
            ],
        ]);

        $item->addChild('Openstaande facturen', [
            'route' => 'app.backend.financial.invoices.index',
            'routeParameters' => [
                'view' => FinancialInvoicesController::VIEW_PENDING,
                'profile_id' => $profile_id,
            ],
        ]);

        $item->addChild('Achterstallige facturen', [
            'route' => 'app.backend.financial.invoices.index',
            'routeParameters' => [
                'view' => FinancialInvoicesController::VIEW_OVERDUE,
                'profile_id' => $profile_id,
            ],
        ]);

        $item->addChild('Betaalde facturen', [
            'route' => 'app.backend.financial.invoices.index',
            'routeParameters' => [
                'view' => FinancialInvoicesController::VIEW_PAID,
                'profile_id' => $profile_id,
            ],
        ]);

        $item->addChild('Kortingscodes', ['route' => 'app.backend.financial.discounts.index']);
        $item->addChild('Uitbetalingen', ['route' => 'app.backend.financial.payouts.index']);

        /*
        * Instellingen
        */
        $item = $menu->addChild('Instellingen', ['route' => 'app.backend.settings.default.index'])
            ->setAttribute('icon', 'icon-cog');

        $item->addChild('Algemeen', ['route' => 'app.backend.settings.default.index']);
        $item->addChild('Advieswizard', ['route' => 'app.backend.settings.wizard.types.index', 'routeParameters' => ['profile_id' => $profile_id]]);
        $item->addChild('Categorieën', ['route' => 'app.backend.settings.categories.index', 'routeParameters' => ['shop_id' => $shop_id]]);
        $item->addChild('Partners (referrers)', ['route' => 'app.backend.settings.referrers.index', 'routeParameters' => ['profile_id' => $profile_id]]);
        $item->addChild('Kennispartners', ['route' => 'app.backend.settings.partners.index', 'routeParameters' => ['profile_id' => $profile_id]]);
        $item->addChild('Landingspagina\'s', ['route' => 'app.backend.settings.landing_pages.index', 'routeParameters' => ['profile_id' => $profile_id]]);
        $item->addChild('Betalingsmethoden', ['route' => 'app.backend.settings.payment_methods.index', 'routeParameters' => ['profile_id' => $profile_id]]);

        /*
        * Systeem
        */
//        $item = $menu->addChild('Systeem', ['route' => 'core.system.default.index'])
//            ->setAttribute('icon', 'icon-code');
//
//        $item->addChild('Informatie', ['route' => 'core.system.default.index']);
//        $item->addChild('Gebruikers', ['route' => 'core.users.default.index']);
//        $item->addChild('Periodieke taken', ['route' => 'core.jobs.default.index']);

        /**
         * Paginas.
         */
        $item = $menu->addChild('Paginas', [
            'route' => 'app.backend.pages.page.index',
            'routeParameters' => ['profile_id' => $profile_id],
        ])->setAttribute('icon', 'icon-pencil');
        $item->addChild('Overzicht', [
            'route' => 'app.backend.pages.page.index',
            'routeParameters' => ['profile_id' => $profile_id],
        ]);

        return $menu;
    }

    /**
     * @param string      $index
     * @param string|null $default
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     *
     * @return string|null
     *
     * Check if passed index is in request paramaters or can be found in session. If not return default
     */
    private function getParamOrSession(string $index, string $default = null)
    {
        if (!$this->container
            ->get('request_stack')
            ->getCurrentRequest()) {
            throw new \RuntimeException(sprintf("Current request isn't set"));
        }

        return $this->container
            ->get('request_stack')
            ->getCurrentRequest()
            ->get($index,
                $this->container
                    ->get('session')
                    ->get($index, $default)
            );
    }

    private function getProfileId(): string
    {
        if (!$this->container
            ->get('request_stack')
            ->getCurrentRequest()) {
            throw new \RuntimeException(sprintf("Current request isn't set"));
        }

        $sessionProfile = $this->container
            ->get('request_stack')
            ->getCurrentRequest()
            ->get('profile_id',
                $this->container
                    ->get('session')
                    ->get('profile_id')
            );

        if (!$sessionProfile) {
            $firstProfile = $this->container->get(ProfileRepositoryInterface::class)
                ->getFirst();

            return $firstProfile;
        }

        return $sessionProfile;
    }
}
