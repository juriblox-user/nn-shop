<?php

namespace NnShop\Application\AppBundle\Mail;

use Core\Component\Mail\Recipient;
use NnShop\Domain\Orders\Entity\Order;
use Symfony\Component\Translation\TranslatorInterface;

class DeliverSubscriptionOrderMail extends AbstractTranslationMail
{
    /**
     * @var Order
     */
    private $order;

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $invoice = $this->order->getInvoice();

        $customer = $this->order->getCustomer();
        $document = $this->order->getDocument();
        $template = $document->getTemplate();

        $locale = $customer->getLocale()->getLocale();

        $subject = $this->translator->trans('email_subject.deliver_subscription_order', ['%template_title%' => $template->getTitle()], 'email', $locale);

        $this->setLocale($locale);

        $this->setSubject($subject);
        $this->setBodyView('mails/order/deliver-subscription-order.html.twig');

        $this->addTo(Recipient::fromWithName($customer->getEmail(), $customer->getName()));
        $this->addBcc(Recipient::from("docs@legalfit.eu"));

        $this->setData([
            'order' => $this->order,

            'invoice' => $invoice,

            'customer' => $customer,
            'document' => $document,
            'template' => $template,

            'subscription' => $this->order->getSubscription(),
        ]);
    }

    /**
     * @param Order               $order
     * @param TranslatorInterface $translator
     *
     * @return DeliverSubscriptionOrderMail
     */
    public static function fromOrder(Order $order, TranslatorInterface $translator): self
    {
        $mail = static::prepare();
        $mail->order = $order;
        $mail->setTranslator($translator);

        return $mail;
    }
}
