<?php

namespace NnShop\Application\AppBundle\Mail;

use Core\Component\Mail\AbstractMail;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class AbstractTranslationMail.
 */
abstract class AbstractTranslationMail extends AbstractMail
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }
}
