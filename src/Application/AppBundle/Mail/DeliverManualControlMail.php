<?php

namespace NnShop\Application\AppBundle\Mail;

use Core\Component\Mail\AbstractMail;
use Core\Component\Mail\Recipient;
use NnShop\Domain\Orders\Entity\Order;

class DeliverManualControlMail extends AbstractMail
{
    /**
     * @var Order
     */
    private $order;

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $customer = $this->order->getCustomer();
        $template = $this->order->getTemplate();

        $this->setLocale($customer->getLocale()->getLocale());

        $this->setSubject(sprintf('Aanvraag 30 minuten juridisch advies: %s!', $template->getTitle()));
        $this->setBodyView('mails/order/deliver-manual-control-request.html.twig');

        $this->addTo(Recipient::from('docs@legalfit.eu'));

        $this->setData([
            'order' => $this->order,
            'customer' => $customer,
        ]);
    }

    /**
     * @param Order $order
     *
     * @return DeliverManualControlMail
     */
    public static function fromOrder(Order $order): self
    {
        $mail = static::prepare();
        $mail->order = $order;

        return $mail;
    }
}
