<?php

namespace NnShop\Application\AppBundle\Mail;

use Core\Component\Mail\Recipient;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Enumeration\PaymentMethod;
use Symfony\Component\Translation\TranslatorInterface;

class OrderConfirmationMail extends AbstractTranslationMail
{
    /**
     * @var Order
     */
    private $order;

    /**
     * {@inheritdoc}
     *
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function configure()
    {
        $invoice = $this->order->getInvoice();

        $customer = $this->order->getCustomer();
        $template = $this->order->getTemplate();

        $locale = $customer->getLocale()->getLocale();

        $subject = $this->translator->trans('email_subject.order_confirmation', ['%template_title%' => $template->getTitle()], 'email', $locale);

        $this->setLocale($locale);
        $this->setSubject($subject);
        $this->setBodyView('mails/order/confirmation.html.twig');

        $this->addTo(Recipient::fromWithName($customer->getEmail(), $customer->getName()));
        $this->addBcc(Recipient::from("docs@legalfit.eu"));

        $this->setData([
            'order'   => $this->order,
            'invoice' => $invoice,

            'customer' => $customer,
            'template' => $template,

            'reasonPayment' => $this->order->getPaymentMethod()->is(PaymentMethod::BANK_TRANSFER),
            'reasonManual'  => $this->order->isCheckRequested() || $this->order->isCustomRequested(),
        ]);
    }

    /**
     * @param Order               $order
     * @param TranslatorInterface $translator
     *
     * @return OrderConfirmationMail
     */
    public static function fromOrder(Order $order, TranslatorInterface $translator): self
    {
        $mail        = static::prepare();
        $mail->order = $order;
        $mail->setTranslator($translator);

        return $mail;
    }
}
