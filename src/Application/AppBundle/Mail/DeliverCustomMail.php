<?php

namespace NnShop\Application\AppBundle\Mail;

use Core\Component\Mail\Recipient;
use NnShop\Domain\Orders\Entity\Order;
use Symfony\Component\Translation\TranslatorInterface;

class DeliverCustomMail extends AbstractTranslationMail
{
    /**
     * @var Order
     */
    private $order;

    /**
     * {@inheritdoc}
     *
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function configure()
    {
        $customer = $this->order->getCustomer();
        $template = $this->order->getTemplate();

        $locale = $customer->getLocale()->getLocale();

        $subject = $this->translator->trans('email_subject.deliver_custom', ['%template_title%' => $template->getTitle()], 'email', $locale);

        $this->setLocale($locale);

        $this->setSubject($subject);
        $this->setBodyView('mails/order/deliver-custom-request.html.twig');

        $this->addTo(Recipient::from('docs@legalfit.eu'));

        $this->setData([
            'order' => $this->order,
            'customer' => $customer,
        ]);
    }

    /**
     * @param Order               $order
     * @param TranslatorInterface $translator
     *
     * @return DeliverCustomMail
     */
    public static function fromOrder(Order $order, TranslatorInterface $translator): self
    {
        $mail = static::prepare();
        $mail->order = $order;
        $mail->setTranslator($translator);

        return $mail;
    }
}
