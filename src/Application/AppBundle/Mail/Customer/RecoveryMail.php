<?php

namespace NnShop\Application\AppBundle\Mail\Customer;

use Core\Component\Mail\AbstractMail;
use NnShop\Application\AppBundle\Services\Security\MagicLink\UrlSigner;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Translation\Entity\Profile;

class RecoveryMail extends AbstractMail
{
    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var UrlSigner
     */
    private $urlSigner;

    /**
     * @param Customer  $customer
     * @param Profile   $profile
     * @param UrlSigner $urlSigner
     *
     * @return RecoveryMail
     */
    public static function create(Customer $customer, Profile $profile, UrlSigner $urlSigner): self
    {
        /** @var RecoveryMail $mail */
        $mail = self::prepare();
        $mail->customer = $customer;
        $mail->profile = $profile;

        $mail->urlSigner = $urlSigner;

        return $mail;
    }

    /**
     * Mail configureren.
     */
    public function configure()
    {
        $this->setLocale($this->customer->getLocale()->getLocale());

        $this->setSubject('Wachtwoord vergeten?');
        $this->setBodyView('mails/customer/recovery.html.twig');

        $this->setData([
            'customer' => $this->customer,

            'recoveryUrl' => $this->urlSigner->signRoute('app.frontend.account.recover/complete', [
                'host' => $this->profile->getHost(),
            ], $this->customer),
        ]);
    }
}
