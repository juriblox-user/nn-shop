<?php

namespace NnShop\Application\AppBundle\Mail\Customer;

use Core\Component\Mail\AbstractMail;
use NnShop\Application\AppBundle\Services\Security\MagicLink\UrlSigner;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Translation\Entity\Profile;

class InvitationMail extends AbstractMail
{
    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var UrlSigner
     */
    private $urlSigner;

    /**
     * @param Customer  $customer
     * @param Profile   $profile
     * @param UrlSigner $urlSigner
     *
     * @return InvitationMail
     */
    public static function create(Customer $customer, Profile $profile, UrlSigner $urlSigner): self
    {
        /** @var InvitationMail $mail */
        $mail = self::prepare();
        $mail->customer = $customer;
        $mail->profile = $profile;

        $mail->urlSigner = $urlSigner;

        return $mail;
    }

    /**
     * Mail configureren.
     */
    public function configure()
    {
        $this->setLocale($this->customer->getLocale()->getLocale());

        $this->setSubject('Welkom bij Legal Fit Docs');
        $this->setBodyView('mails/customer/invitation.html.twig');

        $this->setData([
            'customer' => $this->customer,

            'welcomeUrl' => $this->urlSigner->signRoute('app.frontend.account.register.welcome', [
                'host' => $this->profile->getHost(),
            ], $this->customer),
        ]);
    }
}
