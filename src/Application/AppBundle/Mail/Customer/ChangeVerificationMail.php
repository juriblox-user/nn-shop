<?php

namespace NnShop\Application\AppBundle\Mail\Customer;

use Core\Component\Mail\AbstractMail;
use NnShop\Application\AppBundle\Services\Security\MagicLink\UrlSigner;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Translation\Entity\Profile;

class ChangeVerificationMail extends AbstractMail
{
    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var UrlSigner
     */
    private $urlSigner;

    /**
     * @param Customer  $customer
     * @param Profile   $profile
     * @param UrlSigner $urlSigner
     *
     * @return ChangeVerificationMail
     */
    public static function create(Customer $customer, Profile $profile, UrlSigner $urlSigner): self
    {
        /** @var ChangeVerificationMail $mail */
        $mail = self::prepare();
        $mail->customer = $customer;
        $mail->profile = $profile;

        $mail->urlSigner = $urlSigner;

        return $mail;
    }

    /**
     * Mail configureren.
     */
    public function configure()
    {
        $this->setLocale($this->customer->getLocale()->getLocale());

        $this->setSubject('Bevestig je e-mailadres');
        $this->setBodyView('mails/customer/change_verification.html.twig');

        $this->setData([
            'customer' => $this->customer,

            'verificationUrl' => $this->urlSigner->signRoute('app.frontend.account.verify', [
                'host' => $this->profile->getHost(),
                'hash' => $this->customer->getEmailChangeHash(),
            ], $this->customer),
        ]);
    }
}
