<?php

namespace NnShop\Application\AppBundle\Mail;

use Core\Component\Mail\Recipient;
use NnShop\Domain\Orders\Entity\Order;
use Symfony\Component\Translation\TranslatorInterface;

class OrderUpsellMail extends AbstractTranslationMail
{
    /**
     * @var Order
     */
    private $order;

    /**
     * {@inheritdoc}
     *
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function configure()
    {
        $customer = $this->order->getCustomer();
        $template = $this->order->getTemplate();

        $locale = $customer->getLocale()->getLocale();

        $subject = $this->translator->trans('email_subject.order_upsell', [], 'email', $locale);

        $this->setLocale($locale);

        $this->setSubject($subject);
        $this->setBodyView('mails/order/upsell.html.twig');

        $this->addTo(Recipient::fromWithName($customer->getEmail(), $customer->getName()));

        $relatedTemplates = array_slice($template->getRelated(), 0, 3);

        $this->setData([
            'order'            => $this->order,
            'customer'         => $customer,
            'template'         => $template,
            'relatedTemplates' => $relatedTemplates,
        ]);
    }

    /**
     * @param Order               $order
     * @param TranslatorInterface $translator
     *
     * @return OrderUpsellMail
     */
    public static function fromOrder(Order $order, TranslatorInterface $translator): self
    {
        $mail        = static::prepare();
        $mail->order = $order;
        $mail->setTranslator($translator);

        return $mail;
    }
}
