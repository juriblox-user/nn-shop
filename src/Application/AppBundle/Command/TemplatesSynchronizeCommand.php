<?php

namespace NnShop\Application\AppBundle\Command;

use NnShop\Domain\Templates\Command\Template\SynchronizeTemplateCommand;
use NnShop\Domain\Templates\Value\TemplateId;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TemplatesSynchronizeCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('commands:templates:synchronize')
            ->setDescription('Force-sync a specific template');

        $this->addArgument('template-id', InputArgument::REQUIRED, 'Template ID (UUID)');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $commandBus = $this->getContainer()->get('command_bus');
        $commandBus->handle(SynchronizeTemplateCommand::fromLocalId(TemplateId::from($input->getArgument('template-id'))));
    }
}
