<?php

namespace NnShop\Application\AppBundle\Command;

use NnShop\Domain\Orders\Command\Customer\InviteCustomerCommand;
use NnShop\Domain\Orders\Value\CustomerId;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CustomersInviteCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('commands:customers:invite')
            ->addArgument('customer-id', InputArgument::REQUIRED, 'Customer ID (UUID)');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $commandBus = $this->getContainer()->get('command_bus');
        $commandBus->handle(InviteCustomerCommand::create(new CustomerId($input->getArgument('customer-id'))));
    }
}
