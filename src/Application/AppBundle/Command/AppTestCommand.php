<?php


namespace NnShop\Application\AppBundle\Command;


use NnShop\Application\AppBundle\Mail\DeliverCustomMail;
use NnShop\Application\AppBundle\Mail\DeliverManualControlMail;
use NnShop\Application\AppBundle\Mail\DeliverSubscriptionOrderMail;
use NnShop\Application\AppBundle\Mail\DeliverUnitOrderMail;
use NnShop\Application\AppBundle\Mail\OrderAbandonedMail;
use NnShop\Application\AppBundle\Mail\OrderConfirmationMail;
use NnShop\Application\AppBundle\Mail\OrderRequeuedMail;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Orders\Value\OrderId;
use SimpleBus\SymfonyBridge\Bus\CommandBus;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Core\Application\CoreBundle\Service\Mail\Mailer;

class AppTestCommand extends ContainerAwareCommand
{
    private $orderRepository;

    private $commandBus;

    private $mailer;

    private $translator;

    /**
     * AppTestCommand constructor.
     * @param $orderRepository
     * @param $commandBus
     * @param $mailer
     * @param $translator
     */
    public function __construct(OrderRepositoryInterface $orderRepository, CommandBus $commandBus, Mailer $mailer, TranslatorInterface $translator)
    {
        parent::__construct();

        $this->orderRepository = $orderRepository;
        $this->commandBus = $commandBus;
        $this->mailer = $mailer;
        $this->translator = $translator;
    }


    protected function configure()
    {
        $this
            ->setName('commands:app:test')
            ->setDescription('Test emails')
            ->addArgument('order-id', InputArgument::REQUIRED, 'Order ID (UUID)');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $order = $this->orderRepository->getById(new OrderId($input->getArgument('order-id')));

        $this->mailer->send(OrderAbandonedMail::fromOrder($order, $this->translator));
        $this->mailer->send(OrderConfirmationMail::fromOrder($order, $this->translator));
        $this->mailer->send(DeliverCustomMail::fromOrder($order, $this->translator));
        $this->mailer->send(DeliverManualControlMail::fromOrder($order));
        $this->mailer->send(DeliverSubscriptionOrderMail::fromOrder($order, $this->translator));
        $this->mailer->send(DeliverUnitOrderMail::fromOrder($order, $this->translator));
        $this->mailer->send(OrderRequeuedMail::fromOrder($order, $this->translator));
    }
}
