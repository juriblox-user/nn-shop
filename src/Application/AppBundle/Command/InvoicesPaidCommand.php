<?php

namespace NnShop\Application\AppBundle\Command;

use Carbon\Carbon;
use NnShop\Domain\Orders\Command\Invoice\ProcessInvoicePaymentCommand;
use NnShop\Domain\Orders\Value\InvoiceId;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InvoicesPaidCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('commands:invoices:paid')
            ->setDescription('Set an invoice as PAID');

        $this->addArgument('invoice-id', InputArgument::REQUIRED, 'Invoice ID (UUID)');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $commandBus = $this->getContainer()->get('command_bus');
        $commandBus->handle(ProcessInvoicePaymentCommand::create(InvoiceId::from($input->getArgument('invoice-id')), Carbon::now()));
    }
}
