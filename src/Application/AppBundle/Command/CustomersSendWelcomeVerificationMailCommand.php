<?php

namespace NnShop\Application\AppBundle\Command;

use NnShop\Domain\Orders\Command\Customer\SendWelcomeVerificationMailCommand;
use NnShop\Domain\Orders\Value\CustomerId;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CustomersSendWelcomeVerificationMailCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('commands:customers:send-welcome-verification-mail')
            ->addArgument('customer-id', InputArgument::REQUIRED, 'Customer ID (UUID)');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $commandBus = $this->getContainer()->get('command_bus');
        $commandBus->handle(SendWelcomeVerificationMailCommand::create(new CustomerId($input->getArgument('customer-id'))));
    }
}
