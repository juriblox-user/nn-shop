<?php

namespace NnShop\Application\AppBundle\Command;

use NnShop\Application\AppBundle\Services\Reporting\NnTenantsUploader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UploadNnTenantsCommand extends Command
{
    private $tenantsUploader;

    public function __construct(NnTenantsUploader $invoicesUploader)
    {
        parent::__construct();

        $this->tenantsUploader = $invoicesUploader;
    }

    public function configure()
    {
        $this->setName('reporting:upload-nn-tenants');
        $this->setDescription('Upload klanten');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Generating and uploading report...');

        $this->tenantsUploader->upload();

        $output->writeln(sprintf('<info>✔</info> Report has been generated'));
    }
}
