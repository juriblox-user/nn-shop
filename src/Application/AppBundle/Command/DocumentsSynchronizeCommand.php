<?php

namespace NnShop\Application\AppBundle\Command;

use NnShop\Domain\Templates\Command\Document\SynchronizeDocumentCommand;
use NnShop\Domain\Templates\Command\Document\SynchronizePendingDocumentsCommand;
use NnShop\Domain\Templates\Value\DocumentId;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DocumentsSynchronizeCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('commands:documents:synchronize')
            ->setDescription('Synchronize all pending documents or a specific DocumentId');

        $this->addArgument('document-id', InputArgument::OPTIONAL, 'Document ID (UUID)');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $commandBus = $this->getContainer()->get('command_bus');

        if (null !== $input->getArgument('document-id')) {
            $commandBus->handle(SynchronizeDocumentCommand::fromLocal(DocumentId::from($input->getArgument('document-id'))));
        } else {
            $commandBus->handle(SynchronizePendingDocumentsCommand::create());
        }
    }
}
