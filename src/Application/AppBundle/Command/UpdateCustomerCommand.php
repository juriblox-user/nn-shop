<?php

namespace NnShop\Application\AppBundle\Command;

use Core\Domain\Value\Geography\Address;
use Core\Domain\Value\Web\EmailAddress;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Enumeration\CustomerType;
use NnShop\Domain\Orders\Value\CustomerId;

class UpdateCustomerCommand
{
    /**
     * @var Address
     */
    private $address;

    /**
     * @var string|null
     */
    private $companyName;

    /**
     * @var EmailAddress
     */
    private $emailAddress;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var CustomerId
     */
    private $id;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var string|null
     */
    private $phone;

    /**
     * @var CustomerType
     */
    private $type;

    /**
     * @var string|null
     */
    private $vat;

    /**
     * @var string|null
     */
    private $coc;

    /**
     * Constructor.
     *
     * @param CustomerId $id
     */
    private function __construct(CustomerId $id)
    {
        $this->id = $id;
    }

    /**
     * @param Customer $customer
     *
     * @return UpdateCustomerCommand
     */
    public static function from(Customer $customer): self
    {
        $command = new self($customer->getId());
        $command->type = $customer->getType();

        $command->companyName = $customer->getCompanyName();
        $command->firstname = $customer->getFirstname();
        $command->lastname = $customer->getLastname();

        $command->address = $customer->getAddress();
        $command->phone = $customer->getPhone();
        $command->vat = $customer->getVat();
        $command->coc = $customer->getCocNumber();

        return $command;
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * @return string|null
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @return EmailAddress
     */
    public function getEmailAddress(): EmailAddress
    {
        return $this->emailAddress;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @return CustomerId
     */
    public function getId(): CustomerId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return CustomerType
     */
    public function getType(): CustomerType
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @return string|null
     */
    public function getCoc()
    {
        return $this->coc;
    }

    /**
     * @param Address $address
     *
     * @return UpdateCustomerCommand
     */
    public function setAddress(Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @param string|null $companyName
     *
     * @return UpdateCustomerCommand
     */
    public function setCompanyName($companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * @param EmailAddress $emailAddress
     *
     * @return UpdateCustomerCommand
     */
    public function setEmailAddress(EmailAddress $emailAddress): self
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * @param string $firstname
     *
     * @return UpdateCustomerCommand
     */
    public function setFirstname($firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @param string $lastname
     *
     * @return UpdateCustomerCommand
     */
    public function setLastname($lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @param string|null $phone
     *
     * @return UpdateCustomerCommand
     */
    public function setPhone($phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @param CustomerType $type
     *
     * @return UpdateCustomerCommand
     */
    public function setType(CustomerType $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @param string|null $vat
     *
     * @return UpdateCustomerCommand
     */
    public function setVat($vat): self
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * @param string|null $coc
     *
     * @return UpdateCustomerCommand
     */
    public function setCoc($coc): self
    {
        $this->coc = $coc;

        return $this;
    }


}
