<?php

namespace NnShop\Application\AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProcessHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\ProcessBuilder;

class ProcessesStartCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:processes:start')
            ->setDescription('Run application processes');

        $this->addOption('debug', 'Load debug configuration and display more verbose information');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $rootDir = realpath($this->getContainer()->get('kernel')->getRootDir() . '/..');

        if ($this->getContainer()->get('kernel')->isDebug()) {
            $output->setVerbosity(OutputInterface::VERBOSITY_DEBUG);
        }

        /** @var ProcessHelper $helper */
        $helper = $this->getHelper('process');

        $builder = new ProcessBuilder();
        $builder->setTimeout(null);
        $builder->setWorkingDirectory($rootDir);

        $builder->setArguments([
            'supervisord',
            '--configuration',
            sprintf('%s/supervisor/supervisord.conf', $this->getContainer()->get('kernel')->getRootDir()),
            '--nodaemon',
        ]);

        $helper->run($output, $builder->getProcess());
    }
}
