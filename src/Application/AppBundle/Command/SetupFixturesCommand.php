<?php

namespace NnShop\Application\AppBundle\Command;

use Core\Application\KernelInterface;
use Core\Common\Exception\EnvironmentProtectionException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SetupFixturesCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:setup:fixtures')
            ->setDescription('Fixtures van de applicatie uitvoeren')

            ->addOption('clean', null, InputOption::VALUE_NONE, 'Empty the current database prior to loading fixtures')
            ->addOption('drop', null, InputOption::VALUE_NONE, 'Drop the database schema, recreate it and then load all fixtures');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var KernelInterface $kernel */
        $kernel = $this->getApplication()->getKernel();

        /*
         * Drop and re-create the schema
         */
        if ($input->getOption('drop')) {
            if ($kernel->isEnvironment(KernelInterface::ENVIRONMENT_PRODUCTION)) {
                throw new EnvironmentProtectionException('The --drop option cannot be used in a production environment');
            }

            $command = $this->getApplication()->find('doctrine:schema:drop');

            $dropInput = new ArrayInput([
                'command' => 'doctrine:schema:drop',

                '--force' => true,
                '--no-interaction' => true,
            ]);

            $dropInput->setInteractive(false);

            $command->run($dropInput, $output);

            // doctrine:schema:create
            $command = $this->getApplication()->find('doctrine:schema:create');

            $createInput = new ArrayInput([
                'command' => 'doctrine:schema:create',

                '--no-interaction' => true,
            ]);

            $createInput->setInteractive(false);

            $command->run($createInput, $output);
        }

        $includeDevelopment = $kernel->isEnvironment([KernelInterface::ENVIRONMENT_DEVELOPMENT, KernelInterface::ENVIRONMENT_STAGING]);
        if ($includeDevelopment) {
            $output->writeln('Loading fixtures <comment>(incl. development)</comment>...');
        } else {
            $output->writeln('Loading fixtures...');
        }

        /*
         * Fixtures
         */
        $command = $this->getApplication()->find('doctrine:fixtures:load');

        $paths = [];
        foreach ($kernel->getBundles() as $bundle) {
            $paths[] = $bundle->getPath() . '/DataFixtures/ORM';
            $paths[] = $bundle->getPath() . '/Fixture/Base';

            if ($includeDevelopment) {
                $paths[] = $bundle->getPath() . '/Fixture/Development';
            }
        }

        for ($i = 0, $_i = count($paths); $i < $_i; ++$i) {
            if (!is_dir($paths[$i])) {
                unset($paths[$i]);
            }
        }

        $fixturesInput = new ArrayInput([
            'command' => 'doctrine:fixtures:load',

            '--append' => !$input->getOption('clean'),
            '--fixtures' => $paths,
            '--no-interaction' => true,
        ]);

        $fixturesInput->setInteractive(false);

        $command->run($fixturesInput, $output);
    }
}
