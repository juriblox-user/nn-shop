<?php

namespace NnShop\Application\AppBundle\Command;

use NnShop\Application\AppBundle\Services\Reporting\NnNewsletterUploader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UploadNnNewsletterCommand extends Command
{
    private $newsletterUploader;

    public function __construct(NnNewsletterUploader $newsletterUploader)
    {
        parent::__construct();

        $this->newsletterUploader = $newsletterUploader;
    }

    public function configure()
    {
        $this->setName('reporting:upload-nn-newsletter');
        $this->setDescription('Upload nieuwsbrief inschrijvingen');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Generating and uploading report...');

        $this->newsletterUploader->upload();

        $output->writeln(sprintf('<info>✔</info> Report has been generated'));
    }
}
