<?php

namespace NnShop\Application\AppBundle\Command;

use Carbon\Carbon;
use NnShop\Domain\Orders\Command\Invoice\CreditInvoiceCommand;
use NnShop\Domain\Orders\Value\InvoiceId;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InvoicesCreditCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('commands:invoices:credit')
            ->setDescription('Credit an invoice');

        $this->addArgument('invoice-id', InputArgument::REQUIRED, 'Invoice ID (UUID)');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $commandBus = $this->getContainer()->get('command_bus');
        $commandBus->handle(CreditInvoiceCommand::create(InvoiceId::from($input->getArgument('invoice-id')), Carbon::now()));
    }
}
