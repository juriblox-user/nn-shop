<?php

namespace NnShop\Application\AppBundle\Command;

use NnShop\Domain\Orders\Command\Order\DeliverOrderCommand;
use NnShop\Domain\Orders\Value\OrderId;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OrdersDeliverCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('commands:orders:deliver')
            ->setDescription('Deliver an order');

        $this->addArgument('order-id', InputArgument::REQUIRED, 'Order ID (UUID)');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $commandBus = $this->getContainer()->get('command_bus');
        $commandBus->handle(DeliverOrderCommand::create(OrderId::from($input->getArgument('order-id'))));
    }
}
