<?php

namespace NnShop\Application\AppBundle\Command;

use Core\Application\CoreBundle\Service\Reporter\BugsnagReporter;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BugsnagTestCommand extends ContainerAwareCommand
{
    /**
     * @var BugsnagReporter
     */
    private $reporter;

    /**
     * Constructor.
     *
     * @param BugsnagReporter $reporter
     */
    public function __construct(BugsnagReporter $reporter)
    {
        parent::__construct();

        $this->reporter = $reporter;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('bugsnag:test');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->reporter->reportException(new \Exception('Test'));
    }
}
