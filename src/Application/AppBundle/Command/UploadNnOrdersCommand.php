<?php

namespace NnShop\Application\AppBundle\Command;

use NnShop\Application\AppBundle\Services\Reporting\NnOrdersUploader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UploadNnOrdersCommand extends Command
{
    private $ordersUploader;

    public function __construct(NnOrdersUploader $ordersUploader)
    {
        parent::__construct();

        $this->ordersUploader = $ordersUploader;
    }

    public function configure()
    {
        $this->setName('reporting:upload-nn-orders');
        $this->setDescription('Upload bestellingen');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Generating and uploading report...');

        $this->ordersUploader->upload();

        $output->writeln(sprintf('<info>✔</info> Report has been generated'));
    }
}
