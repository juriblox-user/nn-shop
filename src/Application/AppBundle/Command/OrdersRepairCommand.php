<?php

namespace NnShop\Application\AppBundle\Command;

use NnShop\Domain\Orders\Command\Order\RepairOrderCommand;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Orders\Value\OrderId;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Workflow\Workflow;

class OrdersRepairCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('commands:orders:repair')
            ->setDescription('Repair order');

        $this->addArgument('order-id', InputArgument::REQUIRED, 'Order ID (UUID)');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var OrderRepositoryInterface $orderRepository */
        $orderRepository = $this->getContainer()->get(OrderRepositoryInterface::class);

        /** @var Workflow $workflow */
        $workflow = $this->getContainer()->get('state_machine.order');

        $order = $orderRepository->getById(OrderId::from($input->getArgument('order-id')));

        $table = new Table($output);

        $transitions = [];
        foreach ($workflow->getEnabledTransitions($order) as $transition) {
            $transitions[] = sprintf('%s [%s]', $transition->getName(), implode(', ', $transition->getTos()));
        }

        $table->setHeaders(['Field', 'Value'])
            ->setRows([
                ['Order ID', $order->getId()],
                ['Order number', $order->getCode()],
                ['Order date/time', $order->getTimestampCreated()],
                ['', ''],

                ['Check requested', $order->isCheckRequested() ? 'true' : 'false'],
                ['Customizations requested', $order->isCustomRequested() ? 'true' : 'false'],
                ['', ''],

                ['Customer', sprintf('%s [%s]', $order->getCustomer()->getName(), $order->getCustomer()->getId())],
                ['Company', $order->getCustomer()->getCompanyName()],
                ['', ''],

                ['Invoice ID', $order->getInvoice()->getId()],
                ['Invoice code', $order->getInvoice()->getCode()],
                ['Status', $order->getInvoice()->getStatus()],
                ['', ''],

                ['Document ID', $order->getDocument()->getId()],
                ['Template ID', $order->getDocument()->getTemplate()->getId()],
                ['Template title', $order->getDocument()->getTemplate()->getTitle()],
                ['', ''],

                ['Current order status', $order->getStatus()->getName()],
                ['Enabled transitions', implode("\n", $transitions)],
            ]);

        $table->render();

        // Commando afvuren
        $commandBus = $this->getContainer()->get('command_bus');
        $commandBus->handle(RepairOrderCommand::create($order->getId()));

        // Gegevens opnieuw binnenhalen
        $order = $orderRepository->getById($order->getId());

        $output->write("\n");
        $output->writeln(sprintf('Order status after repair action: <info>%s</info>', $order->getStatus()->getName()));

        $output->write("\n");
    }
}
