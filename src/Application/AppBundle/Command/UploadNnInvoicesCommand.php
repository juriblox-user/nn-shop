<?php

namespace NnShop\Application\AppBundle\Command;

use NnShop\Application\AppBundle\Services\Reporting\NnInvoicesUploader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UploadNnInvoicesCommand extends Command
{
    private $invoicesUploader;

    public function __construct(NnInvoicesUploader $invoicesUploader)
    {
        parent::__construct();

        $this->invoicesUploader = $invoicesUploader;
    }

    public function configure()
    {
        $this->setName('reporting:upload-nn-invoices');
        $this->setDescription('Upload facturen');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Generating and uploading report...');

        $this->invoicesUploader->upload();

        $output->writeln(sprintf('<info>✔</info> Report has been generated'));
    }
}
