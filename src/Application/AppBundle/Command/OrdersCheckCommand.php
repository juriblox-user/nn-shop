<?php

namespace NnShop\Application\AppBundle\Command;

use Carbon\CarbonInterval;
use NnShop\Domain\Orders\Command\Order\AbandonOrderCommand;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class OrdersCheckCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('commands:orders:check')
            ->setDescription('Check all orders, marking them as abandoned and canceling them if necessary');

        $this->addOption('hours', null, InputOption::VALUE_REQUIRED, 'Threshold (hours)');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->getContainer()->get('command_bus');

        /** @var OrderRepositoryInterface $orderRepository */
        $orderRepository = $this->getContainer()->get(OrderRepositoryInterface::class);

        if (null === $input->getOption('hours')) {
            $interval = CarbonInterval::hours(2);
        } else {
            $interval = CarbonInterval::hours($input->getOption('hours'));
        }

        foreach ($orderRepository->findAbandoned($interval, false) as $order) {
            $commandBus->handle(AbandonOrderCommand::create($order->getId()));
        }
    }
}
