<?php

namespace NnShop\Application\AppBundle\Command;

use NnShop\Domain\Mollie\Value\TransactionId;
use NnShop\Domain\Orders\Command\Payment\SynchronizePendingPaymentsCommand;
use NnShop\Domain\Orders\Command\Payment\SynchronizeTransactionCommand;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PaymentsSynchronizeCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('commands:payments:synchronize')
            ->setDescription('Synchronize all pending payments or a specific TransactionId');

        $this->addArgument('transaction-id', InputArgument::OPTIONAL, 'Transaction ID (tr_foo)');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $commandBus = $this->getContainer()->get('command_bus');

        if (null !== $input->getArgument('transaction-id')) {
            $commandBus->handle(SynchronizeTransactionCommand::create(TransactionId::from($input->getArgument('transaction-id'))));
        } else {
            $commandBus->handle(SynchronizePendingPaymentsCommand::create());
        }
    }
}
