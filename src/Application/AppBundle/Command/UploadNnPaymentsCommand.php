<?php

namespace NnShop\Application\AppBundle\Command;

use NnShop\Application\AppBundle\Services\Reporting\NnPaymentsUploader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UploadNnPaymentsCommand extends Command
{
    private $paymentsUploader;

    public function __construct(NnPaymentsUploader $paymentsUploader)
    {
        parent::__construct();

        $this->paymentsUploader = $paymentsUploader;
    }

    public function configure()
    {
        $this->setName('reporting:upload-nn-payments');
        $this->setDescription('Upload betalingen');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Generating and uploading report...');

        $this->paymentsUploader->upload();

        $output->writeln(sprintf('<info>✔</info> Report has been generated'));
    }
}
