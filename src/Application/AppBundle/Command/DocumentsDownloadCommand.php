<?php

namespace NnShop\Application\AppBundle\Command;

use NnShop\Domain\Templates\Command\Document\CacheDocumentCommand;
use NnShop\Domain\Templates\Value\DocumentId;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DocumentsDownloadCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('commands:documents:download')
            ->setDescription('Download the generated files for a document');

        $this->addArgument('document-id', InputArgument::REQUIRED, 'Document ID (UUID)');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $commandBus = $this->getContainer()->get('command_bus');
        $commandBus->handle(CacheDocumentCommand::create(DocumentId::from($input->getArgument('document-id'))));
    }
}
