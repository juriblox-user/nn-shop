<?php

namespace NnShop\Application\AppBundle\Controller\Webhooks\Mollie;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\Common\Exception\SecurityException;
use NnShop\Domain\Mollie\Event\Transaction\ReceivedTransactionNotificationEvent;
use NnShop\Domain\Mollie\Value\TransactionId;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $eventBus = $this->get('event_bus');

        $transactionId = $request->get('id');
        if (null === $transactionId) {
            throw new SecurityException('Mollie webhook was called without a transaction ID');
        }

        if (!TransactionId::valid($transactionId)) {
            throw new SecurityException('Mollie webhook was called with an improperly formatted transaction ID');
        }

        $eventBus->handle(
            new ReceivedTransactionNotificationEvent(TransactionId::from($transactionId))
        );

        return new Response('OK');
    }
}
