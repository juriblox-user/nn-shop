<?php

namespace NnShop\Application\AppBundle\Controller\Webhooks\JuriBlox;

use Core\Application\CoreBundle\Controller\AbstractController;
use JuriBlox\Sdk\Domain\Offices\Values\OfficeId as RemotePartnerId;
use JuriBlox\Sdk\Webhooks\Request as WebhookRequest;
use JuriBlox\Sdk\Webhooks\Requests\DocumentGenerationRequest;
use JuriBlox\Sdk\Webhooks\Requests\TemplatePublishRequest;
use NnShop\Domain\JuriBlox\Event\DocumentRequest\DocumentGenerationFailedEvent;
use NnShop\Domain\JuriBlox\Event\DocumentRequest\DocumentGenerationSucceededEvent;
use NnShop\Domain\JuriBlox\Event\Template\TemplatePublishedEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $eventBus = $this->get('event_bus');

        // JuriBlox request uitlezen
        $webhookRequest = WebhookRequest::fromInput(); // TODO: WebhookRequest::fromGlobals i.v.m. X-JuriBlox-*

        $remotePartnerId = new RemotePartnerId($request->headers->get('X-JuriBlox-Office-Id'));

        // Notificatie na het genereren van een document
        if ($webhookRequest instanceof DocumentGenerationRequest) {
            if (null === $webhookRequest->getRequestId()) {
                return new Response('IGNORED');
            }

            $eventBus->handle(
                $webhookRequest->isSuccess() ? DocumentGenerationSucceededEvent::create($webhookRequest->getRequestId(), $webhookRequest->getDocumentId()) : DocumentGenerationFailedEvent::fromRemote($webhookRequest->getRequestId())
            );
        }

        // Notificatie na het publiceren van een template
        elseif ($webhookRequest instanceof TemplatePublishRequest) {
            $eventBus->handle(
                TemplatePublishedEvent::create($webhookRequest->getTemplateId(), $remotePartnerId)
            );
        }

        return new Response('OK');
    }
}
