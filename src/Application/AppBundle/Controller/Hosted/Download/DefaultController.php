<?php

namespace NnShop\Application\AppBundle\Controller\Hosted\Download;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\Common\Exception\SecurityException;
use NnShop\Application\AppBundle\Services\UrlGenerator\SubscriptionUrlGenerator;
use NnShop\Domain\Orders\Repository\SubscriptionLinkRepository;
use NnShop\Domain\Orders\Value\SubscriptionLinkToken;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @throws SecurityException
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var SubscriptionLinkRepository $repository */
        $repository = $this->get(SubscriptionLinkRepository::class);

        $urlGenerator = $this->get(SubscriptionUrlGenerator::class);

        $token = $request->get('token');
        if (!SubscriptionLinkToken::valid($token)) {
            throw $this->createNotFoundException();
        }

        $link = $repository->findOneByToken(SubscriptionLinkToken::from($token));
        if (null === $link) {
            throw new SecurityException('User tried to guess a SubscriptionLinkToken');
        }

        return new RedirectResponse($urlGenerator->generateDirectDownloadUrl($link));
    }
}
