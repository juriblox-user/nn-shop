<?php

namespace NnShop\Application\AppBundle\Controller\Hosted\Embed;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Domain\Orders\Repository\SubscriptionRepositoryInterface;
use NnShop\Domain\Orders\Value\SubscriptionId;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    /**
     * @param string $subscriptionId
     *
     * @return Response
     */
    public function indexAction(string $subscriptionId)
    {
        /** @var SubscriptionRepositoryInterface $repository */
        $repository = $this->get(SubscriptionRepositoryInterface::class);

        $subscription = $repository->findOneById(SubscriptionId::from($subscriptionId));
        if (null === $subscription || ($subscription->isExpired() && !$subscription->isInGracePeriod())) {
            throw $this->createNotFoundException();
        }

        return $this->render(':hosted/default:index.html.twig', [
            'subscription' => $subscription,
            'document' => $subscription->getOrder()->getDocument(),
        ]);
    }
}
