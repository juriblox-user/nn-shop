<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Contact;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Domain\Translation\Entity\Profile;
use GuzzleHttp\Exception\RequestException;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Contact\ContactRequest;
use NnShop\Application\AppBundle\Forms\Frontend\Type\Contact\ContactType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request, \Swift_Mailer $mailer): Response
    {
        $formRequest = new ContactRequest();

        $form = $this->createForm(ContactType::class, $formRequest);
        $form->handleRequest($request);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            /** @var Profile $profile */
            $profile = $request->get('_profile');

            $message = (new \Swift_Message('Nieuw bericht van Legal Fit Docs'))
                ->setFrom('docs@legalfit.eu')
                ->setTo($profile->getEmail())
                ->setBody(
                    $this->renderView(
                        'mails/contact/message.html.twig',
                        ['subject' => 'Nieuw bericht van Legal Fit Docs', 'name' => $data->getName(), 'email' => $data->getEmail(), 'phone' => $data->getPhone(), 'message' => $data->getMessage()]
                    ),
                    'text/html'
                );

            $mailer->send($message);

            $this->addFlash('success', 'Je vraag is succesvol verstuurd, bedankt! Je hoort zo snel mogelijk van ons.');

            return $this->redirectToRoute('app.frontend.contact', ['host' => $request->getHost()]);
        }

        return $this->render('frontend/contact/default/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
