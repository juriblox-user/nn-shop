<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Order;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\Domain\Value\Web\EmailAddress;
use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Order\RegisteredEmailRequest;
use NnShop\Application\AppBundle\Forms\Frontend\Type\Order\RegisteredEmailType;
use NnShop\Domain\Orders\Command\Order\SendRegisteredEmailCommand;
use NnShop\Domain\Orders\Entity\Order;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class RegisteredEmailController extends AbstractController implements CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @return Response
     */
    public function indexAction(Order $order, Request $request)
    {
        if (!$order->isRegisteredEmailRequested()) {
            throw $this->createAccessDeniedException();
        }

        if ($order->isRegisteredEmailSent()) {
            return $this->redirectToRoute('app.frontend.order.status', [
                'orderId' => $order->getId(),
                'host' => $request->getHost(),
            ]);
        }

        if (!$order->getDocument()->isCached()) {
            return $this->redirectToRoute('app.frontend.order.status', [
                'orderId' => $order->getId(),
                'host' => $request->getHost(),
            ]);
        }

        /*
         * Formulier
         */
        $formRequest = new RegisteredEmailRequest();

        $form = $this->createForm(RegisteredEmailType::class, $formRequest);
        $form->handleRequest($request);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {
            $this->commandBus->handle(
                SendRegisteredEmailCommand::create($order->getId(), EmailAddress::from($formRequest->getEmail()))
                    ->setSubject($formRequest->getSubject())
                    ->setMessage($formRequest->getMessage())
            );

            $this->addFlash('success', 'Uw document is per Aangetekend Mailen® verstuurd.');

            return $this->redirectToRoute('app.frontend.order.status', [
                'orderId' => $order->getId(),
                'host' => $request->getHost(),
            ]);
        }

        return $this->render('frontend/order/registered_email/index.html.twig', [
            'form' => $form->createView(),

            'order' => $order,
            'document' => $order->getDocument(),
            'template' => $order->getDocument()->getTemplate(),
        ]);
    }
}
