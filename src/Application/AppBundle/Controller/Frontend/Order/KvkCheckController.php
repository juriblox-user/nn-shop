<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Order;

use Core\Application\CoreBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 *
 * @return Response
 */
class KvkCheckController extends AbstractController
{
    public function checkAction(Request $request)
    {
        $query = $request->get('q');

        if (empty($query)) {
            return new JsonResponse([
                'items' => [],
            ]);
        }

        $xml = new \SimpleXMLElement(file_get_contents("https://ws1.webservices.nl/rpc/get-simplexml/utf-8/dutchBusinessSearch/Legalfit_User/_Q(TcXj4mkjW3d3s//" . urlencode($query)));

        if (false === $xml) {
            return new JsonResponse([
                'items' => [],
            ]);
        }

        $json = json_encode($xml->results);
        $parsed = json_decode($json, true);

        $results = [];
        if (isset($parsed['entry'])) {
            foreach ($parsed['entry'] as $result) {
                $results[] = [
                    'kvk' => $result['dossier_number'],
                    'text' => $result['trade_name'],
                    'id' => $result['trade_name'],
                ];
            }
        }

        array_unshift($results, ['text' => $query, 'id' => $query]);

        return new JsonResponse([
            'items' => $results,
        ]);
    }
}
