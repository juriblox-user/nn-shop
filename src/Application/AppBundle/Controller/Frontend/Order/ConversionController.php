<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Order;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\Common\Exception\SecurityException;
use NnShop\Application\AppBundle\Services\Guard\OrderGuard;
use NnShop\Domain\Orders\Attribute\OrderAttribute;
use NnShop\Domain\Orders\Command\Order\CompleteOrderConversionCommand;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Orders\Value\OrderId;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ConversionController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @throws SecurityException
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /** @var OrderRepositoryInterface $orderRepository */
        $orderRepository = $this->get(OrderRepositoryInterface::class);

        /*
         * Bestelling ophalen
         */
        $orderId = OrderId::from($request->get('orderId'), false);
        if (null === $orderId) {
            throw new SecurityException('Malformed OrderId provided');
        }

        $order = $orderRepository->findOneById($orderId);
        if (null === $order) {
            throw new SecurityException('User tried to guess an order ID');
        }

        switch ($request->get('service')) {
            case 'adwords':
                $attribute = OrderAttribute::ADWORDS_CONVERSION_TRACKED;

                break;

            case 'google':
                $attribute = OrderAttribute::GOOGLE_CONVERSION_TRACKED;

                break;

            case 'twitter':
                $attribute = OrderAttribute::TWITTER_CONVERSION_TRACKED;

                break;

            default:
                throw new \InvalidArgumentException(sprintf('Invalid service "%s"', $request->get('service')));
        }

        $commandBus->handle(CompleteOrderConversionCommand::create($order->getId(), $attribute));

        return new JsonResponse('');
    }
}
