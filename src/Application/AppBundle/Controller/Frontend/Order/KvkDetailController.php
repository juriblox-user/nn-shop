<?php


namespace NnShop\Application\AppBundle\Controller\Frontend\Order;


use Core\Application\CoreBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class KvkDetailController extends AbstractController
{
    public function detailsAction(Request $request)
    {
        $id = $request->get('id');

        if (null === $id) {
            return new JsonResponse(['trade_name_full' => $request->get('name')]);
        }

        $xml = new \SimpleXMLElement(file_get_contents("https://ws1.webservices.nl/rpc/get-simplexml/utf-8/dutchBusinessGetDossier/Legalfit_User/_Q(TcXj4mkjW3d3s/" . $id));

        if (false === $xml) {
            return new JsonResponse(null);
        }

        $json = json_encode($xml);
        $parsed = json_decode($json, true);

        return new JsonResponse($parsed);
    }
}
