<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Order;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\Common\Exception\SecurityException;
use Core\Component\Flow\GenericStateStep;
use NnShop\Application\AppBundle\Forms\Frontend\Flow\GenericStateFlow;
use NnShop\Domain\Orders\Enumeration\OrderStatus;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Orders\Value\OrderId;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class StatusController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @throws SecurityException
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var OrderRepositoryInterface $orderRepository */
        $orderRepository = $this->get(OrderRepositoryInterface::class);

        /*
         * Bestelling ophalen
         */
        $orderId = OrderId::from($request->get('orderId'), false);
        if (null === $orderId) {
            throw new SecurityException('Malformed OrderId provided');
        }

        $order = $orderRepository->findOneById($orderId);
        if (null === $order) {
            throw new SecurityException('User tried to guess an order ID');
        }

        // Controleren of alle informatie compleet is
        if (!$order->getStatus()->isPending() && !$order->getStatus()->isCompleted()) {
            return $this->redirectToRoute('app.frontend.order', [
                'templateSlug' => $order->getTemplate()->getSlug(),
                'orderId' => $order->getId(),
            ]);
        }

        /*
         * Stappen van het bestelproces
         */
        $steps = [
            GenericStateStep::fromTitle('Bestelling ontvangen'),
            GenericStateStep::fromTitle('Betaling verwerken')
                ->addState(OrderStatus::from(OrderStatus::PENDING_PAYMENT)),
        ];

        $steps[] = GenericStateStep::fromTitle('Document genereren')
            ->addState(OrderStatus::from(OrderStatus::PENDING))
            ->addState(OrderStatus::from(OrderStatus::REQUEST))
            ->addState(OrderStatus::from(OrderStatus::REQUEUE));

        $steps[] = GenericStateStep::fromTitle('Document afleveren')
            ->addState(OrderStatus::from(OrderStatus::DELIVER));

        // Extra stap voor handmatige controle
        if ($order->isCheckRequested()) {
            $steps[] = GenericStateStep::fromTitle('Handmatige controle')
                ->addState(OrderStatus::from(OrderStatus::MANUAL_CHECK));
        }

        // Extra stap voor maatwerk aanpassingen
        if ($order->isCustomRequested()) {
            $steps[] = GenericStateStep::fromTitle('Maatwerk aanpassingen')
                ->addState(OrderStatus::from(OrderStatus::MANUAL_CUSTOM));
        }

        $flow = GenericStateFlow::withSteps($order, $steps, true);
        if ($order->getStatus()->is(OrderStatus::COMPLETED)) {
            $flow->override();
        }

        $this->setBridgeVariable('status', $order->getStatus()->getValue());
        $this->setBridgeVariable('redirectUrl', $this->generateUrl('app.frontend.order.status', [
            'orderId' => $order->getId(),
            'refreshed' => true,
            'host' => $order->getTemplate()->getPartner()->getCountryProfile()->getHost(),
        ]));

        return $this->render('frontend/order/status/index.html.twig', [
            'flow' => $flow,
            'refreshed' => (bool) $request->get('refreshed'),

            'order' => $order,
            'document' => $order->getDocument(),
            'template' => $order->getDocument()->getTemplate(),
            'invoice' => $order->getInvoice(),
        ]);
    }
}
