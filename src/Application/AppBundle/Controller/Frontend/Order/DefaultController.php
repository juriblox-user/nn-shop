<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Order;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\Application\CoreBundle\Forms\Flow\FlowRequestInterface;
use Core\Common\Exception\CriticalSecurityException;
use Core\Common\Exception\SecurityException;
use Core\Domain\Value\Geography\Address;
use Core\Domain\Value\Geography\CountryCode;
use Core\Domain\Value\Geography\HouseNumber;
use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use NnShop\Application\AppBundle\Command\UpdateCustomerCommand;
use NnShop\Application\AppBundle\Forms\AbstractType;
use NnShop\Application\AppBundle\Forms\Frontend\Flow\OrderStatusFlow;
use NnShop\Application\AppBundle\Forms\Frontend\Flow\OrderStatusStep;
use NnShop\Application\AppBundle\Forms\Frontend\Flow\QuestionsFlow;
use NnShop\Application\AppBundle\Forms\Frontend\Flow\QuestionsStep;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Order\OrderAccountRequest;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Order\OrderConfirmRequest;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Order\OrderOptionsRequest;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Order\OrderQuestionsRequest;
use NnShop\Application\AppBundle\Forms\RequestInterface;
use NnShop\Application\AppBundle\Services\Payment\MollieGateway;
use NnShop\Domain\Orders\Command\Customer\ClaimOrderCommand;
use NnShop\Domain\Orders\Command\Customer\SubscribeToNewsletterCommand;
use NnShop\Domain\Orders\Command\Customer\UnsubscribeFromNewsletterCommand;
use NnShop\Domain\Orders\Command\Order\ApplyDiscountToOrderCommand;
use NnShop\Domain\Orders\Command\Order\CompleteAccountStepCommand;
use NnShop\Domain\Orders\Command\Order\CompleteConfirmStepCommand;
use NnShop\Domain\Orders\Command\Order\CompleteOptionsStepCommand;
use NnShop\Domain\Orders\Command\Order\CompleteQuestionsStepCommand;
use NnShop\Domain\Orders\Command\Order\DeleteDiscountFromOrderCommand;
use NnShop\Domain\Orders\Command\Order\PrepareCustomerForOrderCommand;
use NnShop\Domain\Orders\Command\Order\RevertAccountStepCommand;
use NnShop\Domain\Orders\Command\Order\RevertConfirmStepCommand;
use NnShop\Domain\Orders\Command\Order\RevertOptionsStepCommand;
use NnShop\Domain\Orders\Command\Order\SetQuestionsStepCommand;
use NnShop\Domain\Orders\Data\QuestionWithAnswer;
use NnShop\Domain\Orders\Enumeration\CustomerType;
use NnShop\Domain\Orders\Enumeration\OrderStatus;
use NnShop\Domain\Orders\Enumeration\OrderType;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Orders\Services\Resolver\QuestionsGraph;
use NnShop\Domain\Orders\Services\Resolver\QuestionStepResolver;
use NnShop\Domain\Orders\Services\Validator\DiscountValidator;
use NnShop\Domain\Orders\Value\DiscountId;
use NnShop\Domain\Orders\Value\OrderId;
use NnShop\Domain\Templates\Command\Answer\AnswerQuestionCommand;
use NnShop\Domain\Templates\Repository\QuestionStepRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use NnShop\Domain\Translation\Entity\Profile;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Translation\TranslatorInterface;

class DefaultController extends AbstractController implements CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @var AuthenticationUtils
     */
    private $authentication;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * Constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     * @param AuthenticationUtils         $authentication
     * @param TokenStorageInterface       $tokenStorage
     * @param TranslatorInterface         $translator
     */
    public function __construct(CustomerRepositoryInterface $customerRepository, AuthenticationUtils $authentication, TokenStorageInterface $tokenStorage, TranslatorInterface $translator)
    {
        $this->customerRepository = $customerRepository;

        $this->authentication = $authentication;
        $this->tokenStorage = $tokenStorage;

        $this->translator = $translator;
    }

    /**
     * @param string  $templateSlug
     * @param string  $orderId
     * @param Request $request
     *
     * @throws CriticalSecurityException
     * @throws SecurityException
     *
     * @return Response
     */
    public function indexAction(string $templateSlug, string $orderId, Request $request)
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        $mollieGateway = $this->get(MollieGateway::class);

        /** @var QuestionStepRepositoryInterface $stepRepository */
        $stepRepository = $this->get(QuestionStepRepositoryInterface::class);

        /** @var TemplateRepositoryInterface $templateRepository */
        $templateRepository = $this->get(TemplateRepositoryInterface::class);

        /** @var OrderRepositoryInterface $orderRepository */
        $orderRepository = $this->get(OrderRepositoryInterface::class);

        /** @var DiscountValidator $discountValidator */
        $discountValidator = $this->get(DiscountValidator::class);

        /*
         * Bestelling en template laden
         */
        $orderId = OrderId::from($orderId, false);
        if (null === $orderId) {
            throw new CriticalSecurityException('Malformed OrderId provided');
        }

        $order = $orderRepository->findOneById($orderId);
        if (null === $order) {
            throw new SecurityException('User tried to guess an order ID');
        }

        $template = $templateRepository->getVisibleBySlug($templateSlug);
        if ($template->getId() != $order->getTemplate()->getId()) {
            throw new CriticalSecurityException('The template linked to this order does not match the provided slugs');
        }

        /** @var OrderStatusFlow $orderFlow */
        $orderFlow = new OrderStatusFlow();
        $orderFlow->load($order, true);

        // Als we deze status niet ondersteunen redirecten we naar de voortgangs-pagina
        if (!$orderFlow->supportsState($order->getState()) || $order->isLocked()) {
            return $this->redirectToRoute('app.frontend.order.status', [
                'orderId' => $order->getId(),
                'host' => $template->getPartner()->getCountryProfile()->getHost(),
            ]);
        }

        /** @var OrderStatusStep $orderStep */
        $orderStep = $orderFlow->getCurrentStep();

        /** @var AbstractType $orderStepType */
        $orderStepType = $orderStep->getType();

        $questionsFlow = null;

        /*
         * Vragenlijst
         */
        if (OrderStatus::STEP_QUESTIONS == $orderFlow->getCurrentState()) {
            /** @var QuestionsFlow $questionsFlow */
            $questionsFlow = $orderFlow->inject(QuestionsFlow::fromTemplate($template));
            $questionsFlow->setCurrentQuestionStep($order->getStep());

            // Cache met alle vragen
            $questionsGraph = new QuestionsGraph($stepRepository);
            $questionsGraph->warmup($order->getDocument());

            // Resolver voor het overslaan van stappen
            $stepResolver = new QuestionStepResolver($questionsGraph);

            $questionsRequest = new OrderQuestionsRequest($orderFlow, $questionsGraph, $questionsFlow->getCurrentStepEntity());

            $form = $this->createNamedForm(null, $orderStepType, $questionsRequest);
            $form->handleRequest($request);

            $questionsRequest->bind($form);

            if ($form->isSubmitted()) {

                /*
                 * Naar de volgende stap
                 */
                if ($questionsRequest->clickedNext() && $form->isValid()) {
                    /** @var QuestionWithAnswer $answer */
                    foreach ($questionsRequest->getAnswers() as $answer) {
                        // Antwoorden met een waarde
                        if (null !== $answer->getValue()) {
                            $command = AnswerQuestionCommand::create($order->getDocument()->getId(), $answer->getQuestionId(), $answer->getValue());
                        }

                        // Antwoorden met opties
                        else {
                            $command = AnswerQuestionCommand::prepare($order->getDocument()->getId(), $answer->getQuestionId());
                            foreach ($answer->getOptionIds() as $optionId) {
                                $command->addOptionId($optionId);
                            }
                        }

                        $commandBus->handle($command);
                    }

                    // Na het verwerken van de antwoorden bepalen welke stappen we moeten overslaan
                    $questionsGraph->refresh();

                    foreach ($stepResolver->getSkippedSteps() as $step) {
                        $questionsFlow->skipQuestionStep($step);
                    }

                    if ($questionsFlow->hasNext()) {
                        /** @var QuestionsStep $step */
                        $step = $orderFlow->moveNext();

                        $commandBus->handle(SetQuestionsStepCommand::create($order->getId(), $step->getEntity()->getId()));
                    }

                    /*
                     * Vragenlijst is afgerond
                     */
                    else {
                        $commandBus->handle(CompleteQuestionsStepCommand::create($order->getId()));
                    }

                    return $this->redirectToRoute('app.frontend.order', [
                        'templateSlug' => $template->getSlug(),
                        'orderId' => $order->getId(),
                        'host' => $template->getPartner()->getCountryProfile()->getHost(),
                    ]);
                }

                /*
                 * Naar de vorige stap
                 */
                elseif ($questionsRequest->clickedBack()) {
                    // Overbodige stappen overslaan
                    foreach ($stepResolver->getSkippedSteps() as $step) {
                        $questionsFlow->skipQuestionStep($step);
                    }

                    /** @var QuestionsStep $step */
                    $step = $orderFlow->moveBack();

                    $commandBus->handle(SetQuestionsStepCommand::create($order->getId(), $step->getEntity()->getId()));

                    return $this->redirectToRoute('app.frontend.order', [
                        'templateSlug' => $template->getSlug(),
                        'orderId' => $order->getId(),
                        'host' => $template->getPartner()->getCountryProfile()->getHost(),
                    ]);
                }
            }

            // Voor de weergave bepalen welke stappen we moeten overslaan
            else {
                foreach ($stepResolver->getSkippedSteps() as $step) {
                    $questionsFlow->skipQuestionStep($step);
                }
            }
        }

        /*
         * Overige stappen
         */
        else {
            /** @var RequestInterface $requestClass */
            $requestClass = $orderStepType::getRequestClass();

            /** @var FlowRequestInterface $orderRequest */
            $orderRequest = new $requestClass($orderFlow);

            $orderStepOptions = [];

            if ($orderRequest instanceof OrderAccountRequest) {
                $orderRequest->setProfileCountry($template->getPartner()->getCountryProfile()->getCountry());
            }

            if ($orderRequest instanceof OrderConfirmRequest) {
                $orderRequest->setValidator($discountValidator);
            }

            $form = $this->createForm($orderStepType, $orderRequest, $orderStepOptions);
            $form->handleRequest($request);

            $orderRequest->bind($form);

            /*
             * Submit afhandelen
             */
            if ($form->isSubmitted()) {

                /*
                 * Stap 4: Toepassen van kortingscode
                 */
                if ($form->isValid() && OrderStatus::STEP_CONFIRM == $order->getStatus() && $form->has('apply_discount') && $form->get('apply_discount')->isClicked()) {
                    /** @var OrderConfirmRequest $orderConfirmRequest */
                    $orderConfirmRequest = $orderRequest;

                    if (null !== $orderConfirmRequest->getDiscount()) {
                        $commandBus->handle(ApplyDiscountToOrderCommand::createWithCode($order->getId(), $orderConfirmRequest->getDiscount()));

                        $this->addFlash('success', 'De kortingscode is toegepast! Het nieuwe bedrag is direct verwerkt in onderstaand overzicht.');

                        return $this->redirectToRoute('app.frontend.order', [
                            'templateSlug' => $template->getSlug(),
                            'orderId' => $order->getId(),
                            'host' => $order->getTemplate()->getPartner()->getCountryProfile()->getHost(),
                        ]);
                    }
                }

                /*
                 * Naar de volgende stap
                 */
                if ($orderRequest->clickedNext() || $orderRequest->clickedFinish()) {

                    /*
                     * Van aanmelden naar de opties/bevestigen
                     */
                    if ($form->isValid() && OrderStatus::STEP_ACCOUNT == $order->getStatus()) {
                        /** @var OrderAccountRequest $orderAccountRequest */
                        $orderAccountRequest = $orderRequest;

                        /** @var Profile $profile */
                        $profile = $request->get('_profile');

                        if ($form->isValid()) {

                            $existingCustomer = $this->customerRepository->findOneByEmail($orderAccountRequest->getCustomerEmail());

                            $customerAddress = Address::fromFullAddress($orderAccountRequest->getCustomerAddressStreet(), HouseNumber::fromOptional($orderAccountRequest->getCustomerAddressNumber(), $orderAccountRequest->getCustomerAddressAddition()), $orderAccountRequest->getCustomerAddressZipcode(), $orderAccountRequest->getCustomerAddressCity(), new CountryCode($orderAccountRequest->getCustomerAddressCountry()));

                            /*
                             * Update existing customer
                             */
                            if (null !== $existingCustomer) {
                                $customerId = $existingCustomer->getId();

                                $this->commandBus->handle(
                                    UpdateCustomerCommand::from($existingCustomer)
                                        ->setType(CustomerType::from($orderAccountRequest->getCustomerType()))

                                        ->setCompanyName($orderAccountRequest->getCustomerCompanyName())
                                        ->setFirstname($orderAccountRequest->getCustomerFirstname())
                                        ->setLastname($orderAccountRequest->getCustomerLastname())

                                        ->setAddress($customerAddress)
                                        ->setPhone($orderAccountRequest->getCustomerPhone())
                                        ->setVat($orderAccountRequest->getCustomerVat())
                                        ->setCoc($orderAccountRequest->getCustomerCoc())
                                );

                                if ($orderAccountRequest->getRegisterNewsletter()) {
                                    $this->commandBus->handle(SubscribeToNewsletterCommand::create($customerId));
                                } else {
                                    $this->commandBus->handle(UnsubscribeFromNewsletterCommand::create($customerId));
                                }

                                $commandBus->handle(ClaimOrderCommand::create($customerId, $order->getId()));
                            }

                            /*
                             * Register new guest account
                             */
                            else {
                                $command = PrepareCustomerForOrderCommand::prepare($order->getId(), $orderAccountRequest->getCustomerEmail(), $profile->getId(), $profile->getMainLocale()->getId());
                                $command->setCustomerType(CustomerType::from($orderAccountRequest->getCustomerType()));

                                $command->setCompanyName($orderAccountRequest->getCustomerCompanyName());
                                $command->setFirstname($orderAccountRequest->getCustomerFirstname());
                                $command->setLastname($orderAccountRequest->getCustomerLastname());

                                $command->setAddress($customerAddress);
                                $command->setPhone($orderAccountRequest->getCustomerPhone());
                                $command->setVat($orderAccountRequest->getCustomerVat());
                                $command->setCoc($orderAccountRequest->getCustomerCoc());

                                $commandBus->handle($command);
                            }

                            $commandBus->handle(CompleteAccountStepCommand::create($order->getId()));
                        }
                    }

                    /*
                     * Van opties naar bevestigen
                     */
                    elseif ($form->isValid() && OrderStatus::STEP_OPTIONS == $order->getStatus()) {
                        /** @var OrderOptionsRequest $orderOptionsRequest */
                        $orderOptionsRequest = $orderRequest;

                        $command = CompleteOptionsStepCommand::prepare($order->getId());
                        $command->setType(OrderType::from($orderOptionsRequest->getType()));

                        $command->setCheck($orderOptionsRequest->getCheck());
                        $command->setCustom($orderOptionsRequest->getCustom());

                        if (null !== $template->getRegisteredEmailPrice()) {
                            $command->setRegisteredEmail($orderOptionsRequest->getRegisteredEmail());
                        }

                        $commandBus->handle($command);
                    }

                    /*
                     * Van bevestigen naar betalen
                     */
                    elseif ($form->isValid() && OrderStatus::STEP_CONFIRM == $order->getStatus()) {
                        /** @var OrderConfirmRequest $orderCheckoutRequest */
                        $orderCheckoutRequest = $orderRequest;

                        $command = CompleteConfirmStepCommand::prepare($order->getId());
                        $command->setPaymentMethod($orderCheckoutRequest->getMethod());

                        $commandBus->handle($command);

                        return $this->redirectToRoute('app.frontend.payment', [
                            'invoiceId' => $order->getInvoice()->getId(),
                            'host' => $template->getPartner()->getCountryProfile()->getHost(),
                        ]);
                    }

                    if ($form->isValid()) {
                        return $this->redirectToRoute('app.frontend.order', [
                            'templateSlug' => $template->getSlug(),
                            'orderId' => $order->getId(),
                            'host' => $template->getPartner()->getCountryProfile()->getHost(),
                        ]);
                    }
                }

                /*
                 * Naar de vorige stap
                 */
                elseif ($orderRequest->clickedBack()) {
                    // Stap terug vanaf bedrijfsgegevens/aanmelden
                    if (OrderStatus::STEP_ACCOUNT == $order->getStatus()) {
                        $commandBus->handle(RevertAccountStepCommand::create($order->getId()));
                    }

                    // Stap terug vanaf de opties
                    elseif (OrderStatus::STEP_OPTIONS == $order->getStatus()) {
                        $commandBus->handle(RevertOptionsStepCommand::create($order->getId()));
                    }

                    // Stap terug vanaf bevestigen
                    elseif (OrderStatus::STEP_CONFIRM == $order->getStatus()) {
                        $commandBus->handle(RevertConfirmStepCommand::create($order->getId()));
                    }

                    return $this->redirectToRoute('app.frontend.order', [
                        'templateSlug' => $template->getSlug(),
                        'orderId' => $order->getId(),
                        'host' => $template->getPartner()->getCountryProfile()->getHost(),
                    ]);
                }
            }
        }

        $this->setBridgeVariable('status', $order->getStatus()->getValue());

        $allowedPaymentMethods = $mollieGateway->getProfileAllowedMethods($request->get('_profile'));

        return $this->render('frontend/order/default/index.html.twig', [
            'orderFlow' => $orderFlow,
            'questionsFlow' => $questionsFlow,

            'loginError' => $this->authentication->getLastAuthenticationError(),
            'allowedPaymentMethods' => $allowedPaymentMethods,

            'form' => $form->createView(),

            'order' => $order,
            'template' => $template,
        ]);
    }

    /**
     * @param Request $request
     * @param string  $discountId
     * @param string  $orderId
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteDiscountAction(Request $request, string $discountId, string $orderId)
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        $discountId = DiscountId::from($discountId);

        $orderId = OrderId::from($orderId);

        $commandBus->handle(DeleteDiscountFromOrderCommand::create($orderId, $discountId));

        return $this->redirect($request->headers->get('referer'));
    }
}
