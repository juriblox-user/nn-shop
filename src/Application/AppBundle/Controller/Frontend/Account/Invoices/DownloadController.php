<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Account\Invoices;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Application\AppBundle\Services\Manager\InvoicePdfManager;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Entity\Invoice;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\StreamedResponse;

class DownloadController extends AbstractController
{
    /**
     * @var InvoicePdfManager
     */
    private $invoicePdfManager;

    /**
     * Constructor.
     *
     * @param InvoicePdfManager $invoicePdfManager
     */
    public function __construct(InvoicePdfManager $invoicePdfManager)
    {
        $this->invoicePdfManager = $invoicePdfManager;
    }

    /**
     * @Route(path="/invoices/{id}", name="app.frontend.account.invoices.download")
     *
     * @param Invoice $invoice
     *
     * @return StreamedResponse
     */
    public function indexAction(Invoice $invoice)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        if (!$invoice->getCustomer()->isEqualTo($customer)) {
            throw $this->createAccessDeniedException();
        }

        return $this->invoicePdfManager->stream($invoice);
    }
}
