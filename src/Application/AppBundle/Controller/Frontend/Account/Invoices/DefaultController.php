<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Account\Invoices;

use Core\Application\CoreBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Entity\Invoice;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PaginatorInterface
     */
    private $paginator;

    /**
     * Constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param PaginatorInterface     $paginator
     */
    public function __construct(EntityManagerInterface $entityManager, PaginatorInterface $paginator)
    {
        $this->entityManager = $entityManager;
        $this->paginator = $paginator;
    }

    /**
     * @Route(path="/invoices", name="app.frontend.account.invoices")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        $builder = $this->entityManager->createQueryBuilder();

        $builder
            ->select('i')
            ->from(Invoice::class, 'i')
            ->where(
                $builder->expr()->eq('i.customer', ':customer')
            )
            ->orderBy('i.timestampIssued', 'DESC')
            ->setParameters([
                'customer' => $customer,
            ]);

        /** @var SlidingPagination|Invoice[] $invoices */
        $invoices = $this->paginator->paginate($builder->getQuery(), $request->query->getInt('page', 1), 10);

        return $this->render(':frontend/account/invoices/default:index.html.twig', [
            'invoices' => $invoices,
        ]);
    }
}
