<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Account\Recover;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\Domain\Value\Web\EmailAddress;
use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Account\Recover\CompleteRecoveryRequest;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Account\Recover\RequestRecoveryRequest;
use NnShop\Application\AppBundle\Forms\Frontend\Type\Account\Recover\CompleteRecoveryType;
use NnShop\Application\AppBundle\Forms\Frontend\Type\Account\Recover\RequestRecoveryType;
use NnShop\Domain\Orders\Command\Customer\ChangePasswordCommand;
use NnShop\Domain\Orders\Command\Customer\CompleteRecoveryCommand;
use NnShop\Domain\Orders\Command\Customer\RequestRecoveryCommand;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Enumeration\CustomerStatus;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Translation\TranslatorInterface;

class DefaultController extends AbstractController implements CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * Constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     * @param TokenStorageInterface $tokenStorage
     * @param TranslatorInterface $translator
     */
    public function __construct(CustomerRepositoryInterface $customerRepository, TokenStorageInterface $tokenStorage, TranslatorInterface $translator)
    {
        $this->customerRepository = $customerRepository;
        $this->tokenStorage = $tokenStorage;
        $this->translator = $translator;
    }

    /**
     * @Route(path="/recover", name="app.frontend.account.recover")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $formRequest = new RequestRecoveryRequest();

        $form = $this->createForm(RequestRecoveryType::class, $formRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $customer = $this->customerRepository->findOneByEmail(EmailAddress::from($formRequest->getEmail()));

            if (null !== $customer && $customer->getStatus()->is(CustomerStatus::DRAFT)) {
                $customer = null;
            }
            //U heeft uw account nog niet geactiveerd nadat u zich heeft geregistreerd. Volg eerst de instructies in die e-mail die u eerder heeft ontvangen.
            if (null !== $customer && !$customer->getStatus()->allowsInteractiveLogin()) {
                $form->get('email')->addError(new FormError($this->translator->trans('account_not_activated', [], 'validators')));
            }

            if ($form->isValid()) {
                if (null !== $customer) {
                    $this->commandBus->handle(RequestRecoveryCommand::create($customer->getId()));
                }
                //Als uw e-mailadres bekend bij ons is ontvangt u binnen enkele ogenblikken een e-mail waarmee u een nieuw wachtwoord kunt instellen.
                $this->addFlash('success', $this->translator->trans('restore_password_text', [], 'validators'));

                return $this->redirectToRoute('app.frontend.account.session.login', [
                    'host' => $request->get('host'),
                ]);
            }
        }

        return $this->render(':frontend/account/recover/default:index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route(path="/recover/complete", name="app.frontend.account.recover/complete")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function completeAction(Request $request)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        if ($customer->getStatus()->isNot(CustomerStatus::RECOVER)) {
            return $this->redirectToRoute('app.frontend.account.dashboard', [
                'host' => $request->get('host'),
            ]);
        }

        $formRequest = new CompleteRecoveryRequest();

        $form = $this->createForm(CompleteRecoveryType::class, $formRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // Wachtwoord instellen
            $this->commandBus->handle(ChangePasswordCommand::create($customer->getId(), $formRequest->getPassword()));

            // Herstelprocedure afronden
            $this->commandBus->handle(CompleteRecoveryCommand::create($customer->getId()));

            // Afmelden
            $this->tokenStorage->setToken(null);
            //Uw nieuwe wachtwoord is succesvol ingesteld. U kunt zich nu hiermee aanmelden.
            $this->addFlash('success', $this->translator->trans('password_reset_success', [], 'validators'));

            return $this->redirectToRoute('app.frontend.account.session.login', [
                'host' => $request->get('host'),
            ]);
        }

        return $this->render(':frontend/account/recover/default:complete.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
