<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Account\Verify;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use NnShop\Domain\Orders\Command\Customer\CompleteEmailChangeCommand;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Services\Validator\CustomerValidator;
use NnShop\Domain\Orders\Value\CustomerEmailHash;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Translation\TranslatorInterface;

class DefaultController extends AbstractController implements CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @var CustomerValidator
     */
    private $doctorValidator;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * Constructor.
     *
     * @param CustomerValidator $doctorValidator
     * @param TokenStorageInterface $tokenStorage
     * @param TranslatorInterface $translator
     */
    public function __construct(CustomerValidator $doctorValidator, TokenStorageInterface $tokenStorage, TranslatorInterface $translator)
    {
        $this->doctorValidator = $doctorValidator;
        $this->tokenStorage = $tokenStorage;
        $this->translator = $translator;
    }

    /**
     * @Route(path="/confirm/{hash}", name="app.frontend.account.verify")
     *
     * @param string  $hash
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(string $hash, Request $request)
    {
        $hash = new CustomerEmailHash($hash);

        /** @var Customer $customer */
        $customer = $this->getUser();

        if (!$customer->hasEmailChange()) {
            return $this->redirectToRoute('app.frontend.account.dashboard', [
                'host' => $request->get('host'),
            ]);
        }

        if (!$hash->matches($customer->getEmailChange())) {
            //De link die u heeft gebruikt om uw e-mailadres te bevestigen is niet (meer) actueel. Dit kan voorkomen als u uw nieuwe e-mailadres al eerder heeft bevestigd.
            $this->addFlash('error', $this->translator->trans('link_expired', [], 'validators'));

            return $this->redirectToRoute('app.frontend.account.dashboard', [
                'host' => $request->get('host'),
            ]);
        }

        // After-the-fact dubbele e-mailadressen voorkomen
        if (!$this->doctorValidator->isUniqueEmail($customer->getEmailChange(), $customer)) {
            //Het e-mailadres dat u wilt gebruiken worden al door een ander account gebruikt. Dit kan voorkomen als u uw nieuwe e-mailadres al eerder heeft bevestigd.
            $this->addFlash('error', $this->translator->trans('email_in_use_other_account', [], 'validators'));

            return $this->redirectToRoute('app.frontend.account.dashboard', [
                'host' => $request->get('host'),
            ]);
        }

        // Wijziging afronden
        $this->commandBus->handle(CompleteEmailChangeCommand::create($customer->getId()));

        // Afmelden
        $this->tokenStorage->setToken(null);
        //Uw e-mailadres is nu gewijzigd in %email%. Bedankt voor het doorgeven van deze wijziging!
        $this->addFlash('success', $this->translator->trans('email_changed_thanks', ['%email%'=> $customer->getEmail()], 'validators'));

        return $this->redirectToRoute('app.frontend.account.session.login', [
            'host' => $request->get('host'),
        ]);
    }
}
