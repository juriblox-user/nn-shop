<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Account\Verify;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use NnShop\Domain\Orders\Command\Customer\CompleteWelcomeVerificationCommand;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Enumeration\CustomerStatus;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

class WelcomeController extends AbstractController implements CommandBusAwareInterface
{
    use CommandBusAwareTrait;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * WelcomeController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route(path="/register/confirm", name="app.frontend.account.verify.welcome")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        if ($customer->getStatus()->isNot(CustomerStatus::DRAFT)) {
            return $this->redirectToRoute('app.frontend.account.dashboard', [
                'host' => $request->get('host'),
            ]);
        }

        $this->commandBus->handle(CompleteWelcomeVerificationCommand::create($customer->getId()));
        //Uw e-mailadres is nu bevestigd, dankuwel!
        $this->addFlash('success', $this->translator->trans('email_confirmed', [], 'validators'));

        return $this->redirectToRoute('app.frontend.account.dashboard', [
            'host' => $request->get('host'),
        ]);
    }
}
