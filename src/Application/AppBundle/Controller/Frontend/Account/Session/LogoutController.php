<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Account\Session;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\Common\Exception\NotImplementedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;

class LogoutController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * LogoutController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route(path="/logout", name="app.frontend.account.session.logout")
     *
     * @throws NotImplementedException
     */
    public function indexAction()
    {
        throw new NotImplementedException();
    }

    /**
     * @Route(path="/logged-out", name="app.frontend.account.session.logout/success")
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function successAction(Request $request)
    {
        //U bent succesvol afgemeld. U kunt het browservenster nu veilig sluiten.
        $this->addFlash('success', $this->translator->trans('logout_succes', [], 'validators'));

        return $this->redirectToRoute('app.frontend.account.session.login', [
            'host' => $request->get('host'),
        ]);
    }
}
