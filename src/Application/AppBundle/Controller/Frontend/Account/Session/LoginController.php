<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Account\Session;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Account\Session\LoginRequest;
use NnShop\Application\AppBundle\Forms\Frontend\Type\Account\Session\LoginType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends AbstractController
{
    /**
     * @var AuthenticationUtils
     */
    private $authentication;

    /**
     * Constructor.
     *
     * @param AuthenticationUtils $authentication
     */
    public function __construct(AuthenticationUtils $authentication)
    {
        $this->authentication = $authentication;
    }

    /**
     * @Route(path="/login", name="app.frontend.account.session.login")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        if (null !== $this->getUser()) {
            return $this->redirectToRoute('app.frontend.account.dashboard', [
                'host' => $request->get('host'),
            ]);
        }

        $formRequest = new LoginRequest();
        if ('' != $this->authentication->getLastUsername()) {
            $formRequest->setEmail($this->authentication->getLastUsername());
        }

        $form = $this->createForm(LoginType::class, $formRequest);

        return $this->render(':frontend/account/session/login:index.html.twig', [
            'form' => $form->createView(),
            'error' => $this->authentication->getLastAuthenticationError(),

            'targetPath' => $request->query->get('_target_path'),
        ]);
    }
}
