<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Account\Orders;

use Core\Application\CoreBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Entity\Order;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PaginatorInterface
     */
    private $paginator;

    /**
     * Constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param PaginatorInterface     $paginator
     */
    public function __construct(EntityManagerInterface $entityManager, PaginatorInterface $paginator)
    {
        $this->entityManager = $entityManager;
        $this->paginator = $paginator;
    }

    /**
     * @Route(path="/orders", name="app.frontend.account.orders")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        $builder = $this->entityManager->createQueryBuilder();

        $builder
            ->select('o')
            ->from(Order::class, 'o')
            ->where(
                $builder->expr()->eq('o.customer', ':customer')
            )
            ->orderBy('o.timestampCreated', 'DESC')
            ->setParameters([
                'customer' => $customer,
            ]);

        /** @var SlidingPagination|Order[] $orders */
        $orders = $this->paginator->paginate($builder->getQuery(), $request->query->getInt('page', 1), 10);

        return $this->render(':frontend/account/orders/default:index.html.twig', [
            'orders' => $orders,
        ]);
    }
}
