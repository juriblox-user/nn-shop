<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Account\Profile;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Account\Profile\NewsletterRequest;
use NnShop\Application\AppBundle\Forms\Frontend\Type\Account\Profile\NewsletterType;
use NnShop\Domain\Orders\Command\Customer\SubscribeToNewsletterCommand;
use NnShop\Domain\Orders\Command\Customer\UnsubscribeFromNewsletterCommand;
use NnShop\Domain\Orders\Entity\Customer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

class NewsletterController extends AbstractController implements CommandBusAwareInterface
{
    use CommandBusAwareTrait;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * NewsletterController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }


    /**
     * @Route(path="/profile/newsletter", name="app.frontend.account.profile.newsletter")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        $formRequest = new NewsletterRequest();
        $formRequest->setNewsletter($customer->getNewsletter());

        $form = $this->createForm(NewsletterType::class, $formRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($formRequest->getNewsletter()) {
                $this->commandBus->handle(SubscribeToNewsletterCommand::create($customer->getId()));
            } else {
                $this->commandBus->handle(UnsubscribeFromNewsletterCommand::create($customer->getId()));
            }
            // 'Uw voorkeur is succesvol opgeslagen, bedankt voor het doorgeven!'
            $this->addFlash('success', $this->translator->trans('newsletter_success', [], 'validators'));

            return $this->redirectToRoute('app.frontend.account.dashboard', [
                'host' => $request->get('host'),
            ]);
        }

        return $this->render(':frontend/account/profile/newsletter:index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
