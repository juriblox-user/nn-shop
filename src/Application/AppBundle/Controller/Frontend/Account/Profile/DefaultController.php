<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Account\Profile;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use NnShop\Application\AppBundle\Command\UpdateCustomerCommand;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Account\Profile\EditDetailsRequest;
use NnShop\Application\AppBundle\Forms\Frontend\Type\Account\Profile\EditDetailsType;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Enumeration\CustomerType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

class DefaultController extends AbstractController implements CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * DefaultController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {

        $this->translator = $translator;
    }

    /**
     * @Route(path="/profile", name="app.frontend.account.profile")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        $formRequest = new EditDetailsRequest();
        $formRequest->read($customer);

        $form = $this->createForm(EditDetailsType::class, $formRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $customerType = null;
            if (null !== $formRequest->getCompanyName()) {
                $customerType = new CustomerType(CustomerType::BUSINESS);
            } else {
                $customerType = new CustomerType(CustomerType::CONSUMER);
            }

            $this->commandBus->handle(
                UpdateCustomerCommand::from($customer)
                    ->setType($customerType)

                    ->setCompanyName($formRequest->getCompanyName())
                    ->setFirstname($formRequest->getFirstname())
                    ->setLastname($formRequest->getLastname())

                    ->setAddress($formRequest->getAddress())
                    ->setPhone($formRequest->getPhone())
                    ->setVat($formRequest->getVat())
            );

            //Uw gegevens zijn succesvol bijgewerkt, bedankt!
            $this->addFlash('success', $this->translator->trans('profile_success', [], 'validators'));

            return $this->redirectToRoute('app.frontend.account.dashboard', [
                'host' => $request->get('host'),
            ]);
        }

        return $this->render(':frontend/account/profile/default:index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
