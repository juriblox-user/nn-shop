<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Account\Profile;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\Domain\Value\Web\EmailAddress;
use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Account\Profile\EditCredentialsRequest;
use NnShop\Application\AppBundle\Forms\Frontend\Type\Account\Profile\EditCredentialsType;
use NnShop\Domain\Orders\Command\Customer\ChangePasswordCommand;
use NnShop\Domain\Orders\Command\Customer\RequestEmailChangeCommand;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Services\Validator\CustomerValidator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

class CredentialsController extends AbstractController implements CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @var CustomerValidator
     */
    private $customerValidator;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * Constructor.
     *
     * @param CustomerValidator $customerValidator
     * @param TranslatorInterface $translator
     */
    public function __construct(CustomerValidator $customerValidator, TranslatorInterface $translator)
    {
        $this->customerValidator = $customerValidator;
        $this->translator = $translator;
    }

    /**
     * @Route(path="/profile/credentials", name="app.frontend.account.profile.credentials")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        $formRequest = new EditCredentialsRequest();
        $formRequest->read($customer);

        $form = $this->createForm(EditCredentialsType::class, $formRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $email = EmailAddress::from($formRequest->getEmail());

            $changeEmail = $customer->getEmail()->notEquals($email);

            // Dubbel e-mailadres voorkomen
            if (!$this->customerValidator->isUniqueEmail($email, $customer)) {
                // 'Dit e-mailadres wordt al door een ander account gebruikt. Kies een ander ander e-mailadres.'
                $form->get('email')->addError(new FormError($this->translator->trans('email_in_use', [], 'validators')));
            }

            /*
             * Wijzigingen opslaan
             */
            if ($form->isValid()) {

                // Wijziging e-mailadres
                if ($changeEmail) {
                    $this->commandBus->handle(RequestEmailChangeCommand::create($customer->getId(), $email));
                }

                // Wijziging wachtwoord
                if (null !== $formRequest->getUpdatedPassword()) {
                    $this->commandBus->handle(ChangePasswordCommand::create($customer->getId(), $formRequest->getEmail()));
                }

                if ($changeEmail) {
                    // Wij hebben een verificatiemail gestuurd naar %email% waarmee de wijziging definitief kan worden gemaakt.
                    $this->addFlash('success', $this->translator->trans('verification_email_sent', ['%email%' => $formRequest->getEmail()], 'validators'), [
                        '%email%' => $formRequest->getEmail(),
                    ]);
                } else {
                    // Uw aanmeldgegevens zijn succesvol bijgewerkt, bedankt!
                    $this->addFlash('success', $this->translator->trans('credentials_success', [], 'validators'));
                }

                return $this->redirectToRoute('app.frontend.account.dashboard', [
                    'host' => $request->get('host'),
                ]);
            }
        }

        return $this->render(':frontend/account/profile/credentials:index.html.twig', [
            'form' => $form->createView(),

            'emailChange' => $customer->getEmailChange(),
        ]);
    }
}
