<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Account\Register;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Account\Register\RegisterRequest;
use NnShop\Application\AppBundle\Forms\Frontend\Type\Account\Register\RegisterType;
use NnShop\Domain\Orders\Command\Customer\ChangePasswordCommand;
use NnShop\Domain\Orders\Command\Customer\CompleteInvitationCommand;
use NnShop\Domain\Orders\Command\Customer\SubscribeToNewsletterCommand;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Enumeration\CustomerStatus;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

class WelcomeController extends AbstractController implements CommandBusAwareInterface
{
    use CommandBusAwareTrait;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * WelcomeController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route(path="/welcome", name="app.frontend.account.register.welcome")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        if ($customer->getStatus()->isNot(CustomerStatus::INVITED)) {
            return $this->redirectToRoute('app.frontend.account.dashboard', [
                'host' => $request->get('host'),
            ]);
        }

        $formRequest = new RegisterRequest();
        $formRequest->setEmail($customer->getEmail()->getString());

        $form = $this->createForm(RegisterType::class, $formRequest, [
            'disable_email_address' => true,
        ]);

        $form->handleRequest($request);

        /*
         * Gegevens opslaan
         */
        if ($form->isSubmitted() && $form->isValid()) {
            $this->commandBus->handle(ChangePasswordCommand::create($customer->getId(), $formRequest->getPassword()));

            if ($formRequest->getNewsletter()) {
                $this->commandBus->handle(SubscribeToNewsLetterCommand::create($customer->getId()));
            }

            $this->commandBus->handle(CompleteInvitationCommand::create($customer->getId()));
            //Uw accountgegevens zijn succesvol ingesteld, bedankt!
            $this->addFlash('success', $this->translator->trans('invitation_success', [], 'validators'));

            return $this->redirectToRoute('app.frontend.account.dashboard', [
                'host' => $request->get('host'),
            ]);
        }

        return $this->render(':frontend/account/register/welcome:index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
