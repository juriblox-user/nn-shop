<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Account\Register;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\Domain\Value\Web\EmailAddress;
use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Account\Register\RegisterRequest;
use NnShop\Application\AppBundle\Forms\Frontend\Type\Account\Register\RegisterType;
use NnShop\Domain\Orders\Command\Customer\CreateCustomerCommand;
use NnShop\Domain\Orders\Command\Customer\InviteCustomerCommand;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Enumeration\CustomerStatus;
use NnShop\Domain\Orders\Enumeration\CustomerType;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;
use NnShop\Domain\Orders\Services\Validator\CustomerValidator;
use NnShop\Domain\Orders\Value\CustomerId;
use NnShop\Domain\Translation\Entity\Profile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

class DefaultController extends AbstractController implements CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var CustomerValidator
     */
    private $customerValidator;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * Constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     * @param CustomerValidator $customerValidator
     * @param TranslatorInterface $translator
     */
    public function __construct(CustomerRepositoryInterface $customerRepository, CustomerValidator $customerValidator, TranslatorInterface $translator)
    {
        $this->customerRepository = $customerRepository;
        $this->customerValidator = $customerValidator;
        $this->translator = $translator;
    }

    /**
     * @Route(path="/register", name="app.frontend.account.register")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        if (null !== $customer) {
            return $this->redirectToRoute('app.frontend.account.dashboard', [
                'host' => $request->get('host'),
            ]);
        }

        $formRequest = new RegisterRequest();

        $form = $this->createForm(RegisterType::class, $formRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $email = EmailAddress::from($formRequest->getEmail());

            /** @var Customer $existingCustomer */
            $existingCustomer = null;

            // Dubbele e-mailadressen voorkomen
            if ($form->isValid() && !$this->customerValidator->isUniqueEmail($email)) {
                $customer = $this->customerRepository->findOneByEmail($email);

                if ($customer->getStatus()->is(CustomerStatus::DRAFT)) {
                    $existingCustomer = $customer;
                } else {
                    $form->get('email')->addError(new FormError($this->translator->trans('email_in_use_forgot', [], 'validators')));
                }
            }

            /*
             * Registreren
             */
            if ($form->isValid()) {
                /** @var Profile $profile */
                $profile = $request->get('_profile');

                /*
                 * Registreren - bestaand account claimen
                 */
                if (null !== $existingCustomer) {
                    $this->commandBus->handle(InviteCustomerCommand::create($existingCustomer->getId()));

                    $this->addFlash('success', $this->translator->trans('existing_account', ['%email%' => $email], 'validators'));
                }

                /*
                 * Registreren - nieuw account
                 */
                else {
                    $this->commandBus->handle(
                        CreateCustomerCommand::prepare(CustomerId::generate(), new CustomerType(CustomerType::UNKNOWN), $email, $profile->getId(), $profile->getMainLocale()->getId())
                            ->setPassword($formRequest->getPassword())
                            ->setNewsletter($formRequest->getNewsletter())
                    );

                    $this->addFlash('success', $this->translator->trans('account_success_confirm', ['%email%' => $email], 'validators'));
                }

                return $this->redirectToRoute('app.frontend.account.session.login', [
                    'host' => $request->get('host'),
                ]);
            }
        }

        return $this->render(':frontend/account/register/default:index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
