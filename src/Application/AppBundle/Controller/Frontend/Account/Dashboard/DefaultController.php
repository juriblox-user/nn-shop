<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Account\Dashboard;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * Constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @Route(path="/", name="app.frontend.account.dashboard")
     *
     * @return Response
     */
    public function indexAction()
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        return $this->render(':frontend/account/dashboard/default:index.html.twig', [
            'recentOrders' => $this->orderRepository->findRecentOrders($customer, 5),
        ]);
    }
}
