<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Templates;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Application\AppBundle\Services\UrlGenerator\TemplateUrlGenerator;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use NnShop\Domain\Templates\Value\TemplateKey;

class ShortlinkController extends AbstractController
{
    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * @var TemplateUrlGenerator
     */
    private $templateUrlGenerator;

    /**
     * Constructor.
     *
     * @param TemplateRepositoryInterface $templateRepository
     * @param TemplateUrlGenerator        $templateUrlGenerator
     */
    public function __construct(TemplateRepositoryInterface $templateRepository, TemplateUrlGenerator $templateUrlGenerator)
    {
        $this->templateRepository = $templateRepository;
        $this->templateUrlGenerator = $templateUrlGenerator;
    }

    /**
     * @param $templateKey
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexAction($templateKey)
    {
        $template = $this->templateRepository->getByKey(TemplateKey::from($templateKey));

        return $this->redirect($this->templateUrlGenerator->generateDetailsUrl($template));
    }
}
