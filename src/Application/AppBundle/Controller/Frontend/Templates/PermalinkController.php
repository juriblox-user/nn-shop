<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Templates;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Application\AppBundle\Services\UrlGenerator\TemplateUrlGenerator;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use NnShop\Domain\Templates\Value\TemplateId;

class PermalinkController extends AbstractController
{
    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    /**
     * @var TemplateUrlGenerator
     */
    private $templateUrlGenerator;

    /**
     * Constructor.
     *
     * @param TemplateRepositoryInterface $templateRepository
     * @param TemplateUrlGenerator        $templateUrlGenerator
     */
    public function __construct(TemplateRepositoryInterface $templateRepository, TemplateUrlGenerator $templateUrlGenerator)
    {
        $this->templateRepository = $templateRepository;
        $this->templateUrlGenerator = $templateUrlGenerator;
    }

    /**
     * @param $templateId
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexAction($templateId)
    {
        $template = $this->templateRepository->getById(TemplateId::from($templateId));

        return $this->redirect($this->templateUrlGenerator->generateDetailsUrl($template));
    }
}
