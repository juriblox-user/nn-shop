<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Counters;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Domain\System\Enumeration\CounterName;
use NnShop\Domain\System\Repository\CounterRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class SmiirlController extends AbstractController
{
    /**
     * @return Response
     */
    public function indexAction()
    {
        /** @var CounterRepositoryInterface $repository */
        $repository = $this->get(CounterRepositoryInterface::class);

        $counter = $repository->getByName(CounterName::from(CounterName::GENERATED_DOCUMENTS));

        return new JsonResponse([
            'number' => $counter->getAmount(),
        ]);
    }
}
