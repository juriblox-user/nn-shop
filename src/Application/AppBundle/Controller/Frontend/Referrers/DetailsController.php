<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Referrers;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Domain\Shops\Repository\ReferrerRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use Symfony\Component\HttpFoundation\Response;

class DetailsController extends AbstractController
{
    /**
     * @param string $referrerSlug
     *
     * @return Response
     */
    public function indexAction(string $referrerSlug)
    {
        /** @var ReferrerRepositoryInterface $referrerRepository */
        $referrerRepository = $this->get(ReferrerRepositoryInterface::class);

        /** @var TemplateRepositoryInterface $templateRepository */
        $templateRepository = $this->get(TemplateRepositoryInterface::class);

        $referrer = $referrerRepository->findOneBySlug($referrerSlug);
        if (null === $referrer || $referrer->isDeleted() || !$referrer->hasLanding()) {
            throw $this->createNotFoundException();
        }

        return $this->render(':frontend/referrers/details:default.html.twig', [
            'referrer' => $referrer,
            'templates' => $templateRepository->findVisibleByReferrer($referrer),
        ]);
    }
}
