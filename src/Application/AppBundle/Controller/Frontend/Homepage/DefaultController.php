<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Homepage;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use GuzzleHttp\Client;
use Psr\Cache\CacheItemInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        /** @var AdapterInterface $chache */
        $cache = $this->get('cache.app');

        /** @var TemplateRepositoryInterface $templateRepository */
        $templateRepository = $this->get(TemplateRepositoryInterface::class);

        $cacheItem = $cache->getItem('feedbackcompany.reviews');

        if (!$cacheItem->isHit()) {
            $this->fetchReviews($cacheItem);

            $cachedReviews = $cacheItem->get();
        } else {
            $cachedReviews = $cacheItem->get();

            if (true || $cachedReviews['expiration'] < time()) {
                $this->fetchReviews($cacheItem);

                $cachedReviews = $cacheItem->get();
            }
        }

        $reviews = null !== $cachedReviews ? $cachedReviews['reviews'] : [];

        return $this->render('frontend/homepage/default/index.html.twig', [
            'spotlight' => $templateRepository->findShuffledSpotlight(4),
            'reviews' => $reviews,
        ]);
    }

    private function fetchReviews(CacheItemInterface $cacheItem)
    {
        $client = new Client();

        /** @var AdapterInterface $cache */
        $cache = $this->get('cache.app');

        try {
            $tokenResponse = $client->request('GET', 'https://www.feedbackcompany.com/api/v2/oauth2/token', [
                'query' => [
                    'client_id' => getenv('FEEDBACK_COMPANY_ID'),
                    'client_secret' => getenv('FEEDBACK_COMPANY_SECRET'),
                    'grant_type' => 'authorization_code',
                ]
            ]);
        } catch (\Exception $exception) {
            return null;
        }

        $tokenResult = json_decode($tokenResponse->getBody()->getContents());

        $token = $tokenResult->access_token;

        if (!$token) {
            return null;
        }

        try {
            $reviewsResponse = $client->request('GET', 'https://www.feedbackcompany.com/api/v2/review/', [
                'headers' => [
                    'Authorization' => 'Bearer '.$token,
                ]
            ]);
        } catch (\Exception $exception) {
            return null;
        }

        $reviewsResult = json_decode($reviewsResponse->getBody()->getContents());

        $reviews = $reviewsResult->reviews;

        foreach ($reviews as $review) {
            $review->total_score = $review->total_score * 2;
        }

        if (null !== $reviews) {
            $cacheItem->set([
                'reviews' => $reviews,
                'expiration' => time() + 3600,
            ]);

            $cache->save($cacheItem);
        }
    }
}
