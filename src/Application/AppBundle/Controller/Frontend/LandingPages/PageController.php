<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\LandingPages;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Domain\Shops\Repository\LandingPageRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use Symfony\Component\HttpFoundation\Response;

class PageController extends AbstractController
{
    /**
     * @param string $pageSlug
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return Response
     */
    public function indexAction(string $pageSlug): Response
    {
        /** @var LandingPageRepositoryInterface $pageRepository */
        $pageRepository = $this->get(LandingPageRepositoryInterface::class);

        /** @var TemplateRepositoryInterface $templateRepository */
        $templateRepository = $this->get(TemplateRepositoryInterface::class);

        $landingPage = $pageRepository->findOneBySlug($pageSlug);
        if (null === $landingPage) {
            throw $this->createNotFoundException();
        }

        return $this->render(':frontend/landing_pages/page:default.html.twig', [
            'landingPage' => $landingPage,
            'templates' => $templateRepository->findVisibleByLandingPage($landingPage),
        ]);
    }
}
