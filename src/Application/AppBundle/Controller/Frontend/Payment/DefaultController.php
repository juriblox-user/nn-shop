<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Payment;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\Common\Exception\SecurityException;
use NnShop\Application\AppBundle\Services\Payment\MollieGateway;
use NnShop\Domain\Orders\Command\Payment\SynchronizePendingPaymentsCommand;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Repository\InvoiceRepositoryInterface;
use NnShop\Domain\Orders\Value\InvoiceId;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @throws SecurityException
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $commandBus = $this->get('command_bus');
        $mollieGateway = $this->get(MollieGateway::class);

        /** @var InvoiceRepositoryInterface $invoiceRepository */
        $invoiceRepository = $this->get(InvoiceRepositoryInterface::class);

        $invoice = $invoiceRepository->findOneById(InvoiceId::from($request->get('invoiceId'), false));
        if (null === $invoice) {
            throw new SecurityException('User tried to guess an Invoice ID', [
                'invoice' => $request->get('invoiceId'),
            ]);
        }

        /*
         * Betaalmethode overnemen uit bestelling
         */
        $method = null;
        if (!$invoice->getOrders()->isEmpty()) {
            /** @var Order $order */
            $order = $invoice->getOrders()->first();

            $method = $order->getPaymentMethod();
        }

        /*
         * Openstaande betaling? Dan 1x status controleren, anders redirecten naar Mollie
         */
        $payment = $invoice->findPendingPayment();
        if (null !== $payment) {
            // Indien nodig status controleren
            if (null !== $request->get('payment') && $request->get('payment') == $payment->getId()) {
                return $this->redirect($mollieGateway->getPaymentUrlForInvoice($invoice, $method));
            }

            $commandBus->handle(SynchronizePendingPaymentsCommand::create($invoice->getId()));

            $this->setBridgeVariable('paymentId', $payment->getId()->getString());

            return $this->render(':frontend/payment/default:index.html.twig', [
                'invoice' => $invoice,
            ]);
        }

        /*
         * Factuur is al betaald
         */
        if ($invoice->isPaid()) {
            $orders = $invoice->getOrders();

            // Factuur zonder bestelling, of factuur met meerdere bestellingen
            if ($orders->isEmpty() || $orders->count() > 1) {
                return $this->redirectToRoute('app.frontend.homepage');
            }

            return $this->redirectToRoute('app.frontend.order.status', [
                'orderId' => $orders[0]->getId(),
                'host' => $orders[0]->getTemplate()->getPartner()->getCountryProfile()->getHost(),
            ]);
        }

        return $this->redirect($mollieGateway->getPaymentUrlForInvoice($invoice, $method));
    }
}
