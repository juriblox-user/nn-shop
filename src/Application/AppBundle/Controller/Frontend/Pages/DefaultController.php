<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Pages;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Domain\Pages\Repository\PageRepositoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DefaultController.
 */
class DefaultController extends AbstractController
{
    /**
     * @param string  $code
     * @param Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return Response
     */
    public function pageAction(string $code, Request $request): Response
    {
        $profile = $request->get('_profile');
        if (!($page = $this->get(PageRepositoryInterface::class)->findOneBy(['code' => $code, 'profile' => $profile]))) {
            throw new NotFoundHttpException(sprintf('Kan pagina voor code %s niet vinden', $code));
        }

        return $this->render(':frontend/pages/default:page.html.twig', [
            'page' => $page,
        ]);
    }

    /**
     * @param Request $request
     *
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return Response
     */
    public function aboutAction(Request $request): Response
    {
        $profile = $request->get('_profile');
        if (!($about = $this->get(PageRepositoryInterface::class)->findOneBy(['code' => 'about', 'profile' => $profile]))) {
            throw new NotFoundHttpException(sprintf('Kan pagina voor code %s niet vinden', 'about'));
        }

        return $this->render(':frontend/pages/default:about.html.twig', [
            'page' => $about,
        ]);
    }

    /**
     * @param Request $request
     *
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return Response
     */
    public function privacyAction(Request $request): Response
    {
        $profile = $request->get('_profile');
        if (!($about = $this->get(PageRepositoryInterface::class)->findOneBy(['code' => 'privacy', 'profile' => $profile]))) {
            throw new NotFoundHttpException(sprintf('Kan pagina voor code %s niet vinden', 'privacy'));
        }

        return $this->render(':frontend/pages/default:about.html.twig', [
            'page' => $about,
        ]);
    }
}
