<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Express;

use Core\Application\CoreBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    /**
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('frontend/express/default/index.html.twig');
    }
}
