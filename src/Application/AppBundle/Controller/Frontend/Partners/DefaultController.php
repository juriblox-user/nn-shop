<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Partners;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Domain\Templates\QueryBuilder\PartnerQueryBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController.
 */
class DefaultController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request): Response
    {
        $profile = $request->get('_profile');
        /** @var PartnerQueryBuilderInterface $partnerBuilder */
        $partnerBuilder = $this->get(PartnerQueryBuilderInterface::class);

        $partnerBuilder
            ->reset()
            ->filterProfile($profile)
            ->excludeWithSlug('digitrage')
            ->excludeWithoutLogo();

        return $this->render(':frontend/partners:index.html.twig', [
            'partners' => $partnerBuilder->build()->getResult(),
        ]);
    }
}
