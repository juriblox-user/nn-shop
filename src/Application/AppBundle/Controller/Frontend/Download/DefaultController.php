<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Download;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\Common\Exception\SecurityException;
use Core\Domain\Value\Security\Token;
use NnShop\Application\AppBundle\Services\Manager\DocumentFileManager;
use NnShop\Domain\Templates\Entity\Document;
use NnShop\Domain\Templates\Enumeration\DocumentStatus;
use NnShop\Domain\Templates\Event\Document\DocumentDownloadedEvent;
use NnShop\Domain\Templates\Repository\DocumentRepositoryInterface;
use NnShop\Domain\Templates\Value\DocumentId;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    const MAXIMUM_DOWNLOADS = 20;

    /** @var DocumentFileManager */
    private $fileManager;

    /**
     * DefaultController constructor.
     *
     * @param DocumentFileManager $fileManager
     */
    public function __construct(DocumentFileManager $fileManager)
    {
        $this->fileManager = $fileManager;
    }

    /**
     * @param         $format
     * @param Request $request
     *
     * @throws SecurityException
     * @throws \Doctrine\ORM\EntityNotFoundException
     *
     * @return Response
     */
    public function downloadAction($format, Request $request)
    {
        /** @var MessageBus $eventBus */
        $eventBus = $this->get('event_bus');

        /** @var DocumentRepositoryInterface $repository */
        $repository = $this->get(DocumentRepositoryInterface::class);

        // Document ophalen
        $document = $repository->getById(DocumentId::from($request->get('documentId')));

        // Toegang controleren
        $this->checkAccess($document, $request);

        // TODO: quick 'n dirty truc: de links vanuit de abonnementen gebruiken "nonce", dus daar geen maximum op de downloads
        if (null === $request->get('nonce') && $document->getDownloads() >= self::MAXIMUM_DOWNLOADS) {
            return new Response('Dit document is te vaak gedownload. Neem contact op met docs@legalfit.eu voor meer informatie.', 429);
        }

        $eventBus->handle(DocumentDownloadedEvent::create($document->getId()));

        // Download als PDF
        if (DocumentFileManager::TYPE_PDF === $format) {
            return $this->fileManager->streamPdf($document);
        }

        // Download als Word-document
        elseif (DocumentFileManager::TYPE_WORD2007 === $format) {
            return $this->fileManager->streamWord2007($document);
        }

        // Download als Markdown
        elseif (DocumentFileManager::TYPE_MARKDOWN === $format) {
            return $this->fileManager->streamMarkdown($document);
        }

        // Onbekend formaat

        throw $this->createNotFoundException();
    }

    /**
     * Toegang tot een bepaald document afschermen.
     *
     * @param Document $document
     * @param Request  $request
     *
     * @throws SecurityException
     */
    private function checkAccess(Document $document, Request $request)
    {
        /*
         * TODO
         * Zodra de abonnementen en de accounts zijn geïmplementeerd vervalt de token, en wordt dit vervangen door een
         * nonce (class: DocumentNonce). De nonce wordt gegenereerd op basis van Document::$token maar verloopt in
         * 15 minuten. Voor een abonnement kan https://download.juridox.nl/[subscription-id]/pdf dan redirecten naar
         * deze route, met de nonce erbij. static::indexAction() doet dit dan ook.
         */

        $token = $request->get('token');
        if (null === $token) {
            $token = $request->get('nonce');
        }

        if (null === $token) {
            throw new SecurityException(sprintf('Cannot verify access to Document [%s]', $document->getId()));
        }

        if (!Token::valid($token)) {
            throw new SecurityException(sprintf('Document token for Document [%s] is not properly formatted', $document->getId()));
        }

        if ($document->getToken() != Token::from($token)) {
            throw new SecurityException(sprintf('Invalid document download token for Document [%s]', $document->getId()));
        }

        // Alleen gegenereerde documenten kunnen worden gedownload
        if ($document->getStatus()->isNot(DocumentStatus::GENERATED)) {
            throw new SecurityException(sprintf('User tried to download Document [%s] with an invalid DocumentStatus', $document->getId()));
        }
    }
}
