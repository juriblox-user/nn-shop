<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Wizard;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Domain\Templates\Repository\CompanyTypeRepositoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        /** @var CompanyTypeRepositoryInterface $typeRepository */
        $typeRepository = $this->get(CompanyTypeRepositoryInterface::class);

        return $this->render('frontend/wizard/default/index.html.twig', [
            'types' => $typeRepository->findAllOrderedByTitle($request->get('_profile')),
        ]);
    }
}
