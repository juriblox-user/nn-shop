<?php

namespace NnShop\Application\AppBundle\Controller\Frontend\Wizard;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Wizard\CompanyActivitiesRequest;
use NnShop\Application\AppBundle\Forms\Frontend\Type\Wizard\CompanyActivitiesType;
use NnShop\Domain\Templates\Entity\CompanyType;
use NnShop\Domain\Templates\Repository\CompanyTypeRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ActivitiesController extends AbstractController
{
    /**
     * @param Request $request
     * @param string  $typeSlug
     *
     * @throws \Doctrine\ORM\EntityNotFoundException
     *
     * @return Response
     */
    public function indexAction(Request $request, string $typeSlug)
    {
        /** @var CompanyTypeRepositoryInterface $typeRepository */
        $typeRepository = $this->get(CompanyTypeRepositoryInterface::class);

        $companyType = $typeRepository->getBySlug($typeSlug);

        /*
         * Formulier
         */
        $formRequest = new CompanyActivitiesRequest($companyType);

        $form = $this->createNamedForm(null, CompanyActivitiesType::class, $formRequest);
        $form->add('generate', SubmitType::class, [
            'label' => 'Naar mijn advies',
            'translation_domain' => 'forms',
        ]);

        $form->handleRequest($request);

        $formRequest->bind($form);

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->forward('AppBundle:Frontend/Wizard/Activities:templates', [
                'request' => $formRequest,
                'type' => $companyType,
            ]);
        }

        return $this->render('frontend/wizard/activities/index.html.twig', [
            'form' => $form->createView(),
            'type' => $companyType,
        ]);
    }

    /**
     * @param CompanyActivitiesRequest $request
     * @param CompanyType              $type
     *
     * @return Response
     */
    public function templatesAction(CompanyActivitiesRequest $request, CompanyType $type)
    {
        /** @var TemplateRepositoryInterface $templateRepository */
        $templateRepository = $this->get(TemplateRepositoryInterface::class);

        return $this->render(':frontend/wizard/activities:templates.html.twig', [
            'templates' => $templateRepository->findVisibleByActivityIds($request->getActivities()),
            'type' => $type,
        ]);
    }
}
