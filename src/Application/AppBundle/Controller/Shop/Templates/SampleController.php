<?php

namespace NnShop\Application\AppBundle\Controller\Shop\Templates;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Application\AppBundle\Services\Manager\SampleFileManager;
use NnShop\Application\AppBundle\Services\ShopContext;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SampleController extends AbstractController
{
    /** @var SampleFileManager */
    private $sampleManager;

    public function __construct(SampleFileManager $sampleFileManager)
    {
        $this->sampleManager = $sampleFileManager;
    }

    /**
     * @param string  $categorySlug
     * @param string  $templateSlug
     * @param Request $request
     *
     * @throws \NnShop\Common\Exceptions\ShopContextException
     *
     * @return Response
     */
    public function indexAction(string $categorySlug, string $templateSlug, Request $request)
    {
        /** @var ShopContext $context */
        $context = $this->get(ShopContext::class);
        if (!$context->isShopContext()) {
            throw $this->createNotFoundException();
        }

        /** @var TemplateRepositoryInterface $repository */
        $repository = $this->get(TemplateRepositoryInterface::class);

        /*
         * Template ophalen
         */
        $template = $repository->findOneBySlugs($categorySlug, $templateSlug);
        if (null === $template) {
            //$template = $repository->getByLegacySlugs($categorySlug, $templateSlug);
        }

        if (null === $template || !$template->isPublished() || !$template->hasSample()) {
            throw $this->createNotFoundException();
        }

        return $this->sampleManager->stream($template);
    }
}
