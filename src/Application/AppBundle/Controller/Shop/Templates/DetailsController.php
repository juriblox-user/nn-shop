<?php

namespace NnShop\Application\AppBundle\Controller\Shop\Templates;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Application\AppBundle\Services\Context\ReferrerContext;
use NnShop\Application\AppBundle\Services\ShopContext;
use NnShop\Domain\Orders\Command\Order\CreateAnonymousOrderCommand;
use NnShop\Domain\Orders\Command\Order\CreateCustomerOrderCommand;
use NnShop\Domain\Orders\Command\Order\StartOrderCommand;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Enumeration\CustomerStatus;
use NnShop\Domain\Orders\Value\OrderId;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DetailsController extends AbstractController
{
    /**
     * @param string $categorySlug
     * @param string $templateSlug
     * @param Request $request
     * @param ReferrerContext $referrerContext
     *
     * @return Response
     */
    public function indexAction(string $categorySlug, string $templateSlug, Request $request, ReferrerContext $referrerContext): Response
    {
        $profile = $request->get('_profile');

        if(!$profile) {
            throw new NotFoundHttpException();
        }

        /** @var ShopContext $context */
        $context = $this->get(ShopContext::class);
        if (!$context->isShopContext()) {
            throw $this->createNotFoundException();
        }

        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /** @var TemplateRepositoryInterface $templateRepository */
        $templateRepository = $this->get(TemplateRepositoryInterface::class);

        /** @var Customer|null $customer */
        $customer = $this->getUser();
        if (!$customer instanceof Customer) {
            $customer = null;
        }

        /*
         * Template ophalen
         */
        $template = $templateRepository->findOneBySlugs($categorySlug, $templateSlug);

        if (null === $template || !$template->isPublished()) {
            throw $this->createNotFoundException();
        }

        /*
         * Bestelling starten
         */
        $formBuilder = $this->createFormBuilder();

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $orderId = OrderId::generate();

            // Bestelling van een aangemelde gebruiker
            if (null !== $customer && $customer->getStatus()->is(CustomerStatus::ACTIVE)) {
                $command = CreateCustomerOrderCommand::create($orderId, $template->getId(), $customer->getId());

                if ($referrerContext->hasReferrer()) {
                    $command->setReferrerId($referrerContext->getReferrer()->getId());
                }

                $commandBus->handle($command);
            }

            // Bestelling van een (nog) onbekende gebruiker
            else {
                $command = CreateAnonymousOrderCommand::create($orderId, $template->getId());

                if ($referrerContext->hasReferrer()) {
                    $command->setReferrerId($referrerContext->getReferrer()->getId());
                }

                $commandBus->handle($command);
            }

            // Bestelling starten
            $commandBus->handle(StartOrderCommand::create($orderId));

            return $this->redirectToRoute('app.frontend.order', [
                'templateSlug' => $template->getSlug(),
                'orderId' => $orderId,
                'host' => $template->getPartner()->getCountryProfile()->getHost(),

                // GA-tracking overnemen
                '_ga' => $request->query->has('_ga') ? $request->query->get('_ga') : null,
            ]);
        }

        $response = new Response($this->renderView('shop/templates/details/index.html.twig', [
            'template' => $template,
            'form' => $form->createView(),
            'profile' => $profile,
        ]));

        if ($template->isHidden()) {
            $response->headers->set('X-Robots-Tag', 'index, follow');
        }

        return $response;
    }
}
