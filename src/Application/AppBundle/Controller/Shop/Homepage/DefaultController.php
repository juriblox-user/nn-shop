<?php

namespace NnShop\Application\AppBundle\Controller\Shop\Homepage;

use Core\Application\CoreBundle\Controller\AbstractController;

class DefaultController extends AbstractController
{
    public function indexAction()
    {
        return $this->render('shop/default/index.html.twig');
    }
}
