<?php

namespace NnShop\Application\AppBundle\Controller\Shared\Helper;

use Core\Application\CoreBundle\Controller\AbstractController;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use NnShop\Domain\Translation\Entity\Profile;
use Symfony\Component\HttpFoundation\Response;

class ShopSelectorController extends AbstractController
{
    /**
     * @param Profile $profile
     *
     * @return Response
     */
    public function headerAction(Profile $profile)
    {
        /** @var TemplateRepositoryInterface $templateRepository */
        $templateRepository = $this->get(TemplateRepositoryInterface::class);

        $categories = new ArrayCollection();
        foreach ($templateRepository->findVisible($profile) as $template) {
            foreach ($template->getCategories() as $category) {

                /** @var ArrayCollection $categoryMatches */
                $categoryMatches = $categories->matching(
                    Criteria::create()->where(
                        Criteria::expr()->eq('entity', $category)
                    )
                );

                if ($categoryMatches->isEmpty()) {
                    $categoryItem = (object) [
                        'entity' => $category,
                        'templates' => new ArrayCollection(),
                        'totalItems' => 0,
                        'totalTemplates' => 0,
                    ];

                    $categories->add($categoryItem);
                } else {
                    $categoryItem = $categoryMatches->first();
                }

                $categoryItem->templates->add($template);

                ++$categoryItem->totalItems;
                ++$categoryItem->totalTemplates;
            }
        }

        $orderedCategories = [];
        foreach ($categories as $category) {
            $orderedCategories[$category->entity->getTitle()] = $category;
        }

        ksort($orderedCategories);

        return $this->render('shared/_helpers/shop-selector.html.twig', [
            'categories' => $orderedCategories,
        ]);
    }
}
