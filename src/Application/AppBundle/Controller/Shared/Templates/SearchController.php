<?php

namespace NnShop\Application\AppBundle\Controller\Shared\Templates;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Application\AppBundle\Services\UrlGenerator\TemplateUrlGenerator;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Templates\QueryBuilder\TemplateQueryBuilderInterface;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class SearchController extends AbstractController
{
    private $templateRepository;

    private $templateurlGenerator;

    public function __construct(TemplateRepositoryInterface $templateRepository, TemplateUrlGenerator $templateUrlGenerator)
    {
        $this->templateRepository = $templateRepository;
        $this->templateurlGenerator = $templateUrlGenerator;
    }

    const QUERY_PARAM = 'zoek';

    /**
     * @param Request $request
     *
     * @throws \NnShop\Common\Exceptions\ShopContextException
     *
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $profile = $request->attributes->get('_profile');

        $paginator = $this->get('knp_paginator');

        /** @var TemplateQueryBuilderInterface $builder */
        $builder = $this->get(TemplateQueryBuilderInterface::class);

        $builder
            ->reset()
            ->filterCorrupted()
            ->filterPublished()
            ->filterHidden();

        // Zoeken op sleutelwoorden
        if ($request->query->has(self::QUERY_PARAM) && '' !== $request->query->get(self::QUERY_PARAM)) {
            $builder->matchKeywords($request->query->get(self::QUERY_PARAM));

            $builder->filterProfile($profile->getId());
        }


        $builder->filterProfile($profile->getId());
        /** @var SlidingPagination|Template[] $pagination */
        $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 9);

        $pagination->setParam(self::QUERY_PARAM, $request->query->get(self::QUERY_PARAM));

        return $this->render('shared/templates/search/index.html.twig', [
            'pagination' => $pagination,
            'criteria' => $request->query->get(self::QUERY_PARAM),
        ]);
    }

    /**
     * @param Request $request
     * @Method({"POST"})
     * @return Response
     * @throws \Exception
     */
    public function searchAction(Request $request): Response
    {
        $requestString = $request->get('q');

        $templates = $this->templateRepository->findPublishedByString($requestString);

        $result = [];

        if ($templates) {
            foreach ($templates as $template) {
                $url = $this->templateurlGenerator->generateDetailsUrl($template);
                $values = Array('title' => $template->getTitle(), 'url' => $url);
                array_push($result, $values);
            }
        }

        return new Response(json_encode($result));
    }
}
