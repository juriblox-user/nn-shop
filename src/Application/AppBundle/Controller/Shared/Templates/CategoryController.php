<?php

namespace NnShop\Application\AppBundle\Controller\Shared\Templates;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Templates\QueryBuilder\TemplateQueryBuilderInterface;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use NnShop\Domain\Templates\Repository\CategoryRepositoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends AbstractController {

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    /**
     * CategoryController constructor.
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function indexAction($categorySlug, Request $request):Response
    {
        $paginator = $this->get('knp_paginator');
        $builder = $this->get(TemplateQueryBuilderInterface::class);

        $category = $this->categoryRepository->getBySlug($categorySlug);

        $builder
            ->reset()
            ->filterCorrupted()
            ->filterPublished()
            ->filterCategory($category)
            ->filterHidden();

        /** @var SlidingPagination|Template[] $pagination */
        $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 9);

        return $this->render('shared/templates/category/index.html.twig', [
            'pagination' => $pagination,
            'category' => $category,
        ]);
    }
}
