<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Translation;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Application\AppBundle\Forms\Backend\Request\Translation\CreateLocaleRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Translation\EditLocaleRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\Translation\CreateLocaleType;
use NnShop\Application\AppBundle\Forms\Backend\Type\Translation\EditLocaleType;
use NnShop\Domain\Translation\Command\Locale\CreateLocaleCommand;
use NnShop\Domain\Translation\Command\Locale\UpdateLocaleCommand;
use NnShop\Domain\Translation\Entity\Locale;
use NnShop\Domain\Translation\QueryBuilder\LocaleQueryBuilderInterface;
use NnShop\Domain\Translation\Value\LocaleId;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ProfileController.
 *
 * @Route("/translation/languages")
 */
class LocaleController extends AbstractController
{
    /**
     * @Route("/language/{id}", name="app.backend.translation.locale.edit")
     * @ParamConverter("locale", class="NnShop\Domain\Translation\Entity\Locale")
     *
     * @param Locale  $locale
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function editAction(Locale $locale, Request $request): Response
    {
        $localeRequest = new EditLocaleRequest($locale);
        $form = $this->createForm(EditLocaleType::class, $localeRequest);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $commandBus = $this->get('command_bus');
            $command = UpdateLocaleCommand::update($locale->getId());
            $command->setLocale($localeRequest->getLocale());
            $commandBus->handle($command);

            $this->clearCache();

            return new RedirectResponse($this->generateUrl('app.backend.translation.locale.index'));
        }

        return $this->render(':backend/translation/locale:edit.html.twig', [
            'locale' => $localeRequest,
            'form' => $form->createView(),
            'mode' => 'edit',
        ]);
    }

    /**
     * @Route("/add", name="app.backend.translation.locale.create")
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function createAction(Request $request): Response
    {
        $localeRequest = new CreateLocaleRequest();

        $form = $this->createForm(CreateLocaleType::class, $localeRequest);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $commandBus = $this->get('command_bus');
            $command = CreateLocaleCommand::create(LocaleId::generate());
            $command->setLocale($localeRequest->getLocale());
            $commandBus->handle($command);

            $this->clearCache();

            return new RedirectResponse($this->generateUrl('app.backend.translation.locale.index'));
        }

        return $this->render(':backend/translation/locale:edit.html.twig', [
            'locale' => $localeRequest,
            'form' => $form->createView(),
            'mode' => 'new',
        ]);
    }

    /**
     * @Route("/", name="app.backend.translation.locale.index")
     *
     * @param Request $request
     *
     * @throws \LogicException
     *
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $paginator = $this->get('knp_paginator');

        $builder = $this->get(LocaleQueryBuilderInterface::class);
        $builder->reset();

        $pageSubtitle = 'Alle talen';

        $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 20);

        return $this->render(':backend/translation/locale:index.html.twig', [
            'pageSubtitle' => $pageSubtitle,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @throws \Exception
     */
    private function clearCache()
    {
        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);
        $output = new NullOutput();

        $inputCacheClear = new ArrayInput([
            'command' => 'cache:clear',
        ]);
        $application->run($inputCacheClear, $output);

        $inputCacheWarmup = new ArrayInput([
            'command' => 'cache:warmup',
        ]);
        $application->run($inputCacheWarmup, $output);
    }
}
