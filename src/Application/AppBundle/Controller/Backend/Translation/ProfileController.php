<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Translation;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Application\AppBundle\Forms\Backend\Request\Translation\CreateProfileRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Translation\EditProfileRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\Translation\CreateProfileType;
use NnShop\Application\AppBundle\Forms\Backend\Type\Translation\EditProfileType;
use NnShop\Domain\Translation\Command\Profile\CreateProfileCommand;
use NnShop\Domain\Translation\Command\Profile\UpdateProfileCommand;
use NnShop\Domain\Translation\Entity\Profile;
use NnShop\Domain\Translation\QueryBuilder\ProfileQueryBuilderInterface;
use NnShop\Domain\Translation\Value\ProfileId;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ProfileController.
 *
 * @Route("/translation/countries")
 */
class ProfileController extends AbstractController
{
    /**
     * @Route("/countries/{id}", name="app.backend.translation.profile.edit")
     * @ParamConverter("profile", class="NnShop\Domain\Translation\Entity\Profile")
     *
     * @param Profile $profile
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function editAction(Profile $profile, Request $request): Response
    {
        $profileRequest = new EditProfileRequest($profile);
        $form = $this->createForm(EditProfileType::class, $profileRequest);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $commandBus = $this->get('command_bus');
            $command = UpdateProfileCommand::create($profile->getId());
            $command->setCountry($profileRequest->getCountry());
            $command->setTimezone($profileRequest->getTimezone());
            $command->setCurrency($profileRequest->getCurrency());
            $command->setMainLocale($profileRequest->getMainLocale());
            $command->setUrl($profileRequest->getUrl());
            $command->setMollieKey($profileRequest->getMollieKey());
            $command->setEmail($profileRequest->getEmail());
            $command->setPhone($profileRequest->getPhone());
            $command->setEntityName($profileRequest->getEntityName());
            $command->setEntityCountry($profileRequest->getEntityCountry());
            $command->setAddressLine1($profileRequest->getAddressLine1());
            $command->setAddressLine2($profileRequest->getAddressLine2());
            $command->setZipcode($profileRequest->getZipcode());
            $command->setCity($profileRequest->getCity());
            $command->setProvince($profileRequest->getProvince());
            $command->setCocLabel($profileRequest->getCocLabel());
            $command->setCocNumber($profileRequest->getCocNumber());
            $command->setVatLabel($profileRequest->getVatLabel());
            $command->setVatNumber($profileRequest->getVatNumber());
            $commandBus->handle($command);

            $this->clearCache();

            return new RedirectResponse($this->generateUrl('app.backend.translation.profile.index'));
        }

        return $this->render(':backend/translation/profile:edit.html.twig', [
            'profile' => $profileRequest,
            'form' => $form->createView(),
            'mode' => 'edit',
        ]);
    }

    /**
     * @Route("/add", name="app.backend.translation.profile.create")
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function createAction(Request $request): Response
    {
        $profileRequest = new CreateProfileRequest();
        $form = $this->createForm(CreateProfileType::class, $profileRequest);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (!$profileRequest->getMainLocale()) {
                throw new \RuntimeException(sprintf("Main locale can't be found!"));
            }

            $commandBus = $this->get('command_bus');
            $command = CreateProfileCommand::create(ProfileId::generate(), $profileRequest->getMainLocale()->getId());
            $command->setCountry($profileRequest->getCountry());
            $command->setTimezone($profileRequest->getTimezone());
            $command->setCurrency($profileRequest->getCurrency());
            $command->setUrl($profileRequest->getUrl());
            $command->setMollieKey($profileRequest->getMollieKey());
            $command->setEmail($profileRequest->getEmail());
            $command->setPhone($profileRequest->getPhone());
            $command->setEntityName($profileRequest->getEntityName());
            $command->setEntityCountry($profileRequest->getEntityCountry());
            $command->setAddressLine1($profileRequest->getAddressLine1());
            $command->setAddressLine2($profileRequest->getAddressLine2());
            $command->setZipcode($profileRequest->getZipcode());
            $command->setCity($profileRequest->getCity());
            $command->setProvince($profileRequest->getProvince());
            $command->setCocLabel($profileRequest->getCocLabel());
            $command->setCocNumber($profileRequest->getCocNumber());
            $command->setVatLabel($profileRequest->getVatLabel());
            $command->setVatNumber($profileRequest->getVatNumber());
            $commandBus->handle($command);

            $this->clearCache();

            return new RedirectResponse($this->generateUrl('app.backend.translation.profile.index'));
        }

        return $this->render(':backend/translation/profile:edit.html.twig', [
            'profile' => $profileRequest,
            'form' => $form->createView(),
            'mode' => 'create',
        ]);
    }

    /**
     * @Route("/", name="app.backend.translation.profile.index")
     *
     * @param Request $request
     *
     * @throws \LogicException
     *
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $paginator = $this->get('knp_paginator');

        $builder = $this->get(ProfileQueryBuilderInterface::class);
        $builder->reset();

        $pageSubtitle = 'Alle landen';

        $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 20);

        return $this->render(':backend/translation/profile:index.html.twig', [
            'pageSubtitle' => $pageSubtitle,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @throws \Exception
     */
    private function clearCache()
    {
        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);
        $output = new NullOutput();

        $inputCacheClear = new ArrayInput([
            'command' => 'cache:clear --no-warmup',
        ]);
        $application->run($inputCacheClear, $output);

        $inputCacheWarmup = new ArrayInput([
            'command' => 'cache:warmup',
        ]);
        $application->run($inputCacheWarmup, $output);
    }
}
