<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Pages;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Application\AppBundle\Forms\Backend\Request\Pages\CreatePageRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Pages\EditPageRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\Pages\CreatePageType;
use NnShop\Application\AppBundle\Forms\Backend\Type\Pages\EditPageType;
use NnShop\Domain\Orders\QueryBuilder\OrderQueryBuilderInterface;
use NnShop\Domain\Pages\Command\Page\CreatePageCommand;
use NnShop\Domain\Pages\Command\Page\DeletePageCommand;
use NnShop\Domain\Pages\Command\Page\UpdatePageCommand;
use NnShop\Domain\Pages\Entity\Page;
use NnShop\Domain\Pages\QueryBuilder\PageQueryBuilderInterface;
use NnShop\Domain\Pages\Value\PageId;
use NnShop\Domain\Translation\Entity\Profile;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PaceController.
 *
 * @Route("/pages")
 */
class PageController extends AbstractController
{
    /**
     * @Route("/deletepage/{slug}", name="app.backend.pages.page.delete")
     * @ParamConverter("page", class="NnShop\Domain\Pages\Entity\Page")
     * @Method({"POST"})
     *
     * @param Page $page
     *
     * @throws \InvalidArgumentException
     *
     * @return Response
     */
    public function deleteAction(Page $page): Response
    {
        $commandBus = $this->get('command_bus');
        $command = DeletePageCommand::create($page->getId());
        $commandBus->handle($command);

        return new RedirectResponse($this->generateUrl('app.backend.pages.page.index', ['profile_id' => $page->getProfile()->getId()]));
    }

    /**
     * @Route("/page/{slug}", name="app.backend.pages.page.edit")
     * @ParamConverter("page", class="NnShop\Domain\Pages\Entity\Page")
     *
     * @param Request $request
     * @param Page    $page
     *
     * @throws \InvalidArgumentException
     *
     * @return Response
     */
    public function editAction(Request $request, Page $page): Response
    {
        $formRequest = new EditPageRequest($page);

        $form = $this->createNamedForm(null, EditPageType::class, $formRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $command = UpdatePageCommand::create($formRequest->getId());

            $commandBus = $this->get('command_bus');

            $command->setCode($formRequest->getCode());
            $command->setTitle($formRequest->getTitle());
            $command->setContent($formRequest->getContent());
            $command->setLead($formRequest->getLead());
            $command->setFooter($formRequest->getFooter());
            $command->setLastUpdated($formRequest->getLastUpdated());

            if ($formRequest->deletePdf()) {
                $command->setDeletePdf(true);
            } elseif (null !== $formRequest->getPdf()) {
                $command->setPdf($formRequest->getPdf());
            }

            $commandBus->handle($command);

            return new RedirectResponse($this->generateUrl('app.backend.pages.page.index',
                ['profile_id' => $page->getProfile()->getId(),
                ]));
        }

        return $this->render(':backend/pages/page:edit.html.twig', [
            'page' => $page,
            'form' => $form->createView(),
            'mode' => 'edit',
        ]);
    }

    /**
     * @Route("/addpage/{profile_id}", name="app.backend.pages.page.create")
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @throws \InvalidArgumentException
     *
     * @return Response
     */
    public function createAction(Request $request, Profile $profile): Response
    {
        $formRequest = new CreatePageRequest($profile);
        $form = $this->createNamedForm(null, CreatePageType::class, $formRequest);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $command = CreatePageCommand::create(PageId::generate(), $formRequest->getProfile()->getId());

            $commandBus = $this->get('command_bus');

            $command->setCode($formRequest->getCode());
            $command->setTitle($formRequest->getTitle());
            $command->setContent($formRequest->getContent());
            $command->setLead($formRequest->getLead());
            $command->setFooter($formRequest->getFooter());
            $command->setLastUpdated($formRequest->getLastUpdated());

            if (null !== $formRequest->getPdf()) {
                $command->setPdf($formRequest->getPdf());
            }

            $commandBus->handle($command);

            return new RedirectResponse($this->generateUrl(
                'app.backend.pages.page.index',
                ['profile_id' => $profile->getId()]));
        }

        return $this->render(':backend/pages/page:edit.html.twig', [
            'page' => $formRequest,
            'form' => $form->createView(),
            'mode' => 'new',
        ]);
    }

    /**
     * @Route("/{profile_id}", name="app.backend.pages.page.index")
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @throws \LogicException
     *
     * @return Response
     */
    public function indexAction(Request $request, Profile $profile): Response
    {
        $paginator = $this->get('knp_paginator');

        /** @var OrderQueryBuilderInterface $builder */
        $builder = $this->get(PageQueryBuilderInterface::class);
        $builder
            ->reset();

        $pageSubtitle = 'Alle paginas';

        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 20);

        return $this->render(':backend/pages/page:index.html.twig', [
            'pageSubtitle' => $pageSubtitle,
            'pagination' => $pagination,
            'profile' => $profile,
        ]);
    }
}
