<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Dashboard;

use Core\Application\CoreBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="app.backend.dashboard.default.index")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        return $this->render(':backend/dashboard/default:index.html.twig', [
            'pageTitle' => 'Dashboard',
        ]);
    }
}
