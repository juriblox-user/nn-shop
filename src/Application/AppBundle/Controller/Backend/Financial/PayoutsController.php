<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Financial;

use Core\Application\CoreBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PayoutsController extends AbstractController
{
    /**
     * @Route("/financial/payouts", name="app.backend.financial.payouts.index")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        return $this->render(':backend/dashboard/default:index.html.twig');
    }
}
