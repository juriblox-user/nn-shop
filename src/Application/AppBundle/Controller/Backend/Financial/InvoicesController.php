<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Financial;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\Application\CoreBundle\Forms\Type\IconSubmitType;
use NnShop\Application\AppBundle\Services\Manager\InvoicePdfManager;
use NnShop\Domain\Orders\Command\Payment\SynchronizePendingPaymentsCommand;
use NnShop\Domain\Orders\QueryBuilder\InvoiceQueryBuilderInterface;
use NnShop\Domain\Orders\Repository\InvoiceRepositoryInterface;
use NnShop\Domain\Orders\Value\InvoiceId;
use NnShop\Domain\Translation\Entity\Profile;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class InvoicesController extends AbstractController
{
    const VIEW_OVERDUE = 'overdue';

    const VIEW_OVERVIEW = 'overview';

    const VIEW_PAID = 'paid';

    const VIEW_PENDING = 'pending';

    /**
     * @Route("/financial/invoices/{invoiceId}/{view}", name="app.backend.financial.invoices.details", requirements={"invoiceId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$", "view": "overdue|overview|paid|pending"})
     *
     * @param Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     *
     * @return Response
     */
    public function detailsAction(Request $request): Response
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /** @var InvoiceRepositoryInterface $repository */
        $repository = $this->get(InvoiceRepositoryInterface::class);

        $invoiceId = $request->get('invoiceId');
        if (!InvoiceId::valid($invoiceId)) {
            throw $this->createNotFoundException();
        }

        $invoice = $repository->getById(InvoiceId::from($invoiceId));

        /*
         * Toolbar
         */
        $toolbar = $this->createFormBuilder()
            ->add('synchronize', IconSubmitType::class, [
                'icon' => 'icon icon-cycle',
                'label' => 'Betaalstatus vernieuwen',
                'translation_domain' => false,
            ])->getForm();

        $toolbar->handleRequest($request);

        if ($toolbar->isSubmitted() && $toolbar->isValid() && $toolbar->get('synchronize')->isClicked()) {
            $commandBus->handle(SynchronizePendingPaymentsCommand::create($invoice->getId()));

            $this->addFlash('success', 'De status van de betalingen is gesynchroniseerd met Mollie.');
        }

        return $this->render(':backend/financial/invoices:details.html.twig', [
            'invoice' => $invoice,
            'customer' => $invoice->getCustomer(),

            'toolbar' => $toolbar->createView(),
        ]);
    }

    /**
     * @Route("/financial/invoices/{invoiceId}/pdf", name="app.backend.financial.invoices.stream_pdf", requirements={"invoiceId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @param Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return Response
     */
    public function streamAction(Request $request): Response
    {
        $pdfManager = $this->get(InvoicePdfManager::class);

        /** @var InvoiceRepositoryInterface $repository */
        $repository = $this->get(InvoiceRepositoryInterface::class);

        $invoiceId = $request->get('invoiceId');
        if (!InvoiceId::valid($invoiceId)) {
            throw $this->createNotFoundException();
        }

        $invoice = $repository->getById(InvoiceId::from($invoiceId));

        return $pdfManager->stream($invoice);
    }

    /**
     * @Route("/financial/invoices/{view}/{profile_id}", name="app.backend.financial.invoices.index", requirements={"view": "overdue|overview|paid|pending"})
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param string  $view
     * @param Request $request
     * @param Profile $profile
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     *
     * @return Response
     */
    public function indexAction(string $view, Request $request, Profile $profile): Response
    {
        $query = $request->query;
        $enabledSearch = false;

        $paginator = $this->get('knp_paginator');

        /** @var InvoiceQueryBuilderInterface $builder */
        $builder = $this->get(InvoiceQueryBuilderInterface::class);
        $builder->reset();

        if ($query->get('invoiceNumber')) {
            $builder->filterInvoiceNumber($query->get('invoiceNumber'));
            $enabledSearch = true;
        }

        $pageSubtitle = 'Alle facturen';

        switch ($view) {
            case self::VIEW_OVERVIEW:
                break;

            case self::VIEW_OVERDUE:
                $builder->filterStatus()
                    ->includePending()
                    ->filterOverdue();

                $pageSubtitle = 'Achterstallige betalingen';

                break;

            case self::VIEW_PENDING:
                $builder->filterStatus()
                    ->includePending();

                $pageSubtitle = 'In afwachting van betaling';

                break;

            case self::VIEW_PAID:
                $builder->filterStatus()
                    ->includePaid();

                $pageSubtitle = 'Betaalde facturen';

                break;

            default:
                throw $this->createNotFoundException();
                break;
        }

        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 20);

        return $this->render(':backend/financial/invoices:index.html.twig', [
            'pageSubtitle' => $pageSubtitle,
            'pagination' => $pagination,
            'enabledSearch' => $enabledSearch,
            'searchAttributesBag' => $query,
        ]);
    }
}
