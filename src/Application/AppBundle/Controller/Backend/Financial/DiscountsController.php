<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Financial;

use Core\Application\CoreBundle\Controller\AbstractController;
use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Application\AppBundle\Forms\Backend\Request\Financial\CreateDiscountRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Financial\EditDiscountRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\Financial\CreateDiscountType;
use NnShop\Application\AppBundle\Forms\Backend\Type\Financial\EditDiscountType;
use NnShop\Domain\Orders\Command\Discount\CreateDiscountCommand;
use NnShop\Domain\Orders\Command\Discount\DeleteDiscountCommand;
use NnShop\Domain\Orders\Command\Discount\UpdateDiscountCommand;
use NnShop\Domain\Orders\Entity\Discount;
use NnShop\Domain\Orders\Enumeration\DiscountType;
use NnShop\Domain\Orders\Enumeration\OrderItemType;
use NnShop\Domain\Orders\QueryBuilder\DiscountQueryBuilderInterface;
use NnShop\Domain\Orders\QueryBuilder\InvoiceItemQueryBuilderInterface;
use NnShop\Domain\Orders\Repository\DiscountRepositoryInterface;
use NnShop\Domain\Orders\Services\Validator\DiscountValidator;
use NnShop\Domain\Orders\Value\DiscountId;
use NnShop\Domain\Translation\Entity\Profile;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DiscountsController extends AbstractController
{
    /**
     * @Route("/financial/discounts", name="app.backend.financial.discounts.index")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $paginator = $this->get('knp_paginator');

        /** @var DiscountQueryBuilderInterface $builder */
        $builder = $this->get(DiscountQueryBuilderInterface::class);
        $builder->reset();

        /** @var SlidingPagination|Discount[] $pagination */
        $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 20);

        return $this->render(':backend/financial/discounts:index.html.twig', [
            'pagination' => $pagination,
            'profile' => $this->getProfile($request),
        ]);
    }

    /**
     * @Route("/financial/discounts/add", name="app.backend.financial.discounts.create")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        $discountValidator = $this->get(DiscountValidator::class);

        /*
         * Formulier
         */
        $formRequest = new CreateDiscountRequest();

        $form = $this->createNamedForm(null, CreateDiscountType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {
            if (0 === preg_match(Discount::CODE_REGEX, $formRequest->getCode())) {
                $form->addError(new FormError('Een kortingscode mag alleen uit hoofdletters (A-Z) en cijfers (0-9) bestaan.'));
            }

            if ($form->isValid() && !$discountValidator->isUnique($formRequest->getCode())) {
                $form->addError(new FormError('Er bestaat al een kortingscode met deze code.'));
            }

            if ($form->isValid() && !$discountValidator->isValidEmails($formRequest->getEmails())) {
                $form->addError(new FormError('Geen geldig e-mailadres.'));
            }

            if ($form->isValid()) {
                $command = CreateDiscountCommand::prepare(DiscountId::generate());

                if ($formRequest->getType()->is(DiscountType::FIXED)) {
                    $command->setAmount($formRequest->getAmount());
                } else {
                    $command->setPercentage($formRequest->getPercentage());
                }

                $command->setTemplates(
                    $formRequest->hasTemplates() ? $formRequest->getTemplates() : new ArrayCollection()
                );

                $command->setCode($formRequest->getCode());
                $command->setTitle($formRequest->getTitle());
                $command->setMaxUses($formRequest->getMaxUses());
                $command->setActive($formRequest->isActive());
                $command->setEmails(
                    $formRequest->hasEmails() ? $formRequest->getEmails() : new ArrayCollection()
                );

                $commandBus->handle($command);

                $this->addFlash('success', 'De kennispartner is succesvol toegevoegd.');

                return $this->redirectToRoute('app.backend.financial.discounts.index');
            }
        }

        return $this->render(':backend/financial/discounts:create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/financial/discounts/{discountId}/edit", name="app.backend.financial.discounts.edit", requirements={"discountId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function editAction(Request $request)
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /** @var DiscountRepositoryInterface $discountRepository */
        $discountRepository = $this->get(DiscountRepositoryInterface::class);

        $discountValidator = $this->get(DiscountValidator::class);

        $discountId = $request->get('discountId');
        if (!DiscountId::valid($discountId)) {
            throw $this->createNotFoundException();
        }

        $discount = $discountRepository->findOneById(DiscountId::from($discountId));
        if (null === $discount) {
            throw $this->createNotFoundException();
        }

        /*
         * Formulier
         */
        $formRequest = new EditDiscountRequest($discount);

        $form = $this->createNamedForm(null, EditDiscountType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {
            if (0 === preg_match(Discount::CODE_REGEX, $formRequest->getCode())) {
                $form->addError(new FormError('Een kortingscode mag alleen uit hoofdletters (A-Z) en cijfers (0-9) bestaan.'));
            }

            if ($form->isValid() && !$discountValidator->isUnique($formRequest->getCode(), $discount)) {
                $form->addError(new FormError('Er bestaat al een kortingscode met deze code.'));
            }

            if ($form->isValid() && !$discountValidator->isValidEmails($formRequest->getEmails())) {
                $form->addError(new FormError('Geen geldig e-mailadres.'));
            }

            if ($form->isValid()) {
                $command = UpdateDiscountCommand::prepare($discount);

                $command->setTitle($formRequest->getTitle());
                $command->setCode($formRequest->getCode());
                $command->setType($formRequest->getType());
                $command->setMaxUses($formRequest->getMaxUses());
                $command->setActive($formRequest->isActive());

                $command->setTemplates(
                    $formRequest->hasTemplates() ? $formRequest->getTemplates() : new ArrayCollection()
                );
                $command->setEmails(
                    $formRequest->hasEmails() ? $formRequest->getEmails() : new ArrayCollection()
                );

                if ($formRequest->getType()->is(DiscountType::FIXED)) {
                    $command->setAmount($formRequest->getAmount());
                } else {
                    $command->setPercentage($formRequest->getPercentage());
                }

                $commandBus->handle($command);

                $this->addFlash('success', 'De gegevens van de kortingscode zijn succesvol gewijzigd.');

                return $this->redirectToRoute('app.backend.financial.discounts.index');
            }
        }

        return $this->render(':backend/financial/discounts:edit.html.twig', [
            'discount' => $discount,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/financial/discounts/{discountId}/usage/{profile_id}", name="app.backend.financial.discounts.usage", requirements={"discountId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function usageAction(Request $request, Profile $profile)
    {
        $paginator = $this->get('knp_paginator');

        /** @var DiscountRepositoryInterface $discountRepository */
        $discountRepository = $this->get(DiscountRepositoryInterface::class);
        $discountId = $request->get('discountId');
        if (!DiscountId::valid($discountId)) {
            throw $this->createNotFoundException();
        }

        $discount = $discountRepository->findOneById(DiscountId::from($discountId));
        if (null === $discount) {
            throw $this->createNotFoundException();
        }

        $profile = $this->getProfile($request);

        /** @var InvoiceItemQueryBuilderInterface $builder */
        $builder = $this->get(InvoiceItemQueryBuilderInterface::class);
        $builder->reset()
            ->filterDiscount($discount->getId())
            ->filterType(new OrderItemType(OrderItemType::DOCUMENT));

        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 20);

        return $this->render(':backend/financial/discounts:usage.html.twig', [
            'discount' => $discount,
            'profile' => $profile,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/financial/discounts/delete/{discount}", name="app.backend.financial.discounts.delete", requirements={"discountId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     * @Method({"POST"})
     *
     * @param Discount $discount
     *
     * @throws \InvalidArgumentException
     *
     * @return Response
     */
    public function deleteAction(Discount $discount): Response
    {
        $commandBus = $this->get('command_bus');
        $command = DeleteDiscountCommand::create($discount->getId());
        $commandBus->handle($command);

        return new RedirectResponse($this->generateUrl('app.backend.financial.discounts.index'));
    }

    /**
     * @param Request|null $request
     *
     * @return Profile
     */
    private function getProfile(Request $request = null)
    {
        /**
         * Chosen profile set in session so it can be used in menu.
         */
        $session = $request->getSession();

        $profileRepository = $this->get(ProfileRepositoryInterface::class);

        $profileId = $request->get('profile_id', $session->get('profile_id'));

        /** @var Profile $profile */
        $profile = $profileId ? $profileRepository->find($profileId) : $profileRepository->getFirst();

        if (!$session) {
            throw new \RuntimeException("Session isn't set!");
        }

        $session->set('profile_id', $profile->getId()->getString());

        return $profile;
    }
}
