<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Orders;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\Application\CoreBundle\Forms\Type\IconSubmitType;
use NnShop\Domain\Orders\Command\Order\ChangeOrderPaymentMethodCommand;
use NnShop\Domain\Orders\Command\Order\ManualCheckCompletedCommand;
use NnShop\Domain\Orders\Command\Order\ManualCustomCompletedCommand;
use NnShop\Domain\Orders\Command\Order\RepairOrderAsyncCommand;
use NnShop\Domain\Orders\Enumeration\OrderStatus;
use NnShop\Domain\Orders\QueryBuilder\OrderQueryBuilderInterface;
use NnShop\Domain\Orders\Repository\OrderRepositoryInterface;
use NnShop\Domain\Orders\Value\OrderId;
use NnShop\Domain\Translation\Entity\Profile;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Workflow\Workflow;

class DefaultController extends AbstractController
{
    const VIEW_COMPLETED = 'completed';

    const VIEW_MANUAL = 'manual';

    const VIEW_OVERVIEW = 'overview';

    const VIEW_PENDING = 'pending';

    const VIEW_WARNING = 'warning';

    /**
     * @Route("/orders/{orderId}", name="app.backend.orders.default.details", requirements={"orderId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function detailsAction(Request $request): Response
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /** @var OrderRepositoryInterface $repository */
        $repository = $this->get(OrderRepositoryInterface::class);

        /** @var Workflow $workflow */
        $workflow = $this->get('state_machine.order');

        $orderId = $request->get('orderId');
        if (!OrderId::valid($orderId)) {
            throw $this->createNotFoundException();
        }

        $order = $repository->getById(OrderId::from($orderId));

        /*
         * Toolbar
         */
        $toolbarForm = $this->createFormBuilder()
            ->add('repair', IconSubmitType::class, [
                'icon' => 'icon icon-thunder-cloud',
                'label' => 'Bestelling repareren',
                'translation_domain' => false,
            ])->getForm();

        $toolbarForm->handleRequest($request);

        if ($toolbarForm->isSubmitted() && $toolbarForm->isValid() && $toolbarForm->get('repair')->isClicked()) {
            $commandBus->handle(RepairOrderAsyncCommand::create($order->getId()));

            $this->addFlash('success', 'De bestelling wordt op de achtergrond gerepareerd/bijgewerkt.');

            return $this->redirectToRoute('app.backend.orders.default.details', [
                'orderId' => $order->getId(),
            ]);
        }

        /*
         * Vervolgstappen
         */
        $actionsBuilder = $this->createFormBuilder();

        // Betaalmethode resetten
        if ($order->getStatus()->is(OrderStatus::PENDING_PAYMENT)) {
            $actionsBuilder->add('change_payment_method', SubmitType::class, [
                'label' => 'Betaalmethode wijzigen',
                'translation_domain' => false,
            ]);
        }

        // Handmatige controle
        if ($order->getStatus()->is(OrderStatus::MANUAL_CHECK)) {
            $actionsBuilder->add('check_completed', SubmitType::class, [
                'label' => 'Handmatige controle afgerond',
                'translation_domain' => false,
            ]);
        }

        // Maatwerk aanpassingen
        if ($order->getStatus()->is(OrderStatus::MANUAL_CUSTOM)) {
            $actionsBuilder->add('custom_completed', SubmitType::class, [
                'label' => 'Maatwerk aanpassingen afgerond',
                'translation_domain' => false,
            ]);
        }

        $actionsForm = $actionsBuilder->getForm();
        $actionsForm->handleRequest($request);

        /*
         * POST afhandelen
         */
        if ($actionsForm->isSubmitted() && $actionsForm->isValid()) {
            // Betaalmethode wijzigen
            if ($actionsForm->has('change_payment_method') && $actionsForm->get('change_payment_method')->isClicked()) {
                $commandBus->handle(ChangeOrderPaymentMethodCommand::create($order->getId()));

                $this->addFlash('success', 'De betaalmethode van deze bestelling is hersteld. De klant kan nu zelf vanuit de orderstatus een andere betaalmethode kiezen bij Mollie.');

                return $this->redirectToRoute('app.backend.orders.default.details', [
                    'orderId' => $order->getId(),
                ]);
            }

            // Handmatige controle is afgerond
            if ($actionsForm->has('check_completed') && $actionsForm->get('check_completed')->isClicked()) {
                $commandBus->handle(ManualCheckCompletedCommand::create($order->getId()));

                $this->addFlash('success', 'De "handmatige controle" stap van deze bestelling is afgerond.');

                return $this->redirectToRoute('app.backend.orders.default.details', [
                    'orderId' => $order->getId(),
                ]);
            }

            // Maatwerk aanpassingen zijn doorgevoerd
            if ($actionsForm->has('custom_completed') && $actionsForm->get('custom_completed')->isClicked()) {
                $commandBus->handle(ManualCustomCompletedCommand::create($order->getId()));

                $this->addFlash('success', 'De "maatwerk aanpassingen" stap van deze bestelling is afgerond.');

                return $this->redirectToRoute('app.backend.orders.default.details', [
                    'orderId' => $order->getId(),
                ]);
            }
        }

        $invoice = $order->findInvoice();
        $profile = null;
        if ($invoice) {
            $profile = $invoice->getProfile();
        } elseif ($order->getShop()) {
            $profile = $order->getShop()->getProfile();
        }

        return $this->render(':backend/orders/default:details.html.twig', [
            'order' => $order,
            'document' => $order->getDocument(),
            'template' => $order->getDocument()->getTemplate(),
            'customer' => $order->getCustomer(),
            'invoice' => $invoice,
            'profile' => $profile,
            'transitions' => $workflow->getEnabledTransitions($order),

            'toolbar' => $toolbarForm->createView(),
            'actions' => $actionsForm->createView(),
        ]);
    }

    /**
     * @Route("/orders/{view}/{profile_id}", name="app.backend.orders.default.index", requirements={"view": "completed|manual|overview|pending|warning"}, defaults={ "shop_id": null })
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param string  $view
     * @param Request $request
     * @param Profile $profile
     *
     * @return Response
     */
    public function indexAction(string $view, Request $request, Profile $profile): Response
    {
        $query = $request->query;
        $enabledSearch = false;

        $paginator = $this->get('knp_paginator');

        /** @var OrderQueryBuilderInterface $builder */
        $builder = $this->get(OrderQueryBuilderInterface::class);
        $builder
            ->reset();

        if ($query->get('orderNumber')) {
            $builder->filterOrderNumber($query->get('orderNumber'));
            $enabledSearch = true;
        }

        if ($query->get('template')) {
            $builder->filterTemplate($query->get('template'));
            $enabledSearch = true;
        }

        if ($query->get('fromDate')) {
            $fromDate = \DateTime::createFromFormat('m/d/Y', $query->get('fromDate'));

            if (!$fromDate instanceof \DateTime) {
                throw new HttpException(400);
            }

            $builder->filterFromDate($fromDate);
            $enabledSearch = true;
        }

        if ($query->get('toDate')) {
            $toDate = \DateTime::createFromFormat('m/d/Y', $query->get('toDate'));

            if (!$toDate instanceof \DateTime) {
                throw new HttpException(400);
            }

            $builder->filterToDate($toDate);
            $enabledSearch = true;
        }

        $pageSubtitle = 'Alle bestellingen';

        switch ($view) {
            case self::VIEW_OVERVIEW:
                break;

            case self::VIEW_PENDING:
                $builder->filterStatus()
                    ->includePending();

                $pageSubtitle = 'Bestelling in afwachting';

                break;

            case self::VIEW_WARNING:
                $builder->filterStatus()
                    ->includeWarning();

                $pageSubtitle = 'Bestellingen met foutmeldingen';

                break;

            case self::VIEW_COMPLETED:
                $builder->filterStatus()
                    ->includeCompleted();

                $pageSubtitle = 'Afgeronde bestellingen';

                break;

            case self::VIEW_MANUAL:
                $builder->filterStatus()
                    ->includeManual();

                $pageSubtitle = 'Bestellingen met handmatige acties';

                break;

            default:
                throw $this->createNotFoundException();
                break;
        }

        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 20);

        return $this->render(':backend/orders/default:index.html.twig', [
            'pageSubtitle' => $pageSubtitle,
            'pagination' => $pagination,
            'enabledSearch' => $enabledSearch,
            'searchAttributesBag' => $query,
        ]);
    }
}
