<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Customers;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Domain\Orders\QueryBuilder\CustomerQueryBuilderInterface;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;
use NnShop\Domain\Orders\Value\CustomerId;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    const VIEW_OVERVIEW = 'overview';

    const VIEW_REVENUE = 'revenue';

    /**
     * @Route("/customers/{customerId}", name="app.backend.customers.default.details", requirements={"customerId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function detailsAction(Request $request)
    {
        /** @var CustomerRepositoryInterface $repository */
        $repository = $this->get(CustomerRepositoryInterface::class);

        $customerId = $request->get('customerId');
        if (!CustomerId::valid($customerId)) {
            throw $this->createNotFoundException();
        }

        $customer = $repository->getById(CustomerId::from($customerId));

        return $this->render(':backend/customers/default:details.html.twig', [
            'customer' => $customer,
        ]);
    }

    /**
     * @Route("/customers/{view}", name="app.backend.customers.default.index", requirements={"view": "overview|revenue"})
     *
     * @param string  $view
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(string $view, Request $request)
    {
        $query = $request->query;
        $enabledSearch = false;

        $paginator = $this->get('knp_paginator');

        /** @var CustomerQueryBuilderInterface $builder */
        $builder = $this->get(CustomerQueryBuilderInterface::class);
        $builder->reset();

        if ($query->get('name')) {
            $builder->filterName($query->get('name'));
            $enabledSearch = true;
        }

        if ($query->get('companyName')) {
            $builder->filterCompanyName($query->get('companyName'));
            $enabledSearch = true;
        }

        if ($query->get('email')) {
            $builder->filterEmail($query->get('email'));
            $enabledSearch = true;
        }

        $pageSubtitle = 'Alle klanten';

        switch ($view) {
            case self::VIEW_OVERVIEW:
                break;

            case self::VIEW_REVENUE:
                $builder->orderByWorth();

                $pageSubtitle = 'Big spenders';

                break;

            default:
                throw $this->createNotFoundException();
                break;
        }

        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 20);

        return $this->render(':backend/customers/default:index.html.twig', [
            'pageSubtitle' => $pageSubtitle,
            'pagination' => $pagination,
            'searchAttributesBag' => $query,
            'enabledSearch' => $enabledSearch,
        ]);
    }
}
