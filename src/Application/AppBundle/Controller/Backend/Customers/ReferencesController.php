<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Customers;

use Core\Application\CoreBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReferencesController extends AbstractController
{
    /**
     * @Route("/customers/references", name="app.backend.customers.references.index")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        return $this->render(':backend/dashboard/default:index.html.twig');
    }
}
