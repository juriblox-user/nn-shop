<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Templates;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Application\AppBundle\Forms\Backend\Request\Templates\PublishTemplateRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\Templates\PublishTemplateType;
use NnShop\Domain\Templates\Command\Template\PublishTemplateCommand;
use NnShop\Domain\Templates\Command\Template\UpdateLinkedCategoriesCommand;
use NnShop\Domain\Templates\Command\Template\UpdateTemplateCommand;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use NnShop\Domain\Templates\Value\TemplateId;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PublishController extends AbstractController
{
    /**
     * @Route("/templates/{templateId}/publish", name="app.backend.templates.publish", requirements={"templateId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /** @var TemplateRepositoryInterface $templateRepository */
        $templateRepository = $this->get(TemplateRepositoryInterface::class);

        $templateId = $request->get('templateId');
        if (!TemplateId::valid($templateId)) {
            throw $this->createNotFoundException();
        }

        $template = $templateRepository->findOneById(TemplateId::from($templateId));
        if (null === $template) {
            throw $this->createNotFoundException();
        }

        if ($template->isPublished() || $template->isCorrupted()) {
            return $this->redirectToRoute('app.backend.templates.default.details', [
                'templateId' => $template->getId(),
            ]);
        }

        /*
         * Formulier
         */
        $formRequest = new PublishTemplateRequest($template);

        $form = $this->createNamedForm(null, PublishTemplateType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {
            // Wijzigingen aan het template
            $command = UpdateTemplateCommand::prepare($template->getId());
            $command->setTitle($formRequest->getTitle());

            $command->setUnitPrice($formRequest->getUnitPrice());
            $command->setCheckPrice($formRequest->getCheckPrice());

            $command->setHidden($formRequest->isHidden());

            $command->setService($formRequest->isService());

            if ($formRequest->isSubscription()) {
                $command->setMontlyPrice($formRequest->getMonthlyPrice());
                $command->setSubscription(true);
            } else {
                $command->setSubscription(false);
            }

            $commandBus->handle($command);

            // Gekoppelde categorieën
            $command = UpdateLinkedCategoriesCommand::prepare($template->getId());
            $command->setPrimary($formRequest->getCategories()->getPrimary()->getId());

            foreach ($formRequest->getCategories()->getSelected() as $category) {
                $command->addCategory($category->getId());
            }

            $commandBus->handle($command);

            // Template publiceren
            $commandBus->handle(PublishTemplateCommand::create($template->getId()));

            $this->addFlash('success', 'Het template is succesvol gepubliceerd.');

            return $this->redirectToRoute('app.backend.templates.default.details', [
                'templateId' => $template->getId(),
            ]);
        }

        return $this->render(':backend/templates/publish:index.html.twig', [
            'template' => $template,
            'form' => $form->createView(),
        ]);
    }
}
