<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Templates;

use Core\Application\CoreBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UpdatesController extends AbstractController
{
    /**
     * @Route("/templates/updates", name="app.backend.templates.updates.index")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        return $this->render(':backend/templates/updates:index.html.twig');
    }
}
