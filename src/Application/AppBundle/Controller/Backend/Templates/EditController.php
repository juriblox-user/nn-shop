<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Templates;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Application\AppBundle\Forms\Backend\Request\Templates\EditTemplateRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Templates\LinkCategoriesRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Templates\LinkRelatedTemplatesRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\Templates\EditTemplateType;
use NnShop\Application\AppBundle\Forms\Backend\Type\Templates\LinkCategoriesType;
use NnShop\Application\AppBundle\Forms\Backend\Type\Templates\LinkRelatedTemplatesType;
use NnShop\Domain\Templates\Command\Template\UpdateLinkedCategoriesCommand;
use NnShop\Domain\Templates\Command\Template\UpdateRelatedTemplatesCommand;
use NnShop\Domain\Templates\Command\Template\UpdateTemplateCommand;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use NnShop\Domain\Templates\Value\TemplateId;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EditController extends AbstractController
{
    /**
     * @Route("/templates/{templateId}/edit", name="app.backend.templates.edit", requirements={"templateId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /** @var TemplateRepositoryInterface $templateRepository */
        $templateRepository = $this->get(TemplateRepositoryInterface::class);

        $templateId = $request->get('templateId');
        if (!TemplateId::valid($templateId)) {
            throw $this->createNotFoundException();
        }

        $template = $templateRepository->findOneById(TemplateId::from($templateId));
        if (null === $template) {
            throw $this->createNotFoundException();
        }

        if (!$template->isPublished()) {
            return $this->redirectToRoute('app.backend.templates.default.details', [
                'templateId' => $template->getId(),
            ]);
        }

        /*
         * Formulier
         */
        $formRequest = new EditTemplateRequest($template);

        $form = $this->createNamedForm(null, EditTemplateType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {
            $command = UpdateTemplateCommand::prepare($template->getId());

            $command->setTitle($formRequest->getTitle());
            $command->setDescription($formRequest->getDescription());

            $command->setExcerpt($formRequest->getExcerpt());

            $command->setSpotlight($formRequest->isSpotlight());

            $command->setUnitPrice($formRequest->getUnitPrice());
            $command->setCheckPrice($formRequest->getCheckPrice());
            $command->setRegisteredEmailPrice($formRequest->getRegisteredEmailPrice());

            $command->setHidden($formRequest->isHidden());

            $command->setService($formRequest->isService());

            if ($formRequest->isSubscription()) {
                $command->setMontlyPrice($formRequest->getMonthlyPrice());
                $command->setSubscription(true);
            } else {
                $command->setSubscription(false);
            }

            if ($formRequest->hasSample()) {
                $command->setSample($formRequest->getSample());
            }

            if (null !== $formRequest->getPreview()) {
                $command->setPreview($formRequest->getPreview());
            }

            $commandBus->handle($command);

            $this->addFlash('success', 'De wijzigingen aan het template zijn succesvol doorgevoerd.');

            return $this->redirectToRoute('app.backend.templates.default.details', [
                'templateId' => $template->getId(),
            ]);
        }

        return $this->render(':backend/templates/edit:index.html.twig', [
            'template' => $template,
            'form'     => $form->createView(),
        ]);
    }

    /**
     * @Route("/templates/{templateId}/categories", name="app.backend.templates.edit.categories", requirements={"templateId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function categoriesAction(Request $request)
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /** @var TemplateRepositoryInterface $templateRepository */
        $templateRepository = $this->get(TemplateRepositoryInterface::class);

        $templateId = $request->get('templateId');
        if (!TemplateId::valid($templateId)) {
            throw $this->createNotFoundException();
        }

        $template = $templateRepository->findOneById(TemplateId::from($templateId));
        if (null === $template) {
            throw $this->createNotFoundException();
        }

        if (!$template->isPublished()) {
            return $this->redirectToRoute('app.backend.templates.default.details', [
                'templateId' => $template->getId(),
            ]);
        }

        /*
         * Formulier
         */
        $formRequest = new LinkCategoriesRequest($template);

        $form = $this->createNamedForm(null, LinkCategoriesType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {
            $command = UpdateLinkedCategoriesCommand::prepare($template->getId());
            $command->setPrimary($formRequest->getCategories()->getPrimary()->getId());

            foreach ($formRequest->getCategories()->getSelected() as $category) {
                $command->addCategory($category->getId());
            }

            $commandBus->handle($command);

            $this->addFlash('success', 'De gekoppelde categorieën zijn succesvol gewijzigd.');

            return $this->redirectToRoute('app.backend.templates.default.details', [
                'templateId' => $template->getId(),
            ]);
        }

        return $this->render(':backend/templates/edit:categories.html.twig', [
            'template' => $template,
            'form'     => $form->createView(),
        ]);
    }

    /**
     * @Route("/templates/{templateId}/gerelateerd", name="app.backend.templates.edit.related", requirements={"templateId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @return Response
     */
    public function relatedAction(Request $request)
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /** @var TemplateRepositoryInterface $templateRepository */
        $templateRepository = $this->get(TemplateRepositoryInterface::class);

        $templateId = $request->get('templateId');
        if (!TemplateId::valid($templateId)) {
            throw $this->createNotFoundException();
        }

        $template = $templateRepository->findOneById(TemplateId::from($templateId));
        if (null === $template) {
            throw $this->createNotFoundException();
        }

        if (!$template->isPublished()) {
            return $this->redirectToRoute('app.backend.templates.default.details', [
                'templateId' => $template->getId(),
            ]);
        }

        /*
         * Formulier
         */
        $formRequest = new LinkRelatedTemplatesRequest($template);

        $form = $this->createNamedForm(null, LinkRelatedTemplatesType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {
            $command = UpdateRelatedTemplatesCommand::prepare($template->getId());

            foreach ($formRequest->getRelated()->getSelected() as $related) {
                $command->addRelated($related->getId());
            }

            $commandBus->handle($command);

            $this->addFlash('success', 'De gekoppelde gerelateerde templates zijn succesvol gewijzigd.');

            return $this->redirectToRoute('app.backend.templates.default.details', [
                'templateId' => $template->getId(),
            ]);
        }

        return $this->render(':backend/templates/edit:related.html.twig', [
            'template' => $template,
            'form'     => $form->createView(),
        ]);
    }
}
