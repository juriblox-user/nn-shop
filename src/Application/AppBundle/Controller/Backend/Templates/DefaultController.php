<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Templates;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\Application\CoreBundle\Forms\Type\IconSubmitType;
use NnShop\Domain\Templates\Command\Template\SynchronizeTemplateAsyncCommand;
use NnShop\Domain\Templates\Entity\Partner;
use NnShop\Domain\Templates\QueryBuilder\PartnerQueryBuilderInterface;
use NnShop\Domain\Templates\QueryBuilder\TemplateQueryBuilderInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use NnShop\Domain\Templates\Value\TemplateId;
use NnShop\Domain\Translation\Entity\Profile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    const VIEW_CORRUPT = 'corrupt';

    const VIEW_IMPORTED = 'imported';

    const VIEW_OVERVIEW = 'overview';

    /**
     * @Route("/templates/{templateId}", name="app.backend.templates.default.details", requirements={"templateId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @param Request $request
     *
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return Response
     */
    public function detailsAction(Request $request): Response
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /** @var TemplateRepositoryInterface $repository */
        $repository = $this->get(TemplateRepositoryInterface::class);

        $templateId = $request->get('templateId');
        if (!TemplateId::valid($templateId)) {
            throw $this->createNotFoundException();
        }

        $template = $repository->findOneById(TemplateId::from($templateId));
        if (null === $template) {
            throw $this->createNotFoundException();
        }

        /*
         * Toolbar
         */
        $toolbar = $this->createFormBuilder()
            ->add('synchronize', IconSubmitType::class, [
                'icon' => 'icon icon-cycle',
                'label' => 'Synchroniseren',
                'translation_domain' => false,
            ])->getForm();

        $toolbar->handleRequest($request);

        if ($toolbar->isSubmitted() && $toolbar->isValid() && $toolbar->get('synchronize')->isClicked()) {
            $commandBus->handle(SynchronizeTemplateAsyncCommand::create($template->getId()));

            $this->addFlash('success', 'Het template wordt op de achtergrond gesynchroniseerd.');
        }

        return $this->render(':backend/templates/default:details.html.twig', [
            'template' => $template,
            'partner' => $template->getPartner(),
            'categories' => $repository->findCategories($template),

            'toolbar' => $toolbar->createView(),
        ]);
    }

    /**
     * @Route("/templates/{view}/partner/{partner_id}", name="app.backend.templates.default.index", requirements={"view": "corrupt|imported|overview"})
     * @ParamConverter("partner", options={"mapping": {"partner_id": "id"}})
     *
     * @param string       $view
     * @param Request      $request
     * @param Partner|null $partner
     *
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return Response
     */
    public function indexAction(string $view, Request $request, Partner $partner = null): Response
    {
        $paginator = $this->get('knp_paginator');

        /** @var TemplateQueryBuilderInterface $builder */
        $builder = $this->get(TemplateQueryBuilderInterface::class);
        $builder->reset();

        $pageSubtitle = 'Alle templates';

        switch ($view) {
            case self::VIEW_OVERVIEW:
                break;

            case self::VIEW_CORRUPT:
                $builder->filterCorrupted()
                    ->includeCorrupted();

                $pageSubtitle = 'Corrupte templates';

                break;

            case self::VIEW_IMPORTED:
                $builder->filterPublished()
                    ->includeUnpublished();

                $pageSubtitle = 'Geïmporteerde templates';

                break;

            default:
                throw $this->createNotFoundException();
                break;
        }

        /*
         * If partner isn't passed (for example if in indexByProfileAction selected profile has no partner), then pagination is set to null as there's nothing to list
         */
        if ($partner) {
            $builder->filterPartner($partner->getId());

            $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 20);
        } else {
            $pagination = null;
        }

        return $this->render(':backend/templates/default:index.html.twig', [
            'pageSubtitle' => $pageSubtitle,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/templates/{view}/{profile_id}", name="app.backend.templates.default.index_by_profile", requirements={"view": "corrupt|imported|overview"})
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param string  $view
     * @param Request $request
     * @param Profile $profile
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     *
     * @return Response
     */
    public function indexByProfileAction(string $view, Request $request, Profile $profile): Response
    {
        $builder = $this->get(PartnerQueryBuilderInterface::class);

        $builder->reset();

        $partners = $builder->build()->getResult();

        return $this->indexAction($view, $request, \count($partners) > 0 ? $partners[0] : null);
    }
}
