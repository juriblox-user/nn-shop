<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Subscriptions;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Domain\Orders\QueryBuilder\SubscriptionQueryBuilderInterface;
use NnShop\Domain\Orders\Repository\SubscriptionRepositoryInterface;
use NnShop\Domain\Orders\Value\SubscriptionId;
use NnShop\Domain\Translation\Entity\Profile;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    /**
     * Overzicht.
     */
    const VIEW_OVERVIEW = 'overview';

    /**
     * Verlopen.
     */
    const VIEW_EXPIRED = 'expired';

    /**
     * Afkoelperiode.
     */
    const VIEW_GRACE_PERIOD = 'graceperiod';

    /**
     * @Route("/subscriptions/{subscriptionId}", name="app.backend.subscriptions.default.details", requirements={"subscriptionId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @param Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return Response
     */
    public function detailsAction(Request $request): Response
    {
        /** @var SubscriptionRepositoryInterface $repository */
        $repository = $this->get(SubscriptionRepositoryInterface::class);

        $subscriptionId = $request->get('subscriptionId');
        if (!SubscriptionId::valid($subscriptionId)) {
            throw $this->createNotFoundException();
        }

        $subscription = $repository->getById(SubscriptionId::from($subscriptionId));

        return $this->render(':backend/subscriptions/default:details.html.twig', [
            'subscription' => $subscription,

            'order' => $subscription->getOrder(),
            'customer' => $subscription->getCustomer(),
            'template' => $subscription->getTemplate(),
        ]);
    }

    /**
     * @Route("/subscriptions/{view}/{profile_id}", name="app.backend.subscriptions.default.index", requirements={"view": "overview|expired|graceperiod"})
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param string  $view
     * @param Request $request
     * @param Profile $profile
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     *
     * @return Response
     */
    public function indexAction(string $view, Request $request, Profile $profile): Response
    {
        $paginator = $this->get('knp_paginator');

        /** @var SubscriptionQueryBuilderInterface $builder */
        $builder = $this->get(SubscriptionQueryBuilderInterface::class);
        $builder->reset();

        $pageSubtitle = 'Alle abonnementen';

        switch ($view) {
            case self::VIEW_OVERVIEW:
                break;

            case self::VIEW_EXPIRED:
                $builder->filterExpired()
                    ->includeExpired();

                break;

            case self::VIEW_GRACE_PERIOD:
                $builder->filterExpired()
                    ->includeExpired()
                    ->filterGracePeriod();

                break;

            default:
                throw $this->createNotFoundException();
                break;
        }

        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 20);

        return $this->render(':backend/subscriptions/default:index.html.twig', [
            'pageSubtitle' => $pageSubtitle,
            'pagination' => $pagination,
        ]);
    }
}
