<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Shared\Helper;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Domain\Shops\Entity\Shop;
use NnShop\Domain\Shops\Repository\ShopRepositoryInterface;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BackendShopSelectorController.
 */
class BackendShopSelectorController extends AbstractController
{
    /**
     * @param string  $route
     * @param array   $route_params
     * @param bool    $disable_locale
     * @param Request $request
     *
     * @throws \LogicException
     *
     * @return Response
     */
    public function menuAction(string $route, array $route_params, bool $disable_locale = false, Request $request): Response
    {
        $profileRepository = $this->get(ProfileRepositoryInterface::class);
        $shopRepository = $this->get(ShopRepositoryInterface::class);

        $profiles = $profileRepository->getMenu();

        if (!isset($route_params['shop_id'])) {
            $shops = $shopRepository->getMenu();
            $route_params['shop_id'] = $shops[0]->getId();
            $shop = $shops[0];
        } else {
            $shop = $shopRepository->findOneBy(['id' => $route_params['shop_id']]);
            $shops = $shopRepository->getMenuByProfile($shop->getProfile());
        }

        /**
         * Chosen shop set in session so it can be used in menu.
         */
        $session = $request->getSession();
        $session->set('shop_id', $route_params['shop_id']);

        return $this->render('shared/_helpers/backend-shop-selector.html.twig', [
            'shops' => $shops,
            'route' => $route,
            'route_params' => $route_params,
            'disable_locale' => $disable_locale,
            'profiles' => $profiles,
            'shop' => $shop,
        ]);
    }

    /**
     * @param string $route
     * @param array  $route_params
     * @param Shop   $shop
     * @param bool   $disable_locale
     *
     * @throws \LogicException
     *
     * @return Response
     */
    public function itemAction(string $route, array $route_params, Shop $shop, bool $disable_locale = false): Response
    {
        $shops = $this->get(ShopRepositoryInterface::class)->getMenuByProfile($shop->getProfile());
        $profiles = $this->get(ProfileRepositoryInterface::class)->getMenu();

        return $this->render('shared/_helpers/backend-item-shop-selector.html.twig', [
            'shops' => $shops,
            'route' => $route,
            'route_params' => $route_params,
            'current_shop' => $shop,
            'disable_locale' => $disable_locale,
            'profiles' => $profiles,
        ]);
    }
}
