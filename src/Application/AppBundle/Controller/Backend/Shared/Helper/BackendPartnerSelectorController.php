<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Shared\Helper;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Domain\Templates\Entity\Partner;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Templates\Repository\PartnerRepositoryInterface;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BackendPartnerSelectorController.
 */
class BackendPartnerSelectorController extends AbstractController
{
    /**
     * @param string  $route
     * @param array   $route_params
     * @param Request $request
     *
     * @return Response
     */
    public function menuAction(string $route, array $route_params, Request $request): Response
    {
        $profileRepository = $this->get(ProfileRepositoryInterface::class);
        $partnerRepository = $this->get(PartnerRepositoryInterface::class);

        if (isset($route_params['partner_id'])) {
            $selectedPartner = $partnerRepository
                ->findOneBy(['id' => $route_params['partner_id']]);

            if (!$selectedPartner) {
                throw new \RuntimeException(sprintf("Partner can't be found for partner_id %s!", $route_params['partner_id']));
            }

            $selectedProfile = $selectedPartner->getCountryProfile();
        } else {
            $selectedProfile = $profileRepository
                ->findOneBy(['id' => $route_params['profile_id']]);
            /** @var Partner[] $partners */
            $partners = $partnerRepository->getMenuByProfile($selectedProfile);

            if (\count($partners) > 0) {
                $selectedPartner = $partners[0];
                $route_params['partner_id'] = $selectedPartner->getId();
            } else {
                $selectedPartner = null;
            }
        }

        /**
         * Set slugs to session if they exist.
         */
        $session = $request->getSession();

        if (!$session) {
            throw new \RuntimeException('No Session set!');
        }

        if (isset($route_params['partner_id'])) {
            $session->set('partner_id', $route_params['partner_id']);
        }

        if (isset($route_params['profile_id'])) {
            $session->set('profile_id', $route_params['profile_id']);
        }

        $partners = $partnerRepository->getMenuByProfile($selectedProfile);
        $profiles = $profileRepository->getMenu();

        return $this->render('shared/_helpers/backend-partner-selector.html.twig', [
            'partners' => $partners,
            'profiles' => $profiles,
            'route' => $route,
            'route_params' => $route_params,
            'selected_partner' => $selectedPartner,
            'selected_profile' => $selectedProfile,
        ]);
    }

    /**
     * @param string   $route
     * @param array    $route_params
     * @param Template $template
     *
     * @return Response
     */
    public function itemAction(string $route, array $route_params, Template $template): Response
    {
        $partners = $this->get(PartnerRepositoryInterface::class)->getMenuByProfile($template->getPartner()->getCountryProfile());
        $profiles = $this->get(ProfileRepositoryInterface::class)->getMenu();

        return $this->render('shared/_helpers/backend-item-partner-selector.html.twig', [
            'partners' => $partners,
            'profiles' => $profiles,
            'route' => $route,
            'route_params' => $route_params,
            'selected_template' => $template,
        ]);
    }
}
