<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Shared\Helper;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Domain\Translation\Entity\Profile;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BackendProfileSelectorController.
 */
class BackendProfileSelectorController extends AbstractController
{
    /**
     * @param string  $route
     * @param array   $route_params
     * @param Request $request
     * @param bool    $disable_locale
     *
     * @throws \Doctrine\ORM\ORMException
     *
     * @return Response
     */
    public function menuAction(string $route, array $route_params, Request $request, bool $disable_locale = false): Response
    {
        /** @var Profile[] $profiles */
        $profiles = $this->get(ProfileRepositoryInterface::class)->getMenu();

        if (!isset($route_params['profile_id'])) {
            $route_params['profile_id'] = $profiles[0]->getId();
        }

        /**
         * Chosen profile set in session so it can be used in menu.
         */
        $session = $request->getSession();

        if (!$session) {
            throw new \RuntimeException("Session isn't set!");
        }

        $session->set('profile_id', $route_params['profile_id']);

        return $this->render('shared/_helpers/backend-profile-selector.html.twig', [
            'profiles' => $profiles,
            'route' => $route,
            'route_params' => $route_params,
            'disable_locale' => $disable_locale,
        ]);
    }

    /**
     * @param string  $route
     * @param array   $route_params
     * @param Profile $profile
     * @param bool    $disable_locale
     *
     * @throws \Doctrine\ORM\ORMException
     *
     * @return Response
     */
    public function itemAction(string $route, array $route_params, Profile $profile, bool $disable_locale = false): Response
    {
        $profiles = $this->get(ProfileRepositoryInterface::class)->getMenu();

        return $this->render('shared/_helpers/backend-item-profile-selector.html.twig', [
            'profiles' => $profiles,
            'route' => $route,
            'route_params' => $route_params,
            'current_profile' => $profile,
            'disable_locale' => $disable_locale,
        ]);
    }
}
