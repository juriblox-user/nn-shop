<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Shared\Helper;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Domain\Templates\Entity\CompanyActivity;
use NnShop\Domain\Templates\Repository\CompanyTypeRepositoryInterface;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BackendCompanyTypeSelectorController.
 */
class BackendCompanyTypeSelectorController extends AbstractController
{
    /**
     * @param string  $route
     * @param array   $route_params
     * @param Request $request
     *
     * @return Response
     */
    public function menuAction(string $route, array $route_params, Request $request): Response
    {
        $companyTypeRepository = $this->get(CompanyTypeRepositoryInterface::class);
        $profileRepository = $this->get(ProfileRepositoryInterface::class);

        $selectedType = $companyTypeRepository
            ->find($route_params['typeId']);

        $types = $companyTypeRepository->getMenuByProfile($selectedType->getProfile());
        $profiles = $profileRepository->getMenu();

        /**
         * Chosen shop set in session so it can be used in menu.
         */
        $session = $request->getSession();
        $session->set('typeId', $route_params['typeId']);

        return $this->render('shared/_helpers/backend-company-type-selector.html.twig', [
            'types' => $types,
            'profiles' => $profiles,
            'route' => $route,
            'route_params' => $route_params,
            'selected_type' => $selectedType,
        ]);
    }

    /**
     * @param string          $route
     * @param array           $route_params
     * @param CompanyActivity $activity
     *
     * @return Response
     */
    public function itemAction(string $route, array $route_params, CompanyActivity $activity): Response
    {
        $types = $this->get(CompanyTypeRepositoryInterface::class)->getMenuByProfile($activity->getType()->getProfile());
        $profiles = $this->get(ProfileRepositoryInterface::class)->getMenu();

        return $this->render('shared/_helpers/backend-item-company-type-selector.html.twig', [
            'types' => $types,
            'profiles' => $profiles,
            'route' => $route,
            'route_params' => $route_params,
            'current_activity' => $activity,
        ]);
    }
}
