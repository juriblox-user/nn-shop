<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Settings;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\CreateReferrerRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\EditReferrerRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\Settings\CreateReferrerType;
use NnShop\Application\AppBundle\Forms\Backend\Type\Settings\EditReferrerType;
use NnShop\Domain\Shops\Command\Referrer\CreateReferrerCommand;
use NnShop\Domain\Shops\Command\Referrer\UpdateReferrerCommand;
use NnShop\Domain\Shops\Entity\Referrer;
use NnShop\Domain\Shops\QueryBuilder\ReferrerQueryBuilderInterface;
use NnShop\Domain\Shops\Repository\ReferrerRepositoryInterface;
use NnShop\Domain\Shops\Value\ReferrerId;
use NnShop\Domain\Translation\Entity\Profile;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReferrersController extends AbstractController
{
    /**
     * @Route("/settings/partners/{profile_id}", name="app.backend.settings.referrers.index")
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @throws \LogicException
     *
     * @return Response
     */
    public function indexAction(Request $request, Profile $profile): Response
    {
        $paginator = $this->get('knp_paginator');

        /** @var ReferrerQueryBuilderInterface $builder */
        $builder = $this->get(ReferrerQueryBuilderInterface::class);
        $builder->reset();

        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 20);

        return $this->render(':backend/settings/referrers:index.html.twig', [
            'pagination' => $pagination,
            'profile' => $profile,
        ]);
    }

    /**
     * @Route("/settings/partners/add/{profile_id}", name="app.backend.settings.referrers.create")
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @throws \LogicException
     *
     * @return Response
     */
    public function createAction(Request $request, Profile $profile): Response
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /*
         * Formulier
         */
        $formRequest = new CreateReferrerRequest($profile);

        $form = $this->createNamedForm(null, CreateReferrerType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {
            $command = CreateReferrerCommand::prepare(ReferrerId::generate(), $profile->getId());

            $command->setTitle($formRequest->getTitle());
            $command->setText($formRequest->getText());
            $command->setWebsite($formRequest->getWebsite());

            $command->setHomepage($formRequest->hasHomepage());
            $command->setKickback($formRequest->hasKickback());
            $command->setLanding($formRequest->hasLanding());

            // Gekoppelde kortingscode
            if (null !== $formRequest->getDiscount()) {
                $command->linkDiscount($formRequest->getDiscount()->getId());
            }

            // Gekoppelde templates
            foreach ($formRequest->getTemplates() as $template) {
                $command->addTemplate($template->getId());
            }

            // Logo instellen
            if (null !== $formRequest->getLogo()) {
                $command->setLogo($formRequest->getLogo());
            }

            $commandBus->handle($command);

            $this->addFlash('success', 'De partner is succesvol toegevoegd.');

            return $this->redirectToRoute('app.backend.settings.referrers.index', ['profile_id' => $profile->getId()]);
        }

        return $this->render(':backend/settings/referrers:create.html.twig', [
            'form' => $form->createView(),
            'profile' => $profile,
        ]);
    }

    /**
     * @Route("/settings/partners/{referrerId}/edit", name="app.backend.settings.referrers.edit", requirements={"referrerId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @param Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     *
     * @return Response
     */
    public function editAction(Request $request): Response
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /** @var ReferrerRepositoryInterface $referrerRepository */
        $referrerRepository = $this->get(ReferrerRepositoryInterface::class);

        $referrerId = $request->get('referrerId');
        if (!ReferrerId::valid($referrerId)) {
            throw $this->createNotFoundException();
        }

        /** @var Referrer|null $referrer */
        $referrer = $referrerRepository->findOneById(ReferrerId::from($referrerId));
        if (null === $referrer) {
            throw $this->createNotFoundException();
        }

        /*
         * Formulier
         */
        $formRequest = new EditReferrerRequest($referrer);

        $form = $this->createNamedForm(null, EditReferrerType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {
            $command = UpdateReferrerCommand::prepare($referrer);

            $command->setTitle($formRequest->getTitle());
            $command->setText($formRequest->getText());
            $command->setWebsite($formRequest->getWebsite());

            $command->setHomepage($formRequest->hasHomepage());
            $command->setKickback($formRequest->hasKickback());
            $command->setLanding($formRequest->hasLanding());

            // Gekoppelde kortingscode
            if (null !== $formRequest->getDiscount()) {
                $command->linkDiscount($formRequest->getDiscount()->getId());
            } else {
                $command->unlinkDiscount();
            }

            // Gekoppelde templates
            foreach ($formRequest->getTemplates() as $template) {
                $command->addTemplate($template->getId());
            }

            // Logo instellen
            if ($formRequest->deleteLogo()) {
                $command->setDeleteLogo(true);
            } elseif (null !== $formRequest->getLogo()) {
                $command->setLogo($formRequest->getLogo());
            }

            $commandBus->handle($command);

            $this->addFlash('success', 'De gegevens van de partner zijn succesvol gewijzigd.');

            return $this->redirectToRoute('app.backend.settings.referrers.index', ['profile_id' => $referrer->getProfile()->getId()]);
        }

        return $this->render(':backend/settings/referrers:edit.html.twig', [
            'referrer' => $referrer,
            'form' => $form->createView(),
        ]);
    }
}
