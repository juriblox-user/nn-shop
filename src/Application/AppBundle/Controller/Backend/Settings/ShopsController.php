<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Settings;

use Core\Application\CoreBundle\Controller\AbstractController;
use Core\Application\CoreBundle\Service\Bridge\Bridge;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\CreateShopRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\EditShopRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\Settings\CreateShopType;
use NnShop\Application\AppBundle\Forms\Backend\Type\Settings\EditShopType;
use NnShop\Domain\Shops\Command\Shop\CreateShopCommand;
use NnShop\Domain\Shops\Command\Shop\UpdateShopCommand;
use NnShop\Domain\Shops\QueryBuilder\ShopQueryBuilderInterface;
use NnShop\Domain\Shops\Repository\ShopRepositoryInterface;
use NnShop\Domain\Shops\Value\ShopId;
use NnShop\Domain\Translation\Entity\Profile;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ShopsController extends AbstractController
{
    /**
     * @Route("/settings/shops/{profile_id}", name="app.backend.settings.shops.index")
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @throws \LogicException
     *
     * @return Response
     */
    public function indexAction(Request $request, Profile $profile): Response
    {
        $paginator = $this->get('knp_paginator');

        /** @var ShopQueryBuilderInterface $builder */
        $builder = $this->get(ShopQueryBuilderInterface::class);
        $builder->reset();

        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 20);

        return $this->render(':backend/settings/shops:index.html.twig', [
            'pagination' => $pagination,
            'profile' => $profile,
        ]);
    }

    /**
     * @Route("/settings/shops/add/{profile_id}", name="app.backend.settings.shops.create")
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @throws \LogicException
     *
     * @return Response
     */
    public function createAction(Request $request, Profile $profile): Response
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /*
         * Formulier
         */
        $formRequest = new CreateShopRequest();

        $form = $this->createNamedForm(null, CreateShopType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {
            $command = CreateShopCommand::prepare(ShopId::generate(), $profile->getId());

            $command->setTitle($formRequest->getTitle());
            $command->setHostname($formRequest->getHostname());
            $command->setMetaDescription($formRequest->getMetaDescription());
            $command->setDescription($formRequest->getDescription());

            $commandBus->handle($command);

            $this->addFlash('success', 'De shop is succesvol toegevoegd.');

            return $this->redirectToRoute('app.backend.settings.shops.index',
                ['profile_id' => $profile->getId()]);
        }

        return $this->render(':backend/settings/shops:create.html.twig', [
            'form' => $form->createView(),
            'profile' => $profile,
        ]);
    }

    /**
     * @Route("/settings/shops/{shopId}/edit", name="app.backend.settings.shops.edit", requirements={"shopId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @param Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     *
     * @return Response
     */
    public function editAction(Request $request): Response
    {
        $bridge = $this->get(Bridge::class);

        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /** @var ShopRepositoryInterface $shopRepository */
        $shopRepository = $this->get(ShopRepositoryInterface::class);

        $shopId = $request->get('shopId');
        if (!ShopId::valid($shopId)) {
            throw $this->createNotFoundException();
        }

        $shop = $shopRepository->findOneById(ShopId::from($shopId));
        if (null === $shop) {
            throw $this->createNotFoundException();
        }

        /*
         * Formulier
         */
        $formRequest = new EditShopRequest($shop);

        $form = $this->createNamedForm(null, EditShopType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {
            $command = UpdateShopCommand::prepare($shop->getId());
            $command->setEnabled($formRequest->isEnabled());

            $command->setTitle($formRequest->getTitle());
            $command->setHostname($formRequest->getHostname());
            $command->setMetaDescription($formRequest->getMetaDescription());
            $command->setDescription($formRequest->getDescription());

            $commandBus->handle($command);

            $this->addFlash('success', 'De gegevens van de shop zijn succesvol gewijzigd.');

            return $this->redirectToRoute('app.backend.settings.shops.index', ['profile_id' => $shop->getProfile()->getId()]);
        }

        $record = dns_get_record($shop->getHostname(), DNS_CNAME);
        if ($record) {
            $target = ($record['target'] ?? $record[0]['target']);
        } else {
            $target = null;
        }

        $bridge->setVariable('hostname', $shop->getHostname());

        return $this->render(':backend/settings/shops:edit.html.twig', [
            'shop' => $shop,
            'form' => $form->createView(),
        ]);
    }
}
