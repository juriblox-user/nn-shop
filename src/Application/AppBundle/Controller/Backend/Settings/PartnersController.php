<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Settings;

use Core\Application\CoreBundle\Controller\AbstractController;
use JuriBlox\Sdk\Domain\Offices\Values\OfficeId as RemoteId;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\CreatePartnerRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\EditPartnerRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\Settings\CreatePartnerType;
use NnShop\Application\AppBundle\Forms\Backend\Type\Settings\EditPartnerType;
use NnShop\Domain\Templates\Command\Partner\CreatePartnerCommand;
use NnShop\Domain\Templates\Command\Partner\SynchronizePartnerAsyncCommand;
use NnShop\Domain\Templates\Command\Partner\UpdatePartnerCommand;
use NnShop\Domain\Templates\QueryBuilder\PartnerQueryBuilderInterface;
use NnShop\Domain\Templates\Repository\PartnerRepositoryInterface;
use NnShop\Domain\Templates\Value\PartnerId;
use NnShop\Domain\Translation\Entity\Profile;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PartnersController extends AbstractController
{
    /**
     * @Route("/settings/knowledgepartners/{profile_id}", name="app.backend.settings.partners.index")
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @throws \LogicException
     *
     * @return Response
     */
    public function indexAction(Request $request, Profile $profile): Response
    {
        $paginator = $this->get('knp_paginator');

        /** @var PartnerQueryBuilderInterface $builder */
        $builder = $this->get(PartnerQueryBuilderInterface::class);
        $builder->reset();

        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 20);

        return $this->render(':backend/settings/partners:index.html.twig', [
            'pagination' => $pagination,
            'profile' => $profile,
        ]);
    }

    /**
     * @Route("/settings/knowledgepartners/add/{profile_id}", name="app.backend.settings.partners.create")
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @throws \LogicException
     *
     * @return Response
     */
    public function createAction(Request $request, Profile $profile): Response
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /*
         * Formulier
         */
        $formRequest = new CreatePartnerRequest();

        $form = $this->createNamedForm(null, CreatePartnerType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {
            $command = CreatePartnerCommand::prepare(PartnerId::generate(), new RemoteId($formRequest->getRemoteId()), $profile->getId());

            $command->setTitle($formRequest->getTitle());
            $command->setExcerpt($formRequest->getExcerpt());
            $command->setProfile($formRequest->getProfile());

            if ($formRequest->isSyncEnabled()) {
                $command->enableSync($formRequest->getSyncClientId(), $formRequest->getSyncClientKey());
            }

            if (null !== $formRequest->getLogo()) {
                $command->setLogo($formRequest->getLogo());
            }

            $commandBus->handle($command);

            if ($formRequest->isSyncEnabled()) {
                $commandBus->handle(SynchronizePartnerAsyncCommand::create($command->getId()));

                $this->addFlash('success', 'De kennispartner is succesvol toegevoegd en de templates worden nu op de achtergrond gesynchroniseerd.');
            } else {
                $this->addFlash('success', 'De kennispartner is succesvol toegevoegd.');
            }

            return $this->redirectToRoute('app.backend.settings.partners.index', [
                'profile_id' => $profile->getId(),
            ]);
        }

        return $this->render(':backend/settings/partners:create.html.twig', [
            'form' => $form->createView(),
            'profile' => $profile,
        ]);
    }

    /**
     * @Route("/settings/knowledgepartners/{partnerId}/edit", name="app.backend.settings.partners.edit", requirements={"partnerId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @param Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     *
     * @return Response
     */
    public function editAction(Request $request): Response
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /** @var PartnerRepositoryInterface $partnerRepository */
        $partnerRepository = $this->get(PartnerRepositoryInterface::class);

        $partnerId = $request->get('partnerId');
        if (!PartnerId::valid($partnerId)) {
            throw $this->createNotFoundException();
        }

        $partner = $partnerRepository->findOneById(PartnerId::from($partnerId));
        if (null === $partner) {
            throw $this->createNotFoundException();
        }

        /*
         * Formulier
         */
        $formRequest = new EditPartnerRequest($partner);

        $form = $this->createNamedForm(null, EditPartnerType::class, $formRequest);
        $form->handleRequest($request);

        $formRemoteId = $form->getData()->getRemoteId();
        $isDuplicate = $partnerRepository->remoteIdIsDuplicate($formRemoteId, $partner->getId()->getString());
        if ($isDuplicate) {
            $form->addError(new FormError('Juriblox-ID komt al een keer voor!'));
        }

        $formRequest->bind($form);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {
            $command = UpdatePartnerCommand::prepare($partner->getId());

            $command->setTitle($formRequest->getTitle());
            $command->setExcerpt($formRequest->getExcerpt());
            $command->setProfile($formRequest->getProfile());

            $command->setRemoteId(new RemoteId($formRequest->getRemoteId()));

            $forceSync = false;

            // Synchronisatie inschakelen (en eventueel forceren)
            if ($formRequest->isSyncEnabled()) {
                $command->enableSync($formRequest->getSyncClientId(), $formRequest->getSyncClientKey());

                if (!$partner->isSyncEnabled()) {
                    $forceSync = true;
                }
            } else {
                $command->disableSync();
            }

            // Logo instellen
            if ($formRequest->deleteLogo()) {
                $command->setDeleteLogo(true);
            } elseif (null !== $formRequest->getLogo()) {
                $command->setLogo($formRequest->getLogo());
            }

            $commandBus->handle($command);

            if ($forceSync) {
                $commandBus->handle(SynchronizePartnerAsyncCommand::create($partner->getId()));

                $this->addFlash('success', 'De gegevens van de kennispartner zijn succesvol gewijzigd. De templates worden nu op de achtergrond gesynchroniseerd.');
            } else {
                $this->addFlash('success', 'De gegevens van de kennispartner zijn succesvol gewijzigd.');
            }

            return $this->redirectToRoute('app.backend.settings.partners.index', ['profile_id' => $partner->getCountryProfile()->getId(),
            ]);
        }

        return $this->render(':backend/settings/partners:edit.html.twig', [
            'partner' => $partner,
            'form' => $form->createView(),
        ]);
    }
}
