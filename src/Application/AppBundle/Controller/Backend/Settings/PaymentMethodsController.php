<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Settings;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\EditPaymentMethodRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\Settings\EditPaymentMethodType;
use NnShop\Application\AppBundle\Services\Payment\MollieGateway;
use NnShop\Domain\Translation\Command\AllowedPaymentMethod\UpdateAllowedPaymentMethodCommand;
use NnShop\Domain\Translation\Entity\Profile;
use Mollie\Api\Exceptions\ApiException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PaymentMethodsController extends AbstractController
{
    /**
     * @Route("/settings/paymentmethods/{profile_id}", name="app.backend.settings.payment_methods.index")
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @throws \Mollie\Api\Exceptions\ApiException
     *
     * @return Response
     */
    public function methodsIndexAction(Request $request, Profile $profile): Response
    {
        $mollieGateway = $this->get(MollieGateway::class);

        return $this->render(':backend/settings/payment_methods:index.html.twig', [
            'methods' => $mollieGateway->getAllowedMethods($profile),
            'profile' => $profile,
        ]);
    }

    /**
     * @Route("/settings/paymentmethods/{profile_id}/{methodId}", name="app.backend.settings.payment_methods.edit")
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @throws ApiException
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAllowedMethodAction(Request $request, Profile $profile)
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        $mollieGateway = $this->get(MollieGateway::class);

        $method = $mollieGateway->findMollieMethodById($profile, $request->get('methodId'));

        if (!$method) {
            throw $this->createNotFoundException();
        }

        /*
         * Formulier
         */
        $formRequest = new EditPaymentMethodRequest($method);

        $form = $this->createNamedForm(null, EditPaymentMethodType::class, $formRequest);

        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {
            $command = new UpdateAllowedPaymentMethodCommand();

            $command->setProfileId($profile->getId());
            $command->setEnabled($formRequest->isEnabled());
            $command->setExternallyDisabled($formRequest->isExternallyDisabled());
            $command->setMethod($formRequest->getMethod());
            $command->setMethodObject($formRequest->getMethodObject());

            $commandBus->handle($command);

            $this->addFlash('success', 'De gegevens van de betalingsmethoden zijn succesvol gewijzigd.');

            return $this->redirectToRoute('app.backend.settings.payment_methods.index', ['profile_id' => $profile->getId()]);
        }

        return $this->render(':backend/settings/payment_methods:edit.html.twig', [
            'method' => $method,
            'form' => $form->createView(),
        ]);
    }
}
