<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Settings;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\CreateCategoryRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\EditCategoryRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\Settings\CreateCategoryType;
use NnShop\Application\AppBundle\Forms\Backend\Type\Settings\EditCategoryType;
use NnShop\Domain\Shops\Entity\Shop;
use NnShop\Domain\Shops\Repository\ShopRepositoryInterface;
use NnShop\Domain\Templates\Command\Category\CreateCategoryCommand;
use NnShop\Domain\Templates\Command\Category\UpdateCategoryCommand;
use NnShop\Domain\Templates\QueryBuilder\CategoryQueryBuilderInterface;
use NnShop\Domain\Templates\Repository\CategoryRepositoryInterface;
use NnShop\Domain\Templates\Value\CategoryId;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoriesController extends AbstractController
{
    /**
     * @Route("/settings/categories/{shop_id}", defaults={ "shop_id": null }, name="app.backend.settings.categories.index")
     *
     * @param Request     $request
     * @param string|null $shop_id
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     *
     * @return Response
     */
    public function indexAction(Request $request, string $shop_id = null): Response
    {
        $shopRepository = $this->get(ShopRepositoryInterface::class);

        /*
         * Get shop from shop_id, otherwise just get the first one from the menu
         */
        if ($shop_id) {
            if (!($shop = $shopRepository->find($shop_id))) {
                throw new NotFoundHttpException(sprintf("Can't find shop for id %s", $shop_id));
            }
        } else {
            $shops = $shopRepository->getMenu();
            $shop = $shops[0];
        }

        $paginator = $this->get('knp_paginator');

        /** @var CategoryQueryBuilderInterface $builder */
        $builder = $this->get(CategoryQueryBuilderInterface::class);
        $builder->reset()
            ->filterShop($shop->getId());

        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 20);

        return $this->render(':backend/settings/categories:index.html.twig', [
            'pagination' => $pagination,
            'shop' => $shop,
        ]);
    }

    /**
     * @Route("/settings/categories/add/{shop_id}", name="app.backend.settings.categories.create")
     * @ParamConverter("shop", options={"mapping": {"shop_id": "id"}})
     *
     * @param Request $request
     * @param Shop    $shop
     *
     * @throws \LogicException
     *
     * @return Response
     */
    public function createAction(Request $request, Shop $shop): Response
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /*
         * Formulier
         */
        $formRequest = new CreateCategoryRequest();
        $formRequest->setShop($shop);

        $form = $this->createNamedForm(null, CreateCategoryType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {
            $commandBus->handle(CreateCategoryCommand::create(CategoryId::generate(), $formRequest->getTitle(), $formRequest->getShop(), $formRequest->getDescription(), $formRequest->getMetaDescription()));

            $this->addFlash('success', 'De categorie is succesvol toegevoegd aan de shop.');

            return $this->redirectToRoute('app.backend.settings.categories.index', ['shop_id' => $shop->getId()]);
        }

        return $this->render(':backend/settings/categories:create.html.twig', [
            'form' => $form->createView(),
            'category' => $formRequest,
        ]);
    }

    /**
     * @Route("/settings/categories/{categoryId}/edit", name="app.backend.settings.categories.edit", requirements={"categoryId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @param Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     *
     * @return Response
     */
    public function editAction(Request $request): Response
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /** @var CategoryRepositoryInterface $categoryRepository */
        $categoryRepository = $this->get(CategoryRepositoryInterface::class);

        $categoryId = $request->get('categoryId');
        if (!CategoryId::valid($categoryId)) {
            throw $this->createNotFoundException();
        }

        $category = $categoryRepository->findOneById(CategoryId::from($categoryId));
        if (null === $category) {
            throw $this->createNotFoundException();
        }

        /*
         * Formulier
         */
        $formRequest = new EditCategoryRequest($category);

        $form = $this->createNamedForm(null, EditCategoryType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {
            $command = UpdateCategoryCommand::prepare($category->getId());

            $command->setTitle($formRequest->getTitle());
            $command->setDescription($formRequest->getDescription());
            $command->setMetaDescription($formRequest->getMetaDescription());
            $command->move($formRequest->getShop()->getId());

            $commandBus->handle($command);

            $this->addFlash('success', 'De gegevens van de categorie zijn succesvol gewijzigd.');

            return $this->redirectToRoute('app.backend.settings.categories.index', ['shop_id' => $category->getShop()->getId()]);
        }

        return $this->render(':backend/settings/categories:edit.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }
}
