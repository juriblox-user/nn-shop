<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Settings;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\CreateCompanyActivityRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\CreateCompanyTypeRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\EditCompanyActivityRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\EditCompanyTypeRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\Settings\CreateCompanyActivityType;
use NnShop\Application\AppBundle\Forms\Backend\Type\Settings\CreateCompanyTypeType;
use NnShop\Application\AppBundle\Forms\Backend\Type\Settings\EditCompanyActivityType;
use NnShop\Application\AppBundle\Forms\Backend\Type\Settings\EditCompanyTypeType;
use NnShop\Domain\Templates\Command\CompanyActivity\CreateActivityCommand;
use NnShop\Domain\Templates\Command\CompanyActivity\UpdateActivityCommand;
use NnShop\Domain\Templates\Command\CompanyType\CreateTypeCommand;
use NnShop\Domain\Templates\Command\CompanyType\UpdateTypeCommand;
use NnShop\Domain\Templates\QueryBuilder\CompanyActivityQueryBuilderInterface;
use NnShop\Domain\Templates\QueryBuilder\CompanyTypeQueryBuilderInterface;
use NnShop\Domain\Templates\Repository\CompanyActivityRepositoryInterface;
use NnShop\Domain\Templates\Repository\CompanyTypeRepositoryInterface;
use NnShop\Domain\Templates\Value\CompanyActivityId;
use NnShop\Domain\Templates\Value\CompanyTypeId;
use NnShop\Domain\Translation\Entity\Profile;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WizardController extends AbstractController
{
    /**
     * @Route("/settings/wizard/{typeId}", name="app.backend.settings.wizard.activities.index", requirements={"typeId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @param Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     *
     * @return Response
     */
    public function companyActivitiesIndexAction(Request $request): Response
    {
        $paginator = $this->get('knp_paginator');

        /** @var CompanyTypeRepositoryInterface $typeRepository */
        $typeRepository = $this->get(CompanyTypeRepositoryInterface::class);

        $typeId = $request->get('typeId');
        if (!CompanyTypeId::valid($typeId)) {
            throw $this->createNotFoundException();
        }

        $type = $typeRepository->findOneById(CompanyTypeId::from($typeId));
        if (null === $type) {
            throw $this->createNotFoundException();
        }

        /** @var CompanyActivityQueryBuilderInterface $builder */
        $builder = $this->get(CompanyActivityQueryBuilderInterface::class);

        $builder->reset()
            ->filterType()
            ->includeType($type);

        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 20);

        return $this->render(':backend/settings/wizard:activities_index.html.twig', [
            'type' => $type,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/settings/wizard/overview/{profile_id}", name="app.backend.settings.wizard.types.index")
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @throws \LogicException
     *
     * @return Response
     */
    public function companyTypesIndexAction(Request $request, Profile $profile): Response
    {
        $paginator = $this->get('knp_paginator');

        /** @var CompanyTypeQueryBuilderInterface $builder */
        $builder = $this->get(CompanyTypeQueryBuilderInterface::class);
        $builder->reset();

        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 20);

        return $this->render(':backend/settings/wizard:types_index.html.twig', [
            'pagination' => $pagination,
            'profile' => $profile,
        ]);
    }

    /**
     * @Route("/settings/wizard/{typeId}/add", name="app.backend.settings.wizard.activities.create", requirements={"typeId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @param Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     *
     * @return Response
     */
    public function createCompanyActivityAction(Request $request): Response
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /** @var CompanyTypeRepositoryInterface $typeRepository */
        $typeRepository = $this->get(CompanyTypeRepositoryInterface::class);

        $typeId = $request->get('typeId');
        if (!CompanyTypeId::valid($typeId)) {
            throw $this->createNotFoundException();
        }

        $type = $typeRepository->findOneById(CompanyTypeId::from($typeId));
        if (null === $type) {
            throw $this->createNotFoundException();
        }

        /*
        * Formulier
        */
        $formRequest = new CreateCompanyActivityRequest($type->getProfile());

        $form = $this->createNamedForm(null, CreateCompanyActivityType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
        * POST afhandelen
        */
        if ($form->isSubmitted() && $form->isValid()) {
            $activityId = CompanyActivityId::generate();

            $command = CreateActivityCommand::prepare($activityId, $formRequest->getTitle(), $type->getId());
            foreach ($formRequest->getTemplates() as $template) {
                $command->addTemplate($template->getId());
            }

            $commandBus->handle($command);

            $this->addFlash('success', 'De activiteit is succesvol toegevoegd.');

            return $this->redirectToRoute('app.backend.settings.wizard.activities.index', [
                'typeId' => $type->getId(),
            ]);
        }

        return $this->render(':backend/settings/wizard:activities_create.html.twig', [
            'type' => $type,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/settings/wizard/add/{profile_id}", name="app.backend.settings.wizard.types.create")
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @throws \LogicException
     *
     * @return Response
     */
    public function createCompanyTypeAction(Request $request, Profile $profile): Response
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /*
        * Formulier
        */
        $formRequest = new CreateCompanyTypeRequest();

        $form = $this->createNamedForm(null, CreateCompanyTypeType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
        * POST afhandelen
        */
        if ($form->isSubmitted() && $form->isValid()) {
            $typeId = CompanyTypeId::generate();

            $command = CreateTypeCommand::prepare($typeId, $formRequest->getTitle(), $profile->getId());
            $command->setDescription($formRequest->getDescription());

            $commandBus->handle($command);

            $this->addFlash('success', sprintf('Het scenario "%s" is succesvol toegevoegd.', $command->getTitle()));

            return $this->redirectToRoute('app.backend.settings.wizard.activities.index', [
                'typeId' => $typeId,
            ]);
        }

        return $this->render(':backend/settings/wizard:types_create.html.twig', [
            'form' => $form->createView(),
            'type' => $formRequest,
            'profile' => $profile,
        ]);
    }

    /**
     * @Route("/settings/wizard/{typeId}/edit", name="app.backend.settings.wizard.types.edit", requirements={"typeId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @param Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     *
     * @return Response
     */
    public function editCompanyTypeAction(Request $request): Response
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /** @var CompanyTypeRepositoryInterface $typeRepository */
        $typeRepository = $this->get(CompanyTypeRepositoryInterface::class);

        $typeId = $request->get('typeId');
        if (!CompanyTypeId::valid($typeId)) {
            throw $this->createNotFoundException();
        }

        $type = $typeRepository->findOneById(CompanyTypeId::from($typeId));
        if (null === $type) {
            throw $this->createNotFoundException();
        }

        /*
        * Formulier
        */
        $formRequest = new EditCompanyTypeRequest($type);

        $form = $this->createNamedForm(null, EditCompanyTypeType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
        * POST afhandelen
        */
        if ($form->isSubmitted() && $form->isValid()) {
            $command = UpdateTypeCommand::prepare($type->getId());

            $command->setTitle($formRequest->getTitle());
            $command->setDescription($formRequest->getDescription());

            $commandBus->handle($command);

            $this->addFlash('success', 'De gegevens van het scenario zijn succesvol gewijzigd.');

            return $this->redirectToRoute('app.backend.settings.wizard.types.index', ['profile_id' => $type->getProfile()->getId()]);
        }

        return $this->render(':backend/settings/wizard:types_edit.html.twig', [
            'type' => $type,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/settings/wizard/{typeId}/{activityId}/edit", name="app.backend.settings.wizard.activities.edit", requirements={"typeId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$", "activityId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @param Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     *
     * @return Response
     */
    public function editCompanyActivityAction(Request $request): Response
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /** @var CompanyActivityRepositoryInterface $activityRepository */
        $activityRepository = $this->get(CompanyActivityRepositoryInterface::class);

        $activityId = $request->get('activityId');
        if (!CompanyActivityId::valid($activityId)) {
            throw $this->createNotFoundException();
        }

        $activity = $activityRepository->findOneById(CompanyActivityId::from($activityId));
        if (null === $activity || $activity->getType()->getId() != CompanyTypeId::from($request->get('typeId', false))) {
            throw $this->createNotFoundException();
        }

        /*
        * Formulier
        */
        $formRequest = new EditCompanyActivityRequest($activity);

        $form = $this->createNamedForm(null, EditCompanyActivityType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
        * POST afhandelen
        */
        if ($form->isSubmitted() && $form->isValid()) {
            $command = UpdateActivityCommand::prepare($activity->getId());
            $command->setTitle($formRequest->getTitle());

            foreach ($formRequest->getTemplates() as $template) {
                $command->addTemplate($template->getId());
            }

            $commandBus->handle($command);

            $this->addFlash('success', 'De gegevens van de activiteit zijn succesvol gewijzigd.');

            return $this->redirectToRoute('app.backend.settings.wizard.activities.index', [
                'typeId' => $activity->getType()->getId(),
            ]);
        }

        return $this->render(':backend/settings/wizard:activities_edit.html.twig', [
            'activity' => $activity,
            'form' => $form->createView(),
        ]);
    }
}
