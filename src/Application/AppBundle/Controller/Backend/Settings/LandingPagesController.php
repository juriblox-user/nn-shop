<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Settings;

use Core\Application\CoreBundle\Controller\AbstractController;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\CreateLandingPageRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\EditLandingPageRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\Settings\CreateLandingPageType;
use NnShop\Application\AppBundle\Forms\Backend\Type\Settings\EditLandingPageType;
use NnShop\Domain\Shops\Command\LandingPage\CreateLandingPageCommand;
use NnShop\Domain\Shops\Command\LandingPage\UpdateLandingPageCommand;
use NnShop\Domain\Shops\Entity\LandingPage;
use NnShop\Domain\Shops\QueryBuilder\LandingPageQueryBuilderInterface;
use NnShop\Domain\Shops\Repository\LandingPageRepositoryInterface;
use NnShop\Domain\Shops\Value\LandingPageId;
use NnShop\Domain\Translation\Entity\Profile;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LandingPagesController extends AbstractController
{
    /**
     * @Route("/settings/landingpages/{profile_id}", name="app.backend.settings.landing_pages.index")
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @throws \LogicException
     *
     * @return Response
     */
    public function indexAction(Request $request, Profile $profile): Response
    {
        $paginator = $this->get('knp_paginator');

        /** @var LandingPageQueryBuilderInterface $builder */
        $builder = $this->get(LandingPageQueryBuilderInterface::class);
        $builder->reset();

        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($builder->build(), $request->query->getInt('pagina', 1), 20);

        return $this->render(':backend/settings/landing_pages:index.html.twig', [
            'pagination' => $pagination,
            'profile' => $profile,
        ]);
    }

    /**
     * @Route("/settings/landingpages/add/{profile_id}", name="app.backend.settings.landing_pages.create")
     * @ParamConverter("profile", options={"mapping": {"profile_id": "id"}})
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @throws \LogicException
     *
     * @return Response
     */
    public function createAction(Request $request, Profile $profile): Response
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /*
         * Formulier
         */
        $formRequest = new CreateLandingPageRequest($profile);

        $form = $this->createNamedForm(null, CreateLandingPageType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {
            $command = CreateLandingPageCommand::prepare(LandingPageId::generate(), $profile->getId());

            $command->setTitle($formRequest->getTitle());
            $command->setText($formRequest->getText());

            // Gekoppelde kortingscode
            if (null !== $formRequest->getDiscount()) {
                $command->linkDiscount($formRequest->getDiscount()->getId());
            }

            // Gekoppelde templates
            foreach ($formRequest->getTemplates() as $template) {
                $command->addTemplate($template->getId());
            }

            $commandBus->handle($command);

            $this->addFlash('success', 'De landingspagina is succesvol toegevoegd.');

            return $this->redirectToRoute('app.backend.settings.landing_pages.index', ['profile_id' => $profile->getId()]);
        }

        return $this->render(':backend/settings/landing_pages:create.html.twig', [
            'form' => $form->createView(),
            'profile' => $profile,
        ]);
    }

    /**
     * @Route("/settings/landingpages/{pageId}/edit", name="app.backend.settings.landing_pages.edit", requirements={"pageId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"})
     *
     * @param Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     *
     * @return Response
     */
    public function editAction(Request $request): Response
    {
        /** @var MessageBus $commandBus */
        $commandBus = $this->get('command_bus');

        /** @var LandingPageRepositoryInterface $pageRepository */
        $pageRepository = $this->get(LandingPageRepositoryInterface::class);

        $pageId = $request->get('pageId');
        if (!LandingPageId::valid($pageId)) {
            throw $this->createNotFoundException();
        }

        /** @var LandingPage|null $landingPage */
        $landingPage = $pageRepository->findOneById(LandingPageId::from($pageId));
        if (null === $landingPage) {
            throw $this->createNotFoundException();
        }

        /*
         * Formulier
         */
        $formRequest = new EditLandingPageRequest($landingPage);

        $form = $this->createNamedForm(null, EditLandingPageType::class, $formRequest);
        $form->handleRequest($request);

        $formRequest->bind($form);

        /*
         * POST afhandelen
         */
        if ($form->isSubmitted() && $form->isValid()) {
            $command = UpdateLandingPageCommand::prepare($landingPage);

            $command->setTitle($formRequest->getTitle());
            $command->setText($formRequest->getText());

            // Gekoppelde kortingscode
            if (null !== $formRequest->getDiscount()) {
                $command->linkDiscount($formRequest->getDiscount()->getId());
            } else {
                $command->unlinkDiscount();
            }

            // Gekoppelde templates
            foreach ($formRequest->getTemplates() as $template) {
                $command->addTemplate($template->getId());
            }

            $commandBus->handle($command);

            $this->addFlash('success', 'De gegevens van de landingspage zijn succesvol gewijzigd.');

            return $this->redirectToRoute('app.backend.settings.landing_pages.index', ['profile_id' => $landingPage->getProfile()->getId()]);
        }

        return $this->render(':backend/settings/landing_pages:edit.html.twig', [
            'landingPage' => $landingPage,
            'form' => $form->createView(),
        ]);
    }
}
