<?php

namespace NnShop\Application\AppBundle\Controller\Backend\Settings;

use Core\Application\CoreBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    /**
     * @Route("/settings/general", name="app.backend.settings.default.index")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        return $this->render(':backend/dashboard/default:index.html.twig');
    }
}
