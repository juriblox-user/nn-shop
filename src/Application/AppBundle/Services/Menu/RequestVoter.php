<?php

namespace NnShop\Application\AppBundle\Services\Menu;

use Knp\Menu\ItemInterface;
use Knp\Menu\Matcher\Voter\VoterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class RequestVoter.
 *
 * @deprecated
 *
 * @todo Imported from symfony-core: 1.3.16
 */
class RequestVoter implements VoterInterface
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RequestStack $requestStack, RouterInterface $router)
    {
        $this->requestStack = $requestStack;
        $this->router = $router;
    }

    public function matchItem(ItemInterface $item)
    {
        $request = $this->requestStack->getCurrentRequest();

        // URL's zijn precies gelijk
        if ($item->getUri() === $request->getRequestUri()) {
            return true;
        }

        // URL is niet alleen "/" en precies gelijk
        if ('/' !== $item->getUri() && (mb_substr($request->getRequestUri(), 0, mb_strlen($item->getUri())) === $item->getUri())) {
            return true;
        }

        // URL is gedeeltelijk gelijk
        if ((1 == $item->getLevel() || $this->isSubRoute($request, $item)) && (null !== $item->getUri() || $item->hasChildren())) {
            if (null !== $item->getUri()) {
                $itemParts = explode('/', trim($item->getUri(), '/'));
            } else {
                $itemParts = explode('/', trim($item->getFirstChild()->getUri(), '/'));
            }

            $requestParts = explode('/', trim($request->getRequestUri(), '/'));

            for ($i = 1; $i < count($itemParts) && $i < count($requestParts); ++$i) {
                if (implode('/', array_slice($itemParts, 0, $i)) == implode('/', array_slice($requestParts, 0, $i))) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param Request       $request
     * @param ItemInterface $item
     *
     * @return bool
     */
    private function isSubRoute(Request $request, ItemInterface $item): bool
    {
        $routeItems = $item->getExtra('routes', []);
        $refererUrlParams = $this->router->match(parse_url($request->headers->get('referer'), PHP_URL_PATH));
        $actionName = '';
        $currentRoute = $request->attributes->get('_route');
        $routePrefix = $this->getRoutePrefix($currentRoute, $actionName);
        foreach ($routeItems as $routeItem) {
            if (isset($refererUrlParams['view'])) {

                $hasView = isset($routeItem['parameters']['view']) &&
                    $refererUrlParams['view'] === $routeItem['parameters']['view'];

                if ($hasView && $request->attributes->has('view') && $actionName !== 'index_by_profile') {
                    continue;
                }
                if ($hasView && $routePrefix === $this->getRoutePrefix($routeItem['route'])) {
                    return true;
                }
            } elseif ('index' !== $actionName && "{$routePrefix}.index" === $routeItem['route']) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $routeName
     * @param string $suffix
     *
     * @return string
     */
    private function getRoutePrefix(string $routeName, string &$suffix = '')
    {
        $exploded = explode('.', $routeName);
        $suffix = end($exploded);
        unset($exploded[count($exploded) - 1]);

        return implode('.', $exploded);
    }
}
