<?php

namespace NnShop\Application\AppBundle\Services\Factory;

use Imagine\Gd\Imagine as GdImagine;
use Imagine\Image\ImagineInterface;
use Imagine\Imagick\Imagine as ImagickImagine;

class ImagineFactory
{
    /**
     * @return ImagineInterface
     */
    public static function create(): ImagineInterface
    {
        $engine = getenv('IMAGINE_ENGINE');

        /*
         * Gebruik maken van de IMAGINE_ENGINE env variable
         */
        if (false !== $engine && '' !== $engine) {
            switch ($engine) {
                case 'gd':
                    return new GdImagine();

                case 'imagick':
                    return new ImagickImagine();

                default:
                    throw new \RuntimeException(sprintf('The Imagine engine "%s" is unsupported, check the IMAGINE_ENGINE env variable', $engine));
            }
        }

        if (\extension_loaded('imagick')) {
            return new ImagickImagine();
        } elseif (\extension_loaded('gd')) {
            return new GdImagine();
        }

        throw new \RuntimeException('Could not find a Imagine instance for this environment');
    }
}
