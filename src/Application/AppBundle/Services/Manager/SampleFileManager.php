<?php

namespace NnShop\Application\AppBundle\Services\Manager;

use Gaufrette\Filesystem;
use NnShop\Domain\Templates\Entity\Template;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;

class SampleFileManager
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @param Template $template
     *
     * @return string
     */
    public function get(Template $template): string
    {
        $file = $this->filesystem->get($template->getId()->getString());

        return $file->getContent();
    }

    /**
     * @param Template $template
     * @param File     $file
     */
    public function store(Template $template, File $file)
    {
        $managedFile = $this->filesystem->createFile($template->getId()->getString());
        $managedFile->setContent(file_get_contents($file->getRealPath()));
    }

    /**
     * @param Template $template
     *
     * @return StreamedResponse
     */
    public function stream(Template $template): StreamedResponse
    {
        $response = new StreamedResponse(
            function () use ($template) {
                echo $this->get($template);
            }, 200, [
                'Content-Type' => 'application/pdf',
            ]
        );

        $response->headers->set('Content-Disposition', $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            sprintf('%s (voorbeeld).pdf', $template->getSlug())
        ));

        return $response;
    }
}
