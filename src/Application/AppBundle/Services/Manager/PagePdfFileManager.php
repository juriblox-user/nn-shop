<?php

namespace NnShop\Application\AppBundle\Services\Manager;

use Gaufrette\Filesystem;
use NnShop\Application\AppBundle\Forms\Backend\Request\Pages\CreatePageRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Pages\EditPageRequest;
use NnShop\Domain\Pages\Entity\Page;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class PagePdfFileManager.
 */
class PagePdfFileManager
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $publicPath;

    /**
     * @param Filesystem $filesystem
     */
    public function __construct(FileSystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @param Page $page
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     * @throws \Gaufrette\Exception\FileNotFound
     */
    public function delete(Page $page)
    {
        $file = $this->filesystem->get($page->getPdf());
        $file->delete();

        $page->setPdf(null);
        $page->setPdfDirectory(null);
        $page->setPdfName(null);
    }

    /**
     * @param Page $page
     *
     * @throws \InvalidArgumentException
     * @throws \Gaufrette\Exception\FileNotFound
     *
     * @return string|null
     */
    public function getUrl($page)
    {
        /*
         * If form doesn't validate show nothing.
         */
        if ($page instanceof CreatePageRequest || $page instanceof  EditPageRequest) {
            return;
        }

        $file = $this->filesystem->get($page->getPdfDirectory() . '/' . $page->getPdfName());

        return $this->publicPath . $file->getName();
    }

    /**
     * @param Page $page
     * @param File $file
     *
     * @throws \InvalidArgumentException
     */
    public function store(Page $page, File $file)
    {
        $managedFile = $this->filesystem->createFile($page->getSlug() . '/' . $page->getId() . '.pdf');
        $managedFile->setContent(file_get_contents($file->getRealPath()));

        $page->setPdfDirectory($page->getSlug());
        $page->setPdfName($page->getId() . '.pdf');
        $page->setPdf($managedFile->getName());
    }

    /**
     * @param string $publicPath
     */
    public function setPublicPath(string $publicPath)
    {
        $this->publicPath = rtrim($publicPath, '/') . '/';
    }
}
