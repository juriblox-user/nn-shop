<?php

namespace NnShop\Application\AppBundle\Services\Manager;

use Core\Application\CoreBundle\Service\Manager\AbstractImageManager;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;
use Imagine\Image\ImageInterface;
use NnShop\Domain\Templates\Entity\Partner;
use Symfony\Component\HttpFoundation\File\File;

class PartnerLogoManager extends AbstractImageManager
{
    /**
     * Normale grootte.
     */
    const SIZE_DEFAULT = 'default';

    /**
     * Thumbnail.
     */
    const SIZE_THUMBNAIL = 'thumbnail';

    /**
     * Logo verwijderen.
     *
     * @param Partner $partner
     */
    public function deleteLogo(Partner $partner)
    {
        if (null === $partner->getLogo()) {
            return;
        }

        $this->eachSize(function ($size) use ($partner) {
            $this->deleteJpeg($partner->getLogo(), $size);
        });

        $partner->setLogo(null);
    }

    /**
     * URL voor het logo ophalen.
     *
     * @param Partner $partner
     * @param string  $size
     *
     * @return string
     */
    public function getLogoUrl(Partner $partner, string $size): string
    {
        if (null === $partner->getLogo()) {
            return null;
        }

        if (!isset($this->sizes[$size])) {
            throw new \InvalidArgumentException(sprintf('The size "%s" has not been defined', $size));
        }

        return $this->getPublicPath() . $this->getJpegFilename($partner->getLogo(), $this->sizes[$size]);
    }

    /**
     * Logo instellen.
     *
     * @param Partner $partner
     * @param File    $file
     */
    public function setLogo(Partner $partner, File $file)
    {
        $this->deleteLogo($partner);

        $image = $this->imagine->open($file->getPathname());

        $partner->setLogo($this->generateFilename());

        $this->thumbnails($image, ImageInterface::THUMBNAIL_INSET, function (ImageInterface $image, BoxInterface $size) use ($partner) {
            $this->writeJpeg($image, $partner->getLogo(), $size);
        });
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->sizes = [
            self::SIZE_THUMBNAIL => new Box(200, 200),
            self::SIZE_DEFAULT => new Box(400, 400),
        ];
    }
}
