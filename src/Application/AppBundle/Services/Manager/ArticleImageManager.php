<?php

namespace NnShop\Application\AppBundle\Services\Manager;

use Core\Application\CoreBundle\Service\Manager\AbstractImageManager;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;
use Imagine\Image\ImageInterface;
use NnShop\Application\AppBundle\Forms\Backend\Request\Blog\CreateArticleRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Blog\EditArticleRequest;
use NnShop\Domain\Blog\Entity\Article;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class ArticleImageManager.
 */
class ArticleImageManager extends AbstractImageManager
{
    /**
     * Normale grootte.
     */
    const SIZE_DEFAULT = 'default';

    /**
     * Thumbnail.
     */
    const SIZE_THUMBNAIL = 'thumbnail';

    /**
     * @param Article $article
     */
    public function deleteImage(Article $article)
    {
        if (null === $article->getImage()) {
            return;
        }

        $this->eachSize(function ($size) use ($article) {
            $this->deleteJpeg($article->getImage(), $size);
        });

        $article->setImage(null);
    }

    /**
     * @param Article $article
     * @param string  $size
     *
     * @throws \Exception
     * @throws \InvalidArgumentException
     *
     * @return string
     */
    public function getImageUrl($article, string $size): string
    {
        if (!($article instanceof Article || $article instanceof  EditArticleRequest || $article instanceof CreateArticleRequest)) {
            throw new \InvalidArgumentException(sprintf('only objects of Article|EditArticleRequest|CreateArticleRequest are allowed'));
        }

        if (null === $article->getImage()) {
            return null;
        }

        if (!isset($this->sizes[$size])) {
            throw new \InvalidArgumentException(sprintf('The size "%s" has not been defined', $size));
        }

        return $this->getPublicPath() . $this->getJpegFilename($article->getImage(), $this->sizes[$size]);
    }

    /**
     * @param Article $article
     * @param File    $file
     *
     * @throws \Imagine\Exception\RuntimeException
     */
    public function setImage(Article $article, File $file)
    {
        $this->deleteImage($article);

        $image = $this->imagine->open($file->getPathname());

        $article->setImage($this->generateFilename());

        $this->thumbnails($image, ImageInterface::THUMBNAIL_INSET, function (ImageInterface $image, BoxInterface $size) use ($article) {
            $this->writeJpeg($image, $article->getImage(), $size);
        });
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Imagine\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this->sizes = [
            self::SIZE_THUMBNAIL => new Box(200, 200),
            self::SIZE_DEFAULT => new Box(400, 400),
        ];
    }
}
