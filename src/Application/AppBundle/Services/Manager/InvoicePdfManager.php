<?php

namespace NnShop\Application\AppBundle\Services\Manager;

use Core\Component\Filesystem\TempFile;
use Gaufrette\Filesystem;
use NnShop\Domain\Orders\Entity\Invoice;
use Knp\Snappy\Pdf as SnappyPdf;
use mikehaertl\pdftk\Pdf;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;

class InvoicePdfManager
{
    /**
     * @var bool
     */
    private $cache;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string|null
     */
    private $letterhead;

    /**
     * @var SnappyPdf
     */
    private $snappy;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * InvoicePdfManager constructor.
     *
     * @param Filesystem        $filesystem
     * @param SnappyPdf         $snappy
     * @param \Twig_Environment $twig
     */
    public function __construct(Filesystem $filesystem, SnappyPdf $snappy, \Twig_Environment $twig)
    {
        $this->cache = true;
        $this->filesystem = $filesystem;

        $this->snappy = $snappy;
        $this->twig = $twig;
    }

    /**
     * Cache uitschakelen.
     */
    public function disableCache()
    {
        $this->cache = false;
    }

    /**
     * Cache inschakelen.
     */
    public function enableCache()
    {
        $this->cache = true;
    }

    /**
     * @param Invoice $invoice
     *
     * @return string
     */
    public function get(Invoice $invoice): string
    {
        if ($this->cache && $this->filesystem->has($invoice->getId())) {
            return $this->filesystem->get($invoice->getId()->getString())->getContent();
        }

        // Factuur genereren
        $html = $this->twig->render(':pdf:invoice.html.twig', [
            'invoice' => $invoice,
            'customer' => $invoice->getCustomer(),
        ]);

        $raw = $this->snappy->getOutputFromHtml($html, [
            'margin-left' => 20,
            'margin-right' => 15,
            'margin-top' => 70,
            'margin-bottom' => 20,
        ]);

        /*
         * Briefpapier toevoegen
         */
        if (null !== $this->letterhead) {
            $inputFile = TempFile::fromContent($raw);
            $outputFile = TempFile::getTempFile();

            $pdf = new Pdf($inputFile->getPathname());

            $basic_slug = preg_replace('/[0-9]+/', '', $invoice->getProfile()->getSlug());

            $letterhead = str_replace('{country-language}', $basic_slug, $this->letterhead);
            if (!file_exists($letterhead)) {
                throw new \RuntimeException($letterhead . ' does not exist');
            }

            $pdf->multiBackground(realpath($letterhead));

            $pdf->saveAs($outputFile);

            $raw = file_get_contents($outputFile);
        }

        // PDF opslaan in cache
        if ($this->cache) {
            $file = $this->filesystem->createFile($invoice->getId()->getString());
            $file->setContent($raw);
        }

        return $raw;
    }

    /**
     * Bepalen of caching is ingeschakeld.
     *
     * @return bool
     */
    public function isCacheEnabled(): bool
    {
        return $this->cache;
    }

    /**
     * @param $file
     */
    public function setLetterhead($file)
    {
        $this->letterhead = $file;
    }

    /**
     * @param Invoice $invoice
     *
     * @return StreamedResponse
     */
    public function stream(Invoice $invoice): StreamedResponse
    {
        $response = new StreamedResponse(
            function () use ($invoice) {
                echo $this->get($invoice);
            }, 200, [
                'Content-Type' => 'application/pdf',
            ]
        );

        $response->headers->set('Content-Disposition', $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            sprintf('Factuur %s.pdf', $invoice->getCode())
        ));

        return $response;
    }
}
