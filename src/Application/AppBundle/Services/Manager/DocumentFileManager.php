<?php

namespace NnShop\Application\AppBundle\Services\Manager;

use Gaufrette\Filesystem;
use NnShop\Domain\Templates\Entity\Document;
use Stringy\Stringy;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;

class DocumentFileManager
{
    /**
     * Markdown.
     */
    const TYPE_MARKDOWN = 'markdown';

    /**
     * PDF.
     */
    const TYPE_PDF = 'pdf';

    /**
     * Word-2007.
     */
    const TYPE_WORD2007 = 'word2007';

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * DocumentFileManager constructor.
     *
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * Bestand als Markdown ophalen.
     *
     * @param Document $document
     *
     * @return string
     */
    public function getMarkdown(Document $document): string
    {
        $file = $this->filesystem->get(sprintf('%s/%s', $document->getId(), self::TYPE_MARKDOWN));

        return $file->getContent();
    }

    /**
     * Bestand als PDF-data ophalen.
     *
     * @param Document $document
     *
     * @return string
     */
    public function getPdf(Document $document): string
    {
        $file = $this->filesystem->get(sprintf('%s/%s', $document->getId(), self::TYPE_PDF));

        return $file->getContent();
    }

    /**
     * Bestand als Word-2007 data ophalen.
     *
     * @param Document $document
     *
     * @return string
     */
    public function getWord2007(Document $document): string
    {
        $file = $this->filesystem->get(sprintf('%s/%s', $document->getId(), self::TYPE_WORD2007));

        return $file->getContent();
    }

    /**
     * @param Document $document
     * @param          $raw
     */
    public function storeMarkdown(Document $document, $raw)
    {
        $file = $this->filesystem->createFile(sprintf('%s/%s', $document->getId(), self::TYPE_MARKDOWN));
        $file->setContent(trim($raw));
    }

    /**
     * @param Document $document
     * @param          $raw
     */
    public function storePdf(Document $document, $raw)
    {
        $file = $this->filesystem->createFile(sprintf('%s/%s', $document->getId(), self::TYPE_PDF));
        $file->setContent($raw);
    }

    /**
     * @param Document $document
     * @param          $raw
     */
    public function storeWord2007(Document $document, $raw)
    {
        $file = $this->filesystem->createFile(sprintf('%s/%s', $document->getId(), self::TYPE_WORD2007));
        $file->setContent($raw);
    }

    /**
     * Markdown StreamedResponse voorbereiden.
     *
     * @param Document $document
     *
     * @return StreamedResponse
     */
    public function streamMarkdown(Document $document): StreamedResponse
    {
        $response = new StreamedResponse(
            function () use ($document) {
                echo $this->getMarkdown($document);
            }, 200, [
                'Content-Type' => 'text/markdown',
            ]
        );

        $response->headers->set('Content-Disposition', $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            sprintf('%s.md', $this->sanitizeTitle($document))
        ));

        return $response;
    }

    /**
     * PDF StreamedResponse voorbereiden.
     *
     * @param Document $document
     *
     * @return StreamedResponse
     */
    public function streamPdf(Document $document): StreamedResponse
    {
        $response = new StreamedResponse(
            function () use ($document) {
                echo $this->getPdf($document);
            }, 200, [
                'Content-Type' => 'application/pdf',
            ]
        );

        $response->headers->set('Content-Disposition', $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            sprintf('%s.pdf', $this->sanitizeTitle($document))
        ));

        return $response;
    }

    /**
     * Word-2007 StreamedResponse voorbereiden.
     *
     * @param Document $document
     *
     * @return StreamedResponse
     */
    public function streamWord2007(Document $document): StreamedResponse
    {
        $response = new StreamedResponse(
            function () use ($document) {
                echo $this->getWord2007($document);
            }, 200, [
                'Content-Type' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            ]
        );

        $response->headers->set('Content-Disposition', $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            sprintf('%s.docx', $this->sanitizeTitle($document))
        ));

        return $response;
    }

    private function sanitizeTitle(Document $document)
    {
        $title = Stringy::create(preg_replace('/[^A-Za-z0-9\s\-\(\)]+/', '', $document->getTemplate()->getTitle()))
            ->collapseWhitespace()
            ->__toString();

        return $title;
    }
}
