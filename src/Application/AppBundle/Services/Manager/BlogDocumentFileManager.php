<?php

namespace NnShop\Application\AppBundle\Services\Manager;

use Gaufrette\Filesystem;
use NnShop\Application\AppBundle\Forms\Backend\Request\Blog\DocumentRequest;
use NnShop\Domain\Blog\Entity\Document;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class BlogDocumentFileManager.
 */
class BlogDocumentFileManager
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $publicPath;

    /**
     * @param Filesystem $filesystem
     */
    public function __construct(FileSystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @param Document $document
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     * @throws \Gaufrette\Exception\FileNotFound
     */
    public function delete(Document $document)
    {
        $file = $this->filesystem->get($document->getFile());
        $file->delete();
    }

    /**
     * @param Document $document
     *
     * @throws \InvalidArgumentException
     * @throws \Gaufrette\Exception\FileNotFound
     *
     * @return string|null
     */
    public function getUrl($document)
    {
        if (!($document instanceof Document || $document instanceof DocumentRequest)) {
            throw new \InvalidArgumentException(sprintf('only objects of Author or EditAuthorRequest or CreateAuthorRequest are allowed'));
        }

        /*
         * If request object is passed then the form didn't validate correctly. There is no file to show url of.
         */
        if ($document instanceof DocumentRequest) {
            return;
        }

        $file = $this->filesystem->get($document->getDirectory() . '/' . $document->getName());

        return $this->publicPath . $file->getName();
    }

    /**
     * @param Document $document
     * @param File     $file
     *
     * @throws \InvalidArgumentException
     */
    public function store(Document $document, File $file)
    {
        $managedFile = $this->filesystem->createFile($document->getArticle()->getSlug() . '/' . $document->getId() . '.pdf');
        $managedFile->setContent(file_get_contents($file->getRealPath()));

        $document->setDirectory($document->getArticle()->getSlug());
        $document->setName($document->getId() . '.pdf');
        $document->setFile($managedFile->getName());
    }

    /**
     * @param string $publicPath
     */
    public function setPublicPath(string $publicPath)
    {
        $this->publicPath = rtrim($publicPath, '/') . '/';
    }
}
