<?php

namespace NnShop\Application\AppBundle\Services\Manager;

use Core\Application\CoreBundle\Service\Manager\AbstractImageManager;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;
use Imagine\Image\ImageInterface;
use NnShop\Domain\Shops\Entity\Referrer;
use Symfony\Component\HttpFoundation\File\File;

class ReferrerLogoManager extends AbstractImageManager
{
    /**
     * Normale grootte.
     */
    const SIZE_DEFAULT = 'default';

    /**
     * Homepage.
     */
    const SIZE_HOMEPAGE = 'homepage';

    /**
     * Thumbnail.
     */
    const SIZE_THUMBNAIL = 'thumbnail';

    /**
     * Logo verwijderen.
     *
     * @param Referrer $referrer
     */
    public function deleteLogo(Referrer $referrer)
    {
        if (null === $referrer->getLogo()) {
            return;
        }

        $this->eachSize(function ($size) use ($referrer) {
            $this->deleteJpeg($referrer->getLogo(), $size);
        });

        $referrer->setLogo(null);
    }

    /**
     * URL voor het logo ophalen.
     *
     * @param Referrer $referrer
     * @param string   $size
     *
     * @return string
     */
    public function getLogoUrl(Referrer $referrer, string $size): string
    {
        if (null === $referrer->getLogo()) {
            return null;
        }

        if (!isset($this->sizes[$size])) {
            throw new \InvalidArgumentException(sprintf('The size "%s" has not been defined', $size));
        }

        return $this->getPublicPath() . $this->getJpegFilename($referrer->getLogo(), $this->sizes[$size]);
    }

    /**
     * Logo instellen.
     *
     * @param Referrer $referrer
     * @param File     $file
     */
    public function setLogo(Referrer $referrer, File $file)
    {
        $this->deleteLogo($referrer);

        $image = $this->imagine->open($file->getPathname());

        $referrer->setLogo($this->generateFilename());

        // Thumbnail
        $this->thumbnail($image, $this->sizes[self::SIZE_THUMBNAIL], ImageInterface::THUMBNAIL_INSET, function (ImageInterface $image, BoxInterface $size) use ($referrer) {
            $this->writeJpeg($image, $referrer->getLogo(), $size);
        });

        // Normale maat
        $this->thumbnail($image, $this->sizes[self::SIZE_DEFAULT], ImageInterface::THUMBNAIL_INSET, function (ImageInterface $image, BoxInterface $size) use ($referrer) {
            $this->writeJpeg($image, $referrer->getLogo(), $size);
        });

        // Homepage (gecentreerd)
        $this->thumbnail($image, $this->sizes[self::SIZE_HOMEPAGE], ImageInterface::THUMBNAIL_INSET, function (ImageInterface $image, BoxInterface $size) use ($referrer) {
            $this->writeJpeg($this->center($image, $size), $referrer->getLogo(), $size);
        });
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->sizes = [
            self::SIZE_THUMBNAIL => new Box(200, 200),
            self::SIZE_DEFAULT => new Box(600, 500),
            self::SIZE_HOMEPAGE => new Box(400, 200),
        ];
    }
}
