<?php


namespace NnShop\Application\AppBundle\Services\Manager;


use Core\Application\CoreBundle\Service\Manager\AbstractImageManager;
use NnShop\Domain\Templates\Entity\Template;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;
use Imagine\Image\ImageInterface;
use Symfony\Component\HttpFoundation\File\File;

class TemplatePreviewManager extends AbstractImageManager
{
    const SIZE_DEFAULT = 'default';

    public function getPreviewUrl(Template $template, string $size): string
    {
        if (null === $template->getPreview()) {
            return null;
        }

        if (!isset($this->sizes[$size])) {
            throw new \InvalidArgumentException(sprintf("The size '%s' has not been defined", $size));
        }

        return $this->getPublicPath() . $this->getJpegFilename($template->getPreview(), $this->sizes[$size]);
    }

    public function setPreview(Template $template, File $file)
    {
        $preview = $this->imagine->open($file->getPathname());

        $template->setPreview($this->generateFilename());

        $this->thumbnails($preview, ImageInterface::THUMBNAIL_INSET, function (ImageInterface $preview, BoxInterface $size) use ($template) {
            $this->writeJpeg($preview, $template->getPreview(), $size);
        });
    }

    protected function configure()
    {
       $this->sizes = [
           self::SIZE_DEFAULT => new Box(420, 580),
       ];
    }
}
