<?php

namespace NnShop\Application\AppBundle\Services\Manager;

use Core\Application\CoreBundle\Service\Manager\AbstractImageManager;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;
use Imagine\Image\ImageInterface;
use NnShop\Application\AppBundle\Forms\Backend\Request\Blog\CreateAuthorRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Blog\EditAuthorRequest;
use NnShop\Domain\Blog\Entity\Author;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class AuthorImageManager.
 */
class AuthorImageManager extends AbstractImageManager
{
    /**
     * Normale grootte.
     */
    const SIZE_DEFAULT = 'default';

    /**
     * Thumbnail.
     */
    const SIZE_THUMBNAIL = 'thumbnail';

    /**
     * @param Author $author
     */
    public function deleteImage(Author $author)
    {
        if (null === $author->getImage()) {
            return;
        }

        $this->eachSize(function ($size) use ($author) {
            $this->deleteJpeg($author->getImage(), $size);
        });

        $author->setImage(null);
    }

    /**
     * @param Author $author
     * @param string $size
     *
     * @throws \InvalidArgumentException
     *
     * @return string
     */
    public function getImageUrl($author, string $size): string
    {
        if (!($author instanceof Author || $author instanceof  EditAuthorRequest || $author instanceof CreateAuthorRequest)) {
            throw new \InvalidArgumentException(sprintf('only objects of Author or EditAuthorRequest or CreateAuthorRequest are allowed'));
        }

        if (null === $author->getImage()) {
            return null;
        }

        if (!isset($this->sizes[$size])) {
            throw new \InvalidArgumentException(sprintf('The size "%s" has not been defined', $size));
        }

        return $this->getPublicPath() . $this->getJpegFilename($author->getImage(), $this->sizes[$size]);
    }

    /**
     * @param Author $author
     * @param File   $file
     *
     * @throws \Imagine\Exception\RuntimeException
     */
    public function setImage(Author $author, File $file)
    {
        $this->deleteImage($author);

        $image = $this->imagine->open($file->getPathname());

        $author->setImage($this->generateFilename());

        $this->thumbnails($image, ImageInterface::THUMBNAIL_INSET, function (ImageInterface $image, BoxInterface $size) use ($author) {
            $this->writeJpeg($image, $author->getImage(), $size);
        });
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Imagine\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this->sizes = [
            self::SIZE_THUMBNAIL => new Box(50, 200),
            self::SIZE_DEFAULT => new Box(100, 400),
        ];
    }
}
