<?php

namespace NnShop\Application\AppBundle\Services;


use NnShop\Domain\Shops\Entity\Shop;

use NnShop\Domain\Translation\Entity\Profile;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ShopContext
 * @package SafeShop\Application\AppBundle\Services
 */
class ShopContext
{
    /**
     * @var Profile
     */
    private $profile;

    /**
     * ShopContext constructor.
     * @param RequestStack $stack
     */
    public function __construct(RequestStack $stack)
    {
        if($stack->getCurrentRequest()) {
            $this->profile = $stack->getCurrentRequest()->get('_profile');
        }
    }

    /**
     * Profile bepalen.
     *
     * @return Shop|null
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function getProfile()
    {
        if (!$this->isShopContext()) {
            return null;
        }

        if (null !== $this->profile) {
            return $this->profile;
        }

        throw new NotFoundHttpException(sprintf('No Profile can be found!'));
    }

    public function hasProfile(): bool
    {
        return null !== $this->profile;
    }

    /**
     * Bepalen of we ons in de context van een shop bevinden.
     *
     * @return bool
     */
    public function isShopContext(): bool
    {
        return true;
    }
}
