<?php

namespace NnShop\Application\AppBundle\Services\Security\UserChecker;

use NnShop\Common\Exceptions\Security\DeletedCustomerException;
use NnShop\Domain\Orders\Entity\Customer;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class SoftDeletedCustomerChecker implements UserCheckerInterface
{
    /**
     * {@inheritdoc}
     *
     * @param Customer $customer
     */
    public function checkPreAuth(UserInterface $customer)
    {
        if ($customer->isDeleted()) {
            throw new DeletedCustomerException();
        }
    }

    /**
     * {@inheritdoc}
     *
     * @param Customer $customer
     */
    public function checkPostAuth(UserInterface $customer)
    {
    }
}
