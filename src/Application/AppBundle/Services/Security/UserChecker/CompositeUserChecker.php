<?php

namespace NnShop\Application\AppBundle\Services\Security\UserChecker;

use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class CompositeUserChecker implements UserCheckerInterface
{
    /**
     * @var UserCheckerInterface[]
     */
    private $checkers;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->checkers = [];
    }

    /**
     * @param UserCheckerInterface $userChecker
     */
    public function addChecker(UserCheckerInterface $userChecker)
    {
        $this->checkers[] = $userChecker;
    }

    /**
     * {@inheritdoc}
     */
    public function checkPostAuth(UserInterface $user)
    {
        foreach ($this->checkers as $checker) {
            $checker->checkPostAuth($user);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function checkPreAuth(UserInterface $user)
    {
        foreach ($this->checkers as $checker) {
            $checker->checkPreAuth($user);
        }
    }
}
