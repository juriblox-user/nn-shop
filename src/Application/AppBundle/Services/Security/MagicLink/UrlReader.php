<?php

namespace NnShop\Application\AppBundle\Services\Security\MagicLink;

use Carbon\Carbon;
use NnShop\Common\Exceptions\Security\MagicUrlSecurityException;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Value\CustomerId;

class UrlReader
{
    /**
     * @var UrlSigner
     */
    private $urlSigner;

    /**
     * Constructor.
     *
     * @param UrlSigner $urlSigner
     */
    public function __construct(UrlSigner $urlSigner)
    {
        $this->urlSigner = $urlSigner;
    }

    /**
     * @param string $signedUrl
     *
     * @return string
     */
    public function getCleanUrl(string $signedUrl): string
    {
        $cleanUrl = $this->removeQueryParameter(UrlSigner::PAYLOAD_QUERY_PARAMETER, $signedUrl);
        $cleanUrl = $this->removeQueryParameter(UrlSigner::SIGNATURE_QUERY_PARAMETER, $cleanUrl);

        return $cleanUrl;
    }

    /**
     * @param string $signedUrl
     *
     * @return CustomerId
     */
    public function getDoctorId(string $signedUrl): CustomerId
    {
        $payload = $this->getPayload($signedUrl);

        if (!isset($payload->id) || !CustomerId::valid($payload->id)) {
            throw new MagicUrlSecurityException('The provided signed URL does not contain the Doctor ID, or the Doctor ID is invalid', [
                'payload' => $payload,
            ]);
        }

        return new CustomerId($payload->id);
    }

    /**
     * @param string $signedUrl
     *
     * @return bool
     */
    public function isCorrupt(string $signedUrl): bool
    {
        try {
            $parameters = $this->getQueryParameters($signedUrl);
        } catch (\InvalidArgumentException $exception) {
            return true;
        }

        $testParameters = [
            UrlSigner::PAYLOAD_QUERY_PARAMETER,     // __magic
            UrlSigner::SIGNATURE_QUERY_PARAMETER,   // __signature
        ];

        foreach ($testParameters as $name) {
            if (!isset($parameters[$name])) {
                return true;
            }

            if ('' === $parameters[$name]) {
                return true;
            }

            // Base64 decode test
            if (false === base64_decode($parameters[$name], true)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $signedUrl
     *
     * @return bool
     */
    public function isExpired(string $signedUrl): bool
    {
        $payload = $this->getPayload($signedUrl);

        if (!isset($payload->expiration) || !is_numeric($payload->expiration)) {
            throw new MagicUrlSecurityException('The provided signed URL does not contain an expiration timestamp', [
                'payload' => $payload,
            ]);
        }

        return Carbon::now()->gte(Carbon::createFromTimestampUTC($payload->expiration));
    }

    /**
     * @param string   $signedUrl
     * @param Customer $customer
     *
     * @return bool
     */
    public function isValidSignature(string $signedUrl, Customer $customer): bool
    {
        // __signature uit de URL verwijderen
        $baseUrl = $this->removeQueryParameter(UrlSigner::SIGNATURE_QUERY_PARAMETER, $signedUrl);

        return hash_equals($this->urlSigner->getSignature($baseUrl, $customer->getToken()), $this->getSignature($signedUrl));
    }

    /**
     * @param string $signedUrl
     *
     * @return object
     */
    private function getPayload(string $signedUrl)
    {
        $payload = @unserialize($this->getQueryParameter(UrlSigner::PAYLOAD_QUERY_PARAMETER, $signedUrl));
        if (false === $payload) {
            throw new MagicUrlSecurityException('The payload could not be unserialized');
        }

        return $payload;
    }

    /**
     * @param string $parameter
     * @param string $signedUrl
     *
     * @return mixed
     */
    private function getQueryParameter(string $parameter, string $signedUrl)
    {
        $parameters = $this->getQueryParameters($signedUrl);

        $payload = $parameters[$parameter] ?? null;
        if (null === $payload) {
            throw new \InvalidArgumentException();
        }

        $payload = base64_decode($payload, true);
        if (false === $payload) {
            throw new MagicUrlSecurityException('The provided parameter could not be decoded', [
                'parameter' => $payload,
                'signedUrl' => $signedUrl,
            ]);
        }

        return $payload;
    }

    /**
     * @param string $signedUrl
     *
     * @return array
     */
    private function getQueryParameters(string $signedUrl): array
    {
        $query = parse_url($signedUrl, PHP_URL_QUERY);
        if (false === $query || null === $query) {
            throw new \InvalidArgumentException();
        }

        parse_str($query, $queryParameters);

        return $queryParameters;
    }

    /**
     * @param string $signedUrl
     *
     * @return string
     */
    private function getSignature(string $signedUrl): string
    {
        return $this->getQueryParameter(UrlSigner::SIGNATURE_QUERY_PARAMETER, $signedUrl);
    }

    /**
     * @param string $parameter
     * @param string $url
     *
     * @return string
     */
    private function removeQueryParameter(string $parameter, string $url): string
    {
        do {
            $url = preg_replace('/([\?&])' . preg_quote($parameter, '/') . '=[^&]*&{0,1}/i', '$1', $url, -1, $count);
        } while ($count > 0);

        return rtrim($url, '/?&');
    }
}
