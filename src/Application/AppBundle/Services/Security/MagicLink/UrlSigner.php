<?php

namespace NnShop\Application\AppBundle\Services\Security\MagicLink;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Core\Common\Security\SensitiveValue;
use NnShop\Domain\Orders\Entity\Customer;
use Symfony\Component\Routing\RouterInterface;

class UrlSigner
{
    /**
     * Query parameter waar de "magic" payload in zit (__magic).
     */
    const PAYLOAD_QUERY_PARAMETER = '__magic';

    /**
     * Query parameter waar de signature in zit (__signature).
     */
    const SIGNATURE_QUERY_PARAMETER = '__signature';

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * Constructor.
     *
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param string         $url
     * @param SensitiveValue $token
     *
     * @return string
     */
    public function getSignature(string $url, SensitiveValue $token): string
    {
        return hash_hmac('sha256', $url, $token->peek(), true);
    }

    /**
     * URL ondertekenen met de token van de gebruiker.
     *
     * @param string              $url
     * @param Customer            $customer
     * @param CarbonInterval|null $expiration
     *
     * @return string
     */
    public function sign(string $url, Customer $customer, CarbonInterval $expiration = null): string
    {
        $parsedUrl = parse_url($url);
        if (!isset($parsedUrl['path'])) {
            $url .= '/';
        }

        $separator = !isset($parsedUrl['query']) ? '?' : '&';

        // Standaard is de verloopdatum +1 week
        if (null === $expiration) {
            $expiration = CarbonInterval::week();
        }

        // "Magic" data toevoegen
        $queryData = [
            self::PAYLOAD_QUERY_PARAMETER => base64_encode(serialize((object) [
                'id' => $customer->getId()->getString(),
                'expiration' => Carbon::now()->add($expiration)->getTimestamp(),
            ])),
        ];

        // URL die wordt ondertekend
        $signingUrl = $url . $separator . http_build_query($queryData);

        return $signingUrl . '&' . http_build_query([
                self::SIGNATURE_QUERY_PARAMETER => base64_encode($this->getSignature($signingUrl, $customer->getToken())),
            ]);
    }

    /**
     * @param string              $name
     * @param array               $parameters
     * @param Customer            $customer
     * @param CarbonInterval|null $expiration
     *
     * @return string
     */
    public function signRoute(string $name, array $parameters, Customer $customer, CarbonInterval $expiration = null): string
    {
        return $this->sign($this->router->generate($name, $parameters, RouterInterface::ABSOLUTE_URL), $customer, $expiration);
    }
}
