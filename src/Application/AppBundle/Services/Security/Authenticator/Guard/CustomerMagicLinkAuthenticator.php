<?php

namespace NnShop\Application\AppBundle\Services\Security\Authenticator\Guard;

use NnShop\Common\Exceptions\Security\ExpiredMagicLinkException;
use NnShop\Domain\Orders\Entity\Customer;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class CustomerMagicLinkAuthenticator extends AbstractAuthenticator
{
    /**
     * @var Request|null
     */
    private $request;

    /**
     * {@inheritdoc}
     *
     * @param $customer Customer
     */
    public function checkCredentials($credentials, UserInterface $customer)
    {
        if (!$this->urlReader->isValidSignature($this->request->getUri(), $customer)) {
            return false;
        }

        // Aanvullende controles uitvoeren
        $this->checkAccount($customer);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials(Request $request)
    {
        $this->request = $request;

        // Controleren of URL is vervallen
        if ($this->urlReader->isExpired($request->getUri())) {
            throw new ExpiredMagicLinkException();
        }

        return [
            'id' => $this->urlReader->getDoctorId($request->getUri()),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        return $this->customerRepository->findOneById($credentials['id']);
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);

        return new RedirectResponse($this->router->generate('app.frontend.account.session.login', [
            'host' => $request->get('host'),
        ]));
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return new RedirectResponse($this->urlReader->getCleanUrl($request->getUri()));
    }

    /**
     * {@inheritdoc}
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new RedirectResponse($this->router->generate('app.frontend.account.session.login', [
            'host' => $request->get('host'),
        ]));
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request)
    {
        return $request->isMethod('GET') && !$this->urlReader->isCorrupt($request->getUri());
    }
}
