<?php

namespace NnShop\Application\AppBundle\Services\Security\Authenticator\Guard;

use Core\Domain\Value\Web\EmailAddress;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Account\Session\LoginRequest;
use NnShop\Application\AppBundle\Forms\Frontend\Type\Account\Session\LoginType;
use NnShop\Application\AppBundle\Services\Security\MagicLink\UrlReader;
use NnShop\Common\Exceptions\Security\Guard\CustomerEmailNotFoundAuthenticationException;
use NnShop\Common\Exceptions\Security\Guard\CustomerHasPendingInvitationAuthenticationException;
use NnShop\Common\Exceptions\Security\Guard\CustomerStatusPreventsLoginAuthenticationException;
use NnShop\Common\Exceptions\Security\Guard\InvalidPasswordAuthenticationException;
use NnShop\Domain\Orders\Command\Customer\CompleteRecoveryCommand;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Enumeration\CustomerStatus;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class CustomerFormAuthenticator extends AbstractAuthenticator
{
    use TargetPathTrait;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * Constructor.
     *
     * @param CustomerRepositoryInterface  $customerRepository
     * @param RouterInterface              $router
     * @param UrlReader                    $urlReader
     * @param FormFactoryInterface         $formFactory
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(CustomerRepositoryInterface $customerRepository, RouterInterface $router, UrlReader $urlReader, FormFactoryInterface $formFactory, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct($customerRepository, $router, $urlReader);

        $this->formFactory = $formFactory;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * {@inheritdoc}
     *
     * @param Customer $customer
     */
    public function checkCredentials($credentials, UserInterface $customer)
    {
        if ($customer->getStatus()->is(CustomerStatus::DRAFT)) {
            throw new CustomerEmailNotFoundAuthenticationException();
        }

        if ($customer->getStatus()->is(CustomerStatus::INVITED)) {
            throw new CustomerHasPendingInvitationAuthenticationException();
        }

        // Voorkomen dat iemand zonder verificatie via het loginformulier kan aanmelden
        if (!$customer->getStatus()->allowsInteractiveLogin()) {
            throw new CustomerStatusPreventsLoginAuthenticationException();
        }

        // Wachtwoord controleren
        if (!$this->passwordEncoder->isPasswordValid($customer, $credentials['password'])) {
            throw new InvalidPasswordAuthenticationException();
        }

        // Algemene controles op het account
        $this->checkAccount($customer);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials(Request $request)
    {
        $formRequest = new LoginRequest();

        $form = $this->formFactory->create(LoginType::class, $formRequest);
        $form->handleRequest($request);

        // Laatstgebruikte e-mailadres overnemen
        $request->getSession()->set(Security::LAST_USERNAME, $formRequest->getEmail());

        if (!$form->isValid()) {
            $errors = $form->getErrors(true);

            throw new CustomUserMessageAuthenticationException(count($errors) > 0 ? $errors[0]->getMessage() : 'U kon niet worden aangemeld. Probeer nogmaals, alstublieft.');
        }

        return [
            'email' => $formRequest->getEmail(),
            'password' => $formRequest->getPassword(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $employee = $this->customerRepository->findOneByEmail(EmailAddress::from($credentials['email']));
        if (null === $employee) {
            throw new CustomerEmailNotFoundAuthenticationException();
        }

        return $employee;
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        /** @var Customer $customer */
        $customer = $token->getUser();

        $targetPath = $request->get('_target_path') ?: null;

        if (null === $targetPath) {
            $targetPath = $this->getTargetPath($request->getSession(), $providerKey);
        }

        if (null === $targetPath) {
            $targetPath = $this->router->generate('app.frontend.account.dashboard', [
                'host' => $request->get('host'),
            ]);
        } else {
            $targetPath = $this->urlReader->getCleanUrl($targetPath);
        }

        // Recover-procedure afronden
        if ($customer->getStatus()->is(CustomerStatus::RECOVER)) {
            $this->commandBus->handle(CompleteRecoveryCommand::create($customer->getId()));
        }

        return new RedirectResponse($targetPath);
    }

    /**
     * {@inheritdoc}
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new RedirectResponse($this->router->generate('app.frontend.account.session.login', [
            'host' => $request->get('host'),
        ]));
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return false;
        }

        return $request->getPathInfo() === $this->router->generate('app.frontend.account.session.login', [
            'host' => $request->get('host'),
        ]);
    }
}
