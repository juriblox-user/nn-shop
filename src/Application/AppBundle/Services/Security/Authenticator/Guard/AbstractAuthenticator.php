<?php

namespace NnShop\Application\AppBundle\Services\Security\Authenticator\Guard;

use Core\SimpleBus\CommandBusAwareInterface;
use Core\SimpleBus\CommandBusAwareTrait;
use NnShop\Application\AppBundle\Services\Security\MagicLink\UrlReader;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Repository\CustomerRepositoryInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

abstract class AbstractAuthenticator extends AbstractGuardAuthenticator implements CommandBusAwareInterface
{
    use CommandBusAwareTrait;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var UrlReader
     */
    protected $urlReader;

    /**
     * Constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     * @param RouterInterface             $router
     * @param UrlReader                   $urlReader
     */
    public function __construct(CustomerRepositoryInterface $customerRepository, RouterInterface $router, UrlReader $urlReader)
    {
        $this->customerRepository = $customerRepository;

        $this->router = $router;
        $this->urlReader = $urlReader;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsRememberMe()
    {
        return false;
    }

    /**
     * @param Customer $customer
     */
    protected function checkAccount(Customer $customer)
    {
    }
}
