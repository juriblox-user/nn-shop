<?php

namespace NnShop\Application\AppBundle\Services\Security\Authenticator\Guard;

use NnShop\Application\AppBundle\Forms\Frontend\Flow\OrderStatusFlow;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Order\OrderAccountRequest;
use NnShop\Application\AppBundle\Forms\Frontend\Type\Order\OrderAccountType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class CustomerCheckoutFormAuthenticator extends CustomerFormAuthenticator
{
    use TargetPathTrait;

    /**
     * {@inheritdoc}
     */
    public function getCredentials(Request $request)
    {
        $formRequest = new OrderAccountRequest(new OrderStatusFlow());

        $form = $this->formFactory->create(OrderAccountType::class, $formRequest);
        $form->handleRequest($request);

        // Laatstgebruikte e-mailadres overnemen
        $request->getSession()->set(Security::LAST_USERNAME, $formRequest->getLoginEmail());

        if (!$form->isValid()) {
            $errors = $form->getErrors(true);

            throw new CustomUserMessageAuthenticationException(count($errors) > 0 ? $errors[0]->getMessage() : 'U kon niet worden aangemeld. Probeer nogmaals, alstublieft.');
        }

        return [
            'email' => $formRequest->getLoginEmail(),
            'password' => $formRequest->getLoginPassword(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return false;
        }

        if ('app.frontend.order' !== $request->attributes->get('_route')) {
            return false;
        }

        if (!$request->request->has('order_account')) {
            return false;
        }

        $formFields = $request->request->get('order_account');

        $supports = $formFields['loginEmail'] ?? null && $formFields['loginPassword'] ?? null;
        if ($supports) {
            $this->saveTargetPath($request->getSession(), 'frontend', $request->getUri());
        }

        return $supports;
    }
}
