<?php

namespace NnShop\Application\AppBundle\Services\Payment;

use Carbon\Carbon;
use Core\Application\CoreBundle\Service\DependencyInjection\ApplicationParameters;
use Core\Application\KernelInterface;
use Core\Common\Exception\DataIntegrityException;
use Core\Common\Exception\Domain\TransformerConversionException;
use Core\Common\Exception\NotImplementedException;
use Core\Common\Exception\SecurityException;
use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Application\AppBundle\Services\UrlGenerator\OrderUrlGenerator;
use NnShop\Domain\Mollie\Data\AllowedPaymentMethod;
use NnShop\Domain\Mollie\Value\TransactionId;
use NnShop\Domain\Orders\Entity\Invoice;
use NnShop\Domain\Orders\Entity\InvoiceItem;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Entity\Payment;
use NnShop\Domain\Orders\Enumeration\PaymentMethod;
use NnShop\Domain\Orders\Enumeration\PaymentStatus;
use NnShop\Domain\Orders\Repository\PaymentRepositoryInterface;
use NnShop\Domain\Orders\Value\PaymentId;
use NnShop\Domain\Translation\Entity\Profile;
use NnShop\Infrastructure\Orders\Transformer\PaymentMethodMollieTransformer;
use Mollie\Api\MollieApiClient;
use Money\Currencies\ISOCurrencies;
use Money\Formatter\DecimalMoneyFormatter;
use Symfony\Component\Routing\RouterInterface;

class MollieGateway
{
    /**
     * @var \Mollie\Api\MollieApiClient
     */
    private $mollie;

    /**
     * @var OrderUrlGenerator
     */
    private $orderUrlGenerator;

    /**
     * @var ApplicationParameters
     */
    private $parameters;

    /**
     * @var PaymentRepositoryInterface
     */
    private $paymentRepository;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * MollieGateway constructor.
     *
     * @param MollieApiClient            $mollie
     * @param PaymentRepositoryInterface $paymentRepository
     * @param OrderUrlGenerator          $orderUrlGenerator
     * @param RouterInterface            $router
     * @param ApplicationParameters      $parameters
     */
    public function __construct(MollieApiClient $mollie, PaymentRepositoryInterface $paymentRepository, OrderUrlGenerator $orderUrlGenerator, RouterInterface $router, ApplicationParameters $parameters)
    {
        $this->mollie = $mollie;
        $this->paymentRepository = $paymentRepository;

        $this->orderUrlGenerator = $orderUrlGenerator;
        $this->router = $router;

        $this->parameters = $parameters;
    }

    /**
     * @param Payment $payment
     *
     * @throws \LogicException
     * @throws \Mollie\Api\Exceptions\ApiException
     *
     * @return string
     */
    public function getPaymentUrl(Payment $payment): string
    {
        if ($payment->getStatus()->isNot(PaymentStatus::PENDING)) {
            throw new \LogicException(sprintf('Payment [%s] no longer has a pending status, which means that it cannot be paid either', $payment->getId()));
        }

        $transaction = $this->getMollie($payment->getInvoice()->getProfile())->payments->get($payment->getTransactionId());
        if (!$transaction->isOpen()) {
            throw new \LogicException(sprintf('The transaction for payment [%s] has been aborted with Mollie, but its status has not yet synced to our local entity. You should create a new payment for this invoice and try again', $payment->getId()));
        }

        return $transaction->getCheckoutUrl();
    }

    /**
     * @param Invoice       $invoice
     * @param PaymentMethod $method
     *
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws TransformerConversionException
     * @throws \Core\Common\Exception\DataIntegrityException
     * @throws \LogicException
     * @throws DataIntegrityException
     * @throws \Mollie\Api\Exceptions\ApiException
     *
     * @return string
     */
    public function getPaymentUrlForInvoice(Invoice $invoice, PaymentMethod $method = null): string
    {
        $payment = $invoice->findPendingPayment();
        if ($invoice->isPaid()) {
            if (null !== $payment) {
                throw new DataIntegrityException(sprintf('Invoice [%s] has been paid but still has pending payments. This means something has gone wrong.', $invoice->getId()));
            }

            throw new \LogicException(sprintf('Invoice [%s] has already been paid, so you cannot request a payment URL', $invoice->getId()));
        }

        if (null === $payment) {
            $payment = $invoice->preparePayment(PaymentId::generate(), $method);
            $payment->setTransactionId($this->prepareTransaction($payment));

            $this->paymentRepository->save($payment);
        }

        return $this->getPaymentUrl($payment);
    }

    /**
     * @param Order $order
     *
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws TransformerConversionException
     * @throws \RuntimeException
     * @throws \LogicException
     * @throws \Exception
     * @throws DataIntegrityException
     * @throws \Exception
     *
     * @return string
     */
    public function getPaymentUrlForOrder(Order $order): string
    {
        if (0 === $order->getInvoiceItems()->count()) {
            throw new \RuntimeException(sprintf('The order [%s] does not have any associated InvoiceItems', $order->getId()));
        }

        /** @var InvoiceItem $invoiceItem */
        $invoiceItem = $order->getInvoiceItems()->last();

        return $this->getPaymentUrlForInvoice($invoiceItem->getInvoice(), $order->getPaymentMethod());
    }

    /**
     * Transactie voorbereiden bij Mollie.
     *
     * @param Payment $payment
     *
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Mollie\Api\Exceptions\ApiException
     *
     * @return TransactionId
     */
    public function prepareTransaction(Payment $payment): TransactionId
    {
        $invoice = $payment->getInvoice();
        $orders = $invoice->getOrders();

        $moneyFormatter = new DecimalMoneyFormatter(new ISOCurrencies());

        $mollieLocales = [
            'en' => 'en_US',
            'nl' => 'nl_NL',
            'fr' => 'fr_BE',
            'es' => 'es_ES',
            'de' => 'de_DE',
        ];

        $locale = null;

        $profile = $invoice->getProfile();

        $profileLocale = $profile->getMainLocale();
        if (isset($mollieLocales[$profileLocale->getLocale()])) {
            $locale = $mollieLocales[$profileLocale->getLocale()];
        }

        $data = [
            'method' => null,
            'locale' => $locale,
            'amount' => [
                'currency' => $profile->getCurrency(),
                'value' => $moneyFormatter->format($payment->getAmount()),
            ],
            'description' => sprintf('Factuur %s', $invoice->getCode()),

            'redirectUrl' => $this->router->generate('app.frontend.homepage', ['host' => $orders[0]->getTemplate()->getPartner()->getCountryProfile()->getHost()], RouterInterface::ABSOLUTE_URL),
            'webhookUrl' => $this->router->generate('app.webhooks.mollie', [], RouterInterface::ABSOLUTE_URL),

            'metadata' => [
                'invoice_id' => $invoice->getId()->getString(),
                'payment_id' => $payment->getId()->getString(),
            ],
        ];

        if (null !== $payment->getMethod()) {
            $data['method'] = PaymentMethodMollieTransformer::toRemote($payment->getMethod());
        }

        /*
         * 1 gekoppelde bestelling
         */
        if (1 == $orders->count()) {
            /** @var Order $order */
            $order = $orders->first();

            $data['redirectUrl'] = $this->orderUrlGenerator->generateStatusUrl($order, true);
            $data['description'] = sprintf('Factuur %s: %s', $invoice->getCode(), $order->getDocument()->getTemplate()->getTitle());

            $data['metadata']['order_id'] = $order->getId()->getString();
            $data['metadata']['document_id'] = $order->getDocument()->getId()->getString();
        }

        /*
         * Meerdere gekoppelde bestellingen
         */
        elseif ($orders->count() > 1) {
            $data['description'] = sprintf('Factuur %s: %d documenten', $invoice->getCode(), $orders->count());
        }

        $transaction = $this->getMollie($payment->getInvoice()->getProfile())->payments->create($data);

        return TransactionId::from($transaction->id);
    }

    /**
     * Gegevens van een betaling synchroniseren met Mollie.
     *
     * @param Payment $payment
     *
     * @throws \Core\Common\Exception\NotImplementedException
     * @throws \InvalidArgumentException
     * @throws \Core\Common\Exception\SecurityException
     * @throws SecurityException
     * @throws NotImplementedException
     * @throws SecurityException
     * @throws \Mollie\Api\Exceptions\ApiException
     */
    public function synchronizePayment(Payment $payment)
    {
        if (null === $payment->getTransactionId()) {
            throw new \InvalidArgumentException(sprintf('The payment [%s] does not have a TransactionId, which means we cannot request its current status with Mollie', $payment->getId()));
        }

        $profile = $payment->getInvoice()->getProfile();

        $transaction = $this->getMollie($payment->getInvoice()->getProfile())->payments->get($payment->getTransactionId()->getString());

        // Environment van de applicatie controleren
        if ('live' !== $transaction->mode && KernelInterface::ENVIRONMENT_PRODUCTION == $this->parameters->getEnvironment()) {
            throw new SecurityException(sprintf('Attempt to spoof a payment notification for Payment [%s]', $payment->getId()), [
                'transaction' => $transaction,
            ]);
        }

        switch ($transaction->status) {
            case \Mollie\Api\Types\PaymentStatus::STATUS_PAID:
                $payment->setStatus(PaymentStatus::from(PaymentStatus::RECEIVED), Carbon::parse($transaction->paidAt, new \DateTimeZone('UTC')));

                break;

            case \Mollie\Api\Types\PaymentStatus::STATUS_EXPIRED:
                $payment->setStatus(PaymentStatus::from(PaymentStatus::EXPIRED), Carbon::parse($transaction->expiresAt, new \DateTimeZone('UTC')));

                break;

            case \Mollie\Api\Types\PaymentStatus::STATUS_FAILED:
                $payment->setStatus(PaymentStatus::from(PaymentStatus::EXPIRED), Carbon::parse($transaction->expiresAt, new \DateTimeZone('UTC')));

                break;

            case \Mollie\Api\Types\PaymentStatus::STATUS_CANCELED:
                $payment->setStatus(PaymentStatus::from(PaymentStatus::CANCELED), Carbon::parse($transaction->canceledAt, new \DateTimeZone('UTC')));

                break;

            case \Mollie\Api\Types\PaymentStatus::STATUS_OPEN:
                break;

            default:
                throw new NotImplementedException(sprintf('The Mollie transaction status "%s" cannot be processed yet', $transaction->status));
        }

        // Betaalmethode bijwerken
        if (null !== $transaction->method) {
            $payment->setMethod(PaymentMethodMollieTransformer::fromRemote($transaction->method));
        }
    }

    /**
     * @param Profile $profile
     * @param $id
     *
     * @throws \Mollie\Api\Exceptions\ApiException
     *
     * @return AllowedPaymentMethod|null
     */
    public function findMollieMethodById(Profile $profile, $id)
    {
        $allowedMethods = $this->getAllowedMethods($profile);

        foreach ($allowedMethods as $allowedMethod) {
            if ($allowedMethod->getId() === $id) {
                return $allowedMethod;
            }
        }

        return null;
    }

    /**
     * @param Profile $profile
     *
     * @throws \Mollie\Api\Exceptions\ApiException
     *
     * @return ArrayCollection
     */
    public function getProfileAllowedMethods(Profile $profile)
    {
        $methods = new ArrayCollection();
        $mollieMethods = $this->getAllowedMethods($profile);
        foreach ($profile->getAllowedPaymentMethods() as $allowedPaymentMethod) {
            $method = $mollieMethods->get($allowedPaymentMethod);
            if ($method) {
                $methods->set($method->getId(), $method);
            }
        }

        return $methods;
    }

    /**
     * @param Profile $profile
     *
     * @throws \Mollie\Api\Exceptions\ApiException
     *
     * @return ArrayCollection
     */
    public function getAllowedMethods(Profile $profile)
    {
        return $this->prepareAllowedMethods($profile);
    }

    /**
     * @param Profile $profile
     *
     * @throws \Mollie\Api\Exceptions\ApiException
     *
     * @return ArrayCollection
     */
    private function prepareAllowedMethods(Profile $profile): ArrayCollection
    {
        $methods = new ArrayCollection();
        $mollieMethodsIds = [];

        $allowedMethods = $profile->getAllowedPaymentMethods();

        // Value 1.00 hardcoded because Mollie needs to get value field for this action. https://docs.mollie.com/reference/v2/methods-api/list-methods
        $mollieMethods = $this->getMollie($profile)->methods->all(['amount' => ['value' => '1.00', 'currency' => $profile->getCurrency()]]);

        foreach ($mollieMethods as $mollieMethod) {
            try {
                $paymentMethod = PaymentMethodMollieTransformer::fromRemote($mollieMethod->id);
                if ($paymentMethod instanceof PaymentMethod) {
                    $mollieMethodsIds[$mollieMethod->id] = $mollieMethod->id;

                    $methods->set(
                        $mollieMethod->id,
                        new AllowedPaymentMethod(
                            $mollieMethod->id,
                            false,
                            in_array($mollieMethod->id, $allowedMethods),
                            $mollieMethod,
                            $paymentMethod
                        )
                    );
                }
            } catch (TransformerConversionException $exception) {
                continue;
            }
        }

        foreach ($allowedMethods as $allowedMethod) {
            if (!array_key_exists($allowedMethod, $mollieMethodsIds)) {
                $methods->set(
                    $allowedMethod,
                    new AllowedPaymentMethod(
                        $allowedMethod,
                        true,
                        false
                    )
                );
            }
        }

        return $methods;
    }

    /**
     * @param Profile $profile
     *
     * @throws \Mollie\Api\Exceptions\ApiException
     *
     * @return MollieApiClient
     */
    private function getMollie(Profile $profile): MollieApiClient
    {
        $this->mollie->setApiKey($profile->getMollieKey());

        return $this->mollie;
    }
}
