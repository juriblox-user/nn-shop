<?php

namespace NnShop\Application\AppBundle\Services\UrlGenerator;

use NnShop\Domain\Orders\Entity\SubscriptionLink;
use NnShop\Domain\Orders\Enumeration\SubscriptionLinkFormat;
use Symfony\Component\Routing\RouterInterface;

class SubscriptionUrlGenerator
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * SubscriptionUrlGenerator constructor.
     *
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param SubscriptionLink $link
     *
     * @return string
     */
    public function generateDownloadUrl(SubscriptionLink $link): string
    {
        return $this->router->generate('app.hosted.download', [
            'token' => $link->getToken(),
        ], RouterInterface::ABSOLUTE_URL);
    }

    /**
     * @param SubscriptionLink $link
     *
     * @return string
     */
    public function generateDirectDownloadUrl(SubscriptionLink $link): string
    {
        $document = $link->getDocument();

        // Markdown
        if ($link->getFormat()->is(SubscriptionLinkFormat::MARKDOWN)) {
            return $this->router->generate('app.frontend.download.stream_markdown', [
                'documentId' => $document->getId(),
                'nonce' => $document->getToken()->getString(),
                'host' => $document->getPartner()->getCountryProfile()->getHost(),
            ], RouterInterface::ABSOLUTE_URL);
        }

        // PDF
        elseif ($link->getFormat()->is(SubscriptionLinkFormat::PDF)) {
            return $this->router->generate('app.frontend.download.stream_pdf', [
                'documentId' => $document->getId(),
                'nonce' => $document->getToken()->getString(),
                'host' => $document->getPartner()->getCountryProfile()->getHost(),
            ], RouterInterface::ABSOLUTE_URL);
        }

        // Word2007
        elseif ($link->getFormat()->is(SubscriptionLinkFormat::WORD2007)) {
            return $this->router->generate('app.frontend.download.stream_word2007', [
                'documentId' => $document->getId(),
                'nonce' => $document->getToken()->getString(),
                'host' => $document->getPartner()->getCountryProfile()->getHost(),
            ], RouterInterface::ABSOLUTE_URL);
        }

        // Onbekend type

        throw new \InvalidArgumentException(sprintf('Unsupported SubscriptionLink format "%s"', $link->getFormat()));
    }
}
