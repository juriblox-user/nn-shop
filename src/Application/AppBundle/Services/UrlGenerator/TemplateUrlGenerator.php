<?php

namespace NnShop\Application\AppBundle\Services\UrlGenerator;

use Core\Application\CoreBundle\Service\DependencyInjection\ApplicationParameters;
use Doctrine\Common\Collections\Criteria;
use NnShop\Application\AppBundle\Services\ShopContext;
use NnShop\Domain\Orders\Value\OrderId;
use NnShop\Domain\Shops\Entity\Referrer;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Translation\Entity\Profile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class TemplateUrlGenerator
{
    /**
     * @var ShopContext
     */
    private $context;

    /**
     * @var ApplicationParameters
     */
    private $parameters;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var Request
     */
    private $request;

    /**
     * TemplateUrlGenerator constructor.
     *
     * @param RouterInterface       $router
     * @param ShopContext           $context
     * @param ApplicationParameters $parameters
     * @param RequestStack $requestStack
     */
    public function __construct(RouterInterface $router, ShopContext $context, ApplicationParameters $parameters, RequestStack $requestStack)
    {
        $this->router = $router;
        $this->context = $context;

        $this->parameters = $parameters;
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @param Template      $template
     * @param Referrer|null $referrer
     *
     * @return string
     */
    public function generateAbsoluteUrl(Template $template, Referrer $referrer = null): string
    {
        $category = $template->getPrimaryCategory();

        $context = $this->router->getContext();
        $context->setHost($this->request->get('_profile')->getHost());
        $context->setScheme($this->parameters->getHostScheme());

        $parameters = [
            'categorySlug' => $category->getSlug(),
            'templateSlug' => $template->getSlug(),
            'host' => $this->request->get('_profile')->getHost()
        ];

        if (null !== $referrer) {
            $parameters['r'] = $referrer->getSlug();
        }

        return $this->router->generate('app.frontend.templates.details', $parameters, RouterInterface::ABSOLUTE_URL);
    }

    /**
     * Generate a URL to the template.
     *
     * @param Template $template
     * @param Referrer|null $referrer
     * @param Profile|null $profile
     * @param bool $absoluteUrl
     * @param array $parameters
     *
     * @return string
     */
    public function generateDetailsUrl(Template $template, Referrer $referrer = null, Profile $profile = null, $absoluteUrl = false, array $parameters = []): string
    {
        $referenceType = RouterInterface::ABSOLUTE_PATH;

        /*
         * Choose the category/shop combo to link to
         */
        if (null !== $profile) {
            $categories = $template->getCategories();

            if ($categories->isEmpty()) {
                throw new \DomainException(sprintf('The template "%s" [%s] is not linked to the profile "%s" [%s]', $template->getTitle(), $template->getId()->getString(), (string)$profile, $profile->getId()->getString()));
            }

            $category = $categories->first();
        } else {
            $category = $template->getPrimaryCategory();
        }

        /*
         * Use a full URL if we leave our own shop
         */
        if ($absoluteUrl || !$this->context->isShopContext()) {
            $context = $this->router->getContext();

            $context->setHost($this->request->get('_profile')->getHost());
            $context->setScheme($this->parameters->getHostScheme());

            $referenceType = RouterInterface::ABSOLUTE_URL;
        }

        $parameters = array_merge($parameters, [
            'categorySlug' => $category->getSlug(),
            'templateSlug' => $template->getSlug(),
            'host' => $template->getPartner()->getCountryProfile()->getHost(),
        ]);

        if (null !== $referrer) {
            $parameters['r'] = $referrer->getSlug();
        }

        return $this->router->generate('app.frontend.templates.details', $parameters, $referenceType);
    }

    /**
     * Generate a URL to the template's order page.
     *
     * @param Template $template
     * @param OrderId  $orderId
     * @param bool     $absolute
     *
     * @return string
     */
    public function generateOrderUrl(Template $template, OrderId $orderId, $absolute = false): string
    {
        $parameters = [
            'templateSlug' => $template->getSlug(),
            'orderId' => $orderId,
            'host' => $template->getPartner()->getCountryProfile()->getHost(),
        ];

        $referenceType = $absolute || $this->context->isShopContext() ? RouterInterface::ABSOLUTE_URL : RouterInterface::ABSOLUTE_PATH;

        return $this->router->generate('app.frontend.order', $parameters, $referenceType);
    }

    /**
     * Permalink genereren.
     *
     * @param Template $template
     *
     * @return string
     */
    public function generatePermalink(Template $template): string
    {
        $parameters = [
            'templateId' => $template->getId(),
            'host' => $template->getPartner()->getCountryProfile()->getHost(),
        ];

        return $this->router->generate('app.frontend.templates.permalink', $parameters, RouterInterface::ABSOLUTE_URL);
    }

    /**
     * @param Template $template
     * @param Profile|null $profile
     * @return string
     */
    public function generateSampleUrl(Template $template, Profile $profile = null): string
    {
        $referenceType = RouterInterface::ABSOLUTE_PATH;

        /*
         * Choose the category/profile combo to link to
         */
        if (null !== $profile) {
            $categories = $template->getCategories()->matching(
                Criteria::create()->where(
                    Criteria::expr()->eq('profile', $profile)
                )
            );

            if ($categories->isEmpty()) {
                throw new \DomainException(sprintf('The template "%s" [%s] is not linked to the profile "%s" [%s]', $template->getTitle(), $template->getId()->getString(), $shop->getTitle(), $shop->getId()->getString()));
            }

            $category = $categories->first();
        } else {
            $category = $template->getPrimaryCategory();
        }

        /*
         * Use a full URL if we leave our own profile
         */
        if (!$this->context->isShopContext()) {
            $context = $this->router->getContext();

            $context->setHost($category->getprofile()->getHostname());
            $context->setScheme($this->parameters->getHostScheme());

            $referenceType = RouterInterface::ABSOLUTE_URL;
        }

        $parameters = [
            'categorySlug' => $category->getSlug(),
            'templateSlug' => $template->getSlug(),
            'host' => $template->getPartner()->getCountryProfile()->getHost(),
        ];

        return $this->router->generate('app.frontend.templates.sample', $parameters, $referenceType);
    }

    /**
     * Shortlink genereren.
     *
     * @param Template $template
     *
     * @return string
     */
    public function generateShortlink(Template $template): string
    {
        $parameters = [
            'templateKey' => $template->getKey(),
            'host' => $template->getPartner()->getCountryProfile()->getHost(),
        ];

        return $this->router->generate('app.frontend.shortlink', $parameters, RouterInterface::ABSOLUTE_URL);
    }
}
