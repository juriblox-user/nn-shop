<?php

namespace NnShop\Application\AppBundle\Services\UrlGenerator;

use Core\Application\CoreBundle\Service\DependencyInjection\ApplicationParameters;
use NnShop\Domain\Shops\Entity\Shop;
use Symfony\Component\Routing\RouterInterface;

class ShopUrlGenerator
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var ApplicationParameters
     */
    private $parameters;

    /**
     * ShopUrlGenerator constructor.
     *
     * @param RouterInterface       $router
     * @param ApplicationParameters $parameters
     */
    public function __construct(RouterInterface $router, ApplicationParameters $parameters)
    {
        $this->router = $router;
        $this->parameters = $parameters;
    }

    /**
     * URL genereren.
     *
     * @param Shop   $shop
     * @param string $route
     * @param array  $parameters
     *
     * @return string
     */
    public function generate(Shop $shop, $route, $parameters): string
    {
        $context = $this->router->getContext();

        $context->setHost($shop->getHostname());
        $context->setScheme($this->parameters->getHostScheme());

        return $this->router->generate($route, $parameters, RouterInterface::ABSOLUTE_URL);
    }
}
