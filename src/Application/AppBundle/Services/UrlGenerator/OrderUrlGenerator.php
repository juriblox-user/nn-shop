<?php

namespace NnShop\Application\AppBundle\Services\UrlGenerator;

use NnShop\Application\AppBundle\Services\ShopContext;
use NnShop\Domain\Orders\Entity\Order;
use Symfony\Component\Routing\RouterInterface;

class OrderUrlGenerator
{
    /**
     * @var ShopContext
     */
    private $context;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * OrderUrlGenerator constructor.
     *
     * @param RouterInterface $router
     * @param ShopContext     $context
     */
    public function __construct(RouterInterface $router, ShopContext $context)
    {
        $this->router = $router;
        $this->context = $context;
    }

    /**
     * URL genereren naar de statuspagina van een bestelling.
     *
     * @param Order $order
     * @param bool  $absolute Absolute URL genereren, vooral handig voor redirects
     *
     * @return string
     */
    public function generateStatusUrl(Order $order, $absolute = false): string
    {
        $parameters = [
            'orderId' => $order->getId(),
            'host' => $order->getTemplate()->getPartner()->getCountryProfile()->getHost(),
        ];

        $referenceType = $this->context->isShopContext() || $absolute ? RouterInterface::ABSOLUTE_URL : RouterInterface::ABSOLUTE_PATH;

        return $this->router->generate('app.frontend.order.status', $parameters, $referenceType);
    }
}
