<?php

namespace NnShop\Application\AppBundle\Services\UrlGenerator;

use NnShop\Domain\Templates\Entity\Document;
use Symfony\Component\Routing\RouterInterface;

class DocumentUrlGenerator
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * DocumentUrlGenerator constructor.
     *
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param Document $document
     *
     * @return string
     */
    public function generateDownloadMarkdownUrl(Document $document): string
    {
        // TODO: hier een nonce genereren, zie Frontend/Download/Default:download

        return $this->router->generate('app.frontend.download.stream_markdown', [
            'documentId' => $document->getId(),
            'token' => $document->getToken()->getString(),
            'host' => $document->getPartner()->getCountryProfile()->getHost(),
        ], RouterInterface::ABSOLUTE_URL);
    }

    /**
     * @param Document $document
     *
     * @return string
     */
    public function generateDownloadPdfUrl(Document $document): string
    {
        // TODO: hier een nonce genereren, zie Frontend/Download/Default:download

        return $this->router->generate('app.frontend.download.stream_pdf', [
            'documentId' => $document->getId(),
            'token' => $document->getToken()->getString(),
            'host' => $document->getPartner()->getCountryProfile()->getHost(),
        ], RouterInterface::ABSOLUTE_URL);
    }

    /**
     * @param Document $document
     *
     * @return string
     */
    public function generateDownloadWord2007Url(Document $document): string
    {
        // TODO: hier een nonce genereren, zie Frontend/Download/Default:download

        return $this->router->generate('app.frontend.download.stream_word2007', [
            'documentId' => $document->getId(),
            'token' => $document->getToken()->getString(),
            'host' => $document->getPartner()->getCountryProfile()->getHost(),
        ], RouterInterface::ABSOLUTE_URL);
    }
}
