<?php

namespace NnShop\Application\AppBundle\Services\UrlGenerator;

use NnShop\Application\AppBundle\Services\Payment\MollieGateway;
use NnShop\Domain\Orders\Entity\Invoice;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Entity\Payment;
use Symfony\Component\Routing\RouterInterface;

class PaymentUrlGenerator
{
    /**
     * @var MollieGateway
     */
    private $mollie;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * PaymentUrlGenerator constructor.
     *
     * @param RouterInterface $router
     * @param MollieGateway   $mollie
     */
    public function __construct(RouterInterface $router, MollieGateway $mollie)
    {
        $this->router = $router;
        $this->mollie = $mollie;
    }

    /**
     * @param Invoice $invoice
     * @param bool    $absolute
     *
     * @return string
     */
    public function generateInvoicePaymentUrl(Invoice $invoice, $absolute = false): string
    {
        $referenceType = $absolute ? RouterInterface::ABSOLUTE_URL : RouterInterface::ABSOLUTE_PATH;
        $orders = $invoice->getOrders();

        return $this->router->generate('app.frontend.payment', [
            'invoiceId' => $invoice->getId(),
            'host' => $orders[0]->getTemplate()->getPartner()->getCountryProfile()->getHost(),
        ], $referenceType);
    }

    /**
     * Mollie-status URL genereren.
     *
     * @param Payment $payment
     *
     * @return string
     */
    public function generateMollieStatusUrl(Payment $payment): string
    {
        return sprintf('https://www.mollie.com/dashboard/payments/%s', $payment->getTransactionId());
    }

    /**
     * @param Order $order
     * @param bool  $absolute
     *
     * @return string
     */
    public function generateOrderPaymentUrl(Order $order, $absolute = false): string
    {
        return $this->router->generate('app.frontend.payment', [
            'invoiceId' => $order->getInvoice()->getId(),
            'orderId' => $order->getId(),
            'host' => $order->getTemplate()->getPartner()->getCountryProfile()->getHost(),
        ], $absolute);
    }
}
