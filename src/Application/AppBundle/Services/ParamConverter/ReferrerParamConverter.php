<?php

namespace NnShop\Application\AppBundle\Services\ParamConverter;

use NnShop\Application\AppBundle\Services\Context\ReferrerContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class ReferrerParamConverter implements ParamConverterInterface
{
    /**
     * @var ReferrerContext
     */
    private $referrerContext;

    /**
     * Constructor.
     *
     * @param ReferrerContext $referrerContext
     */
    public function __construct(ReferrerContext $referrerContext)
    {
        $this->referrerContext = $referrerContext;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $request->attributes->set($configuration->getName(), $this->referrerContext);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(ParamConverter $configuration)
    {
        return ReferrerContext::class === $configuration->getClass();
    }
}
