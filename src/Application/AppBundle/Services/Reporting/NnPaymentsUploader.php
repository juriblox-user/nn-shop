<?php

namespace NnShop\Application\AppBundle\Services\Reporting;

use NnShop\Application\AppBundle\Services\Exporter\NnPaymentsExporter;
use Gaufrette\Filesystem;

class NnPaymentsUploader
{
    private $filesystem;

    private $paymentsExporter;

    public function __construct(Filesystem $filesystem, NnPaymentsExporter $paymentsExporter)
    {
        $this->filesystem = $filesystem;
        $this->paymentsExporter = $paymentsExporter;
    }

    public function upload()
    {
        $this->filesystem->write(sprintf('%s_nn_csvexport_payments.csv', date('Ymd')), $this->paymentsExporter->getRaw());
    }
}
