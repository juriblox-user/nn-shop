<?php

namespace NnShop\Application\AppBundle\Services\Reporting;

use NnShop\Application\AppBundle\Services\Exporter\NnOrdersExporter;
use Gaufrette\Filesystem;

class NnOrdersUploader
{
    private $filesystem;

    private $ordersExporter;

    public function __construct(Filesystem $filesystem, NnOrdersExporter $ordersExporter)
    {
        $this->filesystem = $filesystem;
        $this->ordersExporter = $ordersExporter;
    }

    public function upload()
    {
        $this->filesystem->write(sprintf('%s_nn_csvexport_orders.csv', date('Ymd')), $this->ordersExporter->getRaw());
    }
}
