<?php

namespace NnShop\Application\AppBundle\Services\Reporting;

use NnShop\Application\AppBundle\Services\Exporter\NnInvoicesExporter;
use Gaufrette\Filesystem;

class NnInvoicesUploader
{
    private $filesystem;

    private $invoicesExporter;

    public function __construct(Filesystem $filesystem, NnInvoicesExporter $invoicesExporter)
    {
        $this->filesystem = $filesystem;
        $this->invoicesExporter = $invoicesExporter;
    }

    public function upload()
    {
        $this->filesystem->write(sprintf('%s_nn_csvexport_invoices.csv', date('Ymd')), $this->invoicesExporter->getRaw());
    }
}
