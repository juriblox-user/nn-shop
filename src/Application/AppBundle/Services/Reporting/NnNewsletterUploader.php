<?php

namespace NnShop\Application\AppBundle\Services\Reporting;

use NnShop\Application\AppBundle\Services\Exporter\NnNewsletterExporter;
use Gaufrette\Filesystem;

class NnNewsletterUploader
{
    private $filesystem;

    private $newsletterExporter;

    public function __construct(Filesystem $filesystem, NnNewsletterExporter $newsletterExporter)
    {
        $this->filesystem = $filesystem;
        $this->newsletterExporter = $newsletterExporter;
    }

    public function upload()
    {
        $this->filesystem->write(sprintf('%s_nn_csvexport_newsletter.csv', date('Ymd')), $this->newsletterExporter->getRaw());
    }
}
