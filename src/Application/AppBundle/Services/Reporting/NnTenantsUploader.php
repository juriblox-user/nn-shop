<?php

namespace NnShop\Application\AppBundle\Services\Reporting;

use NnShop\Application\AppBundle\Services\Exporter\NnTenantsExporter;
use Gaufrette\Filesystem;

class NnTenantsUploader
{
    private $filesystem;

    private $tenantsExporter;

    public function __construct(Filesystem $filesystem, NnTenantsExporter $tenantsExporter)
    {
        $this->filesystem = $filesystem;
        $this->tenantsExporter = $tenantsExporter;
    }

    public function upload()
    {
        $this->filesystem->write(sprintf('%s_nn_csvexport_tenants.csv', date('Ymd')), $this->tenantsExporter->getRaw());
    }
}
