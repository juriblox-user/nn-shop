<?php

namespace NnShop\Application\AppBundle\Services\Context;

use NnShop\Domain\Shops\Entity\Referrer;
use NnShop\Domain\Shops\Repository\ReferrerRepositoryInterface;
use NnShop\Domain\Shops\Value\ReferrerId;

class ReferrerContext
{
    /**
     * @var Referrer|null
     */
    private $referrer;

    /**
     * @var ReferrerRepositoryInterface
     */
    private $referrerRepository;

    /**
     * Constructor.
     *
     * @param ReferrerRepositoryInterface $referrerRepository
     */
    public function __construct(ReferrerRepositoryInterface $referrerRepository)
    {
        $this->referrerRepository = $referrerRepository;
    }

    /**
     * @return Referrer|null
     */
    public function getReferrer()
    {
        return $this->referrer;
    }

    /**
     * @return bool
     */
    public function hasReferrer(): bool
    {
        return null !== $this->referrer;
    }

    /**
     * @param ReferrerId $id
     */
    public function loadId(ReferrerId $id)
    {
        $this->referrer = $this->referrerRepository->getById($id);
    }

    /**
     * @param string $slug
     */
    public function loadSlug(string $slug)
    {
        $this->referrer = $this->referrerRepository->getBySlug($slug);
    }

    /**
     * @param Referrer $referrer
     */
    public function setReferrer(Referrer $referrer)
    {
        $this->referrer = $referrer;
    }
}
