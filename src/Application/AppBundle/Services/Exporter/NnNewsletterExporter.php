<?php

namespace NnShop\Application\AppBundle\Services\Exporter;

use Carbon\Carbon;
use Core\Component\Filesystem\TempFile;
use NnShop\Domain\Orders\Entity\Customer;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Writer;

class NnNewsletterExporter
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getPath()
    {
        $tempFile = TempFile::getTempFile();

        $csv = Writer::createFromPath($tempFile, 'w');
        $csv->insertOne([
            'Datum',
            'Niewsbrief',
            'E-mail',
            'Voornaam',
            'Achternaam',
            'Bedrijfsnaam'
        ]);

        $builder = $this->entityManager->createQueryBuilder();

        $builder
            ->select('c', 'o')
            ->from(Customer::class, 'c')
            ->join('c.orders', 'o')
        ;

        $query = $builder->getQuery();

        foreach ($query->getResult(AbstractQuery::HYDRATE_ARRAY) as $row) {
            $firstStartedTimestamp = null;

            foreach ($row['orders'] as $order) {
                $started = Carbon::make($order['timestampStarted']);

                if (null == $firstStartedTimestamp || $started->lt($firstStartedTimestamp)) {
                    $firstStartedTimestamp = $started;
                }
            }

            $columns = [
                $firstStartedTimestamp ? $firstStartedTimestamp : 'onbekend',
                $row['newsletter'] ? 'ja' : 'nee',
                $row['email'],
                $row['firstname'],
                $row['lastname'],
                $row['companyName']
            ];

            $csv->insertOne($columns);
        }

        clearstatcache(false, $tempFile);

        return $tempFile;
    }

    public function getRaw() {
        return file_get_contents($this->getPath());
    }
}
