<?php

namespace NnShop\Application\AppBundle\Services\Exporter;

use Core\Component\Filesystem\TempFile;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Enumeration\InvoiceStatus;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Writer;
use Money\Currencies\ISOCurrencies;
use Money\Formatter\DecimalMoneyFormatter;

class NnOrdersExporter
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getPath()
    {
        $tempFile = TempFile::getTempFile();

        $csv = Writer::createFromPath($tempFile, 'w');
        $csv->insertOne([
            'Business Unit',
            'Systeemnummer',
            'Relatienummer',
            'COMP Id',
            'KvK-nummer',
            'KvK-vestigingsnummer',
            'Contractnummer',
            'Contract soort',
            'Contractcode',
            'Contractnaam',
            'Begindatum',
            'Einddatum',
            'Status',
            'Datum status',
            'Bedrag',
            'Aantal units',
            'Voorletters contactpersoon',
            'Tussevoegsel',
            'Achternaam',
            'Geslacht contactpersoon',
            'Emailadres contactpersoon',
            'Telefoon contactpersoon',
            'Rol/functie contactpersoon',
            'Datum no mail'
        ]);

        $builder = $this->entityManager->createQueryBuilder();

        $builder
            ->select('o', 'c', 'i', 'ii')
            ->from(Order::class, 'o')
            ->join('o.customer', 'c')
            ->join('o.invoiceItems', 'ii')
            ->join('ii.invoice', 'i')
            ->where($builder->expr()->eq('i.status', ':status'))
            ->setParameter('status', InvoiceStatus::PAID);

        $query = $builder->getQuery();

        $formatter = new DecimalMoneyFormatter(new ISOCurrencies());

        foreach ($query->getResult(AbstractQuery::HYDRATE_ARRAY) as $row) {
            $columns = [
                1007,
                28,
                $row['customer']['id']->getString(),
                NULL,
                $row['customer']['cocNumber'],
                NULL,
                $row['id']->getString(),
                3,
                sprintf('%0.2f', 8.50),
                $row['invoiceItems'][0]['title'],
                $row['invoiceItems'][0]['invoice']['timestampPaid']->format('d-m-Y'),
                NULL,
                99,
                $row['timestampDelivered'] ? $row['timestampDelivered']->format('d-m-Y H:i:s') : '',
                $row['invoiceItems'][0]['invoice']['subtotal']->getAmount(),
                NULL,
                $row['customer']['firstname'],
                NULL,
                $row['customer']['lastname'],
                NULL,
                $row['customer']['email']->getString(),
                $row['customer']['phone'],
                NULL,
                NULL

            ];

            $csv->insertOne($columns);
        }

        clearstatcache(false, $tempFile);

        return $tempFile;
    }

    public function getRaw(): string
    {
        return file_get_contents($this->getPath());
    }
}
