<?php

namespace NnShop\Application\AppBundle\Services\Exporter;

use Core\Component\Filesystem\TempFile;
use NnShop\Domain\Orders\Entity\Customer;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Writer;

class NnTenantsExporter
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getPath()
    {
        $tempFile = TempFile::getTempFile();

        $csv = Writer::createFromPath($tempFile, 'w');
        $csv->insertOne([
            'Business Unit',
            'Systeemnummer',
            'Relatienummer',
            'COMP Id',
            'KvK-nummer',
            'KvK-vestigingsnummer',
            'RSIN-nummer',
            'Bedrijfsnaam',
            'Rechtsvorm',
            'Klantsegment',
            'Klantstaus',
            'Blacklist',
            'Webadres',
            'Emailadres',
            '1e telefoonnummer',
            '2e telefoonnummer',
            'Faxnummer',
            'Straatnaam',
            'Huisnummer',
            'Toevoeging postadres',
            'Extra regel postadres',
            'Postcode postadres',
            'Plaats postadres',
            'Land postadres',
            'Straatnaam vestiginsgadres',
            'Huisnummer vestiginsgadres',
            'Toevoeging vestiginsgadres',
            'Extra regel vestiginsgadres',
            'Postcode vestiginsgadres',
            'Plaats vestiginsgadres',
            'Land vestiginsgadres'
        ]);

        $builder = $this->entityManager->createQueryBuilder();

        $builder
            ->select('c')
            ->from(Customer::class, 'c')
        ;

        $query = $builder->getQuery();

        foreach ($query->getResult(AbstractQuery::HYDRATE_ARRAY) as $row) {
            $columns = [
                1007,
                28,
                $row['id']->getString(),
                '',
                $row['cocNumber'],
                '',
                '',
                $row['companyName'],
                '',
                '',
                4,
                '',
                '',
                $row['email'],
                $row['phone'],
                '',
                '',
                $row['address.street'],
                $row['address.number.number'],
                $row['address.number.addition'],
                '',
                $row['address.zipcode'],
                $row['address.city'],
                $row['address.country.code'],
                $row['address.street'],
                $row['address.number.number'],
                $row['address.number.addition'],
                '',
                $row['address.zipcode'],
                $row['address.city'],
                $row['address.country.code']
            ];

            $csv->insertOne($columns);
        }

        clearstatcache(false, $tempFile);

        return $tempFile;
    }

    public function getRaw(): string
    {
        return file_get_contents($this->getPath());
    }
}













