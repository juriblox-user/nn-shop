<?php

namespace NnShop\Application\AppBundle\Services\Exporter;

use Core\Component\Filesystem\TempFile;
use NnShop\Domain\Orders\Entity\Payment;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Writer;
use Money\Currencies\ISOCurrencies;
use Money\Formatter\DecimalMoneyFormatter;

class NnPaymentsExporter
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getPath()
    {
        $tempFile = TempFile::getTempFile();

        $formatter = new DecimalMoneyFormatter(new ISOCurrencies());

        $csv = Writer::createFromPath($tempFile, 'w');
        $csv->insertOne([
            'Business Unit',
            'Systeemnummer',
            'Factuurnummer',
            'Betaaldatum',
            'Betaalde bedrag'
        ]);

        $builder = $this->entityManager->createQueryBuilder();

        $builder
            ->select('p', 'i')
            ->from(Payment::class, 'p')
            ->join('p.invoice', 'i')
       ;

        $query = $builder->getQuery();

        foreach ($query->getResult(AbstractQuery::HYDRATE_ARRAY) as $row) {

            $columns = [
                1007,
                28,
                $row['invoice']['id']->getString(),
                $row['invoice']['timestampPaid'] ? $row['invoice']['timestampPaid']->format('d-m-Y H:i:s'): '',
                $row['amount'] ? $row['amount']->getAmount() : '',
            ];

            $csv->insertOne($columns);
        }

        clearstatcache(false, $tempFile);

        return $tempFile;
    }

    public function getRaw(): string
    {
        return file_get_contents($this->getPath());
    }
}
