<?php

namespace NnShop\Application\AppBundle\Services\Exporter;

use Core\Component\Filesystem\TempFile;
use NnShop\Domain\Orders\Entity\Invoice;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Writer;
use Money\Currencies\ISOCurrencies;
use Money\Formatter\DecimalMoneyFormatter;

class NnInvoicesExporter
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getPath()
    {
        $tempFile = TempFile::getTempFile();

        $formatter = new DecimalMoneyFormatter(new ISOCurrencies());

        $csv = Writer::createFromPath($tempFile, 'w');
        $csv->insertOne([
            'Business Unit',
            'Systeemnummer',
            'Relatienummer',
            'Systeemnummer Relatienr',
            'Contractnummer',
            'Systeemnummer Contractnr',
            'Dossiernummer',
            'Systeemnummer Dossiernr',
            'Factuurnummer',
            'Factuurdatum',
            'Factuur Omzetbedrag',
            'Factuur Doorbelastekosten',
            'Factuur voorschot',
            'Hoofdsom',
            'Kosten voor eigen rekening',
            'BTW bedrag/assurantiebelasting',
            'Factuur-totaalbedrag'
        ]);

        $builder = $this->entityManager->createQueryBuilder();

        $builder
            ->select('i', 'c', 'it', 'o')
            ->from(Invoice::class, 'i')
            ->join('i.customer', 'c')
            ->join('i.items', 'it')
            ->join('it.order', 'o')
        ;

        $query = $builder->getQuery();

        foreach ($query->getResult(AbstractQuery::HYDRATE_ARRAY) as $row) {
            $columns = [
                1007,
                28,
                $row['customer']['id']->getString(),
                28,
                $row['items'][0]['order']['id']->getString(), //TODO order id ophalen
                28,
                NULL,
                NULL,
                $row['id']->getString(),
                $row['timestampIssued'] ? $row['timestampIssued']->format('d-m-Y') : '',
                $row['subtotal'] ? $row['subtotal']->getAmount() : '',
                0,
                0,
                0,
                $row['vatAmount'] ? $row['vatAmount']->getAmount(): '',
                $row['total']->getAmount(),
            ];

            $csv->insertOne($columns);
        }

        clearstatcache(false, $tempFile);

        return $tempFile;
    }

    public function getRaw(): string
    {
        return file_get_contents($this->getPath());
    }
}
