<?php

namespace NnShop\Application\AppBundle\Extension\Twig;

use NnShop\Application\AppBundle\Forms\Backend\Request\Blog\DocumentRequest;
use NnShop\Application\AppBundle\Services\Manager\BlogDocumentFileManager;
use NnShop\Domain\Blog\Entity\Document;

/**
 * Class DocumentFileExtension.
 */
class DocumentFileExtension extends \Twig_Extension
{
    /**
     * @var BlogDocumentFileManager
     */
    private $manager;

    /**
     * DocumentFileExtension constructor.
     *
     * @param BlogDocumentFileManager $manager
     */
    public function __construct(BlogDocumentFileManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('documentFileUrl', [$this, 'getDocumentFileUrl']),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'document_file';
    }

    /**
     * @param Document $document
     *
     * @throws \RuntimeException
     * @throws \Gaufrette\Exception\FileNotFound
     * @throws \InvalidArgumentException
     *
     * @return string|null
     */
    public function getDocumentFileUrl($document)
    {
        if (!($document instanceof Document || $document instanceof DocumentRequest)) {
            throw new \InvalidArgumentException(sprintf('only objects of Author or EditAuthorRequest or CreateAuthorRequest are allowed'));
        }

        if (null === $document->getFile()) {
            throw new \RuntimeException('The document does not have a file');
        }

        return $this->manager->getUrl($document);
    }
}
