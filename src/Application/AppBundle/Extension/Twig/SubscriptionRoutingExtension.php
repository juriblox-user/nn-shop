<?php

namespace NnShop\Application\AppBundle\Extension\Twig;

use NnShop\Application\AppBundle\Services\UrlGenerator\SubscriptionUrlGenerator;
use NnShop\Domain\Orders\Entity\SubscriptionLink;
use Symfony\Bridge\Twig\Extension\RoutingExtension;

class SubscriptionRoutingExtension extends \Twig_Extension
{
    /**
     * @var SubscriptionUrlGenerator
     */
    private $generator;

    /**
     * @var RoutingExtension
     */
    private $routing;

    /**
     * SubscriptionRoutingExtension constructor.
     *
     * @param SubscriptionUrlGenerator $generator
     * @param RoutingExtension         $routing
     */
    public function __construct(SubscriptionUrlGenerator $generator, RoutingExtension $routing)
    {
        $this->generator = $generator;
        $this->routing = $routing;
    }

    /**
     * @param SubscriptionLink $link
     *
     * @return string
     */
    public function getDownloadUrl(SubscriptionLink $link)
    {
        return $this->generator->generateDownloadUrl($link);
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('subscriptionDownloadUrl', [$this, 'getDownloadUrl'], [
                'is_safe_callback' => [
                    $this->routing,
                    'isUrlGenerationSafe',
                ],
            ]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'subscription_routing';
    }
}
