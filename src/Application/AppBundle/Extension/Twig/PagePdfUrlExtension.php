<?php

namespace NnShop\Application\AppBundle\Extension\Twig;

use NnShop\Application\AppBundle\Services\Manager\PagePdfFileManager;
use NnShop\Domain\Pages\Entity\Page;

/**
 * Class PagePdfUrlExtension.
 */
class PagePdfUrlExtension extends \Twig_Extension
{
    /**
     * @var PagePdfFileManager
     */
    private $manager;

    /**
     * PagePdfUrlExtension constructor.
     *
     * @param PagePdfFileManager $manager
     */
    public function __construct(PagePdfFileManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('pagePdfUrl', [$this, 'getPdfUrl']),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'page_pdf';
    }

    /**
     * @param Page $page
     *
     * @throws \Gaufrette\Exception\FileNotFound
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     *
     * @return string|null
     */
    public function getPdfUrl($page)
    {
        if (null === $page->getPdf()) {
            throw new \RuntimeException('The page does not have a pdf');
        }

        return $this->manager->getUrl($page);
    }
}
