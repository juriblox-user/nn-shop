<?php

namespace NnShop\Application\AppBundle\Extension\Twig;

use NnShop\Application\AppBundle\Services\Manager\ReferrerLogoManager;
use NnShop\Domain\Shops\Entity\Referrer;

class ReferrerLogoExtension extends \Twig_Extension
{
    /**
     * @var ReferrerLogoManager
     */
    private $manager;

    /**
     * @param ReferrerLogoManager $manager
     */
    public function __construct(ReferrerLogoManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('referrerLogoUrl', [$this, 'getReferrerLogoUrl']),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'referrer_logo';
    }

    /**
     * @param Referrer $referrer
     * @param string   $size
     *
     * @return string
     */
    public function getReferrerLogoUrl(Referrer $referrer, string $size = ReferrerLogoManager::SIZE_DEFAULT): string
    {
        return $this->manager->getLogoUrl($referrer, $size);
    }
}
