<?php

namespace NnShop\Application\AppBundle\Extension\Twig;

use NnShop\Application\AppBundle\Services\UrlGenerator\ShopUrlGenerator;
use NnShop\Domain\Shops\Entity\Shop;
use Symfony\Bridge\Twig\Extension\RoutingExtension;

class ShopRoutingExtension extends \Twig_Extension
{
    /**
     * @var ShopUrlGenerator
     */
    private $generator;

    /**
     * @var RoutingExtension
     */
    private $routing;

    /**
     * ShopRoutingExtension constructor.
     *
     * @param ShopUrlGenerator $generator
     * @param RoutingExtension $routing
     */
    public function __construct(ShopUrlGenerator $generator, RoutingExtension $routing)
    {
        $this->generator = $generator;
        $this->routing = $routing;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('shopUrl', [$this, 'getUrl'], [
                'is_safe_callback' => [
                    $this->routing,
                    'isUrlGenerationSafe',
                ],
            ]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'shop_routing';
    }

    /**
     * URL van shop ophalen.
     *
     * @param Shop   $shop
     * @param string $route
     * @param array  $parameters
     *
     * @return string
     */
    public function getUrl(Shop $shop, $route = 'app.shop', array $parameters = [])
    {
        return $this->generator->generate($shop, $route, $parameters);
    }
}
