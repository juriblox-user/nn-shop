<?php

namespace NnShop\Application\AppBundle\Extension\Twig;

use NnShop\Application\AppBundle\Services\Manager\PartnerLogoManager;
use NnShop\Domain\Templates\Entity\Partner;

class PartnerLogoExtension extends \Twig_Extension
{
    /**
     * @var PartnerLogoManager
     */
    private $manager;

    /**
     * PartnerLogoExtension constructor.
     *
     * @param PartnerLogoManager $manager
     */
    public function __construct(PartnerLogoManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('partnerLogoUrl', [$this, 'getPartnerLogoUrl']),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'partner_logo';
    }

    /**
     * @param Partner $partner
     * @param string  $size
     *
     * @return string
     */
    public function getPartnerLogoUrl(Partner $partner, string $size = PartnerLogoManager::SIZE_DEFAULT): string
    {
        if (null === $partner->getLogo()) {
            throw new \RuntimeException('The partner does not have a logo');
        }

        return $this->manager->getLogoUrl($partner, $size);
    }
}
