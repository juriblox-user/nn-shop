<?php

namespace NnShop\Application\AppBundle\Extension\Twig;

use NnShop\Application\AppBundle\Services\ShopContext;
use NnShop\Application\AppBundle\Services\UrlGenerator\ShopUrlGenerator;

class ShopContextExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    /**
     * @var ShopContext
     */
    private $context;

    /**
     * @var ShopUrlGenerator
     */
    private $generator;

    /**
     * ShopContextExtension constructor.
     *
     * @param ShopContext      $context
     * @param ShopUrlGenerator $generator
     */
    public function __construct(ShopContext $context, ShopUrlGenerator $generator)
    {
        $this->context = $context;
        $this->generator = $generator;
    }

    /**
     * {@inheritdoc}
     */
    public function getGlobals(): array
    {
        return [
            'context' => $this->context,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'shop_context_extension';
    }
}
