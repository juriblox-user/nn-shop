<?php


namespace NnShop\Application\AppBundle\Extension\Twig;


use NnShop\Application\AppBundle\Services\Manager\TemplatePreviewManager;
use NnShop\Domain\Templates\Entity\Template;
use http\Exception\RuntimeException;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TemplatePreviewExtension extends AbstractExtension
{
    /**
     * @var TemplatePreviewManager
     */
    private $manager;
    /**
     * TemplatePreviewExtension constructor.
     * @param TemplatePreviewManager $manager
     */
    public function __construct(TemplatePreviewManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('templatePreviewUrl', [$this, 'getTemplatePreviewUrl']),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return 'template_preview';
    }

    /**
     * @param Template $template
     * @param string $size
     *
     * @return string
     */
    public function getTemplatePreviewUrl(Template $template, string $size = TemplatePreviewManager::SIZE_DEFAULT): string
    {
        if (null === $template->getPreview()) {
            throw new RuntimeException('The template does not have a preview');
        }

        return $this->manager->getPreviewUrl($template, $size);
    }
}
