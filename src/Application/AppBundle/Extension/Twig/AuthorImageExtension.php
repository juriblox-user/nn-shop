<?php

namespace NnShop\Application\AppBundle\Extension\Twig;

use NnShop\Application\AppBundle\Forms\Backend\Request\Blog\CreateAuthorRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Blog\EditAuthorRequest;
use NnShop\Application\AppBundle\Services\Manager\AuthorImageManager;
use NnShop\Domain\Blog\Entity\Author;

/**
 * Class AuthorImageExtension.
 */
class AuthorImageExtension extends \Twig_Extension
{
    /**
     * @var AuthorImageManager
     */
    private $manager;

    /**
     * AuthorImageExtension constructor.
     *
     * @param AuthorImageManager $manager
     */
    public function __construct(AuthorImageManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('authorImageUrl', [$this, 'getAuthorImageUrl']),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'author_image';
    }

    /**
     * @param Author $author
     * @param string $size
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     *
     * @return string
     */
    public function getAuthorImageUrl($author, string $size = AuthorImageManager::SIZE_DEFAULT): string
    {
        if (!($author instanceof Author || $author instanceof  EditAuthorRequest || $author instanceof CreateAuthorRequest)) {
            throw new \InvalidArgumentException(sprintf('only objects of Author or EditAuthorRequest or CreateAuthorRequest are allowed'));
        }

        if (null === $author->getImage()) {
            throw new \RuntimeException('The author does not have an image');
        }

        return $this->manager->getImageUrl($author, $size);
    }
}
