<?php

namespace NnShop\Application\AppBundle\Extension\Twig;

use NnShop\Application\AppBundle\Forms\Backend\Request\Blog\CreateArticleRequest;
use NnShop\Application\AppBundle\Forms\Backend\Request\Blog\EditArticleRequest;
use NnShop\Application\AppBundle\Services\Manager\ArticleImageManager;
use NnShop\Domain\Blog\Entity\Article;

/**
 * Class ArticleImageExtension.
 */
class ArticleImageExtension extends \Twig_Extension
{
    /**
     * @var ArticleImageManager
     */
    private $manager;

    /**
     * ArticleImageExtension constructor.
     *
     * @param ArticleImageManager $manager
     */
    public function __construct(ArticleImageManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('articleImageUrl', [$this, 'getArticleImageUrl']),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'article_image';
    }

    /**
     * @param Article $article
     * @param string  $size
     *
     * @throws \InvalidArgumentException
     * @throws \Exception
     * @throws \RuntimeException
     *
     * @return string
     */
    public function getArticleImageUrl($article, string $size = ArticleImageManager::SIZE_DEFAULT): string
    {
        if (!($article instanceof Article || $article instanceof  EditArticleRequest || $article instanceof CreateArticleRequest)) {
            throw new \InvalidArgumentException(sprintf('only objects of Article|EditArticleRequest|CreateArticleRequest are allowed'));
        }

        if (null === $article->getImage()) {
            throw new \RuntimeException('The article does not have an image');
        }

        return $this->manager->getImageUrl($article, $size);
    }
}
