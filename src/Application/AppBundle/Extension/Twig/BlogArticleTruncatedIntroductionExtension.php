<?php

namespace NnShop\Application\AppBundle\Extension\Twig;

use NnShop\Domain\Blog\Entity\Article;

/**
 * Class BlogArticleTruncatedIntroductionExtension.
 */
class BlogArticleTruncatedIntroductionExtension extends \Twig_Extension
{
    /**
     * {@inheritdoc}
     */
    public function getFilters(): array
    {
        return [
            new \Twig_SimpleFilter('truncatedArticleIntroduction', [$this, 'truncate']),
        ];
    }

    /**
     * @param Article $article
     * @param int     $length
     *
     * @return string
     */
    public function truncate(Article $article, int $length): string
    {
        if (strip_tags($article->getIntroduction())) {
            $truncated = $article->getIntroduction();
        } else {
            $truncated = preg_replace('/\s+?(\S+)?$/', '', mb_substr($article->getContent(), 0, 450));
        }

        return $truncated . '...';
    }
}
