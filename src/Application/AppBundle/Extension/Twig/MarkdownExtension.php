<?php

namespace NnShop\Application\AppBundle\Extension\Twig;

use cebe\markdown\Parser;

class MarkdownExtension extends \Twig_Extension
{
    /**
     * @var Parser
     */
    private $parser;

    /**
     * MarkdownExtension constructor.
     *
     * @param Parser $parser
     */
    public function __construct(Parser $parser)
    {
        $this->parser = $parser;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('markdown', [$this, 'markdownFilter'], [
                'is_safe' => ['html'],
            ]),
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'markdown';
    }

    /**
     * @param $plain
     *
     * @return string
     */
    public function markdownFilter($plain)
    {
        return $this->parser->parse($plain);
    }
}
