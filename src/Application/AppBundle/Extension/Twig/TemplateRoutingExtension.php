<?php

namespace NnShop\Application\AppBundle\Extension\Twig;

use NnShop\Application\AppBundle\Services\UrlGenerator\TemplateUrlGenerator;
use NnShop\Domain\Shops\Entity\Referrer;
use NnShop\Domain\Shops\Entity\Shop;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Translation\Entity\Profile;
use Symfony\Bridge\Twig\Extension\RoutingExtension;

class TemplateRoutingExtension extends \Twig_Extension
{
    /**
     * @var TemplateUrlGenerator
     */
    private $generator;

    /**
     * @var RoutingExtension
     */
    private $routing;

    /**
     * TemplateRoutingExtension constructor.
     *
     * @param TemplateUrlGenerator $generator
     * @param RoutingExtension     $routing
     */
    public function __construct(TemplateUrlGenerator $generator, RoutingExtension $routing)
    {
        $this->generator = $generator;
        $this->routing = $routing;
    }

    /**
     * Volledige URL van shop + template ophalen, met domeinnaam..
     *
     * @param Template      $template
     * @param Referrer|null $referrer
     *
     * @return string
     */
    public function getAbsoluteUrl(Template $template, Referrer $referrer = null)
    {
        return $this->generator->generateAbsoluteUrl($template, $referrer);
    }

    /**
     * URL van shop + template ophalen.
     *
     * @param Template $template
     * @param Referrer|null $referrer
     * @param Profile $profile
     * @param bool $absoluteUrl
     * @param array $parameters
     *
     * @return string
     */
    public function getDetailsUrl(Template $template, Referrer $referrer = null, Profile $profile = null, $absoluteUrl = false, array $parameters = [])
    {
        return $this->generator->generateDetailsUrl($template, $referrer, $profile, $absoluteUrl, $parameters);
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('templateAbsoluteUrl', [$this, 'getAbsoluteUrl'], [
                'is_safe_callback' => [
                    $this->routing,
                    'isUrlGenerationSafe',
                ],
            ]),

            new \Twig_SimpleFunction('templatePermalink', [$this, 'getPermalink'], [
                'is_safe_callback' => [
                    $this->routing,
                    'isUrlGenerationSafe',
                ],
            ]),

            new \Twig_SimpleFunction('templateSampleUrl', [$this, 'getSampleUrl'], [
                'is_safe_callback' => [
                    $this->routing,
                    'isUrlGenerationSafe',
                ],
            ]),

            new \Twig_SimpleFunction('templateShortlink', [$this, 'getShortlink'], [
                'is_safe_callback' => [
                    $this->routing,
                    'isUrlGenerationSafe',
                ],
            ]),

            new \Twig_SimpleFunction('templateUrl', [$this, 'getDetailsUrl'], [
                'is_safe_callback' => [
                    $this->routing,
                    'isUrlGenerationSafe',
                ],
            ]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'template_routing';
    }

    /**
     * @param Template $template
     *
     * @return string
     */
    public function getSampleUrl(Template $template)
    {
        return $this->generator->generateSampleUrl($template);
    }

    /**
     * @param Template $template
     * @return string
     */
    public function getPermalink(Template $template)
    {
        return $this->generator->generatePermalink($template);
    }

    /**
     * @param Template $template
     *
     * @return string
     */
    public function getShortlink(Template $template)
    {
        return $this->generator->generateShortlink($template);
    }
}
