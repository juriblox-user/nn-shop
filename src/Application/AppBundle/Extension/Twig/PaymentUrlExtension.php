<?php

namespace NnShop\Application\AppBundle\Extension\Twig;

use NnShop\Application\AppBundle\Services\UrlGenerator\PaymentUrlGenerator;
use NnShop\Domain\Orders\Entity\Invoice;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Entity\Payment;
use Symfony\Bridge\Twig\Extension\RoutingExtension;

class PaymentUrlExtension extends \Twig_Extension
{
    /**
     * @var PaymentUrlGenerator
     */
    private $generator;

    /**
     * @var RoutingExtension
     */
    private $routing;

    /**
     * PaymentUrlExtension constructor.
     *
     * @param PaymentUrlGenerator $generator
     * @param RoutingExtension    $routing
     */
    public function __construct(PaymentUrlGenerator $generator, RoutingExtension $routing)
    {
        $this->generator = $generator;
        $this->routing = $routing;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('mollieStatusUrl', [$this, 'getMollieStatusUrl'], [
                'is_safe_callback' => [
                    $this->routing,
                    'isUrlGenerationSafe',
                ],
            ]),

            new \Twig_SimpleFunction('orderPaymentUrl', [$this, 'getOrderPaymentUrl'], [
                'is_safe_callback' => [
                    $this->routing,
                    'isUrlGenerationSafe',
                ],
            ]),

            new \Twig_SimpleFunction('invoicePaymentUrl', [$this, 'getInvoicePaymentUrl'], [
                'is_safe_callback' => [
                    $this->routing,
                    'isUrlGenerationSafe',
                ],
            ]),
        ];
    }

    /**
     * @param Invoice $invoice
     * @param bool    $absolute
     *
     * @return string
     */
    public function getInvoicePaymentUrl(Invoice $invoice, $absolute = false)
    {
        return $this->generator->generateInvoicePaymentUrl($invoice, $absolute);
    }

    /**
     * @param Payment $payment
     *
     * @return string
     */
    public function getMollieStatusUrl(Payment $payment)
    {
        return $this->generator->generateMollieStatusUrl($payment);
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'payment_url';
    }

    /**
     * @param Order $order
     * @param bool  $absolute
     *
     * @return string
     */
    public function getOrderPaymentUrl(Order $order, $absolute = false)
    {
        return $this->generator->generateOrderPaymentUrl($order, $absolute);
    }
}
