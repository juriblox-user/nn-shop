<?php

namespace NnShop\Application\AppBundle\Extension\Twig;

use NnShop\Application\AppBundle\Services\UrlGenerator\DocumentUrlGenerator;
use NnShop\Domain\Templates\Entity\Document;
use Symfony\Bridge\Twig\Extension\RoutingExtension;

class DocumentRoutingExtension extends \Twig_Extension
{
    /**
     * @var DocumentUrlGenerator
     */
    private $generator;

    /**
     * @var RoutingExtension
     */
    private $routing;

    /**
     * DocumentRoutingExtension constructor.
     *
     * @param DocumentUrlGenerator $generator
     * @param RoutingExtension     $routing
     */
    public function __construct(DocumentUrlGenerator $generator, RoutingExtension $routing)
    {
        $this->generator = $generator;
        $this->routing = $routing;
    }

    /**
     * @param Document $document
     *
     * @return string
     */
    public function getDownloadMarkdownUrl(Document $document)
    {
        return $this->generator->generateDownloadMarkdownUrl($document);
    }

    /**
     * @param Document $document
     *
     * @return string
     */
    public function getDownloadPdfUrl(Document $document)
    {
        return $this->generator->generateDownloadPdfUrl($document);
    }

    /**
     * @param Document $document
     *
     * @return string
     */
    public function getDownloadWord2007Url(Document $document)
    {
        return $this->generator->generateDownloadWord2007Url($document);
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('documentDownloadMarkdownUrl', [$this, 'getDownloadMarkdownUrl'], [
                'is_safe_callback' => [
                    $this->routing,
                    'isUrlGenerationSafe',
                ],
            ]),
            new \Twig_SimpleFunction('documentDownloadPdfUrl', [$this, 'getDownloadPdfUrl'], [
                'is_safe_callback' => [
                    $this->routing,
                    'isUrlGenerationSafe',
                ],
            ]),
            new \Twig_SimpleFunction('documentDownloadWord2007Url', [$this, 'getDownloadWord2007Url'], [
                'is_safe_callback' => [
                    $this->routing,
                    'isUrlGenerationSafe',
                ],
            ]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'document_routing';
    }
}
