<?php

namespace NnShop\Application\AppBundle\Forms;

/**
 * Class TypeInterface.
 *
 * @deprecated
 *
 * @todo Imported from symfony-core: 1.3.16
 */
interface TypeInterface
{
    /**
     * @return string
     */
    public static function getRequestClass(): string;
}
