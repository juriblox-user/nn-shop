<?php

namespace NnShop\Application\AppBundle\Forms;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;

/**
 * @deprecated
 *
 * @todo Imported from symfony-core: 1.3.16
 */
abstract class AbstractRequest implements RequestInterface
{
    /**
     * @var Form
     */
    protected $form;

    /**
     * AbstractRequest constructor.
     */
    public function __construct()
    {
        $this->configure();
    }

    /**
     * @param FormInterface $form
     */
    public function bind(FormInterface $form)
    {
        $this->form = $form;
    }

    /**
     * Configuratie van het request (bijvoorbeeld defaults).
     */
    protected function configure()
    {
    }
}
