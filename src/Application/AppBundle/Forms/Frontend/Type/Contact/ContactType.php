<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Type\Contact;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ContactType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => 'Naam',
            'translation_domain' => 'forms',
        ]);

        $builder->add('email', EmailType::class, [
            'label' => 'E-mailadres',
            'translation_domain' => 'forms',
        ]);

        $builder->add('phone', TextType::class, [
            'label' => 'Telefoonnummer',
            'translation_domain' => 'forms',
            'required' => false,
        ]);

        $builder->add('message', TextareaType::class, [
            'label' => 'Bericht',
            'translation_domain' => 'forms',
        ]);
    }
}
