<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Type\Wizard;

use NnShop\Application\AppBundle\Forms\AbstractType;
use NnShop\Application\AppBundle\Forms\Frontend\DataMapper\CompanyActivityIdListMapper;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Wizard\CompanyActivitiesRequest;
use NnShop\Application\AppBundle\Forms\Shared\Type\EnhancedCheckboxChoicesType;
use Symfony\Component\Form\FormBuilderInterface;

class CompanyActivitiesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var CompanyActivitiesRequest $request */
        $request = $this->getRequest($builder);

        $choices = [];
        foreach ($request->getType()->getActivities() as $activity) {
            $choices[$activity->getId()->getString()] = $activity->getId();
        }

        $builder->add('activities', EnhancedCheckboxChoicesType::class, [
            'data_mapper' => CompanyActivityIdListMapper::class,

            'choices' => $choices,
            'required' => false,
        ]);

        parent::buildForm($builder, $options);
    }

    /**
     * @return string
     */
    public static function getRequestClass(): string
    {
        return CompanyActivitiesRequest::class;
    }
}
