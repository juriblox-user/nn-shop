<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Type\Account\Session;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class LoginType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', TextType::class, [
            'label' => 'email_address',
            'translation_domain' => 'forms',
        ]);

        $builder->add('password', PasswordType::class, [
            'label' => 'password',
            'translation_domain' => 'forms',
        ]);
    }
}
