<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Type\Account\Profile;

use Core\Application\CoreBundle\Forms\Type\AddressType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class EditDetailsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('company_name', TextType::class, [
            'label' => 'Bedrijfsnaam',
            'translation_domain' => 'forms',

            'required' => false,
        ]);

        $builder->add('firstname', TextType::class, [
            'label' => 'Voornaam',
            'translation_domain' => 'forms',
        ]);

        $builder->add('lastname', TextType::class, [
            'label' => 'Achternaam',
            'translation_domain' => 'forms',
        ]);

        $builder->add('address', AddressType::class, [
            'label' => false,
        ]);

        $builder->add('phone', TextType::class, [
            'label' => 'Telefoonnummer (optioneel)',
            'translation_domain' => 'forms',
            'required' => false,
        ]);

        $builder->add('vat', TextType::class, [
            'label' => 'btw-nummer (optioneel)',
            'translation_domain' => 'forms',
            'required' => false,
        ]);

        parent::buildForm($builder, $options);
    }
}
