<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Type\Account\Profile;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;

class EditCredentialsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, [
            'label' => 'email_address',
            'translation_domain' => 'forms',
        ]);

        $builder->add('currentPassword', PasswordType::class, [
            'label' => 'current_password',
            'translation_domain' => 'forms',
        ]);

        $builder->add('updatedPassword', RepeatedType::class, [
            'type' => PasswordType::class,

            'required' => false,

            'first_options' => [
                'label' => 'new_password',
                'translation_domain' => 'forms',
            ],

            'second_options' => [
                'label' => 'repeat_password',
                'translation_domain' => 'forms',
            ],
        ]);
    }
}
