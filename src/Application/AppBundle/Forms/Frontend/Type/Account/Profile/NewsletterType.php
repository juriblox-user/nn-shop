<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Type\Account\Profile;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;

class NewsletterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('newsletter', CheckboxType::class, [
            'label' => 'subscribe_newsletter',
            'required' => false,
            'translation_domain' => 'forms',
        ]);
    }
}
