<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Type\Account\Recover;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;

class RequestRecoveryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, [
            'label' => 'email_address',
            'translation_domain' => 'forms',
        ]);
    }
}
