<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Type\Account\Recover;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;

class CompleteRecoveryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('password', RepeatedType::class, [
            'type' => PasswordType::class,

            'first_options' => [
                'label' => 'password',
                'translation_domain' => 'forms',
            ],

            'second_options' => [
                'label' => 'repeat_password',
                'translation_domain' => 'forms',
            ],
        ]);
    }
}
