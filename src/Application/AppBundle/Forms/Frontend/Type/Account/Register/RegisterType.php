<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Type\Account\Register;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, [
            'label' => 'email_address',
            'disabled' => $options['disable_email_address'],
            'translation_domain' => 'forms',
        ]);

        $builder->add('password', RepeatedType::class, [
            'type' => PasswordType::class,

            'first_options' => [
                'label' => 'password',
                'translation_domain' => 'forms',
            ],

            'second_options' => [
                'label' => 'confirm_password',
                'translation_domain' => 'forms',
            ],
        ]);

        $builder->add('newsletter', CheckboxType::class, [
            'label' => 'subscribe_newsletter',
            'required' => false,
            'translation_domain' => 'forms',
        ]);

        $builder->add('terms', CheckboxType::class, [
            'label' => 'accept_terms',
            'required' => true,
            'translation_domain' => 'forms',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('disable_email_address', false);
        $resolver->setAllowedTypes('disable_email_address', ['boolean']);
    }
}
