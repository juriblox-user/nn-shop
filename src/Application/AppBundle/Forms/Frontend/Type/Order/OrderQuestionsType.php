<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Type\Order;

use NnShop\Application\AppBundle\Forms\Frontend\Request\Order\OrderQuestionsRequest;
use Symfony\Component\Form\FormBuilderInterface;

class OrderQuestionsType extends AbstractOrderType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('answers', OrderQuestionsStepType::class, [
            'data' => $options['data'],
        ]);

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public static function getRequestClass(): string
    {
        return OrderQuestionsRequest::class;
    }
}
