<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Type\Order;

use Core\Application\CoreBundle\Forms\Type\EmailValueObjectType;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Order\OrderAccountRequest;
use NnShop\Domain\Orders\Enumeration\CustomerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class OrderAccountType extends AbstractOrderType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var OrderAccountRequest $formRequest */
        $formRequest = $options['data'];

        $builder->add('customerType', ChoiceType::class, [
            'label' => 'Ik ben een',
            'required' => true,
            'expanded' => true,
            'multiple' => false,
            'choices' => [
                'Ondernemer' => CustomerType::BUSINESS,
                'Particulier' => CustomerType::CONSUMER,
            ],
            'data' => CustomerType::BUSINESS,
        ]);

        /*
         * Customer information
         */
        $builder->add('customerCompanyName', TextType::class, [
            'label' => 'Bedrijfsnaam',
            'translation_domain' => 'forms',
            'required' => false,
        ]);

        $builder->add('customerFirstname', TextType::class, [
            'label' => 'Voornaam',
            'translation_domain' => 'forms',
            'required' => false,
        ]);

        $builder->add('customerLastname', TextType::class, [
            'label' => 'Achternaam',
            'translation_domain' => 'forms',
            'required' => false,
        ]);

        $builder->add('customerEmail', EmailValueObjectType::class, [
            'label' => 'E-mailadres',
            'translation_domain' => 'forms',
            'disabled' => $options['disable_email_address'],
            'required' => false,
        ]);

        $builder->add('customerAddressStreet', TextType::class, [
            'label' => 'address.street',
            'required' => false,
        ]);

        $builder->add('customerAddressNumber', TextType::class, [
            'label' => 'address.number',
            'required' => false,
        ]);

        $builder->add('customerAddressAddition', TextType::class, [
            'label' => 'address.addition',
            'required' => false,
        ]);

        $builder->add('customerAddressZipcode', TextType::class, [
            'label' => 'address.zipcode',
            'required' => false,
        ]);

        $builder->add('customerAddressCity', TextType::class, [
            'label' => 'address.city',
            'required' => false,
        ]);

        $builder->add('customerAddressCountry', CountryType::class, [
            'label' => 'address.country',
            'preferred_choices' => [$formRequest->getProfileCountry()],
            'placeholder' => 'Kies uw land',
            'required' => false,
            'data' => $formRequest->getProfileCountry(),
        ]);

        $builder->add('customerPhone', TextType::class, [
            'label' => 'Telefoonnummer (optioneel)',
            'translation_domain' => 'forms',
            'required' => false,
        ]);

        $builder->add('customerVat', TextType::class, [
            'label' => 'btw-nummer (optioneel)',
            'translation_domain' => 'forms',
            'required' => false,
        ]);

        $builder->add('customerCoc', TextType::class, [
            'label' => 'KvK-nummer (optioneel)',
            'translation_domain' => 'forms',
            'required' => false,
        ]);

        /*
         * Registration fields (new customer)
         */
//        $builder->add('registerPassword', RepeatedType::class, [
//            'type' => PasswordType::class,
//
//            'first_options' => [
//                'label' => 'password',
//                'translation_domain' => 'forms',
//            ],
//
//            'second_options' => [
//                'label' => 'confirm_password',
//                'translation_domain' => 'forms',
//            ],
//        ]);
//
//        $builder->add('registerNewsletter', CheckboxType::class, [
//            'label' => 'subscribe_newsletter',
//            'required' => false,
//            'translation_domain' => 'forms',
//        ]);
//
//        $builder->add('registerTerms', CheckboxType::class, [
//            'label' => 'accept_terms',
//            'required' => true,
//            'translation_domain' => 'forms',
//        ]);

        /*
         * Login (existing customer)
         */
//        $builder->add('loginEmail', TextType::class, [
//            'label' => 'email_address',
//            'translation_domain' => 'forms',
//        ]);
//
//        $builder->add('loginPassword', PasswordType::class, [
//            'label' => 'password',
//            'translation_domain' => 'forms',
//        ]);

        parent::buildForm($builder, $options);
    }

    /**
     * OrderCustomerType configuratie.
     */
    public function configure()
    {
        $this->setNextButtonLabel('flow.choose_options', 'forms');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefault('disable_email_address', false);
        $resolver->setAllowedTypes('disable_email_address', ['boolean']);

        $resolver->setDefaults(array(
            'validation_groups' => function (FormInterface $form) {
                /** @var OrderAccountRequest $formRequest */
                $formRequest = $form->getData();

//                if (null !== $formRequest->getLoginEmail() || null !== $formRequest->getLoginPassword()) {
//                    return ['Default', 'login'];
//                }
//
//                if (null !== $formRequest->getRegisterPassword() || $formRequest->getRegisterTerms()) {
//                    return ['Default', 'register'];
//                }

                return ['Default', 'customer'];
            },
        ));
    }

    /**
     * {@inheritdoc}
     */
    public static function getRequestClass(): string
    {
        return OrderAccountRequest::class;
    }
}
