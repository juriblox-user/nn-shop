<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Type\Order;

use NnShop\Application\AppBundle\Forms\Frontend\Request\Order\OrderOptionsRequest;
use NnShop\Application\AppBundle\Forms\Shared\Type\EnhancedCheckboxType;
use NnShop\Application\AppBundle\Forms\Shared\Type\EnhancedRadioChoicesType;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Enumeration\OrderType;
use Money\Money;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class OrderOptionsType extends AbstractOrderType
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * Constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        parent::__construct();

        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    public static function getRequestClass(): string
    {
        return OrderOptionsRequest::class;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var OrderOptionsRequest $request */
        $request = $this->getRequest($builder);

        /** @var Order $order */
        $order = $request->getFlow()->getSubject();

        if (null !== $order->getTemplate()->getMonthlyPrice()) {
            $builder->add('type', EnhancedRadioChoicesType::class, [
                'choices' => [
                    'unit' => OrderType::UNIT,
                    'subscription' => OrderType::SUBSCRIPTION,
                ],
            ]);
        }

        $builder->add('custom', EnhancedCheckboxType::class, [
            'required' => false,
        ]);

        if (!$order->getTemplate()->getCheckPrice()->isZero()) {
            $builder->add('check', EnhancedCheckboxType::class, [
                'required' => false,
            ]);
        }

        if (null !== $order->getTemplate()->getRegisteredEmailPrice()) {
            $builder->add('registeredEmail', EnhancedCheckboxType::class, [
                'required' => false,
            ]);
        }

        parent::buildForm($builder, $options);
    }

    /**
     * Configuratie flow.
     */
    protected function configure()
    {
        parent::configure();

        $token = $this->tokenStorage->getToken();

        if (null === $token->getUser() || !$token->getUser() instanceof Customer) {
            $this->hideBackButton();
        }

        $this->setNextButtonLabel('Controleer uw bestelling');
    }
}
