<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Type\Order;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

final class RegisteredEmailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('subject', TextType::class, [
            'label' => 'Onderwerp voor ontvanger',
            'translation_domain' => 'forms',
        ]);

        $builder->add('email', EmailType::class, [
            'label' => 'E-mailadres ontvanger',
            'translation_domain' => 'forms',
        ]);

        $builder->add('message', TextareaType::class, [
            'label' => 'Bericht voor ontvanger',
            'translation_domain' => 'forms',
        ]);
    }
}
