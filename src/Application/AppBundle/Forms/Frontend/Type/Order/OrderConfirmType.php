<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Type\Order;

use NnShop\Application\AppBundle\Forms\Frontend\DataMapper\PaymentMethodListMapper;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Order\OrderConfirmRequest;
use NnShop\Application\AppBundle\Forms\Shared\Type\EnhancedRadioChoicesType;
use NnShop\Application\AppBundle\Services\Payment\MollieGateway;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Enumeration\PaymentMethod;
use NnShop\Domain\Translation\Entity\Profile;
use NnShop\Infrastructure\Orders\Transformer\PaymentMethodMollieTransformer;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class OrderConfirmType extends AbstractOrderType
{
    /**
     * @var bool
     */
    private $zeroInvoice = false;

    /**
     * @var MollieGateway
     */
    private $mollieGateway;

    /**
     * {@inheritdoc}
     *
     * @throws \Mollie\Api\Exceptions\ApiException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var OrderConfirmRequest $request */
        $request = $this->getRequest($builder);

        /** @var Order $order */
        $order = $request->getFlow()->getSubject();

        $this->zeroInvoice = $order->getProforma()->isZero();

        /*
         * Optie voor niet-gratis bestellingen
         */
        if (!$this->zeroInvoice) {
            $builder->add('discount', TextType::class, [
                'label' => 'Kortingscode',
                'translation_domain' => 'forms',

                'required' => false,

                'attr' => [
                    'placeholder' => 'Kortingscode',
                ],
            ]);

            $builder->add('apply_discount', SubmitType::class, [
                'label' => 'OK',
                'translation_domain' => 'forms',
            ]);

            // Betaalmethode
            $builder->add('method', EnhancedRadioChoicesType::class, [
                'label' => 'Betaalmethode',
                'data_mapper' => PaymentMethodListMapper::class,
                'choices' => $this->getPaymentMethodChoices($order->getTemplate()->getProfile()),
            ]);
        }

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public static function getRequestClass(): string
    {
        return OrderConfirmRequest::class;
    }

    /**
     * @param MollieGateway $mollieGateway
     *
     * @return OrderConfirmType
     */
    public function setMollieGateway(MollieGateway $mollieGateway): self
    {
        $this->mollieGateway = $mollieGateway;

        return $this;
    }

    /**
     * OrderCheckoutType configuratie.
     */
    protected function configure()
    {
        if ($this->zeroInvoice) {
            $this->setFinishButtonLabel('flow.order', 'forms');
        } else {
            $this->setFinishButtonLabel('flow.order_and_pay', 'forms');
        }
    }

    /**
     * @param Profile $profile
     *
     * @throws \Mollie\Api\Exceptions\ApiException
     *
     * @return array
     */
    private function getPaymentMethodChoices(Profile $profile)
    {
        $allowedMethods = $profile->getAllowedPaymentMethods();
        $mollieAllowedMethods = $this->mollieGateway->getAllowedMethods($profile);
        $choices = [];

        foreach (PaymentMethod::all() as $item) {
            $mollieMethodKey = PaymentMethodMollieTransformer::toRemote($item);
            if (in_array($mollieMethodKey, $allowedMethods)) {
                if (!$mollieAllowedMethods->containsKey($mollieMethodKey)) {
                    continue;
                }

                if ($mollieAllowedMethods->get($mollieMethodKey)->isExternallyDisabled()) {
                    continue;
                }

                $choices[$item->getValue()] = $item;
            }
        }

        return $choices;
    }
}
