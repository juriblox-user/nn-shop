<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Type\Order;

use Core\Application\CoreBundle\Forms\Flow\AbstractFlowType;
use NnShop\Application\AppBundle\Forms\TypeInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractOrderType extends AbstractFlowType implements TypeInterface
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'attr' => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }
}
