<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Type\Order;

use NnShop\Application\AppBundle\Forms\AbstractType;
use NnShop\Application\AppBundle\Forms\Frontend\Builder\DocumentQuestionBuilder;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Order\OrderQuestionsRequest;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderQuestionsStepType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var OrderQuestionsRequest $request */
        $request = $this->getRequest($builder);

        $builder->setDataMapper($request);

        $questionBuilder = new DocumentQuestionBuilder($builder, $request);

        foreach ($request->getQuestions() as $question) {
            $questionBuilder->build($question);
        }

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'label' => false,
        ]);
    }

    public static function getRequestClass(): string
    {
        return OrderQuestionsRequest::class;
    }
}
