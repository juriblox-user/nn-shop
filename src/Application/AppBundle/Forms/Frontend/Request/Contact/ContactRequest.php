<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Request\Contact;

use Symfony\Component\Validator\Constraints as Assert;

class ContactRequest
{
    /**
     * @Assert\NotBlank(message="Wilt u uw e-mailadres opgeven? Dan kunnen wij reageren op uw vraag.")
     *
     * @var string
     */
    private $email;

    /**
     * @Assert\NotBlank(message="We hebben uiteraard wel een bericht nodig om te kunnen beantwoorden :)")
     *
     * @var string
     */
    private $message;

    /**
     * @Assert\NotBlank(message="Zou u uw naam willen laten weten? Dan kunnen we u netjes aanspreken.")
     *
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $phone;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message ?: null;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name ?: null;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone ?: null;
    }
}
