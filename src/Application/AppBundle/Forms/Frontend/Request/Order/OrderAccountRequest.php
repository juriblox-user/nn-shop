<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Request\Order;

use Core\Domain\Value\Web\EmailAddress;
use NnShop\Domain\Orders\Enumeration\CustomerType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class OrderAccountRequest extends AbstractOrderRequest
{
    /**
     * @var string|null
     */
    private $customerAddressAddition;

    /**
     * @Assert\NotBlank(groups={"customer", "register"}, message="address.city.required")
     *
     * @var string|null
     */
    private $customerAddressCity;

    /**
     * @Assert\NotBlank(groups={"customer", "register"}, message="address.country.required")
     *
     * @var string|null
     */
    private $customerAddressCountry;

    /**
     * @Assert\NotBlank(groups={"customer", "register"}, message="address.number.required")
     *
     * @var string|null
     */
    private $customerAddressNumber;

    /**
     * @Assert\NotBlank(groups={"customer", "register"}, message="address.street.required")
     *
     * @var string|null
     */
    private $customerAddressStreet;

    /**
     * @Assert\NotBlank(groups={"customer", "register"}, message="address.zipcode.required")
     *
     * @var string|null
     */
    private $customerAddressZipcode;

    /**
     * @var string|null
     */
    private $customerCompanyName;

    /**
     * @Assert\NotBlank(groups={"customer", "register"}, message="email.required")
     * @Assert\Email(message="email.format")
     *
     * @var EmailAddress|null
     */
    private $customerEmail;

    /**
     * @Assert\NotBlank(groups={"customer", "register"}, message="firstname.required")
     *
     * @var string
     */
    private $customerFirstname;

    /**
     * @Assert\NotBlank(groups={"customer", "register"}, message="lastname.required")
     *
     * @var string
     */
    private $customerLastname;

    /**
     * @Assert\Regex(groups={"customer", "register"}, pattern="/^[0-9\-\s\+]+$/", message="Dit lijkt geen geldig telefoonnummer te zijn.")
     *
     * @var string
     */
    private $customerPhone;

    /**
     * @Assert\NotBlank(groups={"customer", "register"}, message="Maak a.u.b een keuze")
     *
     * @var CustomerType
     */
    private $customerType;

    /**
     * @var string
     */
    private $customerVat;

    /**
     * @var string
     */
    private $customerCoc;

    /**
     * @Assert\NotBlank(groups={"login"})
     * @Assert\Email
     *
     * @var string|null
     */
    private $loginEmail;

    /**
     * @Assert\NotBlank(groups={"login"})
     *
     * @var string|null
     */
    private $loginPassword;

    /**
     * @var string
     */
    private $profileCountry;

    /**
     * @var bool
     */
    private $registerNewsletter;

    /**
     * @Assert\NotBlank(groups={"register"})
     * @Assert\Length(min="6", minMessage="password_too_short")
     *
     * @var string|null
     */
    private $registerPassword;

    /**
     * @Assert\IsTrue(groups={"register"}, message="must_agree_terms")
     *
     * @var bool
     */
    private $registerTerms;

    /**
     * @return string|null
     */
    public function getCustomerAddressAddition()
    {
        return $this->customerAddressAddition;
    }

    /**
     * @return string|null
     */
    public function getCustomerAddressCity()
    {
        return $this->customerAddressCity;
    }

    /**
     * @return string|null
     */
    public function getCustomerAddressCountry()
    {
        return $this->customerAddressCountry;
    }

    /**
     * @return string|null
     */
    public function getCustomerAddressNumber()
    {
        return $this->customerAddressNumber;
    }

    /**
     * @return string|null
     */
    public function getCustomerAddressStreet()
    {
        return $this->customerAddressStreet;
    }

    /**
     * @return string|null
     */
    public function getCustomerAddressZipcode()
    {
        return $this->customerAddressZipcode;
    }

    /**
     * @return string
     */
    public function getCustomerCompanyName()
    {
        return $this->customerCompanyName;
    }

    /**
     * @return EmailAddress|null
     */
    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    /**
     * @return string
     */
    public function getCustomerFirstname()
    {
        return $this->customerFirstname;
    }

    /**
     * @return string
     */
    public function getCustomerLastname()
    {
        return $this->customerLastname;
    }

    /**
     * @return string
     */
    public function getCustomerPhone()
    {
        return $this->customerPhone;
    }

    /**
     * @return string
     */
    public function getCustomerVat()
    {
        return $this->customerVat;
    }

    /**
     * @return string|null
     */
    public function getLoginEmail()
    {
        return $this->loginEmail;
    }

    /**
     * @return string|null
     */
    public function getLoginPassword()
    {
        return $this->loginPassword;
    }

    /**
     * @return string
     */
    public function getProfileCountry()
    {
        return $this->profileCountry;
    }

    /**
     * @return bool
     */
    public function getRegisterNewsletter(): bool
    {
        return $this->registerNewsletter;
    }

    /**
     * @return string|null
     */
    public function getRegisterPassword()
    {
        return $this->registerPassword;
    }

    /**
     * @return bool
     */
    public function getRegisterTerms(): bool
    {
        return $this->registerTerms;
    }

    /**
     * @return string|null
     */
    public function getCustomerCoc()
    {
        return $this->customerCoc;
    }

    /**
     * @return CustomerType
     */
    public function getCustomerType()
    {
        return $this->customerType;
    }

    /**
     * OrderCustomerRequest configurator.
     */
    public function initialize()
    {
        $this->registerNewsletter = false;
        $this->registerTerms = false;
    }

    /**
     * @param string|null $customerAddressAddition
     */
    public function setCustomerAddressAddition($customerAddressAddition)
    {
        $this->customerAddressAddition = $customerAddressAddition ?: null;
    }

    /**
     * @param string|null $customerAddressCity
     */
    public function setCustomerAddressCity($customerAddressCity)
    {
        $this->customerAddressCity = $customerAddressCity ?: null;
    }

    /**
     * @param string|null $customerAddressCountry
     */
    public function setCustomerAddressCountry($customerAddressCountry)
    {
        $this->customerAddressCountry = $customerAddressCountry ?: null;
    }

    /**
     * @param string|null $customerAddressNumber
     */
    public function setCustomerAddressNumber($customerAddressNumber)
    {
        $this->customerAddressNumber = $customerAddressNumber ?: null;
    }

    /**
     * @param string|null $customerAddressStreet
     */
    public function setCustomerAddressStreet($customerAddressStreet)
    {
        $this->customerAddressStreet = $customerAddressStreet ?: null;
    }

    /**
     * @param string|null $customerAddressZipcode
     */
    public function setCustomerAddressZipcode($customerAddressZipcode)
    {
        $this->customerAddressZipcode = $customerAddressZipcode ?: null;
    }

    /**
     * @param string $customerCompanyName
     */
    public function setCustomerCompanyName($customerCompanyName)
    {
        $this->customerCompanyName = $customerCompanyName ?: null;
    }

    /**
     * @param EmailAddress $customerEmail
     */
    public function setCustomerEmail($customerEmail)
    {
        $this->customerEmail = $customerEmail;
    }

    /**
     * @param string $customerFirstname
     */
    public function setCustomerFirstname($customerFirstname)
    {
        $this->customerFirstname = $customerFirstname ?: null;
    }

    /**
     * @param string $customerLastname
     */
    public function setCustomerLastname($customerLastname)
    {
        $this->customerLastname = $customerLastname ?: null;
    }

    /**
     * @param string $customerPhone
     */
    public function setCustomerPhone($customerPhone)
    {
        $this->customerPhone = $customerPhone;
    }

    /**
     * @param string $customerVat
     */
    public function setCustomerVat($customerVat)
    {
        $this->customerVat = $customerVat ?: null;
    }

    /**
     * @param string|null $loginEmail
     */
    public function setLoginEmail($loginEmail)
    {
        $this->loginEmail = $loginEmail ?: null;
    }

    /**
     * @param string|null $loginPassword
     */
    public function setLoginPassword($loginPassword)
    {
        $this->loginPassword = $loginPassword ?: null;
    }

    /**
     * @param string $profileCountry
     */
    public function setProfileCountry($profileCountry)
    {
        $this->profileCountry = $profileCountry ?: null;
    }

    /**
     * @param bool $registerNewsletter
     */
    public function setRegisterNewsletter(bool $registerNewsletter)
    {
        $this->registerNewsletter = $registerNewsletter;
    }

    /**
     * @param string|null $registerPassword
     */
    public function setRegisterPassword($registerPassword)
    {
        $this->registerPassword = $registerPassword ?: null;
    }

    /**
     * @param bool $registerTerms
     */
    public function setRegisterTerms(bool $registerTerms)
    {
        $this->registerTerms = $registerTerms;
    }

    /**
     * @param string|null $customerCoc
     */
    public function setCustomerCoc($customerCoc)
    {
        $this->customerCoc = $customerCoc ?: null;
    }

    /**
     * @param string $customerType
     */
    public function setCustomerType($customerType)
    {
        $this->customerType = $customerType;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(new Assert\Callback('checkIfBusinessType'));
    }

    public function checkIfBusinessType(ExecutionContextInterface $context)
    {
        if(($this->getCustomerType() === CustomerType::BUSINESS) && ($this->getCustomerCompanyName() === null)) {
            $context->buildViolation('Vul alstublieft uw bedrijfsnaam in.')
                ->atPath('customerCompanyName')
                ->addViolation();
        }
    }
}
