<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Request\Order;

use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Enumeration\OrderType;

class OrderOptionsRequest extends AbstractOrderRequest
{
    /**
     * Handmatige controle aanvragen.
     *
     * @var bool
     */
    private $check;

    /**
     * Maatwerk aanpassingen aanvragen.
     *
     * @var bool
     */
    private $custom;

    /**
     * @var bool
     */
    private $registeredEmail;

    /**
     * Type document dat wordt aangevraagd.
     *
     * @var string
     */
    private $type;

    /**
     * @return bool
     */
    public function getCheck(): bool
    {
        return $this->check;
    }

    /**
     * @return bool
     */
    public function getCustom(): bool
    {
        return $this->custom;
    }

    public function getRegisteredEmail(): bool
    {
        return $this->registeredEmail;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * OrderOptionsRequest configurator.
     */
    public function initialize()
    {
        /** @var Order $order */
        $order = $this->getFlow()->getSubject();

        if ($order->getType()->in([OrderType::UNIT, OrderType::SUBSCRIPTION])) {
            $this->type = $order->getType();
        } else {
            $this->type = OrderType::UNIT;
        }

        $this->check = null !== $order->isCheckRequested() ? $order->isCheckRequested() : false;
        $this->custom = null !== $order->isCustomRequested() ? $order->isCustomRequested() : false;

        $this->registeredEmail = $order->isRegisteredEmailRequested();
    }

    /**
     * @param bool $check
     */
    public function setCheck(bool $check)
    {
        $this->check = $check;
    }

    /**
     * @param bool $custom
     */
    public function setCustom(bool $custom)
    {
        $this->custom = $custom;
    }

    public function setRegisteredEmail(bool $registeredEmail): void
    {
        $this->registeredEmail = $registeredEmail;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }
}
