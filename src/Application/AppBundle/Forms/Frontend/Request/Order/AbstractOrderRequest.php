<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Request\Order;

use Core\Application\CoreBundle\Forms\Flow\AbstractFlowRequest;
use NnShop\Application\AppBundle\Forms\RequestInterface;

abstract class AbstractOrderRequest extends AbstractFlowRequest implements RequestInterface
{

}
