<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Request\Order;

use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Enumeration\PaymentMethod;
use NnShop\Domain\Orders\Services\Validator\DiscountValidator;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContext;

class OrderConfirmRequest extends AbstractOrderRequest
{
    /**
     * @var string
     */
    private $discount;

    /**
     * @var DiscountValidator
     */
    private $discountValidator;

    /**
     * @var PaymentMethod
     */
    private $method;

    /**
     * OrderConfirmRequest configurator.
     */
    public function initialize()
    {
        /** @var Order $order */
        $order = $this->getFlow()->getSubject();

        $this->method = $order->getPaymentMethod() ?: PaymentMethod::from(PaymentMethod::IDEAL);
    }

    /**
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @return PaymentMethod
     */
    public function getMethod(): PaymentMethod
    {
        return $this->method;
    }

    /**
     * @param string $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount ?: null;
    }

    /**
     * @param PaymentMethod $method
     */
    public function setMethod(PaymentMethod $method)
    {
        $this->method = $method;
    }

    /**
     * @param DiscountValidator $validator
     */
    public function setValidator(DiscountValidator $validator)
    {
        $this->discountValidator = $validator;
    }

    /**
     * @Assert\Callback
     *
     * @param ExecutionContext $context
     */
    public function validate(ExecutionContext $context)
    {
        /** @var Order $order */
        $order = $this->getFlow()->getSubject();

        /** @var OrderConfirmRequest $request */
        $request = $context->getObject();

        if (null !== $request->getDiscount()) {
            $code = $request->getDiscount();

            // Controleren of de kortingscode überhaupt geldig is
            if (!$this->discountValidator->isValid($code)) {
                $context->buildViolation('orders.question.discount_code_not_found')
                    ->atPath('discount')
                    ->addViolation();
            }

            // Controleren of de kortingscode geldig is voor deze order
            elseif (!$this->discountValidator->isValidForOrder($code, $order)) {
                $context->buildViolation('orders.question.discount_code_cant_be_used_again')
                    ->atPath('discount')
                    ->addViolation();
            }

            // Controleren of de kortingscode niet conflicteert
            elseif ($this->discountValidator->isConflicting($code, $order)) {
                $context->buildViolation('orders.question.discount_code_cant_be_used_in_combination')
                    ->atPath('discount')
                    ->addViolation();
            }
        }
    }
}
