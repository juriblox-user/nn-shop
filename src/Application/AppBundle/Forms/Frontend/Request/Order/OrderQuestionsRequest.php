<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Request\Order;

use Core\Common\Validation\Assertion;
use Doctrine\Common\Collections\Collection;
use NnShop\Application\AppBundle\Forms\Frontend\Flow\OrderStatusFlow;
use NnShop\Domain\Orders\Data\QuestionWithAnswerBag;
use NnShop\Domain\Orders\Services\Resolver\QuestionConditionResolver;
use NnShop\Domain\Orders\Services\Resolver\QuestionsGraph;
use NnShop\Domain\Templates\Entity\Question;
use NnShop\Domain\Templates\Entity\QuestionOption;
use NnShop\Domain\Templates\Entity\QuestionStep;
use NnShop\Domain\Templates\Value\QuestionId;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContext;

class OrderQuestionsRequest extends AbstractOrderRequest implements DataMapperInterface
{
    /**
     * @var QuestionWithAnswerBag
     */
    private $answers;

    /**
     * @var QuestionsGraph
     */
    private $graph;

    /**
     * @var QuestionConditionResolver
     */
    private $resolver;

    /**
     * @var QuestionStep
     */
    private $step;

    /**
     * OrderQuestionsRequest constructor.
     *
     * @param OrderStatusFlow $flow
     * @param QuestionsGraph  $graph
     * @param QuestionStep    $step
     */
    public function __construct(OrderStatusFlow $flow, QuestionsGraph $graph, QuestionStep $step)
    {
        parent::__construct($flow);

        $this->graph = $graph;
        $this->step = $step;

        $this->answers = new QuestionWithAnswerBag();

        $this->resolver = new QuestionConditionResolver($this->graph);
        $this->resolver->load();
    }

    /**
     * @param QuestionId $questionId
     *
     * @return mixed
     */
    public function getAnswerValue(QuestionId $questionId)
    {
        return $this->answers->get($questionId)->getValue();
    }

    /**
     * Combinatie van QuestionId met het antwoord.
     */
    public function getAnswers(): QuestionWithAnswerBag
    {
        return $this->answers;
    }

    /**
     * @return Collection|Question[]
     */
    public function getQuestions(): Collection
    {
        return $this->graph->getQuestions($this->step->getId());
    }

    /**
     * @return QuestionConditionResolver
     */
    public function getResolver(): QuestionConditionResolver
    {
        return $this->resolver;
    }

    /**
     * @param QuestionId $questionId
     *
     * @return bool
     */
    public function hasAnswerValue(QuestionId $questionId): bool
    {
        return $this->answers->has($questionId);
    }

    /**
     * Maps properties of some data to a list of forms.
     *
     * @param OrderQuestionsRequest $data  Structured data
     * @param FormInterface[]       $forms A list of {@link FormInterface} instances
     *
     * @throws \Assert\AssertionFailedException
     * @throws \Core\Common\Exception\DataIntegrityException
     */
    public function mapDataToForms($data, $forms)
    {
        Assertion::isInstanceOf($data, self::class);

        foreach ($forms as $field) {
            if (!QuestionId::valid($field->getName())) {
                continue;
            }

            $questionId = QuestionId::from($field->getName());
            if (!$data->graph->hasQuestion($questionId)) {
                continue;
            }

            if ($data->hasAnswerValue($questionId)) {
                $field->setData($data->getAnswerValue($questionId));
            } else {
                $question = $data->graph->getQuestion($questionId);

                $answer = $data->graph->findAnswer($question->getId());

                /*
                 * Er is nog geen antwoord bekend
                 */
                if (null === $answer) {
                    if ($question->getType()->usesOptions()) {
                        $defaults = [];
                        foreach ($question->getDefaultVisibleOptions() as $defaultOption) {
                            $defaults[] = $defaultOption;
                        }

                        if (count($defaults) > 0) {
                            $field->setData($question->getType()->hasMultipleOptions() ? $defaults : $defaults[0]);
                        } elseif (!$question->getType()->hasMultipleOptions()) {
                            $field->setData($question->getVisibleOptions()->first());
                        } else {
                            $field->setData(null);
                        }
                    } else {
                        $field->setData(null);
                    }
                }

                /*
                 * Antwoord met opties
                 */
                elseif ($question->getType()->usesOptions()) {
                    if ($question->getType()->hasMultipleOptions()) {
                        $field->setData($answer->getOptions()->toArray());
                    } elseif ($answer->hasOptions()) {
                        $field->setData($answer->getOptions()->first());
                    }
                }

                /*
                 * Antwoord met platte tekst
                 */
                else {
                    $field->setData($answer->getValue());
                }
            }
        }
    }

    /**
     * Maps the data of a list of forms into the properties of some data.
     *
     * @param FormInterface[]       $forms A list of {@link FormInterface} instances
     * @param OrderQuestionsRequest $data  Structured data
     *
     * @throws \Assert\AssertionFailedException
     * @throws \Core\Common\Exception\DataIntegrityException
     */
    public function mapFormsToData($forms, &$data)
    {
        Assertion::isInstanceOf($data, self::class);

        foreach ($forms as $field) {
            if (!QuestionId::valid($field->getName()) || null === $field->getData()) {
                continue;
            }

            $questionId = QuestionId::from($field->getName());
            if (!$data->graph->hasQuestion($questionId)) {
                continue;
            }

            $question = $data->graph->getQuestion($questionId);

            // Verwijzing naar een QuestionOptionId
            if ($question->getType()->usesOptions()) {
                if ($question->getType()->hasMultipleOptions() && is_array($field->getData())) {
                    if (0 == count($field->getData())) {
                        $this->answers->setEmpty($questionId);
                    }

                    /** @var QuestionOption $option */
                    foreach ($field->getData() as $option) {
                        $this->answers->addOption($questionId, $option->getId());
                    }
                } else {
                    $this->answers->setOption($questionId, $field->getData()->getId());
                }
            }

            // Simpele rechttoe, rechtaan waarde
            else {
                $this->answers->setValue($questionId, $field->getData());
            }
        }
    }

    /**
     * Fictieve setter voor het "answers" field.
     *
     * @param $answers
     */
    public function setAnswers($answers)
    {
        // Deze moet blijven staan om NoSuchPropertyExceptions te voorkomen
    }

    /**
     * @Assert\Callback
     *
     * @param ExecutionContext $context
     *
     * @throws \Exception
     */
    public function validate(ExecutionContext $context)
    {
        /** @var OrderQuestionsRequest $request */
        $request = $context->getObject();

        $this->resolver->load($this->answers);

        foreach ($request->getQuestions() as $question) {
            $result = $this->resolver->evaluate($question);

            if ($result->isFail()) {
                if ($result->hasFailedConditions()) {
                    $violation = $context->buildViolation('orders.question.fails_condition')
                        ->setParameter('%question%', $result->getLastFailedQuestion()->getTitle());
                } else {
                    $violation = $context->buildViolation('orders.question.not_blank');
                }

                $violation->atPath('answers.' . $question->getId()->getString())
                    ->addViolation();
            }
        }
    }

    /**
     * Request initialiseren.
     */
    public function initialize()
    {
        // TODO: Implement initialize() method.
    }
}
