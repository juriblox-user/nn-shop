<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Request\Order;

use Symfony\Component\Validator\Constraints as Assert;

final class RegisteredEmailRequest
{
    /**
     * @Assert\NotBlank(message="Wilt u het e-mailadres van de ontvanger opgeven?")
     * @Assert\Email
     *
     * @var string
     */
    private $email;

    /**
     * @Assert\NotBlank(message="Wilt u een bericht toevoegen voor de ontvanger?")
     *
     * @var string
     */
    private $message;

    /**
     * @Assert\NotBlank(message="Wilt u het onderwerp voor de aangetekende mail toevoegen?")
     *
     * @var string
     */
    private $subject;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @param string $message
     */
    public function setMessage($message): void
    {
        $this->message = $message ?: null;
    }

    public function setSubject($subject): void
    {
        $this->subject = $subject ?: null;
    }
}
