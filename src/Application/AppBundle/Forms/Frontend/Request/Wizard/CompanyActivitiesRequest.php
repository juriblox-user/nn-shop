<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Request\Wizard;

use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Domain\Templates\Entity\CompanyType;
use NnShop\Domain\Templates\Value\CompanyActivityId;
use Symfony\Component\Validator\Constraints as Assert;

class CompanyActivitiesRequest extends AbstractRequest
{
    /**
     * @Assert\Count(min=1, minMessage="U moet minimaal 1 activiteit selecteren.")
     *
     * @var array|CompanyActivityId[]
     */
    private $activities;

    /**
     * @var CompanyType
     */
    private $type;

    /**
     * CompanyActivitiesRequest constructor.
     *
     * @param CompanyType $type
     */
    public function __construct(CompanyType $type)
    {
        $this->type = $type;

        parent::__construct();
    }

    /**
     * @return array|CompanyActivityId[]
     */
    public function getActivities()
    {
        return $this->activities;
    }

    /**
     * @return CompanyType
     */
    public function getType(): CompanyType
    {
        return $this->type;
    }

    /**
     * @param array|CompanyActivityId[] $activities
     */
    public function setActivities($activities)
    {
        $this->activities = $activities;
    }
}
