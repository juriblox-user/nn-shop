<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Request\Account\Profile;

use Core\Domain\Value\Geography\Address;
use NnShop\Domain\Orders\Entity\Customer;
use Symfony\Component\Validator\Constraints as Assert;

class EditDetailsRequest
{
    /**
     * @var Address|null
     */
    private $address;

    /**
     * @Assert\NotBlank(groups={"business"}, message="Het is voor zakelijke klanten verplicht om een bedrijfsnaam op te geven.")
     *
     * @var string
     */
    private $companyName;

    /**
     * @Assert\NotBlank
     *
     * @var string
     */
    private $firstname;

    /**
     * @Assert\NotBlank
     *
     * @var string
     */
    private $lastname;

    /**
     * @Assert\Regex("/^[0-9\-\s\+]+$/", message="Dit lijkt geen geldig telefoonnummer te zijn.")
     *
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $vat;

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param Customer $customer
     */
    public function read(Customer $customer)
    {
        $this->companyName = $customer->getCompanyName();
        $this->firstname = $customer->getFirstname();
        $this->lastname = $customer->getLastname();

        $this->address = $customer->getAddress();
        $this->phone = $customer->getPhone();

        $this->vat = $customer->getVat();
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return Address|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param Address|null $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName ?: null;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname ?: null;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname ?: null;
    }

    /**
     * @return string
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param string $vat
     */
    public function setVat($vat)
    {
        $this->vat = $vat ?: null;
    }
}
