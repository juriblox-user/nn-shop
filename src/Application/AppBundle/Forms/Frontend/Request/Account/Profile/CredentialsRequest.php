<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Request\Account\Profile;

use Symfony\Component\Validator\Constraints as Assert;

class CredentialsRequest
{
    /**
     * @Assert\NotBlank
     * @Assert\Email
     *
     * @var string|null
     */
    private $email;

    /**
     * @Assert\NotBlank
     * @Assert\Length(min="6", minMessage="password_too_short")
     *
     * @var string|null
     */
    private $password;

    /**
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return null|string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string|null $email
     */
    public function setEmail($email)
    {
        $this->email = $email ?: null;
    }

    /**
     * @param null|string $password
     */
    public function setPassword($password)
    {
        $this->password = $password ?: null;
    }
}
