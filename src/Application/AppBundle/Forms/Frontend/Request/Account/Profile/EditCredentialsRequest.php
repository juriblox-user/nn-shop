<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Request\Account\Profile;

use NnShop\Domain\Orders\Entity\Customer;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Component\Validator\Constraints as Assert;

class EditCredentialsRequest
{
    /**
     * @Assert\NotBlank
     * @SecurityAssert\UserPassword(message="Het ingevoerde wachtwoord komt niet overeen met uw huidige wachtwoord.")
     *
     * @var string|null
     */
    private $currentPassword;

    /**
     * @Assert\NotBlank
     * @Assert\Email
     *
     * @var string|null
     */
    private $email;

    /**
     * @Assert\Length(min="6", minMessage="Gebruik een wachtwoord van minimaal 6 karakters lang.")
     *
     * @var string|null
     */
    private $updatedPassword;

    /**
     * @return null|string
     */
    public function getCurrentPassword()
    {
        return $this->currentPassword;
    }

    /**
     * @return null|string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return null|string
     */
    public function getUpdatedPassword()
    {
        return $this->updatedPassword;
    }

    /**
     * @param Customer $customer
     */
    public function read(Customer $customer)
    {
        $this->email = $customer->getEmail();
    }

    /**
     * @param null|string $currentPassword
     */
    public function setCurrentPassword($currentPassword)
    {
        $this->currentPassword = $currentPassword;
    }

    /**
     * @param null|string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param null|string $updatedPassword
     */
    public function setUpdatedPassword($updatedPassword)
    {
        $this->updatedPassword = $updatedPassword;
    }
}
