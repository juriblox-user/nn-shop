<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Request\Account\Profile;

class NewsletterRequest
{
    /**
     * @var bool
     */
    private $newsletter;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->newsletter = false;
    }

    /**
     * @return bool
     */
    public function getNewsletter(): bool
    {
        return $this->newsletter;
    }

    /**
     * @param bool $newsletter
     */
    public function setNewsletter(bool $newsletter)
    {
        $this->newsletter = $newsletter;
    }
}
