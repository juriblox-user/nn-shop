<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Request\Account\Session;

use Symfony\Component\Validator\Constraints as Assert;

class LoginRequest
{
    /**
     * @Assert\NotBlank
     * @Assert\Email
     *
     * @var string|null
     */
    private $email;

    /**
     * @Assert\NotBlank
     *
     * @var string|null
     */
    private $password;

    /**
     * @return null|string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return null|string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param null|string $email
     */
    public function setEmail($email)
    {
        $this->email = $email ?: null;
    }

    /**
     * @param null|string $password
     */
    public function setPassword($password)
    {
        $this->password = $password ?: null;
    }
}
