<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Request\Account\Register;

use Symfony\Component\Validator\Constraints as Assert;

class RegisterRequest
{
    /**
     * @Assert\NotBlank
     * @Assert\Email
     *
     * @var string|null
     */
    private $email;

    /**
     * @var bool
     */
    private $newsletter;

    /**
     * @Assert\IsTrue(message="must_agree_terms")
     * @var bool
     */
    private $terms;

    /**
     * @Assert\NotBlank
     * @Assert\Length(min="6", minMessage="password_too_short")
     *
     * @var string|null
     */
    private $password;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->newsletter = false;
    }

    /**
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function getNewsletter(): bool
    {
        return $this->newsletter;
    }

    /**
     * @return null|string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return bool
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * @param string|null $email
     */
    public function setEmail($email)
    {
        $this->email = $email ?: null;
    }

    /**
     * @param bool $newsletter
     */
    public function setNewsletter(bool $newsletter)
    {
        $this->newsletter = $newsletter;
    }

    /**
     * @param null|string $password
     */
    public function setPassword($password)
    {
        $this->password = $password ?: null;
    }

    /**
     * @param $terms
     */
    public function setTerms($terms)
    {
        $this->terms = $terms;
    }
}
