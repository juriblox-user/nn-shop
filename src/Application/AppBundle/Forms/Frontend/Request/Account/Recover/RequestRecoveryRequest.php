<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Request\Account\Recover;

use Symfony\Component\Validator\Constraints as Assert;

class RequestRecoveryRequest
{
    /**
     * @Assert\NotBlank
     * @Assert\Email
     *
     * @var string|null
     */
    private $email;

    /**
     * @return null|string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param null|string $email
     */
    public function setEmail($email)
    {
        $this->email = $email ?: null;
    }
}
