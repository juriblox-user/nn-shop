<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Request\Account\Recover;

use Symfony\Component\Validator\Constraints as Assert;

class CompleteRecoveryRequest
{
    /**
     * @Assert\NotBlank
     * @Assert\Length(min="6", minMessage="password_too_short")
     *
     * @var string|null
     */
    private $password;

    /**
     * @return null|string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param null|string $password
     */
    public function setPassword($password)
    {
        $this->password = $password ?: null;
    }
}
