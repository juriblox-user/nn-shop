<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Flow;

use Core\Component\Flow\AbstractStateStep;

class OrderStatusStep extends AbstractStateStep
{
    /**
     * @var int
     */
    private $duration;

    /**
     * @var string
     */
    private $type;

    /**
     * @param string $type
     * @param string $title
     *
     * @return OrderStatusStep
     */
    public static function fromType(string $type, string $title): self
    {
        $step = static::fromTitle($title);
        $step->type = $type;

        return $step;
    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $duration
     *
     * @return $this
     */
    public function setDuration(int $duration)
    {
        $this->duration = $duration;

        return $this;
    }
}
