<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Flow;

use Core\Component\Flow\AbstractStateFlow;
use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Application\AppBundle\Forms\Frontend\Type\Order\OrderAccountType;
use NnShop\Application\AppBundle\Forms\Frontend\Type\Order\OrderConfirmType;
use NnShop\Application\AppBundle\Forms\Frontend\Type\Order\OrderOptionsType;
use NnShop\Application\AppBundle\Forms\Frontend\Type\Order\OrderQuestionsType;
use NnShop\Domain\Orders\Entity\Order;
use NnShop\Domain\Orders\Enumeration\OrderStatus;

class OrderStatusFlow extends AbstractStateFlow
{
    /**
     * {@inheritdoc}
     */
    public function setup()
    {
        /** @var Order $order */
        $order = $this->getSubject();

        // 1: Vragenlijst
        $this->addStep(
            OrderStatusStep::fromType(OrderQuestionsType::class, 'Vragenlijst')
                ->setDuration(floor($order->getDocument()->getTemplate()->getOrderSeconds() / 60))
                ->addState(OrderStatus::from(OrderStatus::STEP_QUESTIONS))
                ->setLocked($order->isLocked())
        );

        // 2: Bedrijfsgegevens
        $this->addStep(
            OrderStatusStep::fromType(OrderAccountType::class, 'Bedrijfsgegevens')
                ->addState(OrderStatus::from(OrderStatus::STEP_ACCOUNT))
                ->setLocked($order->isLocked())
        );

        // 3: Opties
        $this->addStep(
            OrderStatusStep::fromType(OrderOptionsType::class, 'Opties')
                ->addState(OrderStatus::from(OrderStatus::STEP_OPTIONS))
                ->setLocked($order->isLocked())
        );

        // 4: Bevestigen
        $this->addStep(
            OrderStatusStep::fromType(OrderConfirmType::class, 'Bevestigen')
                ->addState(OrderStatus::from(OrderStatus::STEP_CONFIRM))
                ->setLocked($order->isLocked())
        );
    }

    /**
     * @return ArrayCollection|OrderStatusStep[]
     */
    public function getSteps(): ArrayCollection
    {
        return parent::getSteps();
    }
}
