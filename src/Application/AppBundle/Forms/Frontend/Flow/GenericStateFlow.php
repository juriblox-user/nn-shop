<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Flow;

use Core\Component\Flow\AbstractStateFlow;
use Core\Component\Flow\StateStepInterface;
use Core\Component\Flow\StateSubjectInterface;
use Core\Component\Flow\SubjectAwareInterface;

/**
 * Class GenericStateFlow.
 *
 * @todo Added because symfony core GenericStateFlow has conflict with SubjectAwareInterface
 */
final class GenericStateFlow extends AbstractStateFlow
{
    /** @var SubjectAwareInterface */
    private $subject;

    /**
     * Flow initialiseren op basis van het subject.
     */
    public function setup()
    {
    }

    /**
     * @param StateSubjectInterface      $subject
     * @param array|StateStepInterface[] $steps
     * @param bool                       $flexible flexibel omgaan met een state die niet wordt ondersteund
     *
     * @return GenericStateFlow
     */
    public static function withSteps(StateSubjectInterface $subject, array $steps, $flexible = false): self
    {
        $flow = self::prepareWithSubject($subject);

        foreach ($steps as $step) {
            $flow->addStep($step);
        }

        $flow->setState($subject->getState(), $flexible);

        return $flow;
    }

    /**
     * @param StateSubjectInterface $subject
     *
     * @return static
     */
    protected static function prepareWithSubject(StateSubjectInterface $subject)
    {
        $flow = new static();
        $flow->subject = $subject;

        $flow->setup();

        return $flow;
    }
}
