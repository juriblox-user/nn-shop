<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Flow;

use Core\Component\Flow\AbstractStep;
use NnShop\Domain\Templates\Entity\QuestionStep;

class QuestionsStep extends AbstractStep
{
    /**
     * @var string
     */
    private $description;

    /**
     * @var QuestionStep
     */
    private $entity;

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return QuestionStep
     */
    public function getEntity(): QuestionStep
    {
        return $this->entity;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description ?: null;

        return $this;
    }

    /**
     * @param QuestionStep $entity
     *
     * @return $this
     */
    public function setEntity(QuestionStep $entity)
    {
        $this->entity = $entity;

        return $this;
    }
}
