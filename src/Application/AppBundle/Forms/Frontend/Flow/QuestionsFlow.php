<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Flow;

use Core\Component\Flow\AbstractFlow;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use NnShop\Domain\Templates\Entity\QuestionStep;
use NnShop\Domain\Templates\Entity\Template;

class QuestionsFlow extends AbstractFlow
{
    /**
     * @var Template
     */
    private $template;

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        foreach ($this->template->getQuestionSteps() as $questionStep) {
            $step = QuestionsStep::fromTitle($questionStep->getTitle())
                ->setDescription($questionStep->getDescription())
                ->setEntity($questionStep);

            $this->addStep($step);
        }
    }

    /**
     * @param Template $template
     *
     * @return QuestionsFlow
     */
    public static function fromTemplate(Template $template)
    {
        $flow = new self();
        $flow->template = $template;

        $flow->configure();

        return $flow;
    }

    /**
     * @return QuestionStep
     */
    public function getCurrentStepEntity(): QuestionStep
    {
        /** @var QuestionsStep $step */
        $step = $this->getCurrentStep();

        if (null === $step) {
            throw new \LogicException('There is no current step');
        }

        return $step->getEntity();
    }

    /**
     * @return ArrayCollection|QuestionsStep[]
     */
    public function getSteps(): ArrayCollection
    {
        return parent::getSteps();
    }

    /**
     * @param QuestionStep $step
     */
    public function setCurrentQuestionStep(QuestionStep $step)
    {
        $matches = $this->steps->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('entity', $step)
            )
        );

        if ($matches->isEmpty()) {
            throw new \InvalidArgumentException(sprintf('The step "%s" [%s] could not be found in this flow', $step->getTitle(), $step->getId()));
        }

        $this->setCurrentStep($matches->first());
    }

    /**
     * @param QuestionStep $step
     */
    public function skipQuestionStep(QuestionStep $step)
    {
        $matches = $this->steps->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('entity', $step)
            )
        );

        if ($matches->isEmpty()) {
            throw new \InvalidArgumentException(sprintf('The step "%s" [%s] could not be found in this flow', $step->getTitle(), $step->getId()));
        }

        $this->skipStep($matches->first());
    }
}
