<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\DataMapper;

use Core\Common\Validation\Assertion;
use NnShop\Domain\Orders\Enumeration\PaymentMethod;
use Symfony\Component\Form\DataMapperInterface;

class PaymentMethodListMapper implements DataMapperInterface
{
    /**
     * {@inheritdoc}
     *
     * @param $method PaymentMethod
     */
    public function mapDataToForms($method, $radios)
    {
        Assertion::isInstanceOf($method, PaymentMethod::class);

        foreach ($radios as $radio) {
            $value = $radio->getConfig()->getOption('value');
            $radio->setData($method->getValue() == $value);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function mapFormsToData($types, &$type)
    {
        Assertion::nullOrIsInstanceOf($type, PaymentMethod::class);

        $choice = null;

        foreach ($types as $radio) {
            if ($radio->getData()) {
                if ('placeholder' === $radio->getName()) {
                    return;
                }

                $type = $radio->getConfig()->getOption('value');

                return;
            }
        }
    }
}
