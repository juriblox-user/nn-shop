<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\DataMapper;

use NnShop\Domain\Templates\Value\CompanyActivityId;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Extension\Core\DataMapper\CheckboxListMapper;

class CompanyActivityIdListMapper extends CheckboxListMapper implements DataMapperInterface
{
    /**
     * {@inheritdoc}
     */
    public function mapDataToForms($choices, $checkboxes)
    {
        if (null === $choices) {
            $choices = [];
        }

        if (!is_array($choices)) {
            throw new UnexpectedTypeException($choices, 'array');
        }

        foreach ($checkboxes as $checkbox) {
            /** @var CompanyActivityId $value */
            $value = $checkbox->getConfig()->getOption('value');

            $checkbox->setData(in_array($value->getString(), $choices, true));
        }
    }
}
