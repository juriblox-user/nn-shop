<?php

namespace NnShop\Application\AppBundle\Forms\Frontend\Builder;

use Core\Common\Validation\Assertion;
use NnShop\Application\AppBundle\Forms\Frontend\Request\Order\OrderQuestionsRequest;
use NnShop\Application\AppBundle\Forms\Shared\Type\StatementType;
use NnShop\Domain\Templates\Entity\Question;
use NnShop\Domain\Templates\Entity\QuestionOption;
use NnShop\Domain\Templates\Enumeration\QuestionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class DocumentQuestionBuilder
{
    /**
     * @var FormBuilderInterface
     */
    private $builder;

    /**
     * @var OrderQuestionsRequest
     */
    private $request;

    /**
     * DocumentQuestionTransformer constructor.
     *
     * @param FormBuilderInterface  $builder
     * @param OrderQuestionsRequest $request
     */
    public function __construct(FormBuilderInterface $builder, OrderQuestionsRequest $request)
    {
        $this->builder = $builder;
        $this->request = $request;
    }

    /**
     * @param Question $question
     */
    public function build(Question $question)
    {
        switch ($question->getType()) {
            case QuestionType::TYPE_BOOLEAN:
                $this->buildBoolean($question);

                break;

            case QuestionType::TYPE_CHOICE:
                $this->buildChoice($question);

                break;

            case QuestionType::TYPE_COC:
                $this->buildChamberOfCommerce($question);

                break;

            case QuestionType::TYPE_DATE:
                $this->buildDate($question);

                break;

            case QuestionType::TYPE_DROPDOWN:
                $this->buildDropdown($question);

                break;

            case QuestionType::TYPE_INFOBOX:
                $this->buildInfobox($question);

                break;

            case QuestionType::TYPE_PRICE:
                $this->buildPrice($question);

                break;

            case QuestionType::TYPE_NUMERIC:
                $this->buildNumeric($question);

                break;

            case QuestionType::TYPE_MULTILINE_TEXT:
                $this->buildMultilineText($question);

                break;

            case QuestionType::TYPE_MULTIPLE_BOOLEANS:
                $this->buildMultipleBooleans($question);

                break;

            case QuestionType::TYPE_SINGLE_LINE_TEXT:
                $this->buildText($question);

                break;

            case QuestionType::TYPE_STATEMENT:
                $this->buildStatement($question);

                break;

            default:
                throw new \InvalidArgumentException(sprintf('The QuestionType "%s" is not supported by the DocumentQuestionBuilder', $question->getType()->getValue()));
        }
    }

    /**
     * @param Question $question
     */
    public function buildBoolean(Question $question)
    {
        $this->builder->add($question->getId()->getString(), ChoiceType::class, [
            'label' => $question->getTitle(),
            'translation_domain' => false,

            'attr' => $this->getAttributes($question),

            'choices' => $question->getVisibleOptions(),
            'choice_label' => function ($option) {
                Assertion::nullOrIsInstanceOf($option, QuestionOption::class);

                if (null === $option) {
                    return;
                }

                return 'booleans.yes-no.' . $option->getValue();
            },
            'choice_value' => function ($option) {
                Assertion::nullOrIsInstanceOf($option, QuestionOption::class);

                if (null === $option) {
                    return;
                }

                return $option->getId();
            },

            'choice_translation_domain' => 'messages',

            'expanded' => true,
            'multiple' => false,
        ]);
    }

    /**
     * @param Question $question
     */
    public function buildChamberOfCommerce(Question $question)
    {
        $this->builder->add($question->getId()->getString(), TextType::class, [
            'label' => $question->getTitle(),
            'translation_domain' => false,

            'attr' => $this->getAttributes($question),
        ]);
    }

    /**
     * @param Question $question
     */
    public function buildChoice(Question $question)
    {
        $this->builder->add($question->getId()->getString(), ChoiceType::class, [
            'label' => $question->getTitle(),
            'translation_domain' => false,

            'attr' => $this->getAttributes($question),

            'choices' => $question->getVisibleOptions(),

            'expanded' => true,
            'multiple' => false,

            // Labels en values via een property path uit QuestionOption lezen
            'choice_label' => 'title',
            'choice_value' => 'id',
        ]);
    }

    /**
     * @param Question $question
     */
    public function buildDate(Question $question)
    {
        $this->builder->add($question->getId()->getString(), DateType::class, [
            'label' => $question->getTitle(),
            'translation_domain' => false,

            'attr' => $this->getAttributes($question),

            'html5' => false,
            'format' => 'dd-MM-yyyy',
            'widget' => 'single_text',
        ]);
    }

    /**
     * @param Question $question
     */
    public function buildDropdown(Question $question)
    {
        $this->builder->add($question->getId()->getString(), ChoiceType::class, [
            'label' => $question->getTitle(),
            'translation_domain' => false,

            'attr' => $this->getAttributes($question),

            'choices' => $question->getVisibleOptions(),

            'expanded' => false,
            'multiple' => false,

            // Labels en values via een property path uit QuestionOption lezen
            'choice_label' => 'title',
            'choice_value' => 'id',
        ]);
    }

    /**
     * @param Question $question
     */
    public function buildInfobox(Question $question)
    {
        $this->builder->add($question->getId()->getString(), HiddenType::class, [
            'attr' => $this->getAttributes($question, [
                'data-title' => $question->getTitle(),
            ]),
        ]);
    }

    /**
     * @param Question $question
     */
    public function buildMultilineText(Question $question)
    {
        $this->builder->add($question->getId()->getString(), TextareaType::class, [
            'label' => $question->getTitle(),
            'translation_domain' => false,

            'attr' => $this->getAttributes($question),
        ]);
    }

    /**
     * @param Question $question
     */
    public function buildMultipleBooleans(Question $question)
    {
        $this->builder->add($question->getId()->getString(), ChoiceType::class, [
            'label' => $question->getTitle(),
            'translation_domain' => false,

            'attr' => $this->getAttributes($question),

            'choices' => $question->getVisibleOptions(),

            'expanded' => true,
            'multiple' => true,

            // Labels en values via een property path uit QuestionOption lezen
            'choice_label' => 'title',
            'choice_value' => 'id',
        ]);
    }

    /**
     * @param Question $question
     */
    public function buildNumeric(Question $question)
    {
        $this->builder->add($question->getId()->getString(), IntegerType::class, [
            'label' => $question->getTitle(),
            'translation_domain' => false,

            'attr' => $this->getAttributes($question, [
                'min' => 0,
            ]),
        ]);
    }

    /**
     * @param Question $question
     */
    public function buildPrice(Question $question)
    {
        $this->builder->add($question->getId()->getString(), MoneyType::class, [
            'label' => $question->getTitle(),
            'translation_domain' => false,

            'attr' => $this->getAttributes($question),

            'grouping' => true,
        ]);
    }

    /**
     * @param Question $question
     */
    public function buildStatement(Question $question)
    {
        $this->builder->add($question->getId()->getString(), StatementType::class, [
            'label' => $question->getTitle(),
            'translation_domain' => false,

            'attr' => $this->getAttributes($question),
        ]);
    }

    /**
     * @param Question $question
     */
    public function buildText(Question $question)
    {
        $this->builder->add($question->getId()->getString(), TextType::class, [
            'label' => $question->getTitle(),
            'translation_domain' => false,

            'attr' => $this->getAttributes($question),
        ]);
    }

    /**
     * @param Question $question
     * @param array    $attributes
     *
     * @return array
     */
    private function getAttributes(Question $question, array $attributes = []): array
    {
        $conditions = [];
        foreach ($question->getConditions() as $condition) {
            $parent = $condition->getParent();

            // Conditie op basis van een optie
            if (null !== $condition->getOption()) {
                $subject = $condition->getOption()->getQuestion();

                if ($this->request->getResolver()->isVisible($subject) && $this->request->getResolver()->isAnsweredWithOption($subject, $condition->getOption())) {
                    $result = 'true';
                } else {
                    $result = 'false';
                }

                $conditions[] = sprintf('option:%s:%s:%s', $condition->getOption()->getId(), $result, $subject->getId());
            }

            // Conditie op basis van een andere vraag
            elseif (null !== $parent) {
                $result = $this->request->getResolver()->getAnswer($parent);

                if (null !== $result && $this->request->getResolver()->isVisible($parent)) {
                    // Voor een statement vergelijken we ook de waarde van de vraag
                    if ($parent->getType()->is(QuestionType::TYPE_STATEMENT)) {
                        $default = $result->getValue();
                    }

                    // Voor andere vragen geldt "beantwoord is beantwoord"
                    else {
                        $default = $result->isAnswered();
                    }
                } else {
                    $default = false;
                }

                $conditions[] = sprintf('question:%s:%s', $parent->getId(), $default ? 'true' : 'false');
            }

            // Onbekende soort conditie
            else {
                throw new \Exception('Could not convert QuestionCondition');
            }
        }

        return array_merge([
            'required' => false,
            'placeholder' => $question->getTitle(),

            'data-id' => $question->getId(),
            'data-help' => str_replace("\n", '\n', $question->getHelp()),
            'data-type' => $question->getType(),
            'data-required' => $question->isRequired(),
            'data-conditions' => implode($conditions, ','),
        ], $attributes);
    }
}
