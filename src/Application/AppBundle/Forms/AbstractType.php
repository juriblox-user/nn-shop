<?php

namespace NnShop\Application\AppBundle\Forms;

use Core\Common\Validation\Assertion;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AbstractType.
 *
 * @deprecated
 *
 * @todo Imported from symfony-core: 1.3.16
 */
abstract class AbstractType extends \Symfony\Component\Form\AbstractType
{
    /**
     * {@inheritdoc}
     *
     * @throws \Assert\AssertionFailedException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        Assertion::implementsInterface(static::getRequestClass(), RequestInterface::class, sprintf('%s does not implement RequestInterface, check your getRequestClass() return value.', get_class($this)));

        $resolver->setDefaults([
            'data_class' => static::getRequestClass(),
        ]);
    }

    /**
     * @deprecated Imported from TypeInterface. Need to refactor this functionality.
     *
     * @return string
     */
    abstract public static function getRequestClass(): string;

    /**
     * @param FormBuilderInterface $builder
     *
     * @return RequestInterface
     */
    protected function getRequest(FormBuilderInterface $builder): RequestInterface
    {
        return $builder->getData();
    }
}
