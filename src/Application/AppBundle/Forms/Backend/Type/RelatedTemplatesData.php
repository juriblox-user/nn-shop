<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type;

use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Domain\Templates\Entity\Template;

final class RelatedTemplatesData
{
    /**
     * @var ArrayCollection|Template[]
     */
    private $selected;

    /**
     * LinkCategoriesData constructor.
     *
     * @param ArrayCollection|Template[] $templates
     */
    public function __construct(ArrayCollection $templates)
    {
        $this->selected = $templates;
    }

    /**
     * @return ArrayCollection|Template[]
     */
    public function getSelected(): ArrayCollection
    {
        return $this->selected;
    }
}
