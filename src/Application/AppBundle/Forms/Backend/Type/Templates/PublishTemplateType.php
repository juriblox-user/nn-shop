<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type\Templates;

use Core\Application\CoreBundle\Forms\Type\MoneyValueObjectType;
use NnShop\Application\AppBundle\Forms\AbstractType;
use NnShop\Application\AppBundle\Forms\Backend\Request\Templates\PublishTemplateRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\CategoriesType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class PublishTemplateType.
 */
class PublishTemplateType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $request = $this->getRequest($builder);
        $profile = $request->getTemplate()->getPartner()->getCountryProfile();

        $builder->add('title', TextType::class, [
            'label' => 'Titel',
            'translation_domain' => false,
        ]);

        $builder->add('unit_price', MoneyValueObjectType::class, [
            'label' => 'Stukprijs',
            'translation_domain' => false,
        ]);

        $builder->add('check_price', MoneyValueObjectType::class, [
            'label' => 'Prijs handmatige controle',
            'translation_domain' => false,
        ]);

        $builder->add('subscription', CheckboxType::class, [
            'label' => 'Beschikbaar als abonnement',
            'required' => false,
            'translation_domain' => false,
        ]);

        $builder->add('monthly_price', MoneyValueObjectType::class, [
            'label' => 'Maandbedrag',
            'required' => false,
            'translation_domain' => false,
        ]);

        $builder->add('hidden', CheckboxType::class, [
            'label' => 'Verborgen',
            'required' => false,
            'translation_domain' => false,
        ]);

        $builder->add('service', CheckboxType::class, [
            'label' => 'Is een dienst',
            'required' => false,
            'translation_domain' => false,
        ]);

        $builder->add('categories', CategoriesType::class, ['profile' => $profile]);

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public static function getRequestClass(): string
    {
        return PublishTemplateRequest::class;
    }
}
