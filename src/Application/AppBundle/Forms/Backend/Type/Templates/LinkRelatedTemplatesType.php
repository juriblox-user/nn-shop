<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type\Templates;

use NnShop\Application\AppBundle\Forms\AbstractType;
use NnShop\Application\AppBundle\Forms\Backend\Request\Templates\LinkRelatedTemplatesRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\RelatedTemplatesType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class LinkRelatedTemplatesType.
 */
final class LinkRelatedTemplatesType extends AbstractType
{
    public static function getRequestClass(): string
    {
        return LinkRelatedTemplatesRequest::class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $request = $this->getRequest($builder);
        $profile = $request->getTemplate()->getPartner()->getCountryProfile();

        $builder->add('related', RelatedTemplatesType::class, [
            'profile' => $profile,
        ]);

        parent::buildForm($builder, $options);
    }
}
