<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type\Templates;

use Core\Application\CoreBundle\Forms\Type\MoneyValueObjectType;
use NnShop\Application\AppBundle\Forms\AbstractType;
use NnShop\Application\AppBundle\Forms\Backend\Request\Templates\EditTemplateRequest;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class EditTemplateType.
 */
class EditTemplateType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $request = $this->getRequest($builder);
        $currency = $request->getTemplate()->getPartner()->getCountryProfile()->getCurrency();

        $builder->add('title', TextType::class, [
            'label' => 'Titel',
            'translation_domain' => false,
        ]);

        $builder->add('spotlight', CheckboxType::class, [
            'label' => 'Spotlight (homepage)',
            'required' => false,

            'translation_domain' => false,
        ]);

        $builder->add('remote_description', TextareaType::class, [
            'label' => 'Omschrijving JuriBlox',
            'disabled' => true,
            'required' => false,

            'translation_domain' => false,
        ]);

        $builder->add('description', TextareaType::class, [
            'label' => 'description_shop',
            'translation_domain' => false,
        ]);

        $builder->add('excerpt', TextareaType::class, [
            'label' => 'Omschrijving kort',
            'translation_domain' => false,
            'error_bubbling' => true,
        ]);

        $builder->add('unit_price', MoneyValueObjectType::class, [
            'label' => 'Stukprijs',
            'translation_domain' => false,
            'currency' => $currency,
        ]);

        $builder->add('registered_email_price', MoneyValueObjectType::class, [
            'label' => 'Prijs aangetekende mail',
            'translation_domain' => false,
            'required' => false,
            'currency' => $currency,
        ]);

        $builder->add('check_price', MoneyValueObjectType::class, [
            'label' => 'Prijs handmatige controle',
            'translation_domain' => false,
            'currency' => $currency,
        ]);

        $builder->add('subscription', CheckboxType::class, [
            'label' => 'Beschikbaar als abonnement',
            'required' => false,
            'translation_domain' => false,
        ]);

        $builder->add('monthly_price', MoneyValueObjectType::class, [
            'label' => 'Maandbedrag',
            'required' => false,
            'translation_domain' => false,
            'currency' => $currency,
        ]);

        $builder->add('sample', FileType::class, [
            'label' => 'Voorbeeld-PDF',
            'required' => false,

            'error_bubbling' => true,       // TODO: meh.
            'translation_domain' => false,
        ]);

        $builder->add('preview', FileType::class, [
            'label' => 'Voorbeeld afbeelding',
            'required' => false,
            'translation_domain' => false,
        ]);

        $builder->add('hidden', CheckboxType::class, [
            'label' => 'Verborgen',
            'required' => false,
            'translation_domain' => false,
        ]);

        $builder->add('service', CheckboxType::class, [
            'label' => 'Is een dienst',
            'required' => false,
            'translation_domain' => false,
        ]);

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public static function getRequestClass(): string
    {
        return EditTemplateRequest::class;
    }
}
