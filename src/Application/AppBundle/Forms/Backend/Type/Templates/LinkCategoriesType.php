<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type\Templates;

use NnShop\Application\AppBundle\Forms\AbstractType;
use NnShop\Application\AppBundle\Forms\Backend\Request\Templates\LinkCategoriesRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\CategoriesType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class LinkCategoriesType.
 */
class LinkCategoriesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $request = $this->getRequest($builder);
        $profile = $request->getTemplate()->getPartner()->getCountryProfile();

        $builder->add('categories', CategoriesType::class, ['profile' => $profile]);

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public static function getRequestClass(): string
    {
        return LinkCategoriesRequest::class;
    }
}
