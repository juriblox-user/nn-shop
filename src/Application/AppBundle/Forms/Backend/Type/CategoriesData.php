<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type;

use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Domain\Templates\Entity\Category;

class CategoriesData
{
    /**
     * @var ArrayCollection|Category[]
     */
    private $selected;

    /**
     * @var Category
     */
    private $primary;

    /**
     * LinkCategoriesData constructor.
     *
     * @param ArrayCollection|Category[] $categories
     * @param Category                   $primary
     */
    public function __construct(ArrayCollection $categories, Category $primary = null)
    {
        $this->selected = $categories;
        $this->primary = $primary;
    }

    /**
     * @return ArrayCollection|Category[]
     */
    public function getSelected(): ArrayCollection
    {
        return $this->selected;
    }

    /**
     * @return Category|null
     */
    public function getPrimary()
    {
        return $this->primary;
    }
}
