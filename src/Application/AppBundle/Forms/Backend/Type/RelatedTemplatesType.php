<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type;

use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;
use NnShop\Domain\Translation\Entity\Profile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class RelatedTemplatesType extends AbstractType implements DataTransformerInterface
{
    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;
    /**
     * @var array|Template[]
     */
    private $templates;

    /**
     * LinkCategoriesType constructor.
     */
    public function __construct(TemplateRepositoryInterface $templateRepository)
    {
        $this->templateRepository = $templateRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $this->templates = $this->templateRepository->findVisible($options['profile']);

        $builder->addModelTransformer($this);

        $builder->add('selected', ChoiceType::class, [
            'expanded' => true,
            'multiple' => true,

            'choices'      => $this->templates,
            'choice_label' => false,
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['templates'] = $this->templates;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefined('profile');
        $resolver->setRequired('profile');
        $resolver->addAllowedTypes('profile', Profile::class);

        parent::configureOptions($resolver);
    }

    /**
     * @param array $value
     */
    public function reverseTransform($value)
    {
        if (!isset($value['selected'])) {
            throw new TransformationFailedException('The $value array does not contain "selected" and "primary" keys');
        }

        return new RelatedTemplatesData(new ArrayCollection($value['selected']));
    }

    /**
     * @param RelatedTemplatesData|null $value
     */
    public function transform($value)
    {
        if (null === $value) {
            return [
                'selected' => new ArrayCollection(),
            ];
        }

        return [
            'selected' => $value->getSelected()->toArray(),
        ];
    }
}
