<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type\Pages;

use NnShop\Application\AppBundle\Forms\AbstractType;
use NnShop\Application\AppBundle\Forms\Backend\Request\Pages\EditPageRequest;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class PageType.
 */
class EditPageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $request = $this->getRequest($builder);

        $builder->add('title', TextType::class, [
            'label' => 'Titel',
        ])
            ->add('code', TextType::class, [
                'label' => 'Code',
            ])
            ->add('lastUpdated', DateType::class, [
                'label' => 'Laatst aangepast',
            ])
            ->add('lead', TextareaType::class, [
                'label' => 'Lead',
                'required' => false,
                'attr' => [
                    'class' => 'editable',
                ],
            ])
            ->add('footer', TextareaType::class, [
                'label' => 'Footer',
                'required' => false,
                'attr' => [
                    'class' => 'editable',
                ],
            ])
            ->add('pdf', FileType::class, [
                'label' => 'Pdf',
                'required' => false,
                'data_class' => null,
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Content',
                'required' => false,
                'attr' => [
                    'class' => 'editable',
                ],
            ]);

        if (null !== $request->getPage()->getPdf()) {
            $builder->add('deletePdf', CheckboxType::class, [
                'label' => 'Pdf verwijderen',
                'required' => false,
            ]);
        }

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public static function getRequestClass(): string
    {
        return EditPageRequest::class;
    }
}
