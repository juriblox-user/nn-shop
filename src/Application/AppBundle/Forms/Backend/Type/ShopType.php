<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type;

use NnShop\Domain\Shops\Entity\Shop;
use NnShop\Domain\Shops\Repository\ShopRepositoryInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShopType extends ChoiceType
{
    /**
     * @var ShopRepositoryInterface
     */
    private $shopRepository;

    /**
     * ShopType constructor.
     *
     * @param ShopRepositoryInterface $shopRepository
     */
    public function __construct(ShopRepositoryInterface $shopRepository)
    {
        parent::__construct();

        $this->shopRepository = $shopRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'expanded' => false,
            'multiple' => false,

            'choices' => $this->shopRepository->findAll(),
            'choice_label' => function (Shop $shop) {
                return $shop->getTitle();
            },

            'translation_domain' => false,
        ]);
    }
}
