<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type;

use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Domain\Templates\Entity\Category;
use NnShop\Domain\Templates\Repository\CategoryRepositoryInterface;
use NnShop\Domain\Translation\Entity\Profile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoriesType extends AbstractType implements DataTransformerInterface
{
    /**
     * @var array|Category[]
     */
    private $categories;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    /**
     * LinkCategoriesType constructor.
     *
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->categories = $this->categoryRepository->findAllWithShopAndProfile($options['profile']);

        $builder->addModelTransformer($this);

        $builder->add('selected', ChoiceType::class, [
            'expanded' => true,
            'multiple' => true,

            'choices' => $this->categories,
            'choice_label' => false,
        ]);

        $builder->add('primary', ChoiceType::class, [
            'expanded' => true,
            'multiple' => false,

            'choices' => $this->categories,
            'choice_label' => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['categories'] = $this->categories;
    }

    /**
     * {@inheritdoc}
     *
     * @param array $value
     */
    public function reverseTransform($value)
    {
        if (!isset($value['selected']) || !isset($value['primary'])) {
            throw new TransformationFailedException('The $value array does not contain "selected" and "primary" keys');
        }

        return new CategoriesData(new ArrayCollection($value['selected']), $value['primary']);
    }

    /**
     * {@inheritdoc}
     *
     * @param CategoriesData|null $value
     */
    public function transform($value)
    {
        if (null === $value) {
            return [
                'selected' => new ArrayCollection(),
                'primary' => null,
            ];
        }

        return [
            'selected' => $value->getSelected()->toArray(),
            'primary' => $value->getPrimary(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined('profile');
        $resolver->setRequired('profile');
        $resolver->addAllowedTypes('profile', Profile::class);

        parent::configureOptions($resolver);
    }
}
