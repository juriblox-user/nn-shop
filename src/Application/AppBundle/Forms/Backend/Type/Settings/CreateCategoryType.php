<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type\Settings;

use NnShop\Application\AppBundle\Forms\AbstractType;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\CreateCategoryRequest;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CreateCategoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'label' => 'Titel',
            'translation_domain' => false,
        ]);

        $builder->add('description', TextareaType::class, [
            'label' => 'Beschrijving',
            'translation_domain' => false,
            'required' => false,
        ]);

        $builder->add('metaDescription', TextareaType::class, [
            'label' => 'Meta Description (SEO)',
            'translation_domain' => false,
            'required' => false,
        ]);

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public static function getRequestClass(): string
    {
        return CreateCategoryRequest::class;
    }
}
