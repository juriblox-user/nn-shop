<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type\Settings;

use NnShop\Application\AppBundle\Forms\AbstractType;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\CreateCompanyActivityRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\TemplateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CreateCompanyActivityType.
 */
class CreateCompanyActivityType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $request = $this->getRequest($builder);
        $profile = $request->getProfile();

        $builder->add('title', TextType::class, [
            'label' => 'Titel',
            'translation_domain' => false,
        ]);

        $builder->add('templates', TemplateType::class, [
            'multiple' => true,
            'expanded' => true,
            'profile' => $profile,
        ]);

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public static function getRequestClass(): string
    {
        return CreateCompanyActivityRequest::class;
    }
}
