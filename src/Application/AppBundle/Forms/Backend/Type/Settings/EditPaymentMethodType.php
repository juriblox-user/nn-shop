<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type\Settings;

use NnShop\Application\AppBundle\Forms\AbstractType;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\EditPaymentMethodRequest;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class EditPaymentMethodType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('enabled', ChoiceType::class, [
            'label' => 'Ingeschakeld',
            'translation_domain' => false,
            'required' => true,
            'choices' => [
                'Ja' => true,
                'Nee' => false,
            ],
        ]);

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public static function getRequestClass(): string
    {
        return EditPaymentMethodRequest::class;
    }
}
