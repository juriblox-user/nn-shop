<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type\Settings;

use NnShop\Application\AppBundle\Forms\AbstractType;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\EditLandingPageRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\TemplateType;
use NnShop\Domain\Orders\Entity\Discount;
use NnShop\Domain\Orders\Repository\DiscountRepositoryInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class EditLandingPageType extends AbstractType
{
    /**
     * @var DiscountRepositoryInterface
     */
    private $discountRepository;

    /**
     * @param DiscountRepositoryInterface $discountRepository
     */
    public function __construct(DiscountRepositoryInterface $discountRepository)
    {
        $this->discountRepository = $discountRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var EditLandingPageRequest $request */
        $request = $this->getRequest($builder);
        $profile = $request->getLandingPage()->getProfile();

        $builder->add('title', TextType::class, [
            'label' => 'Titel',
            'translation_domain' => false,
        ]);

        $builder->add('text', TextareaType::class, [
            'label' => 'Tekst',
            'translation_domain' => false,
        ]);

        $builder->add('discount', ChoiceType::class, [
            'label' => 'Kortingscode',
            'required' => false,

            'expanded' => false,
            'multiple' => false,

            'choices' => $this->discountRepository->findAllForMultipleUse(),

            'choice_label' => function (Discount $discount) {
                return $discount->getTitle() ?: $discount->getCode();
            },

            'translation_domain' => false,
        ]);

        $builder->add('templates', TemplateType::class, [
            'label' => 'Gekoppelde templates',

            'multiple' => true,
            'expanded' => true,
            'profile' => $profile,
        ]);

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public static function getRequestClass(): string
    {
        return EditLandingPageRequest::class;
    }
}
