<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type\Settings;

use NnShop\Application\AppBundle\Forms\AbstractType;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\CreatePartnerRequest;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CreatePartnerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'label' => 'Titel',
            'translation_domain' => false,
        ]);

        $builder->add('excerpt', TextareaType::class, [
            'label' => 'Korte omschrijving',
            'translation_domain' => false,
        ]);

        $builder->add('profile', TextareaType::class, [
            'label' => 'Profiel',
            'translation_domain' => false,
        ]);

        $builder->add('remote_id', IntegerType::class, [
            'label' => 'JuriBlox-ID',
            'translation_domain' => false,
        ]);

        $builder->add('sync_enabled', CheckboxType::class, [
            'label' => 'Synchroniseren',
            'required' => false,

            'translation_domain' => false,
        ]);

        $builder->add('sync_client_id', IntegerType::class, [
            'label' => 'Client ID',
            'required' => false,

            'translation_domain' => false,
        ]);

        $builder->add('sync_client_key', TextType::class, [
            'label' => 'Client key',
            'required' => false,

            'translation_domain' => false,
        ]);

        $builder->add('logo', FileType::class, [
            'label' => 'Logo',
            'error_bubbling' => true,       // TODO: meh.
            'translation_domain' => false,
        ]);

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public static function getRequestClass(): string
    {
        return CreatePartnerRequest::class;
    }
}
