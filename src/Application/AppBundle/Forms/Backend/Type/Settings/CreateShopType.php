<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type\Settings;

use NnShop\Application\AppBundle\Forms\AbstractType;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\CreateShopRequest;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CreateShopType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'label' => 'Titel',
            'translation_domain' => false,
        ]);

        $builder->add('hostname', TextType::class, [
            'label' => 'Domeinnaam',
            'error_bubbling' => true,
            'translation_domain' => false,
        ]);

        $builder->add('meta_description', TextareaType::class, [
            'label' => 'meta_description',
            'error_bubbling' => true,
            'translation_domain' => false,
            'required' => false,
        ]);

        $builder->add('description', TextareaType::class, [
            'label' => 'Beschrijving',
            'error_bubbling' => true,
            'translation_domain' => false,
            'required' => false,
        ]);

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public static function getRequestClass(): string
    {
        return CreateShopRequest::class;
    }
}
