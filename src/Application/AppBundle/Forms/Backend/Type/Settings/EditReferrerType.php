<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type\Settings;

use NnShop\Application\AppBundle\Forms\AbstractType;
use NnShop\Application\AppBundle\Forms\Backend\Request\Settings\EditReferrerRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\TemplateType;
use NnShop\Domain\Orders\Entity\Discount;
use NnShop\Domain\Orders\Repository\DiscountRepositoryInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;

class EditReferrerType extends AbstractType
{
    /**
     * @var DiscountRepositoryInterface
     */
    private $discountRepository;

    /**
     * @param DiscountRepositoryInterface $discountRepository
     */
    public function __construct(DiscountRepositoryInterface $discountRepository)
    {
        $this->discountRepository = $discountRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $request = $this->getRequest($builder);
        $profile = $request->getReferrer()->getProfile();

        /** @var EditReferrerRequest $request */
        $request = $this->getRequest($builder);

        $builder->add('title', TextType::class, [
            'label' => 'Titel',
            'translation_domain' => false,
        ]);

        $builder->add('text', TextareaType::class, [
            'label' => 'Profiel',
            'translation_domain' => false,
        ]);

        $builder->add('website', UrlType::class, [
            'label' => 'Website',
            'required' => false,

            'translation_domain' => false,
        ]);

        $builder->add('homepage', CheckboxType::class, [
            'label' => 'Weergeven op homepage',
            'required' => false,

            'translation_domain' => false,
        ]);

        $builder->add('kickback', CheckboxType::class, [
            'label' => 'Kickback op bestellingen',
            'required' => false,

            'translation_domain' => false,
        ]);

        $builder->add('landing', CheckboxType::class, [
            'label' => 'Landingspagina ingeschakeld',
            'required' => false,

            'translation_domain' => false,
        ]);

        /*
         * Instellingen voor landingspagina
         */
        $builder->add('discount', ChoiceType::class, [
            'label' => 'Kortingscode',
            'required' => false,

            'expanded' => false,
            'multiple' => false,

            'choices' => $this->discountRepository->findAllForMultipleUse(),

            'choice_label' => function (Discount $discount) {
                return $discount->getTitle() ?: $discount->getCode();
            },

            'translation_domain' => false,
        ]);

        $builder->add('logo', FileType::class, [
            'label' => 'Logo',
            'required' => false,

            'error_bubbling' => true,       // TODO: meh.
            'translation_domain' => false,
        ]);

        if (null !== $request->getReferrer()->getLogo()) {
            $builder->add('delete_logo', CheckboxType::class, [
                'label' => 'Logo verwijderen',
                'required' => false,

                'translation_domain' => false,
            ]);
        }

        $builder->add('templates', TemplateType::class, [
            'label' => 'Gekoppelde templates',

            'multiple' => true,
            'expanded' => true,
            'profile' => $profile,
        ]);

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public static function getRequestClass(): string
    {
        return EditReferrerRequest::class;
    }
}
