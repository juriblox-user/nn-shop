<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type\Financial;

use Core\Application\CoreBundle\Forms\Type\MoneyValueObjectType;
use Core\Domain\Value\Web\EmailAddress;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use NnShop\Application\AppBundle\Forms\AbstractType;
use NnShop\Application\AppBundle\Forms\Backend\Request\Financial\EditDiscountRequest;
use NnShop\Domain\Orders\Enumeration\DiscountType;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Infrastructure\Templates\Repository\TemplateDoctrineRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class EditDiscountType extends AbstractType implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'label' => 'Titel',
            'translation_domain' => false,
        ]);

        $builder->add('code', TextType::class, [
            'label' => 'Kortingscode',
            'translation_domain' => false,
        ]);

        $builder->add('type', ChoiceType::class, [
            'label' => 'Type korting',
            'translation_domain' => false,

            'expanded' => true,
            'multiple' => false,

            'choices' => [
                'Percentage' => DiscountType::from(DiscountType::PERCENTAGE),
                'Vast bedrag' => DiscountType::from(DiscountType::FIXED),
            ],

            'choice_value' => function (DiscountType $type) {
                return $type->getValue();
            },
        ]);

        $builder->add('amount', MoneyValueObjectType::class, [
            'label' => 'Kortingsbedrag',
            'translation_domain' => false,

            'required' => false,
        ]);

        $builder->add('percentage', IntegerType::class, [
            'label' => 'Kortingspercentage',
            'translation_domain' => false,

            'required' => false,

            'attr' => [
                'min' => 0,
                'max' => 100,
            ],
        ]);

        $builder->add('maxUses', IntegerType::class, [
            'label' => 'Aantal',
            'translation_domain' => false,

            'required' => false,

            'attr' => [
                'min' => 1,
            ],
        ]);

        $builder->add('countUses', IntegerType::class, [
            'label' => 'Aantal gebruik',
            'translation_domain' => false,
            'attr' => ['readonly' => true],
            'required' => false,
        ]);

        $builder->add('active', ChoiceType::class, [
            'label' => 'Ingeschakeld',
            'translation_domain' => false,
            'required' => true,
            'choices' => [
                'Ja' => true,
                'Nee' => false,
            ],
        ]);

        $builder->add('hasTemplates', CheckboxType::class, [
            'label' => 'Koppelen aan template(s)',
            'required' => false,
        ]);

        $builder->add('templates', EntityType::class, [
            'class' => Template::class,
            'label' => 'Templates',
            'expanded' => false,
            'multiple' => true,
            'group_by' => 'profile',
            'required' => false,
            'query_builder' => function (TemplateDoctrineRepository $er) {
                return $er->getDiscountFormTemplates();
            },
        ]);

        $builder->add('hasEmails', CheckboxType::class, [
            'label' => 'Koppelen aan emailadres(sen)',
            'required' => false,
        ]);

        $builder->add('emails', TextareaType::class, [
            'attr' => ['rows' => 8],
            'required' => false,
        ]);

        $builder->get('emails')->addModelTransformer($this);

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public static function getRequestClass(): string
    {
        return EditDiscountRequest::class;
    }

    /**
     * {@inheritdoc}
     *
     * @param Collection $value
     *
     * @return string
     */
    public function transform($value)
    {
        return implode(";\r\n", $value->toArray());
    }

    /**
     * {@inheritdoc}
     *
     * @param string $value
     *
     * @return ArrayCollection
     */
    public function reverseTransform($value)
    {
        $result = new ArrayCollection();
        $parsedData = explode(';', str_replace(["\r", "\n", ' '], ['', '', ''], $value));

        foreach ($parsedData as $emailData) {
            if ('' !== $emailData) {
                try {
                    $result->add(EmailAddress::from($emailData));
                } catch (\InvalidArgumentException $e) {
                    throw new TransformationFailedException($e->getMessage());
                }
            }
        }

        return $result;
    }
}
