<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type\Translation;

use NnShop\Application\AppBundle\Forms\AbstractType;
use NnShop\Application\AppBundle\Forms\Backend\Request\Translation\EditLocaleRequest;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class EditLocaleType.
 */
class EditLocaleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('locale', LanguageType::class, [
            'label' => 'Taal',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public static function getRequestClass(): string
    {
        return EditLocaleRequest::class;
    }
}
