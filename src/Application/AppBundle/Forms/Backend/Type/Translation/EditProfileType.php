<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Type\Translation;

use Doctrine\ORM\EntityRepository;
use NnShop\Application\AppBundle\Forms\AbstractType;
use NnShop\Application\AppBundle\Forms\Backend\Request\Translation\EditProfileRequest;
use NnShop\Domain\Translation\Entity\Locale;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimezoneType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class EditProfileType.
 */
class EditProfileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('country', CountryType::class, [
            'label' => 'Land',
        ])
            ->add('timezone', TimezoneType::class, [
                'label' => 'Tijdzone',
            ])
            ->add('currency', CurrencyType::class, [
                'label' => 'Valuta',
            ])
            ->add('mainLocale', EntityType::class, [
                'label' => 'Hoofdtaal',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('l')
                        ->orderBy('l.locale', 'ASC');
                },
                'class' => Locale::class,
            ])
            ->add('url', UrlType::class, [
                'label' => 'Url',
            ])
            ->add('locales', EntityType::class, [
                'label' => 'Talen',
                'disabled' => true,
                'multiple' => true,
                'expanded' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('l')
                        ->orderBy('l.locale', 'ASC');
                },
                'class' => Locale::class,
            ])
            ->add('mollieKey', TextType::class, [
                'label' => 'Mollie API key',
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-mailadres',
            ])
            ->add('phone', TextType::class, [
                'label' => 'Telefoonnummer',
            ])
            ->add('entityName', TextType::class, [
                'label' => 'Naam van de entiteit',
            ])
            ->add('entityCountry', TextType::class, [
                'label' => 'Land van de entiteit',
            ])
            ->add('addressLine1', TextType::class, [
                'label' => 'Adresregel 1',
            ])
            ->add('addressLine2', TextType::class, [
                'label' => 'Adresregel 2',
                'required' => false,
            ])
            ->add('zipcode', TextType::class, [
                'label' => 'Postcode',
            ])
            ->add('city', TextType::class, [
                'label' => 'Stad',
            ])
            ->add('province', TextType::class, [
                'label' => 'Provincie/Staat',
                'required' => false,
            ])
            ->add('vatLabel', TextType::class, [
                'label' => 'Btw label',
            ])
            ->add('vatNumber', TextType::class, [
                'label' => 'Btw nummer',
            ])
            ->add('cocLabel', TextType::class, [
                'label' => 'KVK label',
            ])
            ->add('cocNumber', TextType::class, [
                'label' => 'KVK nummer',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public static function getRequestClass(): string
    {
        return EditProfileRequest::class;
    }
}
