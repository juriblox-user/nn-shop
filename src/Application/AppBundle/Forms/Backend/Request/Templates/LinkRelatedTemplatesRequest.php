<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Templates;

use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\RelatedTemplatesData;
use NnShop\Domain\Templates\Entity\Template;
use Symfony\Component\Form\Form;

final class LinkRelatedTemplatesRequest extends AbstractRequest
{
    /**
     * @var RelatedTemplatesData
     */
    private $related;

    /**
     * @var Template
     */
    private $template;

    /**
     * LinkCategoriesRequest constructor.
     */
    public function __construct(Template $template)
    {
        $this->template = $template;

        parent::__construct();
    }

    public function configure(): void
    {
        $this->related = new RelatedTemplatesData(new ArrayCollection($this->template->getRelated()));
    }

    public function getForm(): Form
    {
        return $this->form;
    }

    public function getRelated(): RelatedTemplatesData
    {
        return $this->related;
    }

    public function getTemplate(): Template
    {
        return $this->template;
    }

    public function setRelated(RelatedTemplatesData $related): void
    {
        $this->related = $related;
    }
}
