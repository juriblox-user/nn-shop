<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Templates;

use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\CategoriesData;
use NnShop\Domain\Templates\Entity\Template;
use Symfony\Component\Form\Form;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class LinkCategoriesRequest extends AbstractRequest
{
    /**
     * @var CategoriesData
     */
    private $categories;

    /**
     * @var Template
     */
    private $template;

    /**
     * LinkCategoriesRequest constructor.
     *
     * @param Template $template
     */
    public function __construct(Template $template)
    {
        $this->template = $template;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        if ($this->template->hasCategories()) {
            $this->categories = new CategoriesData($this->template->getCategories(), $this->template->getPrimaryCategory());
        }
    }

    /**
     * @return Form
     */
    public function getForm(): Form
    {
        return $this->form;
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->template;
    }

    /**
     * @return CategoriesData
     */
    public function getCategories(): CategoriesData
    {
        return $this->categories;
    }

    /**
     * @param CategoriesData $categories
     */
    public function setCategories(CategoriesData $categories)
    {
        $this->categories = $categories;
    }

    /**
     * @Assert\Callback
     *
     * @param ExecutionContextInterface $context
     * @param                           $payload
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if ($this->categories->getSelected()->isEmpty()) {
            $context->buildViolation('U moet minimaal één categorie koppelen voor dit template.')
                ->atPath('categories')
                ->addViolation();
        }

        // Primaire categorie
        elseif (null === $this->categories->getPrimary()) {
            $context->buildViolation('U moet een primaire categorie voor dit template kiezen.')
                ->atPath('categories')
                ->addViolation();
        }

        // Primaire categorie moet wel een gekozen categorie zijn
        elseif (!$this->categories->getSelected()->contains($this->categories->getPrimary())) {
            $context->buildViolation('De gekozen primaire categorie moet wel zijn gekoppeld aan het template.')
                ->atPath('categories')
                ->addViolation();
        }
    }
}
