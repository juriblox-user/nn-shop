<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Templates;

use Doctrine\Common\Collections\ArrayCollection;
use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Application\AppBundle\Forms\Backend\Type\CategoriesData;
use NnShop\Domain\Templates\Entity\Template;
use Money\Money;
use Symfony\Component\Form\Form;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class PublishTemplateRequest extends AbstractRequest
{
    /**
     * @var CategoriesData
     */
    private $categories;

    /**
     * @var Money
     */
    private $checkPrice;

    /**
     * @var Money
     */
    private $monthlyPrice;

    /**
     * @var bool
     */
    private $subscription;

    /**
     * @var Template
     */
    private $template;

    /**
     * @var string
     */
    private $title;

    /**
     * @var Money
     */
    private $unitPrice;

    /**
     * @var bool
     */
    private $hidden;

    /**
     * @var bool
     */
    private $service;

    /**
     * PublishTemplateRequest constructor.
     *
     * @param Template $template
     */
    public function __construct(Template $template)
    {
        $this->template = $template;

        $this->categories = new CategoriesData(new ArrayCollection());

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->subscription = null !== $this->template->getMonthlyPrice();
        if ($this->subscription) {
            $this->monthlyPrice = $this->template->getMonthlyPrice();
        }

        $this->unitPrice = $this->template->getUnitPrice();
        $this->checkPrice = $this->template->getCheckPrice();

        $this->title = $this->template->getTitle();
        $this->hidden = $this->template->isHidden();

        $this->service = $this->template->isService();

        if ($this->template->hasCategories()) {
            $this->categories = new CategoriesData($this->template->getCategories(), $this->template->getPrimaryCategory());
        }
    }

    /**
     * @return Money|null
     */
    public function getCheckPrice()
    {
        return $this->checkPrice;
    }

    /**
     * @return Form
     */
    public function getForm(): Form
    {
        return $this->form;
    }

    /**
     * @return Money|null
     */
    public function getMonthlyPrice()
    {
        return $this->monthlyPrice;
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->template;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Money|null
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @return bool
     */
    public function isSubscription(): bool
    {
        return $this->subscription;
    }

    /**
     * @return CategoriesData
     */
    public function getCategories(): CategoriesData
    {
        return $this->categories;
    }

    /**
     * @param CategoriesData $categories
     */
    public function setCategories(CategoriesData $categories)
    {
        $this->categories = $categories;
    }

    /**
     * @param Money $checkPrice
     */
    public function setCheckPrice(Money $checkPrice)
    {
        $this->checkPrice = $checkPrice;
    }

    /**
     * @param Money $monthlyPrice
     */
    public function setMonthlyPrice(Money $monthlyPrice)
    {
        $this->monthlyPrice = $monthlyPrice;
    }

    /**
     * @param bool $subscription
     */
    public function setSubscription(bool $subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * @param Money $unitPrice
     */
    public function setUnitPrice(Money $unitPrice)
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @return bool
     */
    public function isHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * @param bool $hidden
     */
    public function setHidden(bool $hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * @Assert\Callback
     *
     * @param ExecutionContextInterface $context
     * @param                           $payload
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if ($this->subscription && null === $this->monthlyPrice) {
            $context->buildViolation('U moet een maandbedrag opgeven als dit template als abonnement beschikbaar moet worden.')
                ->atPath('monthlyPrice')
                ->addViolation();
        }

        /*
         * Gekoppelde categorieën
         */
        if ($this->categories->getSelected()->isEmpty()) {
            $context->buildViolation('U moet minimaal één categorie koppelen voor dit template.')
                ->atPath('categories')
                ->addViolation();
        }

        // Primaire categorie
        elseif (null === $this->categories->getPrimary()) {
            $context->buildViolation('U moet een primaire categorie voor dit template kiezen.')
                ->atPath('categories')
                ->addViolation();
        }

        // Primaire categorie moet wel een gekozen categorie zijn
        elseif (!$this->categories->getSelected()->contains($this->categories->getPrimary())) {
            $context->buildViolation('De gekozen primaire categorie moet wel zijn gekoppeld aan het template.')
                ->atPath('categories')
                ->addViolation();
        }
    }

    /**
     * @return bool
     */
    public function isService(): bool
    {
        return $this->service;
    }

    /**
     * @param bool $service
     */
    public function setService(bool $service)
    {
        $this->service = $service;
    }
}
