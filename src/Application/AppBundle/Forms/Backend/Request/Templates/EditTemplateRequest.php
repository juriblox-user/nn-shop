<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Templates;

use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Domain\Templates\Entity\Template;
use Money\Money;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class EditTemplateRequest extends AbstractRequest
{
    /**
     * @var Money
     */
    private $checkPrice;

    /**
     * @var string
     */
    private $description;

    /**
     * @Assert\Length(max="160", maxMessage="Een korte beschrijving mag maximaal 160 tekens bevatten.")
     *
     * @var string
     */
    private $excerpt;

    /**
     * @var bool
     */
    private $hidden;

    /**
     * @var Money
     */
    private $monthlyPrice;

    private $preview;

    /**
     * @var Money|null
     */
    private $registeredEmailPrice;

    /**
     * @var string
     */
    private $remoteDescription;

    /**
     * @Assert\File(
     *     mimeTypes={"application/pdf", "application/x-pdf"},
     *     mimeTypesMessage="Upload een geldig PDF-bestand."
     * )
     *
     * @var File|null
     */
    private $sample;

    /**
     * @var bool
     */
    private $service;

    /**
     * @var bool
     */
    private $spotlight;

    /**
     * @var bool
     */
    private $subscription;

    /*
     * @Assert\File(
     *  mimeTypes={"application/image", "application/},
     *  mimeTypesMessage="Upload een geldige afbeelding."
     * )
     * @var File|null;
     */

    /**
     * @var Template
     */
    private $template;

    /**
     * @var string
     */
    private $title;

    /**
     * @var Money
     */
    private $unitPrice;

    /**
     * EditTemplateRequest constructor.
     *
     * @param Template $template
     */
    public function __construct(Template $template)
    {
        $this->template = $template;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->spotlight = $this->template->isSpotlight();
        $this->subscription = null !== $this->template->getMonthlyPrice();

        if ($this->subscription) {
            $this->monthlyPrice = $this->template->getMonthlyPrice();
        }

        $this->unitPrice = $this->template->getUnitPrice();
        $this->checkPrice = $this->template->getCheckPrice();
        $this->registeredEmailPrice = $this->template->getRegisteredEmailPrice();

        $this->title = $this->template->getTitle();

        $this->excerpt = $this->template->getExcerpt();

        $this->hidden = $this->template->isHidden();

        $this->service = $this->template->isService();

        $this->description = $this->template->getDescription();
        $this->remoteDescription = $this->template->getRemoteDescription();
    }

    /**
     * @return Money
     */
    public function getCheckPrice(): Money
    {
        return $this->checkPrice;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getExcerpt()
    {
        return $this->excerpt;
    }

    /**
     * @return Form
     */
    public function getForm(): Form
    {
        return $this->form;
    }

    /**
     * @return Money|null
     */
    public function getMonthlyPrice()
    {
        return $this->monthlyPrice;
    }

    /**
     * @return null|File
     */
    public function getPreview()
    {
        return $this->preview;
    }

    public function getRegisteredEmailPrice(): ?Money
    {
        return $this->registeredEmailPrice;
    }

    /**
     * @return string
     */
    public function getRemoteDescription()
    {
        return $this->remoteDescription;
    }

    /**
     * @return File|null
     */
    public function getSample()
    {
        return $this->sample;
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->template;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Money|null
     */
    public function getUnitPrice(): Money
    {
        return $this->unitPrice;
    }

    /**
     * @return bool
     */
    public function hasSample(): bool
    {
        return null !== $this->sample;
    }

    /**
     * @return bool
     */
    public function isHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * @return bool
     */
    public function isService(): bool
    {
        return $this->service;
    }

    /**
     * @return bool
     */
    public function isSpotlight(): bool
    {
        return $this->spotlight;
    }

    /**
     * @return bool
     */
    public function isSubscription(): bool
    {
        return $this->subscription;
    }

    /**
     * @param Money $checkPrice
     */
    public function setCheckPrice(Money $checkPrice)
    {
        $this->checkPrice = $checkPrice;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description ?: null;
    }

    /**
     * @param string $excerpt
     */
    public function setExcerpt(string $excerpt)
    {
        $this->excerpt = $excerpt ?: null;
    }

    /**
     * @param bool $hidden
     */
    public function setHidden(bool $hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * @param Money $monthlyPrice
     */
    public function setMonthlyPrice(Money $monthlyPrice)
    {
        $this->monthlyPrice = $monthlyPrice;
    }

    /**
     * @param File $preview
     */
    public function setPreview(File $preview)
    {
        $this->preview = $preview;
    }

    public function setRegisteredEmailPrice(?Money $registeredEmailPrice): void
    {
        $this->registeredEmailPrice = $registeredEmailPrice;
    }

    /**
     * @param string $remoteDescription
     */
    public function setRemoteDescription($remoteDescription)
    {
        $this->remoteDescription = $remoteDescription ?: null;
    }

    /**
     * @param mixed $sample
     */
    public function setSample(File $sample)
    {
        $this->sample = $sample;
    }

    /**
     * @param bool $service
     */
    public function setService(bool $service)
    {
        $this->service = $service;
    }

    /**
     * @param bool $spotlight
     */
    public function setSpotlight(bool $spotlight)
    {
        $this->spotlight = $spotlight;
    }

    /**
     * @param bool $subscription
     */
    public function setSubscription(bool $subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * @param Money $unitPrice
     */
    public function setUnitPrice(Money $unitPrice)
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @Assert\Callback
     *
     * @param ExecutionContextInterface $context
     * @param                           $payload
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if ($this->subscription && null === $this->monthlyPrice) {
            $context->buildViolation('U moet een maandbedrag opgeven als dit template als abonnement beschikbaar moet worden.')
                ->atPath('monthlyPrice')
                ->addViolation();
        }
    }


}
