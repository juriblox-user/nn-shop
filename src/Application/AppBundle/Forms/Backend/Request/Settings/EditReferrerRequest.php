<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Settings;

use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Domain\Orders\Entity\Discount;
use NnShop\Domain\Shops\Entity\Referrer;
use NnShop\Domain\Templates\Entity\Template;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class EditReferrerRequest extends AbstractRequest
{
    /**
     * @var bool
     */
    private $deleteLogo;

    /**
     * @var Discount
     */
    private $discount;

    /**
     * @var bool
     */
    private $homepage;

    /**
     * @var bool
     */
    private $kickback;

    /**
     * @var bool
     */
    private $landing;

    /**
     * @Assert\Image(
     *     detectCorrupted=true,
     *     corruptedMessage="Dit lijkt geen geldig afbeeldingsbestand te zijn."
     * )
     *
     * @var File|null
     */
    private $logo;

    /**
     * @var Referrer
     */
    private $referrer;

    /**
     * @var Template[]
     */
    private $templates;

    /**
     * @Assert\NotBlank
     *
     * @var string
     */
    private $text;

    /**
     * @Assert\NotBlank
     *
     * @var string
     */
    private $title;

    /**
     * @Assert\Url
     *
     * @var string
     */
    private $website;

    /**
     * @param Referrer $referrer
     */
    public function __construct(Referrer $referrer)
    {
        $this->referrer = $referrer;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->title = $this->referrer->getTitle();
        $this->text = $this->referrer->getText();
        $this->website = $this->referrer->getWebsite();

        $this->homepage = $this->referrer->hasHomepage();
        $this->kickback = $this->referrer->hasKickback();
        $this->landing = $this->referrer->hasLanding();

        if ($this->referrer->hasDiscount()) {
            $this->discount = $this->referrer->getDiscount();
        }

        $this->templates = $this->referrer->getTemplates()->toArray();

        $this->deleteLogo = false;
    }

    /**
     * @return bool
     */
    public function deleteLogo(): bool
    {
        return $this->deleteLogo;
    }

    /**
     * @return Discount|null
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @return null|File
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @return Referrer
     */
    public function getReferrer(): Referrer
    {
        return $this->referrer;
    }

    /**
     * @return Template[]
     */
    public function getTemplates(): array
    {
        return $this->templates;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @return bool
     */
    public function hasHomepage()
    {
        return $this->homepage;
    }

    /**
     * @return bool
     */
    public function hasKickback(): bool
    {
        return $this->kickback;
    }

    /**
     * @return bool
     */
    public function hasLanding(): bool
    {
        return $this->landing;
    }

    /**
     * @param bool $deleteLogo
     */
    public function setDeleteLogo(bool $deleteLogo)
    {
        $this->deleteLogo = $deleteLogo;
    }

    /**
     * @param Discount|null $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @param bool $homepage
     */
    public function setHomepage(bool $homepage)
    {
        $this->homepage = $homepage;
    }

    /**
     * @param bool $kickback
     */
    public function setKickback(bool $kickback)
    {
        $this->kickback = $kickback;
    }

    /**
     * @param bool $landing
     */
    public function setLanding(bool $landing)
    {
        $this->landing = $landing;
    }

    /**
     * @param File $logo
     */
    public function setLogo(File $logo)
    {
        $this->logo = $logo;
    }

    /**
     * @param Template[] $templates
     */
    public function setTemplates(array $templates)
    {
        $this->templates = $templates;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text ?: null;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * @param string $website
     */
    public function setWebsite($website)
    {
        $this->website = $website ?: null;
    }

    /**
     * @Assert\Callback
     *
     * @param ExecutionContextInterface $context
     * @param                           $payload
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        //        if ($this->syncEnabled) {
//            if (null === $this->syncClientId) {
//                $context->buildViolation('U moet het API client ID opgeven als u wilt dat de templates van deze kennispartner worden gesynchroniseerd.')
//                    ->atPath('syncClientId')
//                    ->addViolation();
//            }
//
//            if (null === $this->syncClientKey) {
//                $context->buildViolation('U moet de API client key opgeven als u wilt dat de templates van deze kennispartner worden gesynchroniseerd.')
//                    ->atPath('syncClientKey')
//                    ->addViolation();
//            }
//        }
    }
}
