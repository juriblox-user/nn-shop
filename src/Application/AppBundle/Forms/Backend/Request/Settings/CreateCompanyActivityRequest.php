<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Settings;

use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Translation\Entity\Profile;
use Symfony\Component\Validator\Constraints as Assert;

class CreateCompanyActivityRequest extends AbstractRequest
{
    /**
     * @Assert\Count(min=1, minMessage="U moet minimaal 1 template selecteren dat aan deze activiteit wordt gekoppeld.")
     *
     * @var array|Template[]
     */
    private $templates;

    /**
     * @var string
     */
    private $title;

    /**
     * @var Profile
     */
    private $profile;

    /**
     * CreateCompanyActivityRequest constructor.
     */
    public function __construct(Profile $profile)
    {
        parent::__construct();

        $this->templates = [];
        $this->profile = $profile;
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
    }

    /**
     * @return array|Template[]
     */
    public function getTemplates()
    {
        return $this->templates;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param array|Template[] $templates
     */
    public function setTemplates(array $templates)
    {
        $this->templates = $templates;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * @return Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }
}
