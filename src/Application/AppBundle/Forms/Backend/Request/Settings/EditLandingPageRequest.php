<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Settings;

use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Domain\Orders\Entity\Discount;
use NnShop\Domain\Shops\Entity\LandingPage;
use NnShop\Domain\Templates\Entity\Template;
use Symfony\Component\Validator\Constraints as Assert;

class EditLandingPageRequest extends AbstractRequest
{
    /**
     * @var Discount
     */
    private $discount;

    /**
     * @var LandingPage
     */
    private $landingPage;

    /**
     * @var Template[]
     */
    private $templates;

    /**
     * @Assert\NotBlank
     *
     * @var string
     */
    private $text;

    /**
     * @Assert\NotBlank
     *
     * @var string
     */
    private $title;

    /**
     * Constructor.
     *
     * @param LandingPage $landingPage
     */
    public function __construct(LandingPage $landingPage)
    {
        $this->landingPage = $landingPage;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->title = $this->landingPage->getTitle();
        $this->text = $this->landingPage->getText();

        if ($this->landingPage->hasDiscount()) {
            $this->discount = $this->landingPage->getDiscount();
        }

        $this->templates = $this->landingPage->getTemplates()->toArray();
    }

    /**
     * @return Discount|null
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @return LandingPage
     */
    public function getLandingPage(): LandingPage
    {
        return $this->landingPage;
    }

    /**
     * @return Template[]
     */
    public function getTemplates(): array
    {
        return $this->templates;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param Discount|null $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @param Template[] $templates
     */
    public function setTemplates(array $templates)
    {
        $this->templates = $templates;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text ?: null;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }
}
