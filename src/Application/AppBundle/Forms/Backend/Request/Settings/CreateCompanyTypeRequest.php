<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Settings;

use NnShop\Application\AppBundle\Forms\AbstractRequest;

class CreateCompanyTypeRequest extends AbstractRequest
{
    /**
     * @var string|null
     */
    private $description;

    /**
     * @var string|null
     */
    private $title;

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description ?: null;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }
}
