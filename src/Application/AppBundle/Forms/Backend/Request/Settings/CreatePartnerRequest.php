<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Settings;

use NnShop\Application\AppBundle\Forms\AbstractRequest;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class CreatePartnerRequest extends AbstractRequest
{
    /**
     * @var string
     */
    private $excerpt;

    /**
     * @Assert\Image(
     *     detectCorrupted=true,
     *     corruptedMessage="Dit lijkt geen geldig afbeeldingsbestand te zijn."
     * )
     *
     * @Assert\NotBlank(message="Een partner moet een logo hebben.")
     *
     * @var File
     */
    private $logo;

    /**
     * @var string
     */
    private $profile;

    /**
     * @var int
     */
    private $remoteId;

    /**
     * @var string
     */
    private $syncClientId;

    /**
     * @var string
     */
    private $syncClientKey;

    /**
     * @var bool
     */
    private $syncEnabled;

    /**
     * @var string
     */
    private $title;

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->syncEnabled = false;
    }

    /**
     * @return string
     */
    public function getExcerpt()
    {
        return $this->excerpt;
    }

    /**
     * @return File
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @return string
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @return int|null
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * @return string
     */
    public function getSyncClientId()
    {
        return $this->syncClientId;
    }

    /**
     * @return string
     */
    public function getSyncClientKey()
    {
        return $this->syncClientKey;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function isSyncEnabled(): bool
    {
        return $this->syncEnabled;
    }

    /**
     * @param string $excerpt
     */
    public function setExcerpt($excerpt)
    {
        $this->excerpt = $excerpt ?: null;
    }

    /**
     * @param File $logo
     */
    public function setLogo(File $logo)
    {
        $this->logo = $logo;
    }

    /**
     * @param string $profile
     */
    public function setProfile($profile)
    {
        $this->profile = $profile ?: null;
    }

    /**
     * @param int $remoteId
     */
    public function setRemoteId(int $remoteId)
    {
        $this->remoteId = $remoteId;
    }

    /**
     * @param string $syncClientId
     */
    public function setSyncClientId($syncClientId)
    {
        $this->syncClientId = $syncClientId ?: null;
    }

    /**
     * @param string $syncClientKey
     */
    public function setSyncClientKey($syncClientKey)
    {
        $this->syncClientKey = $syncClientKey ?: null;
    }

    /**
     * @param bool $syncEnabled
     */
    public function setSyncEnabled(bool $syncEnabled)
    {
        $this->syncEnabled = $syncEnabled;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * @Assert\Callback
     *
     * @param ExecutionContextInterface $context
     * @param                           $payload
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if ($this->syncEnabled) {
            if (null === $this->syncClientId) {
                $context->buildViolation('U moet het API client ID opgeven als u wilt dat de templates van deze kennispartner worden gesynchroniseerd.')
                    ->atPath('syncClientId')
                    ->addViolation();
            }

            if (null === $this->syncClientKey) {
                $context->buildViolation('U moet de API client key opgeven als u wilt dat de templates van deze kennispartner worden gesynchroniseerd.')
                    ->atPath('syncClientKey')
                    ->addViolation();
            }
        }
    }
}
