<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Settings;

use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Domain\Shops\Entity\Shop;

class CreateCategoryRequest extends AbstractRequest
{
    /**
     * @var Shop
     */
    private $shop;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string | null
     */
    private $description;

    /**
     * @var string | null
     */
    private $metaDescription;

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
    }

    /**
     * @return Shop|null
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param Shop $shop
     */
    public function setShop(Shop $shop)
    {
        $this->shop = $shop;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * @param string|null $metaDescription
     */
    public function setMetaDescription(?string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }
}
