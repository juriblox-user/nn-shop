<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Settings;

use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Domain\Mollie\Data\AllowedPaymentMethod;

/**
 * Class EditPaymentMethodRequest.
 */
class EditPaymentMethodRequest extends AbstractRequest
{
    /**
     * @var string
     */
    private $method;

    /**
     * @var bool
     */
    private $enabled;

    /**
     * @var bool
     */
    private $externallyDisabled;

    /**
     * @var AllowedPaymentMethod
     */
    private $methodObject;

    /**
     * EditCategoryRequest constructor.
     *
     * @param AllowedPaymentMethod $methodObject
     */
    public function __construct(AllowedPaymentMethod $methodObject)
    {
        $this->methodObject = $methodObject;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->method = $this->methodObject->getMethod()->description;
        $this->enabled = $this->methodObject->isEnabled();
        $this->externallyDisabled = $this->methodObject->isExternallyDisabled();
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     *
     * @return EditPaymentMethodRequest
     */
    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return bool
     */
    public function isExternallyDisabled(): bool
    {
        return $this->externallyDisabled;
    }

    /**
     * @param bool $externallyDisabled
     *
     * @return EditPaymentMethodRequest
     */
    public function setExternallyDisabled(bool $externallyDisabled)
    {
        $this->externallyDisabled = $externallyDisabled;

        return $this;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     *
     * @return EditPaymentMethodRequest
     */
    public function setMethod(string $method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @return AllowedPaymentMethod
     */
    public function getMethodObject(): AllowedPaymentMethod
    {
        return $this->methodObject;
    }
}
