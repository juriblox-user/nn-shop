<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Settings;

use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Domain\Shops\Entity\Shop;
use Symfony\Component\Validator\Constraints as Assert;

class EditShopRequest extends AbstractRequest
{
    /**
     * @var bool
     */
    private $enabled;

    /**
     * @Assert\Regex(
     *     pattern="/^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)*[a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?$/",
     *     htmlPattern="^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)*[a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?$",
     *     message="De domeinnaam lijkt niet geldig te zijn."
     * )
     *
     * @var string
     */
    private $hostname;

    /**
     * @var Shop
     */
    private $shop;

    /**
     * @var string
     */
    private $title;

    /**
     * @Assert\Length(max="160", maxMessage="Een meta-description mag maximaal 160 tekens bevatten.")
     *
     * @var string
     */
    private $metaDescription;

    /**
     * @var string
     */
    private $description;

    /**
     * EditShopRequest constructor.
     *
     * @param Shop $shop
     */
    public function __construct(Shop $shop)
    {
        $this->shop = $shop;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->title = $this->shop->getTitle();
        $this->hostname = $this->shop->getHostname();
        $this->metaDescription = $this->shop->getMetaDescription();
        $this->description = $this->shop->getDescription();

        $this->enabled = $this->shop->isEnabled();
    }

    /**
     * @return string
     */
    public function getHostname()
    {
        return $this->hostname;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @param string $hostname
     */
    public function setHostname($hostname)
    {
        $this->hostname = $hostname ?: null;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription ?: null;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description ?: null;
    }
}
