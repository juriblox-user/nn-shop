<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Settings;

use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Domain\Templates\Entity\CompanyActivity;
use NnShop\Domain\Templates\Entity\Template;

class EditCompanyActivityRequest extends AbstractRequest
{
    /**
     * @var CompanyActivity
     */
    private $companyActivity;

    /**
     * @var array|Template[]
     */
    private $templates;

    /**
     * @var string
     */
    private $title;

    /**
     * EditCompanyActivityRequest constructor.
     *
     * @param CompanyActivity $companyActivity
     */
    public function __construct(CompanyActivity $companyActivity)
    {
        $this->companyActivity = $companyActivity;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->title = $this->companyActivity->getTitle();
        $this->templates = $this->companyActivity->getTemplates()->toArray();
    }

    /**
     * @return array|Template[]
     */
    public function getTemplates(): array
    {
        return $this->templates;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param array|Template[] $templates
     */
    public function setTemplates($templates)
    {
        $this->templates = $templates;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * @return CompanyActivity
     */
    public function getCompanyActivity()
    {
        return $this->companyActivity;
    }
}
