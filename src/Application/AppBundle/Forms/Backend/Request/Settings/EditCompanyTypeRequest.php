<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Settings;

use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Domain\Templates\Entity\CompanyType;

class EditCompanyTypeRequest extends AbstractRequest
{
    /**
     * @var CompanyType
     */
    private $companyType;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var string|null
     */
    private $title;

    /**
     * EditCompanyTypeRequest constructor.
     *
     * @param CompanyType $companyType
     */
    public function __construct(CompanyType $companyType)
    {
        $this->companyType = $companyType;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->title = $this->companyType->getTitle();
        $this->description = $this->companyType->getDescription();
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description ?: null;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }
}
