<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Settings;

use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Domain\Orders\Entity\Discount;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Translation\Entity\Profile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class CreateReferrerRequest extends AbstractRequest
{
    /**
     * @var Discount
     */
    private $discount;

    /**
     * @var bool
     */
    private $homepage;

    /**
     * @var bool
     */
    private $kickback;

    /**
     * @var bool
     */
    private $landing;

    /**
     * @Assert\NotBlank
     * @Assert\Image(
     *     detectCorrupted=true,
     *     corruptedMessage="Dit lijkt geen geldig afbeeldingsbestand te zijn."
     * )
     *
     * @var File|null
     */
    private $logo;

    /**
     * @var Template[]
     */
    private $templates;

    /**
     * @var string|null
     */
    private $text;

    /**
     * @var string|null
     */
    private $title;

    /**
     * @Assert\Url
     *
     * @var string
     */
    private $website;

    /**
     * @var Profile
     */
    private $profile;

    /**
     * CreateReferrerRequest constructor.
     *
     * @param Profile $profile
     */
    public function __construct(Profile $profile)
    {
        $this->profile = $profile;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->homepage = false;
        $this->landing = false;
        $this->kickback = false;

        $this->templates = [];
    }

    /**
     * @return Discount|null
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @return null|File
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @return Template[]
     */
    public function getTemplates(): array
    {
        return $this->templates;
    }

    /**
     * @return string|null
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @return bool
     */
    public function hasHomepage(): bool
    {
        return $this->homepage;
    }

    /**
     * @return bool
     */
    public function hasKickback(): bool
    {
        return $this->kickback;
    }

    /**
     * @return bool
     */
    public function hasLanding(): bool
    {
        return $this->landing;
    }

    /**
     * @param Discount $discount
     */
    public function setDiscount(Discount $discount)
    {
        $this->discount = $discount;
    }

    /**
     * @param bool $homepage
     */
    public function setHomepage(bool $homepage)
    {
        $this->homepage = $homepage;
    }

    /**
     * @param bool $kickback
     */
    public function setKickback(bool $kickback)
    {
        $this->kickback = $kickback;
    }

    /**
     * @param bool $landing
     */
    public function setLanding(bool $landing)
    {
        $this->landing = $landing;
    }

    /**
     * @param File $logo
     */
    public function setLogo(File $logo)
    {
        $this->logo = $logo;
    }

    /**
     * @param Template[] $templates
     */
    public function setTemplates(array $templates)
    {
        $this->templates = $templates;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text ?: null;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * @param string $website
     */
    public function setWebsite($website)
    {
        $this->website = $website ?: null;
    }

    /**
     * @Assert\Callback
     *
     * @param ExecutionContextInterface $context
     * @param                           $payload
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        //        if ($this->syncEnabled) {
//            if (null === $this->syncClientId) {
//                $context->buildViolation('U moet het API client ID opgeven als u wilt dat de templates van deze kennispartner worden gesynchroniseerd.')
//                    ->atPath('syncClientId')
//                    ->addViolation();
//            }
//
//            if (null === $this->syncClientKey) {
//                $context->buildViolation('U moet de API client key opgeven als u wilt dat de templates van deze kennispartner worden gesynchroniseerd.')
//                    ->atPath('syncClientKey')
//                    ->addViolation();
//            }
//        }
    }

    /**
     * @return Profile
     */
    public function getProfile(): Profile
    {
        return $this->profile;
    }
}
