<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Translation;

use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Domain\Translation\Entity\Locale;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CreateProfileRequest.
 */
class CreateProfileRequest extends AbstractRequest
{
    /**
     * @var null|string
     */
    private $country;

    /**
     * @var null|string
     */
    private $timezone;

    /**
     * @var null|string
     */
    private $currency;

    /**
     * @var Locale|null
     */
    private $mainLocale;

    /**
     * @var string|null
     */
    private $url;

    /**
     * @var array|null
     */
    private $locales;

    /**
     * @var null|string
     * @Assert\NotBlank(message="Vul a.u.b. een e-mailadres in")
     */
    private $email;

    /**
     * @var null|string
     * @Assert\NotBlank(message="Vul a.u.b. een telefoonnummer in")
     */
    private $phone;

    /**
     * @var null|string
     *                  Assert\NotBlank(message="Vul a.u.b. een Mollie API key in")
     */
    private $mollieKey;

    /**
     * @var string|null
     */
    private $entityName;

    /**
     * @var string|null
     */
    private $addressLine1;

    /**
     * @var string|null
     */
    private $addressLine2;

    /**
     * @var string|null
     */
    private $city;

    /**
     * @var string|null
     */
    private $province;

    /**
     * @var string|null
     */
    private $zipcode;

    /**
     * @var string|null
     */
    private $entityCountry;

    /**
     * @var string|null
     */
    private $vatLabel;

    /**
     * @var string|null
     */
    private $vatNumber;

    /**
     * @var string|null
     */
    private $cocLabel;

    /**
     * @var string|null
     */
    private $cocNumber;

    /**
     * @return null|string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country)
    {
        $this->country = $country;
    }

    /**
     * @return null|string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @param string $timezone
     */
    public function setTimezone(string $timezone)
    {
        $this->timezone = $timezone;
    }

    /**
     * @return null|string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return Locale|null
     */
    public function getMainLocale()
    {
        return $this->mainLocale;
    }

    /**
     * @param Locale|null $locale
     */
    public function setMainLocale(Locale $locale = null)
    {
        $this->mainLocale = $locale;
    }

    /**
     * @return null|string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return array|null
     */
    public function getLocales()
    {
        return $this->locales;
    }

    /**
     * @return null|string
     */
    public function getMollieKey()
    {
        return $this->mollieKey;
    }

    /**
     * @param string|null $mollieKey
     */
    public function setMollieKey($mollieKey)
    {
        $this->mollieKey = $mollieKey;
    }

    /**
     * @return null|string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param null|string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return null|string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param null|string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return null|string
     */
    public function getEntityName()
    {
        return $this->entityName;
    }

    /**
     * @param null|string $entityName
     */
    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;
    }

    /**
     * @return null|string
     */
    public function getAddressLine1()
    {
        return $this->addressLine1;
    }

    /**
     * @param null|string $addressLine1
     */
    public function setAddressLine1($addressLine1)
    {
        $this->addressLine1 = $addressLine1;
    }

    /**
     * @return null|string
     */
    public function getAddressLine2()
    {
        return $this->addressLine2;
    }

    /**
     * @param null|string $addressLine2
     */
    public function setAddressLine2($addressLine2)
    {
        $this->addressLine2 = $addressLine2;
    }

    /**
     * @return null|string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param null|string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return null|string
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * @param null|string $province
     */
    public function setProvince($province)
    {
        $this->province = $province;
    }

    /**
     * @return null|string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param null|string $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return null|string
     */
    public function getEntityCountry()
    {
        return $this->entityCountry;
    }

    /**
     * @param null|string $entityCountry
     */
    public function setEntityCountry($entityCountry)
    {
        $this->entityCountry = $entityCountry;
    }

    /**
     * @return null|string
     */
    public function getVatLabel()
    {
        return $this->vatLabel;
    }

    /**
     * @param null|string $vatLabel
     */
    public function setVatLabel($vatLabel)
    {
        $this->vatLabel = $vatLabel;
    }

    /**
     * @return null|string
     */
    public function getVatNumber()
    {
        return $this->vatNumber;
    }

    /**
     * @param null|string $vatNumber
     */
    public function setVatNumber($vatNumber)
    {
        $this->vatNumber = $vatNumber;
    }

    /**
     * @return null|string
     */
    public function getCocLabel()
    {
        return $this->cocLabel;
    }

    /**
     * @param null|string $cocLabel
     */
    public function setCocLabel($cocLabel)
    {
        $this->cocLabel = $cocLabel;
    }

    /**
     * @return null|string
     */
    public function getCocNumber()
    {
        return $this->cocNumber;
    }

    /**
     * @param null|string $cocNumber
     */
    public function setCocNumber($cocNumber)
    {
        $this->cocNumber = $cocNumber;
    }
}
