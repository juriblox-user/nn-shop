<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Translation;

use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Domain\Translation\Entity\Locale;

/**
 * Class EditLocaleRequest.
 */
class EditLocaleRequest extends AbstractRequest
{
    /**
     * @var null|string
     */
    private $locale;

    /**
     * @var Locale
     */
    private $entity;

    /**
     * EditLocaleRequest constructor.
     *
     * @param Locale $locale
     */
    public function __construct(Locale $locale)
    {
        $this->entity = $locale;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->locale = $this->entity->getLocale();
    }

    /**
     * @return null|string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale(string $locale)
    {
        $this->locale = $locale;
    }
}
