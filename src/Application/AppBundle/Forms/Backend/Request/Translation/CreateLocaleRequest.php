<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Translation;

use NnShop\Application\AppBundle\Forms\AbstractRequest;

/**
 * Class CreateLocaleRequest.
 */
class CreateLocaleRequest extends AbstractRequest
{
    /**
     * @var null|string
     */
    private $locale;

    /**
     * @return null|string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    public function setLocale(string $locale)
    {
        $this->locale = $locale;
    }
}
