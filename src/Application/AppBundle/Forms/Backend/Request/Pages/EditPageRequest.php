<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Pages;

use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Domain\Pages\Entity\Page;
use NnShop\Domain\Pages\Value\PageId;
use NnShop\Domain\Translation\Entity\Profile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class EditPageRequest.
 */
class EditPageRequest extends AbstractRequest
{
    /**
     * @var Page
     */
    private $page;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string|null
     */
    private $lead;

    /**
     * @var string|null
     */
    private $footer;

    /**
     * @var \DateTime
     */
    private $lastUpdated;

    /**
     * @Assert\File(
     *     mimeTypes={"application/pdf", "application/x-pdf"},
     *     mimeTypesMessage="Upload aub een geldig PDF"
     * )
     *
     * @var File|null
     */
    private $pdf;

    /**
     * @var bool
     */
    private $deletePdf;

    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var PageId
     */
    private $id;

    /**
     * PageRequest constructor.
     *
     * @param Page $page
     */
    public function __construct(Page $page)
    {
        $this->page = $page;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->id = $this->page->getId();
        $this->title = $this->page->getTitle();
        $this->code = $this->page->getCode();
        $this->content = $this->page->getContent();
        $this->lead = $this->page->getLead();
        $this->footer = $this->page->getFooter();
        $this->lastUpdated = $this->page->getLastUpdated();
        $this->pdf = $this->page->getPdf();
        $this->profile = $this->page->getProfile();

        $this->deletePdf = false;
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return EditPageRequest|PageRequest
     */
    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string|null
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     *
     * @return EditPageRequest|PageRequest
     */
    public function setContent(string $content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return Page
     */
    public function getPage(): Page
    {
        return $this->page;
    }

    /**
     * @return string|null
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * @param string|null $lead
     *
     * @return EditPageRequest|PageRequest
     */
    public function setLead(string $lead = null)
    {
        $this->lead = $lead;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getFooter()
    {
        return $this->footer;
    }

    /**
     * @param string|null $footer
     *
     * @return EditPageRequest|PageRequest
     */
    public function setFooter(string $footer = null)
    {
        $this->footer = $footer;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdated(): \DateTime
    {
        return $this->lastUpdated;
    }

    /**
     * @param \DateTime $lastUpdated
     *
     * @return EditPageRequest|PageRequest
     */
    public function setLastUpdated(\DateTime $lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    /**
     * @return bool
     */
    public function deletePdf(): bool
    {
        return $this->deletePdf;
    }

    /**
     * @param bool $deletePdf
     *
     * @return EditPageRequest|PageRequest
     */
    public function setDeletePdf(bool $deletePdf)
    {
        $this->deletePdf = $deletePdf;

        return $this;
    }

    /**
     * @return null|File
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * @param File|null $pdf
     *
     * @return EditPageRequest|PageRequest
     */
    public function setPdf(File $pdf = null)
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * @return Profile|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param string $code
     *
     * @return EditPageRequest
     */
    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return PageId
     */
    public function getId(): PageId
    {
        return $this->id;
    }
}
