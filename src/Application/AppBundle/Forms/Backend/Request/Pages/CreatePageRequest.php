<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Pages;

use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Domain\Translation\Entity\Profile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CreatePageRequest.
 */
class CreatePageRequest extends AbstractRequest
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string|null
     */
    private $lead;

    /**
     * @var string|null
     */
    private $footer;

    /**
     * @var \DateTime|nul
     */
    private $lastUpdated;

    /**
     * @Assert\File(
     *     mimeTypes={"application/pdf", "application/x-pdf"},
     *     mimeTypesMessage="Upload aub een geldig PDF"
     * )
     *
     * @var File|null
     */
    private $pdf;

    /**
     * @var Profile
     */
    private $profile;

    /**
     * CreatePageRequest constructor.
     *
     * @param Profile $profile
     */
    public function __construct(Profile $profile)
    {
        $this->profile = $profile;

        parent::__construct();
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return CreatePageRequest|PageRequest
     */
    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return CreatePageRequest
     */
    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     *
     * @return CreatePageRequest|PageRequest
     */
    public function setContent(string $content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * @param string|null $lead
     *
     * @return CreatePageRequest|PageRequest
     */
    public function setLead(string $lead = null)
    {
        $this->lead = $lead;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getFooter()
    {
        return $this->footer;
    }

    /**
     * @param string|null $footer
     *
     * @return CreatePageRequest|PageRequest
     */
    public function setFooter(string $footer = null)
    {
        $this->footer = $footer;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    /**
     * @param \DateTime $lastUpdated
     *
     * @return CreatePageRequest|PageRequest
     */
    public function setLastUpdated(\DateTime $lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    /**
     * @return null|File
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * @param File|null $pdf
     *
     * @return CreatePageRequest|PageRequest
     */
    public function setPdf(File $pdf = null)
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * @return Profile
     */
    public function getProfile(): Profile
    {
        return $this->profile;
    }
}
