<?php

namespace NnShop\Application\AppBundle\Forms\Backend\Request\Financial;

use Doctrine\Common\Collections\Collection;
use NnShop\Application\AppBundle\Forms\AbstractRequest;
use NnShop\Domain\Orders\Entity\Discount;
use NnShop\Domain\Orders\Enumeration\DiscountType;
use Money\Money;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class EditDiscountRequest extends AbstractRequest
{
    /**
     * @var Money
     */
    private $amount;

    /**
     * @var string
     */
    private $code;

    /**
     * @var Discount
     */
    private $discount;

    /**
     * @var int
     */
    private $percentage;

    /**
     * @var string
     */
    private $title;

    /**
     * @var DiscountType
     */
    private $type;

    /**
     * @var int
     */
    private $maxUses;

    /**
     * @var int
     */
    private $countUses;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var Collection
     */
    private $templates;

    /**
     * @var bool
     */
    private $hasTemplates;

    /**
     * @var Collection
     */
    private $emails;

    /**
     * @var bool
     */
    private $hasEmails;

    /**
     * @param Discount $discount
     */
    public function __construct(Discount $discount)
    {
        $this->discount = $discount;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Core\Common\Exceptions\MissingValueException
     */
    public function configure()
    {
        $this->hasTemplates = (bool) $this->discount->getTemplates()->count();
        $this->hasEmails = (bool) $this->discount->getEmails()->count();
        $this->title = $this->discount->getTitle();
        $this->code = $this->discount->getCode();

        $this->templates = $this->discount->getTemplates();
        $this->emails = $this->discount->getEmails();

        $this->type = $this->discount->getType();
        $this->maxUses = $this->discount->getMaxUses();
        $this->countUses = $this->discount->getCountUses();
        $this->active = $this->discount->isActive();

        if ($this->type->is(DiscountType::FIXED)) {
            $this->amount = $this->discount->getAmount();
        } else {
            $this->percentage = $this->discount->getPercentage();
        }
    }

    /**
     * @return Money|null
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return Discount
     */
    public function getDiscount(): Discount
    {
        return $this->discount;
    }

    /**
     * @return int|null
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return DiscountType
     */
    public function getType(): DiscountType
    {
        return $this->type;
    }

    /**
     * @param Money|null $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code ?: null;
    }

    /**
     * @param Discount $discount
     */
    public function setDiscount(Discount $discount)
    {
        $this->discount = $discount;
    }

    /**
     * @param int $percentage
     */
    public function setPercentage(int $percentage)
    {
        $this->percentage = $percentage;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title ?: null;
    }

    /**
     * @param DiscountType $type
     */
    public function setType(DiscountType $type)
    {
        $this->type = $type;
    }

    /**
     * @Assert\Callback
     *
     * @param ExecutionContextInterface $context
     * @param                           $payload
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if ($this->type->is(DiscountType::FIXED) && null === $this->amount) {
            $context->buildViolation('U moet een kortingsbedrag opgeven voor dit type korting.')
                ->atPath('amount')
                ->addViolation();
        } elseif ($this->type->is(DiscountType::PERCENTAGE) && null === $this->percentage) {
            $context->buildViolation('U moet een kortingspercentage opgeven voor dit type korting.')
                ->atPath('percentage')
                ->addViolation();
        }
    }

    /**
     * @return int
     */
    public function getMaxUses()
    {
        return $this->maxUses;
    }

    /**
     * @param int $maxUses
     *
     * @return EditDiscountRequest
     */
    public function setMaxUses($maxUses)
    {
        $this->maxUses = $maxUses;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountUses(): int
    {
        return $this->countUses;
    }

    /**
     * @param int $countUses
     *
     * @return EditDiscountRequest
     */
    public function setCountUses(int $countUses)
    {
        $this->countUses = $countUses;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     *
     * @return EditDiscountRequest
     */
    public function setActive(bool $active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getTemplates(): Collection
    {
        return $this->templates;
    }

    /**
     * @param Collection|null $templates
     *
     * @return self
     */
    public function setTemplates(Collection $templates = null): self
    {
        $this->templates = $templates;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasTemplates()
    {
        return $this->hasTemplates;
    }

    /**
     * @param bool $hasTemplates
     *
     * @return EditDiscountRequest
     */
    public function setHasTemplates($hasTemplates)
    {
        $this->hasTemplates = $hasTemplates;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * @param Collection $emails
     *
     * @return self
     */
    public function setEmails($emails): self
    {
        $this->emails = $emails;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasEmails(): bool
    {
        return $this->hasEmails;
    }

    /**
     * @param bool $hasEmails
     *
     * @return self
     */
    public function setHasEmails(bool $hasEmails): self
    {
        $this->hasEmails = $hasEmails;

        return $this;
    }
}
