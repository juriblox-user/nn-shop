<?php

namespace NnShop\Application\AppBundle\Forms\Shared\Type;

use Symfony\Component\Form\Extension\Core\Type\RadioType;

class EnhancedRadioType extends RadioType
{
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'enhanced_radio';
    }
}
