<?php

namespace NnShop\Application\AppBundle\Forms\Shared\Type;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class EnhancedCheckboxType extends CheckboxType
{
    public function getBlockPrefix()
    {
        return 'enhanced_checkbox';
    }
}
