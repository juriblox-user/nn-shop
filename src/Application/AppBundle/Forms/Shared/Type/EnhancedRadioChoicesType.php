<?php

namespace NnShop\Application\AppBundle\Forms\Shared\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\DataMapper\RadioListMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\Util\FormUtil;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EnhancedRadioChoicesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired([
            'choices',
        ]);

        $resolver->setDefaults([
            'data_mapper' => RadioListMapper::class,
        ]);

        $resolver->setAllowedTypes('choices', ['array', '\Traversable']);
        $resolver->setAllowedTypes('data_mapper', ['string']);
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setDataMapper(new $options['data_mapper']());

        /*
         * @see ChoiceType::buildForm()
         */
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $form = $event->getForm();
            $data = $event->getData();

            if (null === $data) {
                $emptyData = $form->getConfig()->getEmptyData();

                if (false === FormUtil::isEmpty($emptyData) && [] !== $emptyData) {
                    $data = is_callable($emptyData) ? call_user_func($emptyData, $form, $data) : $emptyData;
                }
            }

            // Convert the submitted data to a string, if scalar, before casting it to an array
            if (!is_array($data)) {
                $data = (array) (string) $data;
            }

            // A map from submitted values to integers
            $valueMap = array_flip($data);

            // Make a copy of the value map to determine whether any unknown values were submitted
            $unknownValues = $valueMap;

            // Reconstruct the data as mapping from child names to values
            $data = [];

            /** @var FormInterface $child */
            foreach ($form as $child) {
                $value = $child->getConfig()->getOption('value');
                if (!is_scalar($value)) {
                    $value = (string) $value;
                }

                // Add the value to $data with the child's name as key
                if (isset($valueMap[$value])) {
                    $data[$child->getName()] = $value;
                    unset($unknownValues[$value]);

                    continue;
                }
            }

            // The empty value is always known, independent of whether a field exists for it or not
            unset($unknownValues['']);

            // Throw exception if unknown values were submitted
            if (count($unknownValues) > 0) {
                throw new TransformationFailedException(sprintf('The choices "%s" do not exist in the choice list.', implode('", "', array_keys($unknownValues))));
            }

            $event->setData($data);
        });

        foreach ($options['choices'] as $name => $value) {
            $builder->add($name, EnhancedRadioType::class, [
                'value' => $value,
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        foreach ($view as $childView) {
            $childView->vars['full_name'] = $view->vars['full_name'];
        }
    }
}
