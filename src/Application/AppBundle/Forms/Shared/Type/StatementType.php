<?php

namespace NnShop\Application\AppBundle\Forms\Shared\Type;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class StatementType extends CheckboxType
{
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'statement';
    }
}
