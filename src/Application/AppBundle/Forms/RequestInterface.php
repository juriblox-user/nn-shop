<?php

namespace NnShop\Application\AppBundle\Forms;

use Symfony\Component\Form\FormInterface;

/**
 * Class RequestInterface.
 *
 * @deprecated
 *
 * @todo Imported from symfony-core: 1.3.16
 */
interface RequestInterface
{
    /**
     * @param FormInterface $form
     */
    public function bind(FormInterface $form);
}
