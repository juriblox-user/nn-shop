<?php

namespace NnShop\Application\AppBundle\Routing;

use Doctrine\ORM\EntityManager;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class ProfileLoader.
 */
class ProfileLoader extends Loader
{
    /**
     * @var bool
     */
    private $loaded = false;

    /**
     * @var EntityManager
     */
    private $profileRepository;

    /**
     * ProfileLoader constructor.
     *
     * @param ProfileRepositoryInterface $profileRepository
     */
    public function __construct(ProfileRepositoryInterface $profileRepository)
    {
        $this->profileRepository = $profileRepository;
    }

    /**
     * @param mixed $resource
     * @param null  $type
     *
     * @throws \RuntimeException
     *
     * @return RouteCollection
     */
    public function load($resource, $type = null): RouteCollection
    {
        if (true === $this->loaded) {
            throw new \RuntimeException('Do not add the "profile" loader twice');
        }

        $profiles = $this->profileRepository->findAll();

        $hosts = '';
        foreach ($profiles as $profile) {
            $hosts .= '|' . $profile->getHost();
        }

        $hosts = trim($hosts, '|');

        $resourceYaml = '@AppBundle/Resources/config/routing/frontend/routing.yml';
        $typeYaml = 'yaml';
        $routes = new RouteCollection();
        $importedRoutes = $this->import($resourceYaml, $typeYaml);
        $importedRoutes->setHost('{host}', [], ['host' => $hosts]);
        $routes->addCollection($importedRoutes);

        $this->loaded = true;

        return $routes;
    }

    /**
     * @param mixed $resource
     * @param null  $type
     *
     * @return bool
     */
    public function supports($resource, $type = null): bool
    {
        return 'profile' === $type;
    }
}
