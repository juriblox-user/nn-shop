<?php

namespace NnShop\Application\AppBundle\DependencyInjection\Compiler;

use NnShop\Application\AppBundle\Services\Security\UserChecker\CompositeUserChecker;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class UserCheckersCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        $compositeChecker = $container->findDefinition(CompositeUserChecker::class);

        $userCheckers = $container->findTaggedServiceIds('user_checker');

        foreach ($userCheckers as $checker => $tags) {
            if ($checker === $compositeChecker->getClass()) {
                continue;
            }

            $compositeChecker->addMethodCall('addChecker', [new Reference($checker)]);
        }
    }
}
