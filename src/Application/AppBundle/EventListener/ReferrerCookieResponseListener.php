<?php

namespace NnShop\Application\AppBundle\EventListener;

use Carbon\Carbon;
use NnShop\Application\AppBundle\Services\Context\ReferrerContext;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class ReferrerCookieResponseListener
{
    /**
     * @var ReferrerContext
     */
    private $referrerContext;

    /**
     * Constructor.
     *
     * @param ReferrerContext $referrerContext
     */
    public function __construct(ReferrerContext $referrerContext)
    {
        $this->referrerContext = $referrerContext;
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if (!$this->referrerContext->hasReferrer()) {
            return;
        }

        $event->getResponse()->headers->setCookie(new Cookie('referrer', $this->referrerContext->getReferrer()->getId(), Carbon::today()->addDays(7)));
    }
}
