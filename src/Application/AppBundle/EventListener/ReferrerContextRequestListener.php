<?php

namespace NnShop\Application\AppBundle\EventListener;

use Core\Common\Exception\CriticalSecurityException;
use Core\Common\Exception\Domain\EntityNotFoundException;
use NnShop\Application\AppBundle\Services\Context\ReferrerContext;
use NnShop\Domain\Shops\Repository\ReferrerRepositoryInterface;
use NnShop\Domain\Shops\Value\ReferrerId;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class ReferrerContextRequestListener
{
    /**
     * @var ReferrerContext
     */
    private $referrerContext;

    /**
     * @var ReferrerRepositoryInterface
     */
    private $referrerRepository;

    /**
     * Constructor.
     *
     * @param ReferrerRepositoryInterface $referrerRepository
     * @param ReferrerContext             $referrerContext
     */
    public function __construct(ReferrerRepositoryInterface $referrerRepository, ReferrerContext $referrerContext)
    {
        $this->referrerRepository = $referrerRepository;
        $this->referrerContext = $referrerContext;
    }

    /**
     * @param GetResponseEvent $event
     *
     * @throws CriticalSecurityException
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $session = $event->getRequest()->getSession();

        /*
         * Tracking via de "r" GET parameter
         */
        if ($request->query->has('r')) {
            try {
                $this->referrerContext->loadSlug($request->query->get('r'));
            } catch (EntityNotFoundException $exception) {
                return;
            }

            $session->set('referrer', $this->referrerContext->getReferrer()->getId()->getString());
        }

        /*
         * Tracking via een referrer in de Session
         */
        elseif ($session->has('referrer')) {
            $this->referrerContext->setReferrer($this->referrerRepository->getReference(new ReferrerId($session->get('referrer'))));
        }

        /*
         * Tracking via een "referrer" cookie
         */
        elseif ($request->cookies->has('referrer')) {
            if (!ReferrerId::valid($request->cookies->get('referrer'))) {
                throw new CriticalSecurityException('Invalid ReferrerId provided in "referrer" cookie');
            }

            $referrerId = new ReferrerId($request->cookies->get('referrer'));

            try {
                $this->referrerContext->loadId($referrerId);
            } catch (EntityNotFoundException $exception) {
                return;
            }

            $session->set('referrer', $referrerId->getString());
        }
    }
}
