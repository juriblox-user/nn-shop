<?php

namespace NnShop\Application\AppBundle\EventListener;

use NnShop\Common\Exceptions\ShopDisabledException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Routing\RouterInterface;

class ShopDisabledExceptionListener
{
    /**
     * @var bool
     */
    private $debug;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;

        $this->debug = false;
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if (!($event->getException() instanceof ShopDisabledException)) {
            return;
        }

        if ($this->debug) {
            return;
        }

        $event->setResponse(new RedirectResponse($this->router->generate('app.frontend.homepage', [], RouterInterface::ABSOLUTE_URL)));
    }

    /**
     * @param bool $debug
     */
    public function setDebug(bool $debug)
    {
        $this->debug = $debug;
    }
}
