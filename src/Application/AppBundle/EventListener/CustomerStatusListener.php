<?php

namespace NnShop\Application\AppBundle\EventListener;

use Core\Application\CoreBundle\Service\Context\ApplicationContext;
use NnShop\Domain\Orders\Entity\Customer;
use NnShop\Domain\Orders\Enumeration\CustomerStatus;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CustomerStatusListener implements EventSubscriberInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * Constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param RouterInterface       $router
     */
    public function __construct(TokenStorageInterface $tokenStorage, RouterInterface $router)
    {
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequest', 0],
        ];
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (HttpKernel::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }

        $request = $event->getRequest();

        $context = ApplicationContext::fromRequest($request);
        if (!$context->hasInterface() || 'frontend' !== $context->getInterface()) {
            return;
        }

        $token = $this->tokenStorage->getToken();
        if (null === $token) {
            return;
        }

        /** @var $customer Customer */
        $customer = $token->getUser();
        if (!$customer instanceof Customer) {
            return;
        }

        // Afmelden mag altijd
        if ($request->getPathInfo() === $this->router->generate('app.frontend.account.session.logout', ['host' => $request->get('host')])) {
            return;
        }

        /*
         * Gebruiker is nog uitgenodigd
         */
        if ($customer->getStatus()->is(CustomerStatus::INVITED) && $request->getPathInfo() !== $this->router->generate('app.frontend.account.register.welcome', ['host' => $request->get('host')])) {
            $event->setResponse(new RedirectResponse($this->router->generate('app.frontend.account.register.welcome', [
                'host' => $request->get('host'),
            ])));

            return;
        }

        /*
         * Gebruiker zit in herstelprocedure
         */
        if ($customer->getStatus()->is(CustomerStatus::RECOVER) && $request->getPathInfo() !== $this->router->generate('app.frontend.account.recover/complete', ['host' => $request->get('host')])) {
            $event->setResponse(new RedirectResponse($this->router->generate('app.frontend.account.recover/complete', [
                'host' => $request->get('host'),
            ])));

            return;
        }
    }
}
