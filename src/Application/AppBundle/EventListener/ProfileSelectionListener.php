<?php

namespace NnShop\Application\AppBundle\EventListener;

use Doctrine\ORM\EntityManager;
use NnShop\Domain\Shops\Entity\Shop;
use NnShop\Domain\Translation\Entity\Locale;
use NnShop\Domain\Translation\Entity\Profile;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ProfileSelectionListener.
 *
 * Adds _profile/_locale Request attributes and sets Request default locale according to URL
 */
class ProfileSelectionListener implements EventSubscriberInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var string|null
     */
    private $overruleHost;

    /**
     * ProfileSelectionListener constructor.
     *
     * @param EntityManager $entityManager
     * @param string|null   $overruleHost
     */
    public function __construct(EntityManager $entityManager, string $overruleHost = null)
    {
        $this->em = $entityManager;
        $this->overruleHost = $overruleHost;
    }

    /**
     * @param GetResponseEvent $event
     *
     * @throws \RuntimeException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \UnexpectedValueException
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        /*
         * If using beheer domain nothing needs to be done
         */
        if (0 === mb_strpos($event->getRequest()->getHost(), 'beheer') || 0 === mb_strpos($event->getRequest()->getHost(), 'hosted')) {
            return;
        }

        $host = $event->getRequest()->getScheme() . '://' . $event->getRequest()->getHost();
        $profile = null;

        $request = $event->getRequest();

        /*
        * Add profile to Request attribute if it isn't set already
        */
        if (!$request->attributes->get('_profile')) {
            /*
            * If overrule is set in .env get first profile and override the host. Used in development using fixtures when the Profile url in fixtures doesn't correlate to development url
            */
//            if ($this->overruleHost) {
//                $profiles = $this->em->getRepository(Profile::class)->findAll();
//
//                $profile = $profiles[0];
//                $profile->setUrl($this->overruleHost);
//            } elseif ($request->query->has('profile') && ProfileId::valid($request->query->get('_profile'))) {
//                $profile = $this->em->getRepository(Profile::class)->getById(ProfileId::from($request->query->get('_profile')));
//            }

//            /* Otherwise find profile with current hostname */
//            else {
            $profile = $this->em->getRepository(Profile::class)->findByHost($host);
//            }

            /*
             * If profile is found that means we're on a "main" site (ie juridox.nl). Besides testing for Locale nothing more needs to be done.
             */
            if ($profile instanceof Profile) {
                self::validateProfile($profile);
            }
            /*
             * If no profile is found that means we're probably dealing with a shop. In that case get related shop entity.
             */
            else {
                $shop = $this->em->getRepository(Shop::class)->findByHost($event->getRequest()->getHost());

                /* If no shop found throw error as the host doesn't correlate to any main sites or shops */
                if ($shop) {
                    $profile = $shop->getProfile();
                    self::validateProfile($profile);
                } else {
                    throw new \RuntimeException(sprintf("Shop can't be found for host %s", $host));
                }
            }

            /*
             * If no profile found throw exception. Otherwise set appropriate request attributes
             */
            if ($profile) {
                $request->setDefaultLocale($profile->getMainLocale()->getLocale());
                $request->attributes->set('_locale', $profile->getMainLocale()->getLocale());
                $request->attributes->set('_profile', $profile);
            } else {
                throw new \RuntimeException(sprintf('No profile found for url %s', $host));
            }

            /*
             * Force current locale to be set with default Locale. Otherwise browser request headers will be used to determine language
             */
            $request->setLocale($request->getDefaultLocale());
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest', 15]],
        ];
    }

    /**
     * @param Profile $profile
     *
     * @return bool
     *
     * If profile doesn't isn't linked to a Locale we can't do anything. Throw exception as nothing works without linked Locale
     */
    private static function validateProfile(Profile $profile): bool
    {
        if (!($profile->getMainLocale() instanceof Locale)) {
            throw new \RuntimeException(sprintf('No main locale set for profile %s', $profile));
        }

        return true;
    }
}
