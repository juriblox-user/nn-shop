<?php

namespace NnShop\Application\AppBundle\Fixture\Base;

use Core\Domain\System\Command\CreateJobCommand as CreateTaskCommand;
use Core\Domain\System\Value\JobId as TaskId;
use Doctrine\Common\Persistence\ObjectManager;
use NnShop\Application\AppBundle\Fixture\AbstractFixture;
use NnShop\Application\Worker\CheckOrdersWorker;
use NnShop\Application\Worker\ImportJuribloxWorker;
use NnShop\Application\Worker\SendReferralMailForDeliveredOrdersWorker;
use NnShop\Application\Worker\SendUpsellMailForDeliveredOrdersWorker;
use NnShop\Application\Worker\SynchronizeDocumentsWorker;
use NnShop\Application\Worker\SynchronizePendingPaymentsWorker;

class LoadTasksData extends AbstractFixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $command = CreateTaskCommand::prepare(TaskId::generate());
        $command->setTitle('Gegevens uit JuriBlox importeren');
        $command->setWorker(ImportJuribloxWorker::class);
        $command->setSchedule('@daily');

        $this->getCommandBus()->handle($command);

        $command = CreateTaskCommand::prepare(TaskId::generate());
        $command->setTitle('Documenten synchroniseren');
        $command->setWorker(SynchronizeDocumentsWorker::class);
        $command->setSchedule('*/10 * * * *');

        $this->getCommandBus()->handle($command);

        $command = CreateTaskCommand::prepare(TaskId::generate());
        $command->setTitle('Status betalingen synchroniseren');
        $command->setWorker(SynchronizePendingPaymentsWorker::class);
        $command->setSchedule('*/5 * * * *');

        $this->getCommandBus()->handle($command);

        $command = CreateTaskCommand::prepare(TaskId::generate());
        $command->setTitle('Bestellingen controleren');
        $command->setWorker(CheckOrdersWorker::class);
        $command->setSchedule('*/10 * * * *');

        $this->getCommandBus()->handle($command);

        $command = CreateTaskCommand::prepare(TaskId::generate());
        $command->setTitle('Referral mails versturen');
        $command->setWorker(SendReferralMailForDeliveredOrdersWorker::class);
        $command->setSchedule('* * * * *');

        $this->getCommandBus()->handle($command);

        $command = CreateTaskCommand::prepare(TaskId::generate());
        $command->setTitle('Upsell mails versturen');
        $command->setWorker(SendUpsellMailForDeliveredOrdersWorker::class);
        $command->setSchedule('* * * * *');

        $this->getCommandBus()->handle($command);
    }
}
