<?php

namespace NnShop\Application\AppBundle\Fixture\Base;

use Doctrine\Common\Persistence\ObjectManager;
use NnShop\Application\AppBundle\Fixture\AbstractFixture;
use NnShop\Domain\System\Command\Counter\CreateCounterCommand;
use NnShop\Domain\System\Enumeration\CounterName;
use NnShop\Domain\System\Value\CounterId;

class LoadCountersData extends AbstractFixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        foreach (CounterName::all() as $name) {
            $this->getCommandBus()->handle(
                CreateCounterCommand::create(CounterId::generate(), $name)
            );
        }
    }
}
