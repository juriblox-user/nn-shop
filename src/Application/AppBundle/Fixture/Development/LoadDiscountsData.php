<?php

namespace NnShop\Application\AppBundle\Fixture\Development;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NnShop\Application\AppBundle\Fixture\AbstractFixture;
use NnShop\Domain\Orders\Command\Discount\CreateDiscountCommand;
use NnShop\Domain\Orders\Value\DiscountId;

class LoadDiscountsData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $command = CreateDiscountCommand::prepare(DiscountId::generate());
        $command->setTitle('Test 20%');
        $command->setCode('JURIDOX20');
        $command->setPercentage(20);
        $command->setActive(true);
        $command->setEmails(new ArrayCollection);
        $command->setTemplates(new ArrayCollection);

        $this->getCommandBus()->handle($command);
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 30;
    }
}
