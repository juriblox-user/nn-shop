<?php

namespace NnShop\Application\AppBundle\Fixture\Development;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NnShop\Application\AppBundle\Fixture\AbstractFixture;
use NnShop\Domain\Templates\Command\CompanyActivity\CreateActivityCommand;
use NnShop\Domain\Templates\Command\CompanyType\CreateTypeCommand;
use NnShop\Domain\Templates\Entity\Template;
use NnShop\Domain\Templates\Value\CompanyActivityId;
use NnShop\Domain\Templates\Value\CompanyTypeId;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;

class LoadWizardData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     */
    public function load(ObjectManager $manager)
    {
        $profile = $this->container->get(ProfileRepositoryInterface::class)
            ->findOneBy(['country' => 'NL']);

        $commandBus = $this->getCommandBus();

        // Beschikbare templates
        $templates = $manager->getRepository(Template::class)->findAll();

        /*
         * CompanyTypes
         */
        $types = [
            'Fotograaf', 'Foodblogger', 'Freelance designer', 'Hostingbedrijf', 'ICT en arbeid', 'ICT-ondernemers', 'Ontwikkelaar met ambities', 'Webshop',
        ];

        $typeIds = [];
        foreach ($types as $type) {
            $typeId = CompanyTypeId::generate();

            $command = CreateTypeCommand::prepare($typeId, $type, $profile->getId());
            $command->setDescription('Bent u foodblogger? Dan moet u aan een aantal belangrijke zaken denken. Het auteursrecht op uw recepten, bijvoorbeeld. Maar ook de fotografie van uw gerechten, en uw aansprakelijkheid als een gerecht simpelweg niet weg te werken blijkt te zijn.');

            $typeIds[] = $typeId;
            $commandBus->handle($command);
        }

        /*
         * CompanyActivities
         */
        $activities = [
            'Catering voor foodies', 'Fotografie (van gerechten)', 'Kogelvis bereiden', 'Koken bij opdrachtgevers', 'Quinoa-workshops verzorgen', 'Recepten schrijven samen met kleuters', 'Voedingsadvies op maat',
        ];

        foreach ($typeIds as $typeId) {
            foreach ($activities as $activity) {
                $activityId = CompanyActivityId::generate();

                $command = CreateActivityCommand::prepare($activityId, $activity, $typeId);

                shuffle($templates);

                /** @var Template $template */
                foreach (\array_slice($templates, 0, 6) as $template) {
                    $command->addTemplate($template->getId());
                }

                $commandBus->handle($command);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder(): int
    {
        return 100;
    }
}
