<?php

namespace NnShop\Application\AppBundle\Fixture\Development;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Money\Currency;
use Money\Money;
use NnShop\Application\AppBundle\Fixture\AbstractFixture;
use NnShop\Domain\Templates\Command\Partner\SynchronizePartnerCommand;
use NnShop\Domain\Templates\Command\Template\PublishTemplateCommand;
use NnShop\Domain\Templates\Command\Template\SetTemplateSubscriptionPriceCommand;
use NnShop\Domain\Templates\Command\Template\SetTemplateUnitPriceCommand;
use NnShop\Domain\Templates\Command\Template\UpdateLinkedCategoriesCommand;
use NnShop\Domain\Templates\Entity\Category;
use NnShop\Domain\Templates\Repository\CategoryRepositoryInterface;
use NnShop\Domain\Templates\Repository\PartnerRepositoryInterface;
use NnShop\Domain\Templates\Repository\TemplateRepositoryInterface;

class LoadTemplatesData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        /** @var CategoryRepositoryInterface $categoryRepository */
        $categoryRepository = $this->container->get(CategoryRepositoryInterface::class);

        /** @var PartnerRepositoryInterface $partnerRepository */
        $partnerRepository = $this->container->get(PartnerRepositoryInterface::class);

        /** @var TemplateRepositoryInterface $templateRepository */
        $templateRepository = $this->container->get(TemplateRepositoryInterface::class);

        // Alle templates (via de partners) synchroniseren
        foreach ($partnerRepository->findBySyncEnabled() as $partner) {
            $this->getCommandBus()->handle(SynchronizePartnerCommand::fromLocalId($partner->getId()));
        }

        /*
         * Prijzen instellen en templates publiceren
         */
        foreach ($templateRepository->findAll() as $template) {
            $this->getCommandBus()->handle(SetTemplateUnitPriceCommand::create($template->getId(), new Money(random_int(1000, 2000), new Currency('EUR'))));
            if (0 == random_int(0, 1)) {
                $this->getCommandBus()->handle(SetTemplateSubscriptionPriceCommand::create($template->getId(), new Money(random_int(500, 1000), new Currency('EUR'))));
            }

            $this->getCommandBus()->handle(PublishTemplateCommand::create($template->getId()));

            // "Dirty" aanpassingen
            $templateRepository->refresh($template);

            $template->setOrderCount(random_int(1, 10));
            $template->setOrderSeconds(random_int(60, 300));

            $templateRepository->save($template);
        }

        /** @var Category[] $categories */
        $categories = [];
        foreach ($categoryRepository->findAll() as $category) {
            $categories[] = $category;
        }

        /*
         * Categoriën aan templates koppelen
         */
        foreach ($templateRepository->findAll() as $template) {
            $command = UpdateLinkedCategoriesCommand::prepare($template->getId());

            $category = null;
            for ($i = 1, $_i = random_int(1, 3); $i <= $_i; ++$i) {
                do {
                    $category = $categories[array_rand($categories)];
                } while ($template->hasCategory($category));

                $command->addCategory($category->getId());
            }

            $command->setPrimary($category->getId());

            $this->getCommandBus()->handle($command);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 30;
    }
}
