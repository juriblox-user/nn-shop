<?php

namespace NnShop\Application\AppBundle\Fixture\Development;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NnShop\Application\AppBundle\Fixture\AbstractFixture;
use NnShop\Domain\Shops\Command\Shop\CreateShopCommand;
use NnShop\Domain\Shops\Value\ShopId;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;

class LoadShopsData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     */
    public function load(ObjectManager $manager)
    {
        $profile = $this->container->get(ProfileRepositoryInterface::class)
            ->findOneBy(['country' => 'NL']);

        // Test shop 1
        $command = CreateShopCommand::prepare(ShopId::generate(), $profile->getId());
        $command->setTitle('Test shop 1');
        $command->setHostname('shop-1.' . $this->container->getParameter('host.base'));

        $this->setReference('shop-1', $command->getId());
        $this->getCommandBus()->handle($command);

        // Test shop 2
        $command = CreateShopCommand::prepare(ShopId::generate(), $profile->getId());
        $command->setTitle('Test shop 2');
        $command->setHostname('shop-2.' . $this->container->getParameter('host.base'));

        $this->setReference('shop-2', $command->getId());
        $this->getCommandBus()->handle($command);

        // Test shop 3
        $command = CreateShopCommand::prepare(ShopId::generate(), $profile->getId());
        $command->setTitle('Test shop 3');
        $command->setHostname('shop-3.' . $this->container->getParameter('host.base'));

        $this->setReference('shop-3', $command->getId());
        $this->getCommandBus()->handle($command);
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder(): int
    {
        return 10;
    }
}
