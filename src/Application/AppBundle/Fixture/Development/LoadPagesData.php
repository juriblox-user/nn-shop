<?php

namespace NnShop\Application\AppBundle\Fixture\Development;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NnShop\Application\AppBundle\Fixture\AbstractFixture;
use NnShop\Domain\Pages\Command\Page\CreatePageCommand;
use NnShop\Domain\Pages\Value\PageId;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;

/**
 * Class LoadPagesData.
 */
class LoadPagesData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     */
    public function load(ObjectManager $manager)
    {
        $pages = [];
        $pages[] = [
            'code' => 'terms',
            'title' => 'Voorwaarden',
            'lead' => <<<TEXT
JuriDox is een online juridisch platform voor bedrijven waarmee u door het beantwoorden van vragen een juridisch document of advies op maat maakt. De documenten worden beschikbaar gesteld door kennispartners en door JuriDox zelf. Door het inzetten van kundige kennispartners borgt JuriDox de kwaliteit van de juridische documenten. Aan het gebruik van het platform zijn onderstaande voorwaarden verbonden. Door het platform te gebruiken en/of af te nemen, gaat u hiermee akkoord
TEXT

            , 'content' => <<<TEXT
            
1. ## Gebruik van het platform
    1. De uitkomst van de generatoren is afhankelijk van de door u ingevulde antwoorden. Doorloop de generator dan ook zorgvuldig. Het is verstandig om bij iedere vraag de toelichting te lezen. U bent zelf verantwoordelijk om te bepalen of de het betreffende document dan wel advies bij uw situatie past.
    2. Als u een bestelling plaatst, sluit u een overeenkomst met JuriDox en niet met een van onze kennispartners. Het kan zijn dat u om meerwerk heeft gevraagd. Op dat moment zal de betreffende kennispartner contact met u opnemen en komt er mogelijk een overeenkomst tussen u en de kennispartner tot stand. Zie hiervoor ook artikel 2.2.
    3. Alle documenten en adviezen zijn gebaseerd op de Nederlandse wet- en regelgeving. Als u deze documenten en adviezen wilt inzetten buiten Nederland, is dat voor eigen risico.
    4. U kunt via ons platform toegang krijgen tot een “documentenmap”. Dit is een overzicht van uw gegenereerde documenten en adviezen, inclusief de resultaten van de advieswizard. De documentenmap wordt middels de gebruikersnaam en wachtwoord afgeschermd voor onbevoegden. U dient het wachtwoord strikt geheim te houden. JuriDox gaat ervan uit dat alles dat gebeurt vanaf uw gebruikersaccount na aanmelding met uw gebruikersnaam en wachtwoord, onder uw leiding en toezicht gebeurt.
    5. JuriDox hanteert een 100% tevredenheidsgarantie. Als u niet tevreden bent over het document of advies, dan heeft u de mogelijkheid om binnen 30 dagen, na het plaatsen van uw bestelling, uw geld terug te vragen. U moet uiteraard wel hebben betaald en wij stellen het op prijs als u ons nog in de gelegenheid stelt de eventuele tekortkoming te herstellen. Als u aanspraak maakt op deze regeling, mag u het document of advies niet meer gebruiken.
    6. JuriDox spant zicht maximaal in de juridische documenten en adviezen up-to-date te houden. De kennispartners worden zorgvuldig geselecteerd en beschikken allen over een vaktechnisch bureau, die de relevante wijzigingen ten aanzien van weten regelgeving bijhouden en de documenten en adviezen daarop aanpassen.   
2. ## Kennispartners en meerwerk
    1. Indien JuriDox of een van haar kennispartners op uw verzoek (dat kunt u tijdens het bestelproces aangeven) aanvullende werkzaamheden verricht, wordt dat gedaan tegen het tarief wat bij het aanbod staat vermeld. Het is bijvoorbeeld mogelijk om het door u gegenereerde document of advies door een jurist te laten controleren.
    2. Ook is het mogelijk een meerwerk aanvraag te doen. Een meerwerk aanvraag loopt altijd via de kennispartner van het betreffende document of advies. U gaat dan ook een aparte overeenkomst met de kennispartner aan. In dit geval gelden ook de algemene voorwaarden van de betreffende kennispartner. U kunt JuriDox niet aanspreken voor de resultaten van de meerwerk aanvraag. Uiteraard willen wij dat alle kennispartners kwaliteit leveren. Indien u niet tevreden bent, stellen wij het op prijs als u ons daarover informeert.
3. ## Abonnementen
    1. Het is mogelijk voor bepaalde documenten abonnementen af te nemen. Dit staat bij het aanbod van het document vermeld. Bovendien kunt u de optie kiezen tijdens het bestelproces.
    2. Het abonnement gaat in zodra u de bestelling plaatst en heeft een duur van 12 maanden. De vergoeding voor het abonnement dienst u steeds per jaar vooruit te voldoen.
    3. Na de initiële periode wordt de overeenkomst voor het abonnement steeds stilzwijgend verlengd met dezelfde periode. U kunt steeds tegen het einde van de in lid 1 bedoelde duur opzeggen met een opzegtermijn van een kalendermaand. Deze opzegmogelijkheid geldt ook voor JuriDox. U kunt de overeenkomst niet tussentijds opzeggen.
    4. In geval van een abonnement is het soms ook mogelijk dat JuriDox het document voor u host. U kunt dan een link (URL) opnemen op uw website. Door uw document te hosten houden wij uw documenten realtime up-to-date. JuriDox spant zich in om de documenten online toegankelijk te laten zijn.
 4. ## Privacy, vertrouwelijkheid en beveiliging
    1. JuriDox gaat zorgvuldig om met uw persoonsgegevens. Dit staat nader beschreven in de [privacyverklaring op de website](http://www.juridox.nl/nlprivacy-en-cookies) (deze is te vinden in de footer onderaan de pagina)
    2. JuriDox zal de gegevens die u invult als vertrouwelijke informatie behandelen en niet beschikbaar stellen aan derden zonder uw toestemming.
    3. uriDox maakt gebruik van SSL-certificaten ten behoeve van de veilige overdracht van gegevens en communicatie.
5. ## Eigendomsrechten
    1. Auteursrechten alsmede andere rechten van intellectuele eigendom op alle in het kader van de door JuriDox en/of haar kennispartners beschikbaar gestelde documenten en generatoren, berusten bij JuriDox en/of haar kennispartners. U verkrijgt alleen het recht het gegenereerde document en/of advies te gebruiken voor uw bedrijf en de doeleinden die nodig zijn voor uw bedrijfsvoering.
    2. Het is niet toegestaan om met de gegenereerde documenten en/of adviezen zelf als aanbieder van deze documenten op te treden en daarmee te concurreren met JuriDox. Er zijn zichtbare en onzichtbare kenmerken aangebracht om inbreuk op de rechten van JuriDox te achterhalen.
6. ## Betaling
    1. Voor een document of advies is een vergoeding verschuldigd die bij het aanbod staat vermeld. De vergoeding is altijd exclusief btw.
    2. Betaling kan worden verricht via de betaalmethoden die tijdens het bestelproces worden weergegeven.
    3. U bent akkoord met elektronische facturatie. De factuur zal in PDF beschikbaar worden gesteld en wordt per e-mail naar u gestuurd.
7. ## Aansprakelijkheid en overmacht
    1. Behoudens in geval van opzet of bewuste roekeloosheid is de aansprakelijkheid van JuriDox beperkt tot het bedrag dat u heeft betaald voor de geleverde dienst met een maximum van EUR 1.000,-. Voor de “gratis” documenten en adviezen die via het platform ter beschikking worden gesteld, is JuriDox nimmer aansprakelijk.
    2. Voorwaarde voor het ontstaan van enig recht op schadevergoeding is dat u de schade uiterlijk binnen 30 dagen na ontdekking schriftelijk bij JuriDox meldt.
    3. U vrijwaart JuriDox voor alle aanspraken van derden en stelt JuriDox hiervoor volledig schadeloos.
    4. In geval van overmacht is JuriDox nimmer gehouden tot vergoeding van de daardoor bij u ontstane schade.
8. ## Wijzigingen doorvoeren
    1. JuriDox mag deze voorwaarden op ieder moment aanpassen. JuriDox zal de wijzigingen of aanvullingen ten minste dertig dagen voor inwerkingtreding aankondigen via de JuriDox nieuwsbrief per e-mail, zodat u daar kennis van kunt nemen.
    2. Indien u een wijziging of aanvulling niet wenst te accepteren, kunt u tot de datum van inwerkingtreding de overeenkomst opzeggen. Gebruik van het platform na de datum van inwerkingtreding geldt als acceptatie van de gewijzigde of aangevulde voorwaarden.
 9. ## Overige bepalingen
    1. JuriDox mag deze voorwaarden op ieder moment aanpassen. JuriDox zal de wijzigingen of aanvullingen ten minste dertig dagen voor inwerkingtreding aankondigen via de JuriDox nieuwsbrief per e-mail, zodat u daar kennis van kunt nemen.
    2. Indien u een wijziging of aanvulling niet wenst te accepteren, kunt u tot de datum van inwerkingtreding de overeenkomst opzeggen. Gebruik van het platform na de datum van inwerkingtreding geldt als acceptatie van de gewijzigde of aangevulde voorwaarden.
TEXT
        ];

        $pages[] = [
            'code' => 'privacy',
            'title' => 'Privacy en cookies',
            'lead' => <<<TEXT
Wij respecteren de privacy van bezoekers van onze website en dragen er zorg voor dat de persoonlijke informatie die u ons verschaft vertrouwelijk wordt behandeld. Verwerking van de persoonsgegevens gebeurt op een wijze, die in overeenstemming is met de eisen die hieraan in de privacywetgeving worden gesteld.
TEXT

        , 'content' => <<<TEXT
            
## Doeleinden van de gegevensverwerking

Wij verwerken uw persoonsgegevens voor de volgende doeleinden:

* het verlenen van toegang tot onze website [juridox.nl](http://juridox.nl);
* het gebruik maken van de functionaliteiten op onze website;
* het aangaan en uitvoeren van overeenkomsten met betrekking tot onze dienstverlening;
* het voeren van gesprekken via de op onze de website geboden chatoptie;
* het opnemen van contact met u, indien hierom wordt verzocht;
* het verkrijgen van inzicht in het gebruik van onze website;
* het verbeteren en evalueren van onze website en diensten;
* het voor u mogelijk te maken om een pagina te e-mailen, te printen of om te delen via sociale netwerken, zoals Facebook, Twitter en LinkedIn;

Wij zullen uw persoonsgegevens niet voor andere dan bovengenoemde doeleinden verwerken, tenzij u daar vooraf toestemming voor heeft gegeven of wij dit op grond van de wet mogen of moeten doen.

## Categorieën persoonsgegevens

Wij gebruiken de volgende gegevens voor de in deze privacy- en cookieverklaring genoemde doeleinden:

* Bedrijfsnaam- en - gegevens (voor zover dit persoonsgegevens zijn);
* NAW-gegevens van de contactpersoon;
* E-mailadres van de contactpersoon;
* Telefoonnummer van contactpersoon;
* IP-adressen.

Wij zullen uw persoonsgegevens niet voor andere dan bovengenoemde doeleinden verwerken, tenzij u daar vooraf toestemming voor heeft gegeven of wij dit op grond van de wet mogen of moeten doen.

## Bewaartermijnen

Wij bewaren uw gegevens niet langer dan noodzakelijk is om de in deze privacy- en cookieverklaring genoemde doeleinden te bereiken.

## Verstrekking van gegevens aan derden

Wij verstrekken uw gegevens niet aan derde partijen zonder uw voorafgaande toestemming, tenzij dat noodzakelijk is in het kader van de uitvoering van de overeenkomst of wij op basis van de wet hiertoe verplicht zijn.

## Beveiliging

Wij nemen passende beveiligingsmaatregelen om misbruik van, en ongeautoriseerde toegang tot, uw persoonsgegevens te beperken. Zo zorgen wij dat alleen de noodzakelijke personen toegang hebben tot uw gegevens, dat de toegang tot de gegevens afgeschermd is en dat onze veiligheidsmaatregelen regelmatig gecontroleerd worden. Daarnaast, maken wij gebruik van SSL-certificaten ten behoeve van de veilige overdracht van uw gegevens en communicatie.

## Cookies

Cookies zijn informatiebestandjes die bij het bezoeken van een website automatisch kunnen worden opgeslagen op of uitgelezen van het device (zoals PC, tablet of smartphone) van de bezoeker. Dat gebeurt via de webbrowser op het device.

Op onze website worden cookies of vergelijkbare technologieën gebruikt waarbij informatie op het device wordt opgeslagen en/of uitgelezen (voor het leesgemak worden alle dergelijke technieken hier verder ‘cookies’ genoemd), gebruikt om:

* functionaliteiten van de website mogelijk te maken en om de website te beschermen (technische of functionele cookies);
* het gebruik van de website te analyseren en op basis daarvan de website te verbeteren (analytics cookies);
* het mogelijk maken van de online chatfunctie en het veel gestelde vragen portal (overige cookies).
                       
De informatie die een cookie verkrijgt over uw gebruik van de website kan worden overgebracht naar eigen beveiligde servers van JuriDox B.V. of die van een door ons hiervoor ingeschakelde derde partij.

## Technische of functionele cookies

Sommige cookies zorgen ervoor dat bepaalde onderdelen van de website goed werken en dat uw gebruikersvoorkeuren bekend blijven. Zo kunnen er, bijvoorbeeld, cookies worden gebruikt om lettertypes goed te renderen, om uw gebruikssessie te kunnen onthouden op de webserver zodat u de website kunt bekijken, of om een zoekterm te onthouden waarop wordt gezocht binnen de website of een gekozen filter. De technische en functionele cookies die wij gebruiken zijn:

* JuriDox (voor het opslaan van gebruikersgegevens en het verbergen van informatie na een eerste bezoek aan de website);
* Cloudflare (ter beveiliging van de applicatie en om te controleren of het betreffende device te vertrouwen is, zie voor meer informatie de [Privacy Policy van Cloudflare](https://www.cloudflare.com/security-policy/).

## Analytische cookies

Verder maken wij gebruik van cookies van Google Analytics, zodat Google ons inzicht kan geven in hoe onze website gebruikt wordt, om rapporten over de website aan ons te kunnen verstrekken en om ons informatie over de effectiviteit van onze campagnes te kunnen bieden. De informatie die we daarbij verkrijgen wordt met inbegrip van het adres van uw computer (IP-adres), overgebracht naar Google en door Google opgeslagen op servers in de Verenigde Staten. Wij hebben een bewerkersovereenkomst gesloten met Google waarin afspraken zijn vastgelegd over de omgang met de verzamelde gegevens. Hierin hebben wij Google niet toegestaan de verkregen informatie te gebruiken voor andere Google diensten. Google kan echter informatie aan derden verschaffen indien Google hiertoe wettelijk wordt verplicht, of voor zover deze derden de informatie namens Google verwerken. Wij hebben hier verder geen invloed op. Meer informatie over de gegevensverwerking door Google Analytics kunt u vinden in de [Privacy Policy van Google](http://www.google.com/intl/nl/policies/privacy/) en in de [Privacy Policy van Google Analytics](http://www.google.com/intl/nl/analytics/privacyoverview.html)

## Overige cookies

Naast de hierboven genoemde technische, functionele en analytische cookies, gebruiken wij cookies van Intercom om de online chatfunctie en de veel gestelde vragen portal te laten functioneren. De gegevens die hiervoor door Intercom worden verwerkt zijn browsergegevens en gegevens die door u worden ingevuld in het contactformulier, een chatgesprek of gedurende het plaatsen van een bestelling. De informatie die Intercom hierbij verzamelt wordt opgeslagen op servers in de Verenigde Staten. Wij hebben een bewerkersovereenkomst gesloten met Intercom waarin afspraken zijn vastgelegd over de omgang met de verzamelde gegevens. Intercom kan echter informatie aan derden verschaffen indien Intercom hiertoe wettelijk wordt verplicht, of voor zover deze derden de informatie namens Intercom verwerken. Daarnaast, kan Intercom deze gegevens in geanonimiseerde vorm aan derden verstrekken. Wij hebben hier verder geen invloed op. Meer informatie over de gegevensverwerking door Intercom kunt u vinden in de [Privacy Policy van Intercom](https://www.intercom.com/privacy).

## In- en uitschakelen cookies

In uw browser kunt u instellen dat het opslaan van cookies alleen wordt geaccepteerd, wanneer u ermee instemt. Let op: veel websites werken niet optimaal als de cookies zijn uitgeschakeld.

## Verwijderen van cookies

Veel cookies hebben een houdbaarheidsdatum. Als een houdbaarheidsdatum is ingesteld, wordt de cookie automatisch verwijderd wanneer de houdbaarheidsdatum verstrijkt. U kunt er ook voor kiezen de cookies handmatig te verwijderen voordat de houdbaarheidsdatum is verstreken. Raadpleeg hiervoor de handleiding van uw browser.

## Websites van derden

Deze privacy- en cookieverklaring is niet van toepassing op websites van derden die door middel van links met onze website zijn verbonden. Wij kunnen niet garanderen dat deze derden op een betrouwbare of veilige manier met uw persoonsgegevens omgaan. Wij raden u aan de privacyverklaring van deze websites te lezen alvorens van deze websites gebruik te maken.

## Rechten van betrokkenen

Indien u aan ons persoonsgegevens heeft verstrekt, kunt u ons een verzoek toesturen om deze gegevens in te zien, te wijzigen, te verplaatsen of te verwijderen. Dit verzoek kunt u indienen door een e-mail te sturen naar [help@juridox.nl](mailto:help@juridox.nl), of door telefonisch contact met ons op te nemen via 020 - 229 39 29.

## Aanpassingen privacy- en cookieverklaring

Wij behouden ons het recht voor om deze privacy- en cookieverklaring aan te passen. Wijzigingen zullen op deze website worden gepubliceerd. Het verdient aanbeveling om deze verklaring geregeld te raadplegen, zodat u van deze wijzigingen op de hoogte bent.

## Autoriteit persoonsgegevens

Natuurlijk helpen wij u ook graag verder als u klachten heeft over de verwerking van uw persoonsgegevens. Op grond van de privacywetgeving heeft u het recht om een klacht in te dienen bij de Autoriteit Persoonsgegevens over onze verwerkingen van uw persoonsgegevens. U kunt hiervoor [contact opnemen](https://autoriteitpersoonsgegevens.nl/nl/contact-met-de-autoriteit-persoonsgegevens/tip-ons) met de Autoriteit Persoonsgegevens.
TEXT
        ];

        $pages[] = [
            'code' => 'about',
            'title' => 'Over ons',
            'content' => <<<TEXT
Bij ons maakt u zelf de juridische documenten die u nodig heeft. Onze slimme software loodst u met duidelijke vragen door de juridische kennis van onze kennispartners, zodat u binnen enkele minuten een juridisch document op maat heeft gemaakt.

Standaarddocumenten kunnen volledig geautomatiseerd worden en hoeven dus geen honderden euros te kosten. Dit is de kracht van JuriDox. Wij hebben deze dienst ontwikkeld als eerste stap in onze missie om het juridisch standaardwerk volledig te automatiseren.

Uiteraard moeten juridische document correct en volledig zijn. Daarom is elk document opgesteld door een van de specialisten uit ons kennisnetwerk. Deze specialisten onderhouden de documenten continu, waardoor u altijd een up to date document heeft. Wij zijn dan wel een juridische supermarkt, maar we leveren alleen A-merken!

JuriDox is opgericht door de eigenaren van ICTRecht B.V., internetjuristen Steven Ras en Arnoud Engelfriet. ICTRecht is een juridisch adviesbureau op het gebied van ICT- en internetrecht. ICTRecht heeft zich in 10 jaar tijd neer weten te zetten als een belangrijke nichespeler binnen zijn rechtsgebied. Het adviesbureau is een van de kennispartners in de juridische supermarkt van JuriDox.

Meer weten? Neem dan contact met ons op.
TEXT
            , 'footer' => <<<TEXT

## Techniek achter JuriDox

JuriDox maakt gebruik van de techniek van [JuriBlox B.V.](https://juriblox.nl?utm_source=juridox&amp;utm_campaign=juridox&amp;utm_medium=about-text), een zusteronderneming van JuriDox.

JuriBlox is vooral gericht op gebruikers die met regelmaat juridische documenten, adviezen moeten samenstellen en gegevensverwerkingen bij moeten houden, zoals (bedrijfs)juristen, privacy officers, advocaten en inkopers. Juridische documenten die met regelmaat nodig zijn, zoals een geheimhoudingsovereenkomst of een bewerkersovereenkomst, zijn zeer geschikt om via JuriBlox op te stellen. Maar daarnaast biedt JuriBlox ook de mogelijkheid om grote, complexe maatcontracten aan te maken.

De software van JuriBlox kent uitgebreide toepassingsmogelijkheden. Door te werken met een API kunnen andere systemen gebruik maken van de slimme tools van JuriBlox. JuriDox maakt bijvoorbeeld gebruik van de slimme documentgenerator uit JuriBlox. Maar ook kan een adviestool of een documentgenerator op een willekeurige website worden geplaatst.

Wilt u zelf gebruik maken van JuriBlox of wilt u een eigen (documenten)shop? Neem dan contact op met JuriBlox via **020 - 229 33 45** of [info@juriblox.nl](mailto:https://juriblox.nl?utm_source=juridox&amp;utm_campaign=juridox&amp;utm_medium=about-text).

TEXT
        ];

        $profiles = $this->container->get(ProfileRepositoryInterface::class)->findAll();

        foreach ($profiles as $profile) {
            foreach ($pages as $page) {
                $command = CreatePageCommand::create(PageId::generate(), $profile->getId());
                $command->setTitle($page['title']);
                $command->setCode($page['code']);
                if (isset($page['lead'])) {
                    $command->setLead($page['lead']);
                }
                if (isset($page['footer'])) {
                    $command->setFooter($page['footer']);
                }
                $command->setContent($page['content']);
                $command->setLastUpdated(new \DateTime());
                $this->getCommandBus()->handle($command);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder(): int
    {
        return 25;
    }
}
