<?php

namespace NnShop\Application\AppBundle\Fixture\Development;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NnShop\Application\AppBundle\Fixture\AbstractFixture;
use NnShop\Domain\Shops\Repository\ShopRepositoryInterface;
use NnShop\Domain\Templates\Command\Category\CreateCategoryCommand;
use NnShop\Domain\Templates\Value\CategoryId;

class LoadCategoriesData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        /** @var ShopRepositoryInterface $shopRepository */
        $shopRepository = $this->container->get(ShopRepositoryInterface::class);

        foreach ($shopRepository->findAll() as $shop) {
            for ($i = 1; $i <= 5; ++$i) {
                $command = CreateCategoryCommand::create(
                    CategoryId::generate(),
                    'Rechtsgebied ' . $i,
                    $shop,
                    'Rechtsgebied description ' . $i,
                    'Rechtsgebied meta description ' . $i
                );

                $this->getCommandBus()->handle($command);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 20;
    }
}
