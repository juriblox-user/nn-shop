<?php

namespace NnShop\Application\AppBundle\Fixture\Development;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NnShop\Application\AppBundle\Fixture\AbstractFixture;
use NnShop\Domain\Translation\Command\Profile\CreateProfileCommand;
use NnShop\Domain\Translation\Repository\LocaleRepositoryInterface;
use NnShop\Domain\Translation\Value\ProfileId;

/**
 * Class LoadTranslationProfilesData.
 */
class LoadTranslationProfilesData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     */
    public function load(ObjectManager $manager)
    {
        $localeRepository = $localeRepository = $this->container->get(LocaleRepositoryInterface::class);

        $profiles = [];
        $profiles[] = [
            'country' => 'NL',
            'currency' => 'EUR',
            'timezone' => 'Europe/Amsterdam',
            'url' => 'http://juridox.test',
            'locale' => $localeRepository->findOneBy(['locale' => 'nl']),
        ];
        $profiles[] = [
            'country' => 'FR',
            'currency' => 'EUR',
            'timezone' => 'Europe/Paris',
            'url' => 'http://juridox2.test',
            'locale' => $localeRepository->findOneBy(['locale' => 'fr']),
        ];
        $profiles[] = [
            'country' => 'DE',
            'currency' => 'EUR',
            'timezone' => 'Germany/Berlin',
            'url' => 'http://juridox3.test',
            'locale' => $localeRepository->findOneBy(['locale' => 'de']),
        ];
        $profiles[] = [
            'country' => 'GB',
            'currency' => 'GBP',
            'timezone' => 'Europe/London',
            'url' => 'http://juridox4.test',
            'locale' => $localeRepository->findOneBy(['locale' => 'en']),
        ];

        foreach ($profiles as $profile) {
            $command = CreateProfileCommand::create(ProfileId::generate(), $profile['locale']->getId());
            $command->setEntityName('JuriDox');
            $command->setEntityCountry('The Netherlands');

            $command->setCocLabel('KvK');
            $command->setCocNumber('66244501');

            $command->setVatLabel('btw');
            $command->setVatNumber('NL856460035B01');

            $command->setAddressLine1('Jollemanhof 8a');
            $command->setZipcode('1079 AA');
            $command->setCity('Amsterdam');

            $command->setCountry($profile['country']);
            $command->setCurrency($profile['currency']);
            $command->setTimezone($profile['timezone']);

            $command->setUrl($profile['url']);

            $command->setEmail('development@juridox.nl');
            $command->setPhone('020 229 39 29');

            $command->setMollieKey('test_7xSnm4Nr9hCk7kRNcxb2fG8HHCJHP6');

            $this->getCommandBus()->handle($command);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder(): int
    {
        return 5;
    }
}
