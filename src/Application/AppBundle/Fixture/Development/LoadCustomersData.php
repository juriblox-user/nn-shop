<?php

namespace NnShop\Application\AppBundle\Fixture\Development;

use Core\Domain\Value\Geography\Address;
use Core\Domain\Value\Geography\CountryCode;
use Core\Domain\Value\Geography\HouseNumber;
use Core\Domain\Value\Web\EmailAddress;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NnShop\Application\AppBundle\Fixture\AbstractFixture;
use NnShop\Domain\Orders\Command\Customer\CreateCustomerCommand;
use NnShop\Domain\Orders\Enumeration\CustomerType;
use NnShop\Domain\Orders\Value\CustomerId;
use NnShop\Domain\Translation\Entity\Locale;
use NnShop\Domain\Translation\Entity\Profile;
use NnShop\Domain\Translation\Repository\LocaleRepositoryInterface;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;

class LoadCustomersData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $localeRepository  = $this->container->get(LocaleRepositoryInterface::class);
        $profileRepository = $this->container->get(ProfileRepositoryInterface::class);

        /** @var Locale $locale */
        $locale = $localeRepository->findOneBy(['locale' => 'nl']);

        /** @var Profile $profile */
        $profile = $profileRepository->findOneBy(['country' => 'NL']);

        $command = CreateCustomerCommand::prepare(CustomerId::null(), CustomerType::from(CustomerType::BUSINESS), EmailAddress::from('test@juridox.nl'), $profile->getId(), $locale->getId());
        $command->setCompanyName('MIDDAG');
        $command->setFirstname('Vic');
        $command->setLastname('D\'Elfant');
        $command->setPassword('test');

        $command->setAddress(Address::fromFullAddress('Zwaardvegersdreef', HouseNumber::fromNumber(11), '6216 SL', 'Maastricht', CountryCode::from(CountryCode::NL)));

        $this->setReference('customer', $command->getId());

        $this->getCommandBus()->handle($command);
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 11;
    }
}
