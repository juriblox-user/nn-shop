<?php

namespace NnShop\Application\AppBundle\Fixture\Development;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NnShop\Application\AppBundle\Fixture\AbstractFixture;
use NnShop\Domain\Translation\Command\Locale\CreateLocaleCommand;
use NnShop\Domain\Translation\Value\LocaleId;

/**
 * Class LoadTranslationLocalesData.
 */
class LoadTranslationLocalesData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $locales = [];
        $locales[] = [
            'locale' => 'nl',
        ];
        $locales[] = [
            'locale' => 'fr',
        ];
        $locales[] = [
            'locale' => 'de',
        ];
        $locales[] = [
            'locale' => 'en',
        ];

        foreach ($locales as $locale) {
            $command = CreateLocaleCommand::create(LocaleId::generate());
            $command->setLocale($locale['locale']);

            $this->getCommandBus()->handle($command);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder(): int
    {
        return 0;
    }
}
