<?php

namespace NnShop\Application\AppBundle\Fixture\Development;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use JuriBlox\Sdk\Domain\Offices\Values\OfficeId;
use NnShop\Application\AppBundle\Fixture\AbstractFixture;
use NnShop\Domain\Templates\Command\Partner\CreatePartnerCommand;
use NnShop\Domain\Templates\Value\PartnerId;
use NnShop\Domain\Translation\Repository\ProfileRepositoryInterface;

class LoadPartnersData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \RuntimeException
     * @throws \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
     */
    public function load(ObjectManager $manager)
    {
        $profileRepository = $this->container->get(ProfileRepositoryInterface::class);
        $profile = $profileRepository->findOneBy(['country' => 'NL']);

        $parameters = [
            'fixtures.juridox.office_id',
            'fixtures.juridox.client_id',
            'fixtures.juridox.client_secret',
        ];

        // Checken of we alle variabelen compleet hebben
        foreach ($parameters as $parameter) {
            if ('' == $this->container->getParameter($parameter)) {
                throw new \RuntimeException(sprintf('The "%s" parameter has not been set. You have to set the corresponding env variable in your .env before you can use this fixture.', $parameter));
            }
        }

        // JuriDox
        $command = CreatePartnerCommand::prepare(PartnerId::generate(), new OfficeId($this->container->getParameter('fixtures.juridox.office_id')), $profile->getId());
        $command->setTitle('JuriDox @ production');
        $command->setExcerpt('ICTRecht levert deskundig en praktisch juridisch advies over ICT tegen een gunstig tarief. Onze adviezen zijn begrijpelijk en geven blijk van technische kennis. Wij denken pro-actief mee met de klant. Onze mensen zijn dan ook juridisch en technisch thuis in onze niche.');
        $command->setProfile('ICTRecht levert deskundig en praktisch juridisch advies over ICT tegen een gunstig tarief. Onze adviezen zijn begrijpelijk en geven blijk van technische kennis. Wij denken pro-actief mee met de klant. Onze mensen zijn dan ook juridisch en technisch thuis in onze niche.');

        $command->enableSync($this->container->getParameter('fixtures.juridox.client_id'), $this->container->getParameter('fixtures.juridox.client_secret'));

        $this->getCommandBus()->handle($command);
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder(): int
    {
        return 25;
    }
}
