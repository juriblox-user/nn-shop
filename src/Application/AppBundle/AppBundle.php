<?php

namespace NnShop\Application\AppBundle;

use Core\Application\CoreBundle\Service\Doctrine\ValueTypeRegistrar;
use NnShop\Application\AppBundle\DependencyInjection\Compiler\UserCheckersCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        /*
         * Doctrine types voor value objects registreren
         */
        $registrar = $this->container->get(ValueTypeRegistrar::class);

        /** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
        $types = [

            // NnShop
            \NnShop\Infrastructure\JuriBlox\CustomerReferenceType::class,
            \NnShop\Infrastructure\JuriBlox\DocumentIdType::class,
            \NnShop\Infrastructure\JuriBlox\DocumentRequestIdType::class,
            \NnShop\Infrastructure\JuriBlox\OfficeIdType::class,
            \NnShop\Infrastructure\JuriBlox\TemplateIdType::class,
            \NnShop\Infrastructure\JuriBlox\QuestionOptionIdType::class,
            \NnShop\Infrastructure\JuriBlox\QuestionIdType::class,
            \NnShop\Infrastructure\JuriBlox\QuestionnaireStepIdType::class,

            // Mollie
            \NnShop\Infrastructure\Mollie\TransactionIdType::class,

            // Orders
            \NnShop\Infrastructure\Orders\AdviceIdType::class,
            \NnShop\Infrastructure\Orders\CustomerIdType::class,
            \NnShop\Infrastructure\Orders\CustomerStatusType::class,
            \NnShop\Infrastructure\Orders\CustomerTypeType::class,
            \NnShop\Infrastructure\Orders\DiscountIdType::class,
            \NnShop\Infrastructure\Orders\DiscountTypeType::class,
            \NnShop\Infrastructure\Orders\InvoiceIdType::class,
            \NnShop\Infrastructure\Orders\InvoiceCodeType::class,
            \NnShop\Infrastructure\Orders\InvoiceItemIdType::class,
            \NnShop\Infrastructure\Orders\InvoiceStatusType::class,
            \NnShop\Infrastructure\Orders\OrderAttributeBagType::class,
            \NnShop\Infrastructure\Orders\OrderCodeType::class,
            \NnShop\Infrastructure\Orders\OrderIdType::class,
            \NnShop\Infrastructure\Orders\OrderStatusType::class,
            \NnShop\Infrastructure\Orders\OrderTypeType::class,
            \NnShop\Infrastructure\Orders\OrderItemTypeType::class,
            \NnShop\Infrastructure\Orders\PaymentIdType::class,
            \NnShop\Infrastructure\Orders\PaymentMethodType::class,
            \NnShop\Infrastructure\Orders\PaymentStatusType::class,
            \NnShop\Infrastructure\Orders\RefundIdType::class,
            \NnShop\Infrastructure\Orders\SubscriptionCodeType::class,
            \NnShop\Infrastructure\Orders\SubscriptionIdType::class,
            \NnShop\Infrastructure\Orders\SubscriptionLinkFormatType::class,
            \NnShop\Infrastructure\Orders\SubscriptionLinkIdType::class,
            \NnShop\Infrastructure\Orders\SubscriptionLinkTokenType::class,

            // Shops
            \NnShop\Infrastructure\Shops\LandingPageIdType::class,
            \NnShop\Infrastructure\Shops\ReferrerIdType::class,
            \NnShop\Infrastructure\Shops\ShopIdType::class,
            \NnShop\Infrastructure\Shops\TestimonialIdType::class,

            // Profiles
            \NnShop\Infrastructure\Translation\ProfileIdType::class,
            \NnShop\Infrastructure\Translation\LocaleIdType::class,

            // System
            \NnShop\Infrastructure\System\CounterIdType::class,
            \NnShop\Infrastructure\System\CounterNameType::class,

            // Templates
            \NnShop\Infrastructure\Templates\AnswerIdType::class,
            \NnShop\Infrastructure\Templates\CategoryIdType::class,
            \NnShop\Infrastructure\Templates\CompanyActivityIdType::class,
            \NnShop\Infrastructure\Templates\CompanyTypeIdType::class,
            \NnShop\Infrastructure\Templates\DocumentIdType::class,
            \NnShop\Infrastructure\Templates\DocumentRequestIdType::class,
            \NnShop\Infrastructure\Templates\DocumentStatusType::class,
            \NnShop\Infrastructure\Templates\QuestionConditionIdType::class,
            \NnShop\Infrastructure\Templates\QuestionIdType::class,
            \NnShop\Infrastructure\Templates\QuestionOptionIdType::class,
            \NnShop\Infrastructure\Templates\QuestionStepIdType::class,
            \NnShop\Infrastructure\Templates\QuestionTypeType::class,
            \NnShop\Infrastructure\Templates\PartnerIdType::class,
            \NnShop\Infrastructure\Templates\TemplateIdType::class,
            \NnShop\Infrastructure\Templates\TemplateKeyType::class,
            \NnShop\Infrastructure\Templates\TemplateVersionType::class,
            \NnShop\Infrastructure\Templates\UpdateIdType::class,

            \NnShop\Infrastructure\Pages\PageIdType::class,
        ];

        $registrar->registerTypes($types);
    }

    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new UserCheckersCompilerPass());
    }
}
