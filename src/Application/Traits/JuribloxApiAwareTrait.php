<?php

namespace NnShop\Application\Traits;

use JuriBlox\Sdk\Client;
use JuriBlox\Sdk\Infrastructure\Drivers\GuzzleDriver;
use Psr\Log\LoggerAwareInterface;

trait JuribloxApiAwareTrait
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var GuzzleDriver
     */
    private $driver;

    /**
     * @param $clientId
     * @param $clientKey
     */
    protected function connect($clientId, $clientKey)
    {
        // Driver
        $this->driver = new GuzzleDriver($clientId, $clientKey, getenv('JURIBLOX_API_URL'));

        // SDK client
        $this->client = Client::fromDriverWithName($this->driver, 'LegalFit');
        if ($this instanceof LoggerAwareInterface) {
            $this->client->setLogger($this->logger);
        }
    }
}
