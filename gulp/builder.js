"use strict";

const _ = require('lodash');

// Node plugins
const exec = require('child_process').exec;
const mkdirp = require('mkdirp');
const path = require('path');
const rimraf = require('rimraf');
const url = require('url');
const sprintf = require('sprintf-js').sprintf;

const Q = require('q');

// Gulp + plugins
const util = require('gulp-util');
const environments = require('gulp-environments');
const merge = require('webpack-merge');

// Webpack
const Webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');

// Applicatie plugins
const logger = require('./logger.js');

// Environments
const development = environments.development;
const production = environments.production;

try {
    var assetsConfig = require('../assets.config.local.js');
} catch (e) {
    // Assets config in de root
    var assetsConfig = require('../assets.config.js');
}

module.exports.Builder = class {
    /**
     * @var string
     */
    _outputPath;

    /**
     * Constructor.
     *
     * @param outputPath    string              Het pad waarin de assets uiteindelijk worden opgeslagen
     */
    constructor(outputPath) {
        this._outputPath = outputPath;
    }

    /**
     * Configuratie voor een normale Webpack build genereren.
     *
     * @returns {Object}
     */
    getBuildConfig() {
        return this._getConfig(false);
    }

    /**
     * Uitvoer-pad waarin de assets uiteindelijk worden opgeslagen.
     *
     * @param resolve boolean
     *
     * @returns {string}
     */
    getOutputPath(resolve) {
        if (resolve) {
            return path.resolve(this._outputPath);
        }

        return this._outputPath;
    }

    /**
     * Configuratie voor Webpack server genereren.
     *
     * @param reloadEnabled
     *
     * @returns {Object}
     */
    getServerConfig(reloadEnabled) {
        return this._getConfig(true);
    }

    /**
     * Voorbereiding van een build.
     *
     * @return promise
     */
    prepare() {
        // Oude assets verwijderen
        rimraf.sync(this.getOutputPath());

        // Zorgen dat de map incl. parents bestaat
        mkdirp(this.getOutputPath());

        /*
         * php bin/console assets:install
         */
        return Q.fcall(() => {
            const promise = Q.defer();

            logger.log('Installing Symfony assets...');

            exec(sprintf('php bin/console assets:install --symlink --env="%s"', environments.current().$name), (error) => {
                if (error) {
                    throw new util.PluginError('builder', error);
                }

                logger.done();
                promise.resolve();
            });

            return promise.promise;
        });
    }

    /**
     * Server Js config
     * @returns {*}
     */
    getServerJsConfig() {
        return assetsConfig.serverjs ? assetsConfig.serverjs : { port: 8080, disableHostCheck: true };
    }

    /**
     * Basis Webpack configuratie genereren.
     *
     * @param serverMode
     *
     * @returns {object}
     *
     * @private
     */
    _getConfig(serverMode) {
        if ('' !== process.env.ASSETS_URL_PREFIX) {
            assetsConfig.output.buildUrl = assetsConfig.output.buildUrl.replace('$ASSETS_BASE_URL', process.env.ASSETS_BASE_URL);
            assetsConfig.output.serverUrl = assetsConfig.output.serverUrl.replace('$ASSETS_BASE_URL', process.env.ASSETS_BASE_URL);
        }

        let config = {
            entry: assetsConfig.entrypoints,
            devServer: {
                overlay: true
            },

            node: {
                __dirname: true,
                __filename: true
            },

            output: {
                path: this.getOutputPath(true),
                publicPath: _.trimEnd(serverMode ? assetsConfig.output.serverUrl : assetsConfig.output.buildUrl, '/') + '/',

                filename: serverMode ? '[name].js' : '[name]-[chunkhash].js',
                sourceMapFilename: '[file].map'
            },

            plugins: [
                new ManifestPlugin({
                    publicPath: _.trimEnd(serverMode ? assetsConfig.output.serverUrl : assetsConfig.output.buildUrl, '/') + '/',
                    writeToFileEmit: serverMode,
                }),
                new ExtractTextPlugin({
                    filename: serverMode ? '[name].css' : '[name]-[chunkhash].css'
                })
            ],

            resolve: {
                modules: [
                    path.resolve('./app/Resources'),
                    path.resolve('./web/bundles/core/modules'),
                    'node_modules'
                ]
            }
        };

        if (development()) {
            config = merge(config, {
                devtool: 'source-map'
            });
        }

        /*
         * Loaders
         */
        const cssLoader = {
            loader: 'css-loader',
            options: {
                importLoaders: 1,
                sourceMap: development(),
                minimize: production()
            }
        };

        const postCssLoader = {
            loader: 'postcss-loader',
            options: {
                sourceMap: development()
            }
        };

        const sassLoader = {
            loader: 'sass-loader',
            options: {
                sourceMap: development()
            }
        };

        config = merge(config, {
            module: {
                rules: [
                    { // jQuery
                        test: require.resolve('jquery'),
                        use: [{
                            loader: 'expose-loader',
                            options: 'jQuery'
                        }, {
                            loader: 'expose-loader',
                            options: '$'
                        }]
                    },

                    { // Javascript
                        test: /\.js$/,
                        use: ['babel-loader'],
                        exclude: /(node_modules)/
                    },

                    { // CSS
                        test: /\.css$/,
                        use: function () {
                            return ExtractTextPlugin.extract({
                                fallback: 'style-loader',
                                use: [
                                    cssLoader,
                                    postCssLoader
                                ]
                            });
                        }()
                    },

                    { // SASS
                        test: /\.s[ac]ss$/,
                        use: function () {
                            return ExtractTextPlugin.extract({
                                fallback: 'style-loader',
                                use: [
                                    cssLoader,
                                    postCssLoader,
                                    sassLoader
                                ]
                            });
                        }()
                    },

                    { // Afbeeldingen
                        test: /\.(jpe?g|png|gif|svg)$/,
                        use: function () {
                            if (serverMode) {
                                return [
                                    {
                                        loader: 'file-loader',
                                        options: {
                                            name: '[path][name].[ext]',
                                            context: assetsConfig.output.root
                                        }
                                    }
                                ]
                            }

                            return [
                                {
                                    loader: 'url-loader',
                                    options: {
                                        limit: 10000,
                                        name: '[path][name]-[sha512:hash:hex:20].[ext]',
                                        context: assetsConfig.output.root
                                    }
                                },
                                {
                                    loader: 'image-webpack-loader',
                                    options: {
                                        bypassOnDebug: true,
                                        optipng: {
                                            optimizationLevel: 7,
                                        },
                                        gifsicle: {
                                            interlaced: false,
                                        },
                                    }
                                }
                            ];
                        }()
                    },

                    { // Fonts
                        test: /(fonts\/.+?(\.svg)|\.(eot|ttf|woff|woff2))(\?.+?)?$/,
                        use: function () {
                            if (serverMode) {
                                return [
                                    {
                                        loader: 'file-loader',
                                        options: {
                                            name: 'fonts/[name].[ext]',
                                            context: assetsConfig.output.root
                                        }
                                    }
                                ];
                            }

                            return [
                                {
                                    loader: 'file-loader',
                                    options: {
                                        name: '[name]-[sha512:hash:hex:20].[ext]',
                                        context: assetsConfig.output.root
                                    }
                                }
                            ];
                        }()
                    }
                ]
            }
        });

        /*
         * Afbeeldingen die niet in de CSS worden gebruikt embedden
         */
        if (!serverMode) {
            _.forEach(_.isArray(assetsConfig.keep.images) ? assetsConfig.keep.images : [assetsConfig.keep.images], (directory) => {
                config = merge(config, {
                    module: {
                        rules: [
                            {
                                test: new RegExp(_.escapeRegExp(path.resolve(directory)) + '\/.+\.(jpe?g|png|gif|svg)$'),
                                use: [
                                    {
                                        loader: 'file-loader',
                                        options: {
                                            name: '[path][name]-[sha512:hash:hex:20].[ext]',
                                            context: assetsConfig.output.root
                                        }
                                    }, {
                                        loader: 'image-webpack-loader',
                                        options: {
                                            bypassOnDebug: true,
                                            optipng: {
                                                optimizationLevel: 7,
                                            },
                                            gifsicle: {
                                                interlaced: false,
                                            },
                                        }
                                    }
                                ]
                            }
                        ]
                    }
                });
            });
        }

        /*
         * Plugins
         */
        config = merge(config, {
            plugins: [
                new Webpack.DefinePlugin({
                    'DEVELOPMENT': development(),
                    'PRODUCTION': production(),

                    'process.env': {
                        NODE_ENV: JSON.stringify(environments.current().$name),
                    }
                }),
                new Webpack.ProvidePlugin({
                    jQuery: 'jquery',
                    $: 'jquery'
                }),
                new CopyWebpackPlugin(assetsConfig.copy)
            ]
        });

        if (production()) {
            config = merge(config, {
                plugins: [
                    new Webpack.optimize.CommonsChunkPlugin({
                        name: 'vendor',
                        filename: serverMode ? 'vendor.js' : 'vendor-[chunkhash].js',
                        minChunks: Infinity
                    }),
                    // new Webpack.optimize.UglifyJsPlugin({
                    //     comments: false,
                    //     compress: {
                    //         warnings: true
                    //     }
                    // })
                ]
            });
        }

        return config;
    }
};
