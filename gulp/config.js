const self = module.exports;

const _ = require('lodash');

const autoprefixer = require('autoprefixer');
const path = require('path');
const url = require('url');

// Gulp + plugins
const util = require('gulp-util');
const environments = require('gulp-environments');

const Webpack = require('webpack');

// Webpack
const ConfigBuilder = require('webpack-configurator');

const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');

// PostCSS
const postcssFlexibility = require('postcss-flexibility');

// Environments
const development = environments.development;
const production = environments.production;
const staging = environments.make('staging');

const assetsConfig = require('../assets.config.js');

/**
 * Instellingen
 *
 * @type {{
 *  reloadEnabled: {boolean},
 *  hotReloadEnabled: {boolean}
 * }}
 */
module.exports.settings = {
    reloadEnabled:      development() && (util.env.reload === undefined || util.env.reload === true),
    hotReloadEnabled:   development() && (util.env.reload === undefined || util.env.reload === true) && (util.env.hot === undefined || util.env.hot === true),
};

/**
 * Webpack config genereren
 *
 * @param serverMode {boolean|undefined}
 */
module.exports.buildWebpackConfig = function (serverMode) {
    var config = new ConfigBuilder();

    var webpackConfig = {
        entry: assetsConfig.entrypoints
    };

    /*
     * Modules toevoegen voor (hot) reloading
     */
    if (serverMode && self.settings.reloadEnabled) {
        if (assetsConfig.output === undefined || assetsConfig.output.publicPath === undefined) {
            throw new util.PluginError('config', 'output.publicPath has to be set in the assets config in order for reloading to work properly');
        }

        var publicUrl = url.parse(assetsConfig.output.publicUrl);

        var reloadModules = [
            'webpack-dev-server/client?' + publicUrl.protocol + '//' + publicUrl.host  + '/'
        ];

        if (self.settings.hotReloadEnabled) {
            reloadModules.push('webpack/hot/dev-server');
        }

        _.forEach(webpackConfig.entrypoints, function (modules, entry) {
            if (!_.isArray(modules)) {
                modules = [modules];
            }

            webpackConfig.entrypoints[entry] = modules.concat(reloadModules);
        });
    }

    /*
     * Defaults
     */
    config.merge({
        node: {
            __dirname: true,
            __filename: true
        },

        output: {
            path: path.resolve('./web/assets'),
            publicPath: serverMode ? assetsConfig.output.publicUrl : assetsConfig.output.publicPath,

            filename: serverMode ? '[name].js' : '[name]-[chunkhash].js',
            sourceMapFilename: '[file].map'
        },

        postcss: [
            autoprefixer({
                browsers: ['last 2 versions']
            }),
            postcssFlexibility()
        ],

        resolve: {
            fallback: [
                path.resolve('./web/bundles/core/modules')
            ]
        }
    });

    if (!serverMode || !self.settings.reloadEnabled) {
        var extractCss = new ExtractTextPlugin(serverMode ? '[name].css' : '[name]-[chunkhash].css');

        config.merge({
            plugins: [
                extractCss
            ]
        });
    }

    if (development()) {
        config.merge({
            debug: true,
            devtool: 'cheap-source-map'
        });
    }

    /*
     * Loaders
     */
    config.loader('expose-jquery', {
        test: require.resolve('jquery'),
        loader: 'expose?$!expose?jQuery'
    });

    config.loader('babel', {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /(node_modules|bower_components)/,

        query: {
            presets: ['es2015']
        }
    });

    config.loader('css', {
        test: /\.css$/
    }, function (current) {
        var loader = 'css?sourceMap';

        current.loader = (serverMode && self.settings.reloadEnabled) ? 'style!' + loader : extractCss.extract('style', loader);

        return current;
    });

    config.loader('sass', {
        test: /\.s[ac]ss$/
    }, function (current) {
        var loader = 'css?importLoaders=1&sourceMap!postcss!sass?sourceMap';

        current.loader = (serverMode && self.settings.reloadEnabled) ? 'style!' + loader : extractCss.extract('style', loader);

        return current;
    });

    config.loader('images-with-url', {
        test: /\.(jpe?g|png|gif|svg)$/
    }, function (current) {
        if (serverMode) {
            current.loader = 'file?name=[path][name].[ext]&context=' + assetsConfig.manifest.root;
        } else {
            current.loader = 'url?limit=10000&name=[path][name]-[sha512:hash:hex:20].[ext]&context=' + assetsConfig.manifest.root + '!image-webpack?bypassOnDebug&optipng.optimizationLevel=7&gifsicle.interlaced=false';
        }

        return current;
    });

    _.forEach(_.isArray(assetsConfig.keep.images) ? assetsConfig.keep.images : [assetsConfig.keep.images], function (directory) {
        if (serverMode) {
            return;
        }

        var regex = _.escapeRegExp(path.resolve(directory)) + '\/.+\.(jpe?g|png|gif|svg)$';

        config.loader('images-in-' + directory, {
            test: new RegExp(regex)
        }, function (current) {
            current.loader = 'file?name=[path][name]-[sha512:hash:hex:20].[ext]&context=' + assetsConfig.manifest.root + '!image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false';

            return current;
        });
    });

    config.loader('fonts', {
        test: /\.(eot|svg|ttf|woff|woff2)(\?.+?)?$/
    }, function (current) {
        if (serverMode) {
            current.loader = 'file?name=fonts/[name].[ext]';
        } else {
            current.loader = 'file?name=fonts/[name]-[sha512:hash:hex:20].[ext]';
        }

        return current;
    });

    /*
     * Plugins
     */
    if (serverMode && self.settings.hotReloadEnabled) {
        config.plugin('hot-module-replacement', Webpack.HotModuleReplacementPlugin);
    }

    config.plugin('webpack-define', Webpack.DefinePlugin, [{
        'DEVELOPMENT': development() || staging(),
        'PRODUCTION': production(),

        'process.env': {
            NODE_ENV: JSON.stringify(environments.current().$name),
        }
    }]);

    config.plugin('webpack-provide', Webpack.ProvidePlugin, [{
        jQuery: 'jquery',
        $: 'jquery'
    }]);

    if (production() || staging()) {
        config.plugin('webpack-optimize-commons-chunk', Webpack.optimize.CommonsChunkPlugin, [{
            name: 'vendor',
            filename: serverMode ? 'vendor.js' : 'vendor-[chunkhash].js',
            minChunks: Infinity
        }]);

        config.plugin('dedupe', Webpack.optimize.DedupePlugin);

        config.plugin('uglify-js', Webpack.optimize.UglifyJsPlugin, [{
            comments: false
        }]);
    }

    if (!serverMode) {
        config.plugin('manifest', ManifestPlugin, [{
            basePath: 'assets/'
        }]);
    }

    config.plugin('copy', CopyWebpackPlugin, [assetsConfig.copy]);

    // Samenvoegen met bestaande config
    config.merge(webpackConfig);

    return config.resolve();
};

/**
 * Webpack server config genereren
 */
module.exports.buildWebpackServerConfig = function () {
    return self.buildWebpackConfig(true);
};

/**
 * Juiste "--env=[abc]" bepalen
 *
 * @returns string
 */
module.exports.symfonyConsoleEnvironment = function () {
    return (production()) ? '--env="production"' : '';
};