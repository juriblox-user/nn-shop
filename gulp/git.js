// Node plugins
const os = require('os');
const git = require('git-rev');
const Q = require('q');

/**
 * Git info uitlezen
 *
 * @param callback
 */
module.exports.info = (callback) => {
	const chain = Q.defer();

	let revision = {
		tag:		null,
		branch:		null,
		longHash:	null,
		shortHash:	null,
		version:	null,
		machine:	null,
		timestamp: 	null
	};

	Q.fcall(() => {})
		
		// Lange SHA1 hash
		.then(() => {
			const sub = Q.defer();

			git.long((longHash) => {
				revision.longHash = longHash;
				sub.resolve();
			});

			return sub.promise;
		})

		// Korte SHA1 hash
		.then(() => {
            const sub = Q.defer();

			git.short((shortHash) => {
				revision.shortHash = shortHash;
				sub.resolve();
			});

			return sub.promise;
		})

		// Branch
		.then(() => {
			const sub = Q.defer();

			git.branch((branch) => {
				revision.branch = branch;
				sub.resolve();
			});

			return sub.promise;
		})

		// Tag
		.then(() => {
            const sub = Q.defer();

			git.tag((tag) => {
				revision.tag = (tag === revision.longHash) ? '' : tag;
				sub.resolve();
			});

			return sub.promise;
		})

		// Proces afronden
		.done(() => {
			chain.resolve(revision);
		});

	chain.promise.done(function (revision) {
		revision.version = (revision.tag ? revision.tag : revision.branch);
		revision.machine = os.hostname();
		revision.timestamp = Math.floor(new Date().getTime() / 1000);

		callback(revision);
	});
};