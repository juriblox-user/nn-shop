/*
 * Environment bepalen
 */
const argv = require('yargs').argv;
const environments = require('gulp-environments');

require('dotenv').config({
	silent: true
});

environments.current(environments.make(argv.env || process.env.APP_ENVIRONMENT || process.env.NODE_ENV || 'development'));

// Gulp + plugins
const gulp = require('gulp');
const util = require('gulp-util');

let registeredTasks = [];

/**
 * Wrapper om Gulp waarmee we ook alle andere taken registreren.
 */
module.exports = (tasks) => {
	registeredTasks = tasks;

	tasks.forEach((task) => {
		if (undefined === task.dependencies || task.dependencies.length === 0) {
			gulp.task(task.name, require('./tasks/' + task.name.replace(':', '/')));
		} else {
			gulp.task(task.name, gulp.series(task.dependencies), require('./tasks/' + task.name.replace(':', '/')));
		}
	});

	return gulp;
};

/**
 * Standaard "help" task.
 */
gulp.task('default', () => {
	util.log('');
	util.log('gulp [task] ' + util.colors.gray('[--env development|production] [options]'));

	util.log('');
	util.log(util.colors.green('The following tasks have been registered with Gulp:'));

	registeredTasks.forEach((task) => {
		let line = '  - gulp ' + util.colors.bold(task.name);
		if (task.dependencies !== undefined && task.dependencies.length > 0) {
			line += ' -> depends on ' + task.dependencies.join(', ');
		}

		util.log(line);
	});

	util.log('');
	util.log('Current environment: ' +  util.colors.bold(environments.current().$name));

	util.log('');
});