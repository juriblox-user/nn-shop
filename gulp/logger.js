// Node plugins
const sprintf = require('sprintf-js').sprintf;

// Gulp + plugins
const util = require('gulp-util');

/**
 * Module naam
 *
 * @type string
 */
let moduleName = 'unknown';

/**
 * Constructor waarin we meteen de module naam bepalen
 *
 * @param filename
 */
module.exports = (filename) => {
	if (filename === undefined)
	{
		throw new util.PluginError('logger', 'You are required to pass the module\'s __filename to the logger constructor');
	}

	return module.exports;
};

/**
 * "✔ done" regel schrijven.
 */
module.exports.done = () => {
	module.exports.log(util.colors.green('✔') + ' done');
};

/**
 * Log regel schrijven (met sprintf en multiline ondersteuning)
 *
 * @param message
 */
module.exports.log = function (message) {
	message = sprintf.apply(null, arguments);

	message.trim().split("\n").forEach(function (line) {
		util.log(sprintf(util.colors.gray('[%s]'), moduleName), line);
	});
};

/**
 * Bestandsnaam instellen voor de logger
 *
 * @param filename
 */
module.exports.setFilename = (filename) => {
	const matches = (new RegExp('gulp/tasks/(([\\w\-]+)\/){0,1}([\\w\-]+)\.js$', 'g')).exec(filename);
	if (matches === null)
	{
		throw new util.PluginError('logger', 'Could not generate module name based on filename "' + filename + '"');
	}

	if (matches[2] === undefined)
	{
		moduleName = matches[3];
	}
	else
	{
		moduleName = sprintf('%s:%s', matches[2], matches[3]);
	}
};