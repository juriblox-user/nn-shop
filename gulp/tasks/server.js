// Node plugins
const path = require('path');
const rimraf = require('rimraf');
const webpack = require('webpack');

const Q = require('q');

const WebpackServer = require('webpack-dev-server');

// Gulp + plugins
const gulp = require('gulp');
const util = require('gulp-util');

const environments = require('gulp-environments');

// Applicatie plugins
const Builder = require('../builder.js').Builder;
const logger = require('../logger.js');

// Environments
const development = environments.development;
const production = environments.production;

module.exports = () => {
    logger.setFilename(__filename);

    if (production()) {
        throw new util.PluginError('server', 'It makes no sense to run the Webpack dev-server in production mode');
    }

    const builder = new Builder('./web/assets', './var/themes');
    const serverJsConf = builder.getServerJsConfig();

    logger.log('Webpack ' + util.colors.bgGreen(util.colors.black('development')) + ' server is starting on 0.0.0.0:' + serverJsConf.port);
    logger.log();

    /*
     * Oude /web/assets verwijderen
     */
    Q.fcall(() => {
        logger.log('Removing assets output directory');

        rimraf.sync(builder.getOutputPath());

        logger.done();
    })

    /*
     * Voorbereiden
     */
    .then(() => {
        return builder.prepare();
    })

    /*
     * Webpack server
     */
    .then(() => {
        logger.log('Starting server');

        const config = builder.getServerConfig();

        try {
            new WebpackServer(webpack(config), {
                contentBase: '/web',
                publicPath: config.output.publicPath,
                disableHostCheck: true,
                headers: {
                    'Access-Control-Allow-Origin': '*'
                },

                quiet: false,
                noInfo: false,

                // From webpack docs: "If watching does not work for
                // you try out this option. Watching does not work
                // with NFS and machines in VirtualBox."
                watchOptions: {
                    poll: 1000
                },

                stats: 'normal'
            }).listen(serverJsConf.port, '0.0.0.0', (error) => {
                if (error) {
                    throw new util.PluginError('server', error);
                }
            });
        } catch (e) {
            if (e) {
                throw new util.PluginError('server', e);
            }
        }

    });

    return Q.defer().promise;
};
