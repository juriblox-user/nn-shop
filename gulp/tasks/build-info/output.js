// Node plugins
const _ = require('lodash');

// Gulp plugins
const util = require('gulp-util');

// Applicatie plugins
const git = require('../../git.js');
const logger = require('../../logger.js');

/**
 * Environment vars met build info weergeven
 *
 * @returns {*}
 */
module.exports = function () {
	logger.setFilename(__filename);

	git.info(function (revision) {
		_.forEach(revision, function (value, key) {
			logger.log(util.colors.bold(key + ' =>') + ' ' + (value ? value : util.colors.gray('null')));
		});
	});
};