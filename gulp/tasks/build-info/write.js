const fs = require('fs');

// Applicatie plugins
const git = require('../../git.js');
const logger = require('../../logger.js');

/**
 * Environment vars naar .build schrijven
 *
 * @returns {*}
 */
module.exports = function (callback) {
	logger.setFilename(__filename);

	git.info(function (revision) {
		var variables = [
			'BUILD_HASH=' + revision.longHash,
			'BUILD_HASH_SHORT=' + revision.shortHash,
			'BUILD_TAG=' + revision.tag,
			'BUILD_BRANCH=' + revision.branch,
			'BUILD_VERSION=' + revision.version,
			'BUILD_MACHINE=' + revision.machine,
			'BUILD_TIMESTAMP=' + revision.timestamp
		];

		fs.writeFile('./.build', variables.join("\n"), function () {
			callback();
		});
	});
};