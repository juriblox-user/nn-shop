// Node plugins
const _ = require('lodash');

const webpack = require('webpack');
const Q = require('q');

// Gulp + plugins
const gulp = require('gulp');
const util = require('gulp-util');

const environments = require('gulp-environments');

// Applicatie plugins
const Builder = require('../builder.js').Builder;
const logger = require('../logger.js');

// Environments
const development = environments.development;

module.exports = () => {
    logger.setFilename(__filename);

    const taskPromise = Q.defer();
    const buildType = (development() ? util.colors.bgGreen(util.colors.black('development')) : util.colors.bgRed(util.colors.white(environments.current().$name)));

    logger.log('Running Webpack %s build', buildType);

    const builder = new Builder('./web/assets');

    /*
     * Webpack
     */
    builder.prepare().then(() => {
        const promise = Q.defer();

        Q.fcall(() => {
            logger.log('Generating Webpack config...');

            const config = builder.getBuildConfig();

            logger.done();

            return config;
        }).then((config) => {
            logger.log('Building assets...');
            logger.log(util.colors.gray('Path: ' + config.output.path));

            webpack(config, (error, stats) => {
                util.log(stats.toString({
                    colors: true
                }));

                if (stats.compilation.errors.length > 0) {
                    throw new util.PluginError('build', 'An error occured while running the Webpack build');
                }

                logger.done();
                promise.resolve();
            });
        });

        return promise.promise;
    })

    /*
     * Afronden
     */
        .then(() => {
            logger.log(util.colors.bold(util.colors.green('Assets build completed')));

            taskPromise.resolve();
        });

    return taskPromise.promise;
};