// Node plugins
const del = require('del');
const path = require('path');

// Gulp plugins
const util = require('gulp-util');

// Applicatie plugins
const logger = require('../../logger.js');

/**
 * "assets" map verwijderen
 *
 * @returns {*}
 */
module.exports = function () {
	logger.setFilename(__filename);

	logger.log('Deleted ' + util.colors.bold('./web/assets'));

	return del([
		path.resolve('./web/assets')
	]);
};