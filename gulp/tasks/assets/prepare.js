// Node plugins
const exec = require('child_process').exec;
const del = require('del');
const fs = require('fs');
const Q = require('q');

// Gulp + plugins
const util = require('gulp-util');

// Applicatie plugins
const config = require('../../config.js');
const logger = require('../../logger.js');

/**
 * Symfony assets installeren d.m.v. "assets:install" CLI command
 *
 * @returns {Array|{index: number, input: string}}
 */
module.exports = function () {
	logger.setFilename(__filename);

	var deferred = Q.defer();

    logger.log('Installing Symfony assets...');

	exec('php bin/console assets:install --symlink ' + config.symfonyConsoleEnvironment(), function (error, stdout) {
		if (error) {
			throw error;
		}

		logger.log(stdout);

		deferred.resolve();
	});

	return deferred.promise;
};