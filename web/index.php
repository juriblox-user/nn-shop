<?php

use Core\Application\AbstractKernel;
use Symfony\Component\HttpFoundation\Request;

/**
 * @var Composer\Autoload\ClassLoader
 */
$loader = require __DIR__ . '/../app/autoload.php';

/*
 * robots.txt
 */
if (isset($_SERVER['REQUEST_URI']) && 'robots.txt' == trim($_SERVER['REQUEST_URI'], '/')) {
    if (!isset($_SERVER['HTTP_HOST']) || '.juribloxdev.nl' == mb_substr($_SERVER['HTTP_HOST'], -15) || 'beheer.' == mb_substr($_SERVER['HTTP_HOST'], 0, 7)) {
        die("User-agent: *\nDisallow: /");
    }

    die("User-agent: *\nAllow: /");
}

if (AbstractKernel::globalEnvironmentIs([AbstractKernel::ENVIRONMENT_PRODUCTION, AbstractKernel::ENVIRONMENT_STAGING])) {
    require_once __DIR__ . '/../app/AppCache.php';
}

$kernel = AppKernel::fromEnvironment($loader);

/*
 * Toegangscontrole
 */
if (false !== getenv('APP_RESTRICTED') && (true === getenv('APP_RESTRICTED') || '0' !== getenv('APP_RESTRICTED'))) {
    $kernel->denyAccess();
    $kernel->allowAccessFile('../.access.yml');

    $kernel->checkAccess();
}

$request = Request::createFromGlobals();

$response = $kernel->handle($request);
$response->send();

$kernel->terminate($request, $response);
